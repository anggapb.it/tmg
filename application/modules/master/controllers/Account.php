<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('account_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'account/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaTTD") like \'%'.$filter['key'].'%\' OR
					upper(tbl."InisialBank") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Bank") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoRek") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->account_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->account_model->set_order(array('NamaTTD' => 'ASC'));
		//
		$this->account_model->set_limit($limit);
		$this->account_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->account_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadAccount';
		$page['list'] 		= $this->gen_paging($page, true);
		//
		$data = array();
		$data['content'] = 'account/list';		
		$data['list'] = $this->account_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$account =  $this->account_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'account/input';
		$data['account'] = $account;
		$data['title'] = 'Input Account';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$idAccount		 	= (decode($this->input->post('idAccount'))?:0);
		$data['NamaTTD']	= $this->input->post('NamaTTD');
		$data['Bank']		= $this->input->post('Bank');
		$data['NoRek']		= $this->input->post('NoRek');
		$data['NamaRekening']	= $this->input->post('NamaRekening');
		$data['Status']		= $this->input->post('Status')?1:0;
		$data['InisialBank']	= $this->input->post('InisialBank');
		
		
		if (!$data['NamaRekening']) {
			$this->error('Nama Rekening Tidak Boleh Ada Kosong');
		}
		
		if ($idAccount)
		{	
			$data['idAccount'] 	= $idAccount;
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['updated_by'] = $this->session->userdata('Operator')['LoginName'];
		}else
		{
			/* $this->db->select('tbl."idAccount"');
			$this->db->order_by('idAccount','desc');
			$res = $this->db->get('dataMaster.msSupBenang tbl',1)->row(); */
			
			$data['idAccount'] = 0;	
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['created_by'] = $this->session->userdata('Operator')['LoginName'];
		}
		
		//validasi data kosong
		/* $data['KodeAccount'] = $this->validation_input('kode_account', 1, 10);*/
		$this->validation_input('NamaTTD'); 
		
		if(!$idAccount) {
			$account =  $this->account_model->get(array('NamaTTD' => $data['NamaTTD']));
			
			if($account['NamaTTD']) {
				if($data['NamaTTD']==$account['NamaTTD']){
					$this->error('Nama Account sudah ada');
				}
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->account_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idAccount'] = encode($data['idAccount']);
			$this->update['status'] = $idAccount ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->account_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->account_model->set_limit($limit);
		$this->account_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NamaTTD") like \'%'.$filter['key'].'%\' OR					
					upper(tbl."InisialBank") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Bank") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoRek") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->account_model->set_order(array('NamaTTD' => 'ASC'));
		$this->account_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->account_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataAccount';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->account_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'account/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_account() {
		$code = trim(strtoupper($this->input->post('code')));
		$account = $this->account_model->get(array('upper("tbl"."NamaTTD")' => $code));
		$account['idAccount'] = encode($account['idAccount']);
		echo json_encode($account);		
    }
}