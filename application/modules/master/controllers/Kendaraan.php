<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kendaraan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('kendaraan_model');
		$this->load->model('jenis_kendaraan_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'kendaraan/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Merk") like \'%'.$filter['key'].'%\' OR
					upper(tbl."PlatNomor") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Type") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Dimensi") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Kondisi") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoBPKB") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->kendaraan_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->kendaraan_model->set_order(array('NamaKendaraan' => 'ASC'));
		//
		$this->kendaraan_model->set_limit($limit);
		$this->kendaraan_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->kendaraan_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadKendaraan';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'kendaraan/list';		
		$data['list'] = $this->kendaraan_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$kendaraan =  $this->kendaraan_model->get($id);		
		
		//
		$data = array();		
		$data['jenis_kendaraan'] = $this->jenis_kendaraan_model->get_list();
		$data['content'] = 'kendaraan/input';
		$data['kendaraan'] = $kendaraan;
		$data['title'] = 'Input Kendaraan';
		$this->load->view($data['content'],$data);
	}
	
	function change_pict($id=0, $type='')
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		/* $config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = 100; */
		$_this->load->library('image_lib',$config);
		
		$date = $id.$type.date('YmdHis');
		$structure  = 'files/kendaraan/original';
		$structure2  = 'files/kendaraan/thumbnails_29x29';
		$structure3  = 'files/kendaraan/thumbnails_45x45';
		$structure4  = 'files/kendaraan/thumbnails_90x90';
		$structure5  = 'files/kendaraan/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['Photo'.$type];
		
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);

		if($file_ext != 'jpeg' && $file_ext != 'jpg' && $file_ext != 'gif' && $file_ext != 'png') {
			$this->error('Format photo '.str_replace('_', ' ', $type).' harus jpg/png');
		}
		
		$upload_image = $structure.'/'.$date.'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{			
			$img_size = getimagesize($upload_image);	
		
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"source_image"  => $upload_image,
					"new_image"     => './files/kendaraan/'.$r['name'].'/'.$date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize())       {             				
					$this->error($_this->image_lib->display_errors());
				}
				$_this->image_lib->clear(); 
				
				if($r['width'].'x'.$r['height'] == '90x90') {
					$kendaraan = $this->kendaraan_model->get(decode($id));
					
					if($kendaraan['Photo'.$type]) {
						if($kendaraan['Photo'.$type] != $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext) {
							$photo = str_replace('-90x90', '', $kendaraan['Photo'.$type]);
							$check_ext = explode(".", $photo);
							$check_file_ext = end($check_ext);
							
							$structure  = 'files/kendaraan/original/'.$photo;
							$structure2  = 'files/kendaraan/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
							$structure3  = 'files/kendaraan/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
							$structure4  = 'files/kendaraan/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
							$structure5  = 'files/kendaraan/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
							
							if(file_exists($structure))
								unlink($structure);
						
							if(file_exists($structure2))
								unlink($structure2);
							
							if(file_exists($structure3))
								unlink($structure3);
							
							if(file_exists($structure4))
								unlink($structure4);
							
							if(file_exists($structure5))
								unlink($structure5);
						}
					} 
					
					$fileName = $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext;
					$update = array();
					$update['idKendaraan'] 	= decode($id);
					$update['Photo'.$type] 		= $fileName;
					
					$this->kendaraan_model->save($update);
				}
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$this->error('Foto gagal disimpan');
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
	}
	
	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
		$data = array();
		$this->db->trans_start();
		$idKendaraan		 	= (decode($this->input->post('idKendaraan'))?:0);
		$data['NamaKendaraan']	= $this->input->post('NamaKendaraan');
		$data['PlatNomor']		= $this->input->post('PlatNomor');
		$data['Merk']			= $this->input->post('Merk');
		$data['Type']			= $this->input->post('Type');
		$data['fidJenisKendaraan']	= $this->input->post('fidJenisKendaraan')?:0;
		$data['Model']			= $this->input->post('Model');
		$data['TahunPembuatan']		= $this->input->post('TahunPembuatan');
		$data['Silinder']		= $this->input->post('Silinder');		
		$data['NoRangka']		= $this->input->post('NoRangka');
		$data['NoMesin']		= $this->input->post('NoMesin');
		$data['Warna']			= $this->input->post('Warna');
		$data['BahanBakar']		= $this->input->post('BahanBakar');
		$data['WarnaTNKB']		= $this->input->post('WarnaTNKB');
		$data['TahunRegistrasi']	= $this->input->post('TahunRegistrasi');
		$data['NoBPKB']			= $this->input->post('NoBPKB');
		$data['Kondisi']		= $this->input->post('Kondisi');
		$data['NoUrutDaftar']	= $this->input->post('NoUrutDaftar');
		$data['PanjangKaroseri']	= $this->input->post('PanjangKaroseri')?:0;
		$data['LebarKaroseri']	= $this->input->post('LebarKaroseri')?:0;
		$data['TinggiKaroseri']	= $this->input->post('TinggiKaroseri')?:0;
		$data['Dimensi']		= $this->input->post('Dimensi')?:0;
		$data['BeratKosong']	= $this->input->post('BeratKosong')?:0;
		$data['BeratMax']		= $this->input->post('BeratMax')?:0;
		$data['PanjangMobil']	= $this->input->post('PanjangMobil')?:0;
		$data['LebarMobil']		= $this->input->post('LebarMobil')?:0;
		$data['TinggiMobil']	= $this->input->post('TinggiMobil')?:0;
		$data['KecepatanMax']	= $this->input->post('KecepatanMax')?:0;
		$data['TenagaMax']		= $this->input->post('TenagaMax')?:0;
		$data['UkuranBan']		= $this->input->post('UkuranBan')?:0;
		$data['UkuranRoda']		= $this->input->post('UkuranRoda')?:0;
		$data['KapasitasMuatan']	= $this->input->post('KapasitasMuatan')?:0;
				
		if ($idKendaraan)
		{	
			$data['idKendaraan'] 	= $idKendaraan;
		}else
		{
			$data['idKendaraan'] = 0;	
		}
		
		if(!$data['NamaKendaraan']) {
			$this->update['CallBack'] = 'NamaKendaraan';
			$this->error('Nama Kendaraan Harus diisi');
		}
		//validasi data kosong
		$this->validation_input('NamaKendaraan');
		$this->validation_input('NoBPKB');
		
		if(!$idKendaraan) {
			$kendaraan =  $this->kendaraan_model->get(array('NamaKendaraan' => $data['NamaKendaraan']));
			if($data['NamaKendaraan']==$kendaraan['NamaKendaraan']){
				$this->error('Nama Kendaraan sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->kendaraan_model->save($data);
		
		if(!$idKendaraan)
		$idKendaraan = $this->db->insert_id('"dataMaster"."msKendaraan_idKendaraan_seq"');
				
		if($idKendaraan && $_FILES['PhotoKendaraan1']['name']) {
			$this->change_pict(encode($idKendaraan), 'Kendaraan1');
		}
		if($idKendaraan && $_FILES['PhotoKendaraan2']['name']) {
			$this->change_pict(encode($idKendaraan), 'Kendaraan2');
		}
		if($idKendaraan && $_FILES['PhotoKendaraan3']['name']) {
			$this->change_pict(encode($idKendaraan), 'Kendaraan3');
		}
				
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idKendaraan'] = encode($idKendaraan);
			$this->update['status'] = $idKendaraan ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$kendaraan = $this->kendaraan_model->get($Code);
		$this->kendaraan_model->delete($Code);
		
		if($kendaraan['PhotoKendaraan1']) {
			$photo = str_replace('-90x90', '', $kendaraan['PhotoKendaraan1']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/kendaraan/original/'.$photo;
			$structure2  = 'files/kendaraan/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/kendaraan/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/kendaraan/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/kendaraan/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($kendaraan['PhotoKendaraan2']) {
			$photo = str_replace('-90x90', '', $kendaraan['PhotoKendaraan2']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/kendaraan/original/'.$photo;
			$structure2  = 'files/kendaraan/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/kendaraan/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/kendaraan/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/kendaraan/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($kendaraan['PhotoKendaraan3']) {
			$photo = str_replace('-90x90', '', $kendaraan['PhotoKendaraan3']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/kendaraan/original/'.$photo;
			$structure2  = 'files/kendaraan/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/kendaraan/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/kendaraan/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/kendaraan/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->kendaraan_model->set_limit($limit);
		$this->kendaraan_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Merk") like \'%'.$filter['key'].'%\' OR
					upper(tbl."PlatNomor") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Type") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Dimensi") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Kondisi") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoBPKB") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->kendaraan_model->set_where($where);
		
		$this->pengemudi_model->set_order(array('idKendaraan' => 'DESC'));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->kendaraan_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataKendaraan';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->kendaraan_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'kendaraan/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_kendaraan() {
		$NamaKendaraan = trim($this->input->post('NamaKendaraan'));
		$kendaraan = $this->kendaraan_model->get(array('NamaKendaraan' => $NamaKendaraan));
		$kendaraan['idKendaraan'] = encode($kendaraan['idKendaraan']);
		echo json_encode($kendaraan);		
    }
}