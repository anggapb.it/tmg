<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengemudi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('pengemudi_model');
		$this->load->model('human-capital/religion_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'pengemudi/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoHP") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Alamat") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NIK") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->pengemudi_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->pengemudi_model->set_order(array('NamaLengkap' => 'ASC'));
		//
		$this->pengemudi_model->set_limit($limit);
		$this->pengemudi_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->pengemudi_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadPengemudi';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'pengemudi/list';		
		$data['list'] = $this->pengemudi_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$pengemudi =  $this->pengemudi_model->get($id);		
		
		//
		$data = array();
		$religion =  $this->religion_model->get_list();	
		
		$data['content'] = 'pengemudi/input';
		$data['religion'] = $religion;
		$data['pengemudi'] = $pengemudi;
		$data['title'] = 'Input Pengemudi';
		$this->load->view($data['content'],$data);
	}
	
	function change_pict($id=0, $type='')
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		/* $config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = 100; */
		$_this->load->library('image_lib',$config);
		
		$date = $id.$type.date('YmdHis');
		$structure  = 'files/pengemudi/original';
		$structure2  = 'files/pengemudi/thumbnails_29x29';
		$structure3  = 'files/pengemudi/thumbnails_45x45';
		$structure4  = 'files/pengemudi/thumbnails_90x90';
		$structure5  = 'files/pengemudi/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['Photo'.$type];
		
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);

		if($file_ext != 'jpeg' && $file_ext != 'jpg' && $file_ext != 'gif' && $file_ext != 'png') {
			$this->error('Format photo '.str_replace('_', ' ', $type).' harus jpg/png');
		}
		
		$upload_image = $structure.'/'.$date.'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{			
			$img_size = getimagesize($upload_image);	
		
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"source_image"  => $upload_image,
					"new_image"     => './files/pengemudi/'.$r['name'].'/'.$date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize())       {             				
					$this->error($_this->image_lib->display_errors());
				}
				$_this->image_lib->clear(); 
				
				if($r['width'].'x'.$r['height'] == '90x90') {
					$pengemudi = $this->pengemudi_model->get(decode($id));
					
					if($pengemudi['Photo'.$type]) {
						if($pengemudi['Photo'.$type] != $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext) {
							$photo = str_replace('-90x90', '', $pengemudi['Photo'.$type]);
							$check_ext = explode(".", $photo);
							$check_file_ext = end($check_ext);
							
							$structure  = 'files/pengemudi/original/'.$photo;
							$structure2  = 'files/pengemudi/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
							$structure3  = 'files/pengemudi/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
							$structure4  = 'files/pengemudi/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
							$structure5  = 'files/pengemudi/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
							
							if(file_exists($structure))
								unlink($structure);
						
							if(file_exists($structure2))
								unlink($structure2);
							
							if(file_exists($structure3))
								unlink($structure3);
							
							if(file_exists($structure4))
								unlink($structure4);
							
							if(file_exists($structure5))
								unlink($structure5);
						}
					} 
					
					$fileName = $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext;
					$update = array();
					$update['idPengemudi'] 	= decode($id);
					$update['Photo'.$type] 		= $fileName;
					
					$this->pengemudi_model->save($update);
				}
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$this->error('Foto gagal disimpan');
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
	}
	
	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
		$data = array();
		$this->db->trans_start();
		$idPengemudi		 	= (decode($this->input->post('idPengemudi'))?:0);
		$data['NamaLengkap']	= $this->input->post('NamaLengkap');
		$data['NamaPanggilan']	= $this->input->post('NamaPanggilan');
		$data['NoHP']			= $this->input->post('NoHP');
		$data['Alamat']			= $this->input->post('Alamat');
		$data['Email']			= $this->input->post('Email');
		$data['NIK']			= $this->input->post('NIK');
		$data['TglLahir']		= getSQLDate($this->input->post('TglLahir'));
		$data['GolDarah']		= $this->input->post('GolDarah');		
		$data['fidMsReligion']	= $this->input->post('fidMsReligion');
		$data['PunyaSIM_A']		= $this->input->post('PunyaSIM_A')?1:0;
		$data['PunyaSIM_B1']	= $this->input->post('PunyaSIM_B1')?1:0;
		$data['PunyaSIM_B2']	= $this->input->post('PunyaSIM_B2')?1:0;
		$data['Status']		 	= $this->input->post('Status');
		$data['Bank']		 	= $this->input->post('Bank');
		$data['NoRekening']		= $this->input->post('NoRekening');
				
		if ($idPengemudi)
		{	
			$data['idPengemudi'] 	= $idPengemudi;
		}else
		{
			$data['idPengemudi'] = 0;	
		}
		
		if(!$data['NamaLengkap']) {
			$this->update['CallBack'] = 'NamaLengkap';
			$this->error('Nama Lengkap Harus diisi');
		}
		//validasi data kosong
		$this->validation_input('NamaLengkap');
		$this->validation_input('NoHP');
		$this->validation_input('Email');
		
		if(!$idPengemudi) {
			$pengemudi =  $this->pengemudi_model->get(array('NamaLengkap' => $data['NamaLengkap']));
			if($data['NamaLengkap']==$pengemudi['NamaLengkap']){
				$this->error('Nama Lengkap sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->pengemudi_model->save($data);
		
		if(!$idPengemudi)
		$idPengemudi = $this->db->insert_id('"dataMaster"."msPengemudi_idPengemudi_seq"');
				
		if($idPengemudi && $_FILES['PhotoProfile']['name']) {
			$this->change_pict(encode($idPengemudi), 'Profile');
		}
		if($idPengemudi && $_FILES['PhotoKTP']['name']) {
			$this->change_pict(encode($idPengemudi), 'KTP');
		}
		if($idPengemudi && $_FILES['PhotoSIM_A']['name']) {
			$this->change_pict(encode($idPengemudi), 'SIM_A');
		}
		if($idPengemudi && $_FILES['PhotoSIM_B1']['name']) {
			$this->change_pict(encode($idPengemudi), 'SIM_B1');
		}
		if($idPengemudi && $_FILES['PhotoSIM_B2']['name']) {
			$this->change_pict(encode($idPengemudi), 'SIM_B2');
		}
		
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idPengemudi'] = encode($idPengemudi);
			$this->update['status'] = $idPengemudi ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$pengemudi = $this->pengemudi_model->get($Code);
		$this->pengemudi_model->delete($Code);
		
		if($pengemudi['PhotoProfile']) {
			$photo = str_replace('-90x90', '', $pengemudi['PhotoProfile']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/pengemudi/original/'.$photo;
			$structure2  = 'files/pengemudi/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/pengemudi/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/pengemudi/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/pengemudi/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($pengemudi['PhotoKTP']) {
			$photo = str_replace('-90x90', '', $pengemudi['PhotoKTP']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/pengemudi/original/'.$photo;
			$structure2  = 'files/pengemudi/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/pengemudi/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/pengemudi/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/pengemudi/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($pengemudi['PhotoSIM_A']) {
			$photo = str_replace('-90x90', '', $pengemudi['PhotoSIM_A']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/pengemudi/original/'.$photo;
			$structure2  = 'files/pengemudi/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/pengemudi/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/pengemudi/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/pengemudi/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($pengemudi['PhotoSIM_B1']) {
			$photo = str_replace('-90x90', '', $pengemudi['PhotoSIM_B1']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/pengemudi/original/'.$photo;
			$structure2  = 'files/pengemudi/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/pengemudi/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/pengemudi/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/pengemudi/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		if($pengemudi['PhotoSIM_B2']) {
			$photo = str_replace('-90x90', '', $pengemudi['PhotoSIM_B2']);
			$ext = explode(".", $photo);
			$file_ext = end($ext);
			
			$structure  = 'files/pengemudi/original/'.$photo;
			$structure2  = 'files/pengemudi/thumbnails_29x29/'.$ext[0].'-29x29.'.$file_ext;
			$structure3  = 'files/pengemudi/thumbnails_45x45/'.$ext[0].'-45x45.'.$file_ext;
			$structure4  = 'files/pengemudi/thumbnails_90x90/'.$ext[0].'-90x90.'.$file_ext;
			$structure5  = 'files/pengemudi/thumbnails_128x128/'.$ext[0].'-128x128.'.$file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->pengemudi_model->set_limit($limit);
		$this->pengemudi_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoHP") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Alamat") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NIK") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->pengemudi_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->pengemudi_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataPengemudi';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->pengemudi_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'pengemudi/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_pengemudi() {
		$NamaLengkap = trim($this->input->post('NamaLengkap'));
		$pengemudi = $this->pengemudi_model->get(array('NamaLengkap' => $NamaLengkap));
		$pengemudi['idPengemudi'] = encode($pengemudi['idPengemudi']);
		echo json_encode($pengemudi);		
    }
}