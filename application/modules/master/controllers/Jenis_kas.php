<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_kas extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('jenis_kas_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'jenis_kas/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."Nama") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Keterangan") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->jenis_kas_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->jenis_kas_model->set_order(array('Nama' => 'ASC'));
		//
		$this->jenis_kas_model->set_limit($limit);
		$this->jenis_kas_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->jenis_kas_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadJenisKas';
		$page['list'] 		= $this->gen_paging($page, true);
		//
		$data = array();
		$data['content'] = 'jenis_kas/list';		
		$data['list'] = $this->jenis_kas_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$jenis_kas =  $this->jenis_kas_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'jenis_kas/input';
		$data['jenis_kas'] = $jenis_kas;
		$data['title'] = 'Input Jenis Kas';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$idJenisKas		= (decode($this->input->post('idJenisKas'))?:0);
		$data['Nama'] 	= $this->input->post('Nama');
		$data['Keterangan']	= $this->input->post('Keterangan');
		
		/* if ($data['KodeSupplier'] == trim($data['KodeSupplier']) && strpos($data['KodeSupplier'], ' ') !== false) {
			$this->error('Kode Supplier Tidak Boleh Ada Spasi');
		} */
		
		if ($idJenisKas)
		{	
			$data['idJenisKas'] 	= $idJenisKas;
		}else
		{
			/* $this->db->select('tbl."idSupplier"');
			$this->db->order_by('idSupplier','desc');
			$res = $this->db->get('dataMaster.msSupBenang tbl',1)->row(); */
			
			$data['idJenisKas'] = 0;	
		}
		
		//validasi data kosong
		/* $data['KodeSupplier'] = $this->validation_input('kode_jenis_kas', 1, 10);*/
		$this->validation_input('Nama'); 
		
		if(!$idJenisKas) {
			$jenis_kas =  $this->jenis_kas_model->get(array('Nama' => $data['Nama']));
			
			if($jenis_kas['Nama']) {
				if($data['Nama']==$jenis_kas['Nama']){
					$this->error('Nama Jenis Kas sudah ada');
				}
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->jenis_kas_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idJenisKas'] = encode($data['idJenisKas']);
			$this->update['status'] = $idJenisKas ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}

	/* function cek_detail() {
		$this->load->model('pembelian/po_benang_model');
		
		$fidSupplier = decode($this->input->post('fidSupplier'));
		$po_benang = $this->po_benang_model->get(array('fidSupplier' => $fidSupplier));		
		
		if($po_benang['fidSupplier']) {
			$message = 'Kode Supplier tidak bisa dihapus, karena masih ada di Order Benang. silahkan hapus dulu Order Benang untuk Kode Suppliier ini
						di Transaksi Order Benang.';
			$this->error($message);
		} 
		
		$this->success('data kode Supplier sudah kosong, tidak ada di transaksi order benang');		
	} */
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->jenis_kas_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->jenis_kas_model->set_limit($limit);
		$this->jenis_kas_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("Nama") like \'%'.$lookupkey.'%\'
				or upper("Keterangan") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->jenis_kas_model->set_order(array('KodeSupplier' => 'ASC'));
		$this->jenis_kas_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->jenis_kas_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataJenisKas';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->jenis_kas_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'jenis_kas/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_jenis_kas() {
		$code = trim(strtoupper($this->input->post('code')));
		$jenis_kas = $this->jenis_kas_model->get(array('upper("tbl"."Nama")' => $code));
		$jenis_kas['idJenisKas'] = encode($jenis_kas['idJenisKas']);
		echo json_encode($jenis_kas);		
    }
}