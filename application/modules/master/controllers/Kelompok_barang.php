<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelompok_barang extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('kelompok_barang_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'kelompok_barang/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaKelompokBarang") like \'%'.$filter['key'].'%\' OR
					upper(tbl."KodeKelompokBarang") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->kelompok_barang_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->kelompok_barang_model->set_order(array('KodeKelompokBarang' => 'ASC'));
		//
		$this->kelompok_barang_model->set_limit($limit);
		$this->kelompok_barang_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->kelompok_barang_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadKelompokBarang';
		$page['list'] 		= $this->gen_paging($page, true);
		//
		$data = array();
		$data['content'] = 'kelompok_barang/list';		
		$data['list'] = $this->kelompok_barang_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$kelompok_barang =  $this->kelompok_barang_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'kelompok_barang/input';
		$data['kelompok_barang'] = $kelompok_barang;
		$data['title'] = 'Input Kelompok Barang';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$idKelompokBarang		= (decode($this->input->post('idKelompokBarang'))?:0);
		$data['KodeKelompokBarang'] = $this->input->post('KodeKelompokBarang');
		$data['NamaKelompokBarang']	= $this->input->post('NamaKelompokBarang');
		
		if ($data['KodeKelompokBarang'] == trim($data['KodeKelompokBarang']) && strpos($data['KodeKelompokBarang'], ' ') !== false) {
			$this->error('Kode Kelompok Barang Tidak Boleh Ada Spasi');
		}
		
		if ($idKelompokBarang)
		{	
			$data['idKelompokBarang'] 	= $idKelompokBarang;
		}else
		{
			/* $this->db->select('tbl."idSupplier"');
			$this->db->order_by('idSupplier','desc');
			$res = $this->db->get('dataMaster.msSupBenang tbl',1)->row(); */
			
			$data['idKelompokBarang'] = 0;	
		}
		
		//validasi data kosong
		/* $data['KodeSupplier'] = $this->validation_input('kode_kelompok_barang', 1, 10);*/
		$this->validation_input('KodeKelompokBarang'); 
		
		if(!$idKelompokBarang) {
			$kelompok_barang =  $this->kelompok_barang_model->get(array('KodeKelompokBarang' => $data['KodeKelompokBarang']));
			
			if($kelompok_barang['KodeKelompokBarang']) {
				if($data['KodeKelompokBarang']==$kelompok_barang['KodeKelompokBarang']){
					$this->error('Kode Jenis kendaraan sudah ada');
				}
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->kelompok_barang_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idKelompokBarang'] = encode($data['idKelompokBarang']);
			$this->update['status'] = $idKelompokBarang ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}

	/* function cek_detail() {
		$this->load->model('pembelian/po_benang_model');
		
		$fidSupplier = decode($this->input->post('fidSupplier'));
		$po_benang = $this->po_benang_model->get(array('fidSupplier' => $fidSupplier));		
		
		if($po_benang['fidSupplier']) {
			$message = 'Kode Supplier tidak bisa dihapus, karena masih ada di Order Benang. silahkan hapus dulu Order Benang untuk Kode Suppliier ini
						di Transaksi Order Benang.';
			$this->error($message);
		} 
		
		$this->success('data kode Supplier sudah kosong, tidak ada di transaksi order benang');		
	} */
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->kelompok_barang_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->kelompok_barang_model->set_limit($limit);
		$this->kelompok_barang_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("KodeKelompokBarang") like \'%'.$lookupkey.'%\'
				or upper("NamaKelompokBarang") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->kelompok_barang_model->set_order(array('KodeSupplier' => 'ASC'));
		$this->kelompok_barang_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->kelompok_barang_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataJenisKendaraan';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->kelompok_barang_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'kelompok_barang/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_kelompok_barang() {
		$code = trim(strtoupper($this->input->post('code')));
		$kelompok_barang = $this->kelompok_barang_model->get(array('upper("tbl"."KodeKelompokBarang")' => $code));
		$kelompok_barang['idKelompokBarang'] = encode($kelompok_barang['idKelompokBarang']);
		echo json_encode($kelompok_barang);		
    }
}