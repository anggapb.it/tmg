<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('barang_model');
		$this->load->model('kelompok_barang_model');
		$this->load->model('satuan_model');
		$this->load->model('supplier_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'barang/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."KodeBarang") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaBarang") like \'%'.$filter['key'].'%\' 
				)'] = null;
		}

		$this->barang_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->barang_model->set_order(array('KodeBarang' => 'ASC'));
		//
		$this->barang_model->set_limit($limit);
		$this->barang_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->barang_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadBarang';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'barang/list';		
		$data['list'] = $this->barang_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$barang =  $this->barang_model->get($id);		
		
		//
		$data = array();
		$data['kelompok_barang'] = $this->kelompok_barang_model->get_list();
		$data['satuan'] 	= $this->satuan_model->get_list();
		$data['supplier']	= $this->supplier_model->get_list();
		$data['content'] 	= 'barang/input';
		$data['barang'] 	= $barang;
		$data['title'] 		= 'Input Barang';
		$this->load->view($data['content'],$data);
	}
	
	function change_pict($id=0)
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		/* $config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = 100; */
		$_this->load->library('image_lib',$config);
		
		$date = $id.date('YmdHis');
		$structure  = 'files/barang/original';
		$structure2  = 'files/barang/thumbnails_29x29';
		$structure3  = 'files/barang/thumbnails_45x45';
		$structure4  = 'files/barang/thumbnails_90x90';
		$structure5  = 'files/barang/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['foto'];
		
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);

		if($file_ext != 'jpeg' && $file_ext != 'jpg' && $file_ext != 'gif' && $file_ext != 'png') {
			$this->error('Format photo harus jpg/png');
		}
		
		$upload_image = $structure.'/'.$date.'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{			
			// $img_size = getimagesize($upload_image);	
		  
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"maintain_ratio"      => false,
					"source_image"  => $upload_image,
					"new_image"     => './files/barang/'.$r['name'].'/'.$date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize()) {             
					$this->error($_this->image_lib->display_errors());
				}
				$_this->image_lib->clear(); 
				if($r['width'].'x'.$r['height'] == '90x90') {
					$barang = $this->barang_model->get(decode($id));
					
					if($barang['PhotoBarang']) {
						if($barang['PhotoBarang'] != $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext) {
							$photo = str_replace('-90x90', '', $barang['PhotoBarang']);
							$check_ext = explode(".", $photo);
							$check_file_ext = end($check_ext);
							
							$structure  = 'files/barang/original/'.$photo;
							$structure2  = 'files/barang/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
							$structure3  = 'files/barang/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
							$structure4  = 'files/barang/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
							$structure5  = 'files/barang/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
							
							if(file_exists($structure))
								unlink($structure);
						
							if(file_exists($structure2))
								unlink($structure2);
							
							if(file_exists($structure3))
								unlink($structure3);
							
							if(file_exists($structure4))
								unlink($structure4);
							
							if(file_exists($structure5))
								unlink($structure5);
						}
					}
					
					$fileName = $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext;
					$update = array();
					$update['KodeBarang'] 	= decode($id);
					$update['PhotoBarang'] 	= $fileName;
					$this->barang_model->save($update);
				}
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$this->error('Foto gagal disimpan');
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
	}
	
	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
		$data = array();
		$this->db->trans_start();
		$KodeBarang		 		= (decode($this->input->post('KodeBarang'))?:0);
		$data['fidKelompokBarang']	= $this->input->post('fidKelompokBarang')?:0;
		$data['NamaBarang']		= $this->input->post('NamaBarang');
		$data['fidSatuanKecil']	= $this->input->post('fidSatuanKecil')?:0;
		$data['fidSatuanBesar']	= $this->input->post('fidSatuanBesar')?:0;
		$data['HargaBeli']		= $this->input->post('HargaBeli');
		$data['fidSupplier']	= $this->input->post('fidSupplier')?:0;
		$data['Deskripsi']		= $this->input->post('Deskripsi');
		$data['Merk']			= $this->input->post('Merk');
		$data['Seri']			= $this->input->post('Seri');
			
		if ($KodeBarang)
		{	
			$data['KodeBarang'] = $KodeBarang;
			$data['TglUpdate'] 	= date('Y-m-d');
			$data['userupdate'] = $this->session->userdata('Operator')['LoginName'];
		}else
		{
			$data['TglInput'] 	= date('Y-m-d');
			$data['userinput'] 	= $this->session->userdata('Operator')['LoginName'];
			
			$KodeBarang = $this->barang_model->getNewTrans($data['TglInput']);			
			$data['KodeBarang'] = $KodeBarang;	
		}
		
		if(!$data['NamaBarang']) {
			$this->update['CallBack'] = 'NamaBarang';
			$this->error('Nama Barang Harus diisi');
		}
		
		if(!$data['fidSupplier']) {
			$this->update['CallBack'] = 'fidSupplier';
			$this->error('Supplier Harus diisi');
		}
		//validasi data kosong
		$this->validation_input('NamaBarang');
		
		if(!$KodeBarang) {
			$barang =  $this->barang_model->get(array('NamaBarang' => $data['NamaBarang']));
			if($data['NamaBarang']==$barang['NamaBarang']){
				$this->error('Nama Barang sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->barang_model->save($data);
				
		if($KodeBarang && $_FILES['foto']['name']) {
			$this->change_pict(encode($KodeBarang));
		}
		
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['KodeBarang'] = encode($KodeBarang);
			$this->update['status'] = $KodeBarang ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$barang = $this->barang_model->get($Code);
		$this->barang_model->delete($Code);
		
		if($barang['PhotoBarang']) {
			$photo = str_replace('-90x90', '', $barang['PhotoBarang']);
			$check_ext = explode(".", $photo);
			$check_file_ext = end($check_ext);
			
			$structure  = 'files/barang/original/'.$photo;
			$structure2  = 'files/barang/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
			$structure3  = 'files/barang/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
			$structure4  = 'files/barang/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
			$structure5  = 'files/barang/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey 	= strtoupper($this->input->post('lookup_key'));
		$no_temp 	= $this->input->post('no_temp');
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->barang_model->set_limit($limit);
		$this->barang_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("KodeBarang") like \'%'.$lookupkey.'%\' OR
					upper("NamaBarang") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->barang_model->set_where($where);
		$this->barang_model->set_order(array('KodeBarang' => 'ASC'));
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->barang_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataBarang';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->barang_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'barang/list_lookup'
			,'paging'			=> 	$page
			,'no_temp'			=> 	$no_temp
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_barang() {
		$KodeBarang = trim($this->input->post('code'));
		$barang = $this->barang_model->get(array('KodeBarang' => $KodeBarang));
		// $barang['KodeBarang'] = encode($barang['KodeBarang']);
		echo json_encode($barang);		
    }
}