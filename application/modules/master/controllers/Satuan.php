<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Satuan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('satuan_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'satuan/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaSatuan") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->satuan_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->satuan_model->set_order(array('NamaSatuan' => 'ASC'));
		//
		$this->satuan_model->set_limit($limit);
		$this->satuan_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->satuan_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadSatuan';
		$page['list'] 		= $this->gen_paging($page, true);
		//
		$data = array();
		$data['content'] = 'satuan/list';		
		$data['list'] = $this->satuan_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$satuan =  $this->satuan_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'satuan/input';
		$data['satuan'] = $satuan;
		$data['title'] = 'Input Satuan';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$idSatuan		 	= (decode($this->input->post('idSatuan'))?:0);
		$data['NamaSatuan']	= $this->input->post('NamaSatuan');
		
		
		/* if ($data['KodeSatuan'] == trim($data['KodeSatuan']) && strpos($data['KodeSatuan'], ' ') !== false) {
			$this->error('Kode Satuan Tidak Boleh Ada Spasi');
		} */
		
		if ($idSatuan)
		{	
			$data['idSatuan'] 	= $idSatuan;
		}else
		{
			/* $this->db->select('tbl."idSatuan"');
			$this->db->order_by('idSatuan','desc');
			$res = $this->db->get('dataMaster.msSupBenang tbl',1)->row(); */
			
			$data['idSatuan'] = 0;	
		}
		
		//validasi data kosong
		/* $data['KodeSatuan'] = $this->validation_input('kode_satuan', 1, 10);*/
		$this->validation_input('NamaSatuan'); 
		
		if(!$idSatuan) {
			$satuan =  $this->satuan_model->get(array('NamaSatuan' => $data['NamaSatuan']));
			
			if($satuan['NamaSatuan']) {
				if($data['NamaSatuan']==$satuan['NamaSatuan']){
					$this->error('Nama Satuan sudah ada');
				}
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->satuan_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idSatuan'] = encode($data['idSatuan']);
			$this->update['status'] = $idSatuan ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}

	/* function cek_detail() {
		$this->load->model('pembelian/po_benang_model');
		
		$fidSatuan = decode($this->input->post('fidSatuan'));
		$po_benang = $this->po_benang_model->get(array('fidSatuan' => $fidSatuan));		
		
		if($po_benang['fidSatuan']) {
			$message = 'Kode Satuan tidak bisa dihapus, karena masih ada di Order Benang. silahkan hapus dulu Order Benang untuk Kode Suppliier ini
						di Transaksi Order Benang.';
			$this->error($message);
		} 
		
		$this->success('data kode Satuan sudah kosong, tidak ada di transaksi order benang');		
	} */
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->satuan_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->satuan_model->set_limit($limit);
		$this->satuan_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					 upper("NamaSatuan") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->satuan_model->set_order(array('NamaSatuan' => 'ASC'));
		$this->satuan_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->satuan_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataSatuan';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->satuan_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'satuan/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_satuan() {
		$code = trim(strtoupper($this->input->post('code')));
		$satuan = $this->satuan_model->get(array('upper("tbl"."NamaSatuan")' => $code));
		$satuan['idSatuan'] = encode($satuan['idSatuan']);
		echo json_encode($satuan);		
    }
}