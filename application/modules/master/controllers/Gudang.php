<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gudang extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('gudang_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'gudang/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."KodeGudang") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaGudang") like \'%'.$filter['key'].'%\' OR
					upper(tbl."AlamatGudang") like \'%'.$filter['key'].'%\' OR
					upper(tbl."KotaGudang") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		$where['tbl."idGudang" < 100000'] = null;
		$this->gudang_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->gudang_model->set_order(array('KodeGudang' => 'ASC'));
		//
		$this->gudang_model->set_limit($limit);
		$this->gudang_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->gudang_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadGudang';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'gudang/list';		
		$data['list'] = $this->gudang_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$gudang =  $this->gudang_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'gudang/input';
		$data['gudang'] = $gudang;
		$data['title'] = 'Input Gudang';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$id_gudang		 	= (decode($this->input->post('id_gudang'))?:0);
		$data['KodeGudang'] = $this->input->post('kode_gudang');
		$data['NamaGudang']	= $this->input->post('nama_gudang');
		$data['AlamatGudang']	= $this->input->post('alamat_gudang');
		$data['KotaGudang']	= $this->input->post('kota_gudang');
		
		if ($data['KodeGudang'] == trim($data['KodeGudang']) && strpos($data['KodeGudang'], ' ') !== false) {
			$this->error('Kode Gudang Tidak Boleh Ada Spasi');
		}
		
		if ($id_gudang)
		{	
			$data['idGudang'] 	= $id_gudang;
		}else
		{
			$this->db->select('tbl."idGudang"');
			$this->db->order_by('idGudang','desc');
			$this->db->where('tbl."idGudang" < 100000');
			$res = $this->db->get('dataMaster.msGudang tbl',1)->row();
			
			$data['idGudang'] = (!isset($res->idGudang) ? 1 : $res->idGudang+1);	
		}
		
		//validasi data kosong
		$data['KodeGudang'] = $this->validation_input('kode_gudang', 1, 6);
		$data['NamaGudang'] = $this->validation_input('nama_gudang');
		
		if(!$id_gudang) {
			$gudang =  $this->gudang_model->get(array('KodeGudang' => $data['KodeGudang']));
			if($data['KodeGudang']==$gudang['KodeGudang']){
				$this->error('Kode Gudang sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->gudang_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idGudang'] = encode($data['idGudang']);
			$this->update['status'] = $id_gudang ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
	
	function cekValidasiDelete() {
		$idGudang = decode($this->input->post('idGudang'));
		$cekValidasi = $this->gudang_model->cekValidasiDelete($idGudang);
		
		if($cekValidasi['udf_ValidasiDelGudang'] == 0) {
			$this->error('Data tidak bisa dihapus');
		} else {
			$this->success('Data bisa dihapus');
		}
	}
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->gudang_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->gudang_model->set_limit($limit);
		$this->gudang_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("KodeGudang") like \'%'.$lookupkey.'%\'
				or upper("NamaGudang") like \'%'.$lookupkey.'%\'
				or upper("AlamatGudang") like \'%'.$lookupkey.'%\'
				or upper("KotaGudang") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		
		$where['tbl."idGudang" < 100000'] = null;
		
		$this->gudang_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->gudang_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataGudang';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->gudang_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'gudang/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_gudang() {
		$code = trim($this->input->post('code'));
		$gudang = $this->gudang_model->get(array('KodeGudang' => $code));
		$gudang['idGudang'] = encode($gudang['idGudang']);
		echo json_encode($gudang);		
    }
}