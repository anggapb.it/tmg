<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('customer_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'customer/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoHP") like \'%'.$filter['key'].'%\' OR
					upper(tbl."Alamat") like \'%'.$filter['key'].'%\' OR
					upper(tbl."KotaKabupaten") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->customer_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->customer_model->set_order(array('NamaLengkap' => 'ASC'));
		//
		$this->customer_model->set_limit($limit);
		$this->customer_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->customer_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadCustomer';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'customer/list';		
		$data['list'] = $this->customer_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$customer =  $this->customer_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'customer/input';
		$data['customer'] = $customer;
		$data['title'] = 'Input Customer';
		$this->load->view($data['content'],$data);
	}
	
	function change_pict($id=0)
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		/* $config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = 100; */
		$_this->load->library('image_lib',$config);
		
		$date = $id.date('YmdHis');
		$structure  = 'files/customer/original';
		$structure2  = 'files/customer/thumbnails_29x29';
		$structure3  = 'files/customer/thumbnails_45x45';
		$structure4  = 'files/customer/thumbnails_90x90';
		$structure5  = 'files/customer/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['foto'];
		
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);

		if($file_ext != 'jpeg' && $file_ext != 'jpg' && $file_ext != 'gif' && $file_ext != 'png') {
			$this->error('Format photo harus jpg/png');
		}
		
		$upload_image = $structure.'/'.$date.'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{			
			$img_size = getimagesize($upload_image);	
		
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"source_image"  => $upload_image,
					"new_image"     => './files/customer/'.$r['name'].'/'.$date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize())                    
					$this->error($_this->image_lib->display_errors());
				$_this->image_lib->clear(); 
				
				if($r['width'].'x'.$r['height'] == '90x90') {
					$customer = $this->customer_model->get(decode($id));
					
					if($customer['Photo']) {
						if($customer['Photo'] != $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext) {
							$photo = str_replace('-90x90', '', $customer['Photo']);
							$check_ext = explode(".", $photo);
							$check_file_ext = end($check_ext);
							
							$structure  = 'files/customer/original/'.$photo;
							$structure2  = 'files/customer/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
							$structure3  = 'files/customer/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
							$structure4  = 'files/customer/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
							$structure5  = 'files/customer/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
							
							if(file_exists($structure))
								unlink($structure);
						
							if(file_exists($structure2))
								unlink($structure2);
							
							if(file_exists($structure3))
								unlink($structure3);
							
							if(file_exists($structure4))
								unlink($structure4);
							
							if(file_exists($structure5))
								unlink($structure5);
						}
					}
					
					$fileName = $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext;
					$update = array();
					$update['idCustomer'] 	= decode($id);
					$update['Photo'] 		= $fileName;
					$this->customer_model->save($update);
				}
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$this->error('Foto gagal disimpan');
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
	}
	
	function save()
	{	
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
		
		$data = array();
		$this->db->trans_start();
		$idCustomer		 	= (decode($this->input->post('idCustomer'))?:0);
		$data['NamaLengkap']	= $this->input->post('NamaLengkap');
		$data['NamaPanggilan']	= $this->input->post('NamaPanggilan');
		$data['NoHP']			= $this->input->post('NoHP');
		$data['Alamat']			= $this->input->post('Alamat');
		$data['Email']			= $this->input->post('Email');
		$data['Institusi']		= $this->input->post('Institusi');
		$data['Negara']			= $this->input->post('Negara');
		$data['Provinsi']		= $this->input->post('Provinsi');
		
		$data['KotaKabupaten']	= $this->input->post('KotaKabupaten');
		// $data['Photo']			= $this->input->post('Photo');
				
		if ($idCustomer)
		{	
			$data['idCustomer'] 	= $idCustomer;
		}else
		{
			$data['idCustomer'] = 0;	
		}
		
		if(!$data['NamaLengkap']) {
			$this->update['CallBack'] = 'NamaLengkap';
			$this->error('Nama Customer Harus diisi');
		}
		//validasi data kosong
		$this->validation_input('NamaLengkap');
		$this->validation_input('NoHP');
		$this->validation_input('Email');
		
		if(!$idCustomer) {
			$customer =  $this->customer_model->get(array('NamaLengkap' => $data['NamaLengkap']));
			if($data['NamaLengkap']==$customer['NamaLengkap']){
				$this->error('Nama Customer sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->customer_model->save($data);
		
		if(!$idCustomer)
		$idCustomer = $this->db->insert_id('"dataMaster"."msCustomer_idCustomer_seq"');
		
		if($idCustomer && $_FILES['foto']['name']) {
			$this->change_pict(encode($idCustomer));
		}
		
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idCustomer'] = encode($idCustomer);
			$this->update['status'] = $idCustomer ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$customer = $this->customer_model->get($Code);
		$this->customer_model->delete($Code);
		
		if($customer['Photo']) {
			$photo = str_replace('-90x90', '', $customer['Photo']);
			$check_ext = explode(".", $photo);
			$check_file_ext = end($check_ext);
			
			$structure  = 'files/customer/original/'.$photo;
			$structure2  = 'files/customer/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
			$structure3  = 'files/customer/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
			$structure4  = 'files/customer/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
			$structure5  = 'files/customer/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->customer_model->set_limit($limit);
		$this->customer_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("NamaLengkap") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->customer_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->customer_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataCustomer';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->customer_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'customer/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_customer() {
		$NamaLengkap = trim($this->input->post('NamaLengkap'));
		$customer = $this->customer_model->get(array('NamaLengkap' => $NamaLengkap));
		$customer['idCustomer'] = encode($customer['idCustomer']);
		echo json_encode($customer);		
    }
}