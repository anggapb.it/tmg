<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('supplier_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'supplier/manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."KodeSupplier") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaSupplier") like \'%'.$filter['key'].'%\' OR
					upper(tbl."AlamatSupplier") like \'%'.$filter['key'].'%\' OR
					upper(tbl."KotaSupplier") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->supplier_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->supplier_model->set_order(array('KodeSupplier' => 'ASC'));
		//
		$this->supplier_model->set_limit($limit);
		$this->supplier_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->supplier_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadSupplier';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'supplier/list';		
		$data['list'] = $this->supplier_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$supplier =  $this->supplier_model->get($id);		
		
		//
		$data = array();
		$data['content'] = 'supplier/input';
		$data['supplier'] = $supplier;
		$data['title'] = 'Input Supplier';
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$id_supplier		 	= (decode($this->input->post('id_supplier'))?:0);
		$data['KodeSupplier'] = $this->input->post('kode_supplier');
		$data['NamaSupplier']	= $this->input->post('nama_supplier');
		$data['AlamatSupplier']	= $this->input->post('alamat_supplier');
		$data['Alamat2Supplier']	= $this->input->post('Alamat2Supplier');
		$data['Alamat3Supplier']	= $this->input->post('Alamat3Supplier');
		$data['KotaSupplier']	= $this->input->post('kota_supplier');
		$data['ToleransiOrderDone']	= ($this->input->post('ToleransiOrderDone') ? $this->input->post('ToleransiOrderDone') : 0);
		$data['AttnOrder']		= $this->input->post('AttnOrder');
		$data['Telepon']		= $this->input->post('Telepon');
		$data['Telepon2']		= $this->input->post('Telepon2');
		$data['Telepon3']		= $this->input->post('Telepon3');
		$data['Fax']			= $this->input->post('Fax');
		$data['Fax2']			= $this->input->post('Fax2');
		$data['Email']			= $this->input->post('Email');
		
		if ($data['KodeSupplier'] == trim($data['KodeSupplier']) && strpos($data['KodeSupplier'], ' ') !== false) {
			$this->error('Kode Supplier Tidak Boleh Ada Spasi');
		}
		
		if ($id_supplier)
		{	
			$data['idSupplier'] 	= $id_supplier;
		}else
		{
			/* $this->db->select('tbl."idSupplier"');
			$this->db->order_by('idSupplier','desc');
			$res = $this->db->get('dataMaster.msSupBenang tbl',1)->row(); */
			
			$data['idSupplier'] = 0;	
		}
		
		//validasi data kosong
		$this->validation_input('kode_supplier', 1, 10);
		$this->validation_input('nama_supplier');
		
		if(!$id_supplier) {
			$supplier =  $this->supplier_model->get(array('KodeSupplier' => $data['KodeSupplier']));
			if($data['KodeSupplier']==$supplier['KodeSupplier']){
				$this->error('Kode Supplier sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->supplier_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idSupplier'] = encode($data['idSupplier']);
			$this->update['status'] = $id_supplier ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}

	/* function cek_detail() {
		$this->load->model('pembelian/po_benang_model');
		
		$fidSupplier = decode($this->input->post('fidSupplier'));
		$po_benang = $this->po_benang_model->get(array('fidSupplier' => $fidSupplier));		
		
		if($po_benang['fidSupplier']) {
			$message = 'Kode Supplier tidak bisa dihapus, karena masih ada di Order Benang. silahkan hapus dulu Order Benang untuk Kode Suppliier ini
						di Transaksi Order Benang.';
			$this->error($message);
		} 
		
		$this->success('data kode Supplier sudah kosong, tidak ada di transaksi order benang');		
	} */
	
	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->supplier_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->supplier_model->set_limit($limit);
		$this->supplier_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper("KodeSupplier") like \'%'.$lookupkey.'%\'
				or upper("NamaSupplier") like \'%'.$lookupkey.'%\'
				or upper("AlamatSupplier") like \'%'.$lookupkey.'%\'
				or upper("KotaSupplier") like \'%'.$lookupkey.'%\'
				)'] = null;
		}
		$this->supplier_model->set_order(array('KodeSupplier' => 'ASC'));
		$this->supplier_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->supplier_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataSupplier';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->supplier_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'supplier/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_supplier() {
		$code = trim(strtoupper($this->input->post('code')));
		$supplier = $this->supplier_model->get(array('upper("tbl"."KodeSupplier")' => $code));
		$supplier['idSupplier'] = encode($supplier['idSupplier']);
		echo json_encode($supplier);		
    }
}