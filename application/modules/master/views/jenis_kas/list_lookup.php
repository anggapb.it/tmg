<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Nama</th>
				<th>Keterangan</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectJenisKas('<?=$row['Nama']?>')" style="cursor:pointer;" title="Klik disini <?=$row['Nama']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['Nama'],$key)?></td>
						<td><?= match_key($row['Keterangan'],$key)?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>