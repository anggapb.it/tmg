<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function save()
	{
		showProgres();
		$('#load').button('loading');
		$.post(site_url+'master/gudang/save'
			,$('#gudang_input_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					$('#id_gudang').val(result.idGudang);
					
					if(result.status == 'insert') {
						clear_text();
					}
					
					setTimeout(function () {
						$('#load').button('reset');
						show_gudang_list();
					}, 2000);
					pageLoadGudang(1);
				}else{
					// clean error sign
					$("#gudang_input_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					toastr.error(result.error,'Error');
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
				}
			}					
			,"json"
		);
	}
	
	function clear_text() {	
		$('#id_gudang').val("'<?php echo encode(0) ?>'");
		$('#kode_gudang').val('');
		$('#nama_gudang').val('');
		$('#alamat_gudang').val('');
		$('#kota_gudang').val('');
	}	
	
	function nospaces(){
		var hasSpace = $('#kode_gudang').val().indexOf(' ')>=0;
		
		if(hasSpace) {
			toastr.error("Kode Gudang Tidak Boleh Ada Spasi",'Error');
			// $('#kode_supplier').focus();
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_gudang_list()"> Gudang</a></li>
		<?php if ($gudang['idGudang']) {?>
		<li><a href="#" onclick="gudang_input('<?= encode($gudang['idGudang'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $gudang['TypeCode'] = '';
// $gudang['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="gudang_input_form">
			<input name="id_gudang" hidden value="<?= encode($gudang['idGudang'])?>">
			<!--input type="hidden" class="form-control" id="type_code_current" name="t_type_code_current" placeholder="name" value="<?= $gudang['KodeType']?>"-->
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">
							<label>Kode Gudang</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="kode_gudang" name="kode_gudang" onblur="nospaces()" placeholder="" value="<?= $gudang['KodeGudang']?>" maxlength="6">
							</div>
						</div>
						<div class="form-group">
							<label>Nama Gudang</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="nama_gudang" name="nama_gudang" placeholder="" value="<?= $gudang['NamaGudang']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Alamat Gudang</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="alamat_gudang" name="alamat_gudang" placeholder="" value="<?= $gudang['AlamatGudang']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Kota Gudang</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="kota_gudang" name="kota_gudang" placeholder="" value="<?= $gudang['KotaGudang']?>">
							</div>
						</div>							
					</div>	
				</div>						
			</div><!-- /.box-body -->
		</form>
		<div class="box-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right" onclick="save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>
			<a href="javascript:void(0);" class="btn btn-default" onclick="clear_text()">Reset</a>
			<a href="javascript:void(0);" class="btn btn-warning" onclick="show_gudang_list();">Close</a>
		</div>
	</div>
</section>