<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Kode Gudang</th>
				<th>Nama Gudang</th>
				<th>Alamat Gudang</th>
				<th>Kota Gudang</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectGudang('<?=$row['KodeGudang']?>')" style="cursor:pointer;" title="Klik disini <?=$row['KodeGudang']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['KodeGudang'],$key)?></td>
						<td><?= match_key($row['NamaGudang'],$key)?></td>
						<td><?= match_key($row['AlamatGudang'],$key)?></td>
						<td><?= match_key($row['KotaGudang'],$key)?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>