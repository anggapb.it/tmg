<script type="text/javascript">
	$(function () {
		pageLoadGudang(1);
	});		
	
	function pageLoadGudang(pg)
	{	
		showProgres();
		$.post(site_url+'master/gudang/page/'+pg
			,{t_search_key : $('#search_key').val()}
			,function(result) {
				$('#resultContent').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_gudang_list()
	{
		$('#gudang_list_container').show();
		$('#gudang_input_container').hide();
		
	}
	function show_filter()
	{
		var tp = $('#gudang_effect').val();
		if(tp==1){
			$( "#gudang_toggle_filter" ).toggle( "blind","down" );
			// $('#gudang_effect').val(2);
		}else{
			$( "#gudang_toggle_filter" ).toggle( "blind","down" );
			// $('#gudang_effect').val(1);
		}
		
	}

	function gudang_input(id)
	{
		$('#gudang_list_container').hide();
		$('#gudang_input_container').show();
		//
		showProgres();
		$.post(site_url+'master/gudang/input/'+id
			,{}
			,function(result) {
				$('#gudang_input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}

	function delete_data(code){
		showProgres();
		$.post(site_url+'master/gudang/cekValidasiDelete',
				{idGudang : code},
				function(result) {
					if(result.error){
						toastr.error(result.error,'Error');
						hideProgres();
					}else{
						var options = {
							title: 'Warning',
							message: "Anda yakin akan menghapus Data ?"
						};
						eModal.confirm(options).then(function callback(){
						  //jika OK
							// showProgres();
							$.post(site_url+'master/gudang/delete'
									,{t_Code : code}
									,function(result) {
										// hideProgres();
										pageLoadGudang(1);
										if(result.error){
											toastr.error(result.error,'Error');
											hideProgres();
										}else{
											toastr.success(result.message,'Save');
											hideProgres();
										}
									}					
									,"json"
								);
						},    function callbackCancel(){
						  //JIKA CANCEL
						});
					}
				},
			"json"
			);
	}
</script>
<div id="gudang_list_container">
	<section class="content-header">
		<h1>
			Data Gudang
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Gudang</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="col-sm-4">
								<div class="btn-group">
									<button type="button" class="btn btn-warning" onclick="gudang_input()">New</button>
								</div>
							</div>
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="gudang_toggle_filter" hidden>
							<hr>
							<div class="form-inline">
								<div class='col-sm-1'>
								</div>
								<div class='col-sm-7'>
									<div class="form-group">
										<label>Search</label>
										<input type="text" class="form-control" id="search_key" name="t_search_key" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) pageLoadGudang(1)">
									</div>
									<button type="button" class="btn btn-primary pull-right" onClick="pageLoadGudang(1)">Browse</button>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<table class="table table-hover" id="gudang_list">
								<div id="resultContent"></div>
							</table>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="gudang_input_container" hidden>
	Loading...
</div>