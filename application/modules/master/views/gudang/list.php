<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No </th>
			<th>Kode Gudang</th>
			<th>Nama Gudang</th>
			<th>Alamat Gudang</th>
			<th>Kota Gudang</th>
			<th>*</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?>
			<tr>
				<td><?= $no ?></td>
				<td><?= match_key($row['KodeGudang'],$key['key'])?></td>
				<td><?= match_key($row['NamaGudang'],$key['key'])?></td>
				<td><?= match_key($row['AlamatGudang'],$key['key'])?></td>
				<td><?= match_key($row['KotaGudang'],$key['key'])?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="gudang_input('<?= encode($row['idGudang'])?>')">Edit</a></li>
							<li><a href="#" onClick="delete_data('<?= encode($row['idGudang']) ?>')">Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>