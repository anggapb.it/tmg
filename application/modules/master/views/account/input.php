<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function save()
	{
		showProgres();
		$('#load').button('loading');
		$.post(site_url+'master/account/save'
			,$('#input_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					$('#idAccount').val(result.idAccount);
					
					if(result.status == 'insert') {
						clear_text();
					}
					
					setTimeout(function () {
						$('#load').button('reset');
						show_list();
					}, 2000);
				}else{
					// clean error sign
					$("#input_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					toastr.error(result.error,'Error');
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
				}
			}					
			,"json"
		);
	}
	
	function clear_text() {	
		$('#idAccount').val("'<?php echo encode(0) ?>'");
		$('#NamaAccount').val('');
	}

	/* function nospaces(){
		var hasSpace = $('#kode_account').val().indexOf(' ')>=0;
		
		if(hasSpace) {
			toastr.error("Kode Supplier Tidak Boleh Ada Spasi",'Error');
			// $('#kode_account').focus();
		}
	} */
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Supplier</a></li>
		<?php if ($account['idAccount']) {?>
		<li><a href="#" onclick="account_input('<?= encode($account['idAccount'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<section class="content" >
	<div class="box box-default">
		<form id="input_form">
			<input name="idAccount" hidden value="<?= encode($account['idAccount'])?>">
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Nama TTD</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaTTD" name="NamaTTD" placeholder="" value="<?= $account['NamaTTD']?>">
							</div>
						</div>						
					</div>						
				</div>						
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Insial Bank</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="InisialBank" name="InisialBank" placeholder="" value="<?= $account['InisialBank']?>">
							</div>
						</div>						
					</div>						
				</div>						
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Nama Bank</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Bank" name="Bank" placeholder="" value="<?= $account['Bank']?>">
							</div>
						</div>						
					</div>						
				</div>						
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Nama Rekening</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaRekening" name="NamaRekening" placeholder="" value="<?= $account['NamaRekening']?>">
							</div>
						</div>						
					</div>						
				</div>						
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>No Rekening</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoRek" name="NoRek" placeholder="" value="<?= $account['NoRek']?>">
							</div>
						</div>						
					</div>						
				</div>						
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" id="Status" name="Status" <?php echo ($account['Status'] ? "checked" : "") ?>> Aktif
							</label>
						</div>
					</div>						
				</div>						
			</div><!-- /.box-body -->
		</form>
		<div class="box-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right" onclick="save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>							
			<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
			<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>	
		</div>
	</div>
</section>