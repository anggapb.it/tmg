<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>Nama TTD</th>
			<th>Inisial Bank</th>
			<th>Nama Bank</th>
			<th>Nama Rekening</th>
			<th>No Rekening</th>
			<th class="text-center">Status</th>
			<th>*</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = ($paging['current']-1) * $paging['limit'] ;
		foreach($list->result_array() as $row)
		{
			$no++;
			
		?>
		<tr>
			<td><?= $no?></td>
			<td>
				<input type='hidden' id='idAccount<?php echo $no ?>' value='<?php echo encode($row['idAccount']) ?>'>
				<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
			</td>
			<td><?= match_key($row['NamaTTD'],$key['key'])?></td>
			<td><?= match_key($row['InisialBank'],$key['key'])?></td>
			<td><?= match_key($row['Bank'],$key['key'])?></td>
			<td><?= match_key($row['NamaRekening'],$key['key'])?></td>
			<td><?= match_key($row['NoRek'],$key['key'])?></td>
			<td class="text-center"><?= $row['Status']? '<i class="fa fa-check"></i>' : '<i class="fa fa-remove"></i>' ?></td>
			<td>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
					<ul class="dropdown-menu">
						<li><a href="#" onClick="input('<?= encode($row['idAccount'])?>')">Edit</a></li>
						<li><a href="#" onClick="delete_data('<?= encode($row['idAccount']) ?>')">Delete</a></li>
					</ul>
				</div>
			</td>
		</tr>
<?php }?>
	</tbody>
</table>
<?=$paging['list']?>