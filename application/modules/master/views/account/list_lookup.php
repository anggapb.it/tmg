<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>Nama TTD</th>
			<th>Nama Bank</th>
			<th>No Rekening</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = ($paging['current']-1) * $paging['limit'] ;
		foreach($list->result_array() as $row)
		{
			$no++;
			
		?>
		<tr>
			<td><?= $no?></td>
			<td><?= match_key($row['NamaTTD'],$key['key'])?></td>
			<td><?= match_key($row['Bank'],$key['key'])?></td>
			<td><?= match_key($row['NoRek'],$key['key'])?></td>
			<td><?= $row['Status']? '<i class="fa fa-check"></i>' : '<i class="fa fa-remove"></i>' ?></td>
		</tr>
<?php }?>
	</tbody>
</table>
<?=$paging['list']?>