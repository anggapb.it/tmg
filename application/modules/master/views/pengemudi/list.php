<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>NIK</th>
			<th>Nama </th>
			<th>Alamat </th>
			<th>No HP</th>
			<th>Status</th>
			<th>*</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
				$status = '';
				if($row['Status'] == 10) 
					$status = 'Available';
				elseif($row['Status'] == 20) 
					$status = 'Not Available';
				elseif($row['Status'] == 30) 
					$status = 'On The Way';
			?>
			<tr>
				<td><?= $no ?></td>
				<td>
					<input type='hidden' id='idPengemudi<?php echo $no ?>' value='<?php echo encode($row['idPengemudi']) ?>'>
					<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
				</td>
				<td><?= match_key($row['NIK'],$key['key'])?></td>
				<td><?= match_key($row['NamaLengkap'],$key['key'])?></td>
				<td><?= match_key($row['Alamat'],$key['key'])?></td>
				<td><?= match_key($row['NoHP'],$key['key'])?></td>
				<td><?= $status ?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="input('<?= encode($row['idPengemudi'])?>')">Edit</a></li>
							<li><a href="#" onClick="delete_data('<?= encode($row['idPengemudi']) ?>')">Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>