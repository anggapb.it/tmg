<script type="text/javascript">
$(document).ready(function() {
	objDate('TglLahir');
	// objDate('tglakhirefektif');
});
	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
				
		if($('#PhotoProfile')[0].files[0] !== undefined) {
			if(!($('#PhotoProfile')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoKTP')[0].files[0] !== undefined) {
			if(!($('#PhotoKTP')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoSIM_A')[0].files[0] !== undefined) {
			if(!($('#PhotoSIM_A')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoSIM_B1')[0].files[0] !== undefined) {
			if(!($('#PhotoSIM_B1')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoSIM_B2')[0].files[0] !== undefined) {
			if(!($('#PhotoSIM_B2')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
	
		showProgres();
		$('#load').button('loading');
		
		$.ajax({
			type: "POST",
			url: site_url+'master/pengemudi/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#idPengemudi').val(result.idPengemudi);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.idPengemudi);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input, type) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview'+type).prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Customer</a></li>
		<?php if ($pengemudi['idPengemudi']) {?>
		<li><a href="#" onclick="pengemudi_input('<?= encode($pengemudi['idPengemudi'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $pengemudi['TypeCode'] = '';
// $pengemudi['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="input_form"  method="post" enctype="multipart/form-data">
			<input name="idPengemudi" hidden value="<?= encode($pengemudi['idPengemudi'])?>">
			<div class="box-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>NIK</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NIK" name="NIK" placeholder="" value="<?= $pengemudi['NIK']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Nama Lengkap</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaLengkap" name="NamaLengkap" maxlength=50 placeholder="" value="<?= $pengemudi['NamaLengkap']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Nama Panggilan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaPanggilan" name="NamaPanggilan" maxlength=50 placeholder="" value="<?= $pengemudi['NamaPanggilan']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>No HP</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="NoHP" name="NoHP" placeholder="" maxlength=20 onkeypress='numberOnly(event)' value="<?= $pengemudi['NoHP']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Bank</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="Bank" name="Bank" placeholder="" value="<?= $pengemudi['Bank']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>No. Rekening</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="NoRekening" name="NoRekening" placeholder="" maxlength=50 onkeypress='numberOnly(event)' value="<?= $pengemudi['NoRekening']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Email</label>
							<div class="input-group col-md-12"> 
								<input type="email" class="form-control" id="Email" name="Email" placeholder="" value="<?= $pengemudi['Email']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Alamat</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="Alamat" name="Alamat" cols="20" rows="4"><?= $pengemudi['Alamat']?></textarea>
							</div>
						</div>
					</div>
					<div class='col-md-4'>
						<div class="form-group">
							<label>Tgl Lahir</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TglLahir" name="TglLahir" placeholder="" value="<?= $pengemudi['TglLahir']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Gol. Darah</label>
							<select class="form-control select2" style="width: 100%;" id="GolDarah" name="GolDarah">
								<option value="">Pilih</option>
								<option value="A" <?= ($pengemudi['GolDarah'] == 'A' ?'selected="selected"':'')?>>A</option>
								<option value="B" <?= ($pengemudi['GolDarah'] == 'B' ?'selected="selected"':'')?>>B</option>
								<option value="AB" <?= ($pengemudi['GolDarah'] == 'AB' ?'selected="selected"':'')?>>AB</option>
								<option value="O" <?= ($pengemudi['GolDarah'] == 'O' ?'selected="selected"':'')?>>O</option>
							</select>
						</div>	
						<div class="form-group">
							<label>Agama</label>
							<select class="form-control select2" style="width: 100%;" id="fidMsReligion" name="fidMsReligion">
							<option value="">Pilih</option>
							<?php foreach($religion->result_array() as $row){ ?>
								<option value="<?= $row['idMsReligion']?>" <?= ($row['idMsReligion']==$pengemudi['fidMsReligion']?'selected="selected"':'')?>><?= $row['Description']?></option>
							<?php }?>
							</select>
						</div>		
						<div class="form-group">
							<label>Pengemudi</label>
							<select class="form-control select2" style="width: 100%;" id="Status" name="Status">
								<option value="10" <?php echo $pengemudi['Status'] == '10' ? 'selected' : '' ?>>Available</option>
								<option value="20" <?php echo $pengemudi['Status'] == '20' ? 'selected' : '' ?>>Not Available</option>
								<option value="30" <?php echo $pengemudi['Status'] == '30' ? 'selected' : '' ?>>On The Way</option>
							</select>
						</div>
						<div class='form-group'>
							<label>Foto Profil</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewProfile' width=90 height=90 src='<?php echo get_pict($pengemudi['PhotoProfile'], '90x90', 'pengemudi') ?>' class='img-thumbnail img-responsive' alt='Photo Profile'>
								</div>
								<input type='file' id='PhotoProfile' name='PhotoProfile' onchange='readUrl(this, "Profile")'>
							</div>
						</div>
						<div class='form-group'>
							<label>Foto KTP</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewKTP' width=90 height=90 src='<?php echo get_pict($pengemudi['PhotoKTP'], '90x90', 'pengemudi') ?>' class='img-thumbnail img-responsive' alt='Photo KTP'>
								</div>
								<input type='file' id='PhotoKTP' name='PhotoKTP' onchange='readUrl(this, "KTP")'>
							</div>
						</div>
					</div>	
					<div class='col-md-4'>
						<div class='form-group'>
							<div class="checkbox">
								<label>
								  <input type="checkbox" id="PunyaSIM_A" name="PunyaSIM_A" <?php echo $pengemudi['PunyaSIM_A'] ? 'checked' : '' ?>> Punya SIM A
								</label>
							 </div>
						 </div>
						 <div class='form-group'>
							<label>Foto SIM A</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewSIM_A' width=90 height=90 src='<?php echo get_pict($pengemudi['PhotoSIM_A'], '90x90', 'pengemudi') ?>' class='img-thumbnail img-responsive' alt='Photo SIM A'>
								</div>
								<input type='file' id='PhotoSIM_A' name='PhotoSIM_A' onchange='readUrl(this, "SIM_A")'>
							</div>
						</div>
						 <div class='form-group'>
							<div class="checkbox">
								<label>
								  <input type="checkbox" id="PunyaSIM_B1" name="PunyaSIM_B1" <?php echo $pengemudi['PunyaSIM_B1'] ? 'checked' : '' ?>> Punya SIM B1
								</label>
							 </div>
						 </div>
						 <div class='form-group'>
							<label>Foto SIM B1</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewSIM_B1' width=90 height=90 src='<?php echo get_pict($pengemudi['PhotoSIM_B1'], '90x90', 'pengemudi') ?>' class='img-thumbnail img-responsive' alt='Photo SIM B1'>
								</div>
								<input type='file' id='PhotoSIM_B1' name='PhotoSIM_B1' onchange='readUrl(this, "SIM_B1")'>
							</div>
						</div>
						 <div class='form-group'>
							<div class="checkbox">
								<label>
								  <input type="checkbox" id="PunyaSIM_B2" name="PunyaSIM_B2" <?php echo $pengemudi['PunyaSIM_B2'] ? 'checked' : '' ?>> Punya SIM B2
								</label>
							 </div>
						 </div>
						 <div class='form-group'>
							<label>Foto SIM B2</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewSIM_B2' width=90 height=90 src='<?php echo get_pict($pengemudi['PhotoSIM_B2'], '90x90', 'pengemudi') ?>' class='img-thumbnail img-responsive' alt='Photo SIM B2'>
								</div>
								<input type='file' id='PhotoSIM_B2' name='PhotoSIM_B2' onchange='readUrl(this, "SIM_B2")'>
							</div>
						</div>
					</div>
				</div>						
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</form>
	</div>
</section>