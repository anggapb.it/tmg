<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Nama Customer</th>
				<th>Alamat Customer</th>
				<th>Kota Customer</th>
				<th>No. Tlp</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectCustomer('<?=$row['NamaCustomer']?>')" style="cursor:pointer;" title="Klik disini <?=$row['NamaCustomer']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['NamaCustomer'],$key['key'])?></td>
						<td><?= match_key($row['Alamat1'],$key['key'])?></td>
						<td><?= match_key($row['Kota'],$key['key'])?></td>
						<td><?= match_key($row['NoHP'],$key['key'])?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>