<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
		
		if($('#foto')[0].files[0] !== undefined) {
			if(!($('#foto')[0].files[0].size < 2097152)) { 
				// 10 MB (this size is in bytes)
				//Prevent default and display error
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
	
		showProgres();
		$('#load').button('loading');
		
		$.ajax({
			type: "POST",
			url: site_url+'master/customer/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#idCustomer').val(result.idCustomer);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.idCustomer);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview').prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Customer</a></li>
		<?php if ($customer['idCustomer']) {?>
		<li><a href="#" onclick="customer_input('<?= encode($customer['idCustomer'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $customer['TypeCode'] = '';
// $customer['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="input_form"  method="post" enctype="multipart/form-data">
			<input name="idCustomer" hidden value="<?= encode($customer['idCustomer'])?>">
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">
							<label>Nama Customer</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaLengkap" name="NamaLengkap" placeholder="" value="<?= $customer['NamaLengkap']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kontak Person</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaPanggilan" name="NamaPanggilan" placeholder="" value="<?= $customer['NamaPanggilan']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>No. Tlp</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="NoHP" name="NoHP" placeholder="" value="<?= $customer['NoHP']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Email</label>
							<div class="input-group col-md-12"> 
								<input type="email" class="form-control" id="Email" name="Email" placeholder="" value="<?= $customer['Email']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Jenis Usaha</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Institusi" name="Institusi" placeholder="" value="<?= $customer['Institusi']?>">
							</div>
						</div>	
					</div>
					<div class='col-md-4'>
						<div class="form-group">
							<label>Alamat</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="Alamat" name="Alamat" cols="20" rows="4"><?= $customer['Alamat']?></textarea>
							</div>
						</div>	
						<div class="form-group">
							<label>Kota / Kabupaten</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KotaKabupaten" name="KotaKabupaten" placeholder="" value="<?= $customer['KotaKabupaten']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Provinsi</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Provinsi" name="Provinsi" placeholder="" value="<?= $customer['Provinsi']?>">
							</div>
						</div>							
						<div class="form-group">
							<label>Negara</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Negara" name="Negara" placeholder="" value="<?= $customer['Negara']?>">
							</div>
						</div>	
						<div class='form-group'>
							<label>Foto</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='preview' width=90 height=90 src='<?php echo get_pict($customer['Photo'], '90x90', 'customer') ?>' class='img-thumbnail img-responsive' alt='Photo'>
								</div>
								<input type='file' id='foto' name='foto' onchange='readUrl(this)'>
							</div>
						</div>
						<!--div class='form-group'>
							<label>&nbsp;</label>
							<div class="input-group col-md-12"> 
								<input type='file' id='foto' name='foto' onchange='readUrl(this)'>
							</div>
						</div-->
					</div>						
				</div>						
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</form>
	</div>
</section>