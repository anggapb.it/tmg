<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<script type="text/javascript">
	$(function () {
		pageLoadSupplier(1);
	});		
	
	function pageLoadSupplier(pg)
	{	
		showProgres();
		$.post(site_url+'master/supplier/page/'+pg
			,{t_search_key : $('#search_key').val()}
			,function(result) {
				$('#resultContentSupplier').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_supplier_list()
	{
		$('#supplier_list_container').show();
		$('#supplier_input_container').hide();
		
	}
	function show_filter()
	{
		var tp = $('#supplier_effect').val();
		if(tp==1){
			$( "#supplier_toggle_filter" ).toggle( "blind","down" );
			// $('#supplier_effect').val(2);
		}else{
			$( "#supplier_toggle_filter" ).toggle( "blind","down" );
			// $('#supplier_effect').val(1);
		}
		
	}

	function supplier_input(id)
	{
		$('#supplier_list_container').hide();
		$('#supplier_input_container').show();
		//
		showProgres();
		$.post(site_url+'master/supplier/input/'+id
			,{}
			,function(result) {
				$('#supplier_input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}

	function delete_data(code){
		// showProgres();
		/* $.post(site_url+'master/supplier/cek_detail',
				{fidSupBenang : code},
				function(result) {
					if(result.error){
						toastr.error(result.error,'Error');
						hideProgres();
					}else{ */
						var options = {
							title: 'Warning',
							message: "Anda yakin akan menghapus Data ?"
						};
						eModal.confirm(options).then(function callback(){
						  //jika OK
							showProgres();
							$.post(site_url+'master/supplier/delete'
									,{t_Code : code}
									,function(result) {
										// hideProgres();
										pageLoadSupplier(1);
										if(result.error){
											toastr.error(result.error,'Error');
											hideProgres();
										}else{
											toastr.success(result.message,'Save');
											hideProgres();
										}
									}					
									,"json"
								);
						},    function callbackCancel(){
						  //JIKA CANCEL
						});
					/* }
				}
				,'json'
			); */
	}
	
	
	function check_all() {
        var table= $('#checked_all').closest('#list_table');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function input_check() {
		var searchIDs = $("#list_table input:checkbox:checked").not('#checked_all').map(function(){
						  return $(this).val();
						}).get();
		
		if(searchIDs.length == 0) {
			toastr.error('Silahkan Ceklist terlebih dahulu Supplier yang ingin diedit','Error');
			return false;
		}	
		
		if(searchIDs.length > 1) {
			toastr.error('Ceklist untuk edit tidak boleh lebih dari satu','Error');
			return false;
		}	
		
		$('#supplier_list_container').hide();
		$('#supplier_input_container').show();
		//
		showProgres();
		$.post(site_url+'master/supplier/input/'+$('#idSupplier'+searchIDs[0]).val()
			,{}
			,function(result) {
				$('#supplier_input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function delete_check() {
		var searchIDs = $("#list_table input:checkbox:checked").not('#checked_all').map(function(){
						  return $(this).val();
						}).get();
		
		if(searchIDs.length == 0) {
			toastr.error('Silahkan Ceklist terlebih dahulu Supplier yang ingin dihapus','Error');
			return false;
		}	
				
		var options = {
			title: 'Warning',
			message: "Anda yakin akan menghapus Data sebanyak "+searchIDs.length+" item ?"
		};
		eModal.confirm(options).then(function callback(){
		  //jika OK
			showProgres();
			$('#loadDelete').button('loading');
						
			for(var i=0;i<searchIDs.length;i++) {
				no = searchIDs[i];
				$.post(site_url+'master/supplier/delete'
						,{t_Code : $('#idSupplier'+no).val()}
						,function(result) {
							hideProgres();
							$('#loadDelete').button('reset');
							pageLoadSupplier(1);
							if(result.error){
								toastr.error(result.error,'Error');
								hideProgres();
							}else{
								toastr.success(result.message,'Save');
								hideProgres();
							}
						}					
						,"json"
					);
			}
		},    function callbackCancel(){
		  //JIKA CANCEL
		});
	}
</script>
<div id="supplier_list_container">
	<section class="content-header">
		<h1>
			Data Supplier
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Supplier</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="col-sm-4">
								<div class="btn-group">
									<button type="button" class="btn btn-primary btn-flat" onclick="supplier_input()">+ New</button>
								</div>&nbsp;&nbsp;
								<div class="btn-group">
									<button type="button" class="btn btn-warning btn-flat" onclick="input_check()"><i class='fa fa-pencil'></i> Edit</button>
									<button type="button" class="btn btn-danger btn-flat" onclick="delete_check()" id="loadDelete" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..."><i class='fa fa-trash-o'></i> Hapus</button>
								</div>
							</div>
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="supplier_toggle_filter" hidden>
							<hr>
							<div class="form-inline">
								<div class='col-sm-1'>
								</div>
								<div class='col-sm-7'>
									<div class="form-group">
										<label>Search</label>
										<input type="text" class="form-control" id="search_key" name="t_search_key" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) pageLoadSupplier(1)">
									</div>
									<button type="button" class="btn btn-primary pull-right" onClick="pageLoadSupplier(1)">Browse</button>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<table class="table table-hover" id="supplier_list">
								<div id="resultContentSupplier"></div>
							</table>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="supplier_input_container" hidden>
	Loading...
</div>