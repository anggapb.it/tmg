<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>Kode Supplier</th>
			<th>Nama Supplier</th>
			<th>Alamat Supplier</th>
			<th>Kota Supplier</th>
			<th>Telepon</th>
			<th>Fax</th>
			<th>*</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = ($paging['current']-1) * $paging['limit'] ;
		foreach($list->result_array() as $row)
		{
			$no++;
			
		?>
		<tr>
			<td><?= $no?></td>
			<td>
				<input type='hidden' id='idSupplier<?php echo $no ?>' value='<?php echo encode($row['idSupplier']) ?>'>
				<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
			</td>
			<td><?= match_key($row['KodeSupplier'],$key['key'])?></td>
			<td><?= match_key($row['NamaSupplier'],$key['key'])?></td>
			<td><?= match_key($row['AlamatSupplier'],$key['key'])?></td>
			<td><?= match_key($row['KotaSupplier'],$key['key'])?></td>
			<td><?= match_key($row['Telepon'],$key['key'])?></td>
			<td><?= match_key($row['Fax'],$key['key'])?></td>
			<td>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
					<ul class="dropdown-menu">
						<li><a href="#" onClick="supplier_input('<?= encode($row['idSupplier'])?>')">Edit</a></li>
						<li><a href="#" onClick="delete_data('<?= encode($row['idSupplier']) ?>')">Delete</a></li>
					</ul>
				</div>
			</td>
		</tr>
<?php }?>
	</tbody>
</table>
<?=$paging['list']?>