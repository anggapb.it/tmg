<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function save()
	{
		showProgres();
		$('#load').button('loading');
		$.post(site_url+'master/supplier/save'
			,$('#supplier_input_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					$('#id_supplier').val(result.idSupplier);
					
					if(result.status == 'insert') {
						clear_text();
					}
					
					setTimeout(function () {
						$('#load').button('reset');
						show_supplier_list();
					}, 2000);
					pageLoadSupplier(1)
				}else{
					// clean error sign
					$("#supplier_input_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					toastr.error(result.error,'Error');
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
				}
			}					
			,"json"
		);
	}
	
	function clear_text() {	
		$('#id_supplier').val("'<?php echo encode(0) ?>'");
		$('#kode_supplier').val('');
		$('#nama_supplier').val('');
		$('#alamat_supplier').val('');
		$('#Alamat2Supplier').val('');
		$('#Alamat3Supplier').val('');
		$('#Fax').val('');
		$('#Fax2').val('');
		$('#Telepon').val('');
		$('#Telepon2').val('');
		$('#Telepon3').val('');
		$('#Email').val('');
		// $('#AttnOrder').val('');
		$('#kota_supplier').val('');
		// $('#ToleransiOrderDone').val('');
	}

	function nospaces(){
		var hasSpace = $('#kode_supplier').val().indexOf(' ')>=0;
		
		if(hasSpace) {
			toastr.error("Kode Supplier Tidak Boleh Ada Spasi",'Error');
			// $('#kode_supplier').focus();
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_supplier_list()"> Supplier</a></li>
		<?php if ($supplier['idSupplier']) {?>
		<li><a href="#" onclick="supplier_input('<?= encode($supplier['idSupplier'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $supplier['TypeCode'] = '';
// $supplier['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="supplier_input_form">
			<input name="id_supplier" hidden value="<?= encode($supplier['idSupplier'])?>">
			<!--input type="hidden" class="form-control" id="type_code_current" name="t_type_code_current" placeholder="name" value="<?= $supplier['KodeType']?>"-->
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Kode Supplier</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="kode_supplier" name="kode_supplier" placeholder="" onblur="nospaces()" value="<?= $supplier['KodeSupplier']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Nama Supplier</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="nama_supplier" name="nama_supplier" placeholder="" value="<?= $supplier['NamaSupplier']?>">
							</div>
						</div>		
						<div class="form-group">								
							<label>Alamat Supplier</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="alamat_supplier" name="alamat_supplier" placeholder="" value="<?= $supplier['AlamatSupplier']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Alamat Supplier 2</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="Alamat2Supplier" name="Alamat2Supplier" placeholder="" value="<?= $supplier['Alamat2Supplier']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Alamat Supplier 3</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="Alamat3Supplier" name="Alamat3Supplier" placeholder="" value="<?= $supplier['Alamat3Supplier']?>">
							</div>
						</div>						
						<div class="form-group">								
							<label>Kota Supplier</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="kota_supplier" name="kota_supplier" placeholder="" value="<?= $supplier['KotaSupplier']?>">
							</div>
						</div>
					</div>	
					<div class="col-md-4">	
						<div class="form-group">								
							<label>Telepon</label>
							<div class="input-group col-md-12"> 
								<div class=" input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="number" min=0 class="form-control" id="Telepon" name="Telepon" placeholder="" value="<?= $supplier['Telepon']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Telepon 2</label>
							<div class="input-group col-md-12"> 
								<div class=" input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="number" min=0 class="form-control" id="Telepon2" name="Telepon2" placeholder="" value="<?= $supplier['Telepon2']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Telepon 3</label>
							<div class="input-group col-md-12"> 
								<div class=" input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="number" min=0 class="form-control" id="Telepon3" name="Telepon3" placeholder="" value="<?= $supplier['Telepon3']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Fax</label>
							<div class="input-group col-md-12"> 
								<input type="number" min=0 class="form-control" id="Fax" name="Fax" placeholder="" value="<?= $supplier['Fax']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Fax 2</label>
							<div class="input-group col-md-12"> 
								<input type="number" min=0 class="form-control" id="Fax2" name="Fax2" placeholder="" value="<?= $supplier['Fax2']?>">
							</div>
						</div>
						<div class="form-group">								
							<label>Email</label>
							<div class="input-group col-md-12"> 
								<input type="email" min=0 class="form-control" id="Email" name="Email" placeholder="" value="<?= $supplier['Email']?>">
							</div>
						</div>
						<!--div class="form-group">								
							<label>Attn Order</label>
							<div class="input-group col-md-12"> 
								<input type="text" min=0 class="form-control" id="AttnOrder" name="AttnOrder" placeholder="" value="<?= $supplier['AttnOrder']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Toleransi Order Done</label>
							<div class="input-group col-md-12">
								<input type="number" class="form-control" id="ToleransiOrderDone" name="ToleransiOrderDone" min=0 placeholder="" value="<?= $supplier['ToleransiOrderDone']?>">
							</div>
						</div-->					
					</div>						
				</div>						
			</div><!-- /.box-body -->
		</form>
		<div class="box-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right" onclick="save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>							
			<a href="javascript:void(0);" class="btn btn-default" onclick="clear_text()">Reset</a>
			<a href="javascript:void(0);" class="btn btn-warning" onclick="show_supplier_list();">Close</a>	
		</div>
	</div>
</section>