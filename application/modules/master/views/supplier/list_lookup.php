<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Kode Supplier</th>
				<th>Nama Supplier</th>
				<th>Alamat Supplier</th>
				<th>Kota Supplier</th>
				<th>Telepon</th>
				<th>Fax</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectSupplier('<?=$row['KodeSupplier']?>')" style="cursor:pointer;" title="Klik disini <?=$row['KodeSupplier']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['KodeSupplier'],$key)?></td>
						<td><?= match_key($row['NamaSupplier'],$key)?></td>
						<td><?= match_key($row['AlamatSupplier'],$key)?></td>
						<td><?= match_key($row['KotaSupplier'],$key)?></td>
						<td><?= match_key($row['Telepon'],$key)?></td>
						<td><?= match_key($row['Fax'],$key)?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>