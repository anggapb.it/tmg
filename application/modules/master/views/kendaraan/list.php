<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>Nama Kendaraan</th>
			<th>Plat Nomor</th>
			<th>Merk</th>
			<th>Type</th>
			<th>Jenis</th>
			<th>Dimensi</th>
			<th>Kondisi</th>
			<th>Tahun Pembuatan</th>
			<th>No. BPKB</th>
			<th>*</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
				if($row['Kondisi'] == 'Sangat Baik')
					$colour = 'green';
				elseif($row['Kondisi'] == 'Baik')
					$colour = 'blue';
				elseif($row['Kondisi'] == 'Kurang Baik')
					$colour = 'red';
			?>
			<tr style="color : <?php echo $colour ?>">
				<td><?= $no ?></td>
				<td>
					<input type='hidden' id='idKendaraan<?php echo $no ?>' value='<?php echo encode($row['idKendaraan']) ?>'>
					<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
				</td>
				<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
				<td><?= match_key($row['PlatNomor'],$key['key'])?></td>
				<td><?= match_key($row['Merk'],$key['key'])?></td>
				<td><?= match_key($row['Type'],$key['key'])?></td>
				<td><?= match_key($row['NamaJenisKendaraan'],$key['key'])?></td>
				<td><?= match_key($row['Dimensi'],$key['key'])?></td>
				<td><?= match_key($row['Kondisi'],$key['key'])?></td>
				<td><?= match_key($row['TahunPembuatan'],$key['key'])?></td>
				<td><?= match_key($row['NoBPKB'],$key['key'])?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="input('<?= encode($row['idKendaraan'])?>')">Edit</a></li>
							<li><a href="#" onClick="delete_data('<?= encode($row['idKendaraan']) ?>')">Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>