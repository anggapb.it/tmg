<script type="text/javascript">
$(document).ready(function() {
	// objDate('NoBPKB');
	// objDate('tglakhirefektif');
});
	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
		
		if($('#PhotoKendaraan1')[0].files[0] !== undefined) {
			if(!($('#PhotoKendaraan1')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoKendaraan2')[0].files[0] !== undefined) {
			if(!($('#PhotoKendaraan2')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		if($('#PhotoKendaraan3')[0].files[0] !== undefined) {
			if(!($('#PhotoKendaraan3')[0].files[0].size < 2097152)) { 
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
		
		showProgres();
		$('#load').button('loading');
		
		$.ajax({
			type: "POST",
			url: site_url+'master/kendaraan/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#idKendaraan').val(result.idKendaraan);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.idKendaraan);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input, type) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview'+type).prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Customer</a></li>
		<?php if ($kendaraan['idKendaraan']) {?>
		<li><a href="#" onclick="kendaraan_input('<?= encode($kendaraan['idKendaraan'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $kendaraan['TypeCode'] = '';
// $kendaraan['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="input_form"  method="post" enctype="multipart/form-data">
			<input name="idKendaraan" hidden value="<?= encode($kendaraan['idKendaraan'])?>">
			<div class="box-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Nama Kendaraan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaKendaraan" name="NamaKendaraan" placeholder="" value="<?= $kendaraan['NamaKendaraan']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Plat Nomor</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="PlatNomor" name="PlatNomor" maxlength=20 placeholder="" value="<?= $kendaraan['PlatNomor']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Merk</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Merk" name="Merk" maxlength=50 placeholder="" value="<?= $kendaraan['Merk']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Type</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="Type" name="Type" placeholder="" maxlength=50 value="<?= $kendaraan['Type']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Jenis Kendaraan</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidJenisKendaraan" name="fidJenisKendaraan">
									<?php if($jenis_kendaraan->num_rows() > 0) {
											foreach($jenis_kendaraan->result_array() as $row) { ?>
												<option value='<?php echo $row['idJenisKendaraan'] ?>' <?php echo $row['idJenisKendaraan'] == $kendaraan['fidJenisKendaraan'] ? 'selected' : '' ?>><?php echo $row['NamaJenisKendaraan'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
						<div class="form-group">
							<label>Model</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Model" name="Model" placeholder="" maxlength=50 value="<?= $kendaraan['Model']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Bahan Bakar</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="BahanBakar" name="BahanBakar" placeholder="" maxlength=50 value="<?= $kendaraan['BahanBakar']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Warna TNKB</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="WarnaTNKB" name="WarnaTNKB" placeholder="" maxlength=50 value="<?= $kendaraan['WarnaTNKB']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Tahun Registrasi</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TahunRegistrasi" name="TahunRegistrasi" maxlength=4 onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['TahunRegistrasi']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Kapasitas Muatan (Kg)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KapasitasMuatan" name="KapasitasMuatan" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['KapasitasMuatan']?>">
							</div>
						</div>		
					</div>
					<div class='col-md-3'>
						<div class="form-group">
							<label>No BPKB</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoBPKB" name="NoBPKB" maxlength=50 placeholder="" value="<?= $kendaraan['NoBPKB']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Tahun Pembuatan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TahunPembuatan" name="TahunPembuatan" maxlength=4 onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['TahunPembuatan']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Silinder</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Silinder" name="Silinder" maxlength=50 placeholder="" value="<?= $kendaraan['Silinder']?>">
							</div>
						</div>
						<div class="form-group">
							<label>No Rangka</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoRangka" name="NoRangka" maxlength=50 placeholder="" value="<?= $kendaraan['NoRangka']?>">
							</div>
						</div>
						<div class="form-group">
							<label>No Mesin</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoMesin" name="NoMesin" maxlength=50 placeholder="" value="<?= $kendaraan['NoMesin']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Warna</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Warna" name="Warna" maxlength=50 placeholder="" value="<?= $kendaraan['Warna']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Panjang Karoseri (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="PanjangKaroseri" name="PanjangKaroseri" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['PanjangKaroseri']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Lebar Karoseri (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="LebarKaroseri" name="LebarKaroseri" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['LebarKaroseri']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Tinggi Karoseri (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TinggiKaroseri" name="TinggiKaroseri" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['TinggiKaroseri']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Dimensi</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Dimensi" name="Dimensi" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['Dimensi']?>">
							</div>
						</div>
					</div>
					<div class='col-md-3'>	
						<div class="form-group">
							<label>Panjang Mobil (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="PanjangMobil" name="PanjangMobil" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['PanjangMobil']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Lebar Mobil (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="LebarMobil" name="LebarMobil" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['LebarMobil']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Tinggi Mobil (cm)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TinggiMobil" name="TinggiMobil" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['TinggiMobil']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Berat Kosong (Kg)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="BeratKosong" name="BeratKosong" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['BeratKosong']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Berat Max (Kg)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="BeratMax" name="BeratMax" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['BeratMax']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Ukuran Ban</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="UkuranBan" name="UkuranBan" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['UkuranBan']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Ukuran Roda</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="UkuranRoda" name="UkuranRoda" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['UkuranRoda']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kecepatan Max</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KecepatanMax" name="KecepatanMax" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['KecepatanMax']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Tenaga Max</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TenagaMax" name="TenagaMax" onkeypress='numberOnly(event)' placeholder="" value="<?= $kendaraan['TenagaMax']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kondisi</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="Kondisi" name="Kondisi">
									<option value='Sangat Baik' <?php echo $kendaraan['Kondisi'] == 'Sangat Baik' ? 'selected' : '' ?>>Sangat Baik</option>
									<option value='Baik' <?php echo $kendaraan['Kondisi'] == 'Baik' ? 'selected' : '' ?>>Baik</option>
									<option value='Kurang Baik' <?php echo $kendaraan['Kondisi'] == 'Kurang Baik' ? 'selected' : '' ?>>Kurang Baik</option>
								</select>
							</div>
						</div>
					</div>	
					<div class='col-md-3'>
						 <div class='form-group'>
							<label>Photo Kendaraan 1</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewKendaraan1' width=90 height=90 src='<?php echo get_pict($kendaraan['PhotoKendaraan1'], '90x90', 'kendaraan') ?>' class='img-thumbnail img-responsive' alt='Photo Kendaraan 1'>
								</div>
								<input type='file' id='PhotoKendaraan1' name='PhotoKendaraan1' onchange='readUrl(this, "Kendaraan1")'>
							</div>
						</div>
						 <div class='form-group'>
							<label>Photo Kendaraan 2</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewKendaraan2' width=90 height=90 src='<?php echo get_pict($kendaraan['PhotoKendaraan2'], '90x90', 'kendaraan') ?>' class='img-thumbnail img-responsive' alt='Photo Kendaraan 2'>
								</div>
								<input type='file' id='PhotoKendaraan2' name='PhotoKendaraan2' onchange='readUrl(this, "Kendaraan2")'>
							</div>
						</div>
						 <div class='form-group'>
							<label>Photo Kendaraan 3</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='previewKendaraan3' width=90 height=90 src='<?php echo get_pict($kendaraan['PhotoKendaraan3'], '90x90', 'kendaraan') ?>' class='img-thumbnail img-responsive' alt='Photo Kendaraan 3'>
								</div>
								<input type='file' id='PhotoKendaraan3' name='PhotoKendaraan3' onchange='readUrl(this, "Kendaraan3")'>
							</div>
						</div>
						<div class="form-group">
							<label>No Urut Daftar</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoUrutDaftar" name="NoUrutDaftar" maxlength=50 placeholder="" value="<?= $kendaraan['NoUrutDaftar']?>">
							</div>
						</div>
					</div>
				</div>						
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</form>
	</div>
</section>