<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function save()
	{
		showProgres();
		$('#load').button('loading');
		$.post(site_url+'master/jenis_kendaraan/save'
			,$('#input_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					$('#idJenisKendaraan').val(result.idJenisKendaraan);
					
					if(result.status == 'insert') {
						clear_text();
					}
					
					setTimeout(function () {
						$('#load').button('reset');
						show_list();
					}, 2000);
				}else{
					// clean error sign
					$("#input_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					toastr.error(result.error,'Error');
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
				}
			}					
			,"json"
		);
	}
	
	function clear_text() {	
		$('#idJenisKendaraan').val("'<?php echo encode(0) ?>'");
		$('#NamaJenisKendaraan').val('');
		$('#KodeJenisKendaraan').val('');
	}

	function nospaces(){
		var hasSpace = $('#KodeJenisKendaraan').val().indexOf(' ')>=0;
		
		if(hasSpace) {
			toastr.error("Kode Jenis Kendaraan Tidak Boleh Ada Spasi",'Error');
			// $('#kode_supplier').focus();
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Jenis Kendaraan</a></li>
		<?php if ($jenis_kendaraan['idJenisKendaraan']) {?>
		<li><a href="#" onclick="jenis_kendaraan_input('<?= encode($jenis_kendaraan['idJenisKendaraan'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<section class="content" >
	<div class="box box-default">
		<form id="input_form">
			<input name="idJenisKendaraan" hidden value="<?= encode($jenis_kendaraan['idJenisKendaraan'])?>">
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Kode Jenis Kendaraan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KodeJenisKendaraan" onblur="nospaces()" name="KodeJenisKendaraan" placeholder="" value="<?= $jenis_kendaraan['KodeJenisKendaraan']?>">
							</div>
						</div>						
					</div>						
				</div>
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">								
							<label>Nama Jenis Kendaraan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaJenisKendaraan" name="NamaJenisKendaraan" placeholder="" value="<?= $jenis_kendaraan['NamaJenisKendaraan']?>">
							</div>
						</div>						
					</div>						
				</div>						
			</div><!-- /.box-body -->
		</form>
		<div class="box-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right" onclick="save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>							
			<a href="javascript:void(0);" class="btn btn-default" onclick="clear_text()">Reset</a>
			<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>	
		</div>
	</div>
</section>