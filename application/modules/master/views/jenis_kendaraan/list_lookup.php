<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Kode Jenis Kendaraan</th>
				<th>Nama Jenis Kendaraan</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectSatuan('<?=$row['KodeJenisKendaraan']?>')" style="cursor:pointer;" title="Klik disini <?=$row['KodeJenisKendaraan']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['KodeJenisKendaraan'],$key['key'])?></td>
						<td><?= match_key($row['NamaJenisKendaraan'],$key['key'])?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>