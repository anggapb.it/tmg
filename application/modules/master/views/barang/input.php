<script type="text/javascript">
$(document).ready(function() {
	$('.select2').select2();
	
	<?php

	if($barang['KodeBarang']){
		echo "$('#lbl_trans_no2').html('<strong>$barang[KodeBarang]</strong>');";
	}else{
		echo "$('#lbl_trans_no2').html('<strong>-- AUTO GENERATE --</strong>');";
	}
	?>
});
	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
		
		if($('#foto')[0].files[0] !== undefined) {
			if(!($('#foto')[0].files[0].size < 2097152)) { 
				// 10 MB (this size is in bytes)
				//Prevent default and display error
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
	
		showProgres();
		$('#load').button('loading');
		
		$.ajax({
			type: "POST",
			url: site_url+'master/barang/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#KodeBarang').val(result.KodeBarang);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.KodeBarang);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview').prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Barang</a></li>
		<?php if ($barang['KodeBarang']) {?>
		<li><a href="#" onclick="barang_input('<?= encode($barang['KodeBarang'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $barang['TypeCode'] = '';
// $barang['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="input_form"  method="post" enctype="multipart/form-data">
			<input name="KodeBarang" hidden value="<?= encode($barang['KodeBarang'])?>">
			<div class="box-body">
				<div class="row" >
					<div class="col-md-4 pull-right">
						<div class="info-box bg-aqua">
							<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
							<div class="info-box-content text-center">
								<span class="info-box-number">Kode Barang</span>
								<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
								<span class="progress-description" style="font-size: 12pt;margin: 12px;" id="lbl_trans_no2">
								
								</span>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama Barang</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NamaBarang" name="NamaBarang" placeholder="" value="<?= $barang['NamaBarang']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Seri</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Seri" name="Seri" maxlength="50" placeholder="" value="<?= $barang['Seri']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kelompok Barang</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidKelompokBarang" name="fidKelompokBarang">
									<option value=''></option>
									<?php if($kelompok_barang->num_rows() > 0) {
											foreach($kelompok_barang->result_array() as $row) { ?>
												<option value='<?php echo $row['idKelompokBarang'] ?>' <?php echo $row['idKelompokBarang'] == $barang['fidKelompokBarang'] ? 'selected' : '' ?>><?php echo $row['NamaKelompokBarang'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>		
						<div class="form-group">
							<label>Merk</label>
							<div class="input-group col-md-12">
								<input type="text" class="form-control" id="Merk" name="Merk" placeholder="" value="<?= $barang['Merk']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Satuan Kecil</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidSatuanKecil" name="fidSatuanKecil">
									<option value=''></option>
									<?php if($satuan->num_rows() > 0) {
											foreach($satuan->result_array() as $row) { ?>
												<option value='<?php echo $row['idSatuan'] ?>' <?php echo $row['idSatuan'] == $barang['fidSatuanKecil'] ? 'selected' : '' ?>><?php echo $row['NamaSatuan'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
						<div class="form-group">
							<label>Satuan Besar</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidSatuanBesar" name="fidSatuanBesar">
									<option value=''></option>
									<?php if($satuan->num_rows() > 0) {
											foreach($satuan->result_array() as $row) { ?>
												<option value='<?php echo $row['idSatuan'] ?>' <?php echo $row['idSatuan'] == $barang['fidSatuanBesar'] ? 'selected' : '' ?>><?php echo $row['NamaSatuan'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
					</div>
					<div class='col-md-4'>
						<div class="form-group">
							<label>Harga Beli</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="HargaBeli" name="HargaBeli" onkeypress='numberOnly(event)' placeholder="" value="<?= $barang['HargaBeli']?>">
							</div>
						</div>	
						<div class="form-group">
							<label>Supplier</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidSupplier" name="fidSupplier">
									<option value=''></option>
									<?php if($supplier->num_rows() > 0) {
											foreach($supplier->result_array() as $row) { ?>
												<option value='<?php echo $row['idSupplier'] ?>' <?php echo $row['idSupplier'] == $barang['fidSupplier'] ? 'selected' : '' ?>><?php echo $row['NamaSupplier'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
						<div class="form-group">
							<label>Deskripsi</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="Deskripsi" name="Deskripsi" cols='10' rows='4'><?= $barang['Deskripsi']?></textarea>
							</div>
						</div>	
						<div class='form-group'>
							<label>Foto</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='preview' width=90 height=90 src='<?php echo get_pict($barang['PhotoBarang'], '90x90', 'barang') ?>' class='img-thumbnail img-responsive' alt='Photo'>
								</div>
								<input type='file' id='foto' name='foto' onchange='readUrl(this)'>
							</div>
						</div>
					</div>						
				</div>						
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</form>
	</div>
</section>