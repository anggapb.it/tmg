<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Kode Barang </th>
				<th>Kelompok Barang </th>
				<th>Nama Barang </th>
				<th>Supplier</th>
				<th>Deskripsi</th>
				<th>*</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectBarang('<?=$row['KodeBarang']?>', <?php echo $no_temp ?>)" style="cursor:pointer;" title="Klik disini <?=$row['KodeBarang']?>">
						<td><?= $no ?></td>
						<td><?= match_key($row['KodeBarang'],$key)?></td>
						<td><?= $row['NamaKelompokBarang'] ?></td>
						<td><?= match_key($row['NamaBarang'],$key)?></td>
						<td><?= $row['NamaSupplier'] ?></td>
						<td><?= $row['Deskripsi'] ?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>