<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<script type="text/javascript">
	$(function () {
		pageLoadBarang(1);
	});		
	
	function pageLoadBarang(pg)
	{	
		showProgres();
		$.post(site_url+'master/barang/page/'+pg
			,{t_search_key : $('#search_key').val()}
			,function(result) {
				$('#resultContent').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_list(id)
	{
		$('#list_container').show();
		$('#input_container').hide();
		
		pageLoadBarang(1);
	}
	function show_filter()
	{
		$( "#toggle_filter" ).toggle( "blind","down" );	
	}

	function input(id)
	{
		$('#list_container').hide();
		$('#input_container').show();
		//
		showProgres();
		$.post(site_url+'master/barang/input/'+id
			,{}
			,function(result) {
				$('#input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}

	function delete_data(code){
		showProgres();
		var options = {
			title: 'Warning',
			message: "Anda yakin akan menghapus Data ?"
		};
		eModal.confirm(options).then(function callback(){
		  //jika OK
			// showProgres();
			$.post(site_url+'master/barang/delete'
					,{t_Code : code}
					,function(result) {
						// hideProgres();
						pageLoadBarang(1);
						if(result.error){
							toastr.error(result.error,'Error');
							hideProgres();
						}else{
							toastr.success(result.message,'Save');
							hideProgres();
						}
					}					
					,"json"
				);
		},    function callbackCancel(){
		  //JIKA CANCEL
		});
	}
	
	function check_all() {
        var table= $('#checked_all').closest('#list_table');
		if($('#checked_all').is(':checked')) {
			$('td input:checkbox',table).prop('checked',true);
		} else {
			$('td input:checkbox',table).prop('checked',false);
		}
	}
	
	function input_check() {
		var searchIDs = $("#list_table input:checkbox:checked").not('#checked_all').map(function(){
						  return $(this).val();
						}).get();
		
		if(searchIDs.length == 0) {
			toastr.error('Silahkan Ceklist terlebih dahulu Barang yang ingin diedit','Error');
			return false;
		}	
		
		if(searchIDs.length > 1) {
			toastr.error('Ceklist untuk edit tidak boleh lebih dari satu','Error');
			return false;
		}	
		
		$('#list_container').hide();
		$('#input_container').show();
		//
		showProgres();
		$.post(site_url+'master/barang/input/'+$('#KodeBarang'+searchIDs[0]).val()
			,{}
			,function(result) {
				$('#input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function delete_check() {
		var searchIDs = $("#list_table input:checkbox:checked").not('#checked_all').map(function(){
						  return $(this).val();
						}).get();
		
		if(searchIDs.length == 0) {
			toastr.error('Silahkan Ceklist terlebih dahulu Barang yang ingin dihapus','Error');
			return false;
		}	
				
		var options = {
			title: 'Warning',
			message: "Anda yakin akan menghapus Data sebanyak "+searchIDs.length+" item ?"
		};
		eModal.confirm(options).then(function callback(){
		  //jika OK
			showProgres();
			$('#loadDelete').button('loading');
						
			for(var i=0;i<searchIDs.length;i++) {
				no = searchIDs[i];
				$.post(site_url+'master/barang/delete'
						,{t_Code : $('#KodeBarang'+no).val()}
						,function(result) {
							hideProgres();
							$('#loadDelete').button('reset');
							pageLoadBarang(1);
							if(result.error){
								toastr.error(result.error,'Error');
								hideProgres();
							}else{
								toastr.success(result.message,'Save');
								hideProgres();
							}
						}					
						,"json"
					);
			}
		},    function callbackCancel(){
		  //JIKA CANCEL
		});
	}
</script>
<div id="list_container">
	<section class="content-header">
		<h1>
			Data Barang
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Barang</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="col-sm-4">
								<div class="btn-group">
									<button type="button" class="btn btn-primary btn-flat" onclick="input()">+ New</button>
								</div>&nbsp;&nbsp;
								<div class="btn-group">
									<button type="button" class="btn btn-warning btn-flat" onclick="input_check()"><i class='fa fa-pencil'></i> Edit</button>
									<button type="button" class="btn btn-danger btn-flat" onclick="delete_check()" id="loadDelete" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..."><i class='fa fa-trash-o'></i> Hapus</button>
								</div>
							</div>
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="toggle_filter" hidden>
							<hr>
							<div class="form-inline">
								<div class='col-sm-1'>
								</div>
								<div class='col-sm-7'>
									<div class="form-group">
										<label>Search</label>
										<input type="text" class="form-control" id="search_key" name="t_search_key" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) pageLoadBarang(1)">
									</div>
									<button type="button" class="btn btn-primary pull-right" onClick="pageLoadBarang(1)">Browse</button>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<div id="resultContent"></div>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="input_container" hidden>
	Loading...
</div>