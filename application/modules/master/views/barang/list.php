<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>Kode Barang </th>
			<th>Kelompok Barang </th>
			<th>Nama Barang </th>
			<th>Supplier</th>
			<th>Deskripsi</th>
			<th>*</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?>
			<tr>
				<td><?= $no ?></td>
				<td>
					<input type='hidden' id='KodeBarang<?php echo $no ?>' value='<?php echo encode($row['KodeBarang']) ?>'>
					<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
				</td>
				<td><?= match_key($row['KodeBarang'],$key['key'])?></td>
				<td><?= $row['NamaKelompokBarang'] ?></td>
				<td><?= match_key($row['NamaBarang'],$key['key'])?></td>
				<td><?= $row['NamaSupplier'] ?></td>
				<td><?= $row['Deskripsi'] ?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="input('<?= encode($row['KodeBarang'])?>')">Edit</a></li>
							<li><a href="#" onClick="delete_data('<?= encode($row['KodeBarang']) ?>')">Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>