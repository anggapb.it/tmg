<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>Satuan</th>
			<th>*</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = ($paging['current']-1) * $paging['limit'] ;
		foreach($list->result_array() as $row)
		{
			$no++;
			
		?>
		<tr>
			<td><?= $no?></td>
			<td>
				<input type='hidden' id='idSatuan<?php echo $no ?>' value='<?php echo encode($row['idSatuan']) ?>'>
				<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
			</td>
			<td><?= match_key($row['NamaSatuan'],$key['key'])?></td>
			<td>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
					<ul class="dropdown-menu">
						<li><a href="#" onClick="input('<?= encode($row['idSatuan'])?>')">Edit</a></li>
						<li><a href="#" onClick="delete_data('<?= encode($row['idSatuan']) ?>')">Delete</a></li>
					</ul>
				</div>
			</td>
		</tr>
<?php }?>
	</tbody>
</table>
<?=$paging['list']?>