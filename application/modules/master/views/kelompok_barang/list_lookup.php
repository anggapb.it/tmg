<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Kode Kelompok Barang</th>
				<th>Nama Kelompok Barang</th>
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectSatuan('<?=$row['KodeKelompokBarang']?>')" style="cursor:pointer;" title="Klik disini <?=$row['KodeKelompokBarang']?>">
						<td><?=$no?></td>
						<td><?= match_key($row['KodeKelompokBarang'],$key['key'])?></td>
						<td><?= match_key($row['NamaKelompokBarang'],$key['key'])?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>