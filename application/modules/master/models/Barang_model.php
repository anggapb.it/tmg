<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('dataMaster');
		$this->set_table('msBarang');
		$this->set_pk('KodeBarang');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');
		
		$this->db->select('kel_brg.NamaKelompokBarang');
		$this->db->join('dataMaster.msKelompokBarang kel_brg', 'tbl.fidKelompokBarang = kel_brg.idKelompokBarang', 'left');
				
		$this->db->select('sup.NamaSupplier');
		$this->db->join('dataMaster.msSupplier sup', 'tbl.fidSupplier = sup.idSupplier', 'left');
		
		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->join('dataMaster.msKelompokBarang kel_brg', 'tbl.fidKelompokBarang = kel_brg.idKelompokBarang', 'left');
		$this->db->join('dataMaster.msSupplier sup', 'tbl.fidSupplier = sup.idSupplier', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}
	
	function getNewTrans($tanggal='')
	{
		$this->db->select('KodeBarang');
		$where = array();
		
		$tahun = date('Y', strtotime($tanggal));
		$bulan = date('m', strtotime($tanggal));
		
		if($tahun < date('Y')) {
			
		} else {
		   $tahun = date('Y');
		   $bulan = date('m');
		}
		
		$where['to_char("TglInput", \'YYYY\') ='] = $tahun;
		
		$this->db->where($where);
		$this->db->order_by('TglInput','desc');
		$this->db->order_by('KodeBarang','desc');
		$query = $this->db->get($this->schema.'.'.$this->table,1);
		$row = $query->row_array();
		//
		$recNo = '';
		$pref1 = 'B/';
		$pref2 = '/'.num2month($bulan,3).'/'.$tahun;
		if ($row['KodeBarang'])
		{
			$recNo =$pref1.substr(substr($row['KodeBarang'],2,4)+10001,1).$pref2;
			if(substr($row['KodeBarang'],2,4) == '9999'){
				$recNo = $pref1.'0001'.$pref2;
			}
		}else
		{
			$recNo = $pref1.'0001'.$pref2;;
		}
		
		return $recNo;
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
