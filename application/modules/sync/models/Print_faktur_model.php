<?php

class Print_faktur_model extends Base_Model {

    function __construct() {

        parent::__construct();
		$this->set_schema('toko');
		$this->set_table('trPrint');
		$this->set_pk('TanggalJam');
	}
	
    function count()
	{
		$this->db->select('count(*) as num_rows');
		if($this->where)
		$this->db->where($this->where);
		
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		
		// echo $this->db->last_query();
		// exit;
		$data = $query->row_array();
		return $data['num_rows'];
	}
    function get_list()
	{
		$this->db->select('tbl.*');
		if($this->where)
		$this->db->where($this->where);
		
		// foreach ($this->order_by as $key => $value)
		// {
			// $this->db->order_by($key, $value);
		// }
		// if(!$this->order){
			$this->db->order_by($this->pk_field,'ASC');
		// }

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		
		// echo $this->db->last_query();
		// exit;

		if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
}