<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('ho');
		$this->set_table('trOrder');
		$this->set_pk('NoOrder');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');
		
		$this->db->select('kenda.NamaKendaraan, kenda.PlatNomor,kenda."Merk", kenda."Dimensi"');
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
				
		$this->db->select('cus.NamaLengkap as "NamaCustomer"');
		$this->db->join('dataMaster.msCustomer cus', 'tbl.fidCustomer = cus.idCustomer', 'left');
		
		$this->db->select('mudi.NamaLengkap as "NamaPengemudi"');
		$this->db->join('dataMaster.msPengemudi mudi', 'tbl.fidPengemudi = mudi.idPengemudi', 'left');
		
		$this->db->select('sts.NamaStatusOrder, sts.Colour');
		$this->db->join('dataMaster.msStatusOrder sts', 'tbl.fidStatusOrder = sts.idStatusOrder', 'left');
		
		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msCustomer cus', 'tbl.fidCustomer = cus.idCustomer', 'left');
		$this->db->join('dataMaster.msPengemudi mudi', 'tbl.fidPengemudi = mudi.idPengemudi', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}
	
	function getNoInvoice($tanggal='')
	{
		$this->db->select('NoInvoice');
		$where = array();
		
		$tahun = date('Y', strtotime($tanggal));
		$bulan = date('m', strtotime($tanggal));
		
		if($tahun < date('Y')) {
			
		} else {
		   $tahun = date('Y');
		   $bulan = date('m');
		}
		
		$where['to_char("TglInput", \'YYYY\') ='] = $tahun;
		
		$this->db->where($where);
		$this->db->order_by('TglInput','desc');
		$this->db->order_by('NoInvoice','desc');
		$query = $this->db->get($this->schema.'.'.$this->table,1);
		$row = $query->row_array();
		//
		$recNo = '';
		$pref1 = '/TMG';
		$pref2 = '/'.num2month($bulan,3).'/'.$tahun;
		
		if ($row['NoInvoice'])
		{
			$recNo = substr(substr($row['NoInvoice'],0,4)+10001,1).$pref1.$pref2;
			if(substr($row['NoInvoice'],0,4) == '9999'){
				$recNo = '0001'.$pref1.$pref2;
			}
		}else
		{
			$recNo = '0001'.$pref1.$pref2;;
		}
		
		return $recNo;
	}
	
	function getNewTrans($tanggal='')
	{
		$this->db->select('NoOrder');
		$where = array();
		
		$tahun = date('Y', strtotime($tanggal));
		$bulan = date('m', strtotime($tanggal));
		
		if($tahun < date('Y')) {
			
		} else {
		   $tahun = date('Y');
		   $bulan = date('m');
		}
		
		$where['to_char("TglInput", \'YYYY\') ='] = $tahun;
		
		$this->db->where($where);
		$this->db->order_by('TglInput','desc');
		$this->db->order_by('NoOrder','desc');
		$query = $this->db->get($this->schema.'.'.$this->table,1);
		$row = $query->row_array();
		//
		$recNo = '';
		$pref1 = '/JOB';
		$pref2 = '/'.num2month($bulan,3).'/'.$tahun;
		if ($row['NoOrder'])
		{
			$recNo =substr(substr($row['NoOrder'],0,4)+10001,1).$pref1.$pref2;
			
			if(substr($row['NoOrder'],0,4) == '9999'){
				$recNo = '0001'.$pref1.$pref2;
			}
		}else
		{
			$recNo = '0001'.$pref1.$pref2;;
		}
		
		return $recNo;
	}

	function total_nominal_head()
	{
		// $this->db->select_sum('TotalHargaBayar', 'total_gross_all');
		$this->db->select_sum('TotalHargaBayar', 'total_net_all');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row;
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
