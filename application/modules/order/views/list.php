<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>No Order</th>
			<th>No Invoice</th>
			<th>Tgl Order</th>
			<th>Nama Kostumer </th>
			<th>Nama Kendaraan</th>
			<th>Pengemudi</th>
			<th>Total</th>
			<th>Status</th>
			<th>*</th>
			<th>Print</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?> 
			<tr style="color : <?php echo $row['Colour'] ?>">
				<td><?= $no ?></td>
				<td>
					<input type='hidden' id='NoOrder<?php echo $no ?>' value='<?php echo encode($row['NoOrder']) ?>'>
					<input type='hidden' id='fidStatusOrder<?php echo $no ?>' value='<?php echo $row['fidStatusOrder'] ?>'>
					<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
				</td>
				<td><?= match_key($row['NoOrder'],$key['key'])?></td>
				<td><?= match_key($row['NoInvoice'],$key['key'])?></td>
				<td><?= humanize_mdate($row['TglOrder'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NamaCustomer'],$key['key'])?></td>
				<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
				<td><?= $row['NamaPengemudi'] ?></td>
				<td><?= thausand_spar($row['TotalHargaBayar'])?></td>
				<td><?= $row['NamaStatusOrder'] ?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="input('<?= encode($row['NoOrder'])?>')">Edit</a></li>
							<?php if($row['fidStatusOrder'] == 10) { ?>
							<li><a href="#" onClick="delete_data('<?= encode($row['NoOrder']) ?>')">Delete</a></li>
							<?php } ?>
						</ul>
					</div>
				</td>
				<td>
					<button type="button" class="btn btn-warning" title="Preview Invoice" onClick="print('<?=$row['fidStatusOrder']>'10'?'0':'preview'?>','<?=encode($row['NoOrder'])?>')"><span class="fa <?=$row['fidStatusOrder']>'10'?'fa-print':'fa-search'?>" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-success" title="Preview Surat Jalan" onClick="print_surat_jalan('<?=$row['fidStatusOrder']>'10'?'0':'preview'?>','<?=encode($row['NoOrder'])?>')"><span class="fa <?=$row['fidStatusOrder']>'10'?'fa-print':'fa-search'?>" aria-hidden="true"></span></button>
				</td>
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>