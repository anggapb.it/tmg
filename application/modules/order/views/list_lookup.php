<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Nama Kostumer </th>
				<th>Nama Kendaraan</th>
				<th>Jenis Trip</th>
				<th>Pengemudi</th>
				<th>Status</th>
				<th>*</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectBarang('<?=$row['NamaBarang']?>')" style="cursor:pointer;" title="Klik disini <?=$row['NamaBarang']?>">
						<td><?= $no ?></td>
						<td><?= match_key($row['NamaCustomer'],$key['key'])?></td>
						<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
						<td><?= $row['JenisTrip'] ?></td>
						<td><?= $row['NamaPengemudi'] ?></td>
						<td><?= $row['NamaStatusOrder'] ?></td>>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>