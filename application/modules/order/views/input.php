<style type="text/css">
    body {
        color: #404E67;
        background: #F5F7FA;
		font-family: 'Open Sans', sans-serif;
	}
	.table-wrapper {
		width: 700px;
		margin: 30px auto;
        background: #fff;
        padding: 20px;	
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {
        padding-bottom: 10px;
        margin: 0 0 10px;
    }
    .table-title h2 {
        margin: 6px 0 0;
        font-size: 22px;
    }
    .table-title .add-new {
        float: right;
		height: 30px;
		font-weight: bold;
		font-size: 12px;
		text-shadow: none;
		min-width: 100px;
		border-radius: 50px;
		line-height: 13px;
    }
	.table-title .add-new i {
		margin-right: 4px;
	}
    #table_detail.table {
        table-layout: fixed;
    }
    #table_detail.table tr th, table.table tr td {
        border-color: #e9e9e9;
    }
    #table_detail.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }
    #table_detail.table th:last-child {
        width: 100px;
    }
    #table_detail.table td a {
		cursor: pointer;
        display: inline-block;
        margin: 0 5px;
		min-width: 24px;
    }    
	#table_detail.table td a.add {
        color: #27C46B;
    }
    #table_detail.table td a.edit {
        color: #FFC107;
    }
    #table_detail.table td a.delete {
        color: #E34724;
    }
    #table_detail.table td i {
        font-size: 19px;
    }
	#table_detail.table td a.add i {
        font-size: 24px;
    	margin-right: -1px;
        position: relative;
        top: 3px;
    }    
    #table_detail.table .form-control {
        height: 32px;
        line-height: 32px;
        box-shadow: none;
        border-radius: 2px;
    }
	#table_detail.table .form-control.error {
		border-color: #f50000;
	}
	#table_detail.table td .add {
		display: none;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('.select2').select2();
		
	objDate('TglInvoice');
	objDate('TglOrder');
	objDate('TglKirim');
	<?php
	
	if($order['Total']) {
		echo "$('#showTotal').show();";
		echo "$('#showPLT').hide();";
	}else {
		echo "$('#showTotal').hide();";
		echo "$('#showPLT').show();";
	}
	
	if($order['NoOrder']){
		echo "$('#lbl_trans_no2').html('<strong>$order[NoOrder]</strong>');";
	}else{
		echo "$('#lbl_trans_no2').html('<strong>-- AUTO GENERATE --</strong>');";
	}
	
	if($order['NoInvoice']){
		echo "$('#lbl_trans_no1').html('<strong>$order[NoInvoice]</strong>');";
	}else{
		echo "$('#lbl_trans_no1').html('<strong>-- AUTO GENERATE --</strong>');";
	}
	?>
	
	$('[data-toggle="tooltip"]').tooltip();
	// var actions = $("#table_detail td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
		// $(this).attr("disabled", "disabled");
		// var index = $("#table_detail tbody tr:last-child").index();
        var row = '<tr class="product">' +
            '<td><input type="text" class="form-control" name="NamaBiaya[]" id="NamaBiaya"></td>' +
            '<td><input type="text" class="form-control nominal_pemasukan" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="NominalPemasukan[]" id="NominalPemasukan"></td>' +
            '<td><input type="text" class="form-control nominal_pengeluaran" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="NominalPengeluaran[]" id="NominalPengeluaran"></td>' +
            '<td><input type="text" class="form-control ppn" onkeypress="numberOnlyDec(Event,this.value)" onblur="totalHarga()" name="PPN[]" id="PPN"></td>' +
            '<td><input type="text" class="form-control pph" onkeypress="numberOnlyDec(Event,this.value)" onblur="totalHarga()" name="PPH[]" id="PPH"></td>' +
            '<td><input type="text" class="form-control sub_total" readonly onblur="totalHarga()" name="SubTotal[]" id="SubTotal"></td>' +
			'<td><a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a></td>' +
        '</tr>';
    	$("#table_detail").append(row);		
		/* $("#table_detail tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip(); */
    });
	// Add row on add button click
	/* $('#table_detail').on("click", ".add", function(){
		var empty = false;
		var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });
	// Edit row on edit button click
	$('#table_detail').on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    }); */
	// Delete row on delete button click
	$('#table_detail').on("click", ".delete", function(){
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
		totalHarga();
    });
});

/* function get(){
  var table = $('#table_detail');
  var data = [];

  table.find('tr').each(function (i, el) {
    // no thead
    if( i != 0){
      var $tds = $(this).find('td');
      var row = [];
      $tds.each(function (i, el){
        row.push($(this).text());
      });
      data.push(row);
    }
        
  });
  return data;
} */

	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
		
		if($('#foto')[0].files[0] !== undefined) {
			if(!($('#foto')[0].files[0].size < 2097152)) { 
				// 10 MB (this size is in bytes)
				//Prevent default and display error
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
	
		showProgres();
		$('#load').button('loading');
		
		$.ajax({
			type: "POST",
			url: site_url+'order/manage/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#NoOrder').val(result.NoOrder);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.NoOrder);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview').prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
	
	function showCBM() {
		if($('#setCBM').is(':checked')) {
			$('#showPLT').hide();
			$('#showTotal').show();
		} else {
			$('#showPLT').show();
			$('#showTotal').hide();			
		}
		
		/* $('#Panjang').val('');
		$('#Lebar').val('');
		$('#Tinggi').val('');
		$('#Total').val(''); */
	}
	
	function totalHarga() {		
		var total_beli 	= 0;
		var ppn 		= 0;
		var pph 		= 0;
		var harga_ppn 	= 0;
		var harga_pph 	= 0;
		var sub_total 	= 0;
		var total_biaya	= 0;
		var total_ppn 	 = 0;
		var total_pph 	 = 0;
		var total_sub_total = 0;

		$('.nominal_pemasukan').each(function(index) {
			nominal_pemasukan    = $('.product:eq('+index+') .nominal_pemasukan').val();
			nominal_pengeluaran  = $('.product:eq('+index+') .nominal_pengeluaran').val();
			ppn 				 = Number($('.product:eq('+index+') .ppn').val());
			pph 				 = Number($('.product:eq('+index+') .pph').val());
			
			nominal_pemasukan = Number(nominal_pemasukan.replace(/[ ,.]/g, ""));
			nominal_pengeluaran = Number(nominal_pengeluaran.replace(/[ ,.]/g, ""));

			if(nominal_pemasukan) {
				total_beli = nominal_pemasukan;
			} else {
				total_beli = nominal_pengeluaran;
			}
			/* total_beli = Number(qty*harga_beli);
			harga_diskon = Number(total_beli*(diskon/100)); */
			harga_ppn = Number(total_beli*(ppn/100));
			harga_pph = Number(total_beli*(pph/100));
			sub_total = Number(total_beli+harga_ppn-harga_pph);

			$('.product:eq('+index+') .sub_total').val(sub_total);

			total_biaya 	+= total_beli;
			/*total_diskon 	+= harga_diskon;*/
			total_ppn 		+= harga_ppn; 
			total_pph 		+= harga_pph; 
			total_sub_total += sub_total;
		});
				
		$('#Totalharga').val(total_biaya);
		// $('#TotalDiskon').val(total_diskon);
		$('#TotalPPN').val(total_ppn);
		$('#TotalPPH').val(total_pph);
		$('#TotalHargaBayar').val(total_sub_total);
	}
	
	function save_status(val) {
		<?php if($order['fidStatusOrder']) { ?>
			showProgres();
			$.post(site_url+'order/manage/save_status'
					,{NoOrder : '<?php echo encode($order['NoOrder']) ?>',
						fidStatusOrder : val}
					,function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							
							setTimeout(function () {
								show_list();
							}, 2000);
						} else if(result.error) {
							toastr.error(result.error,'Error');
						}
					}					
					,"json"
				);
		<?php } ?>
	}

	function getKendaraan(val) {
		if(val) {
			showProgres();
			$.post(site_url+'order/manage/get_kendaraan'
					,{idKendaraan : val}
					,function(result) {
						hideProgres();
						$('#Panjang').val(result.PanjangMobil);
						$('#Lebar').val(result.LebarMobil);
						$('#Tinggi').val(result.TinggiMobil);
					}					
					,"json"
				);
		}
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Order</a></li>
		<?php if ($order['NoOrder']) {?>
		<li><a href="#" onclick="order_input('<?= encode($order['NoOrder'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $order['TypeCode'] = '';
// $order['TypeName'] = '';
?>
<section class="content" >
	<form id="input_form"  method="post" enctype="multipart/form-data">
		<div class="box box-default">
			<input name="NoOrder" hidden value="<?= encode($order['NoOrder'])?>">
			<input name="NoInvoice" hidden value="<?= $order['NoInvoice']?>">
			<div class="box-body">
				<div class="row" >
					<div class="col-md-4">
						<div class="info-box bg-aqua">
							<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
							<div class="info-box-content text-center">
								<span class="info-box-number">No Invoice</span>
								<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
								<span class="progress-description" style="font-size: 12pt;margin: 12px;" id="lbl_trans_no1">
								
								</span>
							</div>
						</div>
						
					</div>
					<div class="col-md-4 pull-right">
						<div class="info-box bg-aqua">
							<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
							<div class="info-box-content text-center">
								<span class="info-box-number">No Order</span>
								<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
								<span class="progress-description" style="font-size: 12pt;margin: 12px;" id="lbl_trans_no2">
								
								</span>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Nama Customer</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidCustomer" name="fidCustomer">
									<option value=''></option>
									<?php if($customer->num_rows() > 0) {
											foreach($customer->result_array() as $row) { ?>
												<option value='<?php echo $row['idCustomer'] ?>' <?php echo $row['idCustomer'] == $order['fidCustomer'] ? 'selected' : '' ?>><?php echo $row['NamaLengkap'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Nama Kendaraan</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidKendaraan" name="fidKendaraan" onchange="getKendaraan(this.value)">
									<option value=''></option>
									<?php if($kendaraan->num_rows() > 0) {
											foreach($kendaraan->result_array() as $row) { ?>
												<option value='<?php echo $row['idKendaraan'] ?>' <?php echo $row['idKendaraan'] == $order['fidKendaraan'] ? 'selected' : '' ?>><?php echo $row['NamaKendaraan'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Jenis Trip</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="JenisTrip" name="JenisTrip">
									<option value=''></option>
									<option value='Single' <?php echo $order['JenisTrip'] == 'Single' ? 'selected' : '' ?>>Single</option>
									<option value='Multi' <?php echo $order['JenisTrip'] == 'Multi' ? 'selected' : '' ?>>Multi</option>
								</select>
							</div>
						</div>		
						<div class="form-group">
							<label>Deskripsi Barang</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="DeskripsiBarang" name="DeskripsiBarang" cols='10' rows='1'><?= $order['DeskripsiBarang']?></textarea>
							</div>
						</div>	
						<div class="form-group">
							<label>Berat</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Berat" name="Berat" onkeypress='numberOnly(event)' placeholder="" value="<?= $order['Berat']?>">
							</div>
						</div>						
						<div class='form-group'>
							<div class="checkbox">
							  <label>
								<input type="checkbox" id="setCBM" onchange="showCBM()" <?php echo $order['Total'] ? 'checked' : '' ?>> <strong>CBM / FEET</strong>
							  </label>
							</div>
						</div>
						<div class="form-group" id="showPLT">
							<label>Panjang x Lebar x Tinggi</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" onkeypress='numberOnly(event)' style="width:75px" id="Panjang" name="Panjang" placeholder="CM" value="<?= $order['Panjang']?>">
								<input type="text" class="form-control" onkeypress='numberOnly(event)' style="width:75px" id="Lebar" name="Lebar" placeholder="CM" value="<?= $order['Lebar']?>">
								<input type="text" class="form-control" onkeypress='numberOnly(event)' style="width:75px" id="Tinggi" name="Tinggi" placeholder="CM" value="<?= $order['Tinggi']?>">
							</div>	
						</div>	
						<div class="form-group" id="showTotal">
							<label>Total</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Total" name="Total" onkeypress='numberOnly(event)' placeholder="" value="<?= $order['Total']?>">
							</div>
						</div>	
					</div>
					<div class='col-md-3'>						
						<div class="form-group">
							<label>Tgl Invoice</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TglInvoice" name="TglInvoice" placeholder="" value="<?= $order['TglInvoice']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Waktu Pengambilan</label>
							<div class="input-group col-md-12 clockpicker"> 
								<input type="text" class="form-control" id="WaktuPengambilan" name="WaktuPengambilan" placeholder="" value="<?= date('H:i', strtotime($order['WaktuPengambilan'])) ?>">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-time"></span>
								</span>
							</div>
						</div>
						<script type="text/javascript">
						$('.clockpicker').clockpicker({
							placement: 'top',
							align: 'left',
							donetext: 'Done'
						});
						</script>
						<div class="form-group">
							<label>Lokasi Asal</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="LokasiAsal" name="LokasiAsal" placeholder="" value="<?= $order['LokasiAsal']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Lokasi Tujuan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="LokasiTujuan" name="LokasiTujuan" placeholder="" value="<?= $order['LokasiTujuan']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Area Asal</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="AreaAsal" name="AreaAsal" placeholder="" value="<?= $order['AreaAsal']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Area Tujuan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="AreaTujuan" name="AreaTujuan" placeholder="" value="<?= $order['AreaTujuan']?>">
							</div>
						</div>
					</div>
					<div class='col-md-3'>
						<div class="form-group">
							<label>Tgl Kirim</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TglKirim" name="TglKirim" placeholder="" value="<?= $order['TglKirim']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Detail Lokasi Pengambilan</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="DetailLokasiPengambilan" name="DetailLokasiPengambilan" cols='10' rows='2'><?= $order['DetailLokasiPengambilan']?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label>Detail Lokasi Tujuan</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="DetailLokasiTujuan" name="DetailLokasiTujuan" cols='10' rows='2'><?= $order['DetailLokasiTujuan']?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label>Tipe Kawasan Asal</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TipeKawasanAsal" name="TipeKawasanAsal" placeholder="" value="<?= $order['TipeKawasanAsal']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Tipe Kawasan Tujuan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TipeKawasanTujuan" name="TipeKawasanTujuan" placeholder="" value="<?= $order['TipeKawasanTujuan']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Instruksi Khusus</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" id="InstruksiKhusus" name="InstruksiKhusus" cols='10' rows='1'><?= $order['InstruksiKhusus']?></textarea>
							</div>
						</div>
					</div>
					<div class='col-md-3'>
						<div class="form-group">
							<label>Tgl Order</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TglOrder" name="TglOrder" placeholder="" value="<?= $order['TglOrder']?>">
							</div>
						</div>
						<?php if($this->menu->msOperatorSpecial(array('SpecialVar' => 'set_status_order'))) { ?>
						<div class="form-group">
							<label>Status Order</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidStatusOrder" name="fidStatusOrder" onchange="save_status(this.value)">
									<?php if($status->num_rows() > 0) {
											foreach($status->result_array() as $row) { ?>
												<option value='<?php echo $row['idStatusOrder'] ?>' <?php echo $row['idStatusOrder'] == $order['fidStatusOrder'] ? 'selected' : '' ?>><?php echo $row['NamaStatusOrder'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
						<?php } ?>
						<div class="form-group">
							<label>Pengemudi</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidPengemudi" name="fidPengemudi">
									<option value=''></option>
									<?php if($pengemudi->num_rows() > 0) {
											foreach($pengemudi->result_array() as $row) { ?>
												<option value='<?php echo $row['idPengemudi'] ?>' <?php echo $row['idPengemudi'] == $order['fidPengemudi'] ? 'selected' : '' ?>><?php echo $row['NamaLengkap'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>						
						<div class='form-group'>
							<div class="checkbox">
							  <label>
								<input type="checkbox" id="AddPackaging" name="AddPackaging" <?php echo $order['AddPackaging'] == 1 ? 'checked' : '' ?>> Add Packaging
							  </label>
							</div>
						</div>
						<div class='form-group'>
							<div class="checkbox">
							  <label>
								<input type="checkbox" id="AddTenagaAngkut" name="AddTenagaAngkut" <?php echo $order['AddTenagaAngkut'] == 1 ? 'checked' : '' ?>> Add Tenaga Angkut
							  </label>
							</div>
						</div>	
						<div class='form-group'>
							<label>Foto Barang</label>
							<div class="input-group col-md-12"> 
								<div class='thumbnail'>
									<img id='preview' width=90 height=90 src='<?php echo get_pict($order['PhotoBarang'], '90x90', 'order') ?>' class='img-thumbnail img-responsive' alt='Photo'>
								</div>
								<input type='file' id='foto' name='foto' onchange='readUrl(this)'>
							</div>
						</div>
					</div>						
				</div>						
			</div><!-- /.box-body -->
		</div>
		<div class="box box-default">
			<div class='box-body'>
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8"><h2>Cost <b>Detail</b></h2></div>
						<div class="col-sm-4">
							<?php if($order['fidStatusOrder'] <= 10) { ?>
							<button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
							<?php } ?>
						</div>
					</div>
				</div>
				<table class="table table-bordered" id="table_detail">
					<thead>
						<tr>
							<th>Nama Biaya</th>
							<th>Pemasukan</th>
							<th>Pengeluaran</th>
							<th>PPN</th>
							<th>PPH</th>
							<th>SubTotal</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
				<?php if($detail_order->num_rows() > 0) {
						$no = 0;
						foreach($detail_order->result_array() as $row) { 
						$no++;
						
						$NominalPemasukan = 0;
						if($row['fidJenisKas'] == 1) 
							$NominalPemasukan = thausand_spar($row['Nominal']);
						
						$NominalPengeluaran = 0;
						if($row['fidJenisKas'] == 2) 
							$NominalPengeluaran = thausand_spar($row['Nominal']);
						?>
						<tr class="product">
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control" id="NamaBiaya<?php echo $no ?>" name="NamaBiaya[]" value="<?php echo $row['NamaBiaya'] ?>"></td>
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control nominal_pemasukan" onblur="totalHarga()" id="NominalPemasukan<?php echo $no ?>" name="NominalPemasukan[]" value="<?php echo $NominalPemasukan ?>"></td>
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control nominal_pengeluaran" onblur="totalHarga()" id="NominalPengeluaran<?php echo $no ?>" name="NominalPengeluaran[]" value="<?php echo $NominalPengeluaran ?>"></td>
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control ppn" onblur="totalHarga()" id="PPN<?php echo $no ?>" name="PPN[]" value="<?php echo $row['PPN'] ?>"></td>
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control pph" onblur="totalHarga()" id="PPH<?php echo $no ?>" name="PPH[]" value="<?php echo $row['PPH'] ?>"></td>
							<td><input type='text' <?php if($order['fidStatusOrder'] > 10) { echo 'disabled'; } ?> class="form-control sub_total" onblur="totalHarga()" id="SubTotal<?php echo $no ?>" name="SubTotal[]" value="<?php echo thausand_spar($row['SubTotal']) ?>"></td>
							<td>
								<!--a class="add" title="Add" data-toggle="tooltip"><i class="fa fa-plus-circle"></i></a>
								<a class="edit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a-->
							<?php if($order['fidStatusOrder'] <= 10) { ?>
								<a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>
							<?php } ?>
							</td>
						</tr> 
					<?php } 
						
					} ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box box-default">
			<div class='box-body'>
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8"><h2>Nilai <b>Invoice</b></h2></div>
					</div><br>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Total Harga</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="Totalharga" name="Totalharga" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($order['Totalharga']?:0)?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total PPN</label>
								<div class="input-group col-md-12"> 
									<input type="text" readonly class="form-control" id="TotalPPN" name="TotalPPN" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($order['TotalPPN']?:0) ?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total PPH</label>
								<div class="input-group col-md-12"> 
									<input type="text" readonly class="form-control" id="TotalPPH" name="TotalPPH" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($order['TotalPPH']?:0) ?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total Harga Bayar</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="TotalHargaBayar" name="TotalHargaBayar" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($order['TotalHargaBayar']?:0)?>">
								</div>
							</div>	
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
		<div class="box box-default">
			<div class="box-footer">
				<?php if($order['fidStatusOrder'] <= 10) { ?>
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<?php } ?>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</div>
	</form>
</section>