<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('order_model');
		$this->load->model('order_detail_model');
		$this->load->model('master/kendaraan_model');
		$this->load->model('master/jenis_kendaraan_model');
		$this->load->model('master/pengemudi_model');
		$this->load->model('master/customer_model');
		$this->load->model('master/status_order_model');
		$this->load->model('master/account_model');
		
	}

	public function index()
	{
		$this->manage();
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['orderby'] =  $this->input->post('t_order_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(cus."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(mudi."NamaLengkap") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->order_model->set_where($where);
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->order_model->set_order(array('NoOrder' => 'ASC'));
		//
		$this->order_model->set_limit($limit);
		$this->order_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->order_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadOrder';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->order_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id)?:0;
		$order =  $this->order_model->get($id);		
		
		//
		$data = array();
		
		$data['status'] 	= $this->status_order_model->get_list();
		$data['kendaraan'] 	= $this->kendaraan_model->get_list();
		$data['customer'] 	= $this->customer_model->get_list();
		
		if($id)
			$this->pengemudi_model->set_where(array('(idPengemudi = '.$order['fidPengemudi'].' OR Status IN (10))' => null));
		else
			$this->pengemudi_model->set_where(array('(Status IN (10))' => null));
	
		$data['pengemudi']	= $this->pengemudi_model->get_list();
		$data['content'] 	= 'input';
		$data['order'] 		= $order;
		
		$this->order_detail_model->set_where(array('NoOrder' => $order['NoOrder']));
		$data['detail_order'] 	= $this->order_detail_model->get_list();
		$data['title'] 		= 'Input Order';
		$this->load->view($data['content'],$data);
	}
	
	function change_pict($id=0)
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		/* $config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = 100; */
		$_this->load->library('image_lib',$config);
		
		$date = $id.date('YmdHis');
		$structure  = 'files/order/original';
		$structure2  = 'files/order/thumbnails_29x29';
		$structure3  = 'files/order/thumbnails_45x45';
		$structure4  = 'files/order/thumbnails_90x90';
		$structure5  = 'files/order/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['foto'];
		
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);

		if($file_ext != 'jpeg' && $file_ext != 'jpg' && $file_ext != 'gif' && $file_ext != 'png') {
			$this->error('Format photo harus jpg/png');
		}
		
		$upload_image = $structure.'/'.$date.'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{			
			// $img_size = getimagesize($upload_image);	
		  
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"maintain_ratio"      => false,
					"source_image"  => $upload_image,
					"new_image"     => './files/order/'.$r['name'].'/'.$date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize()) {             
					$this->error($_this->image_lib->display_errors());
				}
				$_this->image_lib->clear(); 
				if($r['width'].'x'.$r['height'] == '90x90') {
					$order = $this->order_model->get(decode($id));
					
					if($order['PhotoBarang']) {
						if($order['PhotoBarang'] != $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext) {
							$photo = str_replace('-90x90', '', $order['PhotoBarang']);
							$check_ext = explode(".", $photo);
							$check_file_ext = end($check_ext);
							
							$structure  = 'files/order/original/'.$photo;
							$structure2  = 'files/order/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
							$structure3  = 'files/order/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
							$structure4  = 'files/order/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
							$structure5  = 'files/order/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
							
							if(file_exists($structure))
								unlink($structure);
						
							if(file_exists($structure2))
								unlink($structure2);
							
							if(file_exists($structure3))
								unlink($structure3);
							
							if(file_exists($structure4))
								unlink($structure4);
							
							if(file_exists($structure5))
								unlink($structure5);
						}
					}
					
					$fileName = $date.'-'.$r['width'].'x'.$r['height'].'.'.$file_ext;
					$update = array();
					$update['NoOrder'] 	= decode($id);
					$update['PhotoBarang'] 	= $fileName;
					$this->order_model->save($update);
				}
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$this->error('Foto gagal disimpan');
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
	}
	
	function save_status() {
		$NoOrder		= (decode($this->input->post('NoOrder'))?:0);
		$fidStatusOrder	= ($this->input->post('fidStatusOrder'));

		$pembelian_det = $this->order_detail_model->get(array('NoOrder' => $NoOrder));
		
		if(!$pembelian_det['idOrderDetail'])
			$this->error('Silahkan input detail order terlebih dahulu');

		$this->db->trans_start();
		$data = array();
		$data['NoOrder'] 		 = $NoOrder;
		$data['fidStatusOrder'] = $fidStatusOrder;

		$this->order_model->save($data);

		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Status telah berhasil diubah ');
		}
	}

	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
		$data = array();
		$this->db->trans_start();
		$NoOrder		 		 = (decode($this->input->post('NoOrder'))?:0);
		$data['fidKendaraan']	 = $this->input->post('fidKendaraan')?:0;
		$data['JenisTrip']		 = $this->input->post('JenisTrip');
		$data['Berat']	 	 	 = $this->input->post('Berat')?:0;
		$data['Panjang']	 	 = $this->input->post('Panjang')?:0;
		$data['Lebar']	 		 = $this->input->post('Lebar')?:0;
		$data['Tinggi']	 		 = $this->input->post('Tinggi')?:0;
		$data['Total']	 		 = $this->input->post('Total')?:0;
		$data['AddPackaging']	 = $this->input->post('AddPackaging')?1:0;
		$data['AddTenagaAngkut'] = $this->input->post('AddTenagaAngkut')?1:0;
		$data['DeskripsiBarang'] = $this->input->post('DeskripsiBarang');
		$data['TglInvoice']		 = $this->input->post('TglInvoice')?getSQLDate($this->input->post('TglInvoice')):date('Y-m-d');
		$data['TglOrder']		 = $this->input->post('TglOrder')?getSQLDate($this->input->post('TglOrder')):date('Y-m-d');
		$data['TglKirim']		 = $this->input->post('TglKirim')?getSQLDate($this->input->post('TglKirim')):date('Y-m-d');
		$data['WaktuPengambilan'] = $this->input->post('WaktuPengambilan');
		$data['LokasiAsal'] 	 = $this->input->post('LokasiAsal');
		$data['LokasiTujuan'] 	 = $this->input->post('LokasiTujuan');
		$data['AreaAsal'] 	 	 = $this->input->post('AreaAsal');
		$data['AreaTujuan'] 	 = $this->input->post('AreaTujuan');
		$data['DetailLokasiPengambilan'] = $this->input->post('DetailLokasiPengambilan');
		$data['DetailLokasiTujuan'] 	 = $this->input->post('DetailLokasiTujuan');
		$data['TipeKawasanAsal'] 	 	 = $this->input->post('TipeKawasanAsal');
		$data['TipeKawasanTujuan'] 	 	 = $this->input->post('TipeKawasanTujuan');
		$data['InstruksiKhusus'] 	 	 = $this->input->post('InstruksiKhusus');
		$data['Totalharga'] 	 	 	 = text2num($this->input->post('Totalharga'))?:0;
		$data['fidCustomer'] 	 	 	 = $this->input->post('fidCustomer')?:0;
		$data['fidPengemudi'] 	 	 	 = $this->input->post('fidPengemudi')?:0;
		$data['TotalPPN'] 	 	 	 	 	 = $this->input->post('TotalPPN')?:0;
		$data['TotalPPH'] 	 	 	 		 = $this->input->post('TotalPPH')?:0;
		$data['TotalHargaBayar'] 	 	 = text2num($this->input->post('TotalHargaBayar'))?:0;
				
		$data['fidStatusOrder'] 		 = $this->input->post('fidStatusOrder')?:10;		

		//validasi data kosong
		$this->validation_input('fidKendaraan');
		$this->validation_input('fidCustomer');
		$this->validation_input('fidPengemudi');
		
		if ($NoOrder)
		{	
			$data['NoOrder'] = $NoOrder;
			$data['TglUpdate'] 	= date('Y-m-d');
			$data['UserUpdate'] = $this->session->userdata('Operator')['LoginName'];
			
			if($this->input->post('fidStatusOrder')) {				
				$update = array();
				if($data['fidPengemudi']) {
					if($data['fidStatusOrder'] == 20) {
						$update['idPengemudi']  = $data['fidPengemudi'];
						$update['Status'] 		= 30;
						$this->pengemudi_model->save($update);
					} elseif($data['fidStatusOrder'] == 50) {
						$update['idPengemudi']  = $data['fidPengemudi'];
						$update['Status'] 		= 10;
						$this->pengemudi_model->save($update);
					}
					
				}
			}
		}else
		{
			$data['TglInput'] 	= date('Y-m-d');
			$data['UserInput'] 	= $this->session->userdata('Operator')['LoginName'];
			// $data['fidStatusOrder'] = 10;
			
			$NoInvoice = $this->order_model->getNoInvoice($data['TglInput']);			
			$data['NoInvoice'] = $NoInvoice;
			
			$NoOrder = $this->order_model->getNewTrans($data['TglInput']);			
			$data['NoOrder'] = $NoOrder;	
		}
		
		if(!$data['fidKendaraan']) {
			$this->update['CallBack'] = 'fidKendaraan';
			$this->error('Kendaraan Harus diisi');
		}
		
		/* if(!$data['PPN']) {
			$this->update['CallBack'] = 'PPN';
			$this->error('PPN Harus diisi');
		} */

		if(!$data['fidPengemudi']) {
			$this->update['CallBack'] = 'fidPengemudi';
			$this->error('Pengemudi Harus diisi');
		}
		
		if(!$data['fidCustomer']) {
			$this->update['CallBack'] = 'fidCustomer';
			$this->error('Customer Harus diisi');
		}
		
		/* if(!$NoOrder) {
			$order =  $this->order_model->get(array('NamaOrder' => $data['NamaOrder']));
			if($data['NamaOrder']==$order['NamaOrder']){
				$this->error('Nama Order sudah ada');
			}
		} */
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$NamaBiaya 			= $this->input->post('NamaBiaya')?:array();
		$NominalPemasukan 	= text2num($this->input->post('NominalPemasukan'))?:0;
		$NominalPengeluaran = text2num($this->input->post('NominalPengeluaran'))?:0;
		$PPN 				= text2num($this->input->post('PPN'))?:0;
		$PPH 				= text2num($this->input->post('PPH'))?:0;
		$SubTotal 			= text2num($this->input->post('SubTotal'))?:0;
		
		if(count($NamaBiaya) > 0) {
			$this->order_detail_model->delete(array('NoOrder' => $data['NoOrder']));
			
			for($i=0;$i<count($NamaBiaya);$i++) {
				$data_detail = array();
				$data_detail['idOrderDetail'] = 0;
				$data_detail['NoOrder'] = $data['NoOrder'];
				$data_detail['NamaBiaya'] = $NamaBiaya[$i];
				$data_detail['PPN'] 	 = $PPN[$i]?:0;
				$data_detail['PPH'] 	 = $PPH[$i]?:0;
				$data_detail['SubTotal'] = $SubTotal[$i]?:0;
				
				if($NominalPemasukan[$i]) {
					$data_detail['Nominal'] = $NominalPemasukan[$i];
					$data_detail['fidJenisKas'] = 1;
				} elseif($NominalPengeluaran[$i]) {
					$data_detail['Nominal'] = $NominalPengeluaran[$i];
					$data_detail['fidJenisKas'] = 2;
				}

				$this->order_detail_model->save($data_detail);
			}
		}
		
		$save = true;
		$save = $this->order_model->save($data);
				
		if($NoOrder && $_FILES['foto']['name']) {
			$this->change_pict(encode($NoOrder));
		}
		
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['NoOrder'] = encode($NoOrder);
			$this->update['status'] = $NoOrder ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$order = $this->order_model->get($Code);
		$this->order_model->delete($Code);
		$this->order_detail_model->delete(array('NoOrder' => $order['NoOrder']));
		
		if($order['PhotoBarang']) {
			$photo = str_replace('-90x90', '', $order['PhotoBarang']);
			$check_ext = explode(".", $photo);
			$check_file_ext = end($check_ext);
			
			$structure  = 'files/order/original/'.$photo;
			$structure2  = 'files/order/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
			$structure3  = 'files/order/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
			$structure4  = 'files/order/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
			$structure5  = 'files/order/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function print_pdf($tp='',$NoOrder='')
	{
		$NoOrder = decode($NoOrder);
		$order =  $this->order_model->get($NoOrder);		
				
		$data = array();
		
		$data['kendaraan'] 	= $this->kendaraan_model->get($order['fidKendaraan']);
		$data['customer'] 	= $this->customer_model->get($order['fidCustomer']);
		$data['pengemudi']	= $this->pengemudi_model->get($order['fidPengemudi']);
		$data['account']	= $this->account_model->get(array('Status' => 1));
		
		$this->order_detail_model->set_order(array('NoOrder' => 'ASC'));
		$this->order_detail_model->set_where(array('NoOrder' => $order['NoOrder'], 'fidJenisKas' => 1));
		$data['detail_order'] 	= $this->order_detail_model->get_list();
		$data['total_nominal'] 	= $this->order_detail_model->total_nominal();
		
		//
		$data['content'] 			= 'pdf';
		$data['order'] 				= $order;
		$data['tp'] 				= $tp;
		$data['title'] 				= 'Invoice';
		
		$this->load->view($data['content'],$data);
	}
	
	function print_surat_jalan($tp='', $NoOrder='')
	{
		$this->load->library('fpdf');	
		
		$order = $this->order_model->get(decode($NoOrder));

		$this->order_detail_model->set_order(array('NoOrder' => 'ASC'));
		$this->order_detail_model->set_where(array('NoOrder' => $order['NoOrder'], 'fidJenisKas' => 1));
		$order_detail = $this->order_detail_model->get_list();

		$kendaraan 	= $this->kendaraan_model->get($order['fidKendaraan']);
		$jenis_kendaraan 	= $this->jenis_kendaraan_model->get($kendaraan['fidJenisKendaraan']);
		$pengemudi	= $this->pengemudi_model->get($order['fidPengemudi']);
		$customer 	= $this->customer_model->get($order['fidCustomer']);
		$account	= $this->account_model->get(array('Status' => 1));
	// Instanciation of inherited class
		$pdf = new fpdf('P','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$pdf->image($_SERVER['DOCUMENT_ROOT'].'/TMG/'.get_myconf('ConfigKeyValue7'),'24', '7', '20', '23', 'PNG');
		$pdf->sety(9);
		$pdf->setx(83);
		$pdf->SetFont('Arial','',20);
		// $pdf->SetTextColor(0,0,255);
		$pdf->Cell(75, 7, get_myconf('ConfigKeyValue3'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(75, 5, get_myconf('ConfigKeyValue10'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(75, 4, get_myconf('ConfigKeyValue11'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(75, 4, get_myconf('ConfigKeyValue12'), 0, 1,'C');
		$gety = $pdf->gety();
		$pdf->SetLineWidth(2);
		$pdf->Line(15,$gety+3, 195, $gety+3);

		$pdf->ln(7);
		$pdf->SetFont('Arial','',16);
		$pdf->Cell(185,5,'SURAT JALAN',0,1,'C');

		$pdf->SetLineWidth(0.2);
		$pdf->ln(1);
		$pdf->SetFont('Arial','',10);
		$pdf->setx(5);
		$pdf->Cell(40,5,'Kepada   ',0,0,'R');
		$pdf->Cell(85,5,$customer['NamaLengkap'],0,0,'L');
		$gety=$pdf->gety();
		$getx=$pdf->getx();
		$pdf->Cell(20,5,'Tanggal',0,0,'L');
		$pdf->Cell(55,5,humanize_mdate($order['TglOrder'], '%d-%m-%Y'),0,1,'L');
		$pdf->setx(5);
		$pdf->Cell(40,5,'',0,0,'R');
		$pdf->MultiCell(75,5,$customer['Alamat'],0,'L');
		$pdf->sety($gety+5);
		$pdf->setx($getx);
		$pdf->Cell(20,5,'No Order',0,0,'L');
		$pdf->Cell(55,5,$order['NoOrder'],0,2,'L');
		$pdf->setx($getx);
		$pdf->Cell(20,5,"Kendaraan",0,0,'L');
		$pdf->sety($gety+10);
		$pdf->setx($getx+20);
		$pdf->MultiCell(55,5,$jenis_kendaraan['NamaJenisKendaraan'].' / '.$kendaraan['PlatNomor'],0,'L');
		$pdf->ln(7);

		$pdf->SetFont('Arial','B',12);
		$pdf->setx(15);
		/* $pdf->Cell(25,7,'NO. MOBIL',1,0,'C');
		$pdf->Cell(50,7,'JENIS TRUCK',1,0,'C');
		$pdf->Cell(25,7,'TANGGAL',1,0,'C');*/
		$pdf->Cell(10,7,'NO',1,0,'C'); 
		$pdf->Cell(135,7,'DESCRIPTION',1,0,'C');
		$pdf->Cell(35,7,'REMARK',1,1,'C');
		
		$gety=$pdf->gety();
		$getx=$pdf->getx();
		
		$total = 0;
		$pdf->setx(15);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,7,'','RL',0,'C'); 
		$pdf->Cell(135,7,$kendaraan['PanjangMobil'].'cm X '.$kendaraan['LebarMobil'].'cm X '.$kendaraan['TinggiMobil'].'cm','RL',0,'L');
		$pdf->Cell(35,7,'','RL',1,'C');
		$pdf->setx(15);
		$pdf->Cell(10,25,'','RLB',0,'C'); 
		$pdf->Cell(135,25,'','RLB',0,'C');
		$pdf->Cell(35,25,'','RLB',1,'C');

		/* if($order_detail->num_rows() > 0) {
			$no = 1;
			$getx = 15;
			foreach($order_detail->result_array() as $row) {	
				$total += $row['SubTotal'];
				
			}
		}
		$pdf->SetFont('Arial','',10);
		$pdf->setx(15);
		$pdf->Cell(145, 5, 'Total', 0, 0,'R');
		$pdf->Cell(35, 5, thausand_spar($total), 1, 0,'R'); */
		
		$pdf->ln(10);
		// $pdf->setx(5);
		// $pdf->Cell(40, 5, '', 0, 1,'R');
		$pdf->setx(5);
		$pdf->Cell(71, 5, 'Bandung, '.date('d-m-Y'), 0, 0,'C');
		$pdf->Cell(40, 5, 'Pengemudi', 0, 0,'R');
		$pdf->Cell(60, 5, 'Penerima', 0, 1,'R');
		$gety = $pdf->gety();
		$pdf->ln(20);
		$pdf->setx(22);
		$pdf->Cell(72, 5, $this->session->userdata('Operator')['LoginName'], 0, 0,'L');
		$pdf->Cell(60, 5, ($pengemudi['NamaLengkap']?:'(........................)'), 0, 0,'L');
		$pdf->Cell(47, 5, $customer['NamaLengkap'], 0, 0,'L');

		/* $pdf->sety($gety+5);
		$pdf->setx(90);
		$pdf->SetLineWidth(1);
		$pdf->MultiCell(85,5,"Pembayaran harap di transfer ke rekening kami :".PHP_EOL.
		$account['Bank'] .($account['InisialBank']? ' ( '.$account['InisialBank'].' )' : '').PHP_EOL.
		"Nomor Rekening : ".$account['NoRek'].PHP_EOL.
		"Atas Nama : PT.TRISULA MULTISARANA GLOBAL",1,'L'); */

		$pdf->Output();	
		
	}

	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->order_model->set_limit($limit);
		$this->order_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(cus."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(mudi."NamaLengkap") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->order_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->order_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataOrder';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->order_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_order() {
		$NoOrder = trim($this->input->post('NoOrder'));
		$order = $this->order_model->get(array('NoOrder' => $NoOrder));
		$order['NoOrder'] = encode($order['NoOrder']);
		echo json_encode($order);		
	}
	
	function get_kendaraan() {
		$idKendaraan = trim($this->input->post('idKendaraan'));
		$kendaraan = $this->kendaraan_model->get($idKendaraan);
		// $kendaraan['idKendaraan'] = encode($kendaraan['idKendaraan']);
		echo json_encode($kendaraan);		
    }
}