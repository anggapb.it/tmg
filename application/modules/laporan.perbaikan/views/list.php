<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>No PO </th>
			<th>Tgl PO </th>
			<th>No Faktur </th>
			<th>Jenis Kendaraan </th>
			<th>Nama Kendaraan </th>
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Harga Beli</th>
			<!--th>Diskon</th>
			<th>PPN</th-->
			<th>Sub Total</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;			
			$i = 0;
			$rowspan_nama = 0;
			$count_nama = 0;

			$rowspan_jenis = 0;
			$count_jenis = 0;
			$j = 0;

			foreach($list->result_array() as $row)
			{
				$no++;
				$nama[$i] = $row['NamaKendaraan'];

				if($i > 0 && $nama[$i] == $nama[$i-1]) {
					$count_nama = 0;
				} else {
					$count_nama = 1;
				}
				$rowspan_nama = $nama_kendaraan[$row['NamaKendaraan']];
				
				$jenis[$i] = $row['NamaJenisKendaraan'];
				if($i > 0 && $jenis[$i] == $jenis[$i-1]) {
					$count_jenis = 0;
				} else {
					$count_jenis = 1;
				}
				$rowspan_jenis = $jenis_kendaraan[$row['NamaJenisKendaraan']];
				
			?>
			<tr>
				<td><?= $no ?></td>
				<td><?php echo match_key($row['NoPembelian'],$key['key']); ?></td>
				<td><?= humanize_mdate($row['TglPembelian'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NoFaktur'],$key['key'])?></td>
				<?php if($count_jenis) { ?>
				<td <?php if($count_jenis) { echo 'rowspan="'.$rowspan_jenis.'" style="vertical-align: middle;text-align:center"'; } ?>><?php if($count_jenis) { echo match_key($row['NamaJenisKendaraan'],$key['key']); } ?></td>
				<?php } ?>
				<?php if($count_nama) { ?>
				<td <?php if($count_nama) { echo 'rowspan="'.$rowspan_nama.'" style="vertical-align: middle;text-align:center"'; } ?>><?php if($count_nama) { echo match_key($row['NamaKendaraan'],$key['key']); }?></td>
				<?php } ?>
				<td><?= match_key($row['NamaBarang'],$key['key'])?></td>
				<td><?= $row['Qty'] ?></td>
				<td><?= thausand_spar($row['HargaBeli']) ?></td>
				<!--td><?= $row['Diskon'] ?></td>
				<td><?= $row['PPN'] ?></td-->
				<td><?= thausand_spar($row['Qty']*$row['HargaBeli']) ?></td>
			</tr>
	<?php 	$i++;	$j++;	
			}?>
			<tr>
				<th colspan='9' class='text-right'>Total</th>
				<th><?php echo thausand_spar($total) ?></th>
			</tr>
		</tbody>
</table>
<?=$paging['list']?>