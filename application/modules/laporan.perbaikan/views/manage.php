<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<script type="text/javascript">
	$(function () {
		
		$('#periode').daterangepicker(
		{
			format: 'DD-MM-YYYY',
			startDate: '<?=date('d-m-Y')?>',
			endDate: '<?=date('d-m-Y')?>'
		}, 
		function(start, end, label) {
			$('#date_start').val(start.format('DD-MM-YYYY'));
			$('#date_end').val(end.format('DD-MM-YYYY'));
		});
		
		
		$('#periode').data('daterangepicker').setStartDate('<?php echo date('01-m-Y') ?>');
		$('#periode').data('daterangepicker').setEndDate('<?php echo date('d-m-Y') ?>');
		
		pageLoadPerbaikan(1);
	});		
	
	function pageLoadPerbaikan(pg)
	{	
		showProgres();
		$.post(site_url+'laporan.perbaikan/manage/page/'+pg
			,{t_search_key : $('#search_key').val(),
				t_date_start : $('#date_start').val(),
				t_date_end : $('#date_end').val(),
				t_periode : $('#periode').val()}
			,function(result) {
				$('#resultContent').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_filter()
	{
		$( "#toggle_filter" ).toggle( "blind","down" );	
	}
	
	function printLaporan(tp)
	{
		var periode   	 = '';
		var date_start   = '';
		var date_end     = '';
		var Search       = '';
		
		if($('#periode').val()) { 
			periode   	 = $('#periode').val() 
		}
		
		if($('#date_start').val()) { 
			date_start   = $('#date_start').val() 
		}
		
		if($('#date_end').val()) { 
			date_end   	 = $('#date_end').val() 
		}
		
		if($('#search_key').val()) { 
			Search   	 = $('#search_key').val() 
		}
				
		var param = periode+'_'+date_start+'_'+date_end+'_'+Search;
		param = btoa(btoa(param));
		
		var win = window.open('laporan.perbaikan/manage/print_pdf/'+tp+'/'+param+'/'+'<?php echo 'Laporan Perbaikan ' ?>', "_blank");
		/* if(tp != 'preview'){
			win.open();
			// setTimeout(function(){ win.close(); }, 10000);
			
		} */
	
	}
</script>
<div id="list_container">
	<section class="content-header">
		<h1>
			Laporan Perbaikan Kendaraan
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Perbaikan</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="toggle_filter" hidden>							
							<div hidden>
							<input name="t_effect" id="effect" value="1">
							<input name="t_date_start" id="date_start" value='<?php echo date('01-m-Y') ?>'>
							<input name="t_date_end" id="date_end" value='<?php echo date('d-m-Y') ?>'>
							</div>
							<hr>
							<div class="row">
								<div class="form-group">
									<div class='col-sm-4'>
										<div class="form-group">
											<label>Search</label>
											<input type="text" class="form-control" id="search_key" name="t_search_key" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) pageLoadPerbaikan(1)">
										</div>
									</div>
									<div class="col-sm-4">
										<label> Periode </label>
										<div class="input-group">
										  <div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										  </div>
										  <input type="text" class="form-control" id="periode" name="t_periode">
										</div>
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="form-group">									
									<div class="col-sm-4 pull-right">
										<label>&nbsp;</label><br>
										<button type="button" class="btn btn-warning" onClick="printLaporan(1)">Print</button>
										&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-primary" onClick="pageLoadPerbaikan(1)">Browse</button>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<div id="resultContent"></div>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="input_container" hidden>
	Loading...
</div>