<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		// $this->load->model('po/po_model');
		$this->load->model('laporan_po_model');
		$this->load->model('master/kendaraan_model');
		$this->load->model('master/supplier_model');
		$this->load->model('master/status_po_model');
		
	}
	
	public function index() {
		$this->manage();
	}

	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		// $data['par'] 	 = $par;
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$par		   = $this->input->post('par');
		$filter['date_start'] = getSQLDate($this->input->post('t_date_start'));
		$filter['date_end']   = getSQLDate($this->input->post('t_date_end'));
		$periode 			  = $this->input->post('t_periode');
		$limit 				  = $this->input->post('t_limit_rows')?:10;
		
		// set condition
		$where = array();
		
		if ($periode){
			if ($filter['date_start'] <> 'NULL')
			{
				$where['tbl.TglPembelian >='] = $filter['date_start'];
			}else{
				$where['tbl.TglPembelian >='] = date('Ymd');
			}
			if ($filter['date_end'] <> 'NULL')
			{
				$where['tbl.TglPembelian <='] = $filter['date_end'];
			}else{
				$where['tbl.TglPembelian <='] = date('Ymd');
			}
		}
				
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(sup."NamaSupplier") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NoFaktur") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		$where['tbl.fidStatusPO'] = 50;
		// $where['det.fidJenisKas'] = $par;
		$this->laporan_po_model->set_where($where);
		
		$total = $this->laporan_po_model->total_nominal();
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->laporan_po_model->set_order(array('tbl.NoPembelian' => 'ASC'));
		
		$list = $this->laporan_po_model->get_list();

		$nama_kendaraan = array();
		$nama = array();
		$count_nama = 0;
		
		$jenis_kendaraan = array();
		$jenis = array();
		$count_jenis = 0;
		if($list->num_rows()) {
			$i = 0;
			$j = 0;
			foreach($list->result_array() as $key => $value) {
				$nama[$i] = $value['NamaKendaraan'];
				if($i>0 && $nama[$i] == $nama[$i-1]) {
					$nama_kendaraan[$value['NamaKendaraan']] = $count_nama + 1;
					$count_nama += 1;
				} else {
					$nama_kendaraan[$value['NamaKendaraan']] = 1;
					$count_nama = 1;
				}
				
				$jenis[$i] = $value['NamaJenisKendaraan'];
				if($i>0 && $jenis[$i] == $jenis[$i-1]) {
					$jenis_kendaraan[$value['NamaJenisKendaraan']] = $count_jenis + 1;
					$count_jenis += 1;
				} else {
					$jenis_kendaraan[$value['NamaJenisKendaraan']] = 1;
					$count_jenis = 1;
				}

				$i++;
				$j++;
			}
		}

		$this->laporan_po_model->set_limit($limit);
		$this->laporan_po_model->set_offset($limit * ($pg - 1));
		//
		
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->laporan_po_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadPerbaikan';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->laporan_po_model->get_list();		
		$data['total'] = $total;		
		$data['nama_kendaraan'] = $nama_kendaraan;		
		$data['jenis_kendaraan'] = $jenis_kendaraan;		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function print_pdf($tp='', $param='')
	{
		$this->load->library('fpdf');	
		
		list($periode,$date_start,$date_end,$Search) = explode('_',decode($param));
		
		if ($periode){
			if ($date_start <> 'NULL')
			{
				$where['tbl.TglPembelian >='] = getSQLDate($date_start);
			}else{
				$where['tbl.TglPembelian >='] = date('Ymd');
			}
			if ($date_end <> 'NULL')
			{
				$where['tbl.TglPembelian <='] = getSQLDate($date_end);
			}else{
				$where['tbl.TglPembelian <='] = date('Ymd');
			}
		}
				
		if ($Search)
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$Search.'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$Search.'%\' OR
					upper(sup."NamaSupplier") like \'%'.$Search.'%\' OR
					upper(tbl."NoFaktur") like \'%'.$Search.'%\'
				)'] = null;
		}
		
		$where['tbl.fidStatusPO'] = 50;
		// $where['det.fidJenisKas'] = $par;
		$this->laporan_po_model->set_where($where);
		
		$total = $this->laporan_po_model->total_nominal();
		$list = $this->laporan_po_model->get_list();
	// Instanciation of inherited class
		$pdf = new fpdf('L','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$pdf->sety(3);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(265, 7, 'Laporan Perbaikan Kendaraan', 0, 1,'C');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(265, 5, $date_start.' - '.$date_end, 0, 1,'C');
		$pdf->ln();
		// $pdf->setx(3);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(7, 7, 'No', 1, 0,'L');
		$pdf->Cell(30, 7, 'No Pembelian', 1, 0,'C');
		$pdf->Cell(26, 7, 'Tgl Pembelian', 1, 0,'C');
		$pdf->Cell(20, 7, 'No Faktur', 1, 0,'C');
		$pdf->Cell(25, 7, 'Jenis Kendaraan', 1, 0,'C');
		$pdf->Cell(50, 7, 'Nama Kendaraan', 1, 0,'C');
		$pdf->Cell(55, 7, 'Nama Barang', 1, 0,'C');
		$pdf->Cell(15, 7, 'Qty', 1, 0,'C');
		$pdf->Cell(20, 7, 'Harga Beli', 1, 0,'C');
		/* $pdf->Cell(15, 7, 'Diskon', 1, 0,'C');
		$pdf->Cell(15, 7, 'PPN', 1, 0,'C'); */
		$pdf->Cell(25, 7, 'SubTotal', 1, 1,'C');
		
		if($list->num_rows() > 0) {
			$no=1;
			foreach($list->result_array() as $row) {
				$pdf->SetFont('Arial','',8);
				// $pdf->setx(3);
				$pdf->Cell(7, 5, $no, 1, 0,'L');
				$pdf->Cell(30, 5, $row['NoPembelian'], 1, 0,'C');
				$pdf->Cell(26, 5, humanize_mdate($row['TglPembelian'], '%d-%m-%Y'), 1, 0,'C');
				$pdf->Cell(20, 5, $row['NoFaktur'], 1, 0,'C');
				$pdf->Cell(25, 5, $row['NamaJenisKendaraan'], 1, 0,'C');
				$pdf->Cell(50, 5, $row['NamaKendaraan'], 1, 0,'C');
				$pdf->Cell(55, 5, $row['NamaBarang'], 1, 0,'C');
				$pdf->Cell(15, 5, $row['Qty'], 1, 0,'C');
				$pdf->Cell(20, 5, thausand_spar($row['HargaBeli']), 1, 0,'R');
				/* $pdf->Cell(15, 5, $row['Diskon'], 1, 0,'C');
				$pdf->Cell(15, 5, $row['PPN'], 1, 0,'C');*/
				$pdf->Cell(25, 5, thausand_spar($row['Qty']*$row['HargaBeli']), 1, 1,'R'); 
										
				$no++;
			}
		} 
		
		$pdf->SetFont('Arial','',8);
		// $pdf->setx(5);
		$pdf->Cell(248, 5, 'Total', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($total), 1, 0,'R');
				
		$pdf->Output();	
		
	}
}