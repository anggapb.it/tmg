<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>No Order </th>
			<th>Tgl Kirim </th>
			<th>Nama Kostumer </th>
			<th>Nama Kendaraan</th>
			<th>Jenis Trip</th>
			<th>Pengemudi</th>
			<th>Gross Nilai Order</th>
			<th>Net Nilai Order</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?>
			<tr>
				<td><?= $no ?></td>
				<td><?= match_key($row['NoOrder'],$key['key'])?></td>
				<td><?= humanize_mdate($row['TglKirim'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NamaCustomer'],$key['key'])?></td>
				<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
				<td><?= $row['JenisTrip'] ?></td>
				<td><?= $row['NamaPengemudi'] ?></td>
				<td><?= thausand_spar($row['total_gross']) ?></td>
				<td><?= thausand_spar($row['total_net']) ?></td>
			</tr>
	<?php }?>
			<tr>
				<th colspan='7' class='text-right'>Total</th>
				<th><?php echo thausand_spar($total['total_gross_all']) ?></th>
				<th><?php echo thausand_spar($total['total_net_all']) ?></th>
			</tr>
		</tbody>
</table>
<?=$paging['list']?>