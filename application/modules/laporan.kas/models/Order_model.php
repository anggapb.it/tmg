<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('ho');
		$this->set_table('trOrder');
		$this->set_pk('NoOrder');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');
		
		$this->db->select('kenda.NamaKendaraan');
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
				
		$this->db->select('cus.NamaLengkap as "NamaCustomer"');
		$this->db->join('dataMaster.msCustomer cus', 'tbl.fidCustomer = cus.idCustomer', 'left');
		
		$this->db->select('mudi.NamaLengkap as "NamaPengemudi"');
		$this->db->join('dataMaster.msPengemudi mudi', 'tbl.fidPengemudi = mudi.idPengemudi', 'left');
		
		$this->db->select('sts.NamaStatusOrder');
		$this->db->join('dataMaster.msStatusOrder sts', 'tbl.fidStatusOrder = sts.idStatusOrder', 'left');
		
		$this->db->select('det.*');
		$this->db->join('(SELECT "NoOrder",
							"fidJenisKas",
							SUM ( "Nominal" ) AS total_gross,
							SUM ( "SubTotal" ) AS total_net 
						FROM
							"ho"."trOrderDetail" 
						GROUP BY
							"NoOrder",
							"fidJenisKas" ) det', 'tbl.NoOrder = det.NoOrder', 'left');
		
		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msCustomer cus', 'tbl.fidCustomer = cus.idCustomer', 'left');
		$this->db->join('dataMaster.msPengemudi mudi', 'tbl.fidPengemudi = mudi.idPengemudi', 'left');
		$this->db->join('(SELECT "NoOrder",
							"fidJenisKas",
							SUM ( "Nominal" ) AS total_gross,
							SUM ( "SubTotal" ) AS total_net 
						FROM
							"ho"."trOrderDetail" 
						GROUP BY
							"NoOrder",
							"fidJenisKas" ) det', 'tbl.NoOrder = det.NoOrder', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}
	
	function total_nominal()
	{
		$this->db->select_sum('total_gross', 'total_gross_all');
		$this->db->select_sum('total_net', 'total_net_all');
				
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msCustomer cus', 'tbl.fidCustomer = cus.idCustomer', 'left');
		$this->db->join('dataMaster.msPengemudi mudi', 'tbl.fidPengemudi = mudi.idPengemudi', 'left');
		$this->db->join('(SELECT "NoOrder",
							"fidJenisKas",
							SUM ( "Nominal" ) AS total_gross,
							SUM ( "SubTotal" ) AS total_net 
						FROM
							"ho"."trOrderDetail" 
						GROUP BY
							"NoOrder",
							"fidJenisKas" ) det', 'tbl.NoOrder = det.NoOrder', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row;
	}

	function get_list_union($where_date='', $where='')
	{
		
		$limit = '';
		$offset = '';
		
		if($this->limit)
			$limit = ' LIMIT '.$this->limit;
		
		if($this->offset)
			$offset = ' OFFSET '.$this->offset;

		// $where_date_order = date('Y-m');
		// $where_date_po = date('Y-m');
		/* $where_date = '';
		if($year && $bulan == 'ALL') {
			$where_date = 'WHERE TO_CHAR(tbl."TglOrder", \'YYYY\') = \''.$year.'\'';
		} elseif($year && $bulan != 'ALL') {			
			$where_date = 'WHERE TO_CHAR(tbl."TglOrder", \'YYYY-MM\') = \''.$year.'-'.$bulan.'\'';
		}	
		
		$where = '';
		if($search) {
			if($where_date)
			$where = " AND UPPER(tbl.\"NoInvoice\") like '%$search%'";
			else
			$where = "WHERE UPPER(tbl.\"NoInvoice\") like '%$search%'";
		} */
		/* if($fidJenisKas) {
			if($where) {
				$where .= ' AND tbl."fidJenisKas" = '.$fidJenisKas;
			} else {
				$where = 'WHERE tbl."fidJenisKas" = '.$fidJenisKas;
			}
		} */
		
		$sql =
<<<EOT
SELECT
	* 
FROM
	(
SELECT
	ord."TglKirim",
	ord."NoOrder",
	COALESCE ( ord_det_keluar.total_gross_pengeluaran, 0 ) total_gross,
	COALESCE ( ord_det_keluar.total_pengeluaran, 0 ) total_net,
	1 AS tipe,
	cus."NamaLengkap" as "NamaCustomer",
	kenda."NamaKendaraan",
	mudi."NamaLengkap" AS "NamaPengemudi",
	ord."JenisTrip" 
FROM
	ho."trOrder" ord
	LEFT JOIN (
SELECT
	"NoOrder",
	SUM ( "Nominal" ) AS total_gross_pengeluaran,
	SUM ( "SubTotal" ) AS total_pengeluaran 
FROM
	ho."trOrderDetail" 
WHERE
	"fidJenisKas" = 2 
GROUP BY
	"NoOrder" 
	) ord_det_keluar ON ord_det_keluar."NoOrder" = ord."NoOrder"
	LEFT JOIN "dataMaster"."msKendaraan" kenda ON kenda."idKendaraan" = ord."fidKendaraan"
	LEFT JOIN "dataMaster"."msCustomer" cus ON cus."idCustomer" = ord."fidCustomer"
	LEFT JOIN "dataMaster"."msPengemudi" mudi ON mudi."idPengemudi" = ord."fidPengemudi" 
WHERE
	ord."fidStatusOrder" = 50 UNION
SELECT
	po."TglPembelian",
	po."NoPembelian",
	COALESCE ( po_det_keluar.total_gross_pengeluaran, 0 ) total_gross_pengeluaran,
	COALESCE ( po_det_keluar.total_pengeluaran, 0 ) total_pengeluaran,
	2 AS tipe,
	'',
	kenda."NamaKendaraan",
	'',
	'' 
FROM
	ho."trPO" po
	LEFT JOIN (
SELECT
	"fidPO",
	SUM ( "Qty" * "HargaBeli" ) AS total_gross_pengeluaran,
	SUM ( "SubTotal" ) AS total_pengeluaran 
FROM
	ho."trPODetail" 
WHERE
	"fidJenisKas" = 2 
GROUP BY
	"fidPO" 
	) po_det_keluar ON po_det_keluar."fidPO" = po."idPO"
	LEFT JOIN "dataMaster"."msKendaraan" kenda ON kenda."idKendaraan" = po."fidKendaraan" 
WHERE
	po."fidStatusPO" = 50 UNION
SELECT
	po."TglPembelian",
	po."NoPembelian",
	COALESCE ( po_det_keluar.total_gross_pengeluaran, 0 ) total_gross_pengeluaran,
	COALESCE ( po_det_keluar.total_pengeluaran, 0 ) total_pengeluaran,
	3 AS tipe,
	'',
	'',
	'',
	'' 
FROM
	ho."trPOMisc" po
	LEFT JOIN (
SELECT
	"fidPOMisc",
	SUM ( "Qty" * "HargaBeli" ) AS total_gross_pengeluaran,
	SUM ( "SubTotal" ) AS total_pengeluaran 
FROM
	ho."trPOMiscDetail" 
WHERE
	"fidJenisKas" = 2 
GROUP BY
	"fidPOMisc" 
	) po_det_keluar ON po_det_keluar."fidPOMisc" = po."idPOMisc" 
WHERE
	po."fidStatusPO" = 50 
	) tbl 
WHERE (tbl.total_gross > 0 and tbl.total_net > 0) $where_date$where
ORDER BY
	tbl.tipe,
	tbl."TglKirim",
	tbl."NoOrder"
$limit $offset		
EOT
;


		$query = $this->db->query($sql);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			return $query;
			$query->free_result();
        }
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
