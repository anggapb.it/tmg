<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('order_model');
		$this->load->model('master/kendaraan_model');
		$this->load->model('master/pengemudi_model');
		$this->load->model('master/customer_model');
		$this->load->model('master/status_order_model');
		
	}
	
	public function manage($par=0)
	{
		$data = array();
		$data['content'] = 'manage';
		$data['par'] 	 = $par;
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$par		   = $this->input->post('par');
		$filter['date_start'] = getSQLDate($this->input->post('t_date_start'));
		$filter['date_end']   = getSQLDate($this->input->post('t_date_end'));
		$periode 			  = $this->input->post('t_periode');
		$limit 				  = $this->input->post('t_limit_rows')?:10;
		
		// set condition
		$where = array();
		
		if ($periode){
			if ($filter['date_start'] <> 'NULL')
			{
				$where['tbl.TglKirim >='] = $filter['date_start'];
			}else{
				$where['tbl.TglKirim >='] = date('Ymd');
			}
			if ($filter['date_end'] <> 'NULL')
			{
				$where['tbl.TglKirim <='] = $filter['date_end'];
			}else{
				$where['tbl.TglKirim <='] = date('Ymd');
			}
		}
				
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(cus."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(mudi."NamaLengkap") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		$where['tbl.fidStatusOrder'] = 50;
		$where['det.fidJenisKas'] 	 = $par;
		$this->order_model->set_where($where);
		
		$total = $this->order_model->total_nominal();
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->order_model->set_order(array('tbl.NoOrder' => 'ASC'));
		
		/* $list = $this->order_model->get_list();	;
		echo $this->db->last_query();die(); */
		$this->order_model->set_limit($limit);
		$this->order_model->set_offset($limit * ($pg - 1));
		//
		
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->order_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadOrder';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->order_model->get_list();		
		$data['total'] = $total;		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function page_union($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$par		   = $this->input->post('par');
		$filter['date_start'] = getSQLDate($this->input->post('t_date_start'));
		$filter['date_end']   = getSQLDate($this->input->post('t_date_end'));
		$periode 			  = $this->input->post('t_periode');
		$limit 				  = $this->input->post('t_limit_rows')?:10;
		
		$where_date = '';
		if ($periode){
			if ($filter['date_start'] <> 'NULL')
			{
				// $where['tbl.TglKirim >='] = $filter['date_start'];
				$where_date .= ' AND tbl."TglKirim" >= \''.$filter['date_start'].'\'';
			}else{
				// $where['tbl."TglKirim" >='] = date('Ymd');
				$where_date .= ' AND tbl."TglKirim" >= \''.date('Ymd').'\'';
			}
			if ($filter['date_end'] <> 'NULL')
			{
				// $where['tbl."TglKirim" <='] = $filter['date_end'];
				$where_date .= ' AND tbl."TglKirim" <= \''.$filter['date_end'].'\'';
			}else{
				// $where['tbl."TglKirim" <='] = date('Ymd');
				$where_date .= ' AND tbl."TglKirim" <= \''.date('Ymd').'\'';
			}
		}
		
		$where = '';
		if ($filter['key'])
		{
			$where = ' AND (
					upper(tbl."NoOrder") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaCustomer") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaPengemudi") like \'%'.$filter['key'].'%\'
				)';
		}
		
		$list = $this->order_model->get_list_union($where_date, $where);

		$total['total_gross_all'] = 0;
		$total['total_net_all'] = 0;
		if($list->num_rows() > 0) {
			foreach($list->result_array() as $row) {
				$total['total_gross_all'] 	+= $row['total_gross'];
				$total['total_net_all'] 	+= $row['total_net'];
			}
		}
		$this->order_model->set_limit($limit);
		$this->order_model->set_offset($limit * ($pg - 1));
		//
		
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $list->num_rows() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadOrder';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->order_model->get_list_union($where_date, $where);		
		$data['total'] = $total;		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}

	function print_pdf($tp='', $param='')
	{
		$this->load->library('fpdf');	
		
		list($periode,$date_start,$date_end,$Search, $par) = explode('_',decode($param));
		
		if ($periode){
			if ($date_start <> 'NULL')
			{
				$where['tbl.TglKirim >='] = getSQLDate($date_start);
			}else{
				$where['tbl.TglKirim >='] = date('Ymd');
			}
			if ($date_end <> 'NULL')
			{
				$where['tbl.TglKirim <='] = getSQLDate($date_end);
			}else{
				$where['tbl.TglKirim <='] = date('Ymd');
			}
		}
				
		if ($Search)
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$Search.'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$Search.'%\' OR
					upper(cus."NamaLengkap") like \'%'.$Search.'%\' OR
					upper(mudi."NamaLengkap") like \'%'.$Search.'%\'
				)'] = null;
		}
		
		$where['tbl.fidStatusOrder'] = 50;
		$where['det.fidJenisKas'] = $par;
		$this->order_model->set_where($where);
		
		$total = $this->order_model->total_nominal();

		$this->order_model->set_order(array('tbl.NoOrder' => 'ASC'));
		$list = $this->order_model->get_list();
	// Instanciation of inherited class
		$pdf = new fpdf('L','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$pdf->sety(3);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(260, 7, 'Laporan Kas', 0, 1,'C');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(37, 5, '', 0, 0,'C');
		$pdf->Cell(185, 5, $date_start.' - '.$date_end, 0, 1,'C');
		$pdf->ln();
		$pdf->setx(5);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(10, 7, 'No', 1, 0,'L');
		$pdf->Cell(28, 7, 'No Order', 1, 0,'C');
		$pdf->Cell(20, 7, 'Tgl Kirim', 1, 0,'C');
		$pdf->Cell(45, 7, 'Nama Kostumer', 1, 0,'C');
		$pdf->Cell(45, 7, 'Nama Kendaraan', 1, 0,'C');
		$pdf->Cell(20, 7, 'Jenis Trip', 1, 0,'C');
		$pdf->Cell(45, 7, 'Pengemudi', 1, 0,'C');
		$pdf->Cell(37, 7, 'Gross Nilai Order', 1, 0,'C');
		$pdf->Cell(35, 7, 'Net Nilai Order', 1, 1,'C');
		
		if($list->num_rows() > 0) {
			$no=1;
			foreach($list->result_array() as $row) {
				$pdf->SetFont('Arial','',8);
				$pdf->setx(5);
				$pdf->Cell(10, 5, $no, 1, 0,'L');
				$pdf->Cell(28, 5, $row['NoOrder'], 1, 0,'C');
				$pdf->Cell(20, 5, humanize_mdate($row['TglKirim'], '%d-%m-%Y'), 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaCustomer'], 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaKendaraan'], 1, 0,'C');
				$pdf->Cell(20, 5, $row['JenisTrip'], 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaPengemudi'], 1, 0,'C');
				$pdf->Cell(37, 5, thausand_spar($row['total_gross']), 1, 0,'R');
				$pdf->Cell(35, 5, thausand_spar($row['total_net']), 1, 1,'R');
										
				$no++;
			}
		} 
		
		$pdf->SetFont('Arial','',8);
		$pdf->setx(5);
		$pdf->Cell(213, 5, 'Total', 1, 0,'R');
		$pdf->Cell(37, 5, thausand_spar($total['total_gross_all']), 1, 0,'R');
		$pdf->Cell(35, 5, thausand_spar($total['total_net_all']), 1, 0,'R');
				
		$pdf->Output();	
		
	}

	function print_pdf_union($tp='', $param='')
	{
		$this->load->library('fpdf');	
		
		list($periode,$date_start,$date_end,$Search, $par) = explode('_',decode($param));
		
		$where_date = '';
		if ($periode){
			if ($date_start <> 'NULL')
			{
				// $where['tbl.TglKirim >='] = $date_start;
				$where_date .= ' AND tbl."TglKirim" >= \''.getSQLDate($date_start).'\'';
			}else{
				// $where['tbl."TglKirim" >='] = date('Ymd');
				$where_date .= ' AND tbl."TglKirim" >= \''.date('Ymd').'\'';
			}
			if ($date_end <> 'NULL')
			{
				// $where['tbl."TglKirim" <='] = $date_end;
				$where_date .= ' AND tbl."TglKirim" <= \''.getSQLDate($date_end).'\'';
			}else{
				// $where['tbl."TglKirim" <='] = date('Ymd');
				$where_date .= ' AND tbl."TglKirim" <= \''.date('Ymd').'\'';
			}
		}
		
		$where = '';
		if ($Search)
		{
			$where = ' AND (
					upper(tbl."NoOrder") like \'%'.$Search.'%\' OR
					upper(tbl."NamaKendaraan") like \'%'.$Search.'%\' OR
					upper(tbl."NamaCustomer") like \'%'.$Search.'%\' OR
					upper(tbl."NamaPengemudi") like \'%'.$Search.'%\'
				)';
		}
		
		$list = $this->order_model->get_list_union($where_date, $where);

		$total['total_gross_all'] = 0;
		$total['total_net_all'] = 0;
		if($list->num_rows() > 0) {
			foreach($list->result_array() as $row) {
				$total['total_gross_all'] 	+= $row['total_gross'];
				$total['total_net_all'] 	+= $row['total_net'];
			}
		}
	// Instanciation of inherited class
		$pdf = new fpdf('L','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$pdf->sety(3);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(260, 7, 'Laporan Kas', 0, 1,'C');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(37, 5, '', 0, 0,'C');
		$pdf->Cell(185, 5, $date_start.' - '.$date_end, 0, 1,'C');
		$pdf->ln();
		$pdf->setx(5);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(10, 7, 'No', 1, 0,'C');
		$pdf->Cell(28, 7, 'No Order', 1, 0,'C');
		$pdf->Cell(20, 7, 'Tgl Kirim', 1, 0,'C');
		$pdf->Cell(45, 7, 'Nama Kostumer', 1, 0,'C');
		$pdf->Cell(45, 7, 'Nama Kendaraan', 1, 0,'C');
		$pdf->Cell(20, 7, 'Jenis Trip', 1, 0,'C');
		$pdf->Cell(45, 7, 'Pengemudi', 1, 0,'C');
		$pdf->Cell(37, 7, 'Gross Nilai Order', 1, 0,'C');
		$pdf->Cell(35, 7, 'Net Nilai Order', 1, 1,'C');
		
		if($list->num_rows() > 0) {
			$no=1;
			foreach($list->result_array() as $row) {
				$pdf->SetFont('Arial','',8);
				$pdf->setx(5);
				$pdf->Cell(10, 5, $no, 1, 0,'C');
				$pdf->Cell(28, 5, $row['NoOrder'], 1, 0,'C');
				$pdf->Cell(20, 5, humanize_mdate($row['TglKirim'], '%d-%m-%Y'), 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaCustomer'], 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaKendaraan'], 1, 0,'C');
				$pdf->Cell(20, 5, $row['JenisTrip'], 1, 0,'C');
				$pdf->Cell(45, 5, $row['NamaPengemudi'], 1, 0,'C');
				$pdf->Cell(37, 5, thausand_spar($row['total_gross']), 1, 0,'R');
				$pdf->Cell(35, 5, thausand_spar($row['total_net']), 1, 1,'R');
										
				$no++;
			}
		} 
		
		$pdf->SetFont('Arial','',8);
		$pdf->setx(5);
		$pdf->Cell(213, 5, 'Total', 1, 0,'R');
		$pdf->Cell(37, 5, thausand_spar($total['total_gross_all']), 1, 0,'R');
		$pdf->Cell(35, 5, thausand_spar($total['total_net_all']), 1, 0,'R');
				
		$pdf->Output();	
		
	}
}