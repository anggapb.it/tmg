<style>
.bigCheckbox
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  padding: 10px;
  margin-top:3px;
}
</style>
<script type="text/javascript">
	$(function () {
		$('.select2').select2();
		/* $('#periode').daterangepicker(
		{
			format: 'DD-MM-YYYY',
			startDate: '<?=date('d-m-Y')?>',
			endDate: '<?=date('d-m-Y')?>'
		}, 
		function(start, end, label) {
			$('#bulan').val(start.format('DD-MM-YYYY'));
			$('#tahun').val(end.format('DD-MM-YYYY'));
		
		
		$('#periode').data('daterangepicker').setStartDate('<?php echo date('01-m-Y') ?>');
		$('#periode').data('daterangepicker').setEndDate('<?php echo date('d-m-Y') ?>'); */
		
		pageLoadLabaRugi(1);
	});		
	
	function pageLoadLabaRugi(pg)
	{	
		showProgres();
		$.post(site_url+'laporan.laba_rugi/manage/page/'+pg
			,{t_search_key : $('#search_key').val(),
				t_bulan : $('#bulan').val(),
				t_tahun : $('#tahun').val()}
			,function(result) {
				$('#resultContent').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_filter()
	{
		$( "#toggle_filter" ).toggle( "blind","down" );	
	}
	
	function printLaporan(tp)
	{
		var bulan   	= '';
		var tahun     	= '';
		var Search     	= 0;
				
		if($('#bulan').val()) { 
			bulan   = $('#bulan').val() 
		}
		
		if($('#tahun').val()) {
			tahun   = $('#tahun').val() 
		}
		
		if($('#search_key').val()) { 
			Search   	 = $('#search_key').val() 
		}
				
		var param = bulan+'_'+tahun+'_'+Search;
		param = btoa(btoa(param));
		
		var win = window.open('laporan.laba_rugi/manage/print_pdf/'+tp+'/'+param+'/'+'<?php echo 'Laporan Laba Rugi ' ?>', "_blank");
		/* if(tp != 'preview'){
			win.open();
			// setTimeout(function(){ win.close(); }, 10000);
			
		} */
	
	}
</script>
<div id="list_container">
	<section class="content-header">
		<h1>
			Laporan Laba Rugi
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">LabaRugi</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="toggle_filter" hidden>							
							<div hidden>
							<!--input name="t_effect" id="effect" value="1">
							<input name="t_bulan" id="bulan" value='<?php echo date('01-m-Y') ?>'>
							<input name="t_tahun" id="tahun" value='<?php echo date('d-m-Y') ?>'-->
							</div>
							<hr>
							<div class="row">
								<div class="form-group">
									<div class='col-sm-4'>
										<div class="form-group">
											<label>Search</label>
											<input type="text" class="form-control" id="search_key" name="t_search_key" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) pageLoadLabaRugi(1)">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
										<label> Bulan </label><br>
										<select id="bulan" name="t_bulan" class="form-control select2">
											<option value="01" <?php echo date('m') == '01' ? 'selected' : '' ?>>Januari</option>
											<option value="02" <?php echo date('m') == '02' ? 'selected' : '' ?>>Februari</option>
											<option value="03" <?php echo date('m') == '03' ? 'selected' : '' ?>>Maret</option>
											<option value="04" <?php echo date('m') == '04' ? 'selected' : '' ?>>April</option>
											<option value="05" <?php echo date('m') == '05' ? 'selected' : '' ?>>Mei</option>
											<option value="06" <?php echo date('m') == '06' ? 'selected' : '' ?>>Juni</option>
											<option value="07" <?php echo date('m') == '07' ? 'selected' : '' ?>>Juli</option>
											<option value="08" <?php echo date('m') == '08' ? 'selected' : '' ?>>Agustus</option>
											<option value="09" <?php echo date('m') == '09' ? 'selected' : '' ?>>September</option>
											<option value="10" <?php echo date('m') == '10' ? 'selected' : '' ?>>Oktober</option>
											<option value="11" <?php echo date('m') == '11' ? 'selected' : '' ?>>November</option>
											<option value="12" <?php echo date('m') == '12' ? 'selected' : '' ?>>Desember</option>
											<option value="0">All</option>
										</select>
										</div>
									</div>
									<div class="col-sm-2">
										
									<div class="form-group">
										<label> Tahun </label><br>
										<select id="tahun" name="t_tahun" class="form-control select2">
										<?php for($i=date('Y')-10;$i<=date('Y')+10;$i++) { ?>
											<option value="<?php echo $i ?>" <?php echo $i == date('Y') ? 'selected' : '' ?>><?php echo $i ?></option>
										<?php } ?>
										</select>
									</div>
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="form-group">									
									<div class="col-sm-4 pull-right">
										<label>&nbsp;</label><br>
										<button type="button" class="btn btn-warning" onClick="printLaporan(1)">Print</button>
										&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-primary" onClick="pageLoadLabaRugi(1)">Browse</button>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<div id="resultContent"></div>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="input_container" hidden>
	Loading...
</div>