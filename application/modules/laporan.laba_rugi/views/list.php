<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>Tgl</th>
			<th>No Transaksi</th>
			<th>Pemasukan</th>
			<th>Pengeluaran </th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;

			/* $Pemasukan = 0;
			$Pengeluaran = 0; */

			// $TotalPemasukan = 0;
			// $TotalPengeluaran = 0;
			$i=0;
			foreach($list->result_array() as $row)
			{
				$no++;
				

				/* if($row['fidJenisKas'] == 1) {
					$Pemasukan = $row['Biaya'];
					$Pengeluaran = 0;
				} elseif($row['fidJenisKas'] == 2) {
					$Pengeluaran = $row['Biaya'];
					$Pemasukan = 0;
				} */

				// $TotalPemasukan += $Pemasukan;
				// $TotalPengeluaran += $Pengeluaran;
			?>
			<tr>
				<td><?= $no ?></td>
				<td><?= humanize_mdate($row['TglInvoice'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NoInvoice'],$key['key'])?></td>
				<td><?= thausand_spar($row['total_pemasukan']) ?></td>
				<td><?= thausand_spar($row['total_pengeluaran']) ?></td>
			</tr>
	<?php 		$i++;
			}?>
			<tr>
				<th colspan='3' class='text-right'>Total</th>
				<th><?php echo thausand_spar($TotalPemasukan) ?></th>
				<th><?php echo thausand_spar($TotalPengeluaran) ?></th>
			</tr>
			<!--tr>
				<th colspan='3' class='text-right'>Total Diskon</th>
				<th><?php echo thausand_spar($TotalDiskonPemasukan) ?></th>
				<th><?php echo thausand_spar($TotalDiskonPengeluaran) ?></th>
			</tr>
			<tr>
				<th colspan='3' class='text-right'>Total PPN</th>
				<th><?php echo thausand_spar($TotalPPNPemasukan) ?></th>
				<th><?php echo thausand_spar($TotalPPNPengeluaran) ?></th>
			</tr>
			<tr>
				<th colspan='3' class='text-right'>Total PPH</th>
				<th><?php echo thausand_spar($TotalPPHPemasukan) ?></th>
				<th><?php echo thausand_spar($TotalPPHPengeluaran) ?></th>
			</tr>
			<tr>
				<th colspan='3' class='text-right'>Grand total</th>
				<th><?php echo thausand_spar($TotalPemasukan-$TotalDiskonPemasukan+$TotalPPNPemasukan-$TotalPPHPemasukan) ?></th>
				<th><?php echo thausand_spar($TotalPengeluaran-$TotalDiskonPengeluaran+$TotalPPNPengeluaran-$TotalPPHPengeluaran) ?></th>
			</tr-->
		</tbody>
</table>
<?=$paging['list']?>