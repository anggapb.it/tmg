<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_po_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('ho');
		$this->set_table('trPO');
		$this->set_pk('idPO');
		$this->set_log(true);
    }	
		
    function get_list($year='', $bulan='ALL', $search='', $fidJenisKas=0)
	{
		
		$limit = '';
		$offset = '';
		
		if($this->limit)
			$limit = ' LIMIT '.$this->limit;
		
		if($this->offset)
			$offset = ' OFFSET '.$this->offset;

		// $where_date_order = date('Y-m');
		// $where_date_po = date('Y-m');
		$where_date = '';
		if($year && $bulan == 'ALL') {
			$where_date = 'WHERE TO_CHAR(tbl."TglInvoice", \'YYYY\') = \''.$year.'\'';
		} elseif($year && $bulan != 'ALL') {			
			$where_date = 'WHERE TO_CHAR(tbl."TglInvoice", \'YYYY-MM\') = \''.$year.'-'.$bulan.'\'';
		}	
		
		$where = '';
		if($search) {
			if($where_date)
			$where = " AND UPPER(tbl.\"NoInvoice\") like '%$search%'";
			else
			$where = "WHERE UPPER(tbl.\"NoInvoice\") like '%$search%'";
		}
		/* if($fidJenisKas) {
			if($where) {
				$where .= ' AND tbl."fidJenisKas" = '.$fidJenisKas;
			} else {
				$where = 'WHERE tbl."fidJenisKas" = '.$fidJenisKas;
			}
		} */
		
		$sql =
<<<EOT
SELECT
	* 
FROM
	(
SELECT
	"TglInvoice",
	"NoInvoice",
	COALESCE ( ord_det_masuk.total_pemasukan, 0 ) total_pemasukan,
	COALESCE ( ord_det_keluar.total_pengeluaran, 0 ) total_pengeluaran,
	1 AS tipe 
FROM
	ho."trOrder" ord
	LEFT JOIN ( SELECT "NoOrder", SUM ( "SubTotal" ) AS total_pemasukan FROM ho."trOrderDetail" WHERE "fidJenisKas" = 1 GROUP BY "NoOrder" ) ord_det_masuk ON ord_det_masuk."NoOrder" = ord."NoOrder"
	LEFT JOIN ( SELECT "NoOrder", SUM ( "SubTotal" ) AS total_pengeluaran FROM ho."trOrderDetail" WHERE "fidJenisKas" = 2 GROUP BY "NoOrder" ) ord_det_keluar ON ord_det_keluar."NoOrder" = ord."NoOrder" 
WHERE
	ord."fidStatusOrder" = 50 UNION
SELECT
	po."TglPembelian",
	po."NoPembelian",
	COALESCE ( po_det_masuk.total_pemasukan, 0 ) total_pemasukan,
	COALESCE ( po_det_keluar.total_pengeluaran, 0 ) total_pengeluaran,
	2 AS tipe 
FROM
	ho."trPO" po
	LEFT JOIN ( SELECT "fidPO", SUM ( "SubTotal" ) AS total_pemasukan FROM ho."trPODetail" WHERE "fidJenisKas" = 1 GROUP BY "fidPO" ) po_det_masuk ON po_det_masuk."fidPO" = po."idPO"
	LEFT JOIN ( SELECT "fidPO", SUM ( "SubTotal" ) AS total_pengeluaran FROM ho."trPODetail" WHERE "fidJenisKas" = 2 GROUP BY "fidPO" ) po_det_keluar ON po_det_keluar."fidPO" = po."idPO" 
WHERE
	po."fidStatusPO" = 50 UNION
SELECT
	po."TglPembelian",
	po."NoPembelian",
	COALESCE ( po_det_masuk.total_pemasukan, 0 ) total_pemasukan,
	COALESCE ( po_det_keluar.total_pengeluaran, 0 ) total_pengeluaran,
	3 AS tipe 
FROM
	ho."trPOMisc" po
	LEFT JOIN ( SELECT "fidPOMisc", SUM ( "SubTotal" ) AS total_pemasukan FROM ho."trPOMiscDetail" WHERE "fidJenisKas" = 1 GROUP BY "fidPOMisc" ) po_det_masuk ON po_det_masuk."fidPOMisc" = po."idPOMisc"
	LEFT JOIN ( SELECT "fidPOMisc", SUM ( "SubTotal" ) AS total_pengeluaran FROM ho."trPOMiscDetail" WHERE "fidJenisKas" = 2 GROUP BY "fidPOMisc" ) po_det_keluar ON po_det_keluar."fidPOMisc" = po."idPOMisc" 
WHERE
	po."fidStatusPO" = 50 
	) tbl 
$where_date$where
ORDER BY
	tbl.tipe,
	tbl."TglInvoice",
	tbl."NoInvoice"
$limit $offset		
EOT
;


		$query = $this->db->query($sql);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			return $query;
			$query->free_result();
        }
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
