<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		// $this->load->model('po/po_model');
		$this->load->model('laporan_po_model');
		
	}
	
	public function index() {
		$this->manage();
	}

	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		// $data['par'] 	 = $par;
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$bulan		   = $this->input->post('t_bulan')?:'ALL';
		$tahun 	   	   = $this->input->post('t_tahun');
		$limit 		   = $this->input->post('t_limit_rows')?:10;
		
		// set condition
		$where = array();
		
		$bulan = $bulan ?:'ALL';
		$list = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key']);
		
		/* $list_masuk = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key'], 1);
		$list_keluar = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key'], 2); */
		// echo $this->db->last_query();die();

		$TotalPemasukan = 0;
		/* $TotalPPHPemasukan = 0;
		$TotalPPNPemasukan = 0;
		$TotalDiskonPemasukan = 0; */
		
		$TotalPengeluaran = 0;
		/* $TotalPPHPengeluaran = 0;
		$TotalPPNPengeluaran = 0;
		$TotalDiskonPengeluaran = 0; */
		
		if($list->num_rows() > 0) {
			foreach($list->result_array() as $row) {
				$TotalPemasukan += $row['total_pemasukan'];
				$TotalPengeluaran += $row['total_pengeluaran'];
			}
		}
		
		/* if($list_keluar->num_rows() > 0) {
			foreach($list_keluar->result_array() as $row) {
				$TotalPengeluaran += $row['Biaya'];
				$TotalPPHPengeluaran += $row['TotalPPH'];
				$TotalPPNPengeluaran += $row['TotalPPN'];
				$TotalDiskonPengeluaran += $row['TotalDiskon'];
			}
		} */

		$this->laporan_po_model->set_limit($limit);
		$this->laporan_po_model->set_offset($limit * ($pg - 1));
		//
		
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $list->num_rows();
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadLabaRugi';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key']);
		$data['TotalPemasukan'] = $TotalPemasukan;		
		$data['TotalPengeluaran'] = $TotalPengeluaran;		
		/* $data['TotalPPHPemasukan'] = $TotalPPHPemasukan;		
		$data['TotalPPHPengeluaran'] = $TotalPPHPengeluaran;		
		$data['TotalPPNPemasukan'] = $TotalPPNPemasukan;		
		$data['TotalPPNPengeluaran'] = $TotalPPNPengeluaran;		
		$data['TotalDiskonPemasukan'] = $TotalDiskonPemasukan;		
		$data['TotalDiskonPengeluaran'] = $TotalDiskonPengeluaran; */		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function print_pdf($tp='', $param='')
	{
		$this->load->library('fpdf');	
		
		list($bulan,$tahun,$Search) = explode('_',decode($param));
		
		$bulan = $bulan ?:'ALL';
		$list = $this->laporan_po_model->get_list($tahun, $bulan, $Search);
		
		/* $list_masuk = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key'], 1);
		$list_keluar = $this->laporan_po_model->get_list($tahun, $bulan, $filter['key'], 2); */
		// echo $this->db->last_query();die();

		$TotalPemasukan = 0;
		/* $TotalPPHPemasukan = 0;
		$TotalPPNPemasukan = 0;
		$TotalDiskonPemasukan = 0; */
		
		$TotalPengeluaran = 0;
		/* $TotalPPHPengeluaran = 0;
		$TotalPPNPengeluaran = 0;
		$TotalDiskonPengeluaran = 0; */
		
		if($list->num_rows() > 0) {
			foreach($list->result_array() as $row) {
				$TotalPemasukan += $row['total_pemasukan'];
				$TotalPengeluaran += $row['total_pengeluaran'];
			}
		}

	// Instanciation of inherited class
		$pdf = new fpdf('P','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$title_bulan = '';
		if($bulan == date('m'))
			$title_bulan = date('M').' - ';
		
		$pdf->sety(3);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(185, 7, 'Laporan Laba Rugi', 0, 1,'C');
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(185, 5, $title_bulan.$tahun, 0, 1,'C');
		$pdf->ln();
		/* $pdf->setx(22);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(50, 7, 'Pendapatan', 0, 1,'L'); */
		$pdf->setx(11);
		$pdf->Cell(7, 7, 'No', 1, 0,'L');
		$pdf->Cell(20, 7, 'Tgl', 1, 0,'C');
		$pdf->Cell(110, 7, 'No Transaksi', 1, 0,'C');
		$pdf->Cell(25, 7, 'Pemasukan', 1, 0,'C');
		$pdf->Cell(25, 7, 'Pengeluaran', 1, 1,'C');
		
		if($list->num_rows() > 0) {
			$no=1;
			/* $Pemasukan = 0;
			$Pengeluaran = 0; */
			foreach($list->result_array() as $row) {
				/* if($row['fidJenisKas'] == 1) {
					$Pemasukan = $row['Biaya'];
					$Pengeluaran = 0;
				} elseif($row['fidJenisKas'] == 2) {
					$Pengeluaran = $row['Biaya'];
					$Pemasukan = 0;
				} */

				$pdf->SetFont('Arial','',8);
				$pdf->setx(11);
				$pdf->Cell(7, 5, $no, 1, 0,'L');
				$pdf->Cell(20, 5, humanize_mdate($row['TglInvoice'], '%d-%m-%Y'), 1, 0,'C');
				$pdf->Cell(110, 5, $row['NoInvoice'], 1, 0,'L');
				$pdf->Cell(25, 5, thausand_spar($row['total_pemasukan']), 1, 0,'R');
				$pdf->Cell(25, 5, thausand_spar($row['total_pengeluaran']), 1, 1,'R');
				// $pdf->Cell(25, 5, thausand_spar($Pengeluaran), 1, 1,'R');
										
				$no++;
			}
		} 
		
		$pdf->SetFont('Arial','',8);
		$pdf->setx(11);
		$pdf->Cell(137, 5, 'Total', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPemasukan), 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPengeluaran), 1, 1,'R');
		/* $pdf->setx(22);
		$pdf->Cell(137, 5, 'Total Diskon', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalDiskonPemasukan), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total PPN', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPPNPemasukan), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total PPH', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPPHPemasukan?$TotalPPHPemasukan*-1:0), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total Pendapatan', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPemasukan-$TotalDiskonPemasukan+$TotalPPNPemasukan-$TotalPPHPemasukan), 1, 1,'R'); */
		// $pdf->Cell(25, 5, thausand_spar($TotalPengeluaran), 1, 0,'R');
		/* 
		$pdf->ln(10);
		$pdf->setx(22);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(50, 7, 'Pengeluaran', 0, 1,'L');
		$pdf->setx(22);
		$pdf->Cell(7, 7, 'No', 1, 0,'L');
		$pdf->Cell(20, 7, 'Tgl', 1, 0,'C');
		$pdf->Cell(110, 7, 'Nama Biaya', 1, 0,'C');
		// $pdf->Cell(25, 7, 'Pemasukan', 1, 0,'C');
		$pdf->Cell(25, 7, 'Pengeluaran', 1, 1,'C');
		
		if($list_keluar->num_rows() > 0) {
			$no=1;
			$Pemasukan = 0;
			$Pengeluaran = 0;
			foreach($list_keluar->result_array() as $row) {
				

				$pdf->SetFont('Arial','',8);
				$pdf->setx(22);
				$pdf->Cell(7, 5, $no, 1, 0,'L');
				$pdf->Cell(20, 5, humanize_mdate($row['TglOrder'], '%d-%m-%Y'), 1, 0,'C');
				$pdf->Cell(110, 5, $row['NamaBiaya'], 1, 0,'L');
				$pdf->Cell(25, 5, thausand_spar($row['Biaya']), 1, 1,'R');
				// $pdf->Cell(25, 5, thausand_spar($Pengeluaran), 1, 1,'R');
										
				$no++;
			}
		} 
		
		$pdf->SetFont('Arial','',8);
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total', 1, 0,'R');
		// $pdf->Cell(25, 5, thausand_spar($TotalPemasukan), 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPengeluaran), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total Diskon', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalDiskonPengeluaran), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total PPN', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPPNPengeluaran), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total PPH', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPPHPengeluaran ? $TotalPPHPengeluaran*-1 : 0), 1, 1,'R');
		$pdf->setx(22);
		$pdf->Cell(137, 5, 'Total Pengeluaran', 1, 0,'R');
		$pdf->Cell(25, 5, thausand_spar($TotalPengeluaran-$TotalDiskonPengeluaran+$TotalPPNPengeluaran-$TotalPPHPengeluaran), 1, 1,'R');
				 */
		$pdf->Output();	
		
	}
}