<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>No PO </th>
			<th>Tgl PO </th>
			<th>Supplier </th>
			<th>No Faktur </th>
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Harga Beli</th>
			<th>Diskon</th>
			<th>PPN</th>
			<th>Sub Total</th>
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?>
			<tr>
				<td><?= $no ?></td>
				<td><?= match_key($row['NoPembelian'],$key['key'])?></td>
				<td><?= humanize_mdate($row['TglPembelian'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NamaSupplier'],$key['key'])?></td>
				<td><?= match_key($row['NoFaktur'],$key['key'])?></td>
				<td><?= match_key($row['NamaBarang'],$key['key'])?></td>
				<td><?= $row['Qty'] ?></td>
				<td><?= thausand_spar($row['HargaBeli']) ?></td>
				<td><?= $row['Diskon'] ?></td>
				<td><?= $row['PPN'] ?></td>
				<td><?= thausand_spar($row['SubTotal']) ?></td>
			</tr>
	<?php }?>
			<tr>
				<th colspan='10' class='text-right'>Total</th>
				<th><?php echo thausand_spar($total) ?></th>
			</tr>
		</tbody>
</table>
<?=$paging['list']?>