<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_po_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('ho');
		$this->set_table('trPO');
		$this->set_pk('idPO');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');
		
		$this->db->select('kenda.NamaKendaraan');
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
				
		$this->db->select('sup.NamaSupplier');
		$this->db->join('dataMaster.msSupplier sup', 'tbl.fidSupplier = sup.idSupplier', 'left');
		
		$this->db->select('sts.NamaStatusPO');
		$this->db->join('dataMaster.msStatusPO sts', 'tbl.fidStatusPO = sts.idStatusPO', 'left');
				
		$this->db->select('det.*');
		$this->db->join('ho.trPODetail det', 'tbl.idPO = det.fidPO', 'left');
		
		$this->db->select('brg.NamaBarang');
		$this->db->join('dataMaster.msBarang brg', 'brg.KodeBarang = det.KodeBarang', 'left');

		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msSupplier sup', 'tbl.fidSupplier = sup.idSupplier', 'left');
		$this->db->join('ho.trPODetail det', 'tbl.idPO = det.fidPO', 'left');
		$this->db->join('dataMaster.msBarang brg', 'brg.KodeBarang = det.KodeBarang', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}
	
	function total_nominal()
	{
		$this->db->select_sum('SubTotal', 'TotalSubTotal');
				
		$this->db->join('dataMaster.msKendaraan kenda', 'tbl.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msSupplier sup', 'tbl.fidSupplier = sup.idSupplier', 'left');
		$this->db->join('ho.trPODetail det', 'tbl.idPO = det.fidPO', 'left');
		$this->db->join('dataMaster.msBarang brg', 'brg.KodeBarang = det.KodeBarang', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['TotalSubTotal']?:0;
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
