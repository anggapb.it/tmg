<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('transfer.kain/transfer_kain_model');
		
	}

	public function index()
	{
		echo 'end';
	}
	
	function get_notif() {
		$privTotalTransferKain = $this->menu->msOperatorSpecial(array('SpecialVar' => 'set_notif_terima_kain_internal'));//check_priv('NotifReqRegCustomer');
		
		$tfkain_count = $this->transfer_kain_model->get_total_request();
		$data['TotalTransferKain'] = ISSET($tfkain_count['TotalTransferKain'])?$tfkain_count['TotalTransferKain']:0;
		$data['privTotalTransferKain'] =	$privTotalTransferKain;
		
		echo json_encode($data);
	}
}