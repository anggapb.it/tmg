<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
	   	if (is_logged_in())
		{
			redirect(base_url());
		}else
		{
			$data['title'] = 'Login';
			$this->load->view('login/login_form',$data);
		}
	}
	
	function window()
	{
		echo 'LogedIn';
	}
	function signin()
	{
		$comp['allow_access'] = true;
		if (!$comp['allow_access'])
		{
			$result['error'] = 'Check Jaringan Anda';
			echo json_encode($result);			
		}else
		{
			$error = '';
			$data['username'] = strtoupper($this->input->post('t_username'));
			$data['password'] = $this->input->post('t_password');
			
			if (!$data['username'])
				$error .= 'Username tidak boleh kosong, ';
			if (!$data['password'])
				$error .= 'Password belum diisi, ';
			if (!$error)
			{
				$this->load->library('auth');
				$return = $this->auth->do_login($data);
				if($return['login'])
				{
					$error = '';
				}else
				{
					$error = $return['message'];
				}
			}
			$result['error'] = $error;
			echo json_encode($result);
		}
	}
	
	function signout()
	{
		$this->auth->logout();
		redirect(site_url());
	}
}

