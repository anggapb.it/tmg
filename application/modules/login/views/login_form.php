<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
	<link rel="shortcut icon" href="<?= base_url().get_myconf('ConfigKeyValue7'); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
	<link href="<?= base_url();?>assets/LTE/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
	<link href="<?= base_url();?>assets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
	<link href="<?= base_url();?>assets/LTE/dist/css/AdminLTE.css" rel="stylesheet" />
    <!-- iCheck -->
	<link href="<?= base_url();?>assets/LTE/plugins/iCheck/square/blue.css" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <script>
  var site_url = "<?= site_url().get_language()?>/";
</script>
<style>
@media (min-width:768px) and (max-width:1366px){
	.visible-small{
		display:none!important
	}
	.visible-large{
		position: fixed;
		bottom: 0px;
		right: 60px;
		float: right!important;
	}
	.hidden-medium{
		display:none!important
	}
}
@media (max-width:767px){
	.visible-medium{
		display:none!important
	}
	.visible-large{
		
	}
	.hidden-small{
		display:none!important
	}
}

</style>
<script type='text/javascript'>
  function login()
  {
    $.post(site_url+"login/signin"
          ,{t_username: $('#t_username').val()
            ,t_password: $('#t_password').val()
            }
          ,function(result) {
            if (result.error)
            {
              toastr.error(result.error);
            }else
            {
              window.location = site_url;
            }
          }         
          ,"json"
        );
    
  } 
  function forgetpass()
  {
  	toastr.warning(" hint : <br> username : adminepo <br>password : (password biasa ict..)<br> (sama dgn password beon utilities)");
  }
</script>
  <body class="hold-transition login-page" 
  style="background-image: url(<?= base_url().get_myconf('ConfigKeyValue6'); ?>);
        background-repeat-x: no-repeat;
        background-position: center;">
    <div class="login-box visible-large">
      <div class="login-box-body" style="background-color: rgba(255,255,255,0.6)">
      <div class="login-logo">
		welcome to <br><b><?= get_myconf('ConfigKeyValue5'); ?></b>
      </div><!-- /.login-logo --> 
          <div class="form-group has-feedback">
            <input type="text" id="t_username" onkeydown="if (event.keyCode == 13) login()" class="form-control" placeholder="username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" id="t_password" onkeydown="if (event.keyCode == 13) login()" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button onclick="login();" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        <!--a href="#" onclick="forgotpass()">I forgot my password</a--><br>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
	
	<script src="<?= base_url();?>assets/LTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?= base_url();?>assets/LTE/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>assets/LTE/plugins/iCheck/icheck.min.js"></script>
    <!-- Toastr style -->
    <link href="<?= base_url();?>assets/LTE/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Toastr -->
    <script src="<?= base_url();?>assets/LTE/plugins/toastr/toastr.min.js"></script>
  
    <script>
      $(function () {
        // untuk memastikan bahwa login masih aktif
        // jika yang dibalikan dari controller adalah window login, maka kita refresh ulang
        var windTitle = document.title ;
        if (windTitle != 'Login')
        // if (document.title != 'Login')
        {
            location.reload();
        }
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
