<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperatorGroup');
		$this->set_pk('idMsOperatorGroup');
		// $this->set_log(true);
    }	
		
	function get_list_user_count()
	{
		$this->db->select('t_ug.*');
		$this->db->select('q_usr."userCount"');
		//join
		$sql = '(select "fidMsOperatorGroup",count(*) as "userCount"
				from "public"."msOperator"
				group by "fidMsOperatorGroup")';
		$this->db->join($sql.' q_usr','q_usr.fidMsOperatorGroup = t_ug.idMsOperatorGroup','left');
		//
		$this->db->where($this->where);
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}
		$this->db->order_by($this->pk_field,'asc');
		
		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->table.' t_ug');
		else
			$query = $this->db->get($this->table.' t_ug',$this->limit,$this->offset);
		
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}

	function get_defaultMenu($id){
		$this->db->select('tbl.DefaultMenu, tbl.DefaultShortcut');
		$this->db->where('idMsOperatorGroup',$id);
		return $this->db->get($this->schema.'.'.$this->table.' tbl',1)->row();
	}

}

/* End of file zona_model.php */
/* Location: ./application/modules/master/models/zona_model.php */
