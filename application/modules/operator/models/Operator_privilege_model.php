<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_privilege_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperatorPrivilege');
		$this->set_pk('idPrivilege');
		$this->set_log(false);
    }	

}
