<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_access_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('msStoreAccess');
		$this->set_pk('idMsStoreAccess');
		// $this->set_log(true);
    }	
		
	function count() 
	{
		$data = array();
		$this->db->select('count(*) as num_rows');
		$this->db->where($this->where);

		$query = $this->db->get($this->table.' tbl');
		$row = $query->row_array();
		return $row['num_rows'];
	}
	
    function get_list()
	{
		$this->db->select('tbl.*');
		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->table.' tbl');
		else
			$query = $this->db->get($this->table.' tbl',$this->limit,$this->offset);
		
		// echo $this->db->last_query();
		// exit;

		$this->db->order_by('StoreCode', 'DESC');

        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}

	function getAll(){
		$this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by('idMsStoreAccess','ASC');
        $query = $this->db->get();

        return $query->result();
    }

	function get_store_type($idmsoperator) 
	{   
		$this->load->model('store_type_model');
		$store_type = $this->store_type_model->get_all_data();
		foreach($store_type->result() as $row){
		$query_check = "(SELECT tbl.\"StoreCode\" 
					FROM \"msStoreAccess\" tbl
					LEFT JOIN \"msStore\" str ON tbl.\"StoreCode\" = str.\"StoreCode\"
					WHERE tbl.\"fidMsOperator\" = '$idmsoperator'
					AND str.\"fidmsStoreType\" = '$row->idmsStoreType'
					)";
		}
		$query = $this->db->get($query_check.' tbl');
		// echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
        
    }
	function get_store_access_count() 
	{   
		$data = array();
		$this->db->select('count(*) as num_rows');
		$this->db->where($this->where);

		$query = $this->db->get($this->table.' tbl');

		// echo $this->db->last_query();
		// exit;

		$row = $query->row_array();
		return $row['num_rows'];
		
    }
	function get_store_access() 
	{   
		$this->db->select('tbl.*');
		//store
		$this->db->select('str.Description,str.Brand,str.Address');
		$this->db->join('msStore str','tbl.StoreCode = str.StoreCode','LEFT');
		
		if($this->where)
			$this->db->where($this->where);
		
		$this->db->order_by('tbl.StoreCode', 'DESC');
		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->table.' tbl');
		else
			$query = $this->db->get($this->table.' tbl',$this->limit,$this->offset);
		
		// echo $this->db->last_query();
		// exit;

		// $query = $this->db->get($query_check.' tbl');
		// echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
        
    }
}

/* End of file zona_model.php */
/* Location: ./application/modules/master/models/zona_model.php */
