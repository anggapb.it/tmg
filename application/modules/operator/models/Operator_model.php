<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperator');
		$this->set_pk('idMsOperator');
		$this->set_log(true);
    }	

}
