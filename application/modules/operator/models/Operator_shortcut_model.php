<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_shortcut_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperatorAppShortCut');
		$this->set_pk('idMsOperatorAppShortKey');
		$this->set_log(false);
    }	

}
