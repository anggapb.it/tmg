<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('operator_model');
		$this->load->model('user_group_model');
	}

	public function index()
	{	
		$id_operator = $this->session->userdata('Operator')['idMsOperator'];
		// $user_group_count = $this->user_group_model->count();
		$user_group_list = $this->user_group_model->get_list_user_count();
		$user_count = $this->operator_model->count();
		$data = array();
		$data['content'] = 'main';
		$data['user_group_list'] =  $user_group_list;
		$data['user_count']= $user_count;	
		//
		$this->load->view($data['content'],$data);
	}

	function page($pg=1)
	{
		$loader_name 	= $this->input->post('loader_name');
		$limit 			= $this->input->post('limit');
		$search_key		= strtoupper($this->input->post('t_search_key'));
		$group_id		= $this->input->post('t_group_id');
		echo (	$search_key);
		// binding data
		$key['search_key'] = '';
		$like = array();
		if($search_key)
		{
			// fullname
			$like['UPPER(tbl."FullName")'] = $search_key;
			$key['FullName'] = $search_key;
			// fullname
			$like['UPPER(tbl."LoginName")'] = $search_key;
			$key['LoginName'] = $search_key;
			
		}	
		$this->user_model->set_like($like);

		$where = array();
		// group
		if($group_id)
		{
			$where['fidMsOperatorGroup']=$group_id;
		}
		$this->user_model->set_where($where);
		$list_all=$this->user_model->get_list();
		$c_list = $list_all->num_rows();
		$count = ($c_list)?$c_list:0;
		
		// binding data
		if ($limit)
		{
			$this->user_model->set_limit($limit);
			$this->user_model->set_offset($limit * ($pg - 1));
		}
		// $order['epo.msOperatorEPO.idMsOperator'] = 'asc';
		// $this->user_model->set_order($order);

		$list=$this->user_model->get_list();

		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $count;
		$page['current'] 	= $pg;
		$page['load_func_name'] = $loader_name;
		$page['list'] 		= $this->gen_paging($page);

		// data binding
		$data = array();
		$data['content'] = 'list';
		$data['user_list'] =  $list;
		$data['paging']= $page;	
		$data['key']= $key;	

		// show
		$this->load->view($data['content'],$data);
	}

	function input($id='')
	{
		$id = $id?decode($id):0;
		$user = $this->user_model->get($id);
		$user_group_list = $this->user_group_model->get_list_user_count();

		// data binding
		$data = array();
		$data['content']			= 'user/input';
		$data['user']				= $user;
		$data['user_group_list'] 	=  $user_group_list;
		
		// show
		$this->load->view($data['content'], $data);
	}

	function save()
	{		
		$data = array();
		$id_operator		 		= (decode($this->input->post('t_id_operator'))?:0);
		$data['FullName'] 			= $this->input->post('t_full_name');
		$data['LoginName'] 			= $this->input->post('t_login_name');
		$data['StoreCode'] 			= $this->input->post('t_store_code');
		$data['ExpiryDate'] 		= getSQLDate($this->input->post('t_expiry_date'));
		$data['StoreCodeDefault'] 	= $this->input->post('t_store_code_default');
		$data['Email'] 				= $this->input->post('t_email');
		$data['MitraCode'] 		= $this->input->post('t_CustCode');
		$password = $this->input->post('t_login_pass');
		$password2 = $this->input->post('t_login_pass2');
		
		
		$nChar = strlen($password)+1;
		$cPassGen = $password;
		$x = 1; 
		while($x <= $nChar) 
		{
			$cPassGen = md5($cPassGen);
			$x++;
		}

		if($password != ""){

			$data['LoginPass']	= $cPassGen;
		}
		
		if (!$data['FullName'])
		{
			$this->error('FullName is required');		
		}

		if ($password != $password2)
		{
			$this->error('Please enter the same password as above');		
		}

		if (!$data['LoginName'])
		{
			$this->error('LoginName is required');		
		}

		if($id_operator == "")
		{
			if (!$password)
			{
				$this->error('LoginPass is required');		
			}
		}

		if (!$data['ExpiryDate'])
		{
			$this->error('ExpiryDate is required');		
		}
		
		$select_store = $this->input->post('t_storeCode');
		$loginname = $this->input->post('t_login_name');

		$mode = 1;
		if($id_operator == ""){
			$q_cek = $this->db->query("SELECT \"idMsOperator\" FROM epo.\"msOperatorEPO\" ORDER BY \"idMsOperator\" DESC LIMIT 1");
			$r_cek = $q_cek->row();
		$mode = 0;
			$id_operator = $r_cek->idMsOperator+1;
		}		
		$data['idMsOperator'] = $id_operator;
			
		// save
		$save = "";
		$metode = "";
		if($mode==1){
		$this->db->where('idMsOperator', $id_operator);
		$save = $this->db->update('epo."msOperatorEPO"', $data); 
		$metode = "update";
		}else{
		$this->db->set($data);
		$save = $this->db->insert('epo."msOperatorEPO"'); 
		$metode = "tambah";
		}
		
		// $save = $this->user_model->save($data);
		
		if ($save)
		{
			// $this->update['id_operator'] = encode($save);
			$this->update['id_operator'] = encode($id_operator);
			$this->success('Data telah di'.$metode.'. ');
		}else
		{
			$this->error('Proses gagal dijalankan. ');
		}
	}
	

	function delete($id)
	{
		//$id = decode($id);
		$query	= $this->user_model->delete($id);
		$this->user_model->deleteStoreCode($id);
		$this->user_model->deletePrivilege($id);
	        
	        
		if ($query)
			$this->success('Data telah dihapus....');
		else
			$this->error('Proses gagal dijalankan....');
	}
	
	function createEpo()
	{
		$season_tahun = substr(date('Y'),2,2);
		$season_bulan = date('m');
		$this->db->trans_start();
		$mitra = decode($this->input->post('t_mitra'));
		if(!$mitra){
			$this->error('Kode Mitra tidak tersedia....');
		}
		$cek_trans_epo = $this->epo_model->cek_transaksi_season($mitra,date('Y'));
		$cek_mitra = isset($cek_trans_epo['MitraCode'])?$cek_trans_epo['MitraCode']:'';
		if($cek_mitra){
			$this->error('Data sudah ada....');
		}
		$data = array();
		$data['MitraCode']	= $mitra;
		$data['Tahun']	= date('Y');
		//[kuartal]
		$data['Kuartal']	= 3;
		$data['TransNum']	= 'REG.'.$season_tahun.$season_bulan.'.'.$mitra;
		$query	= $this->epo_model->save($data);
		// echo $this->db->last_query();
		// exit;
		
		
	    if ($this->db->trans_status() === true){
			$this->db->trans_complete();
			$this->success('Registrasi berhasil....');
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
			$this->error('Proses gagal....');
		}
	}
	function closingEpo()
	{
		$season_tahun = substr(date('Y'),2,2);
		$season_bulan = date('m');
		$this->db->trans_start();
		$mitra = decode($this->input->post('t_mitra'));
		if(!$mitra){
			$this->error('Kode Mitra tidak tersedia....');
		}
		$cek_mitra = $this->mitra_model->cek_mitra($mitra);
		$closepo = isset($cek_mitra['ClosingPo'])?$cek_mitra['ClosingPo']:'';
		$date_close_po = strtotime($closepo);
		$date_now = strtotime(date('Y-m-d H:i:s'));
		$close_po = $date_close_po < $date_now;
		if($close_po){
			$this->error('Data sudah Closing....');
		}
		$this->db->update('epo."msMitra"',array('ClosingPo' => date('Y-m-d H:i:s')),array('MitraCode' => $mitra));
		// echo $this->db->last_query();
		// exit;
		
		
	    if ($this->db->trans_status() === true){
			$this->db->trans_complete();
			$this->success('Closing berhasil....');
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
			$this->error('Proses gagal....');
		}
	}
	
	function allow_access()
	{
		$backend = $this->input->post('t_app_type1');
		$frontend = $this->input->post('t_app_type2');
		$allowbackend = $this->input->post('t_allow1');
		$allowfrontend = $this->input->post('t_allow2');
		echo ($backend);
		echo ($allowbackend);
		exit;
		$data = array();
		$data['idMsOperator']	= decode($this->input->post('t_id_operator'));
		if($backend){
			if($allowbackend){
				$data['BackEnd']	= 1;
			}else{
				$data['BackEnd']	= 0;
			}
		}
		if($frontend){
			if($allowfrontend){
				$data['FrontEnd']	= 1;
			}else{
				$data['FrontEnd']	= 0;
			}
		}
		$this->user_priv_model->save($data);
		    
		if ($query)
			$this->success('Data telah dihapus....');
		else
			$this->error('Proses gagal dijalankan....');
	}

	function changePassword()
	{
		$data['idMsOperator'] 	= decode($this->input->post('t_idMsOperator'));
		$data['oldPassword'] 	= $this->input->post('t_oldPassword');
		$data['LoginPass'] 		= $this->input->post('t_newPassword');
		$data['confPassword'] 	= $this->input->post('t_confPassword');

		//ambil password lama
		$qpl = $this->db->query("SELECT \"LoginPass\" FROM epo.\"msOperatorEPO\" WHERE \"idMsOperator\" = '".$data['idMsOperator']."'");
		$rpl = $qpl->row();
		$passLamaEnc = $rpl->LoginPass;

		//decode passlama
		$nCharLama = strlen($data['oldPassword'])+1;
		$cPassGenLama = $data['oldPassword'];
		$xl = 1; 
		while($xl <= $nCharLama) 
		{
			$cPassGenLama = md5($cPassGenLama);
			$xl++;
		}

		if (!$data['oldPassword'])
		{
			$this->error('Old Password is required');		
		}

		if (!$data['LoginPass'])
		{
			$this->error('New Password is required');		
		}

		if (!$data['confPassword'])
		{
			$this->error('Confirm Password is required');		
		}

		if ($data['LoginPass'] != $data['confPassword'])
		{
			$this->error('Please enter the same password as above');		
		}

		if ($passLamaEnc != $cPassGenLama)
		{
			$this->error('Old password not valid');		
		}

		$password = $this->input->post('t_newPassword');
		
		$nChar = strlen($password)+1;
		$cPassGen = $password;
		$x = 1;
		while($x <= $nChar) 
		{
			$cPassGen = md5($cPassGen);
			$x++;
		}

		if($password != ""){

			$data['LoginPass']	= $cPassGen;
		}

		$save = $this->user_priv_model->save($data);

		if ($save)
		{
			$this->success('Data telah disimpan. ');
		}else
		{
			$this->error('Proses gagal dijalankan. ');
		}

	}
}

/* End of file user.php */
/* Location: ./application/modules/user/controllers/user.php */