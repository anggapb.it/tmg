<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('user_model');
		$this->load->model('user_group_model');
	}

	public function index()
	{	
		$id_operator = $this->session->userdata('Operator')['idMsOperator'];
		// $user_group_count = $this->user_group_model->count();
		// $user_group_list = $this->user_group_model->get_list_user_count();
		// $user_count = $this->user_model->count();
		$data = array();
		$data['content'] = 'profile/main';
		// $data['user_group_list'] =  $user_group_list;
		// $data['user_count']= $user_count;	
		//
		$this->load->view($data['content'],$data);
	}

	
}

/* End of file user.php */
/* Location: ./application/modules/user/controllers/Profile.php */