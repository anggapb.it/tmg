<script type='text/javascript'>
	$(document).ready(function(){
		setTitle('<?= $title;?>');
		showList(1);
		hideProgres();
		//$('#date1').daterangepicker();
		$('#tgl_1_content').datepicker();
		$('#tgl_2_content').datepicker();
		$('.chzn-select').chosen();

	});

	function showList(pg)
	{
		showProgres();
		$.post(site_url+"master/user/page/"+pg
			,$("#form_filter").serialize()
			,function(result) {
				$('#list-data').html(result);
				
				var sum_row = $("#row_per_page").val();

				var current = $('#current').val();
				
				if(current == 'undefined') 
				{
					showList(1);						
				}
				hideProgres();

			}					
			,"html"
		);	
	}

	function deleteConfirm(id)
	{
		bootbox.confirm('Anda yakin data : <br>'+$('#kode_'+id).val()+' akan di Hapus?', 
			function(e){
				if (e){
					$.post(site_url+"master/user_priv/delete/"+id
						,{}
						,function(result) {
							if (result.error)
							{
								pesan_error(result.error);
							}else
							{
								pesan_success(result.message);
								loadList('master/user_priv');
							}
						}					
					,"json"
					);
					}
				}); 
	}

	function inputData(id)
	{
		loadInput('master/user_priv/input/'+id);
	}

	function loadList(url)
	{
		showProgres();
		$.post(site_url+url
				,{}
				,function(result) {
					$('#list-data').html(result);
					$('#main-view').show();
					$('#main-input').hide();	
					$('#input-view').hide();
					
					hideProgres();
				}					
				,"html"
			);
	}
	function loadInput(url)
	{
		showProgres();
		$.post(site_url+url
			,{}
			,function(result) {
				$('#main-input').html(result);
				$('#input-view').show();
				$('#main-view').hide();
				
				hideProgres();
			}					
			,"html"
		);
	}
	function backToList()
	{
		$('#input-view').hide();
		$('#main-view').show();
		$('#detail-input').hide();
	
	}
	function backToMain()
	{
		$('#list-view').hide();
		$('#input').hide();

	}
	function storeAccess(id)
	{
		loadInput('master/user_priv/store_access/'+id);
	}
</script>
<!-- modal filter -->
<div class="modal hide fade" scrolllock="true" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 800px">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h3 id="myModalLabel1">Search</h3>
    </div>
	<div class="modal-body">
        <form id="form_filter">
			<input name="loader_name" type="hidden" value="showList">
			<div class="tabbable portlet-tabs">
				<div class="content">
					<div class="row-fluid">
						<div class="span5">
							<label class="control-label span5">Show</label>
							<div class="controls span6">
							<select class="span8" size="1" aria-controls="dt_gal" id="limit" name="limit">
								<option value="5">5</option>
								<option value="10" >10</option>
								<option value="25" selected="selected">25</option>
								<option value="50">50</option>
								<option value="100">100</option>
							</select> entries
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span5">
							<label class="control-label span5">Full Name  </label>
							<div class="controls span6">
								<input type="text" name="t_fullname_search">
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span5">
							<label class="control-label span5">Login Name  </label>
							<div class="controls span6">
								<input type="text" name="t_login_name">
							</div>
						</div>
					</div>
					
				</div>				
			</div>	
		</form>
    </div>
	
    <div class="modal-footer">
        <div class="form-action">
			<a href="#" class="btn btn-info" data-dismiss="modal" onclick="showList(1);">Browse</a>
		</div>
    </div>
</div>
<!-- end modal filter -->
<div class="widget widget-tabs green" id="main-view">
	<div class="widget-title">
		<h4><i class="icon-reorder"></i>Data Users</h4>
	</div>
	<div class="widget-body">
		<div class="tabbable ">
			<ul class="nav nav-tabs">
				<li><a onclick="showList(1);" title="Refresh Tampilan"href="javascript:;"><i class="icon-refresh"></i></a></li>
				<li><a onclick="loadInput('master/user_priv/input')" title="Tambah" href="#"><i class="icon-plus"></i></a></li>
				<li><a href='#myModal' data-toggle="modal" title="Filter Data" data-backdrop="static"><i class="icon-search"></i></a></li>
			</ul>
			<div class="row-fluid">
				<div class="tab-content">
					<div class="tab-pane active" id="list-data"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="main-input"></div>