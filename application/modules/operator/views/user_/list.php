<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Full Name</th>
             <th style="width: 100px;">Login Name</th>
            <th>Store Code</th>
             <th style="width: 80px;">Expiry Date</th>
            <th>Store Code Default</th>
            <th style="width: 116px;"></th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$no = ($paging['current']-1) * $paging['limit'] ;
		$no++;
		
		if($list->num_rows() > 0){
			foreach($list->result_array() as $row){
                $id_store_def = $row['StoreCodeDefault'];
        ?>
            <input class="span4 " id='kode_<?= $row['idMsOperator']?>' type="hidden" value="<?= $row['idMsOperator']?>"/>
    	<tr>
    		<td><?= $no++;?></td>
    		<td><?= match_key($row['FullName'],$key['FullName']); ?></td>
    		<td><?= match_key($row['LoginName'],$key['LoginName']); ?></td>
            <td><?= $row['StoreCode'].' - '.$row['StoreName']; ?></td>
            <td><?= humanize_mdate($row['ExpiryDate']); ?></td>
    		<td><?= $row['StoreCodeDefault'].' - '.$row['StoreCodeDefaultDesc'] ?>        
            </td>
            <td>
                <a href="#" onclick="loadInput('master/user_priv/input/<?= encode($row['idMsOperator'])?>')" class="btn btn-primary"><i class="icon-pencil"></i></a>
                <a href="#" onclick="deleteConfirm('<?= $row['idMsOperator']?>')" class="btn btn-danger"><i class="icon-remove"></i></a>
                <a href="#" onclick="storeAccess('<?= $row['idMsOperator']?>')" class="btn btn-warning"><i class="icon-home "></i></a>
            </td>
    	</tr>
    	<?php }
    		}
    	?>
    </tbody>
</table>
<div class="form-actions">
	<?= $paging['list']?>
</div>