<table class="table table-striped table-bordered table-advance table-hover">
	<thead>
		<tr>
			<th width="10px"><input type="checkbox" onClick="toggle_store(this)" /></th>
			<th>Store</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($list_store->result() as $row){
				$query_check = $this->db->query("SELECT tbl.\"StoreCode\" 
												FROM \"msStoreAccess\" tbl
												WHERE tbl.\"fidMsOperator\" = '$loginname'
												");
		?>
		<tr>
			<td>
				<input type="checkbox" 
					name="t_storeCode[]" 
					value="<?= $row->StoreCode; ?>"
					<?php 
					foreach ($query_check->result() as $rc)
					{
						$rs = $row->StoreCode;
						if($rs == $rc->StoreCode){ echo 'checked="checked"';} 
					}
					?>
				>
			</td>
			<td><?= $row->StoreCode.' - '.$row->Description ?></td>
		</tr>
		<?php }
		?>
	</tbody>
</table>