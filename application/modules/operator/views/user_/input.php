<?php  
	$loginname = rtrim($list['idMsOperator']) ? rtrim($list['idMsOperator']) : 0;
?>
<script type='text/javascript'>
	$(document).ready(function(){
		$('#tgl_1_content').datepicker();
		$('.chzn-select').chosen();
		EditableTable.init();
		// pageLoadStore(10);
		// getMenu(1);
		// $('#backEnd').hide();
		// $('#frontEnd').hide();
		<?php if($list['BackEndAccess']==1){?>
				$('#backEnd').show();
				$('#allow1').show();
		<?php }else{?>
				$('#backEnd').hide();
				$('#allow1').hide();
		<?php }?>
		$('#frontEnd').hide();
		$('#allow2').hide();
	});
	function pageLoadStore(idmstoretype)
	{
		$.post(site_url+"master/user_priv/page_store/"
			,{t_store_type : idmstoretype
				,t_login : <?=rtrim($list['idMsOperator'])?:0?>}
			,function(result) {
					<?php foreach($store_type->result_array() as $str_tp){?>
					$('#resultContent<?= $str_tp['idmsStoreType']?>').html(result);
					<?php }?>
			}					
			,"html"
		);
	}
	function dataSave()
	{
		$.post(site_url+'master/user_priv/save'
				,$("#form").serialize()
				,function(result) {
					if (result.error)
					{
						pesan_error(result.error);
					}else
					{
						pesan_success(result.message);
						loadInput('master/user_priv/input/'+result.id_operator);
					}
				}					
				,"json"
			);
	}
	function allowAccess()
	{
		$.post(site_url+'master/user_priv/allow_access'
				,$("#form").serialize()
				,function(result) {
					if (result.error)
					{
						pesan_error(result.error);
					}else
					{
						pesan_success(result.message);
						// loadInput('master/user_priv/input/'+result.id_operator);
					}
				}					
				,"json"
			);
	}

	function dataSucces(id)
	{
		// window.location = site_url+'master/report_dis/input/'+id;
		loadList('master/user_priv/input/'+id);
	}
	function dataReset()
	{
		 window.location.reload();
	}

	//check all
	function toggle(source) {
	  checkboxes = document.getElementsByName('t_idAppMenu[]');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
	
	//check all
	function toggle_store(source) {
	  checkboxes = document.getElementsByName('t_storeCode[]');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
	function getMenu(app_type)
	{
		// $.post(site_url+"master/user_priv/get_menu/"
			// ,{t_id_operator : id_operator
				// ,t_app_type : app_type}
			// ,function(result) {
					// $('#resultContentMenu').html(result);
			// }					
			// ,"html"
		// );
		if(app_type==1){
			// show();
			<?php if($list['BackEndAccess']==1){?>
				$('#backEnd').show();
			<?php }else{?>
				$('#backEnd').hide();
			<?php }?>
			$('#frontEnd').hide();
			$('#dashboard').hide();
			$('#allow1').show();
			$('#allow2').hide();
			$('#t_app_type1').val(1);
			
		}else if(app_type==2){
			<?php if($list['FrontEndAccess']==1){?>
				$('#frontEnd').show();
			<?php }else{?>
				$('#frontEnd').hide();
			<?php }?>
			$('#backEnd').hide();
			$('#dashboard').hide();
			$('#allow2').show();
			$('#allow1').hide();
			$('#t_app_type2').val(1);
		}else if(app_type==3){
			$('#frontEnd').hide();
			$('#backEnd').hide();
			$('#dashboard').show();
			$('#allow2').hide();
			$('#allow1').hide();
			$('#t_app_type3').val(1);
		}
	}
	// function allowMenu(allowType)
	// {
		// if(allowType==1){
			// $('#allow1').show();
			// $('#allow2').hide();
		// }else{
			// $('#allow1').hide();
			// $('#allow2').show();
		// }
	// }
	function checkAllow(tp)
	{
		var allow= document.getElementById("t_allow"+tp).checked;
		if(tp==1){
			if(allow == true ){
				$('#backEnd').show();
				$('#frontEnd').hide();
			}else{
				$('#backEnd').hide();
			}
		}else{
			if(allow == true ){
				$('#frontEnd').show();
				$('#backEnd').hide();
			}else{
				$('#frontEnd').hide();
			}
		}
	}
	
</script>
<script>
// var site_url = "<?php echo site_url();?>";
function load_uri(uri,dom)
{
    $.ajax({
        url: site_url+uri,
        success: function(response){            
        $(dom).html(response);
        },
    dataType:"html"         
    });
    return false;
}

function show_extra_combo(combo,combo_level)
{
    var id = $(combo).val();
    // buat dom '.combo-level' di dalam extra-combo jika belum ada
    var domcombo = 'combo-'+combo_level;
    if($('.'+domcombo).length == 0)
    {
        $('#extra-combo').append('&nbsp;<div class="'+domcombo+'"></div>');
    }
    load_uri("master/user_priv/show_customer/"+id+"/"+combo_level,'.'+domcombo);
}

$.event.special.inputchange = {
    setup: function() {
        var self = this, val;
        $.data(this, 'timer', window.setInterval(function() {
            val = self.value;
            if ( $.data( self, 'cache') != val ) {
                $.data( self, 'cache', val );
                $( self ).trigger( 'inputchange' );
            }
        }, 20));
    },
    teardown: function() {
        window.clearInterval( $.data(this, 'timer') );
    },
    add: function() {
        $.data(this, 'cache', this.value);
    }
};

$('#cust_code').on('inputchange', function() {
    show_extra_combo(this,1);
}).change();
</script>
<style>
	.widget-tabs .nav-tabs{
		border-bottom: 1px solid #ddd;
	}
	#myTab a{
		color: #4a8bc2;
	}
	#myTab .active a{
		color: #555;
	}
	#myTab .active{
		color: #555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
	}
	#myTab{
		float: left;
	}
</style>
<div class="row-fluid" id="input-view">
	<div class="span12">
		<!-- BEGIN INLINE TABS PORTLET-->
		<div class="widget green">
			<div class="widget-title">
				<h4><i class="icon-reorder"></i><?=$title?></h4>
			   <span class="tools">
			  <a onclick="backToList();" title="Refresh Tampilan"href="javascript:;"><i class="icon-arrow-left"></i></a>
			   </span>
			</div>
			<div class="widget-body form">
				<form name="form" id="form" class="form-horizontal" action="#">
				<div class="row-fluid">
					<div class="span8">
						<!--BEGIN TABS-->
						<div class="tabbable custom-tab">
							<ul class="nav nav-tabs">
							    <li class="active"><a data-toggle="tab" href="#userinfo">User Info</a></li>
							    <li class=""><a data-toggle="tab" href="#privilege">Privilege</a></li>
						    	<!--li class=""><a data-toggle="tab" href="#storeAccess">Store Access</a></li-->
						    </ul>
							<div class="tab-content">
								<div id="userinfo" class="tab-pane fade active in">
									<input name="t_id_operator" type="hidden" value="<?= encode($list['idMsOperator'])?>"/>
									<div class="control-group">
										<label class="control-label">User Type</label>
										<div class="controls">
											<select name="t_user_type" id="user_type" <?= ($list['idMsOperator'] != "")?'readonly':'' ?>>
												<option value="karyawan" <?= ($list['KodeTypeOperator'] == 'ADM' || $list['KodeTypeOperator'] == 'UTIL' ||$list['KodeTypeOperator'] == 'PI'  ||$list['KodeTypeOperator'] == 'PA')?'selected' : ''; ?>>Karyawan</option>
												<option value="mitra"  <?= ($list['KodeTypeOperator'] == 'MT')?'selected' : ''; ?>>Mitra</option>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Full Name</label>
										<div class="controls">
											<input name="t_full_name" type="text" placeholder="Full Name" value="<?= rtrim($list['FullName']) ?>" class="input-xlarge">
										</div>
									</div>
									<div class="karyawan box">
										<div class="control-group">
											<label class="control-label">NIK</label>
											<div class="controls">
												<input name="t_nik" type="text" placeholder="NIK" value="<?= rtrim($list['Nik']) ?>" class="input-xlarge">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Position</label>
											<div class="controls">
												<select name="t_KodeTypeOperator" id="title" class="input-xlarge">
													<option value=""></option>
													<option value="ADM" <?= ($list['KodeTypeOperator'] == 'ADM')?'selected' : '' ?>>Super Administrator</option>
													<option value="DCI" <?= ($list['KodeTypeOperator'] == 'DCI')?'selected' : '' ?>>Picking</option>
													<option value="DCA" <?= ($list['KodeTypeOperator'] == 'DCA')?'selected' : '' ?>>Packing</option>
													<option value="DCH" <?= ($list['KodeTypeOperator'] == 'DCH')?'selected' : '' ?>>Head Office DC</option>
													<option value="DCV" <?= ($list['KodeTypeOperator'] == 'DCV')?'selected' : '' ?>>Supervisor DC</option>
													<option value="DCM" <?= ($list['KodeTypeOperator'] == 'DCM')?'selected' : '' ?>>Admin DC</option>
													<option value="DCO" <?= ($list['KodeTypeOperator'] == 'DCO')?'selected' : '' ?>>Outbond</option>
													<option value="DCL" <?= ($list['KodeTypeOperator'] == 'DCL')?'selected' : '' ?>>Loading</option>
													<option value="SCO" <?= ($list['KodeTypeOperator'] == 'SCO')?'selected' : '' ?>>Store Coordinator</option>
													<option value="SCA" <?= ($list['KodeTypeOperator'] == 'SCA')?'selected' : '' ?>>Cashier</option>
													<option value="MDC" <?= ($list['KodeTypeOperator'] == 'MDC')?'selected' : '' ?>>MDC</option>
												</select>
											</div>
										</div>
									</div>
									<br>
									<div class="control-group">
										<label class="control-label">Email</label>
										<div class="controls">
											<input name="t_email" type="text" placeholder="Email" value="<?= rtrim($list['Email']) ?>" class="input-xlarge">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Login Name</label>
										<div class="controls">
											<input name="t_login_name" type="text" placeholder="Login Name" value="<?= rtrim($list['LoginName']) ?>" class="input-xlarge">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Password</label>
										<div class="controls">
											<input name="t_login_pass" id="t_login_pass" type="password" placeholder="Password" value="" class="input-xlarge">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Confirmation Password</label>
										<div class="controls">
											<input name="t_login_pass2" id="t_login_pass2" type="password" placeholder="Confirmation Password" value="" class="input-xlarge">
										</div>
									</div>
									<div class="karyawan mitra box">
										<div class="control-group">
											<label class="control-label">Store Code</label>
											<div class="controls">
												<!-- <input name="t_store_code" type="text" placeholder="Store Code" value="<?= rtrim($list['StoreCode']) ?>" class="input-xlarge"> -->
												<?php echo form_dropdown('t_store_code', $options_store, (isset($list['StoreCode']) ?  rtrim($list['StoreCode']) : ''), 'class="input-xlarge chzn-select"'); ?>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Store Code Default</label>
											<div class="controls">
												<!-- <input name="t_store_code_default" type="text" placeholder="Store Code" value="<?= rtrim($list['StoreCodeDefault']) ?>" class="input-xlarge"> -->
												<?php echo form_dropdown('t_store_code_default', $options_store, (isset($list['StoreCodeDefault']) ?  rtrim($list['StoreCodeDefault']) : ''), 'class="input-xlarge chzn-select"'); ?>
											</div>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Expiry Date</label>
										<div class="controls">
											<?php 
												$date = '';
												//$date_default = '01-'.date("m").'-'.date("Y");
												$date_default = '01-'.date("m").'-2020';
											?>
											<div class="input-append date" id="tgl_1_content" data-date="<?= humanize_mdate($list['ExpiryDate'])?humanize_mdate($list['ExpiryDate']) : $date_default  ?>" data-date-format="dd-mm-yyyy">
												<input data-mask="99-99-9999" class="span6" name="t_expiry_date" id="date1" type="text" value="<?= humanize_mdate($list['ExpiryDate'])?humanize_mdate($list['ExpiryDate']) : $date_default ?>" class="input-xlarge">
												<span class="add-on"><i class="icon-th"></i></span>
											</div>
										</div>
									</div>
								</div>
								<div id="privilege" class="tab-pane fade">
									<div class="form-row control-group row-fluid">
										<div class="control-group">
											<label class="control-label"></label>
											<div class="controls">
												<label class="radio">
													<div class="radio" id="uniform-undefined"><span class=""><input type="radio" name="optionsRadios1" checked onClick="getMenu(1);"></span></div>
													Back End
													<input id="t_app_type1" name="t_app_type1" type="hidden" value="" class="input-small">
												</label>
												<label class="radio">
													<div class="radio" id="uniform-undefined"><span class=""><input type="radio" name="optionsRadios1" onClick="getMenu(2);"></span></div>
													Front End
													<input id="t_app_type2" name="t_app_type2" type="hidden" value="" class="input-small">
												</label>
												<label class="radio">
													<div class="radio" id="uniform-undefined"><span class=""><input type="radio" name="optionsRadios1" onClick="getMenu(3);"></span></div>
													Dashboard
													<input id="t_app_type3" name="t_app_type3" type="hidden" value="" class="input-small">
												</label>
												
											</div>
										</div>	
										
										<div class="control-group" id="allow1">
											<label class="control-label"></label>
											<div class="controls">
												<label>
													<input type="checkbox" <?= $list['BackEndAccess']==1?'checked':''?> name="t_allow1" id="t_allow1" onClick="checkAllow(1);"/>
													Allow Access
												</label>
											</div>
										</div>	
										<div class="control-group" id="allow2">
											<label class="control-label"></label>
											<div class="controls">
												<label>
													<input type="checkbox" <?= $list['FrontEndAccess']==1?'checked':''?> name="t_allow2" id="t_allow2" onClick="checkAllow(2);"/>
													Allow Access
												</label>
											</div>
										</div>	
										<table class="table table-striped table-hover table-bordered" id="backEnd">
											<thead>
												<tr>
													<th style="text-align:center;"><!--input type="checkbox" onClick="toggle(this)" /-->
													Menus</th>
													<!-- 
													<th>Check</th> 
													-->
													<th>Description</th> 
												</tr>
											</thead>
											<tbody>
												<?php  
													$this->user_priv_model->showMenu($loginname,1); 
												?> 
											</tbody>
										</table>
										<table class="table table-striped table-hover table-bordered" id="frontEnd">
											<thead>
												<tr>
													<th style="text-align:center;"><!--input type="checkbox" onClick="toggle(this)" /-->
													Menus</th>
													<!-- 
													<th>Check</th> 
													-->
													<th>Description</th> 
												</tr>
											</thead>
											<tbody>
												<?php  
													$this->user_priv_model->showMenu($loginname,2); 
												?> 
											</tbody>
										</table>
										<table class="table table-striped table-hover table-bordered" id="dashboard">
											<thead>
												<tr>
													<th style="text-align:center;"><!--input type="checkbox" onClick="toggle(this)" /-->
													Menus</th>
													<!-- 
													<th>Check</th> 
													-->
													<th>Description</th> 
												</tr>
											</thead>
											<tbody>
												<?php  
													$this->user_priv_model->showDashboard($loginname,3); 
												?> 
											</tbody>
										</table>
									</div>
									
									
								</div>
						        <div id="storeAccess" class="tab-pane fade">
						        	
									<div class="tabbable custom-tab tabs-right">
										<!-- Only required for left/right tabs -->
										<ul class="nav nav-tabs tabs-right">
											<?php
												$num = 0;
												$num++;
												foreach($store_type->result_array() as $str){
												$inum = $num++;
												$idstoretype = $str['idmsStoreType'];
												?>
												<li><a onClick="pageLoadStore(<?=$idstoretype?>)" href="#tab_<?php echo $idstoretype;?>" data-toggle="tab"><?=$str['StoreTypeName'];?></a></li>
											<?php }?>
										</ul>
										<div class="tab-content">
											<?php
												$no = 1;
												
												foreach($store_type->result_array() as $row){
												$i=$no++;
												$idmsstoretype = $row['idmsStoreType'];
												?>
											<div class="tab-pane <?php if($i==1)echo 'active';?>" id="tab_<?= $idmsstoretype;?>">
												<div id="resultContent<?= $idmsstoretype?>">
												</div>
												
											</div>
											<?php }?>
										</div>
									</div>
						        </div>
							</div>
							<div class="form-actions">
									<button type="button" class="btn btn-success" name="save" value="save" onclick="dataSave()">
									Save
								</button>
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				</form>
			</div>
		</div>
		<!-- END INLINE TABS PORTLET-->
	</div>
</div>