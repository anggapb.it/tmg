<!--script src="<?//= base_url();?>assets/js/jquery.nicescroll.js" type="text/javascript"></script-->
<script type='text/javascript'>
	$(document).ready(function(){
		setTitle('<?= $title;?>');
		showStoreAccess(1);
		loadDataStore(1);
		hideProgres();
		//$('#date1').daterangepicker();
		$('#tgl_1_content').datepicker();
		$('#tgl_2_content').datepicker();
		$('.chzn-select').chosen();
		$('#byStoreCode').show();
		$('#byStoreType').hide();
	});

	function showStoreAccess(pg)
	{
		showProgres();
		$.post(site_url+"master/user_priv/store_access_page/"+pg+'/'+<?=$id_user?>
			,$("#form_filter").serialize()
			,function(result) {
				$('#recultContentStoreAccess').html(result);
				
				var sum_row = $("#row_per_page").val();

				var current = $('#current').val();
				
				if(current == 'undefined') 
				{
					showStoreAccess(1);						
				}
				hideProgres();

			}					
			,"html"
		);	
	}

	function deleteConfirm(id)
	{
		bootbox.confirm('Anda yakin data : <br>'+$('#t_code_'+id).val()+' akan di Hapus?', 
			function(e){
				if (e){
					$.post(site_url+"master/user_priv/delete_store/"+id
						,{}
						,function(result) {
							if (result.error)
							{
								pesan_error(result.error);
							}else
							{
								pesan_success(result.message);
								showStoreAccess(1);
							}
						}					
					,"json"
					);
					}
				}); 
	}
	function lookupSelectStore(kode)
	{
		$('#kode').val(kode);
		lookupDataStore(kode,'master/store/get_store_code','resultContentDataStore');
	}
	function lookupDataStore(key,url,objResult)
	{
		$.post(site_url+url
				,{kode:key}
				,function(result){
					$('#'+objResult).html(result);
				});
	}
	function loadDataStore(pg)
	{
		showProgres();
		$.post(site_url+'master/store/lookup_page/'+pg
				,{content_type:$('#content_type').val()
					,lookup_key:$('#lookup_key').val()
					,loader_name:$('#loader').val()}
				,function(result) {
					$('#resultContentStore').html(result);
					hideProgres();
				}					
				,"html"
			);
	}
	function addStore()
	{
		$.post(site_url+"master/user_priv/add_store"
					,{StoreCode : $('#kode').val()
						,fidMsOperator : '<?=encode($id_user)?>'}
					,function(result) {
						if (result.error)
						{
							pesan_error(result.error);
						}else
						{
							
							pesan_success(result.message);
							//setTimeout( "mainPageLoad()", 1900 );
							showStoreAccess(1);
						}
					}					
					,"json"
				);
	}
	function addStoreType()
	{
		alert($('#fid_ms_store_type').val());
		$.post(site_url+"master/user_priv/add_store_type"
					,{fid_ms_store_type : $('#fid_ms_store_type').val()
						,fidMsOperator : '<?=encode($id_user)?>'}
					,function(result) {
						if (result.error)
						{
							pesan_error(result.error);
						}else
						{
							
							pesan_success(result.message);
							//setTimeout( "mainPageLoad()", 1900 );
							showStoreAccess(1);
						}
					}					
					,"json"
				);
	}
	function getFilterBy()
	{
		if($('#filter_by').val()==1){
			$('#byStoreCode').show();
			$('#byStoreType').hide();
		}else{
			$('#byStoreType').show();
			$('#byStoreCode').hide();
		}
	}
</script>
<!-- modal filter -->
<div class="modal hide fade" scrolllock="true" id="myModalStore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 800px">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h3 id="myModalLabel1">Search</h3>
    </div>
	<div class="modal-body">
        	<input type="hidden" id="content_type" value="lookup_user"/>
			<input type="hidden" id="loader" value="loadDataStore"/>
			<div class="tabbable portlet-tabs">
				<div class="content">
					<div class="row-fluid">
						<div class="span12">
							<div class="vcard" >
								<ul>  						
									<li>
									<span class="span2">Key</span>
										<input type="text" aria-co	ntrols="dt_gal" name="lookup_key" id="lookup_key" class="span8" onkeydown="if (event.keyCode == 13) loadDataStore(1)">
									</li>
								</ul>        
							</div>
						</div>
						<div class="tab-pane active" id="resultContentStore">
									
						</div>	
					</div>
					
				</div>				
			</div>	
	</div>
	
    <div class="modal-footer">
        <div class="form-action">
			<a href="#" class="btn btn-info" data-dismiss="modal" onclick="showList(1);">Browse</a>
		</div>
    </div>
</div>
<!-- end modal filter -->
<div class="row-fluid" id="detail-input">
	<div class="span8">
		<div class="widget green">
			<div class="widget-title">
				<h4><i class=" icon-indent-left"></i> <?= $title.' -> '.$user['FullName']?></h4>
			    <span class="tools">
			  <a onclick="backToList();" title="Refresh Tampilan"href="javascript:;"><i class="icon-arrow-left"></i></a>
			   </span>
			</div>
			<div class="widget-body">
				<div class="space10"></div>
				<div class="row-fluid">
					<div class="tab-content">
						<div class="tab-pane active" id="recultContentStoreAccess"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span4">
		<div class="widget green">
			<div class="widget-title">
				<h4><i class=" icon-indent-left"></i> Add Store Access</h4>
			   
			</div>
			<div class="widget-body">
				<div class="row-fluid">
					<div class="controls">
						<label class="control-label">
						<select id="filter_by" class="span6" onChange="getFilterBy();">
							<option value="1">By Store Code</option> 
							<option value="2">By Store Type</option> 
						</select></label>
					</div>
				</div>
				<div class="row-fluid" id="byStoreCode">
					<div class="tab-content">
						<div class="row-fluid">
							<div class="input-append span4">
								<input class="span12" placeholder="Store Code...." name="t_kode" type="text" id="kode" class="row-fluid" onkeydown="if (event.keyCode == 13) lookupSelectStore(this.value)">
								<span class="add-on">
									<a href='#myModalStore' data-toggle="modal" data-backdrop="static">
										<i class="icon-search"></i>
									</a>
								</span>
								
							</div>
						</div>
						<div class="row-fluid">
							<div id="resultContentDataStore">
								</div>
						</div>
					</div>
					<div class="form-action">
						<a class="btn btn-success pull-right" href="javascript:void(0)" title="OK" onclick="addStore()" data-dismiss="modal">
						<i>Add</i> </a>
					</div>
				</div>
				<div class="row-fluid" id="byStoreType">
					<div class="tab-content">
						<div class="row-fluid">
							<div class="controls">
								<select id="fid_ms_store_type" class="span4" name="t_fidmsStoreType">
									<!-- <option value="all">All</option> -->
									<?php
										foreach($store_type->result_array() as $row)
										{
									?>
										<option value="<?= $row['idmsStoreType']?>" >
											<?= $row['StoreTypeName']?>
										</option>
									<?php } ?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="form-action">
						<a class="btn btn-success pull-right" href="javascript:void(0)" title="OK" onclick="addStoreType()" data-dismiss="modal">
						<i>Add</i> </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="main-input"></div>
