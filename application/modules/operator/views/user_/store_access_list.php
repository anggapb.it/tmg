<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Store</th>
            <th>Brand</th>
            <th>Address</th>
            <th style="width:10px">*</th>
        </tr>
    </thead>
    <tbody>
    	<?php
    	$no = ($paging['current']-1) * $paging['limit'] ;
		$no++;
		
		if($list->num_rows() > 0){
			foreach($list->result_array() as $row){
        ?>
        <tr>
    		<td><?= $no++;?></td>
    		<td><?= $row['StoreCode'].' : '.$row['Description']; ?></td>
    		<td><?= $row['Brand']; ?></td>
            <td><?= $row['Address']; ?></td>
            <td>
				<input type="hidden" id="t_code_<?=$row['idMsStoreAccess']?>" value="<?= $row['StoreCode'].' : '.$row['Description']; ?>"/>
                <a href="#" onclick="deleteConfirm('<?= $row['idMsStoreAccess']?>')" class="btn"><i class="icon-trash"></i></a>
            </td>
    	</tr>
    	<?php }
    		}
    	?>
    </tbody>
</table>
<div class="form-actions">
	<?= $paging['list']?>
</div>