<script type="text/javascript">
	$(function () {
        //Initialize Select2 Elements
        refresh_list(1);
	});
	function refresh_list(pg)
	{
		$('#tid_search_key').val($('#tid_search_key_show').val());
		$.post(site_url+"user/page/"+pg
			,$('#filter_content').serialize()
			,function(result){
				show_box(1);
				$('#user_list_container').html(result);
				},"html"
			);
	}
	
	function input(id)
	{	
		$.post(site_url+"user/input/"+id
			,{}
			,function(result){
				show_box(2);
				$('#user_input_box').html(result);
				},"html"
			);
	}
	function show_box(bx)
	{
		$('#user_input_box').hide();
		$('#user_list_box').hide();
		$('#button_new').hide();
		$('#button_list').hide();
		if (bx == 1)
		{
			$('#user_list_box').show();
			$('#button_new').show();
		}else
		{
			$('#user_input_box').show();
			$('#button_list').show();
		}
	}
	function refresh_by_group(id)
	{
		$('#tid_group_id').val(id);
		refresh_list(1);
	}
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		User Management
		<small><?= $user_count?> user<?= ($user_count>1?'s':'')?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"onclick="loadMainContent('dashboard')" ><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">User</li>
	</ol>
</section>
<form id="filter_content" hidden>
	<input name="t_group_id" id="tid_group_id" value="0">
	<input name="t_search_key" id="tid_search_key" value="">
</form>
<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-md-3">
	  <a href="#" class="btn btn-primary btn-block margin-bottom" 
			onclick="input(0);" id="button_new">Create new</a>
	  <a href="#" class="btn btn-warning btn-block margin-bottom"
			onclick="show_box(1);" id="button_list">Back to list</a>
	  <div class="box box-solid">
		<div class="box-header with-border">
		  <h3 class="box-title">Groups</h3>
		  <div class="box-tools">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body no-padding">
		  <ul class="nav nav-pills nav-stacked">
			<?php 
			foreach($user_group_list->result_array() as $row){
				?>
				<li><a href="#" onclick="refresh_by_group('<?= $row['idMsOperatorGroup']?>')"><i class="fa <?= $row['Icon']?>"></i> <?= $row['Name']?>
					<?= ($row['userCount']?'<span class="label label-primary pull-right">'.$row['userCount'].'</span>':'')?>
					</a>
				</li>
				<?php
			}?>
			
		  </ul>
		</div><!-- /.box-body -->
	  </div><!-- /. box -->
	  <div class="box box-solid">
		<div class="box-header with-border">
		  <h3 class="box-title">Status</h3>
		  <div class="box-tools">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body no-padding">
		  <ul class="nav nav-pills nav-stacked">
			<li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Active</a></li>
			<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Expired</a></li>
			<li><a href="#"><i class="fa fa-circle-o text-red"></i> Blocked</a></li>
		  </ul>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div><!-- /.col -->
	<div class="col-md-9">
	  <div class="box box-primary"  id="user_input_box">
	  </div>
	  <div class="box box-primary" id="user_list_box">
		<div class="box-header with-border">
		  <h3 class="box-title">Users List</h3>
		  <div class="box-tools pull-right">
			<div class="has-feedback">
			  <input onkeydown="if (event.keyCode == 13) refresh_list(1)" type="text" id="tid_search_key_show" class="form-control input-sm" placeholder="Search">
			  <span class="glyphicon glyphicon-search form-control-feedback"></span>
			</div>
		  </div><!-- /.box-tools -->
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
		 
		  <div class="table-responsive mailbox-messages" id="user_list_container">
			
		  </div><!-- /.mail-box-messages -->
		</div><!-- /.box-body -->
		<div class="box-footer no-padding">
		  <div class="mailbox-controls">
			<!-- Check all button -->
			<button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
			<div class="btn-group">
			  <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
			  <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
			  <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
			</div><!-- /.btn-group -->
			<button class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
			<div class="pull-right">
			  1-50/200
			  <div class="btn-group">
				<button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
				<button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
			  </div><!-- /.btn-group -->
			</div><!-- /.pull-right -->
		  </div>
		</div>
	  </div><!-- /. box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
