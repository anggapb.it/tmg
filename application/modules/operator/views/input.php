<script type="text/javascript">
$(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
	});
</script>

<div class="box-header with-border">
  <h3 class="box-title">User</h3>
</div><!-- /.box-header -->
<div class="box-body no-padding">
	<form class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Fullname</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="inputEmail3" placeholder="Fullname" value="<?= $user['FullName']?>">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Group</label>
				<div class="col-sm-10">
					<select class="form-control select2" style="width: 100%;">
					<?php foreach($user_group_list->result_array() as $row){ ?>
						<option <?= ($row['idMsOperatorGroup']==$user['fidMsOperatorGroup']?'selected="selected"':'')?>><?= $row['Name']?></option>
					<?php }?>
					  
                    </select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="inputEmail3" placeholder="Email" value="<?= $user['LoginName']?>">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				<div class="checkbox">
					<label>
						<input type="checkbox"> Remember me
					</label>
				</div>
				</div>
			</div>
		</div><!-- /.box-body -->
	</form>
	  <div class="box-footer">
		<button type="submit" class="btn btn-default" onclick="show_box(1);">Cancel</button>
		<button type="submit" class="btn btn-info pull-right">Submit</button>
	  </div><!-- /.box-footer -->

</div>
