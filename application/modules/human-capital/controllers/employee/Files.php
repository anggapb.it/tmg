<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('employee/employee_model');
	}

	public function index()
	{
		echo 'end';
	}
	
	public function manage($id)
	{
		$id = decode($id);
		$data = array();
		$data['content'] = 'employee/files/manage';
		$data['title'] = 'File Management';
		$data['employee'] = $this->employee_model->get($id);
		
		$this->load->view($data['content'],$data);
	}	
	function proses_upload()
	{
		$_this = & get_Instance();
		$_this->load->library('image_lib');
		//save picture
		$structure  = 'files/profile/original';
		$structure2  = 'files/profile/thumbnails_29x29';
		$structure3  = 'files/profile/thumbnails_45x45';
		$structure4  = 'files/profile/thumbnails_90x90';
		$structure5  = 'files/profile/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['userfile'];
		$ext = explode(".", $filedata['name'][0]);
		$file_ext = end($ext);			
		$id_employee = decode($this->input->post('t_id_ms_employee'));
		$employee['EmpCode'] = ($this->input->post('t_employee_code'));
		// $employee = $this->employee_model->get($id_employee);
		// echo($employee['EmpCode'].'.'.$file_ext);
		// print_r($_POST);
		// print_r($filedata);
		// exit;
		$upload_image = $structure.'/'.$employee['EmpCode'].'.'.$file_ext;//$filedata['name'][0];
		if(move_uploaded_file($filedata['tmp_name'][0], $upload_image)) 
		{
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"source_image"  => $upload_image,
					"new_image"     => './files/profile/'.$r['name'].'/'.$employee['EmpCode'].'.'.$file_ext //$filedata['name'][0]
				);
				// print_r($resize);
				// exit;
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize())                    
					die($_this->image_lib->display_errors());
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			// echo $this->upload->display_errors();
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br>(Brand dengan SKU : '.$sku.' tidak ada)</label>.';
		}
		// redirect("loadMainContent('human-capital/employee/main')");
		// echo $msg;
		/*
		if(isset($_FILES['userfile']))
		{
			$this->upload->initialize(array(
				'upload_path' => './files/profile/original/',
				'allowed_types' => 'png|jpg|gif',
				'max_size' => '5000',
				'max_width' => '3000',
				'max_height' => '3000'
			)); 
		
			if($this->upload->do_upload())
			{
				$data_upload = $this->upload->data();
				$size =  array(                
                        array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
                        array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
                        array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
                        array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
                    );
				foreach($size as $r){    
					$this->image_lib->initialize(array(
						'image_library' => 'gd2',
						'source_image' => './files/profile/original/'.$r['name'].'/'.$data_upload['file_name'],
						'new_image' => './files/files/profile/'.$r['name'].'/'.$data_upload['file_name'],
						'maintain_ratio' => false,
						'quality' => '100%',
						'width' => $r['width'],
						'height' => $r['height']
					));
					
					if(!$this->image_lib->resize())
					{
						die($this->image_lib->display_errors());
					}
				}
			}
			else
			{
				die($this->image_lib->display_errors());
			}
			$data['picture'] = $data_upload['file_name'];
		}
		*/
	}
}