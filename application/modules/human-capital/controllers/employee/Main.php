<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('employee/employee_model');
		// $this->load->model('item_unit_model');
	}

	public function index()
	{
		$data = array();
		$data['content'] = 'employee/main';
		// employee summary data
		// get total 
		$data['emp']['count_all'] = $this->employee_model->count();
		// get unApproved
		$where['ApprovalStatus'] = 1;
		$this->employee_model->set_where($where);
		$data['emp']['count_approved'] = $this->employee_model->count();
		$data['emp']['count_on_proces'] = $data['emp']['count_all'] - $data['emp']['count_approved'];
		$data['emp']['progress'] = 0;
		if ($data['emp']['count_all'])
		$data['emp']['progress'] = ($data['emp']['count_approved']/$data['emp']['count_all']) * 100;
		// status message
		if ($data['emp']['count_on_proces'] <> 0)
		{
			$data['emp']['status_msg']	= thausand_spar($data['emp']['count_on_proces']) . ' on remaining';
		}else
		{
			$data['emp']['status_msg']	='Completed';
		}
		// item unit summary
		// get total 
		$data['unit']['count_all'] = $this->employee_model->count();
		
		// 
		$this->load->view($data['content'],$data);
	}
}