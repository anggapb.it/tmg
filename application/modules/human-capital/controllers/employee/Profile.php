<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('employee/employee_model');
		$this->load->model('organization/position_model');
	}

	public function index()
	{
		echo 'end';
	}

	public function info($id='')
	{
		$id = decode($id);
		$emp =  $this->employee_model->get($id);				
		$pos =  $this->position_model->get($emp['fidMsPosition']);				
		//
		$data = array();
		$data['content'] = 'employee/profile/info';
		$data['employee'] = $emp;
		$data['position'] = $pos;
		$this->load->view($data['content'],$data);
	}
}