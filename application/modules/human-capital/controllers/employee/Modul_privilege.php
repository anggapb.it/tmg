<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('employee/employee_model');
		// $this->load->model('dealer/dealer_model');
		// $this->load->model('salesperson/salesperson_model');
		$this->load->model('operator/user_group_model');
		$this->load->model('operator/operator_model');
		$this->load->model('master.identity/hobi/hobi_model');
		$this->load->model('master.identity/pendidikan/pendidikan_model');
	}

	public function index()
	{
		// echo 'end';
		$data = array();
		$data['content'] = 'employee/employee/manage';

		$this->load->view($data['content'],$data);
	}
	
	public function inner_info()
	{
		$where = array();
		$sessOperator =  $this->session->userdata('Operator');
		if($sessOperator['fidMsOperatorGroup'] != 1){ 	// Jika Dealer : tampilkan pegawai di dealer tersebut
			$where['"KodeDealer" = \''.$sessOperator['KodeDealer'].'\''] = null;
			$this->employee_model->set_where($where);
		} 
		if($sessOperator['fidMsOperatorGroup'] == 3){	// Jika Sales : hanya tampilkan akun dirinya saja
			$where['"idMsEmployee" = \''.$sessOperator['fidMsEmployee'].'\''] = null;
			$this->employee_model->set_where($where);
		} 

		if($this->session->userdata('Operator')['idMsOperator'] != 1) {
			$where['idMsEmployee != 1'] = null;
			$this->employee_model->set_where('idMsEmployee != 1');
		}

		$count_all = $this->employee_model->count();
		$where['ApprovalStatus'] = 9;
		$this->employee_model->set_where($where);
		$count_approved = $this->employee_model->count();
		$count_on_proces = $count_all - $count_approved;
		$progress = (int) (($count_approved/$count_all) * 100);
		if ($count_on_proces <> 0)
		{
			$status	= thausand_spar($count_on_proces) . ' on remaining';
		}else
		{
			$status	='Completed';
		}
		// load
		$this->update['title'] = $count_all;
		$this->update['status'] = $status;
		$this->update['progress'] = $progress;
		$this->success('Loaded');
	}	

	public function manage()
	{
		$data = array();
		$data['content'] = 'employee/employee/manage';
		// $data['dealer'] = $this->dealer_model->get_list();
		$data['userGroup'] = $this->user_group_model->get_list();
		
		$this->load->view($data['content'],$data);
	}
	
	public function get_list()
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$filter['KodeDealer'] = $this->input->post('t_dealer_key');
		$row_id = $this->input->post('t_last_row_id');
		$refresh = $this->input->post('t_refresh');
		$limit = 3;
		if ($refresh==1) // reset tampilan awal
		{
			$limit = 10;
			$row_id = 0;
		}
		// set condition
		$where = array();
		if($this->session->userdata('Operator')['fidMsOperatorGroup'] != 1){ 				// Jika Dealer : tampilkan pegawai di dealer tersebut
			$where['tbl."KodeDealer"'] = $this->session->userdata('Employee')['KodeDealer'];
		} 
		if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 3){	// Jika Sales : hanya tampilkan akun dirinya saja
			$where['tbl."idMsEmployee"'] = $this->session->userdata('Operator')['fidMsEmployee'];
		} 


		if ($filter['key'])
		{
			$where['(
					tbl."EmpCode" like \'%'.$filter['key'].'%\'
				or upper(tbl."Name") like \'%'.$filter['key'].'%\'
				or upper(tbl."Address") like \'%'.$filter['key'].'%\'
				or upper(tbl."City") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		if($filter['KodeDealer']) {
			$where['tbl."KodeDealer"'] = $filter['KodeDealer'];
		}
		$this->employee_model->set_where($where);
		//
		$order_by = array('tbl."EmpCode"'=>'asc');
		$this->employee_model->set_order($order_by);
		//
		$this->employee_model->set_limit($limit);
		$this->employee_model->set_offset($row_id);
		//
		$data = array();
		$data['content'] = 'employee/employee/list';		
		$data['emp']['list'] = $this->employee_model->get_list();		
		$data['emp']['row_count'] = $this->employee_model->get_count();		
		$data['emp']['key'] = $filter['key'];		
		$data['page']['row_id'] = $row_id;	
		$data['page']['refresh'] = $refresh;

		$data['list']		= $data['emp']['list'];
		$page['current'] 	= $row_id;
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $data['emp']['row_count'];
		$page['load_func_name'] = 'pageLoadEmployee';
		$page['list'] 		= $this->gen_paging($page);
		$data['paging'] = $page;

		$this->load->view($data['content'],$data);
	}

	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$filter['KodeDealer'] = $this->input->post('t_dealer_key');
		$row_id = $this->input->post('t_last_row_id');
		$refresh = $this->input->post('t_refresh');
		$limit = $this->input->post('t_limit_rows')?:10;
		
		// set condition
		$where = array();
		/* $userGroupList = $this->user_group_model->get_list();
		$whereUserGroup = '0';
		foreach($userGroupList->result_array() as $group){ 
			$id = $group['idMsOperatorGroup'];
			if ($this->input->post('t_user_group_'.$id))
			{
				$whereUserGroup .= ','.$id;
			}
		} */
		// $where['(op."fidMsOperatorGroup" is null OR op."fidMsOperatorGroup" IN ('.$whereUserGroup.'))'] = null;

		 $sessOpr = $this->session->userdata('Operator');
		/*if($sessOpr['fidMsOperatorGroup'] != 1){
			$where['tbl."KodeDealer"'] = $this->session->userdata('Dealer')['KodeDealer'];
		} 
		if($sessOpr['fidMsOperatorGroup'] == 3){
			$where['tbl."idMsEmployee"'] = $this->session->userdata('employee')['idMsEmployee'];
		} 
*/
		if($sessOpr['fidMsEmployee'] != 1){
			$where['idMsEmployee <>'] = 1;
		} 
		if ($filter['key'])
		{
			$where['(
					tbl."EmpCode" like \'%'.$filter['key'].'%\'
				or upper(tbl."Name") like \'%'.$filter['key'].'%\'
				or upper(tbl."Address") like \'%'.$filter['key'].'%\'
				or upper(tbl."City") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		/* if($filter['KodeDealer']) {
			$where['tbl."KodeDealer"'] = $filter['KodeDealer'];
		} */
		$this->employee_model->set_where($where);
		//
		$order_by = array('tbl."Name"'=>'asc');
		$this->employee_model->set_order($order_by);
		//
		$this->employee_model->set_limit($limit);
		$this->employee_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->employee_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadEmployee';
		$page['list'] 		= $this->gen_paging($page);
		//
		// $this->employee_model->get_list();
		// echo $this->db->last_query();die();
		$data = array();
		$data['content'] = 'employee/employee/list';
		$data['list'] = $this->employee_model->get_list();		
		$data['key'] = $filter['key'];		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id='')
	{
		// load addiotinal model
		$this->load->model('religion_model');
		//
		$id = decode($id);
		$emp =  $this->employee_model->get($id);
		if (!isset($emp['idTrDataLog']))
			$emp['idTrDataLog'] = 0;
	
		$title = 'Edit data';
		// jika kosong
		if (!$id)
		{
			$title = 'Create New';
			$emp['EmpCode'] = $this->employee_model->gen_new_code();
		}
		//		
		$religion =  $this->religion_model->get_list();		
		//
		$data = array();
		$data['content'] = 'employee/employee/input';
		$data['employee'] = $emp;
		// $data['dealer'] = $this->dealer_model->get_list();	
		$data['religion'] = $religion;
		$data['hobi'] 				= $this->hobi_model->get_list();		
		$data['pendidikan'] 		= $this->pendidikan_model->get_list();
		$data['title'] 	= $title;
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
		$data = array();
		$blockedObj = array();
		$idDataLog 				= (decode($this->input->post('t_id_log'))?:0);
		$idMsEmployee	 		= (decode($this->input->post('t_id_ms_employee'))?:0);
		$data['idMsEmployee']	= $idMsEmployee;
		if ($idMsEmployee)
		{	
			$data['idMsEmployee']	= $idMsEmployee;
		}else
		{
			$this->db->select('tbl."idMsEmployee"');
			$this->db->order_by('idMsEmployee','desc');
			$res = $this->db->get('humanCapital.msEmployee tbl',1)->row();
			$data['idMsEmployee'] = $res->idMsEmployee +1;
			$data['EmpCode'] 		= $this->employee_model->gen_new_code();
		}
		// Validasi hasil inputan operator
		$data['Name'] 			= $this->validation_input('t_name',5,30);
		$data['NickName'] 		= $this->validation_input('t_nick_name',3);
		$data['CityOfBirth'] 	= $this->validation_input('t_city_of_birth',4);
		$data['DateOfBirth'] 	= $this->validation_input('t_date_of_birth');
		$data['DateOfBirth'] 	= $this->convert_date($data['DateOfBirth']);

		// $data['Jabatan'] 	= $this->validation_input('t_Jabatan');
		$data['IDCardNumber'] 	= $this->validation_input('t_NoKTP',16);
		$data['fidPendidikan'] 	= $this->validation_input('t_Pendidikan');

		if($data['IDCardNumber'] == ""){
			$this->error('No KTP Harus Diisi');
		}
		/* if(!$data['Jabatan']){
			$this->error('Jabatan Harus Diisi');
		} */
		if($data['fidPendidikan'] == "N"){
			$this->error('Pendidikan Harus Diisi');
		}
		$data['fidHobi'] 	= $this->input->post('t_Hobi');
		if($data['fidHobi'] == "N"){
			$this->error('Hobi Harus Diisi');
		}
		$data['Address_KTP'] 		= $this->input->post('t_address_ktp');
		$data['Provinsi_KTP'] 			= $this->input->post('t_Provinsi_ktp');
		$data['City_KTP'] 			= $this->input->post('t_Kota_ktp');
		$data['KodePos_KTP'] 			= $this->input->post('t_KdPos_ktp')?:0;

		// data
		$data['fidMsReligion'] 	= $this->input->post('t_religion')?:0;
		$data['Address'] 		= $this->input->post('t_address');
		$data['Provinsi'] 			= $this->input->post('t_Provinsi');
		$data['City'] 			= $this->input->post('t_city');
		$data['KodePos'] 			= $this->input->post('t_KdPos')?:0;
		$data['PhoneNumber1'] 	= $this->input->post('t_phonenumber1');
		$data['PhoneNumber2'] 	= $this->input->post('t_phonenumber2');
		$data['Email'] 			= $this->input->post('t_email');
		$data['fidGender'] 		= $this->input->post('t_gender')?:0;
		// $data['KodeSalesAHM'] = $this->input->post('t_KodeSalesCurrent')?:'';
		// if (count($blockedObj) > 0)
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}

		// $data['isSales'] = $this->input->post('t_isSales');
		/* if($this->input->post('t_isSales') == 1){
			$data['isSales'] = $this->input->post('t_isSales');
		} */
		/* if($this->input->post('t_KodeSales')){
			$data['KodeSalesAHM'] = $this->input->post('t_KodeSales');
		} */
		/* if($this->input->post('t_KodeDealer')) { 
			$kodeDA = explode("--", $this->input->post('t_KodeDealer'));
			$data['KodeDealer'] = $kodeDA[0];
			$data['KodeAhass'] = $kodeDA[1];
		} else {
			if($this->session->userdata('Operator')['fidMsOperatorGroup'] != 1){
				$data['KodeDealer'] = $this->session->userdata('KodeDealer');
				$data['KodeAhass'] = $this->session->userdata('KodeAHASS');
			}
			// else {
			// 	$this->error('Kode Dealer Harus Diisi');
			// }
		} */

		if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1){
			$data['ApprovalStatus'] = 9;
		}		

		// proses simpan
		$save = false;

		$this->db->trans_start();
			$save = $this->employee_model->save($data);
			if ($save) {
				//jika create new
				if (!$idMsEmployee)
				{
					$where['EmpCode'] = $data['EmpCode'];
					$new = $this->employee_model->get($where);
					$idMsEmployee = $new['idMsEmployee'];
				}
	
			}
			$this->db->trans_complete();
		if($this->db->trans_status())
		{
			$this->update['id_employee_encoded'] = encode($idMsEmployee);
				$this->success('Data saved successfully');
		}else
		{
			$this->error('Proses simpan gagal.');
		}
	}
	function change_pict($id=0)
	{
		$_this = & get_Instance();
		$config['image_library'] = 'gd2';
		$_this->load->library('image_lib',$config);
		$this->load->model('utilities/file_upload/file_upload_model');
		//save picture
		$structure  = 'files/profile/original';
		$structure2  = 'files/profile/thumbnails_29x29';
		$structure3  = 'files/profile/thumbnails_45x45';
		$structure4  = 'files/profile/thumbnails_90x90';
		$structure5  = 'files/profile/thumbnails_128x128';
		if(!file_exists($structure))
		{
			!mkdir($structure, 0777, true);
		}
		if(!file_exists($structure2))
		{
			!mkdir($structure2, 0777, true);
		}
		if(!file_exists($structure3))
		{
			!mkdir($structure3, 0777, true);
		}
		if(!file_exists($structure4))
		{
			!mkdir($structure4, 0777, true);
		}
		if(!file_exists($structure5))
		{
			!mkdir($structure5, 0777, true);
		}
		$msg = '';
		$filedata = $_FILES['Filedata'];
		$ext = explode(".", $filedata['name']);
		$file_ext = end($ext);			
		$id_employee = decode($id);
		$employee = $this->employee_model->get($id_employee);
		// echo($employee['EmpCode'].'.'.$file_ext);
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		$upload_image = $structure.'/'.$employee['EmpCode'].'.'.$file_ext;//$filedata['name'];
		if(move_uploaded_file($filedata['tmp_name'], $upload_image)) 
		{
			$img_size = getimagesize($upload_image);
			//cek fileUpload apa sudah eksis
			$file_up = $this->file_upload_model->get(array('FileName' => $employee['EmpCode']));
			// insert data public.fileUpload
			$file['idFileUpload'] = $file_up['idFileUpload']?:0;
			$file['Category'] = 'Profile';
			$file['fidData'] = $id_employee;
			$file['FileExtention'] = $file_ext;
			$file['FileName'] = $employee['EmpCode'];
			$file['FileSize'] = $filedata['size'];
			$img = $this->file_upload_model->save($file);
			
			$size =  array(                
					array('name'    => 'thumbnails_29x29','width'    => 29, 'height'    => 29, 'quality'    => '100%'),
					array('name'    => 'thumbnails_45x45','width'    => 45, 'height'    => 45, 'quality'    => '100%'),
					array('name'    => 'thumbnails_90x90','width'    => 90, 'height'    => 90, 'quality'    => '100%'),
					array('name'    => 'thumbnails_128x128','width'    => 128, 'height'    => 128, 'quality'    => '100%')
				);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"         => $r['width'],
					"height"        => $r['height'],
					"quality"       => $r['quality'],
					"source_image"  => $upload_image,
					"new_image"     => './files/profile/'.$r['name'].'/'.$employee['EmpCode'].'-'.$r['width'].'x'.$r['height'].'.'.$file_ext //$filedata['name']
				);
				// print_r($resize);
				// exit;
				$_this->image_lib->initialize($resize); 
				if(!$_this->image_lib->resize())                    
					die($_this->image_lib->display_errors());
				
				$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto telah disimpan.</label>';
				
			}
		}else{
			$msg .= '<label style="background-color: rgba(255,212,0,0.58);">Foto gagal disimpan<br></label>.';
		}
		
	}
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->employee_model->set_limit($limit);
		$this->employee_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey){
				$where['(
						upper("EmpCode") like \'%'.$lookupkey.'%\'
					or upper("Name") like \'%'.$lookupkey.'%\'
					)'] = null;
			}
		$this->employee_model->set_where($where);
		$order_by['EmpCode'] = 'DESC';
		$this->employee_model->set_order($order_by);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->employee_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataEmployee';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->employee_model->get_list();
		//
		$data = array('list' 	=> 	$list
			,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'employee/employee/list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}
	function get_employee() {
		$code = $this->input->post('code');
		$employee = $this->employee_model->get(array('EmpCode' => $code));
		echo json_encode(array_merge($employee));
		
    }
	function login_as()
	{
		$idOpr = decode($this->input->post('t_id_operator'));
		$dataAuth = array();
		$dataAuth['idMsOperator'] = $idOpr;
		$this->load->library('auth');
		$return = $this->auth->do_login($dataAuth);
				
		$this->success('okeeeeh');
	}
	
}