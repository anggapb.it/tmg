<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privilege extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('employee/employee_model');
		$this->load->model('engine/menu_model');
		$this->load->model('engine/short_cut_model');
		$this->load->model('operator/operator_model');
		$this->load->model('operator/user_group_model');
		$this->load->model('salesperson/salesperson_model');
		$this->load->model('operator/operator_privilege_model');
		$this->load->model('operator/operator_shortcut_model');
		$this->load->model('modul_privilege/operator_modul_model');
		$this->load->model('modul_privilege/operator_modul_privilege_model');
		$this->load->model('master/gudang_model');
	}

	public function index()
	{
		echo 'end';
	}
	
	public function input($id)
	{
		$id = decode($id);
		$title = 'Set Privilege';
		// get dataEmployee
		$emp =  $this->employee_model->get($id);
		// get data username
		$where['fidMsEmployee'] = $id;
		$opr = $this->operator_model->get($where);
		// get menu master + provilege
		$shortcut = array();
		$menu_list = array();
		if ($opr['idMsOperator'])
		{
			$idMsOpr = $opr['idMsOperator'];
			$menu_list = $this->menu_model->get_menu_by_user($idMsOpr);
			
			// get short cut
			foreach($menu_list->result_array() as $menu)
			{
				$where = array();
				$where['fidAppMenu'] = $menu['idAppMenu'];
				$this->short_cut_model->set_where($where);
				$shortcut[$menu['idAppMenu']] = $this->short_cut_model->get_list($idMsOpr);
			}
		}
		if (isset($opr['idTrDataLog']) == false)
			$opr['idTrDataLog'] = 0;
		
		$this->gudang_model->set_order(array('idGudang' => 'ASC','KodeGudang' => 'ASC'));
		$gudang 		= $this->gudang_model->get_list();
		
		// load data
		$data = array();
		$data['content'] = 'employee/privilege/input';
		$data['menu_list'] = $menu_list;
		$data['shorcut_list'] = $shortcut;
		$data['employee'] 	= $emp;
		$data['operator'] 	= $opr;
		$data['gudang'] 	= $gudang;
		$data['modul']		= $this->operator_modul_model->get_list(($opr['idMsOperator']?:0));				
			
		$whereGroup['(
						tbl."idMsOperatorGroup" >= \''.$this->session->userdata('Operator')['fidMsOperatorGroup'].'\' 
					)'] = null;
		$this->user_group_model->set_where($whereGroup);
		$data['group'] = $this->user_group_model->get_list();	
		
		$data['title'] 	= $title;
		$this->load->view($data['content'],$data);
	}

	public function osave()
	{
		$data = array();
		$blockedObj = array();
		$idDataLog 		= (decode($this->input->post('t_id_log'))?:0);
		$idMsOperator	= (decode($this->input->post('t_id_ms_operator'))?:0);
		$data['idMsOperator'] = $idMsOperator;
		// 1. check DataLog MsOperator
		$log = $this->operator_model->get_last_log($idMsOperator);
		if ($idDataLog <> $log['idTrDataLog'])
		{
			$this->error('Local data and server data does not match');
		}
		// 2. Validasi data tidak boleh kosong, tidak boleh kosong
		$data['LoginName'] 		= $this->validation_input('t_name',4,30);
		$data['ExpiryDate'] 	= $this->validation_input('t_expiry_date');
		$data['ExpiryDate'] 	= $this->convert_date($data['ExpiryDate']);
		$data['FullName'] 		= $this->input->post('t_full_name');
		$data['fidMsEmployee'] 	= decode($this->input->post('t_fid_employee'));
		// 3. check ketersediaan username
		$whereOprCheck = array();
		$whereOprCheck['LoginName'] = $data['LoginName'] ;
		if ($idMsOperator)
		{
			$whereOprCheck['idMsOperator <>'] = $idMsOperator ;
		}
		$oprCheck = $this->operator_model->get($whereOprCheck);
		if ($oprCheck['LoginName'])
		{
			$obj_blocked['obj_name'] = 't_name';
			$obj_blocked['obj_msg'] = 'Username not available';
			$this->blocked_object[] = $obj_blocked;		
		}
		// 4.a validasi password harus terisi
		$pass1 		= $this->validation_input('t_pass1',4);
		$pass2 		= $this->validation_input('t_pass2',4);
		// 4.b validasi reType password harus sesuai
		if ($pass1<>$pass2)
		{
			$obj_blocked['obj_name'] = 't_pass2';
			$obj_blocked['obj_msg'] = 'reType Password does not match';
			$this->blocked_object[] = $obj_blocked;		
		}else
		{
			$opr = $this->operator_model->get($idMsOperator);
			// jika password tidak sama dengan server, 
			// berarti user merubah pasword, 
			// password harus diUpdate keserver 
			if ($pass1 <> $opr['LoginPass'])
			{
				$nChar = strlen($pass1);
				$cPassGen = $pass1;
				$x = 1; 
				while($x <= $nChar+1) 
				{
					$cPassGen = md5($cPassGen);
					$x++;
				} 
				$data['LoginPass'] 	= $cPassGen;
			}
			
		}
		// object validation
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}

		// if($this->session->userdata('Operator')['fidMsOperatorGroup'] != 3) {
			/* if(!$this->input->post('t_Role')){
				$this->error('Role Account Harus Diisi');
			}
			$data['fidMsOperatorGroup'] = $this->input->post('t_Role'); */
		// }
		
		$data['fidMsOperatorGroup'] = 1;
		$data['isDefaultGudang'] = $this->input->post('isDefaultGudang')?:0;
		$data['KodeDealer'] = decode($this->input->post('t_KodeDealer'));
		$data['KodeAHASS'] = decode($this->input->post('t_KodeAhass'));

		// proses simpan
		$this->db->trans_start();
			if($this->input->post('t_id_ms_operator')){
				$idOperator = decode($this->input->post('t_id_ms_operator'));
			} else {
				$this->db->select('tbl."idMsOperator"');
				$this->db->order_by('idMsOperator','desc');
				$res = $this->db->get('public.msOperator tbl',1)->row();
				$idOperator = $res->idMsOperator +1;
				// $idOperator = $this->db->insert_id();
			}
			$data['idMsOperator'] = $idOperator;
			/* if($data['fidMsOperatorGroup'] == 3){ 		// Jika Role=Sales, Add User to msSalesPerson.
				if($this->input->post('t_KodeSales')){
					$sales['Code'] = decode($this->input->post('t_KodeSales'));	
					$data['KodeSalesPerson'] = decode($this->input->post('t_KodeSales'));
				} else {
					$getcode = $this->salesperson_model->get_newcode($data['KodeDealer']);
					$sales['Code'] = $getcode;	
					$data['KodeSalesPerson'] = $getcode;
				}
				if($this->operator_model->save($data)){
					
					$sales['Description'] = $this->input->post('t_full_name');
					// $sales['OrderBy'] = 'SL-009';
					$sales['Jabatan'] = 'Sales';
					$sales['KodeDealer'] = $data['KodeDealer'];
					$sales['idMsEmployee'] = $data['fidMsEmployee'];
					$this->salesperson_model->save($sales);
				}
			} else { */
				$this->operator_model->save($data);
			// }
			
			// Auto Set Privileges
			$getDefaultMenu = $this->user_group_model->get_defaultMenu($data['fidMsOperatorGroup']);
			if($getDefaultMenu->DefaultMenu) {
				$DefaultMenu = explode(',',$getDefaultMenu->DefaultMenu);
				$DefaultShortcut = explode(',',$getDefaultMenu->DefaultShortcut);
				foreach ($DefaultMenu as $key) {
					$id = $key;
					//
					$dataGet = array();
					$whereGet = array();
					$whereGet['fidMsOperator'] = $idOperator;
					$whereGet['fidAppMenu'] = $id;
					$dataGet = $this->operator_privilege_model->get($whereGet);
					//
					$defMenu = array();
					$defMenu['idPrivilege'] = ($dataGet['idPrivilege']?:0);
					$defMenu['fidMsOperator'] = $idOperator;
					$defMenu['fidAppMenu'] = $id;
					$defMenu['Status'] = 1;

					$save = $this->operator_privilege_model->save($defMenu);
				}
				foreach ($DefaultShortcut as $key) {
					$id = $key;
					$dataGet = array();
					$whereGet = array();
					$whereGet['fidMsOperator'] = $idOperator;
					$whereGet['fidAppShortCut'] = $id;
					$dataGet = $this->operator_shortcut_model->get($whereGet);
					//
					$defShort = array();
					$defShort['idMsOperatorAppShortKey'] = ($dataGet['idMsOperatorAppShortKey']?:0);
					$defShort['fidMsOperator'] = $idOperator;
					$defShort['fidAppShortCut'] = $id;
					$defShort['Status'] = 1;
					
					$save = $this->operator_shortcut_model->save($defShort);
				}
			}
		$this->db->trans_complete();
		if($this->db->trans_status())
		{
			$this->success('Data saved successfully');
		}else
		{
			$this->error('Save process has failed.');
		}
	}
	
	function psave()
	{
		$idMsOperator = (decode($this->input->post('t_id_ms_operator'))?:0);
		$fidMenuDefault = $this->input->post('t_fid_default_menu');
		// load model msOperatorPrivilege
		// get Master Menu
		$menu_list = $this->menu_model->get_list();
		// get master Short Cut
		$shortCut_list = $this->short_cut_model->get_list();
		// proses simpan msOperatorPrivilege
		$save = true;
		$this->db->trans_start();
		$dataOpr = array();
		$dataOpr['idMsOperator'] = $idMsOperator;
		$dataOpr['fidMenuDefaultOpen'] = $fidMenuDefault?:0;
		$this->operator_model->save($dataOpr);
		foreach($menu_list->result_array() as $menu)
		{
			$id = $menu['idAppMenu'];
			$isChecked = $this->input->post('ch_menu'.$id);
			// if ($isChecked !== null)
			// {
				//
				$dataGet = array();
				$whereGet = array();
				$whereGet['fidMsOperator'] = $idMsOperator;
				$whereGet['fidAppMenu'] = $id;
				$dataGet = $this->operator_privilege_model->get($whereGet);
				//
				$data = array();
				$data['idPrivilege'] = ($dataGet['idPrivilege']?:0);
				$data['fidMsOperator'] = $idMsOperator;
				$data['fidAppMenu'] = $id;
				$data['Status'] = ($isChecked=='on'?1:0);
				// if ($isChecked || $dataGet['idPrivilege'])
				// if ($isChecked !== null)
				// {
					$save = $this->operator_privilege_model->save($data);
				// }
			// }
		}
		// proses simpan msOperatorShortCut
		foreach($shortCut_list->result_array() as $sc)
		{
			$id = $sc['idAppShortCut'];
			$isChecked = $this->input->post('t_short_cut'.$id);
			if ($isChecked !== null)
			{
				//
				$dataGet = array();
				$whereGet = array();
				$whereGet['fidMsOperator'] = $idMsOperator;
				$whereGet['fidAppShortCut'] = $id;
				$dataGet = $this->operator_shortcut_model->get($whereGet);
				//
				$data = array();
				$data['idMsOperatorAppShortKey'] = ($dataGet['idMsOperatorAppShortKey']?:0);
				$data['fidMsOperator'] = $idMsOperator;
				$data['fidAppShortCut'] = $id;
				$data['Status'] = $isChecked;
				// if ($isChecked || $dataGet['idMsOperatorAppShortKey'])
				if ($isChecked !== null)
					$save = $this->operator_shortcut_model->save($data);
			}
		}
		$this->db->trans_complete();
		if($this->db->trans_status())
		{
			// $this->update['id_employee_encoded'] = encode($idMsEmployee);
			$this->success('Data saved successfully');
		}else
		{
			$this->error('Save process has failed.');
		}
	}
	
	function modul_save() {
		$idOperatorSpecial = $this->input->post('idOperatorSpecial');
		$data['fidMsOperator'] = decode($this->input->post('idMsOperator'));
		$data['SpecialValue'] = 0;
		$idOperatorModul = $this->input->post('idOperatorModul');
		$Status = $this->input->post('Status');
		$KetStatus = $this->input->post('KetStatus');
		
		$this->db->trans_start();
		if(count($idOperatorSpecial) > 0) {
			for($i=0;$i<count($idOperatorSpecial);$i++) {
				if($KetStatus[$i] == '')
					continue; 
					
				$idOperatorSpecial[$i] = decode($idOperatorSpecial[$i]);
				
				if ($idOperatorSpecial[$i])
				{	
					$data['idOperatorSpecial'] 	= $idOperatorSpecial[$i];
				}else
				{					
					$this->db->select('tbl."idOperatorSpecial"');
					$this->db->order_by('idOperatorSpecial','desc');
					$res = $this->db->get('public.msOperatorSpecial tbl',1)->row();
					
					$data['idOperatorSpecial'] = (!isset($res->idOperatorSpecial) ? 1 : $res->idOperatorSpecial+1);	
				}
				
				$data['fidOperatorModul'] = decode($idOperatorModul[$i]);
				$data['Status'] = isset($Status[$i]) ? $Status[$i] : 0;
				
				$this->operator_modul_privilege_model->save($data);
			}
		}
		
		$this->db->trans_complete();
		if($this->db->trans_status())
		{
			// $this->update['id_employee_encoded'] = encode($idMsEmployee);
			$this->success('Data saved successfully');
		}else
		{
			$this->error('Save process has failed.');
		}
	}
	
	function delete_user(){
		$fidMsEmployee = decode($this->input->post('t_fidMsEmployee'));
		
		$this->db->trans_start();

		$where['fidMsEmployee'] = $fidMsEmployee;		
		$operator = $this->operator_model->get($where);
		$this->operator_model->delete($where);
		$this->operator_privilege_model->delete(array('fidMsOperator' => $operator['idMsOperator']));
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
}