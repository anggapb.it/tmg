<script type="text/javascript">
	$(function () {
        show("main_container");
	});
	function show(cont)
	{
		hide_all();
		$("#"+cont).show();
		if ($("#"+cont).children().length <= 2)
			load_data(cont);
	}
	function hide_all()
	{
		$('#main_container').hide();
		$('#org_container').hide();
		$('#emp_container').hide();
	}
	function load_data(cont)
	{
		showProgres();
		var url = '';
		if (cont == 'main_container')
			return;
		if (cont == 'emp_container')
			url = 'human-capital/employee/employee/manage';

		$.post(site_url+url
				,{}
				,function(result) {
					$('#'+cont).html(result);
					hideProgres();
				}
				,"html"
			);
	}
</script>
<div id="main_container">
	<section class="content-header">
	  <h1>
		Employee
		<small>data container</small>
	  </h1>

	  <ol class="breadcrumb">
		<li><a href="#" onclick="loadMainContent('dashboard');"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Human Capital</li>
		<li class="active">Employee</li>
	  </ol>
	</section>
	<section class="content">
		<?= $this->menu->build_short_cut(105);?>
	</section>
</div>
<div id="emp_container">

</div>
<div id="org_container">

</div>
