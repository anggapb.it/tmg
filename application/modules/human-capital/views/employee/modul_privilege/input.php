<script type="text/javascript">
	$(function () {
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
   		objDate("expiry_date");
		

	});	
	
	function set_short_cut(id)
	{
		var val = $('#short_cut'+id).val();
		
		if (val == 1 )
		{
			$('#short_cut'+id).val(0);
			$("#short_cut_display_"+id).attr('class','btn btn-default');
		}else
		{
			$('#short_cut'+id).val(1);
			$("#short_cut_display_"+id).attr('class','btn btn-success');
		}
	}

	function username_save()
	{
		showProgres();
		$.post(site_url+'human-capital/employee/privilege/osave'
			,$('#username_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					privilege_input('<?= encode($employee['idMsEmployee'])?>');					
				}else
				{
					// clean error sign
					$("#username_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					// show error message
					toastr.error(result.error,'Error');
				}
			}					
			,"json"
		);
	}

	function privilege_save()
	{
		showProgres();
		$.post(site_url+'human-capital/employee/privilege/psave'
			,$('#privilege_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					privilege_input('<?= encode($employee['idMsEmployee'])?>');					
				}else
				{
					// show error message
					toastr.error(result.error,'Error');
				}
			}					
			,"json"
		);
	}
	function delete_user(){
		var options = {
				title: 'Warning',
				message: "Anda yakin akan menghapus User <code><?=$operator['LoginName']?></code> ?"
			};
			eModal.confirm(options).then(function callback(){
			  //jika OK
				showProgres();
				$.post(site_url+'human-capital/employee/privilege/delete_user'
						,{t_fidMsEmployee : '<?=encode($operator['fidMsEmployee'])?>'}
						,function(result) {
							// hideProgres();
							if(result.error){
								toastr.error(result.error,'Error');
								hideProgres();
							}else{
								toastr.success(result.message,'Save');
								hideProgres();
								show_employee_list();
							}
						}					
						,"json"
					);
			},    function callbackCancel(){
			  //JIKA CANCEL
			});
	}
</script>
<section class="content-header">
	<h1>
		Privilege <small>Setup user privilege for employee</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Human Capital</li>
		<li><a href="#" onclick="show('main_container')">Manage</a></li>
		<li><a href="#" onclick="show_employee_list()">Employee</a></li>
		<li><a href="#" onclick="privilege_input('<?= encode($employee['idMsEmployee'])?>')"><?= $employee['Name']?></a></li>
	</ol>
</section>
<section class="content" >
	<div class="box box-default">
		<div class="box-body">
			<div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
					<li><a href="#tab_modul_privilege" data-toggle="tab">Modul Privilege</a></li>
                	<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1) { // Jika MD. ?>
                	<li><a href="#tab_privilege" data-toggle="tab">Privilege</a></li>
                	<?php } ?>
                	<li class="active"><a href="#tab_username" data-toggle="tab">Username & Password</a></li>
                  	<li class="pull-left header"><i class="fa fa-key"></i> 
					  	<?= $employee['EmpCode']?> <b><?= $employee['Name']?></b>
					</li>
                </ul>
                <div class="tab-content">
                	<div class="tab-pane active" id="tab_username">
						<form id="username_form">
						<input name="t_id_log" value="<?= encode($operator['idTrDataLog'])?>" hidden>
						<input name="t_id_ms_operator" value="<?= encode($operator['idMsOperator'])?>" hidden>
						<input name="t_fid_employee" value="<?= encode($employee['idMsEmployee'])?>" hidden>
						<input name="t_full_name" value="<?= $employee['Name']?>" hidden>
						<input name="t_KodeDealer" value="<?= encode($employee['KodeDealer'])?>" hidden>
						<input name="t_KodeAhass" value="<?= encode($employee['KodeAhass'])?>" hidden>
						<input name="t_KodeSales" value="<?= encode($operator['KodeSalesPerson'])?>" hidden>
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label>Username</label>
									<div class="input-group col-md-12"> 
										<input type="text" class="form-control" id="name" name = "t_name" placeholder="Username" value="<?= $operator['LoginName']?>">
									</div>
								</div>
							</div>
							<div class="col-md-2">
								
							</div>
							<div class="col-md-3">
								<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1) { ?>
								<div class="form-group"> 
								<label>Expiry Date</label>
								<div class="input-group"> 
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
										</div>
									<input type="text" class="form-control" id="expiry_date" name="t_expiry_date" placeholder="dd/mm/yyyy" value="<?= humanize_date($operator['ExpiryDate'])?>">
								</div>
								</div>
								<?php } ?>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group col-md-12"> 
										<input title="Input Password" type="password" class="form-control" id="pass1" name = "t_pass1" placeholder="password" value="<?= $operator['LoginPass']?>">
									</div>
								</div>
							</div>
							<div class="col-md-2">
								
							</div>
							<div class="col-md-3">
								<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1) { // if MD ?>
								<div class="form-group">
									<label>Role Account</label>
									<div class="input-group col-md-12"> 
										<select class="form-control select2" id="Role" name="t_Role" style="width: 100%;">
										<option value="0">Pilih</option>
										<?php foreach($group->result_array() as $row){ ?>
											<option value="<?=$row['idMsOperatorGroup']?>" <?= ($row['idMsOperatorGroup'] != $operator['fidMsOperatorGroup'])?:'selected' ?>><?=$row['Name']?></option>
										<?php }?>
										</select>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label>ReType-Password</label>
									<div class="input-group col-md-12"> 
										<input title="ReType Password" type="password" class="form-control" id="pass2" name = "t_pass2" placeholder="password" value="<?= $operator['LoginPass']?>">
									</div>
								</div>
							</div>
							
						</div>

						</form>
						<div class="box-footer">
							<button type="submit" class="btn btn-default" onclick="show_employee_list();">Close</button>
							<?php if($operator['fidMsEmployee']){?>
							<button type="submit" class="btn btn-danger" style="margin-left: 50px;" onclick="delete_user();"> Delete User</button>
							<?php }?>
							<button type="submit" class="btn btn-info pull-right" onclick="username_save();">Submit</button>
						</div>
                  	</div>
					
									<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] != 3) { // Jika Sales Tidak Dapat Menrubah Privilege. ?>
                	<div class="tab-pane" id="tab_privilege">
						<?php if ($operator['idMsOperator']) { ?>
						<form id="privilege_form">
						<input name="t_id_ms_operator" value="<?= encode($operator['idMsOperator'])?>" hidden>
						<table class="table table-bordered">
							<tr>
								<th></th>
								<th colspan=3>Main Menu</th>
								<th>Short Cut</th>
								<th>Default Open</th>
			
							</tr>
							<?php
							$i = 0; 
							foreach($menu_list->result_array() as $menu)
							{
								$i++;
								$checked = '';
								$idMenu = $menu['idAppMenu'];
								if($this->session->userdata('Operator')['idMsOperator'] != 1) {
									if($idMenu == 102 || $idMenu == 103) {
										continue;
									}
								}
								if ($menu['idPrivilege']&&$menu['Status'])
								{
									$checked = 'checked';
								}

								?>
								<tr>
								<td width="15px"><?= $i?>
								</td>
								<?php if ($menu['MenuLevel']==1){?>
								<td colspan=3>
									<label>
									<input type="checkbox" class="minimal check_priv" id="menu<?= $idMenu ?>" name="ch_menu<?= $idMenu ?>" <?= $checked; ?>>
									<?= $menu['Title']?>
									</label>
									<small><?= $menu['Description']?'<br>'.$menu['Description']:''?></small>
									
								</td>
								<?php }?>
								
								<?php if ($menu['MenuLevel']==2){?>
									<td width="20px"></td>
									<td colspan=2>
									<label>
									<input type="checkbox" class="minimal check_priv" id="menu<?= $idMenu ?>" name="ch_menu<?= $idMenu ?>" <?= $checked; ?>>
									<?= $menu['Title']?>
									</label>
								</td>
								<?php }?>
								
								<?php if ($menu['MenuLevel']==3){?>
									<td width="20px"></td>
									<td width="20px"></td>
									<td colspan=1>
									<label>
									<input type="checkbox" class="minimal check_priv" id="menu<?= $idMenu ?>" name="ch_menu<?= $idMenu ?>" <?= $checked; ?>>
									<?= $menu['Title']?>
									</label>
								</td>
								<?php }?>
								
									<td><?php
										if ($menu['ShortCutCount'])
										{
											?>
											<div class="btn-group">
											<?php
											foreach($shorcut_list[$idMenu]->result_array() as $short_cut)
											{
												$id = $short_cut['idAppShortCut'];
				
												$true = ($short_cut['idMsOperatorAppShortKey']&&$short_cut['Status']);
												$icon = ($short_cut['Icon']?:'fa-circle-o');
												$class = ($true?'btn-success':'btn-default');
												$value = ($true?1:0);
												
												?>
												<button title="<?= $short_cut['Title']?>" type="button" id="short_cut_display_<?= $id?>"
													class="btn <?= $class ?>" onclick="set_short_cut(<?= $id ?>)">
													<i class="fa <?= $icon ?>"></i>
												</button>
												<input hidden name="t_short_cut<?= $id ?>" id="short_cut<?= $id ?>" value="<?= $value?>" >
												<?php
											}
											?>
											</div>
											<?php
										}?>
									</td>
									<td style="text-align:center">
										<?php 
										if ($menu['URL']){
										?>
										<input type="radio" name="t_fid_default_menu" class="minimal" value="<?=$menu['idAppMenu']?>" <?= ($operator['fidMenuDefaultOpen']==$menu['idAppMenu']?'checked':'')?>>
										<?php }?>
									</td>
								</tr>
								
								<?php
							}
							?>
						</table>
						</form>
						<div class="box-footer">
							<button type="submit" class="btn btn-default" onclick="show_employee_list();">Close</button>
							<button type="submit" class="btn btn-info pull-right" onclick="privilege_save();">Submit</button>
						</div>
						<?php }?>
                  	</div>
                  	<?php } ?>
                </div>
            </div>
		</div>
	</div>
</section>