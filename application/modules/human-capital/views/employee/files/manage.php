<style>
	.box__dragndrop,
	.box__uploading,
	.box__success,
	.box__error {
	  display: none;
	}
	
	.box.has-advanced-upload {
	  background-color: white;
	  outline: 2px dashed black;
	  outline-offset: -10px;
	}
	.box.has-advanced-upload .box__dragndrop {
	  display: inline;
	}
</style>
<script type="text/javascript">
	$(function () {
  
	});		
	function readURL(input) {

	  if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
		  $('#preview_img').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#userfile").change(function() {
	  readURL(this);
	});
</script>
<section class="content-header">
	<h1>
		Employee <small>personal information</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Human Capital</li>
		<li><a href="#" onclick="show('main_container')">Manage</a></li>
		<li><a href="#" onclick="show_employee_list()">Employee</a></li>
		<li><a href="#" onclick="employee_file('<?= encode($employee['idMsEmployee'])?>')"><?= $employee['Name']?></a></li>
	</ol>
</section>
<section class="content" >
	<div class="box box-default">
		<form id="employee_form" method="post" enctype="multipart/form-data"  action="<?= base_url()?>human-capital/employee/files/proses_upload">
			<input name="t_id_log" value="<?//= encode($employee['idTrDataLog'])?>" hidden>
			<input name="t_id_ms_employee" value="<?= encode($employee['idMsEmployee'])?>" hidden>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<h2><?= $title ?></h2>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control"  readonly="true" id="employee_code" name="t_employee_code" placeholder="code" value="<?= $employee['Name']?>">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Code</label>
							<input type="text" class="form-control"  readonly="true" id="employee_code" name="t_employee_code" placeholder="code" value="<?= $employee['EmpCode']?>" >
						</div>
					</div>
				</div>
				<form class="box" method="post" action="" enctype="multipart/form-data">
					<div class="box__input">
						<div class="col-md-3">
							<div class="form-group">
								<input class="box__file" type="file" name="userfile[]" id="userfile" data-multiple-caption="{count} files selected" multiple />
							</div>
							<div class="form-group">
								<button class="box__button" type="submit">Upload</button>
							</div>
						</div>
					</div>
					<img id="preview_img" style="height: 50px;" src="#" alt="No Photos" />
					<div class="box__uploading">Uploading&hellip;</div>
					<div class="box__success">Done!</div>
					<div class="box__error">Error! <span></span>.</div>
				</form>
				
			</div>
		</form>
		<div class="box-footer">
			<button type="submit" class="btn btn-warning" onclick="show_employee_list();">Close</button>
		</div>
	</div>
</section>