<style>
.cam{
	position: absolute;
	right: 1%;
	border: 1px solid #3c8dbc;
	color: #3c8dbc;
	bottom: 1px;
	border-radius: 20%;
}
.cam:hover{
	color: #ffffff;
	border: 1px solid #ffffff;
}
</style>
<?php 
$no = ($paging['current']-1) * $paging['limit'] ;
foreach($list->result_array() as $row)
{
	$status = '';
	if ($row['ApprovalStatus']==9){
		$status = '<span class="label label-success pull-right">Approved</span>';
	} else {
		$status = '<span class="label label-warning pull-right">Draft</span>';
	}
	/* if ($row['isSales']==1){
		$isSales = '<span class="label label-info pull-right">Sales</span>';
	} else {
		$isSales = '<span class="label label-warning pull-right">Bukan Sales</span>';
	} */
	$isSales = '';
	if ($row['hasaccount']){
		$hasAccount = '<span class="label label-primary pull-right">'.$row['LoginName'].'</span>';
	} else {
		$hasAccount = '<span class="label label-warning pull-right">Tidak Punya Akun</span>';
	}
	$id_encoded = encode($row['idMsEmployee']);
	?>
	<li class="item">
	  <div class="col-md-2" style="width: 100px;">
	  		<a href="javascript:void(0);" onclick="changePict('<?= encode($row['idMsEmployee'])?>',this)" class="fa fa-fw fa-camera cam" title="Change Profile Picture"></a>
			<!-- <a href="javascript:void(0);" onclick="employee_profile('<?= encode($row['idMsEmployee']) ?>')"> -->
				<a href="javascript:void(0);">
	  			<img src="<?= get_profile_pict($row['idMsEmployee'],'90x90');?>" alt="Employee Image">
	  		</a>
	  </div>
	  <div class=" col-md-10">
	  	<span class="pull-right">
	  		<?= $status."<br>".$hasAccount ?>
	  	</span>
	    <a class="product-title"><?= match_key($row['Name'],$key) ?> </a>
	    <span class="product-description">
	      
	    </span>

	    <span class="product-description">
	      <?= match_key($row['EmpCode'],$key).' : '.$row['PositionDesription']?>
	    </span>
	    
	    <span class="product-description">
	      <?= match_key($row['Address'],$key).' '.match_key($row['City'],$key)?>
	    </span>
	    
	  	
	  	 <span class="product-description">
	 	 <a href="#" onclick="employee_input('<?= $id_encoded ?>');" class="fa fa-fw fa-edit" title="edit data"></a>
	 	 <?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1) { ?>
	 	 <a href="#" onclick="privilege_input('<?= $id_encoded ?>');"  class="fa fa-fw fa-key" title="Buka Privillage"></a>
	 	 <?php } ?>
	 	 <?php if($this->session->userdata('Operator')['idMsOperator'] == 1 && $row['hasaccount']) { ?>
	 	 <a href="#" onclick="switchUser('<?= encode($row['hasaccount']) ?>','<?= $row['Name']?>');"  class="fa fa-fw fa-user-secret" title="login as <?= $row['Name'].$row['hasaccount']?>"></a>
	 	 <?php } ?>
	 	 <!---
		 <a href="#" onclick="employee_file('<?= $id_encoded ?>')" class="fa fa-fw fa-camera" title="Change Profile Picture"></a>
	 	 <a href="#" class="fa fa-fw fa-code-fork" title="Promote"></a>
		 -->
	  	</span></div>
	</li>
<?php }?>

<div class="clearfix"></div>
<hr>
<?=$paging['list']?>