<script type="text/javascript">
	$(function () {
		$(".select2").select2();
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
   		objDate("date_of_birth");

   	$('.ktpMask').mask('0000000000000000');

	});		
	function employee_save()
	{
		$('#load').button('loading');
		setTimeout(function () {
			$('#load').button('reset');
		}, 3000);
		showProgres();
		$.post(site_url+'human-capital/employee/employee/save'
			,$('#employee_form').serialize()
			,function(result) {
				$('#load').button('reset');
				hideProgres();
				if (result.message)
				{
					// toastr.success(result.message,'Save');
					// employee_input(result.id_employee_encoded);		
					return_employee_list();			
				}else
				{
					// clean error sign
					$("#employee_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					// show error message
					toastr.error(result.error,'Error');
				}
			}					
			,"json"
		);
	}
	
</script>
<section class="content-header">
	<h1>
		Karyawan <small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Human Capital</li>
		<li><a href="#" onclick="show('main_container')">Manage</a></li>
		<li><a href="#" onclick="return_employee_list()">Karyawan</a></li>
		<?php if ($employee['idMsEmployee']) {?>
		<li><a href="#" onclick="employee_input('<?= encode($employee['idMsEmployee'])?>')"><?= $employee['Name']?></a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<section class="content" >
	<div class="box box-default">
		<form id="employee_form">
			<input name="t_id_log" value="<?= encode($employee['idTrDataLog'])?>" hidden>
			<input name="t_id_ms_employee" value="<?= encode($employee['idMsEmployee'])?>" hidden>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						<h2><?= $title ?></h2>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Code</label>
							<input type="text" class="form-control"  readonly="true" id="employee_code" name="t_employee_code" placeholder="code" value="<?= $employee['EmpCode']?>">
						</div>
						<?php /* if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1) { $kode = $employee['KodeDealer']."--".$employee['KodeAhass'];?>
						<div class="form-group">
							<label>Dealer</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="KodeDealer" name="t_KodeDealer" style="width: 100%;">
								<option value="MD--00">MD | Main Dealer</option>
								</select>
							</div>
						</div>
						<?php } else { ?>
						<?php
						if($employee['KodeDealer'] && $employee['KodeAhass']){
							$kode = $employee['KodeDealer']."--".$employee['KodeAhass']; 
						} else {
							$kode = $this->session->userdata('Dealer')['KodeDealer']."--".$this->session->userdata('Dealer')['IDAHM'];
						}
						?>
						<input type="hidden" readonly="true" id="KodeDealer" name="t_KodeDealer" value="<?= $kode; ?>">
						<?php } */ ?>
						<!--div class="form-group">
							<label>Jabatan</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="Jabatan" name="t_Jabatan" style="width: 100%;">
									<option value="">Pilih</option>
									<option value="Pemilik/Investor" <?= ($employee['Jabatan']!='Pemilik/Investor')?:'selected'; ?>>Pemilik/Investor</option>
									<option value="Pemilik / PIC Dealer" <?= ($employee['Jabatan']!='Pemilik / PIC Dealer')?:'selected'; ?>>Pemilik / PIC Dealer</option>
									<option value="Sales Supervisor" <?= ($employee['Jabatan']!='Sales Supervisor')?:'selected'; ?>>Sales Supervisor</option>
									<option value="Sales Koordinator" <?= ($employee['Jabatan']!='Sales Koordinator')?:'selected'; ?>>Sales Koordinator</option>
									<option value="Senior Salesman" <?= ($employee['Jabatan']!='Senior Salesman')?:'selected'; ?>>Senior Salesman</option>
									<option value="Salesman" <?= ($employee['Jabatan']!='Salesman')?:'selected'; ?>>Salesman</option>
									<option value="Junior Salesman" <?= ($employee['Jabatan']!='Junior Salesman')?:'selected'; ?>>Junior Salesman</option>
									<option value="Trainee Salesman" <?= ($employee['Jabatan']!='Trainee Salesman')?:'selected'; ?>>Trainee Salesman</option>
									<option value="Senior Sales Counter" <?= ($employee['Jabatan']!='Senior Sales Counter')?:'selected'; ?>>Senior Sales Counter</option>
									<option value="Sales Counter" <?= ($employee['Jabatan']!='Sales Counter')?:'selected'; ?>>Sales Counter</option>
									<option value="Junior Sales Counter" <?= ($employee['Jabatan']!='Junior Sales Counter')?:'selected'; ?>>Junior Sales Counter</option>
									<option value="Trainee Sales Counter" <?= ($employee['Jabatan']!='Trainee Sales Counter')?:'selected'; ?>>Trainee Sales Counter</option>
									<option value="Delivery Man" <?= ($employee['Jabatan']!='Delivery Man')?:'selected'; ?>>Delivery Man</option>
									<option value="Kasir" <?= ($employee['Jabatan']!='Kasir')?:'selected'; ?>>Kasir</option>
									<option value="Admin STNK / BPKB" <?= ($employee['Jabatan']!='Admin STNK / BPKB')?:'selected'; ?>>Admin STNK / BPKB</option>
									<option value="Satpam" <?= ($employee['Jabatan']!='Satpam')?:'selected'; ?>>Satpam</option>
									<option value="Safety Riding Advisor" <?= ($employee['Jabatan']!='Safety Riding Advisor')?:'selected'; ?>>Safety Riding Advisor</option>
									<option value="Mekanik PDI" <?= ($employee['Jabatan']!='Mekanik PDI')?:'selected'; ?>>Mekanik PDI</option>
									<option value="Big Bike Manager" <?= ($employee['Jabatan']!='Big Bike Manager')?:'selected'; ?>>Big Bike Manager</option>
									<option value="Big Bike Consultan" <?= ($employee['Jabatan']!='Big Bike Consultan')?:'selected'; ?>>Big Bike Consultan</option>
									<option value="Big Bike Assistant" <?= ($employee['Jabatan']!='Big Bike Assistant')?:'selected'; ?>>Big Bike Assistant</option>
									<option value="Wing Sales People" <?= ($employee['Jabatan']!='Wing Sales People')?:'selected'; ?>>Wing Sales People</option>
									<option value="SWAT" <?= ($employee['Jabatan']!='SWAT')?:'selected'; ?>>SWAT</option>
									<option value="Admin CRM H1" <?= ($employee['Jabatan']!='Admin CRM H1')?:'selected'; ?>>Admin CRM H1</option>
									<option value="Admin Warehouse" <?= ($employee['Jabatan']!='Admin Warehouse')?:'selected'; ?>>Admin Warehouse</option>
									<option value="Admin Lain-lain" <?= ($employee['Jabatan']!='Admin Lain-lain')?:'selected'; ?>>Admin Lain-lain</option>
									<option value="PIC CRM H1" <?= ($employee['Jabatan']!='PIC CRM H1')?:'selected'; ?>>PIC CRM H1</option>
									<option value="PDI-man" <?= ($employee['Jabatan']!='PDI-man')?:'selected'; ?>>PDI-man</option>
									<option value="Greeter" <?= ($employee['Jabatan']!='Greeter')?:'selected'; ?>>Greeter</option>
									<option value="Security" <?= ($employee['Jabatan']!='Security')?:'selected'; ?>>Security</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Salesman</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="isSales" name="t_isSales" style="width: 100%;" onchange="set_sales(this.value)">
									<option value="0" <?= ($employee['isSales'])?:'selected'; ?>>Bukan Sales</option>
									<option value="1" <?= (!$employee['isSales'])?:'selected'; ?>>Sales</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Id FLP</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control"  id="KodeSales" name="t_KodeSales" placeholder="Kode Sales"
								<?php
								if($this->session->userdata('Operator')['fidMsOperatorGroup'] != 1){
									echo 'readonly="readonly"';
								} if($employee['KodeSalesAHM']){
									echo 'value="'.$employee['KodeSalesAHM'].'" readonly="readonly"';
								}
								?>>
							</div>
						</div>
						<input type="hidden" id="IsSalesCurrent" name="t_IsSalesCurrent" value="<?= $employee['isSales']?>">
						<input type="hidden" id="KodeSalesCurrent" name="t_KodeSalesCurrent" value="<?= $employee['KodeSalesAHM']?>"-->
						<div class="form-group">
							<label>Nomor KTP</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control ktpMask" id="NoKTP" name = "t_NoKTP" placeholder="Nomor KTP" value="<?= $employee['IDCardNumber']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Nama Lengkap</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="name" name = "t_name" placeholder="Name" value="<?= $employee['Name']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Nama Panggilan</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control"  id="nick_name" name="t_nick_name" placeholder="nick name" value="<?= $employee['NickName']?>">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group"> 
							<label>Tanggal Lahir</label>
							<div class="input-group"> 
								<div class="  input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control" id="date_of_birth" name="t_date_of_birth" placeholder="dd-mm-yyyy" value="<?= humanize_date($employee['DateOfBirth'])?>">
							</div>
						</div>
						<div class="form-group">
							<label>Tempat Lahir</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control"  id="city_of_birth" name="t_city_of_birth" placeholder="City of birth" value="<?= $employee['CityOfBirth']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label> <br>
							<label>
								<input type="radio" name="t_gender" value = "1" class="minimal" <?= ($employee['fidGender']==1?'checked':'')?> > Pria
							</label>
							<label>
								<input type="radio" name="t_gender" value = "2" class="minimal" <?= ($employee['fidGender']==2?'checked':'')?> > Wanita
							</label>
						</div>
						<div class="form-group">
							<label>Telepon Keluarga</label>
							<div class="input-group col-md-12"> 
								<div class="  input-group-addon">
									<i class="fa fa-phone"></i>
								</div>
								<input type="text" class="form-control" id="phonenumber1" name="t_phonenumber1" placeholder="phonenumber" value="<?= $employee['PhoneNumber1']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Nomor Telepon HP</label>
							<div class="input-group col-md-12"> 
								<div class="  input-group-addon">
									<i class="fa fa-mobile-phone"></i>
								</div>
								<input type="text" class="form-control" id="phonenumber2" name="t_phonenumber2" placeholder="phonenumber" value="<?= $employee['PhoneNumber2']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Email</label>
							<div class="input-group col-md-12"> 
								<div class="  input-group-addon">
									<i class="fa fa-envelope-o"></i>
								</div>
								<input type="email" class="form-control" id="email" name="t_email" placeholder="email" value="<?= $employee['Email']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Agama</label>
							<select class="form-control select2" style="width: 100%;" name="t_religion">
							<option value="0">Pilih</option>
							<?php foreach($religion->result_array() as $row){ ?>
								<option value="<?= $row['idMsReligion']?>" <?= ($row['idMsReligion']==$employee['fidMsReligion']?'selected="selected"':'')?>><?= $row['Description']?></option>
							<?php }?>
							</select>
						</div>
						<div class="form-group">
							<label>Pendidikan</label>
							<select class="form-control select2" id="Pendidikan" name="t_Pendidikan" style="width: 100%;">
								<?php foreach($pendidikan->result_array() as $row){ ?>
								<option value="<?=$row['Code']?>" <?= ($row['Code']==$employee['fidPendidikan']?'selected="selected"':'')?>><?=($row['Description']!="NULL")? $row['Description']:"Pilih";?></option>
								<?php }?>
							</select>
						</div>
						<div class="form-group">
							<label>Hobi</label>
							<select class="form-control select2" id="Hobi" name="t_Hobi" style="width: 100%;">
								<option value="N">Pilih</option>
								<?php foreach($hobi->result_array() as $row){ ?>
								<option value="<?=$row['Code']?>" <?= ($row['Code']==$employee['fidHobi']?'selected="selected"':'')?>><?=($row['Description']!="NULL")? $row['Description']:"Pilih";?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Alamat Tinggal</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" rows="3" placeholder="" name="t_address"><?= $employee['Address']; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label>Provinsi</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Provinsi" name="t_Provinsi" placeholder="Provinsi" value="<?= $employee['Provinsi']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kota</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="nick_name" name="t_city" placeholder="Kota" value="<?= $employee['City']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kode Pos</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KdPos" name="t_KdPos" placeholder="Kode Pos" value="<?= $employee['KodePos']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>Alamat Tinggal (KTP)</label>
							<div class="input-group col-md-12"> 
								<textarea class="form-control" rows="3" name="t_address_ktp"><?= $employee['Address_KTP']; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label>Provinsi (KTP)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Provinsi_ktp" name="t_Provinsi_ktp" placeholder="Provinsi" value="<?= $employee['Provinsi_KTP']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kota (KTP)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="Kota_ktp" name="t_Kota_ktp" placeholder="Kota" value="<?= $employee['City_KTP']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label>Kode Pos (KTP)</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="KdPos_ktp" name="t_KdPos_ktp" placeholder="Kode Pos" value="<?= $employee['KodePos_KTP']; ?>">
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</form>
		<div class="box-footer">
			<button type="submit" class="btn btn-warning" onclick="return_employee_list();">Close</button>
			<button type="submit" class="btn btn-info pull-right" onclick="employee_save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Submit</button>
		</div>
	</div>
</section>