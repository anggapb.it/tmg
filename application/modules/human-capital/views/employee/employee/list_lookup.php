<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>Code</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Kode AHASS</th>
				<!--th>*</th-->
			</tr>
			<tr>
		</thead>
		<tbody>
			<?php
					if($list->num_rows() <= 0 ){
						?>
						<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
					<?php
					}else{
						$no = ($paging['current']-1) * $paging['limit'] ;
						foreach($list->result_array() as $row){
						$no++;
						
					?>
			<tr data-dismiss="modal" onClick="lookupSelectEmployee('<?=$row['EmpCode']?>')" style="cursor:pointer;" title="Klik disini <?=$row['EmpCode']?>">
				<td><?=$no?></td>
				<td><?= match_key($row['EmpCode'],$key)?></td>
				<td><?= match_key($row['Name'],$key)?></td>
				<td><?= $row['PhoneNumber2']?$row['PhoneNumber1'].' / '.$row['PhoneNumber2']:$row['PhoneNumber1']?></td>
				<td><?= $row['KodeAhass']?></td>
				<!--td>
					<a href="javascript:void(0);" class="btn btn-success"  data-dismiss="modal" onClick="lookupSelectEmployee('<?=$row['EmpCode']?>')"><i class="fa fa-check"></i> Pilih</a>
				</td-->
			</tr>
			<?php }
					
					}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>