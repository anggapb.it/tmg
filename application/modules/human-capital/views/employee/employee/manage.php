<script type="text/javascript">
	$(function () {
		$(".select2").select2();
		hideProgres();
   		pageLoadEmployee(1);
	});		
	function pageLoadEmployee(pg)
	{	
		$('#employee_list').html('');
		showProgres();
		copy_filter_data();
		$.post(site_url+'human-capital/employee/employee/page/'+pg
			,$('#filter_content').serialize()
			,function(result) {
				hideProgres();
				$('#employee_list').html(result);
			}					
			,"html"
		);
	}

	function refresh()
	{
		$('#refresh').val(1);
		$('#employee_list').html('');
		loadNextPage = true;
		get_list();
	}
	
	function get_list()
	{	
		if (loadNextPage == false)
		{	
			return false;
		}
		
		loadNextPage = false;
		showProgres();
		copy_filter_data();
		$.post(site_url+'human-capital/employee/employee/get_list'
			,$('#filter_content').serialize()
			,function(result) {
				$('.load-more').remove();
				$('#employee_list').append(result);
				hideProgres();
				loadNextPage = true;
				pageLoadEmployee(1);
			}					
			,"html"
			);
		$('#refresh').val(0);
	}
	function copy_filter_data()
	{
		$('#search_key').val($('#search_key_show').val());
		$('#dealer_key').val($('#KodeDealer').val());
	}
	function employee_profile(id)
	{
		loadNextPage = false;
		$('#employee_list_container').hide();
		$('#employee_input_container').show();
		//
		showProgres();
		$.post(site_url+'human-capital/employee/profile/info/'+id
			,{}
			,function(result) {
				$('#employee_input_container').html(result);
				hideProgres();
			}					
			,"html"
			);
	}
	function employee_input(id)
	{
		loadNextPage = false;
		$('#employee_list_container').hide();
		$('#employee_input_container').show();
		//
		showProgres();
		$.post(site_url+'human-capital/employee/employee/input/'+id
			,{}
			,function(result) {
				$('#employee_input_container').html(result);
				hideProgres();
			}					
			,"html"
			);
	}
	
	function show_employee_list()
	{
		loadNextPage = true;
		$('#employee_list_container').show();
		$('#employee_input_container').hide();
		refresh();
	}

	/// privilege
	function privilege_input(id)
	{
		loadNextPage = false;
		$('#employee_list_container').hide();
		$('#employee_input_container').show();
		//
		showProgres();
		$.post(site_url+'human-capital/employee/privilege/input/'+id
			,{}
			,function(result) {
				$('#employee_input_container').html(result);
				hideProgres();
			}					
			,"html"
			);
	}
	
	/// upload image
	function employee_file(id)
	{
		loadNextPage = false;
		$('#employee_list_container').hide();
		$('#employee_input_container').show();
		//
		showProgres();
		$.post(site_url+'human-capital/employee/files/manage/'+id
			,{}
			,function(result) {
				$('#employee_input_container').html(result);
				hideProgres();
			}					
			,"html"
			);
	}
	function changePict(id,obj)
	{
		upclick(
		{
			element: obj,
			action: site_url+'human-capital/employee/employee/change_pict/'+id, 
			onstart:
			function(filename)
			{
					//alert('Start upload: '+filename);
				},
				oncomplete:
				function(response_data) 
				{
					refresh();
				}
			});
	}
	function switchUser(id,name)
	{
		 if(confirm("Tampilkan Data User Sebagai : "+ name)){
			  $.post(site_url+'human-capital/employee/employee/login_as'
				  ,{t_id_operator : id}
				  ,function(result) {	
					if (result.message)
					{
						alert('ok berhasil');
						location.reload();
					}else{
						alert(result.error);
					}
				  },
			  "json");
			}
	}
	
	function show_filterEmployee()
	{
		var tp = $('#effectEmployee').val();
		if(tp==1){
			$( "#toggle_filterEmployee" ).toggle( "blind","down" );
			// $( "#toggle_filterEmployee_browse" ).toggle( "blind","down" );
			$('#effectEmployee').val(2);
		}else{
			$( "#toggle_filterEmployee" ).toggle( "blind","down" );
			// $( "#toggle_filterEmployee_browse" ).toggle( "blind","down" );
			$('#effectEmployee').val(1);
		}
		
	}
	function return_employee_list()
	{
		$('#employee_list_container').show();
		$('#employee_input_container').hide();
		
		refresh();
	}
</script>
<div id="employee_list_container">
	<section class="content-header">
		<h1>
			Karyawan <small></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Karyawan</li>
			<li><a href="#" onclick="show('main_container')">Manage</a></li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div>
							<div class="btn-group">
								<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1 || $this->session->userdata('Operator')['fidMsOperatorGroup'] == 2) { ?>
								<button type="button" class="btn btn-warning" onclick="employee_input()">New</button>
								<?php } ?>
							</div>
							<!--div class="btn-group">
								<button type="button" class="btn btn-default">Print</button>
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div-->
							<div class="btn-group pull-right">
								<?php if($this->session->userdata('Operator')['fidMsOperatorGroup'] == 1 || $this->session->userdata('Operator')['fidMsOperatorGroup'] == 2) { ?>
								<button type="button" class="btn btn-warning" onClick="show_filterEmployee()">Show Filter</button>
								<?php } ?>
							</div>
						</div>
						<div>
							<form id="filter_content">
								<input name="t_refresh" id="refresh" value=1 hidden>
							
								<div id="toggle_filterEmployee" hidden>
									<div class="row"><br>
										<div class="form-inline">
											<div class='col-sm-1'>
											</div>
											<div class='col-sm-7'>
												<div class="form-group">
													<label>Search</label>
													<input type="text" name="t_search_key" class="form-control" id="search_key_show"  placeholder="" value="" onkeydown="if (event.keyCode == 13) pageLoadEmployee(1)">
												</div>
												<button type="button" class="btn btn-primary pull-right" onClick="pageLoadEmployee(1)">Browse</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<ul class="products-list product-list-in-box" id="employee_list">

							</ul>
						</div>
						
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="employee_input_container" hidden></div>
<script>
	$(document).ready(function(){
		$('input[type="checkbox"]').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue'
		});
	})
	
</script>