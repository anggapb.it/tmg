<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_modul_privilege_model extends Base_Model {

	function __construct() {

    parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperatorSpecial');
		$this->set_pk('idOperatorSpecial');
		$this->set_log(false);
    }	
		
	function get_list()
	{
		$this->db->select('tbl.*');
				
		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}
		
		if(!$this->order_by) {
			$this->db->order_by($this->primary_key, 'ASC');
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}

}

/* End of file Employee_model.php */
/* Location: ./application/modules/human-capital/models/employee/employee_model.php */
