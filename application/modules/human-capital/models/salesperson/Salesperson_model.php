<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salesperson_model extends Base_Model {

	function __construct() {

    parent::__construct();
		$this->set_schema('h1');
		$this->set_table('msSalesPerson');
		$this->set_pk('Code');
		$this->set_log(false);
  }	
		
	function get_newcode($KodeDealer)
	{
		$value = '';
		$this->db->select('Code');
		$this->db->like('Code',$KodeDealer,'after');
		$this->db->order_by('Code','desc');
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl',1);
		$row = $query->row_array();
		//
		$counter = 0;
		if ($row['Code']){
			// $counter = substr($row['Code'],3);
			// $counter = substr($counter + 1001,1);
			// $value = $KodeDealer.'-'.$counter;
			$value = $KodeDealer."-".str_pad(substr(trim($row['Code']),-3)+1,3,'0',STR_PAD_LEFT);
		} else {
			$value = $KodeDealer."-001";
		}

		return $value;
	}

	function cek_employee($id){
		$this->db->select('Code');
		$this->db->where('idMsEmployee',$id);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl',1,1);
		$row = $query->row_array();
		return $row['Code'];
	}

}