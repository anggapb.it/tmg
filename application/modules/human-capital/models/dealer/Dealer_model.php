<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer_model extends Base_Model {

	function __construct() {

    parent::__construct();
		$this->set_schema('dataMaster');
		$this->set_table('msDealer');
		$this->set_pk('KodeDealer');
		$this->set_log(false);
   }	
		
}