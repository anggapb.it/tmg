<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Religion_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('humanCapital');
		$this->set_table('msReligion');
		$this->set_pk('idMsReligion');
		$this->set_log(true);
    }	
}
