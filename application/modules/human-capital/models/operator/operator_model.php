<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_model extends Base_Model {

	function __construct() {

    parent::__construct();
		$this->set_schema('public');
		$this->set_table('msOperator');
		$this->set_pk('idMsOperator');
		$this->set_log(false);
    }	
		
		
  function get_id(){
		$this->db->select('tbl."idMsOperator"');
		$this->db->order_by('idMsOperator','desc');
		$res = $this->db->get($this->schema.'.'.$this->table.' tbl',1)->row();
		if($res){
			return $res->idMsOperator + 1;
		} else {
			return 1;
		}
	}

}

/* End of file Employee_model.php */
/* Location: ./application/modules/human-capital/models/employee/employee_model.php */
