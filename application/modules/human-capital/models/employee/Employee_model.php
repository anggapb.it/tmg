<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('humanCapital');
		$this->set_table('msEmployee');
		$this->set_pk('idMsEmployee');
		$this->set_log(false);
    }	
		
    function get_list()
	{
		// $this->db->select('tbl.*, dl.Nama as NamaDealer, op.idMsOperator as hasaccount');
		$this->db->select('tbl.*, op.idMsOperator as hasaccount, op.LoginName');
		// join to msOrganizationStructure
		$this->db->select('ps.Description as PositionDesription');
		$this->db->join('humanCapital.msPosition ps','tbl.fidMsPosition = ps.idMsPosition','left');
		// $this->db->join('dataMaster.msDealer dl','tbl.KodeDealer = dl.KodeDealer','left');
		$this->db->join('public.msOperator op','tbl.idMsEmployee = op.fidMsEmployee','left');
		
		//
		$this->db->where($this->where);
		
		/* if($this->session->userdata('Operator')['idMsOperator'] != 1) {
			$this->db->where('op.idMsOperator != 1');
		} */

		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		$this->db->where($this->where);
		
		/* if($this->session->userdata('Operator')['idMsOperator'] != 1) {
			$this->db->where('op.idMsOperator != 1');
		} */

		$this->db->join('public.msOperator op','tbl.idMsEmployee = op.fidMsEmployee','left');
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}

	function gen_new_code()
	{
		$value = '';
		$this->db->select('EmpCode');
		$this->db->order_by('EmpCode','desc');
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl',1);
		$row = $query->row_array();
		//
		$counter = 0;
		if ($row['EmpCode'])
			$counter = substr($row['EmpCode'],9);
		$counter = substr($counter + 10001,1);
		$value = 'TMG.'.date('ym').'-'.$counter;

		return $value;
	}

	function get_aja()
	{
		$where['PhoneNumber2 > PhoneNumber1'] = null;
		$this->db->where($where);
		
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl',1,1);
		echo $this->db->last_query();
	}

	function get_dealerEmployee($id){
		$this->db->select('KodeDealer');
		$this->db->where('idMsEmployee',$id);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl',1);
		$row = $query->row_array();
		return $row['KodeDealer'];
	}

}

/* End of file Employee_model.php */
/* Location: ./application/modules/human-capital/models/employee/employee_model.php */
