<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		// $this->load->model('master.dealer/dealer/dealer_model');
		$this->load->model('engine/menu_model');
		$this->load->model('main_model');
		// $this->load->model('transfer.kain/transfer_kain_model');
	}
	
	public function index()
	{
		// $this->main_model->syncData(); 	
		$id_operator = $this->session->userdata('Operator')['idMsOperator'];
		// get menu default
		$opr = $this->session->userdata('Operator');
		$menu = $this->menu_model->get($opr['fidMenuDefaultOpen']);
		// $this->dealer_model->set_order(array('KodeDealer'=>'Asc'));
		// $dealer =  $this->dealer_model->get_list();
		$data = array();
		$data['name'] = $this->session->userdata('Operator')['LoginName'];
		$data['content'] = 'main';
		// $data['dealer'] = $dealer;
		$data['menu'] =	$menu;
		$privTotalTransferKain = $this->menu->msOperatorSpecial(array('SpecialVar' => 'set_notif_terima_kain_internal'));;//check_priv('NotifReqRegCustomer');
		// $tfkain_count = $this->transfer_kain_model->get_total_request();
		// $data['TotalTransferKain'] = ISSET($tfkain_count['TotalTransferKain'])?$tfkain_count['TotalTransferKain']:0;
		$data['privTotalTransferKain'] =	$privTotalTransferKain;
		
		$this->load('tpl', $data['content'], $data);
	}

	function dashboard()
	{
		$data = array();
		$data['name'] = $this->session->userdata('Operator')['LoginName'];
		$data['content'] = 'utilities/dashboard/main';
		$data['title'] = 'Dashboard';
		
		$this->load->view($data['content'], $data);
	}

	/* function setAsDealer(){
		$KodeDealer = $this->input->post('t_kodeDealer');
		$newdata['Dealer']	= $this->dealer_model->get($KodeDealer);
		
		$this->session->set_userdata($newdata);	
	} */

}

