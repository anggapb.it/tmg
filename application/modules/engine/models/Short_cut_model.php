<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Short_cut_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('AppShortCut');
		$this->set_pk('idAppShortCut');
		$this->set_log(false);
    }	
	function get_list($id=0)
	{
		$this->db->select('tbl.*');
		$this->db->where($this->where);
		$this->db->like($this->like);
        $sql = '(select * from "msOperatorAppShortCut" where "fidMsOperator" = '.$id.') opr';
        $this->db->select('opr.*');
        $this->db->join($sql,'tbl.idAppShortCut = opr.fidAppShortCut','left');
		$this->db->order_by('OrderBy','asc');
		
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}	
}
