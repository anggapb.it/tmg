<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('AppMenus');
		$this->set_pk('idAppMenu');
		$this->set_log(false);
    }	
	function get_menu_by_user($id)
	{
		$sql = 
<<<EOT
with RECURSIVE appmenulist as (
	select am0."idAppMenu",am0."Title",am0."Description",am0."URL",am0."fidAppMenu",am0."GroupMenu",am0."OrderBy"
	,am0."GroupMenu"||cast(am0."fidAppMenu" as text)||substr(cast(am0."OrderBy"+1000 as text),2) as "NewOrder"
	,cast(1 as int) as "MenuLevel"
	from "AppMenus" am0
	where am0."fidAppMenu" = 0
	union 
	select am1."idAppMenu",am1."Title",am1."Description",am1."URL",am1."fidAppMenu",aml."GroupMenu",am1."OrderBy"
	,aml."NewOrder"||cast(am1."OrderBy" as text) as "NewOrder"
	,cast(aml."MenuLevel"+1 as int) as "MenuLevel"
	from "AppMenus" am1
	inner join appmenulist aml on aml."idAppMenu" = am1."fidAppMenu"
	)
select menu.*,mop."idPrivilege",mop."Status",COALESCE(apc."ShortCutCount",0) as "ShortCutCount"
from appmenulist menu
left join (select *
		from "msOperatorPrivilege"
		where "fidMsOperator" = $id) mop 
	on menu."idAppMenu" = mop."fidAppMenu"
left join (select "fidAppMenu",count(*) as "ShortCutCount"
	from "AppShortCut"
	group by "fidAppMenu" ) apc
	on menu."idAppMenu" = apc."fidAppMenu"
order by "NewOrder";
EOT;
	$query = $this->db->query($sql);
	return $query;
	}
}
