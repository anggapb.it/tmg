<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=get_myconf('ConfigKeyValue5'); ?></title>
    <link rel="shortcut icon" href="<?= base_url().get_myconf('ConfigKeyValue7'); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/thirdparty/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url();?>assets/assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/dist/css/AdminLTE.css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif] onload="window.print();" -->
  </head>
  <style>
				@media print{@page {size: potrait}}
				@media screen, print {
					.draft_stamp{
						background: url(<?=base_url()?>assets/images/stamp_draft.png) center no-repeat;
						background-size: contain;
					}
				}
				.right {
					text-align:right;
				}
				.f9{
					font-size: 9px;
				}
				
				.col-head{
					font-weight: 600;
					text-align:center;
					background:#717070;
					color:white;
				}
				.table{
					padding:2px;
				}
				
				@page:left {
					@bottom-right {
						margin: 10pt 0 30pt 0;
						border-top: .25pt solid #666;
						content: "Our Cats";
						font-size: 9pt;
						color: #333;
					}

					@bottom-left { 
						margin: 10pt 0 30pt 0;
						border-top: .25pt solid #666;
						content: counter(page);
						font-size: 9pt;
					}
				}
				table.detail{
					border: 1px solid black;
					border-collapse:collapse;
					font-size: 7pt;
				}

				table.detail tr th{
					border: 1px solid black;
				}
				table.detail tr td{
					border: 1px solid black;
					padding-left: 6px;
					padding-right: 6px;
					padding: 0px;

				}
			</style>
			
  <body class="<?=$order['fidStatusOrder']>'10'?'':'draft_stamp'?>">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice" style="background: none;">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
            <h2 class="page-header">
			  <!--img src="<?= base_url().get_myconf('ConfigKeyValue7'); ?>" style="height: 30px;"/--> 
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$title?><br>
			  <?= '<small style="margin: 0px 0px 0px 27px;">'.get_myconf('ConfigKeyValue9').'</small>' ?>
            <table class="pull-right" style="font-weight: bold;margin: -35px 0px 0px 0px;">
				<?php
					$no_po = '<i>Tidak bisa tampil dalam mode PREVIEW</i>';
					if($tp <> 'preview'){
						// if($order['Status'] >= 10){
							$no_po = $order['NoOrder'];
						// }
					}
					
					if($tp == 1)
						$no_po = $order['NoOrder'];
				?>
				<tr>
					<td style="font-size: 11pt;">No Order</td>
					<td style="font-size: 11pt;">&nbsp;:&nbsp;</td>
					<td style="font-size: 11pt;"></b> <?=$no_po?><br></td>
				<tr>
					<td style="font-size: 11pt;">Tgl Order</td>
					<td style="font-size: 11pt;">&nbsp;:&nbsp;</td>
					<td style="font-size: 11pt;"><b> <?=humanize_mdate($order['TglOrder'],'%d-%m-%Y')?></b></td>
				</tr>
			</table>
			  	
			</h2>
          </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info"  style="border-bottom: solid 3px;border-bottom-style:double">
          <div class="col-sm-4 invoice-col">
			<table style="width:150px;margin-left:30px">
				<tr>
                  <td>Nama Kostumer</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$customer['NamaLengkap']?></td>
                </tr>
				<tr>
                  <td>Alamat</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$customer['Alamat']?></td>
                </tr>
				
				<tr>
                  <td>No. HP</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$customer['NoHP']?></td>
                </tr>
				
			</table>
             
          </div><!-- /.col -->
          <div class="col-sm-4 invoice-col">
			<table style="width:250px;margin-left:30px">
				<tr>
                  <td>Tgl Kirim</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?= humanize_mdate($order['TglKirim'], '%d-%m-%Y') ?></td>
                </tr>
				
				<tr>
                  <td>Jenis Trip</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$order['JenisTrip']?></td>
                </tr>
				<tr>
                  <td>Pengemudi</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$pengemudi['NamaLengkap']?></td>
                </tr>
				
			</table>
          </div>
          
        </div><!-- /.row -->
		<br>
        <!-- Table row -->
        <div class="row invoice-info">
			<div class="col-xs-12">
				<table class="table detail">
					<thead>
						<tr>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">No</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">Nama Biaya</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">Nominal</th>
						</tr>
						
					</thead>
				    <tbody>
					<?php 
						$no = 0;
						
						if($detail_order->num_rows() <= 0){
						?>
						<i>Data tidak ditemukan...</i>
						<?php
						}else{
							foreach($detail_order->result_array() as $row)
						{
							$no++;			
							
						?>
						<tr>
							<td class='text-center'><?=$no?></td>
							<td><?=$row['NamaBiaya']?></td>
							<td class="text-right"><?=thausand_spar($row['Nominal']?:0)?:'-'?></td>
						</tr>
						<?php }
						
						}?>
						<tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">Sub Total</td>
							<td class="text-right"><?=thausand_spar($total_nominal?:0)?></td>
						</tr>
						<tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">Pajak (%)</td>
							<td class="text-right"><?= $order['PersenPajak']?:0 ?></td>
						</tr>
						<tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">Grand Total</td>
							<td class="text-right"><?=thausand_spar($total_nominal?$total_nominal+($total_nominal*($order['PersenPajak']/100)):0)?></td>
						</tr>
						
				    </tbody>
				</table>
			</div><!-- /.col -->
        </div><!-- /.col -->
		<!--div class="row">			
			<fieldset class="ship" style="width: 100%;border:1px solid #fff;margin: 0% 0.6%;">
				<table class="detail" style="width:100%;border:1px solid #fff">
					<tr class="text_kiri">
						<td style="font-size:8pt;border:1px solid #fff">Note : <p><?=nl2br($order['Notes'])?></p></td>
					</tr>
					
				</table>
			</fieldset>
		</div-->
		<?php if($tp != 1) { ?>
		<div class="row">
			<fieldset class="ship" style="width: 3cm;border:none;margin: 0% 1.5% 0% 5%;">
				<table class="detail" style="width:100%">
					<tr>
						<td style="font-size:9pt;text-align:center;">PREPARED BY</td>
					</tr>
					<tr class="text_tengah">
						<td style="font-size:8pt;">
							<br>
							<br>
							<br>
							<br>
						</td>
					</tr>
					<tr>
						<td style="font-size:9pt">&nbsp;</td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="ship" style="width: 3.5cm;border:none;margin: 0% 1.5%;">
				<table class="detail" style="width:100%">
					<tr class="text_tengah">
						<td style="font-size:9pt;text-align:center;">APPROVED BY</td>
					</tr>
					<tr class="text_tengah">
						<td style="font-size:8pt;">
							<br>
							<br>
							<br>
							<br>
						</td>
					</tr>
					<tr>
						<td style="font-size:9pt">&nbsp;</td>
					</tr>
				</table>
			</fieldset>
		</div>
		<?php } ?>
		<div class="" style="border:none;float:right;margin: 100px 0px;">
			<?= date('d/m/Y').' '.date('H:i:s')?> / <?=$this->session->userdata('Operator')['LoginName']?>
		</div>
        
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
	
	
    <!-- AdminLTE App -->
  </body>
</html>
<script src="<?= base_url();?>assets/LTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$(document).ready(function() {
	<?php
		if($tp == 'preview'){
	?>
	$(document).keydown(function(event){
		if(event.keyCode==123){
		return false;
	   }
		else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
		   return false;  //Prevent from ctrl+shift+i
	   }else if(event.ctrlKey && event.keyCode==80){        
		   return false;  //Prevent from ctrl+shift+p
	   }
	});
	$(document).on("contextmenu",function(e){        
	   e.preventDefault();
	});
		<?php }?>
});
	
</script>
<link href="<?php echo base_url()?>assets/css/cetak.css" rel="stylesheet" type="text/css" />
