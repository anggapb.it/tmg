<style type="text/css">
    body {
        color: #404E67;
        background: #F5F7FA;
		font-family: 'Open Sans', sans-serif;
	}
	.table-wrapper {
		width: 700px;
		margin: 30px auto;
        background: #fff;
        padding: 20px;	
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {
        padding-bottom: 10px;
        margin: 0 0 10px;
    }
    .table-title h2 {
        margin: 6px 0 0;
        font-size: 22px;
    }
    .table-title .add-new {
        float: right;
		height: 30px;
		font-weight: bold;
		font-size: 12px;
		text-shadow: none;
		min-width: 100px;
		bpo-radius: 50px;
		line-height: 13px;
    }
	.table-title .add-new i {
		margin-right: 4px;
	}
    #table_detail.table {
        table-layout: fixed;
    }
    #table_detail.table tr th, table.table tr td {
        bpo-color: #e9e9e9;
    }
    #table_detail.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }
    #table_detail.table th:last-child {
        width: 100px;
    }
    #table_detail.table td a {
		cursor: pointer;
        display: inline-block;
        margin: 0 5px;
		min-width: 24px;
    }    
	#table_detail.table td a.add {
        color: #27C46B;
    }
    #table_detail.table td a.edit {
        color: #FFC107;
    }
    #table_detail.table td a.delete {
        color: #E34724;
    }
    #table_detail.table td i {
        font-size: 19px;
    }
	#table_detail.table td a.add i {
        font-size: 24px;
    	margin-right: -1px;
        position: relative;
        top: 3px;
    }    
    #table_detail.table .form-control {
        height: 32px;
        line-height: 32px;
        box-shadow: none;
        bpo-radius: 2px;
    }
	#table_detail.table .form-control.error {
		bpo-color: #f50000;
	}
	#table_detail.table td .add {
		display: none;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('.select2').select2();
		
	objDate('TglPembelian');
	<?php
	
	/* if($po['Total']) {
		echo "$('#showTotal').show();";
		echo "$('#showPLT').hide();";
	}else {
		echo "$('#showTotal').hide();";
		echo "$('#showPLT').show();";
	} */
	
	if($po['NoPembelian']){
		echo "$('#lbl_trans_no2').html('<strong>$po[NoPembelian]</strong>');";
	}else{
		echo "$('#lbl_trans_no2').html('<strong>-- AUTO GENERATE --</strong>');";
	}
	?>
	
	$('[data-toggle="tooltip"]').tooltip();
	// var actions = $("#table_detail td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
		var no = $('#table_detail tr').length;
		// $(this).attr("disabled", "disabled");
		// var index = $("#table_detail tbody tr:last-child").index();
        var row = '<tr class="product">' +
            '<td><div class="input-group input-group-sm col-md-12">\
				<input type="hidden" id="KodeBarang' + no + '" name="KodeBarang[]">\
					<input type="text" class="form-control" id="NamaBarang' + no + '" name="NamaBarang[]" <?= $po['fidStatusPO'] > 10 ? "disabled" : "readonly" ?> placeholder="Nama Barang">\
					<?php if($po['fidStatusPO'] <= 10) { ?>
					<span class="input-group-btn">\
						<button class="btn btn-info btn-flat" type="button" data-toggle="modal" data-target="#modal-cari-barang" onClick=$("#no_temp").val(' + no + ');loadDataBarang(1);><i class="fa fa-search"></i></button>\
					</span>\
					<?php } ?></div></td>' +
            '<td><input type="text" class="form-control qty" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="Qty[]" id="Qty" value="0"></td>' +
            '<td><input type="text" class="form-control harga_beli" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="HargaBeli[]" id="HargaBeli" value="0">' +
            '<input type="hidden" class="form-control total_beli" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="TotalBeli[]" id="TotalBeli" value="0"></td>' +
			'<td><input type="text" class="form-control diskon" onkeypress="numberOnlyDec(Event, this.value)" onblur="totalHarga()" name="Diskon[]" id="Diskon" value="0">\
				<input type="hidden" class="form-control harga_diskon" onkeypress="numberOnlyDec(Event, this.value)" onblur="totalHarga()" name="HargaDiskon[]" id="HargaDiskon" value="0">\
			</td>' +
			'<td><input type="text" class="form-control ppn" onkeypress="numberOnlyDec(Event, this.value)" onblur="totalHarga()" name="PPN[]" id="PPN" value="0">\
				<input type="hidden" class="form-control harga_ppn" onkeypress="numberOnlyDec(Event, this.value)" onblur="totalHarga()" name="HargaPPN[]" id="HargaPPN" value="0">\
			</td>' +
            '<td><input type="text" class="form-control sub_total" onkeypress="numberOnly(Event)" onblur="totalHarga()" name="SubTotal[]" id="SubTotal" value="0"></td>' +
			'<td><a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a></td>' +
        '</tr>';
    	$("#table_detail").append(row);		
		/* $("#table_detail tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip(); */
    });
	
	// Delete row on delete button click
	$('#table_detail').on("click", ".delete", function(){
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
		totalHarga();
    });
});

	function save()
	{
		var form = $('#input_form');
		var formData = new FormData(form[0]);
		
		showProgres();
		$('#load').button('loading');
			
		$.ajax({
			type: "POST",
			url: site_url+'po/manage/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#load').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#NoPembelian').val(result.NoPembelian);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#load').button('reset');
								show_list(result.NoPembelian);
							}, 2000);
						} else {
							$("#input_form").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#load').button('reset');
							}, 2000);
						}
					}
		});
		
	}
		
	function totalHarga() {		
		var qty 		= 0;
		var harga_beli 	= 0;
		var total_beli 	= 0;
		var diskon 		= 0;
		var harga_diskon = 0;
		var ppn 		= 0;
		var harga_ppn 	= 0;
		var sub_total 	= 0;
		var total_biaya	= 0;
		var total_diskon = 0;
		var total_ppn 	 = 0;
		var total_sub_total = 0;

		$('.qty').each(function(index) {
			qty  		= Number($('.product:eq('+index+') .qty').val());
			harga_beli  = $('.product:eq('+index+') .harga_beli').val();
			diskon 		= Number($('.product:eq('+index+') .diskon').val());
			ppn 		= Number($('.product:eq('+index+') .ppn').val());
			// var myStr = 'this,is,a,test';
			harga_beli = Number(harga_beli.replace(/[ ,.]/g, ""));

			total_beli = Number(qty*harga_beli);
			harga_diskon = Number(total_beli*(diskon/100));
			harga_ppn = Number(total_beli*(ppn/100));
			sub_total = Number(total_beli-harga_diskon+harga_ppn);

			$('.product:eq('+index+') .total_beli').val(total_beli);
			$('.product:eq('+index+') .harga_diskon').val(harga_diskon);
			$('.product:eq('+index+') .harga_ppn').val(harga_ppn);
			$('.product:eq('+index+') .sub_total').val(sub_total);

			total_biaya 	+= total_beli;
			total_diskon 	+= harga_diskon;
			total_ppn 		+= harga_ppn;
			total_sub_total += sub_total;
		});
		
		/* $('.total_beli').each(function() {
			total_biaya += Number($(this).val());
		});

		$('.diskon').each(function(index) {
			total_diskon += Number($(this).val());
		});
		
		$('.ppn').each(function() {
			total_ppn += Number($(this).val());
		}); */
		
		$('#TotalBiaya').val(total_biaya);
		$('#TotalDiskon').val(total_diskon);
		$('#TotalPPN').val(total_ppn);
		$('#TotalBiayaBayar').val(total_sub_total);
	}
	
	function lookupSelectBarang(code, no)
	{
		lookupDataBarang(code,'master/barang/get_barang', no);
	}

	function lookupDataBarang(key,url, no)
	{
		// $('#NamaSJ').html('<i class="fa fa-refresh fa-spin"></i>');
		$.post(site_url+url
				,{code:key}
				,function(result){
					$('#KodeBarang'+no).val(result.KodeBarang);					
					$('#NamaBarang'+no).val(result.NamaBarang);					
				}					
				,"json");
	}
	
	function loadDataBarang(pg)
	{
		// showProgres();
		$.post(site_url+'master/barang/lookup_page/'+pg
				,{lookup_key:$('#lookup_key').val(),
					no_temp : $('#no_temp').val()}
				,function(result) {
					$('#resultContentBarang').html(result);
					// hideProgres();
				}					
				,"html"
			);
	}
	
	function inputBarang()
	{
		// showProgres();
		$.post(site_url+'po/manage/input_barang'
				,{lookup_key:$('#lookup_key').val(),
					no_temp : $('#no_temp').val()}
				,function(result) {
					$('#resultContentaddBarang').html(result);
					// hideProgres();
				}					
				,"html"
			);
	}

	function save_status(val) {
		<?php if($po['fidStatusPO']) { ?>
			showProgres();
			$.post(site_url+'po/manage/save_status'
					,{idPO : '<?php echo encode($po['idPO']) ?>',
						fidStatusPO : val}
					,function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							
							setTimeout(function () {
								show_list();
							}, 2000);
						} else if(result.error) {
							toastr.error(result.error,'Error');
						}
					}					
					,"json"
				);
		<?php } ?>
	}
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_list()"> Pembelian</a></li>
		<?php if ($po['idPO']) {?>
		<li><a href="#" onclick="po_input('<?= encode($po['idPO'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $po['TypeCode'] = '';
// $po['TypeName'] = '';
?>
<section class="content" >
	<form id="input_form"  method="post" enctype="multipart/form-data">
		<div class="box box-default">
			<input name="no_temp" id="no_temp" hidden>
			<input name="idPO" hidden value="<?= encode($po['idPO'])?>">
			<input name="NoPembelian" hidden value="<?= encode($po['NoPembelian'])?>">
			<div class="box-body">
				<div class="row" >
					<div class="col-md-4 pull-right">
						<div class="info-box bg-aqua">
							<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
							<div class="info-box-content text-center">
								<span class="info-box-number">No Pembelian</span>
								<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
								<span class="progress-description" style="font-size: 12pt;margin: 12px;" id="lbl_trans_no2">
								
								</span>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>No Faktur</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="NoFaktur" name="NoFaktur" placeholder="" value="<?= $po['NoFaktur']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Supplier</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidSupplier" name="fidSupplier">
									<option value=''></option>
									<?php if($supplier->num_rows() > 0) {
											foreach($supplier->result_array() as $row) { ?>
												<option value='<?php echo $row['idSupplier'] ?>' <?php echo $row['idSupplier'] == $po['fidSupplier'] ? 'selected' : '' ?>><?php echo $row['NamaSupplier'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>											
					</div>
					<div class="col-md-3">						 						
						<div class="form-group">
							<label>Tgl Pembelian</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="TglPembelian" name="TglPembelian" placeholder="" value="<?= humanize_mdate($po['TglPembelian'], '%d-%m-%Y') ?>">
							</div>
						</div>
					</div>
					<div class="col-md-3">
					<?php if($this->menu->msOperatorSpecial(array('SpecialVar' => 'set_status_po'))) { ?>
						<div class="form-group">
							<label>Status Pembelian</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidStatusPO" name="fidStatusPO" onchange="save_status(this.value)">
									<?php if($status->num_rows() > 0) {
											foreach($status->result_array() as $row) { ?>
												<option value='<?php echo $row['idStatusPO'] ?>' <?php echo $row['idStatusPO'] == $po['fidStatusPO'] ? 'selected' : '' ?>><?php echo $row['NamaStatusPO'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>	
						<?php } ?>				
					</div>		
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama Kendaraan</label>
							<div class="input-group col-md-12"> 
								<select class="form-control select2" id="fidKendaraan" name="fidKendaraan">
									<option value=''></option>
									<?php if($kendaraan->num_rows() > 0) {
											foreach($kendaraan->result_array() as $row) { ?>
												<option value='<?php echo $row['idKendaraan'] ?>' <?php echo $row['idKendaraan'] == $po['fidKendaraan'] ? 'selected' : '' ?>><?php echo $row['NamaKendaraan'].' - '.$row['NamaJenisKendaraan'].' - '.$row['PlatNomor'] ?></option>
									<?php	}
										} ?>
								</select>
							</div>
						</div>						
					</div>	
				</div>						
			</div><!-- /.box-body -->
		</div>
		<div class="box box-default">
			<div class='box-body'>
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8"><h2>Pembelian <b>Detail</b></h2></div>
						<div class="col-sm-4">
							<?php if($po['fidStatusPO'] <= 10) { ?>
							<button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
							<?php } ?>
						</div>
					</div>
				</div>
				<table class="table table-bpoed" id="table_detail">
					<thead>
						<tr>
							<th>Nama Barang</th>
							<th>Qty</th>
							<th>Harga Beli</th>
							<th>Diskon (%)</th>
							<th>PPN (%)</th>
							<th>Sub Total</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
				<?php if($detail_po->num_rows() > 0) {
						$no = 0;
						$total_beli = 0;
						$harga_diskon = 0;
						$harga_ppn = 0;
						$sub_total = 0;

						foreach($detail_po->result_array() as $row) { 
						$no++;
						
						if($row['Qty'] && $row['HargaBeli'])
						$total_beli = $row['Qty']*$row['HargaBeli'];

						if($row['Diskon'])
						$harga_diskon = $total_beli*($row['Diskon']/100);

						if($row['PPN'])
						$harga_ppn = $total_beli*($row['PPN']/100);

						$sub_total = $total_beli-$harga_diskon+$harga_ppn;

						?>
						<tr class="product">
							<td>
								<div class="input-group input-group-sm col-md-12">
									<input type="hidden" id="KodeBarang<?php echo $no ?>" name="KodeBarang[]" value="<?= $row['KodeBarang'] ?>">
									<input type="text" class="form-control" id="NamaBarang<?php echo $no ?>" name="NamaBarang[]" <?= $po['fidStatusPO'] > 10 ? "disabled" : "readonly" ?> placeholder="Nama Barang" value="<?= $row['NamaBarang'] ?>">
									<?php if($po['fidStatusPO'] == 10) { ?>
									<span class="input-group-btn">
										<button class="btn btn-sm btn-info btn-flat" type="button" data-toggle="modal" data-target="#modal-cari-barang" onClick="$('#no_temp').val(<?php echo $no ?>);loadDataBarang(1);"><i class="fa fa-search"></i></button>
									</span>
									<?php } ?>
								</div>
								<!--input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control" id="fidBarang<?php echo $no ?>" name="fidBarang[]" value="<?php echo $row['fidBarang'] ?>"-->
							</td>
							<td><input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control qty" onblur="totalHarga()" id="Qty<?php echo $no ?>" name="Qty[]" value="<?php echo $row['Qty'] ?>"></td>
							<td><input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control harga_beli" onblur="totalHarga()" id="HargaBeli<?php echo $no ?>" name="HargaBeli[]" value="<?php echo thausand_spar($row['HargaBeli']) ?>">
								<input type='hidden' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control total_beli" id="TotalBeli<?php echo $no ?>" name="TotalBeli[]" value="<?php echo thausand_spar($total_beli) ?>">
							</td>
							<td><input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control diskon" onblur="totalHarga()" id="Diskon<?php echo $no ?>" name="Diskon[]" value="<?php echo $row['Diskon'] ?>">
								<input type='hidden' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control harga_diskon" id="HargaDiskon<?php echo $no ?>" name="HargaDiskon[]" value="<?php echo thausand_spar($harga_diskon) ?>">
							</td>
							<td><input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control ppn" onblur="totalHarga()" id="PPN<?php echo $no ?>" name="PPN[]" value="<?php echo $row['PPN'] ?>">
								<input type='hidden' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control harga_ppn" id="HargaPPN<?php echo $no ?>" name="HargaPPN[]" value="<?php echo thausand_spar($harga_ppn) ?>">
							</td>
							<td><input type='text' <?php if($po['fidStatusPO'] > 10) { echo 'disabled'; } ?> class="form-control sub_total" onblur="totalHarga()" id="SubTotal<?php echo $no ?>" name="SubTotal[]" value="<?php echo thausand_spar($row['SubTotal']) ?>"></td>
							<td>
								<!--a class="add" title="Add" data-toggle="tooltip"><i class="fa fa-plus-circle"></i></a>
								<a class="edit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a-->
							<?php if($po['fidStatusPO'] <= 10) { ?>
								<a class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>
							<?php } ?>
							</td>
						</tr> 
					<?php } 
						
					} ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box box-default">
			<div class='box-body'>
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8"><h2>Total <b>Biaya</b></h2></div>
					</div><br>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Total Biaya</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="TotalBiaya" name="TotalBiaya" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($po['TotalBiaya']?:0)?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total Diskon</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="TotalDiskon" name="TotalDiskon" placeholder="" value="<?= $po['TotalDiskon']?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total PPN</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="TotalPPN" name="TotalPPN" placeholder="" value="<?= $po['TotalPPN']?>">
								</div>
							</div>	
						</div>	
						<div class="col-md-3">
							<div class="form-group">
								<label>Total Biaya Bayar</label>
								<div class="input-group col-md-12"> 
									<input type="text" class="form-control" readonly id="TotalBiayaBayar" name="TotalBiayaBayar" onkeypress='numberOnlyDec(event, this.value)' placeholder="" value="<?= thausand_spar($po['TotalBiayaBayar']?:0)?>">
								</div>
							</div>	
						</div>	
					</div>	
				</div>	
			</div>	
		</div>	
		<div class="box box-default">
			<div class="box-footer">
				<?php if($po['fidStatusPO'] <= 10) { ?>
				<a href="javascript:void(0);" class="btn btn-info pull-right" onclick='save()' id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</button>
				<?php } ?>
				<a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
				<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
			</div>
		</div>
	</form>
</section>
<!-- Modal -->
<div class="modal" id="modal-cari-barang"  role="dialog" aria-labelledby="modal-cari-barang" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
		  <div class="row">
		    <div class="col-md-6">
				<h4 class="modal-title">Master Barang</h4>
			</div>
			<div class="col-md-5">			
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-input-barang" onClick="inputBarang();">+ New</button>
				</div>
			</div>
			<div class="col-md-1">			
				<div class="btn-group pull-right">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		  </div>
		</div>
		<div class="modal-body">
		<input type="hidden" id="content_lookup" value="lookup"/>
		  <div class="form-group">
			<div class="form-group has-primary has-feedback">
				<input type="text" class="form-control input-lg f-s-20" id="lookup_key" placeholder="Search Key and Enter..." onKeydown="if (event.keyCode == 13) loadDataBarang(1);">
				<span class="glyphicon glyphicon-search form-control-feedback"></span>
			</div>
			<div id="resultContentBarang"></div>
			
		  </div>
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	  
	</div>
</div>
<!-- Modal -->
<div class="modal" id="modal-input-barang"  role="dialog" aria-labelledby="modal-input-barang" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Input Barang</h4>
		</div>
		<div class="modal-body">
			<div id="resultContentaddBarang"></div>
		</div>
		<div class="modal-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right"  data-dismiss="modal" onclick='saveBarang()' id="loadBarang" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>				
		</div>
	  </div>
	  
	</div>
</div>