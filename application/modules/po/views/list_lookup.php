<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No </th>
				<th>No Pembelian</th>
				<th>Tgl Pembelian</th>
				<th>No Faktur</th>
				<th>Nama Supplier </th>
				<th>Nama Kendaraan</th>
				<th>Total Biaya</th>
				<th>Status</th>
				<th>*</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($list->num_rows() <= 0 ){
					?>
					<tr><td colspan="3"><i>Data tidak ditemukan...</i></td></tr>
				<?php
				}else{
					$no = ($paging['current']-1) * $paging['limit'] ;
					foreach($list->result_array() as $row){
					$no++;
					
				?>
					<tr data-dismiss="modal" onClick="lookupSelectBarang('<?=$row['NamaBarang']?>')" style="cursor:pointer;" title="Klik disini <?=$row['NamaBarang']?>">
						<td><?= $no ?></td>
						<td><?= match_key($row['NoPembelian'],$key['key'])?></td>
						<td><?= humanize_mdate($row['TglPembelian'], '%d-%m-%Y')?></td>
						<td><?= match_key($row['NoFaktur'],$key['key'])?></td>
						<td><?= match_key($row['NamaSupplier'],$key['key'])?></td>
						<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
						<td><?= thausand_spar($row['TotalBiayaBayar']) ?></td>
						<td><?= $row['NamaStatusPO'] ?></td>
					</tr>
				<?php }
					
				}?>
		</tbody>
	</table>
</div>
<?=$paging['list']?>