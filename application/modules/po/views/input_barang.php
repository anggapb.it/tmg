<script type="text/javascript">
	$.fn.ready(function() {
		<?php
			echo "$('#lbl_trans_barang').html('<strong>-- AUTO GENERATE --</strong>');";
			?>
	})
	
	function saveBarang()
	{
		var form = $('#input_form_barang');
		var formData = new FormData(form[0]);
		
		showProgres();
		$('#loadBarang').button('loading');
		
		if($('#foto')[0].files[0] !== undefined) {
			if(!($('#foto')[0].files[0].size < 2097152)) { 
				// 10 MB (this size is in bytes)
				//Prevent default and display error
				toastr.error("File harus kurang dari 2MB");
				return false;
			}
		}
	
		$.ajax({
			type: "POST",
			url: site_url+'master/barang/save',
			dataType: 'json', //not sure but works for me without this
			data: formData,
			contentType: false, //this is requireded please see answers above
			processData: false, //this is requireded please see answers above
			//cache: false, //not sure but works for me without this
			error   : function(result) {							
						hideProgres();
						$("#input_form_barang").find('*').removeClass("has-error");
						$('.error-obj-blocked').remove();
						// sign object that blocked
						var blockObj = result.blocked_object;
						var objName = '';
						var objMsg = '';
						
						if(!Array.isArray(blockObj)) {
							
						} else {
							if (blockObj.length > 0)
							{
								for (obj in blockObj)
								{
									objName = blockObj[obj].obj_name;
									$("input[name="+objName+"]").parent().parent().addClass('has-error');
									$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
								}
							}
						}
						toastr.error(result.error,'Error');
						setTimeout(function () {
							$('#loadBarang').button('reset');
						}, 2000);
					},
			success : function(result) {
						hideProgres();
						if (result.message)
						{
							toastr.success(result.message,'Save');
							// $('#KodeBarang').val(result.KodeBarang);
							
							/* if(result.status == 'insert') {
								clear_text();
							} */
							
							setTimeout(function () {
								$('#loadBarang').button('reset');
								loadDataBarang(1);
							}, 2000);
						} else {
							$("#input_form_barang").find('*').removeClass("has-error");
							$('.error-obj-blocked').remove();
							// sign object that blocked
							var blockObj = result.blocked_object;
							var objName = '';
							var objMsg = '';
							
							if(!Array.isArray(blockObj)) {
							
							} else {
								if (blockObj.length > 0)
								{
									for (obj in blockObj)
									{
										objName = blockObj[obj].obj_name;
										$("input[name="+objName+"]").parent().parent().addClass('has-error');
										$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
									}
								}
							}
							toastr.error(result.error,'Error');
							setTimeout(function () {
								$('#loadBarang').button('reset');
							}, 2000);
						}
					}
		});
		
	}
	
	function readUrl(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			var img = new Image();
		
			reader.onload = function (e) {
					img.src = e.target.result;
					img.onload = function() {
							/* if(this.width > 192 || this.height > 192) {
								alert('Foto Ukuran maksimum 192x192');
								$('#foto').val('');
							}	
							else { */
								$('#preview').prop('src', e.target.result);
							/* }	 */
						}	
				};
			
			reader.readAsDataURL(input.files[0]);
			
		}
	}
</script>
<form id="input_form_barang"  method="post" enctype="multipart/form-data">
	<input name="KodeBarang" hidden>
	<div class="box-body">
		<div class="row" >
			<div class="col-md-6 pull-right">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
					<div class="info-box-content text-center">
						<span class="info-box-number">Kode Barang</span>
						<div class="progress">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
						<span class="progress-description" style="font-size: 12pt;margin: 12px;" id="lbl_trans_barang">
						
						</span>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Nama Barang</label>
					<div class="input-group col-md-12"> 
						<input type="text" class="form-control" id="NamaBarang" name="NamaBarang" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label>Kelompok Barang</label>
					<div class="input-group col-md-12"> 
						<select class="form-control select2" id="fidKelompokBarang" name="fidKelompokBarang">
							<option value=''></option>
							<?php if($kelompok_barang->num_rows() > 0) {
									foreach($kelompok_barang->result_array() as $row) { ?>
										<option value='<?php echo $row['idKelompokBarang'] ?>'><?php echo $row['NamaKelompokBarang'] ?></option>
							<?php	}
								} ?>
						</select>
					</div>
				</div>		
				<div class="form-group">
					<label>Merk</label>
					<div class="input-group col-md-12">
						<input type="text" class="form-control" id="Merk" name="Merk" placeholder="">
					</div>
				</div>	
				<div class="form-group">
					<label>Satuan Kecil</label>
					<div class="input-group col-md-12"> 
						<select class="form-control select2" id="fidSatuanKecil" name="fidSatuanKecil">
							<option value=''></option>
							<?php if($satuan->num_rows() > 0) {
									foreach($satuan->result_array() as $row) { ?>
										<option value='<?php echo $row['idSatuan'] ?>'><?php echo $row['NamaSatuan'] ?></option>
							<?php	}
								} ?>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label>Satuan Besar</label>
					<div class="input-group col-md-12"> 
						<select class="form-control select2" id="fidSatuanBesar" name="fidSatuanBesar">
							<option value=''></option>
							<?php if($satuan->num_rows() > 0) {
									foreach($satuan->result_array() as $row) { ?>
										<option value='<?php echo $row['idSatuan'] ?>'><?php echo $row['NamaSatuan'] ?></option>
							<?php	}
								} ?>
						</select>
					</div>
				</div>	
			</div>
			<div class='col-md-4'>
				<div class="form-group">
					<label>Harga Beli</label>
					<div class="input-group col-md-12"> 
						<input type="text" class="form-control" id="HargaBeli" name="HargaBeli" onkeypress='numberOnly(event)' placeholder="">
					</div>
				</div>	
				<div class="form-group">
					<label>Supplier</label>
					<div class="input-group col-md-12"> 
						<select class="form-control select2" id="fidSupplier" name="fidSupplier">
							<option value=''></option>
							<?php if($supplier->num_rows() > 0) {
									foreach($supplier->result_array() as $row) { ?>
										<option value='<?php echo $row['idSupplier'] ?>'><?php echo $row['NamaSupplier'] ?></option>
							<?php	}
								} ?>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label>Deskripsi</label>
					<div class="input-group col-md-12"> 
						<textarea class="form-control" id="Deskripsi" name="Deskripsi" cols='10' rows='4'></textarea>
					</div>
				</div>	
				<div class='form-group'>
					<label>Foto</label>
					<div class="input-group col-md-12"> 
						<div class='thumbnail'>
							<img id='preview' width=90 height=90 src='<?php echo get_pict('', '90x90', 'barang') ?>' class='img-thumbnail img-responsive' alt='Photo'>
						</div>
						<input type='file' id='foto' name='foto' onchange='readUrl(this)'>
					</div>
				</div>
			</div>						
		</div>						
	</div><!-- /.box-body -->
	<!--div class="box-footer">
		<!--a href="javascript:void(0);" class="btn btn-default" onclick="input()">Reset</a>
		<a href="javascript:void(0);" class="btn btn-warning" onclick="show_list();">Close</a>
	</div-->
</form>