<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th><input type='checkbox' class='bigCheckbox' id='checked_all' onchange='check_all()'></th>
			<th>No Pembelian</th>
			<th>Tgl Pembelian</th>
			<th>No Faktur</th>
			<th>Nama Supplier </th>
			<th>Nama Kendaraan</th>
			<th>Total Biaya</th>
			<th>Status</th>
			<th>*</th>
			<!--th>Print</th-->
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?>
			<tr style="color : <?php echo $row['Colour'] ?>">
				<td><?= $no ?></td>
				<td>
					<input type='hidden' id='idPO<?php echo $no ?>' value='<?php echo encode($row['idPO']) ?>'>
					<input type='hidden' id='fidStatusPO<?php echo $no ?>' value='<?php echo $row['fidStatusPO'] ?>'>
					<input type='checkbox' class='bigCheckbox' id='check_proses<?php echo $no ?>' value='<?php echo $no ?>'>
				</td>
				<td><?= match_key($row['NoPembelian'],$key['key'])?></td>
				<td><?= humanize_mdate($row['TglPembelian'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NoFaktur'],$key['key'])?></td>
				<td><?= match_key($row['NamaSupplier'],$key['key'])?></td>
				<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
				<td><?= thausand_spar($row['TotalBiayaBayar']) ?></td>
				<td><?= $row['NamaStatusPO'] ?></td>
				<td>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#" onClick="input('<?= encode($row['idPO'])?>')">Edit</a></li>
							<?php if($row['fidStatusPO'] == 10) { ?>
							<li><a href="#" onClick="delete_data('<?= encode($row['idPO']) ?>')">Delete</a></li>
							<?php } ?>
						</ul>
					</div>
				</td>
				<!--td>
					<button type="button" class="btn btn-warning" title="Preview" onClick="print('<?=$row['fidStatusPO']>'10'?'0':'preview'?>','<?=encode($row['idPO'])?>')"><span class="fa <?=$row['fidStatusPO']>'10'?'fa-print':'fa-search'?>" aria-hidden="true"></span></button>
				</td-->
			</tr>
	<?php }?>
		</tbody>
</table>
<?=$paging['list']?>