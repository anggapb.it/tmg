<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('po_model');
		$this->load->model('po_detail_model');
		$this->load->model('master/kendaraan_model');
		$this->load->model('master/supplier_model');
		$this->load->model('master/status_po_model');
		$this->load->model('master/kelompok_barang_model');
		$this->load->model('master/satuan_model');
		
	}

	public function index()
	{
		$this->manage();
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['poby'] =  $this->input->post('t_po_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(sup."NamaSupplier") like \'%'.$filter['key'].'%\' 
				)'] = null;
		}

		$this->po_model->set_where($where);
		//
		// po by
		/* $poBy = array();
		if($filter['shortby']){
			$poBy[$filter['shortby']] = $filter['poby'][0];
		} */
		$this->po_model->set_order(array('NoPembelian' => 'ASC'));
		//
		$this->po_model->set_limit($limit);
		$this->po_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->po_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadPembelian';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->po_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id)?:0;
		$po =  $this->po_model->get($id);		
		
		//
		$data = array();
		
		$data['status'] 	= $this->status_po_model->get_list();
		$data['kendaraan'] 	= $this->kendaraan_model->get_list();
		$data['supplier'] 	= $this->supplier_model->get_list();
			
		$data['content'] 	= 'input';
		$data['po'] 		= $po;
		
		$this->po_detail_model->set_where(array('fidPO' => $id));
		$data['detail_po'] 	= $this->po_detail_model->get_list();
		$data['title'] 		= 'Input Pembelian';
		$this->load->view($data['content'],$data);
	}
	
	function save_status() {
		$idPO		 	= (decode($this->input->post('idPO'))?:0);
		$fidStatusPO	= ($this->input->post('fidStatusPO'));
		
		$po_det = $this->po_detail_model->get(array('fidPO' => $idPO));
		
		if(!$po_det['idPODetail'])
			$this->error('Silahkan input detail pembelian terlebih dahulu');
		
		$this->db->trans_start();
		$data = array();
		$data['idPO'] 		 = $idPO;
		$data['fidStatusPO'] = $fidStatusPO;
		$data['TglApproval'] 	= date('Y-m-d');
		$data['UserApproval']   = $this->session->userdata('Operator')['LoginName'];

		$this->po_model->save($data);

		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Status telah berhasil diubah ');
		}
	}
	
	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
	
		$data = array();
		$this->db->trans_start();
		$idPO		 		 	 = (decode($this->input->post('idPO'))?:0);
		$NoPembelian		 	 = (decode($this->input->post('NoPembelian'))?:0);
		$data['fidKendaraan']	 = $this->input->post('fidKendaraan')?:0;
		$data['fidSupplier']	 = $this->input->post('fidSupplier')?:0;
		$data['NoFaktur']		 = $this->input->post('NoFaktur');
		$data['TotalBiaya']	     = text2num($this->input->post('TotalBiaya'))?:0;
		$data['TglPembelian']    = getSQLDate($this->input->post('TglPembelian'));
		$data['TotalDiskon']     = $this->input->post('TotalDiskon')?:0;
		$data['TotalPPN']   	 = $this->input->post('TotalPPN')?:0;
		$data['TotalBiayaBayar'] = text2num($this->input->post('TotalBiayaBayar'))?:0;
		$data['fidStatusPO'] 	 = $this->input->post('fidStatusPO')?:10;
						
		//validasi data kosong
		$this->validation_input('fidKendaraan');
		$this->validation_input('fidSupplier');
		
		if ($idPO)
		{	
			$data['idPO'] 			 = $idPO;
			$data['NoPembelian'] = $NoPembelian;
			$data['TglUpdate'] 	= date('Y-m-d');
			$data['UserUpdate'] = $this->session->userdata('Operator')['LoginName'];
			
		}else
		{
			$data['idPO'] 			 = 0;
			$data['TglInput'] 	 = date('Y-m-d');
			$data['UserInput'] 	 = $this->session->userdata('Operator')['LoginName'];
			$data['fidStatusPO'] = 10;
			
			$NoPembelian = $this->po_model->getNewTrans($data['TglInput']);			
			$data['NoPembelian'] = $NoPembelian;	
		}
		
		if(!$data['fidKendaraan']) {
			$this->update['CallBack'] = 'fidKendaraan';
			$this->error('Kendaraan Harus diisi');
		}
		
		/* if(!$NoPembelian) {
			$po =  $this->po_model->get(array('NamaPembelian' => $data['NamaPembelian']));
			if($data['NamaPembelian']==$po['NamaPembelian']){
				$this->error('Nama Pembelian sudah ada');
			}
		} */
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = $this->po_model->save($data);

		if(!$idPO)
			$idPO = $this->db->insert_id('ho."trPO_idPO_seq"');

		// Detail
		$KodeBarang = $this->input->post('KodeBarang')?:array();
		$Qty 		= $this->input->post('Qty')?:0;
		$HargaBeli  = text2num($this->input->post('HargaBeli'))?:0;
		$Diskon  	= text2num($this->input->post('Diskon'))?:0;
		$PPN  		= text2num($this->input->post('PPN'))?:0;
		$SubTotal   = text2num($this->input->post('SubTotal'))?:0;
		
		if(count($KodeBarang) > 0) {
			$this->po_detail_model->delete(array('fidPO' => $idPO));
			
			for($i=0;$i<count($KodeBarang);$i++) {
				$data_detail = array();
				$data_detail['idPODetail'] 	= 0;
				$data_detail['fidPO'] 	   	= $idPO;
				$data_detail['KodeBarang'] 	= $KodeBarang[$i];
				$data_detail['Qty'] 		= $Qty[$i];
				$data_detail['HargaBeli'] 	= $HargaBeli[$i];
				$data_detail['Diskon'] 		= $Diskon[$i];
				$data_detail['PPN'] 		= $PPN[$i];
				$data_detail['SubTotal'] 	= $SubTotal[$i];
				
				$this->po_detail_model->save($data_detail);
			}
		}
				
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idPO'] = encode($idPO);
			$this->update['status'] = $idPO ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		// $po = $this->po_model->get($Code);
		$this->po_model->delete($Code);
		$this->po_detail_model->delete(array('fidPO' => $Code));
		/* 
		if($po['PhotoBarang']) {
			$photo = str_replace('-90x90', '', $po['PhotoBarang']);
			$check_ext = explode(".", $photo);
			$check_file_ext = end($check_ext);
			
			$structure  = 'files/po/original/'.$photo;
			$structure2  = 'files/po/thumbnails_29x29/'.$check_ext[0].'-29x29.'.$check_file_ext;
			$structure3  = 'files/po/thumbnails_45x45/'.$check_ext[0].'-45x45.'.$check_file_ext;
			$structure4  = 'files/po/thumbnails_90x90/'.$check_ext[0].'-90x90.'.$check_file_ext;
			$structure5  = 'files/po/thumbnails_128x128/'.$check_ext[0].'-128x128.'.$check_file_ext;
			
			if(file_exists($structure))
				unlink($structure);
		
			if(file_exists($structure2))
				unlink($structure2);
			
			if(file_exists($structure3))
				unlink($structure3);
			
			if(file_exists($structure4))
				unlink($structure4);
			
			if(file_exists($structure5))
				unlink($structure5);
		} */
		
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
	
	function print_pdf($tp='',$id='')
	{
		$id = decode($id);
		$po =  $this->po_model->get($NoPembelian);		
				
		$data = array();
		
		$data['kendaraan'] 	= $this->kendaraan_model->get($po['fidKendaraan']);
		$data['supplier'] 	= $this->supplier_model->get($po['fidSupplier']);
		// $data['supplier']	= $this->supplier_model->get($po['fidPengemudi']);
		
		$this->po_detail_model->set_where(array('fidPO' => $idPO));
		$data['detail_po'] 	= $this->po_detail_model->get_list();
		$data['total_nominal'] 	= $this->po_detail_model->total_nominal();
		
		//
		$data['content'] 			= 'pdf';
		$data['po'] 				= $po;
		$data['tp'] 				= $tp;
		$data['title'] 				= 'Invoice';
		
		$this->load->view($data['content'],$data);
	}
	
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->po_model->set_limit($limit);
		$this->po_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(sup."NamaSupplier") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->po_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->po_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataPembelian';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->po_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_po() {
		$NoPembelian = trim($this->input->post('NoPembelian'));
		$po = $this->po_model->get(array('NoPembelian' => $NoPembelian));
		$po['NoPembelian'] = encode($po['NoPembelian']);
		echo json_encode($po);		
	}
	
	function input_barang()
	{
		// $id = decode($id);
		// $barang =  $this->barang_model->get($id);		
		
		//
		$data = array();
		$data['kelompok_barang'] = $this->kelompok_barang_model->get_list();
		$data['satuan'] 	= $this->satuan_model->get_list();
		$data['supplier']	= $this->supplier_model->get_list();
		$data['content'] 	= 'input_barang';
		// $data['barang'] 	= $barang;
		$data['title'] 		= 'Input Barang';
		$this->load->view($data['content'],$data);
	}
}