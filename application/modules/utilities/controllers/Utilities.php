<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('utilities_model');
	}

	public function index()
	{
		$this->manage();
	}	
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		
		$this->load->view($data['content'],$data);
	}
	
	public function get_list()
	{	
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$row_id = $this->input->post('t_last_row_id');
		$refresh = $this->input->post('t_refresh');
		$kolom = $this->input->post('t_kolom');
		$sort = $this->input->post('t_sort')?:'ASC';
		
		$limit = 3;
		if ($row_id==0) // reset tampilan awal
		{
			$limit = 10;
			// $row_id = 0;
		}
		// set condition
		$where = array();
		if ($filter['key'])
		{
			$where['(
					upper(tbl."keyValue") like \'%'.$filter['key'].'%\'
				or upper(tbl."keyText") like \'%'.$filter['key'].'%\'
				or upper(tbl."Caption") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		if($this->session->userdata('Operator')['idMsOperator'] != 1) {
			$where['tbl.keyValue >= 1000'] = null;
		}
		
		$this->utilities_model->set_where($where);
		//
		$order_by = array('tbl.'.$kolom.''=>$sort);
		$this->utilities_model->set_order($order_by);
		//
		$this->utilities_model->set_limit($limit);
		$this->utilities_model->set_offset($row_id);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['type']['list'] = $this->utilities_model->get_list();		
		$data['type']['row_count'] = $this->utilities_model->get_count();		
		$data['type']['key'] = $filter['key'];		
		$data['page']['row_id'] = $row_id;	
		$data['page']['refresh'] = $refresh;		
		$data['type']['kolom'] = $kolom;		
		$data['type']['sort'] = strtolower($sort);		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id);
		$util =  $this->utilities_model->get($id);		
		// $segment =  $this->segment_model->get_list();		
		// $category =  $this->category_model->get_list();		
		//
		$data = array();
		$data['content'] = 'input';
		$data['util'] = $util;
		$data['title'] = 'Input Config Application';
		// $data['category'] = $category;
		$this->load->view($data['content'],$data);
	}
	
	function save()
	{
	
		$data = array();
		$this->db->trans_start();
		$id_config		 	= (decode($this->input->post('id_config'))?:0);
		// $typecode_current = $this->input->post('t_type_code_current');
		$data['keyValue'] 	= $this->input->post('key_value');
		$data['keyText']	= $this->input->post('key_text');
		$data['Caption']	= $this->input->post('caption');
		
		if ($id_config)
		{	
			$data['idConfig'] 	= $id_config;
		}else
		{
			$this->db->select('tbl."idConfig"');
			$this->db->order_by('idConfig','desc');
			$res = $this->db->get('dataMaster.msConfig tbl',1)->row();
			
			$data['idConfig'] = (!isset($res->idConfig) ? 1 : $res->idConfig+1);	
		}

		//validasi data kosong
		$data['keyValue'] 		= $this->validation_input('key_value');
		$data['keyText'] 	= $this->validation_input('key_text');
		$data['Caption'] 		= $this->validation_input('caption');
		/* if(!$data['KodeType']){
			$this->error('Kode Type tidak boleh kosong');
		}
		if(!$data['SegmentCode']){
			$this->error('Segment tidak boleh kosong');
		} */
		/* if($typecode_current){
			//cek data sudah ada tetapi kode nya dirubah
			if($typecode_current <> $data['KodeType']){
				$this->error('Kode Type tidak bisa dirubah');
			}
		}else {*/
			//cek data exist, input baru blokir
		if(!$id_config) {
			$util =  $this->utilities_model->get(array('keyText' => $data['keyText']));
			if($data['keyText']==$util['keyText']){
				$this->error('Key Text sudah ada');
			}

			$util =  $this->utilities_model->get(array('keyValue' => $data['keyValue']));
			if($data['keyValue']==$util['keyValue']){
				$this->error('Key Value sudah ada');
			}
		}
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = true;
		$save = $this->utilities_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idConfig'] = encode($data['idConfig']);
			$this->update['status'] = $id_config ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}

	function delete(){
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		$this->utilities_model->delete($Code);
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
}