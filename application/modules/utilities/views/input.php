<script type="text/javascript">
$(document).ready(function() {
	// objDate('tglawalefektif');
	// objDate('tglakhirefektif');
});
	function type_save()
	{
		showProgres();
		$('#load').button('loading');
		$.post(site_url+'utilities/save'
			,$('#type_input_form').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					$('#id_config').val(result.idConfig);
					
					if(result.status == 'insert') {
						clear_text();
					}
					
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
					refresh();
				}else{
					// clean error sign
					$("#type_input_form").find('*').removeClass("has-error");
					$('.error-obj-blocked').remove();
					// sign object that blocked
					var blockObj = result.blocked_object;
					var objName = '';
					var objMsg = '';
					if (blockObj.length > 0)
					{
						for (obj in blockObj)
						{
							objName = blockObj[obj].obj_name;
							$("input[name="+objName+"]").parent().parent().addClass('has-error');
							$("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+blockObj[obj].obj_msg+'</p>');
						}
					}
					toastr.error(result.error,'Error');
					setTimeout(function () {
						$('#load').button('reset');
					}, 2000);
				}
			}					
			,"json"
		);
	}
	
	function clear_text() {	
		$('#id_config').val("'<?php echo encode(0) ?>'");
		$('#key_value').val('');
		$('#key_text').val('');
		$('#caption').val('');
	}
	// function sign_error_object(data_validation,form_name)
	// {
		// $("#"+form_name).find('*').removeClass("has-error");
		// $('.error-obj-blocked').remove();
		// var objCount = Object.keys(data_validation).length;
		// var objName = '';
		// var objMsg = '';
		// if (objCount > 0)
		// {			
			// for (i = 0; i < objCount; i++)
			// {
				// objName = Object.keys(data_validation)[i];
				// objMsg = Object.values(data_validation)[i];
				
				// $("input[name="+objName+"]").parent().parent().addClass('has-error');
				// $("input[name="+objName+"]").parent().parent().append('<p class="pull-right text-red error-obj-blocked">'+objMsg+'</p>');
				
			// }
			
		// }
		
	// }
	
</script>
<section class="content-header">
	<h1>
		<?= $title ?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#" onclick="show('main_container')"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#" onclick="show_type_list()"> Utilities</a></li>
		<?php if ($util['idConfig']) {?>
		<li><a href="#" onclick="type_input('<?= encode($util['idConfig'])?>')">Edit</a></li>
		<?php }else{?>
		<li class="active">Input</li>
		<?php }?>
	</ol>
</section>
<?php
// $util['TypeCode'] = '';
// $util['TypeName'] = '';
?>
<section class="content" >
	<div class="box box-default">
		<form id="type_input_form">
			<input name="id_config" hidden value="<?= encode($util['idConfig'])?>">
			<!--input type="hidden" class="form-control" id="type_code_current" name="t_type_code_current" placeholder="name" value="<?= $util['KodeType']?>"-->
			<div class="box-body">
				<div class="row">
					<div class="col-md-offset-2 col-md-4">
						<div class="form-group">
							<label>Key Value</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" id="key_value" name="key_value" <?= ($util['keyValue'] ? "readonly" : '') ?> placeholder="" value="<?= $util['keyValue']?>">
							</div>
						</div>
						<div class="form-group">
							<label>Key Text</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" <?php if($this->session->userdata('Operator')['idMsOperator'] != 1) { echo 'readonly'; } ?> id="key_text" name="key_text" placeholder="" value="<?= $util['keyText']?>">
							</div>
						</div>		
						<div class="form-group">
							<label>Caption</label>
							<div class="input-group col-md-12"> 
								<input type="text" class="form-control" placeholder="" name="caption" id="caption" value="<?= $util['Caption']?>"/>
							</div>
						</div>								
					</div>								
				</div>						
			</div><!-- /.box-body -->
		</form>
		<div class="box-footer">
			<a href="javascript:void(0);" class="btn btn-info pull-right" onclick="type_save();" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Save</a>
			<a href="javascript:void(0);" class="btn btn-default" onclick="clear_text()">Reset</a>
			<a href="javascript:void(0);" class="btn btn-warning" onclick="show_type_list();">Close</a>
		</div>
	</div>
</section>