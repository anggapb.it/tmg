<script type="text/javascript">
	$(function () {
		var loading = false;
		
		// load first
		get_list();
		$('#loadMore').click(function($e) {
			$e.preventDefault();
			get_list();
		});
		$('.select2').select2();
	});		
	
	function scrollStop()
	{
		$(window).unbind('scroll');
	}	
	function refresh()
	{
		$('#type_list').html('');
		$('#last_row_id').val(0);
		// loadNextPage = true;
		get_list();
		// scrollStart();
	}
	
	function get_list()
	{	
		// loading = true;
		showProgres();
		copy_filter_data();
		scrollStop();
		$.post(site_url+'utilities/get_list'
			,$('#filter_content_type').serialize()
			,function(result) {
				// $('.load-more').remove();
				$('#type_list').append(result);
				hideProgres();
				// loading = false;
			}					
			,"html"
		);
		// $('#refresh').val(0);
	}
	function copy_filter_data()
	{
		$('#search_key').val($('#search_key_show_type').val());
		$('#kolom').val($('#kolom_title').val());
	}
	
	function type_input(id)
	{
		$('#type_list_container').hide();
		$('#type_input_container').show();
		$('#type_detail_container').hide();
		//
		showProgres();
		$.post(site_url+'utilities/input/'+id
			,{}
			,function(result) {
				$('#type_input_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	}
	
	function show_type_list()
	{
		$('#type_list_container').show();
		$('#type_input_container').hide();
		$('#type_detail_container').hide();
		
	}
	function show_filter()
	{
		var tp = $('#type_effect').val();
		if(tp==1){
			$( "#toggle_filter_type" ).toggle( "blind","down" );
			// $( "#toggle_filter_type_browse" ).toggle( "blind","down" );
			$('#type_effect').val(2);
		}else{
			$( "#toggle_filter_type" ).toggle( "blind","down" );
			// $( "#toggle_filter_type_browse" ).toggle( "blind","down" );
			$('#type_effect').val(1);
		}
		
	}

	function delete_data(code){
		var options = {
			title: 'Warning',
			message: "Anda yakin akan menghapus Data ?"
		};
		eModal.confirm(options).then(function callback(){
		  //jika OK
			showProgres();
			$.post(site_url+'utilities/delete'
					,{t_Code : code}
					,function(result) {
						// hideProgres();
						refresh();
						if(result.error){
							toastr.error(result.error,'Error');
							hideProgres();
						}else{
							toastr.success(result.message,'Save');
							hideProgres();
						}
					}					
					,"json"
				);
		},    function callbackCancel(){
		  //JIKA CANCEL
		});
	}

	/* function type_detail(id)
	{
		$('#type_list_container').hide();
		$('#type_input_container').hide();
		$('#type_detail_container').show();
		//
		// showProgres();
		$.post(site_url+'master.item/type/detail/'+id
			,{}
			,function(result) {
				$('#type_detail_container').html(result);
				hideProgres();
			}					
			,"html"
		);
	} */
</script>
<div id="type_list_container">
	<form id="filter_content_type" hidden>
		<input name="t_refresh" id="refresh" value=1>
		<input name="t_search_key" id="search_key">
		<input name="t_last_row_id" id="last_row_id">
		<input name="t_type_effect" id="type_effect" value="1">
		<input name="t_kolom" id="kolom" value="">
		<input name="t_sort" id="asc_desc" value="ASC">
	</form>
	<section class="content-header">
		<h1>
			Config Application
		</h1>
		<ol class="breadcrumb">
			<li><a href="#" onclick="loadMainContent('dashboard')"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Utilities</li>
			<li class="active">List</li>
		</ol>
	</section>
	<section class="content" >
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
						<?php if($this->session->userdata('Operator')['idMsOperator'] == 1) { ?>
							<div class="col-sm-4">
								<div class="btn-group">
									<button type="button" class="btn btn-warning" onclick="type_input()">New</button>
								</div>
							</div>
						<?php } ?>	
							<div class="box-tools">
								<div class="input-group pull-right" style="width: 100px;">
									<button type="button" class="btn btn-warning" onClick="show_filter()">Show Filter</button>
								</div>
							</div>
						</div>
						<div id="toggle_filter_type" hidden>
							<hr>
							<div class="row">
								<div class="form-group">
									<div class="col-sm-4">
										<label>Search</label>
										<input type="text" class="form-control" id="search_key_show_type" name="t_search_key_show_type" placeholder="Search Key Show" value="" onkeydown="if (event.keyCode == 13) refresh()">
									</div>
									<div class="col-sm-3">
										<label>Kolom</label><br>
										<select class="form-input select2" id="kolom_title" name="t_kolom_title" style="width:100%;">
											<option value="keyValue">Key Value</option>
											<option value="keyText">Key Text</option>
											<option value="Caption">Captioni</option>
										</select>
									</div>
									<div class="col-sm-6">
										<label>Order By</label><br>
										<div class="form-group">
											<div class="col-sm-2">
												<div class="radio">
													<label>
													  <input type="radio" name="t_sort_kolom_title[]" onClick="$('#asc_desc').val(this.value)" id="optionsRadios1" value="ASC" checked="">
													  A -> Z
													</label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="radio">
													<label>
													  <input type="radio" name="t_sort_kolom_title[]" onClick="$('#asc_desc').val(this.value)" id="optionsRadios2" value="DESC">
													  Z -> A
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
									
							</div>
							<div class="row">
								<div class="form-group">
									<div class="input-group pull-right" style="width: 100px;">
										<button type="button" class="btn btn-primary" onClick="refresh()">Browse</button>
										
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>No </th>
										<th>Key Value</th>
										<th>Key Text</th>
										<th>Caption</th>
										<th class="text-center">*</th>
										
									</tr>
								</thead>
								<tbody id="type_list">
								</tbody>
								<tfooter>
									<tr>
										<td colspan="17" style="text-align:center">
											<a href="javascript:void(0)" id="loadMore">load more</a>
										</td>
									</tr>
								</tfooter>
							</table>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section>
</div>
<div id="type_input_container" hidden>
	Loading...
</div>
<div id="type_detail_container" hidden>
	Loading...
</div>