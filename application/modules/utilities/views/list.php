<?php 
$key = $type['key'];
$i = $page['row_id'];
foreach($type['list']->result_array() as $row)
{
	$i++;
	?>
<tr>
	<td><?= $i?></td>
	<td><?= match_key($row['keyValue'],$key)?></td>
	<td><?= match_key($row['keyText'],$key)?></td>
	<td><?= match_key($row['Caption'],$key)?></td>
	<td>
		<div class="input-group-btn">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>
			<ul class="dropdown-menu">
				<li><a href="#" onClick="type_input('<?= encode($row['idConfig'])?>')">Edit</a></li>				
				<?php if($this->session->userdata('Operator')['idMsOperator'] == 1) { ?>
				<li><a href="#" onClick="delete_data('<?= encode($row['idConfig']) ?>')">Delete</a></li>
				<?php } ?>
			</ul>
		</div>
	</td>
</tr>
<?php }?>

<script type="text/javascript">
	$(function () {
		$('#last_row_id').val(<?= $i ?>);
		<?php 
		if ($type['list']->num_rows == 0)
		{?>
		scrollStop();
		<?php 
		}else{?>
		// scrollStart();
		<?php 
		}?>
	});
	
</script>