<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('dataMaster');
		$this->set_table('msConfig');
		$this->set_pk('idConfig');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');

		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	} 
}

/* End of file ms_item_model.php */
/* Location: ./application/modules/item/models/ms_item_unit_model.php */
