<?php

class File_upload_model extends Base_Model {

    function __construct() {

        parent::__construct();
		$this->set_schema('public');
		$this->set_table('fileUpload');
		$this->set_pk('idFileUpload');
		$this->set_log(false);
    }
	
	function get_list($id=0)
	{       
		$this->db->select('tbl.*');
		$this->db->order_by('tbl.idFileUpload', 'ASC');
		if($this->where)
			$this->db->where($this->where);
		
		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->table.' tbl');
		else
			$query = $this->db->get($this->table.' tbl',$this->limit,$this->offset);

			if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
        
    }
	
	
}