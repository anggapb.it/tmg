<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('operator/operator_model');
	}

	public function index()
	{	
		$data = array();
		$idOpr = $this->session->userdata('Operator')['idMsOperator'];
		$opr = $this->operator_model->get($idOpr);
		$data['content'] = 'profile/manage';
		$data['opr'] = $opr;
		
		$this->load->view($data['content'],$data);
	}
	function save()
	{	
		$idMsOperator	= decode($this->input->post('t_id_ms_operator'));
		$opr = $this->operator_model->get($idMsOperator);
		$pass 		= $this->input->post('t_pass');
		if(!$pass){
			$this->error('Current Password tidak boleh kosong');
		}
		//cek password current
		$nChar0 = strlen($pass);
		$cPassGen0 = $pass;
		$x0 = 1; 
		while($x0 <= $nChar0+1) 
		{
			$cPassGen0 = md5($cPassGen0);
			$x0++;
		} 
		if($cPassGen0 <> $opr['LoginPass']){
			$this->error('Current Password salah');
		}
		//
		
		$pass1 		= $this->input->post('t_pass1');
		$pass2 		= $this->input->post('t_pass2');
		$data['idMsOperator'] = $idMsOperator;
		// jika password tidak sama dengan server, 
		// berarti user merubah pasword, 
		// password harus diUpdate keserver 
		if ($pass1<>$pass2)
		{
			$this->error('reType Password tidak sama');
		}else
		{
			// jika password tidak sama dengan server, 
			// berarti user merubah pasword, 
			// password harus diUpdate keserver 
			if ($pass1 <> $opr['LoginPass'])
			{
				$nChar = strlen($pass1);
				$cPassGen = $pass1;
				$x = 1; 
				while($x <= $nChar+1) 
				{
					$cPassGen = md5($cPassGen);
					$x++;
				} 
				$data['LoginPass'] 	= $cPassGen;
			}
			
		}
		// print_r($data);
		// exit;
		// proses
		$this->db->trans_start();
		$save = $this->operator_model->save($data);
		$this->db->trans_complete();
		if($this->db->trans_status())
		{
			$this->success('Password berhasil dirubah...');
		}else
		{
			$this->error('Proses gagal...');
		}
	}
	
}


/* End of file manage.php */
/* Location: ./application/modules/ditribusi/controllers/target/Manage.php */