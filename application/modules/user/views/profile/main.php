<script type="text/javascript">
	$(function () {
		profileManage();
		hideProgres();
	});		
	
	function profileManage()
	{
		showProgres();
		$('#profileManageContainer').hide();
		$.post(site_url+"user/profile/manage"
			,{}
			,function(result){
				$('#profileManageContainer').show();
				$('#profileManageContainerSection').html(result);
				hideProgres();
				},"html"
			);
				
	}
	function show_list()
	{
		$('#form-main').show();
		$('#form-input').hide();
		
	}
</script>
<!-- Main content -->
<div id="profileManageContainer" hidden>
	<div id="profileManageContainerSection" >
	</div>
</div>

<!-- /.content -->
		
		