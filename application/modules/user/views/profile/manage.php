<script type="text/javascript">
	$(function () {

    });
	
	
	function username_save()
	{
		showProgres();
		$.post(site_url+'user/profile/manage/save'
			,$('#formContent').serialize()
			,function(result) {
				hideProgres();
				if (result.message)
				{
					toastr.success(result.message,'Save');
					window.location = site_url;
				}else
				{
					// show error message
					toastr.error(result.error,'Error');
					loadMainContent('user/profile/main');
				}
			}					
			,"json"
		);
	}
</script>
	<section class="content-header">
	  <h1>
		User Profile
	  </h1>
	  <ol class="breadcrumb">
		<li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Input</li>
	  </ol>
	</section>
	<section class="content">
          <div class="row">
			<form id="formContent">
				<input type="hidden" class="form-control" id="id_ms_operator" name = "t_id_ms_operator" placeholder="" value="<?=encode($opr['idMsOperator'])?>">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">

				  <!-- Profile Image -->
				  <div class="box box-primary">
					<div class="box-body box-profile">
					  <img class="profile-user-img img-responsive img-circle" src="<?=get_profile_pict($opr['idMsOperator'],'90x90')?>" alt="User profile picture">
					  <h3 class="profile-username text-center"><?=$opr['FullName']?></h3>
					  
					  <ul class="list-group list-group-unbordered">
						<li class="list-group-item">
						  <b>Username</b> <a class="pull-right"><?=$opr['LoginName']?></a>
						</li>
						<li class="list-group-item">
						  <b>Current Password</b> <input title="Input Password" type="password" class="form-control" id="pass" name = "t_pass" placeholder="password" value="">
						</li>
						<li class="list-group-item">
						  <b>Password</b> <input title="Input Password" type="password" class="form-control" id="pass1" name = "t_pass1" placeholder="password" value="">
						</li>
						<li class="list-group-item">
						  <b>ReType-Password</b> <input title="Input Password" type="password" class="form-control" id="pass2" name = "t_pass2" placeholder="password" value="">
						</li>
					  </ul>
						<a href="javascript:void(0);" class="btn btn-primary btn-block" onclick="username_save();"><b>Change</b></a>
					</div><!-- /.box-body -->
				  </div><!-- /.box -->
					<div class="col-md-4">
					</div>
              
				</div><!-- /.col -->
            
			</form><!-- /.row -->
          </div><!-- /.row -->

        </section>