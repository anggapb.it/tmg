<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		// $this->load->model('po/po_model');
		$this->load->model('order/order_model');
		$this->load->model('order/order_detail_model');
		$this->load->model('master/kendaraan_model');
		$this->load->model('master/pengemudi_model');
		$this->load->model('master/customer_model');
		$this->load->model('master/status_order_model');
		$this->load->model('master/account_model');
		
	}
	
	public function index() {
		$this->manage();
	}

	public function manage()
	{
		$data = array();

		$this->customer_model->set_order(array('NamaLengkap' => 'ASC'));
		$data['customer'] = $this->customer_model->get_list();
		$data['content'] = 'manage';
		// $data['par'] 	 = $par;
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		$par		   = $this->input->post('par');
		$filter['date_start'] = getSQLDate($this->input->post('t_date_start'));
		$filter['date_end']   = getSQLDate($this->input->post('t_date_end'));
		$periode 			  = $this->input->post('t_periode');
		$t_customer 		  = $this->input->post('t_customer');
		$limit 				  = $this->input->post('t_limit_rows')?:10;
		
		// set condition
		$where = array();
		
		if ($periode){
			if ($filter['date_start'] <> 'NULL')
			{
				$where['tbl.TglInvoice >='] = $filter['date_start'];
			}else{
				$where['tbl.TglInvoice >='] = date('Ymd');
			}
			if ($filter['date_end'] <> 'NULL')
			{
				$where['tbl.TglInvoice <='] = $filter['date_end'];
			}else{
				$where['tbl.TglInvoice <='] = date('Ymd');
			}
		}
				
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$filter['key'].'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$filter['key'].'%\' OR
					upper(cus."NamaLengkap") like \'%'.$filter['key'].'%\' OR
					upper(mudi."NamaLengkap") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		
		// $where['tbl.fidStatusOrder'] = 50;

		if($t_customer)
			$where['tbl.fidCustomer'] = $t_customer;
		// $where['det.fidJenisKas'] = $par;
		$this->order_model->set_where($where);
		
		$total = $this->order_model->total_nominal_head();
		//
		// order by
		/* $orderBy = array();
		if($filter['shortby']){
			$orderBy[$filter['shortby']] = $filter['orderby'][0];
		} */
		$this->order_model->set_order(array('tbl.NoOrder' => 'DESC'));
		
		/* $list = $this->order_model->sup_list(Supplier	;
		echo $thisNoFakturdb->last_query();die(); */
		$this->order_model->set_limit($limit);
		$this->order_model->set_offset($limit * ($pg - 1));
		//
		// $total += $row['TotalHargaBayar'];
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->order_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadOrder';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->order_model->get_list();		
		$data['total'] = $total;		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function pdf() {
		$order =  $this->order_model->get('0011/JOB/VIII/2019');		
				
		$data = array();
		
		$kendaraan 	= $this->kendaraan_model->get($order['fidKendaraan']);
		$customer 	= $this->customer_model->get($order['fidCustomer']);
		$pengemudi	= $this->pengemudi_model->get($order['fidPengemudi']);
		$account	= $this->account_model->get(array('Status' => 1));
		
		$this->order_detail_model->set_order(array('NoOrder' => 'ASC'));
		$this->order_detail_model->set_where(array('NoOrder' => $order['NoOrder'], 'fidJenisKas' => 1));
		$detail_order 	= $this->order_detail_model->get_list();
		$total_nominal 	= $this->order_detail_model->total_nominal();
		
	}

	function print_pdf($tp='', $param='')
	{
		$this->load->library('fpdf');	
		
		list($periode,$date_start,$date_end,$Search,$id_customer) = explode('_',decode($param));
		
		if ($periode){
			if ($date_start <> 'NULL')
			{
				$where['tbl.TglInvoice >='] = getSQLDate($date_start);
			}else{
				$where['tbl.TglInvoice >='] = date('Ymd');
			}
			if ($date_end <> 'NULL')
			{
				$where['tbl.TglInvoice <='] = getSQLDate($date_end);
			}else{
				$where['tbl.TglInvoice <='] = date('Ymd');
			}
		}
				
		if ($Search)
		{
			$where['(
					upper(tbl."NoOrder") like \'%'.$Search.'%\' OR
					upper(kenda."NamaKendaraan") like \'%'.$Search.'%\' OR
					upper(sup."NamaSupplier") like \'%'.$Search.'%\' OR
					upper(tbl."NoFaktur") like \'%'.$Search.'%\'
				)'] = null;
		}
		
		// $where['tbl.fidStatusOrder'] = 50;
		
		if($id_customer) {
			$where['tbl.fidCustomer'] = $id_customer;
		}

		$this->order_model->set_where($where);
		
		$this->order_model->set_order(array('NoInvoice' => 'DESC'));
		$last = $this->order_model->get($where);
		
		$this->order_model->set_order(array('NoOrder' => 'ASC'));
		$list = $this->order_model->get_list();

		$customer 	= $this->customer_model->get($id_customer);
		$account	= $this->account_model->get(array('Status' => 1));
	// Instanciation of inherited class
		$pdf = new fpdf('P','mm','A4');
		$pdf->AliasNbPages();
		
		$pdf->AddPage();
		// $pdf->sety(25);
		$pdf->SetAutoPageBreak(true, 1);
		
		$pdf->image($_SERVER['DOCUMENT_ROOT'].'/TMG/'.get_myconf('ConfigKeyValue7'),'24', '7', '20', '23', 'PNG');
		$pdf->sety(9);
		$pdf->setx(83);
		$pdf->SetFont('Arial','',20);
		$pdf->SetTextColor(0,0,255);
		$pdf->Cell(75, 7, get_myconf('ConfigKeyValue3'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(75, 5, get_myconf('ConfigKeyValue10'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(75, 4, get_myconf('ConfigKeyValue11'), 0, 1,'C');
		$pdf->setx(83);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(75, 4, get_myconf('ConfigKeyValue12'), 0, 1,'C');
		$gety = $pdf->gety();
		$pdf->SetLineWidth(2);
		$pdf->Line(15,$gety+3, 195, $gety+3);

		$pdf->ln(7);
		$pdf->SetFont('Arial','',16);
		$pdf->Cell(185,5,'INVOICE',0,1,'C');

		$pdf->SetLineWidth(0.2);
		$pdf->ln(1);
		$pdf->SetFont('Arial','',10);
		$pdf->setx(5);
		$pdf->Cell(40,5,'Kepada   ',0,0,'R');
		$pdf->Cell(85,5,$customer['NamaLengkap'],0,0,'L');
		$gety=$pdf->gety();
		$getx=$pdf->getx();
		$pdf->Cell(20,5,'No',0,0,'L');
		$pdf->Cell(55,5,$last['NoInvoice'],0,1,'L');
		$pdf->setx(5);
		$pdf->Cell(40,5,'',0,0,'R');
		$pdf->MultiCell(75,5,$customer['Alamat'],0,'L');
		$pdf->sety($gety+5);
		$pdf->setx($getx);
		$pdf->Cell(20,5,'Tanggal',0,0,'L');
		$pdf->Cell(55,5,$date_start.' - '.$date_end,0,1,'L');
		$pdf->ln(7);

		$pdf->SetFont('Arial','B',12);
		$pdf->setx(7);
		$pdf->Cell(25,7,'NO. MOBIL',1,0,'C');
		$pdf->Cell(40,7,'JENIS TRUCK',1,0,'C');
		$pdf->Cell(25,7,'TANGGAL',1,0,'C');
		$pdf->Cell(35,7,'TUJUAN',1,0,'C');
		$pdf->Cell(35,7,'DETAIL',1,0,'C');
		$pdf->Cell(35,7,'TAGIHAN',1,1,'C');
		
		$gety=$pdf->gety();
		$getx=$pdf->getx();
		
		$total = 0;
		if($list->num_rows() > 0) {
			$no = 0;
			$getx = 7;
			$h = 0;
			$h1 = 1;
			$h2 = 1;
			$pdf->SetFont('Arial','',10);
			foreach($list->result_array() as $row) {	
				$this->order_detail_model->set_where(array('NoOrder' => $row['NoOrder'], 'fidJenisKas' => 1));
				$order_detail = $this->order_detail_model->get_list();
				// if($no>0){var_dump($order_detail->result_array());die();}
				
					if($no == 0) {
						$pdf->sety($gety);
					} else {
						$gety += 10;
						$pdf->sety($gety);
					}
					$pdf->SetTextColor(255,255,255);
					$pdf->MultiCell(35,5,"Dari ".$row['LokasiAsal'].' KE '.$row['LokasiTujuan'],0,'L',0);
					$gety2 = $pdf->gety();
					$pdf->MultiCell(25,5,$row['PlatNomor'].substr_count("Dari ".$row['LokasiAsal'].' KE '.$row['LokasiTujuan'], ' ').PHP_EOL.($row['NamaPengemudi']?:' '),0,'L');
					$gety3 = $pdf->gety();
					// var_dump($gety3.'-'.$gety2.'-'.$gety);
					if($gety2 != $gety){
						if(($gety2 - $gety) > 10) {
							$h = 2.5;
							$h1 = 2.5;
							$h2 = 0;

						}else {
							$h2 = 0;
						}
						// elseif(($gety2 - $gety) > 5)
						// 	$h = 5;
					}
					if($gety3 != $gety){
						if(($gety3 - $gety) > 20) {
							$h = 2.5;
							$h1 = 0;

							if($h2)	
							$h2 = 2.5;
						} else {
							$h1 = 0;
						}
						// elseif(($gety2 - $gety) > 5)
						// 	$h = 5;
					}
	
					$pdf->SetTextColor(0,0,0);

				$pdf->sety($gety);

				$pdf->setx($getx);
				$pdf->MultiCell(25,5+$h1,$row['PlatNomor'].PHP_EOL.($row['NamaPengemudi']?:' '),1,'L');
				$pdf->sety($gety);
				$pdf->setx($getx+25);
				$pdf->MultiCell(40,5+$h,strlen($row['Merk'].' '.$row['Dimensi'].' CBM') > 20 ? $row['Merk'].' '.$row['Dimensi'].' CBM' : $row['Merk'].' '.$row['Dimensi'].' CBM'.PHP_EOL.' ',1,'L');
				$pdf->sety($gety);
				$pdf->setx($getx+65);
				$pdf->MultiCell(25,5+$h,humanize_mdate($row['TglOrder']).PHP_EOL.' ',1,'L');
				$pdf->sety($gety);
				$pdf->setx($getx+90);
				$pdf->MultiCell(35,5+$h2,"Dari ".$row['LokasiAsal'].' KE '.$row['LokasiTujuan'],1,'L');
				// var_dump($pdf->gety());
				if($order_detail->num_rows() > 0) {
					$det = 1;
					$y = 5;
					foreach($order_detail->result_array() as $res) {
						if($det <= 1) {
							$pdf->sety($gety);
							$pdf->setx($getx+125);
							$pdf->MultiCell(35,5+$h,$res['NamaBiaya'].PHP_EOL.' ',1,'L');
							$pdf->sety($gety);
							$pdf->setx($getx+160);
							$pdf->MultiCell(35,5+$h,'IDR '.thausand_spar($res['SubTotal']).PHP_EOL.' ',1,'R');

							if($h)
							$gety += $h*2;
						}else {
							$y += 5;
							$gety += $y;
							$pdf->sety($gety);
							$pdf->setx($getx);
							$pdf->MultiCell(25,5,' '.PHP_EOL.' ','RLB','L');
							$pdf->sety($gety);
							$pdf->setx($getx+25);
							$pdf->MultiCell(40,5,' '.PHP_EOL.' ','RLB','L');
							$pdf->sety($gety);
							$pdf->setx($getx+65);
							$pdf->MultiCell(25,5,' '.PHP_EOL.' ','RLB','L');
							$pdf->sety($gety);
							$pdf->setx($getx+90);
							$pdf->MultiCell(35,5,' '.PHP_EOL.' ','RLB','L');
							$pdf->sety($gety);
							$pdf->setx($getx+125);
							$pdf->MultiCell(35,5,$res['NamaBiaya'].PHP_EOL.' ','RLB','L');
							$pdf->sety($gety);
							$pdf->setx($getx+160);
							$pdf->MultiCell(35,5,'IDR '.thausand_spar($res['SubTotal']).PHP_EOL.' ','RLB','R');
						}
						$total += $res['SubTotal'];
						$det++;
					}
				} else {					
					$pdf->sety($gety);
					$pdf->setx($getx+125);
					$pdf->MultiCell(35,5,' '.PHP_EOL.' ',0,'L');
					$pdf->sety($gety);
					$pdf->setx($getx+160);
					$pdf->MultiCell(35,5,' '.PHP_EOL.' ',0,'L');
				}			
				/* $pdf->sety($gety);
				$pdf->setx($getx+160);
				$pdf->MultiCell(35,5,'IDR '.thausand_spar($row['TotalHargaBayar']).PHP_EOL.' ',1,'R'); */
				$no++;

			}
		}
		$pdf->SetFont('Arial','',10);
		$pdf->setx(15);
		$pdf->Cell(152, 5, 'Total', 0, 0,'R');
		$pdf->Cell(35, 5, 'IDR '.thausand_spar($total), 1, 0,'R');
		
		$pdf->ln(10);
		$pdf->setx(5);
		$pdf->Cell(40, 5, 'Hormat kami,', 0, 2,'R');
		$pdf->Cell(81.5, 5, get_myconf('ConfigKeyValue3'), 0, 2,'R');
		$gety = $pdf->gety();
		$pdf->ln(21);
		$pdf->setx(22);
		$pdf->Cell(60, 5, $account['NamaTTD'], 0, 2,'L');

		$pdf->sety($gety+5);
		$pdf->setx(90);
		$pdf->SetLineWidth(1);
		$pdf->MultiCell(85,5,"Pembayaran harap di transfer ke rekening kami :".PHP_EOL.
		$account['Bank'] .($account['InisialBank']? ' ( '.$account['InisialBank'].' )' : '').PHP_EOL.
		"Nomor Rekening : ".$account['NoRek'].PHP_EOL.
		"Atas Nama : ".$account['NamaRekening'],1,'L');

		$pdf->Output();	
		
	}
}