<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=get_myconf('ConfigKeyValue5'); ?></title>
    <link rel="shortcut icon" href="<?= base_url().get_myconf('ConfigKeyValue7'); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/thirdparty/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url();?>assets/assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url();?>assets/LTE/dist/css/AdminLTE.css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif] onload="window.print();" -->
  </head>
  <style>
				@media print{@page {size: potrait}}
				@media screen, print {
					.draft_stamp{
						background: url(<?=base_url()?>assets/images/stamp_draft.png) center no-repeat;
						background-size: contain;
					}
				}
				.right {
					text-align:right;
				}
				.f9{
					font-size: 9px;
				}
				
				.col-head{
					font-weight: 600;
					text-align:center;
					background:#717070;
					color:white;
				}
				.table{
					padding:2px;
				}
				
				@page:left {
					@bottom-right {
						margin: 10pt 0 30pt 0;
						border-top: .25pt solid #666;
						content: "Our Cats";
						font-size: 9pt;
						color: #333;
					}

					@bottom-left { 
						margin: 10pt 0 30pt 0;
						border-top: .25pt solid #666;
						content: counter(page);
						font-size: 9pt;
					}
				}
				table.detail{
					border: 1px solid black;
					border-collapse:collapse;
					font-size: 7pt;
				}

				table.detail tr th{
					border: 1px solid black;
				}
				table.detail tr td{
					border: 1px solid black;
					padding-left: 6px;
					padding-right: 6px;
					padding: 0px;

				}
			</style>
			
  <body class="<?=$order['fidStatusOrder']>'10'?'':'draft_stamp'?>">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice" style="background: none;">
        <!-- title row -->
        <div class="row">
          <div class="col-xs-12">
			<table class="table detail" style="width:500px;margin-left:100px;border:none">
				<tr>
					<td rowspan=4 style="border:none"><img src="<?= base_url().get_myconf('ConfigKeyValue7'); ?>" style="height: 80px;"></td>
					<td class="text-center" style="font-size:22px;border:none;color:#0000FF !important"><?=get_myconf('ConfigKeyValue3'); ?></td>
				</tr>
				<tr>
					<td class="text-center" style="font-size:14px;border:none"><?=get_myconf('ConfigKeyValue10'); ?></td>
				</tr>
				<tr>
					<td class="text-center" style="font-size:12px;border:none"><?=get_myconf('ConfigKeyValue11'); ?></td>
				</tr>
				<tr>
					<td class="text-center" style="font-size:11px;border:none"><?=get_myconf('ConfigKeyValue12'); ?></td>
				</tr>
			</table>
            <!--h2 class="page-header text-center" style="border-bottom:3px solid">
			  <img src="<?= base_url().get_myconf('ConfigKeyValue7'); ?>" style="height: 30px;"> 
			 &nbsp;&nbsp;&nbsp; <?=get_myconf('ConfigKeyValue3'); ?><br>
			  <?= '<small style="margin: 0px 0px 0px 27px;">'.
			  '<span>'.get_myconf('ConfigKeyValue10').'</span>'.'<br>'.
			  '<span style="font-size:12px">'.get_myconf('ConfigKeyValue11').'</span>'.'<br>'.
			  '<span style="font-size:11px">'.get_myconf('ConfigKeyValue12').'</span>'.'</small>' ?>
				<?php
					$no_po = '<i>Tidak bisa tampil dalam mode PREVIEW</i>';
					if($tp <> 'preview'){
						// if($order['Status'] >= 10){
							$no_po = $order['NoInvoice'];
						// }
					}
					
					if($tp == 1)
						$no_po = $order['NoInvoice'];
				?>
			</h2-->
          </div><!-- /.col -->
        </div>
		<hr style="margin-top:-42px;border:3px solid">
		<div class="row">
          <div class="col-xs-12">
            <h2 class="page-header text-center" style="border-bottom:none;margin-top:-12px">
				Invoice
			</h2>
		  </div>
		</div>
        <!-- info row -->
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col" style="margin-top:-25px">
			<table style="width:300px;margin-left:60px">
				<tr>
                  <td>Kepada</td>
                   <td>&nbsp;&nbsp;&nbsp;</td>
                  <td><?=$customer['NamaLengkap']?></td>
                </tr>
				<tr>
                  <td>&nbsp;</td>
                   <td>&nbsp;</td>
                  <td><?=$customer['Alamat']?></td>
                </tr>
				
				<!--tr>
                  <td>No. HP</td>
                   <td>&nbsp;:&nbsp;</td>
                  <td><?=$customer['NoHP']?></td>
                </tr-->
				
			</table>
             
          </div><!-- /.col -->
          <div class="col-sm-3 invoice-col" style="margin-top:-25px">
			<table style="width:250px;margin-left:150px">
				<tr>
                  <td>No.</td>
                   <td></td>
                  <td><?=$no_po?></td>
                </tr>
				<tr>
                  <td>Tanggal</td>
                   <td></td>
                  <td><?= humanize_mdate($order['TglInvoice']) ?></td>
                </tr>
			</table>
          </div>          
        </div><!-- /.row -->
		<br>
        <!-- Table row -->
        <div class="row invoice-info">
			<div class="col-xs-12">
				<table class="table detail" style="border-bottom:none">
					<thead>
						<tr>
							<!--th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">No</th-->
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">NO. MOBIL</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">JENIS TRUCK</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">TANGGAL</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">TUJUAN</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">DETAIL</th>
							<th class="text-center" rowspan="2" style="padding: 0px;font-size:10pt;vertical-align:middle;border: solid 1px #000;">TAGIHAN</th>
						</tr>
						
					</thead>
				    <tbody>
					<tr>
						<!--td class='text-center'><?=$no?></td-->
						<td><?=$kendaraan['PlatNomor'].'<br>'.$pengemudi['NamaLengkap'] ?></td>
						<td><?=$kendaraan['NamaKendaraan']?></td>
						<td><?= humanize_mdate($order['TglOrder'])?></td>
						<td><?= "Dari ".$order['LokasiAsal'].' KE '.$order['LokasiTujuan'] ?></td>
					<?php 
						$no = 0;
						
						$ppn_harga = $order['TotalPPN']?:0;
						$pph_harga = $order['TotalPPH']?:0;
						if($detail_order->num_rows() <= 0){
						?>
						<i>Data tidak ditemukan...</i>
						<?php
						}else{
							foreach($detail_order->result_array() as $row)
						{
							$no++;			
							
							
							if($no > 1) {								
						?>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<?php } ?>
							
							<td><?=$row['NamaBiaya']?></td>
							<td class="text-right"><?=thausand_spar($row['SubTotal']?:0)?:'-'?></td>
							<tr>
						<?php }
						
						}?>
						</tr>
						</tr>
						<!--tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">Sub Total</td>
							<td class="text-right"><?=thausand_spar($total_nominal['total_gross_all']?:0)?></td>
						</tr>
						<tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">PPN</td>
							<td class="text-right"><?= $order['TotalPPN']?thausand_spar($order['TotalPPN']):0 ?></td>
						</tr>
						<tr>
							<td class="text-right" colspan="2" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">PPH</td>
							<td class="text-right"><?= $order['TotalPPH']?thausand_spar($order['TotalPPH']*-1):0 ?></td>
						</tr-->
						<tr>
							<td class="text-right" colspan="5" style="border-bottom:none;border-bottom: solid 1px #fff;border-left: solid 1px #fff;">Total</td>
							<td class="text-right"><?=thausand_spar($total_nominal['total_gross_all']+($order['TotalPPN']?:0)-($order['TotalPPH']?:0))?></td>
						</tr>
						
				    </tbody>
				</table>
			</div><!-- /.col -->
        </div><!-- /.col -->
		<!--div class="row">			
			<fieldset class="ship" style="width: 100%;border:1px solid #fff;margin: 0% 0.6%;">
				<table class="detail" style="width:100%;border:1px solid #fff">
					<tr class="text_kiri">
						<td style="font-size:8pt;border:1px solid #fff">Note : <p><?=nl2br($order['Notes'])?></p></td>
					</tr>
					
				</table>
			</fieldset>
		</div-->
		<?php if($tp != 1) { ?>
		<div class="row">
			<fieldset class="ship" style="width: 7cm;border:none;margin: 0% 1.5% 0% 5%;">
				<table class="detail" style="width:100%;border:none">
					<tr>
						<td style="font-size:9pt;border:none">
							Hormat kami,<br>
							<?=get_myconf('ConfigKeyValue3'); ?>
						</td>
					</tr>
					<tr class="text_tengah">
						<td style="font-size:8pt;border:none">
							<br>
							<br>
							<br>
							<br>
							<br>
						</td>
					</tr>
					<tr>
						<td style="font-size:9pt;border:none;margin-left:10px"><?php echo $account['NamaTTD'] ?></td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="ship" style="width: 7cm;border:none;margin: 4% 2.5%;">
				<table class="detail" style="width:100%">
					<tr class="text_tengah">
						<td style="font-size:8pt;border:3px solid">
							Pembayaran harap di transfer ke rekening kami :<br>
							<?php echo $account['Bank'] .($account['InisialBank']? ' ( '.$account['InisialBank'].' )' : '') ?><br>
							Nomor Rekening : <?php echo $account['NoRek'] ?><br>
							Atas Nama : PT.TRISULA MULTISARANA GLOBAL<br>
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
		<?php } ?>
		<div class="" style="border:none;float:right;margin: 100px 0px;">
			<?= date('d/m/Y').' '.date('H:i:s')?> / <?=$this->session->userdata('Operator')['LoginName']?>
		</div>
        
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
	
	
    <!-- AdminLTE App -->
  </body>
</html>
<script src="<?= base_url();?>assets/LTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
$(document).ready(function() {
	<?php
		if($tp == 'preview'){
	?>
	$(document).keydown(function(event){
		if(event.keyCode==123){
		return false;
	   }
		else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
		   return false;  //Prevent from ctrl+shift+i
	   }else if(event.ctrlKey && event.keyCode==80){        
		   return false;  //Prevent from ctrl+shift+p
	   }
	});
	$(document).on("contextmenu",function(e){        
	   e.preventDefault();
	});
		<?php }?>
});
	
</script>
<link href="<?php echo base_url()?>assets/css/cetak.css" rel="stylesheet" type="text/css" />
