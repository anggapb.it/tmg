<table class="table table-bordered table-hover" id="list_table">
	<thead>
		<tr>
			<th>No </th>
			<th>No Order</th>
			<th>No Invoice</th>
			<th>Tgl Order</th>
			<th>Tgl Invoice</th>
			<th>Nama Kostumer </th>
			<th>Nama Kendaraan</th>
			<th>Pengemudi</th>
			<th>Total</th>
			<!--th>Status</th>
			<th>Print</th-->
		</tr>
	</thead>
		<tbody>
			<?php 
			$no = ($paging['current']-1) * $paging['limit'] ;
			foreach($list->result_array() as $row)
			{
				$no++;
				
			?> 
			<tr>
				<td><?= $no ?></td>
				<input type='hidden' id='NoOrder<?php echo $no ?>' value='<?php echo encode($row['NoOrder']) ?>'>
				<input type='hidden' id='fidStatusOrder<?php echo $no ?>' value='<?php echo $row['fidStatusOrder'] ?>'>
				<td><?= match_key($row['NoOrder'],$key['key'])?></td>
				<td><?= match_key($row['NoInvoice'],$key['key'])?></td>
				<td><?= humanize_mdate($row['TglOrder'], '%d-%m-%Y')?></td>
				<td><?= humanize_mdate($row['TglInvoice'], '%d-%m-%Y')?></td>
				<td><?= match_key($row['NamaCustomer'],$key['key'])?></td>
				<td><?= match_key($row['NamaKendaraan'],$key['key'])?></td>
				<td><?= $row['NamaPengemudi'] ?></td>
				<td class="text-right"><?= thausand_spar($row['TotalHargaBayar'])?></td>
				<!--td><?= $row['NamaStatusOrder'] ?></td>
				<td>
					<button type="button" class="btn btn-warning" title="Preview" onClick="print('<?=$row['fidStatusOrder']>'10'?'0':'preview'?>','<?=encode($row['NoOrder'])?>')"><span class="fa <?=$row['fidStatusOrder']>'10'?'fa-print':'fa-search'?>" aria-hidden="true"></span></button>
				</td-->
			</tr>
	<?php }?>
			<tr>
				<th colspan='8' class='text-right'>Total</th>
				<th class='text-right'><?php echo thausand_spar($total['total_net_all']) ?></th>
			</tr>
		</tbody>
</table>
<?=$paging['list']?>