<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_order_detail_model extends Base_Model {

	function __construct() {

        parent::__construct();
		$this->set_schema('ho');
		$this->set_table('trOrderDetail');
		$this->set_pk('idOrderDetail');
		$this->set_log(true);
    }	
		
    function get_list()
	{
		$this->db->select('tbl.*');
		
		$this->db->select('ord.*');
		$this->db->join('ho.trOrder ord', 'ord.NoOrder = tbl.NoOrder', 'left');

		$this->db->select('kenda.NamaKendaraan');
		$this->db->join('dataMaster.msKendaraan kenda', 'ord.fidKendaraan = kenda.idKendaraan', 'left');
				
		$this->db->select('cus.NamaLengkap as "NamaCustomer"');
		$this->db->join('dataMaster.msCustomer cus', 'ord.fidCustomer = cus.idCustomer', 'left');
		
		$this->db->select('mudi.NamaLengkap as "NamaPengemudi"');
		$this->db->join('dataMaster.msPengemudi mudi', 'ord.fidPengemudi = mudi.idPengemudi', 'left');
		
		$this->db->select('sts.NamaStatusOrder, sts.Colour');
		$this->db->join('dataMaster.msStatusOrder sts', 'ord.fidStatusOrder = sts.idStatusOrder', 'left');

		$this->db->where($this->where);
		
		foreach ($this->order_by as $key => $value)
		{
			$this->db->order_by($key, $value);
		}

		if (!$this->limit AND !$this->offset)
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		else
			$query = $this->db->get($this->schema.'.'.$this->table.' tbl',$this->limit,$this->offset);
		// echo $this->db->last_query();
		// exit;
        if($query->num_rows()>0)
		{
			return $query;
        
		}else
		{
			$query->free_result();
            return $query;
        }
	}
	function get_count()
	{
		$this->db->select('count(*) as row_count');
		$this->db->join('ho.trOrder ord', 'ord.NoOrder = tbl.NoOrder', 'left');
		
		$this->db->join('dataMaster.msKendaraan kenda', 'ord.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msCustomer cus', 'ord.fidCustomer = cus.idCustomer', 'left');
		$this->db->join('dataMaster.msPengemudi mudi', 'ord.fidPengemudi = mudi.idPengemudi', 'left');
		$this->db->join('dataMaster.msStatusOrder sts', 'ord.fidStatusOrder = sts.idStatusOrder', 'left');

		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row['row_count'];
	}
	
	
	function total_nominal()
	{
		$this->db->select_sum('Nominal', 'total_gross_all');
		$this->db->select_sum('SubTotal', 'total_net_all');
		
		$this->db->join('ho.trOrder ord', 'ord.NoOrder = tbl.NoOrder', 'left');

		$this->db->join('dataMaster.msKendaraan kenda', 'ord.fidKendaraan = kenda.idKendaraan', 'left');
		$this->db->join('dataMaster.msCustomer cus', 'ord.fidCustomer = cus.idCustomer', 'left');
		$this->db->join('dataMaster.msPengemudi mudi', 'ord.fidPengemudi = mudi.idPengemudi', 'left');
		$this->db->join('dataMaster.msStatusOrder sts', 'ord.fidStatusOrder = sts.idStatusOrder', 'left');
		
		$this->db->where($this->where);
		$query = $this->db->get($this->schema.'.'.$this->table.' tbl');
		$row = $query->row_array();
		return $row;
	}
}

/* End of file dealer_model.php */
/* Location: ./application/modules/master.mitra/models/dealer_model.php */
