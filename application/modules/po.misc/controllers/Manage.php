<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('po_misc_model');
		$this->load->model('po_misc_detail_model');
		$this->load->model('master/status_po_model');		
	}

	public function index()
	{
		$this->manage();
	}
	
	public function manage()
	{
		$data = array();
		$data['content'] = 'manage';
		
		$this->load->view($data['content'],$data);
	}
	
	function page($pg=1)
	{		
		$filter['key'] = strtoupper ($this->input->post('t_search_key'));
		// $filter['shortby'] =  $this->input->post('t_short_by');
		// $filter['poby'] =  $this->input->post('t_po_by');
		
		// $periode = $this->input->post('t_periode');
		$limit = $this->input->post('t_limit_rows')?:10;
		// set condition
		$where = array();
		
		if ($filter['key'])
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaTransaksi") like \'%'.$filter['key'].'%\'
				)'] = null;
		}

		$this->po_misc_model->set_where($where);
		//
		// po by
		/* $po_miscBy = array();
		if($filter['shortby']){
			$po_miscBy[$filter['shortby']] = $filter['poby'][0];
		} */
		$this->po_misc_model->set_order(array('NoPembelian' => 'ASC'));
		//
		$this->po_misc_model->set_limit($limit);
		$this->po_misc_model->set_offset($limit * ($pg - 1));
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->po_misc_model->get_count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'pageLoadPembelian';
		$page['list'] 		= $this->gen_paging($page);
		//
		$data = array();
		$data['content'] = 'list';		
		$data['list'] = $this->po_misc_model->get_list();		
		$data['key'] = $filter;		
		$data['paging'] = $page;		
		$this->load->view($data['content'],$data);
	}
	
	function input($id=0)
	{
		$id = decode($id)?:0;
		$po_misc =  $this->po_misc_model->get($id);		
		
		//
		$data = array();
		
		$data['status'] 	= $this->status_po_model->get_list();
			
		$data['content'] 	= 'input';
		$data['po_misc'] 		= $po_misc;
		
		$this->po_misc_detail_model->set_where(array('fidPOMisc' => $id));
		$data['detail_po'] 	= $this->po_misc_detail_model->get_list();
		$data['title'] 		= 'Input Transaksi Lain-lain';
		$this->load->view($data['content'],$data);
	}
	
	function save_status() {
		$idPOMisc		 	= (decode($this->input->post('idPOMisc'))?:0);
		$fidStatusPO	= ($this->input->post('fidStatusPO'));
		
		$po_misc_det = $this->po_misc_detail_model->get(array('fidPOMisc' => $idPOMisc));
		
		if(!$po_misc_det['idPOMiscDetail'])
			$this->error('Silahkan input detail pembelian terlebih dahulu');
		
		$this->db->trans_start();
		$data = array();
		$data['idPOMisc'] 		 = $idPOMisc;
		$data['fidStatusPO'] 	= $fidStatusPO;
		$data['TglApproval'] 	= date('Y-m-d');
		$data['UserApproval']   = $this->session->userdata('Operator')['LoginName'];

		$this->po_misc_model->save($data);

		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Status telah berhasil diubah ');
		}
	}
	
	function save()
	{
		ini_set('memory_limit', '64M');
		ini_set('max_execution_time', 300);
	
		$data = array();
		$this->db->trans_start();
		$idPOMisc		 		 = (decode($this->input->post('idPOMisc'))?:0);
		$NoPembelian		 	 = (decode($this->input->post('NoPembelian'))?:0);
		/* $data['fidKendaraan']	 = $this->input->post('fidKendaraan')?:0;
		$data['fidSupplier']	 = $this->input->post('fidSupplier')?:0;
		$data['NoFaktur']		 = $this->input->post('NoFaktur'); */
		$data['NamaTransaksi']	 = $this->input->post('NamaTransaksi');
		$data['TotalBiaya']	     = text2num($this->input->post('TotalBiaya'))?:0;
		$data['TglPembelian']    = getSQLDate($this->input->post('TglPembelian'));
		$data['TotalDiskon']     = $this->input->post('TotalDiskon')?:0;
		$data['TotalPPN']   	 = $this->input->post('TotalPPN')?:0;
		$data['TotalPPH']   	 = $this->input->post('TotalPPH')?:0;
		$data['TotalBiayaBayar'] = text2num($this->input->post('TotalBiayaBayar'))?:0;
		$data['fidStatusPO'] 	 = $this->input->post('fidStatusPO')?:10;
						
		//validasi data kosong
		/* $this->validation_input('fidKendaraan');
		$this->validation_input('fidSupplier'); */
		
		if ($idPOMisc)
		{	
			$data['idPOMisc'] 	= $idPOMisc;
			$data['NoPembelian'] = $NoPembelian;
			$data['TglUpdate'] 	= date('Y-m-d');
			$data['UserUpdate'] = $this->session->userdata('Operator')['LoginName'];
			
		}else
		{
			$data['idPOMisc'] 			 = 0;
			$data['TglInput'] 	 = date('Y-m-d');
			$data['UserInput'] 	 = $this->session->userdata('Operator')['LoginName'];
			$data['fidStatusPO'] = 10;
			
			$NoPembelian = $this->po_misc_model->getNewTrans($data['TglInput']);			
			$data['NoPembelian'] = $NoPembelian;	
		}
		
		if(!$data['NamaTransaksi']) {
			$this->error('Nama Transaksi Harus diisi');
		}
		
		/* if(!$NoPembelian) {
			$po_misc =  $this->po_misc_model->get(array('NamaPembelian' => $data['NamaPembelian']));
			if($data['NamaPembelian']==$po_misc['NamaPembelian']){
				$this->error('Nama Pembelian sudah ada');
			}
		} */
		//}
		if (count($this->blocked_object) > 0)
		{	
			$this->error('Please check your data');
		}
		
		$save = $this->po_misc_model->save($data);

		if(!$idPOMisc)
			$idPOMisc = $this->db->insert_id('ho."trPOMisc_idPOMisc_seq"');

		// Detail
		$NamaBarang = $this->input->post('NamaBarang')?:array();
		$Qty 		= $this->input->post('Qty')?:0;
		$HargaBeli  = text2num($this->input->post('HargaBeli'))?:0;
		$Diskon  	= text2num($this->input->post('Diskon'))?:0;
		$PPN  		= text2num($this->input->post('PPN'))?:0;
		$PPH  		= text2num($this->input->post('PPH'))?:0;
		$SubTotal   = text2num($this->input->post('SubTotal'))?:0;
		
		if(count($NamaBarang) > 0) {
			$this->po_misc_detail_model->delete(array('fidPOMisc' => $idPOMisc));
			
			for($i=0;$i<count($NamaBarang);$i++) {
				$data_detail = array();
				$data_detail['idPOMiscDetail'] 	= 0;
				$data_detail['fidPOMisc'] 	   	= $idPOMisc;
				$data_detail['NamaBarang'] 	= $NamaBarang[$i];
				$data_detail['Qty'] 		= $Qty[$i];
				$data_detail['HargaBeli'] 	= $HargaBeli[$i];
				$data_detail['Diskon'] 		= $Diskon[$i];
				$data_detail['PPN'] 		= $PPN[$i];
				$data_detail['PPH'] 		= $PPH[$i];
				$data_detail['SubTotal'] 	= $SubTotal[$i];
				
				$this->po_misc_detail_model->save($data_detail);
			}
		}
				
		$this->db->trans_complete();
				
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->update['idPOMisc'] = encode($idPOMisc);
			$this->update['status'] = $idPOMisc ? 'update' : 'insert';
			$this->success('Data telah disimpan ');
		}
	}
		
	function delete(){
		$id = $this->input->post('t_Code');
		$Code = decode($this->input->post('t_Code'));
		$this->db->trans_start();
		// $po_misc = $this->po_misc_model->get($Code);
		$this->po_misc_model->delete($Code);
		$this->po_misc_detail_model->delete(array('fidPOMisc' => $Code));
				
		$this->db->trans_complete();
		if($this->db->trans_status()==false)
		{
			$this->error('Proses gagal dijalankan. ');		
		}else{
			$this->success('Data telah dihapus ');
		}
	}
		
	function lookup_page($pg=1)
	{
		$lookupkey = strtoupper($this->input->post('lookup_key'));
		
		$limit = $this->input->post('row_per_page')?:10;
		// binding data
		$this->po_misc_model->set_limit($limit);
		$this->po_misc_model->set_offset($limit * ($pg - 1));
		// filtering data
		$where = array();
		if($lookupkey)
		{
			$where['(
					upper(tbl."NoPembelian") like \'%'.$filter['key'].'%\' OR
					upper(tbl."NamaTransaksi") like \'%'.$filter['key'].'%\'
				)'] = null;
		}
		$this->po_misc_model->set_where($where);
		
		//
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->po_misc_model->count() ;
		$page['current'] 	= $pg;
		$page['load_func_name'] = 'loadDataPembelian';
		$page['list'] 		= $this->gen_paging($page);
		//
		$list = $this->po_misc_model->get_list();
		//
		$data = array('list' 	=> 	$list
			// ,'name' 			=> 	$this->session->userdata('username')
			,'content' 			=> 	'list_lookup'
			,'paging'			=> 	$page
			,'key'				=>  $lookupkey
		);
		$this->load->view($data['content'],$data);
	}

	function get_po() {
		$NoPembelian = trim($this->input->post('NoPembelian'));
		$po_misc = $this->po_misc_model->get(array('NoPembelian' => $NoPembelian));
		$po_misc['NoPembelian'] = encode($po_misc['NoPembelian']);
		echo json_encode($po_misc);		
	}
}