<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Auth {

	var $CI = null;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
	}
	
	function do_login($login = NULL)
	{
		$result['login'] = false;
		$result['message'] = '';
	
		// A few safety checks
		// Our array has to be set
		if(!isset($login))
			return $result;
	
		// get user info
		$this->CI->db->select('*');
		$this->CI->db->select('CURRENT_DATE as "CurDate"',false);
		$this->CI->db->from('"public"."msOperator"');
		if (isset($login['idMsOperator']))
		{
			// untuk switch user
			$this->CI->db->where('idMsOperator',$login['idMsOperator']);
		}else{
			//Our array has to have 2 values
			//No more, no less!
			if(count($login) != 2)
				return $result;
			//
			$username = $login['username'];
			$password = $login['password'];
			$nChar = strlen($password)+1;
			$cPassGen = $password;
			$x = 1; 
			while($x <= $nChar) 
			{
				$cPassGen = md5($cPassGen);
				$x++;
			} 

			$this->CI->db->where('Upper("LoginName")', $username);
			$this->CI->db->where('LoginPass',$cPassGen);
		}
		// execute
		$query = $this->CI->db->get();
		$operator = $query->row_array();
		if ($operator['idMsOperator'])
		{
			// get dealer
			/* $this->CI->db->select('*');
			$this->CI->db->from('dataMaster.msDealer');
			$this->CI->db->where('KodeDealer',$operator['KodeDealer']);
			$query = $this->CI->db->get();
			$dealer = $query->row_array(); */
			// get employee
			$this->CI->db->select('*');
			$this->CI->db->from('humanCapital.msEmployee');
			$this->CI->db->where('idMsEmployee',$operator['fidMsEmployee']);
			$query = $this->CI->db->get();
			$employee = $query->row_array();
			// create log
			$data_log = array();
			$data_log['value_before'] ='';
			$data_log['log_type'] = $operator['LoginName'].' login';
			$data_log['ip_comp'] = $this->CI->input->ip_address();
			$data_log['fid_operator'] = $operator['idMsOperator'];
			$data_log['table_name'] = '';
			$data_log['fid_data'] = 0;
			$log = $this->CI->db->insert('terminal.tr_log', $data_log);
			// Our user exists, set session.
			$newdata = array();
			$newdata['LoggedIn'] = true;
			$newdata['Operator'] = $operator;
			// $newdata['Dealer'] = $dealer;
			$newdata['Employee'] = $employee;
			$this->CI->session->set_userdata($newdata);	  
			// done
			$result['login'] = true;
			$result['message'] = 'Login succes';
		}else{
			$result['message'] = 'username or password is incorrect';
		}
		return $result;

	}
	
	
	 /**
	 *
	 * This function restricts users from certain pages.
	 * use restrict(TRUE) if a user can't access a page when logged in
	 *
	 * @access	public
	 * @param	boolean	wether the page is viewable when logged in
	 * @return	void
	 */	
	function restrict($logged_out = FALSE)
	{
		// If the user is logged in and he's trying to access a page
		// he's not allowed to see when logged in,
		// redirect him to the index!
		if ($logged_out && is_logged_in())
		{
				redirect('');
				exit;
				//echo $this->CI->fungsi->warning('Maaf, sepertinya Anda sudah login...',site_url());
				//die();
		}
		
		// If the user isn' logged in and he's trying to access a page
		// he's not allowed to see when logged out,
		// redirect him to the login page!
		if ( ! $logged_out && !is_logged_in()) 
		{
				echo $this->CI->fungsi->warning('Anda diharuskan untuk Login bila ingin mengakses halaman ini.',site_url());
				die();
		}
	}
	
	function logout() 
	{
		$this->CI->session->sess_destroy();	
		return TRUE;
	}
	
	function cek($id,$ret=false)
	{
		$menu = array(
			'data_master'=>'+admin+',
			'manajemen_user'=>'+admin+'
		);
		$allowed = explode('+',$menu[$id]);
		if(!in_array(from_session('level'),$allowed))
		{
			if($ret) return false;
			echo $this->CI->fungsi->warning('Anda tidak diijinkan mengakses halaman ini.',site_url());
			die();
		}
		else
		{
			if($ret) return true;
		}
	}	
}
// End of library class
// Location: system/application/libraries/Auth.php
