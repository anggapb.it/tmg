<style>
@media (max-width: 767px) {
    .table-responsive{
        overflow-x: auto;
        overflow-y: auto;
    }
}
@media (min-width: 767px) {
    .table-responsive{
        overflow: inherit !important; /* Sometimes needs !important */
    }
}

.pulse-button {

  position: relative;
  box-shadow: 0 0 0 0 rgba(232, 76, 61, 0.7);
  border-radius: 1%;
  cursor: pointer;
  -webkit-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
  -moz-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
  -ms-animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
  animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
}
.pulse-button:hover 
{
  -webkit-animation: none;-moz-animation: none;-ms-animation: none;animation: none;
}

@-webkit-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-moz-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@-ms-keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
@keyframes pulse {to {box-shadow: 0 0 0 45px rgba(232, 76, 61, 0);}}
.datepicker {z-index: 1151 !important;}
</style>
	<style>
	</style>
<!--  -->
<?php
$user = $this->session->userdata('Operator');
?>
<div class="modal hide fade" scrolllock="true" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 600px">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h3 id="myModalLabel1">Change Password</h3>
    </div>
	<div class="modal-body">
		<form id="changePassForm" class="form-horizontal">
			<input name="t_idMsOperator" type="hidden" value="<?= encode($user['idMsOperator']) ?>">
			<div class="control-group">
		        <label class="control-label">Old Password</label>
		        <div class="controls">
		            <input name="t_oldPassword" type="password" placeholder="Old Password" value="" class="input-large">
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">New Password</label>
		        <div class="controls">
		            <input name="t_newPassword" type="password" placeholder="New Password" value="" class="input-large">
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Confim New Password</label>
		        <div class="controls">
		            <input name="t_confPassword" type="password" placeholder="Confim New Password" value="" class="input-large">
		        </div>
		    </div>
		</form>
    </div>
    <div class="modal-footer">
        <div class="form-action">
        	
			<a href="#" class="btn btn-info" onclick="changePassword();">Browse</a>
		</div>
    </div>
</div




 <!---- Logo -->
        <a href="<?= base_url();?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>D</b>ev</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">
						<?php echo 'PT. '.$this->menu->msConfig(array('keyValue' => '1')); ?>
					</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Notifications: style can be found in dropdown.less -->
            	<li class="dropdown notifications-menu" title="Transfer Kain">
					<?php if($privTotalTransferKain){?>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" title='Notif Penerimaan Kain Internal' onClick="loadMainContent('penerimaan.kain.internal/manage/');">
					   <i class="fa fa-cart-plus"></i>
					  <span class="label label-success" id="txtTotalTransferKain" style="font-size: 8pt;"><?=0?></span>
					</a>
					<?php }?>
				
				</li>
              
				<li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?= get_profile_pict($user['fidMsEmployee'],'29x29');?>" class="user-image" alt="">
                  <span class="hidden-xs"><?= $user['FullName'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?= get_profile_pict($user['fidMsEmployee'],'90x90');?>" class="img-circle" alt="">
                    <p>
                      <?= $user['FullName'];?>
                      <small>System Owner</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" onclick="loadMainContent('user/profile/main')" class="btn btn-default btn-flat">My Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="#"  onclick="logout()" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
				</li>
			  <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>

        <script type='text/javascript'>
	function logout()
	{
		window.location = site_url+'/login/signout';
	}
	function selectStore(cCode)
	{
		$.post(site_url+'main/select_store/'+cCode
				,{}
				,function(result) {
					location.reload();
				}					
				,"html"
			);
		
	}

	function filterDashboard()
	{
		showProgres();
		var textCode = $('#store_code').val();
		$.post(site_url+"main/select_store/"+textCode
			,$("#form_filter").serialize()
			,function(result) {
				// $('#cobaan').html(result);
				// var textCode = $('#store_code').val();
				$('#textStoreCode').text(textCode);
				monthLoadContent();
				monthToDateLoadContent();
				weeklySalesLoadContent();
				visitorsLoadContent();
				showCodeSelect();
			}					
			,"html"
		);
	}

	function showCodeSelect(){
		var textCode = $('#store_code').val();
		if(textCode == null){
			var textCode = "<?= $this->session->userdata('StoreCodeDefault') ?>";
		}
		$.post(site_url+"main/show_code/"+textCode
			,$("#form_filter").serialize()
			,function(result) {
				$('#cobaan').html(result);
			}					
			,"html"
		);
	}

	function changePassword(){
		$.post(site_url+'master/user_priv/changePassword'
			,$("#changePassForm").serialize()
			,function(result) {
				if (result.error)
				{
					pesan_error(result.error);
				}else
				{
					pesan_success(result.message);
					// loadList('master/user_priv');
					$('#modalChangePassword').modal('hide');
				}
			}					
			,"json"
		);
	}
</script>