PGDMP     (                     w            tmg    11.3    11.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16393    tmg    DATABASE     �   CREATE DATABASE tmg WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE tmg;
             postgres    false                        2615    16394    customerRelationship    SCHEMA     &   CREATE SCHEMA "customerRelationship";
 $   DROP SCHEMA "customerRelationship";
             postgres    false            	            2615    16395 
   dataMaster    SCHEMA        CREATE SCHEMA "dataMaster";
    DROP SCHEMA "dataMaster";
             postgres    false                        2615    16396    ho    SCHEMA        CREATE SCHEMA ho;
    DROP SCHEMA ho;
             postgres    false                        2615    16397    humanCapital    SCHEMA        CREATE SCHEMA "humanCapital";
    DROP SCHEMA "humanCapital";
             postgres    false                        2615    16398    terminal    SCHEMA        CREATE SCHEMA terminal;
    DROP SCHEMA terminal;
             postgres    false                       1255    16399    trig_FillNoUrutQC()    FUNCTION     g  CREATE FUNCTION "dataMaster"."trig_FillNoUrutQC"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

Declare 
    v_idkain integer;
    v_start  integer;
    v_end    integer;
	  v_counter integer;
    v_idbahan integer;

BEGIN
    v_idkain  := NEW."idKain";
		v_idbahan := NEW."fidBahan";
		v_start   := NEW."StartNoUrut";
    v_end     := NEW."EndNoUrut";
		
		if v_end is null or v_start is NULL Then
		     Select "StartNoUrut", "EndNoUrut"
           into v_start, v_end
				   from "dataMaster"."msBahan"
					where "idBahan" = v_idbahan;

					NEW."StartNoUrut" := v_start;
					NEW."EndNoUrut" 	:= v_end;
		End If;

		Delete from "dataMaster"."msNoUrutQC"
		 Where "fidKain" = v_idkain
						and "NoUrut" < 1000
					  and ("NoUrut" < v_start or "NoUrut" > v_end);

		create temporary table tmp1 ("NoUrut" integer);

		v_counter := v_start;

		Loop
		Exit When v_counter > v_end ;
			 Insert into tmp1 Values (v_counter);
			 v_counter := v_counter + 1;
		End Loop;

		Insert into "dataMaster"."msNoUrutQC"
								("fidKain", "NoUrut", "fidQCRol")
		Select v_idkain
						, tmp1."NoUrut"
						, null
		 from tmp1 left outer join (Select "NoUrut" from "dataMaster"."msNoUrutQC" where "fidKain" = v_idkain) x
								on tmp1."NoUrut" = x."NoUrut"
		where x."NoUrut" is null;

		Drop table tmp1;

	  Return NEW;

End;

$$;
 2   DROP FUNCTION "dataMaster"."trig_FillNoUrutQC"();
    
   dataMaster       postgres    false    9                       1255    16400    trig_UpdateMSKain()    FUNCTION     P  CREATE FUNCTION "dataMaster"."trig_UpdateMSKain"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    If TG_OP ='UPDATE' Then
				Update "dataMaster"."msKain"
					 set "StartNoUrut" = NEW."StartNoUrut"
							,"EndNoUrut" = NEW."EndNoUrut"
				 where "fidBahan" = NEW."idBahan";
		END IF ;
		Return NEW;

End;


$$;
 2   DROP FUNCTION "dataMaster"."trig_UpdateMSKain"();
    
   dataMaster       postgres    false    9                       1255    16401    f_LaporanSisa_Benang()    FUNCTION     �G  CREATE FUNCTION public."f_LaporanSisa_Benang"() RETURNS TABLE("NoPOBenang" character varying, "TglPOBenang" date, "NamaBenang" character varying, "Ukuran" character varying, total_order_ball_benang numeric, total_order_kg_benang numeric, "NoSJRcvdBenang" character varying, "TglSJRcvdBenang" date, "KodeMakloon" character varying, "QtyRcvd_Ball" numeric, "QtyRcvd_Kg" numeric, "KodeMakloonTransfer" text, "QtyBall_Transfer" numeric, "QtyKg_Transfer" numeric, "idBenangRcvdDetail" integer, "fidBenangRcvdDetail" smallint, "idPOMakloon" integer, "NoPOMakloon" character varying, "idMakloonJob" integer, "Deskripsi" character varying, "idPOMakloonDetail" integer, "TotalRolMakloon" numeric, "TotalKgMakloon" numeric, "idGreigeRcvd" integer, "idGreigeRcvdDetail" integer, "NoSJGreige" character varying, "TotalTerimaRolGreige" bigint, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" character varying, "idGreigeRcvdTrans" integer, "idGreigeRcvdDetailTrans" integer, "NoSJRcvdGreigeTrans" character varying, "TotalTerimaRolGreigeTrans" bigint, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOMakloon" smallint, "fidPOMakloonDetail" integer, "KodeMakloonGreige" character varying, "TotalSisaRolMakloon" numeric, "TotalSisaKgMakloon" numeric, "fidGreigeRcvd" smallint, "fidGreigeRcvdDetail" smallint, "TglPO-C" date, "PO-C" character varying, "idCelupanJob" integer, "DeskripsiJobCelupan" character varying, "WarnaCelupan" character varying, "TotalOrderCelupan" numeric, "AsalSJ-G" character varying, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
CREATE TEMPORARY TABLE "xTmp1" AS SELECT
	"po_benang"."NoPO" as "NoPOBenang",
	po_benang."Tanggal" as "TglPOBenang",
	"bn"."NamaBenang",
	rcvd_benang_det."Ukuran",
	po_benang_dtl.total_order_ball as "total_order_ball_benang",
	po_benang_dtl.total_order_kg AS "total_order_kg_benang",
	rcvd_benang."NoSJ" AS "NoSJRcvdBenang",
	rcvd_benang."TanggalSJ" AS "TglSJRcvdBenang",
	ms_mak_benang."KodeMakloon",
	rcvd_benang_det."QtyRcvd_Ball",
	rcvd_benang_det."QtyRcvd_Kg",
	ms_mak_asal_bn."KodeMakloon" || '->' || ms_mak_tuju_bn."KodeMakloon" AS "KodeMakloonTransfer",
	tr_benang_det."Qty_Ball" AS "QtyBall_Transfer",
	tr_benang_det."Qty_Kg" AS "QtyKg_Transfer",
	rcvd_benang_det."idBenangRcvdDetail"
FROM
	"ho"."trBenangOrder" "po_benang"
LEFT JOIN (
	SELECT
		MAX (
			a."fidBenangOrderDetailDelivery"
		) AS "fidBenangOrderDetailDelivery",
		a."fidBenangRcvd",
		a."fidBenangOrder"
	FROM
		"ho"."trBenangRcvdOrder" a
	GROUP BY
		a."fidBenangRcvd",
		a."fidBenangOrder"
) "rcvd_benang_order" ON rcvd_benang_order."fidBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN ho."trBenangRcvd" rcvd_benang ON rcvd_benang_order."fidBenangRcvd" = rcvd_benang."idBenangRcvd" -- LEFT JOIN "ho"."trBenangOrder" "po_benang" ON "rcvd_benang_order"."fidBenangOrder" = "po_benang"."idBenangOrder"
LEFT JOIN (
	SELECT
		"fidTrBenangOrder",
		SUM ("QtyOrder_Ball") AS total_order_ball,
		SUM ("QtyOrder_Kg") AS total_order_kg,
		SUM ("Jumlah") AS total_order_harga
	FROM
		ho."trBenangOrderDetail"
	GROUP BY
		"fidTrBenangOrder"
) po_benang_dtl ON po_benang_dtl."fidTrBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN (
	SELECT
		MAX (a."idBenangRcvdDetail") AS "idBenangRcvdDetail",
		COALESCE (SUM(a."QtyRcvd_Ball"), 0) AS "QtyRcvd_Ball",
		COALESCE (SUM(a."QtyRcvd_Kg"), 0) AS "QtyRcvd_Kg",
		a."fidBenangRcvd",
		a."fidBenang",
		a."Ukuran"
	FROM
		ho."trBenangRcvdDetail" a
	GROUP BY
		a."Ukuran",
		a."fidBenangRcvd",
		a."fidBenang"
) rcvd_benang_det ON rcvd_benang_det."fidBenangRcvd" = rcvd_benang_order."fidBenangRcvd"
LEFT JOIN ho."trBenangOrderDetailDelivery" po_benang_dtl_delivery ON po_benang_dtl_delivery."idBenangOrderDetailDelivery" = rcvd_benang_order."fidBenangOrderDetailDelivery"
LEFT JOIN ho."trBenangOrderDelivery" po_benang_delivery ON po_benang_delivery."idBenangOrderDelivery" = po_benang_dtl_delivery."fidBenangOrderDelivery"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_benang ON ms_mak_benang."idMakloon" = po_benang_delivery."fidMakloon"
LEFT JOIN "dataMaster"."msBenang" bn ON bn."idBenang" = rcvd_benang_det."fidBenang"
LEFT JOIN ho."trBenangTransfer" tr_benang ON tr_benang."fidBenangRcvd" = rcvd_benang."idBenangRcvd"
LEFT JOIN ho."trBenangTransferDetail" tr_benang_det ON tr_benang."idBenangTransfer" = tr_benang_det."fidBenangTransfer"
AND tr_benang_det."fidBenang" = rcvd_benang_det."fidBenang"
AND tr_benang_det."Ukuran" = rcvd_benang_det."Ukuran"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_asal_bn ON ms_mak_asal_bn."idMakloon" = tr_benang."fidMakloonAsal"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_tuju_bn ON ms_mak_tuju_bn."idMakloon" = tr_benang."fidMakloonTujuan";

CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_mak_benang."fidBenangRcvdDetail",
	po_mak."idPOMakloon",
	po_mak."NoPO" AS "NoPOMakloon",
	mak_job."idMakloonJob",
	mak_job."Deskripsi",
	po_mak_detail."idPOMakloonDetail",
	po_mak_detail."BanyakRol" AS "TotalRolMakloon",
	(
		po_mak_detail."BanyakRol" * po_mak_detail."BeratPerRol"
	) AS "TotalKgMakloon",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdMakloon" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(
		grg_celupan."TotalTerimaRolGreige" - COALESCE (
			po_celupan."TotalOrderRolCelupan",
			0
		) - COALESCE (
			po_celupan_trans."TotalOrderRolCelupanTrans",
			0
		)
	) AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOMakloonBenang" po_mak_benang
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_benang."fidPOMakloon"
LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."fidPOMakloon" = po_mak."idPOMakloon"
LEFT JOIN "dataMaster"."msMakloonJob" mak_job ON mak_job."idMakloonJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
GROUP BY
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail"
) grg_celupan ON grg_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg."fidCelupan",
		grg_dtl."fidPOMakloonDetail",
		SUM (po_cel."TotalOrder") AS "TotalOrderRolCelupan"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."idPOMakloonDetail" = grg_dtl."fidPOMakloonDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrder"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd"
	AND grg_dtl."idGreigeRcvdMakloon" = po_cel."fidGreigeRcvdDetail"
	GROUP BY
		grg."fidPOMakloon",
		grg."fidCelupan",
		grg_dtl."fidPOMakloonDetail"
) po_celupan ON po_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND po_celupan."fidCelupan" = grg_rcvd."fidCelupan"
AND po_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN (
	SELECT
		grg."idGreigeRcvd",
		grg_dtl."fidPOMakloonDetail",
		grg_dtl."idGreigeRcvdMakloon",
		SUM (
			po_cel_trans."TotalOrderTrans"
		) AS "TotalOrderRolCelupanTrans"
	FROM
		ho."trGreigeRcvdMakloon" grg_dtl
	LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
	LEFT JOIN ho."trGreigeRcvd" grg ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
	LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrderTrans"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrderTrans"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel_trans ON po_cel_trans."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."idGreigeRcvdTransfer" = po_cel_trans."fidGreigeRcvdDetail"
	GROUP BY
		grg."idGreigeRcvd",
		grg_dtl."fidPOMakloonDetail",
		grg_dtl."idGreigeRcvdMakloon"
) po_celupan_trans ON po_celupan_trans."idGreigeRcvd" = grg_rcvd."idGreigeRcvd"
AND po_celupan_trans."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
AND po_celupan_trans."idGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon";


CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_mak_dtl."fidPOMakloon",
	po_mak_dtl."idPOMakloonDetail" as "fidPOMakloonDetail",
	mak."KodeMakloon" AS "KodeMakloonGreige",
	(
		po_mak_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolMakloon",
	(
		(
			po_mak_dtl."BanyakRol" * po_mak_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgMakloon"
FROM
	ho."trPOMakloonDetail" po_mak_dtl
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_dtl."fidPOMakloon"
LEFT JOIN "dataMaster"."msMakloon" mak ON mak."idMakloon" = po_mak."fidMakloon"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
		FROM
			ho."trGreigeRcvdMakloonRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = grg_rcvd_rol."fidGreigeRcvdMakloon"
	GROUP BY
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail"
) grg ON grg."fidPOMakloon" = po_mak_dtl."fidPOMakloon" and grg."fidPOMakloonDetail" = po_mak_dtl."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		WHERE
		(rcvd_det."fidReturKainRcvdDetail" is null OR rcvd_det."fidReturKainRcvdDetail" = 0)
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp1".*, "xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp1"
LEFT JOIN "xTmp2" ON "xTmp1"."idBenangRcvdDetail" = "xTmp2"."fidBenangRcvdDetail"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOMakloon" = "xTmp3"."fidPOMakloon" AND "xTmp2"."idPOMakloonDetail" = "xTmp3"."fidPOMakloonDetail"
LEFT JOIN "xTmp4" ON 
	("xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail") OR ("xTmp2"."idGreigeRcvdTrans" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetailTrans" = "xTmp4"."fidGreigeRcvdDetail");

DROP TABLE "xTmp1",
 "xTmp2",
 "xTmp3",
 "xTmp4";

END; $$;
 /   DROP FUNCTION public."f_LaporanSisa_Benang"();
       public       postgres    false                       1255    16403    f_LaporanSisa_Benang_backup()    FUNCTION     Q;  CREATE FUNCTION public."f_LaporanSisa_Benang_backup"() RETURNS TABLE("NoPOBenang" character varying, "TglPOBenang" date, "NamaBenang" character varying, "Ukuran" character varying, total_order_ball_benang numeric, total_order_kg_benang numeric, "NoSJRcvdBenang" character varying, "TglSJRcvdBenang" date, "KodeMakloon" character varying, "QtyRcvd_Ball" numeric, "QtyRcvd_Kg" numeric, "KodeMakloonTransfer" text, "QtyBall_Transfer" numeric, "QtyKg_Transfer" numeric, "idBenangRcvdDetail" integer, "fidBenangRcvdDetail" smallint, "idPOMakloon" integer, "NoPOMakloon" character varying, "idMakloonJob" integer, "Deskripsi" character varying, "idPOMakloonDetail" integer, "TotalRolMakloon" numeric, "TotalKgMakloon" numeric, "idGreigeRcvd" integer, "idGreigeRcvdDetail" integer, "NoSJGreige" character varying, "TotalTerimaRolGreige" bigint, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" character varying, "idGreigeRcvdTrans" integer, "idGreigeRcvdDetailTrans" integer, "NoSJRcvdGreigeTrans" character varying, "TotalTerimaRolGreigeTrans" bigint, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOMakloon" smallint, "fidPOMakloonDetail" integer, "KodeMakloonGreige" character varying, "TotalSisaRolMakloon" numeric, "TotalSisaKgMakloon" numeric, "fidGreigeRcvd" smallint, "fidGreigeRcvdDetail" smallint, "TglPO-C" date, "PO-C" character varying, "idCelupanJob" integer, "DeskripsiJobCelupan" character varying, "WarnaCelupan" character varying, "TotalOrderCelupan" numeric, "AsalSJ-G" character varying, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
CREATE TEMPORARY TABLE "xTmp1" AS SELECT
	"po_benang"."NoPO" as "NoPOBenang",
	po_benang."Tanggal" as "TglPOBenang",
	"bn"."NamaBenang",
	rcvd_benang_det."Ukuran",
	po_benang_dtl.total_order_ball as "total_order_ball_benang",
	po_benang_dtl.total_order_kg AS "total_order_kg_benang",
	rcvd_benang."NoSJ" AS "NoSJRcvdBenang",
	rcvd_benang."TanggalSJ" AS "TglSJRcvdBenang",
	ms_mak_benang."KodeMakloon",
	rcvd_benang_det."QtyRcvd_Ball",
	rcvd_benang_det."QtyRcvd_Kg",
	ms_mak_asal_bn."KodeMakloon" || '->' || ms_mak_tuju_bn."KodeMakloon" AS "KodeMakloonTransfer",
	tr_benang_det."Qty_Ball" AS "QtyBall_Transfer",
	tr_benang_det."Qty_Kg" AS "QtyKg_Transfer",
	rcvd_benang_det."idBenangRcvdDetail"
FROM
	"ho"."trBenangOrder" "po_benang"
LEFT JOIN (
	SELECT
		MAX (
			a."fidBenangOrderDetailDelivery"
		) AS "fidBenangOrderDetailDelivery",
		a."fidBenangRcvd",
		a."fidBenangOrder"
	FROM
		"ho"."trBenangRcvdOrder" a
	GROUP BY
		a."fidBenangRcvd",
		a."fidBenangOrder"
) "rcvd_benang_order" ON rcvd_benang_order."fidBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN ho."trBenangRcvd" rcvd_benang ON rcvd_benang_order."fidBenangRcvd" = rcvd_benang."idBenangRcvd" -- LEFT JOIN "ho"."trBenangOrder" "po_benang" ON "rcvd_benang_order"."fidBenangOrder" = "po_benang"."idBenangOrder"
LEFT JOIN (
	SELECT
		"fidTrBenangOrder",
		SUM ("QtyOrder_Ball") AS total_order_ball,
		SUM ("QtyOrder_Kg") AS total_order_kg,
		SUM ("Jumlah") AS total_order_harga
	FROM
		ho."trBenangOrderDetail"
	GROUP BY
		"fidTrBenangOrder"
) po_benang_dtl ON po_benang_dtl."fidTrBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN (
	SELECT
		MAX (a."idBenangRcvdDetail") AS "idBenangRcvdDetail",
		COALESCE (SUM(a."QtyRcvd_Ball"), 0) AS "QtyRcvd_Ball",
		COALESCE (SUM(a."QtyRcvd_Kg"), 0) AS "QtyRcvd_Kg",
		a."fidBenangRcvd",
		a."fidBenang",
		a."Ukuran"
	FROM
		ho."trBenangRcvdDetail" a
	GROUP BY
		a."Ukuran",
		a."fidBenangRcvd",
		a."fidBenang"
) rcvd_benang_det ON rcvd_benang_det."fidBenangRcvd" = rcvd_benang_order."fidBenangRcvd"
LEFT JOIN ho."trBenangOrderDetailDelivery" po_benang_dtl_delivery ON po_benang_dtl_delivery."idBenangOrderDetailDelivery" = rcvd_benang_order."fidBenangOrderDetailDelivery"
LEFT JOIN ho."trBenangOrderDelivery" po_benang_delivery ON po_benang_delivery."idBenangOrderDelivery" = po_benang_dtl_delivery."fidBenangOrderDelivery"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_benang ON ms_mak_benang."idMakloon" = po_benang_delivery."fidMakloon"
LEFT JOIN "dataMaster"."msBenang" bn ON bn."idBenang" = rcvd_benang_det."fidBenang"
LEFT JOIN ho."trBenangTransfer" tr_benang ON tr_benang."fidBenangRcvd" = rcvd_benang."idBenangRcvd"
LEFT JOIN ho."trBenangTransferDetail" tr_benang_det ON tr_benang."idBenangTransfer" = tr_benang_det."fidBenangTransfer"
AND tr_benang_det."fidBenang" = rcvd_benang_det."fidBenang"
AND tr_benang_det."Ukuran" = rcvd_benang_det."Ukuran"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_asal_bn ON ms_mak_asal_bn."idMakloon" = tr_benang."fidMakloonAsal"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_tuju_bn ON ms_mak_tuju_bn."idMakloon" = tr_benang."fidMakloonTujuan";

CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_mak_benang."fidBenangRcvdDetail",
	po_mak."idPOMakloon",
	po_mak."NoPO" AS "NoPOMakloon",
	mak_job."idMakloonJob",
	mak_job."Deskripsi",
	po_mak_detail."idPOMakloonDetail",
	po_mak_detail."BanyakRol" AS "TotalRolMakloon",
	(
		po_mak_detail."BanyakRol" * po_mak_detail."BeratPerRol"
	) AS "TotalKgMakloon",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdMakloon" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-grg_celupan."TotalOrderRolCelupan") AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOMakloonBenang" po_mak_benang
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_benang."fidPOMakloon"
LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."fidPOMakloon" = po_mak."idPOMakloon"
LEFT JOIN "dataMaster"."msMakloonJob" mak_job ON mak_job."idMakloonJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan",
	sum(po_cel."TotalOrder") as "TotalOrderRolCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN (
	SELECT
		po_cel_rcvd."fidGreigeRcvd",
		po_cel_rcvd."fidGreigeRcvdDetail",
		po_cel_det."TotalOrder"
	FROM
		ho."trPOCelupanRcvdDetail" po_cel_rcvd
	LEFT JOIN (
		SELECT
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
		FROM
			ho."trPOCelupanDetail" po_det
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		GROUP BY
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = po_cel."fidGreigeRcvdDetail"
GROUP BY
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail"
) grg_celupan ON grg_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_mak_dtl."fidPOMakloon",
	po_mak_dtl."idPOMakloonDetail" as "fidPOMakloonDetail",
	mak."KodeMakloon" AS "KodeMakloonGreige",
	(
		po_mak_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolMakloon",
	(
		(
			po_mak_dtl."BanyakRol" * po_mak_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgMakloon"
FROM
	ho."trPOMakloonDetail" po_mak_dtl
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_dtl."fidPOMakloon"
LEFT JOIN "dataMaster"."msMakloon" mak ON mak."idMakloon" = po_mak."fidMakloon"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
		FROM
			ho."trGreigeRcvdMakloonRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = grg_rcvd_rol."fidGreigeRcvdMakloon"
	GROUP BY
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail"
) grg ON grg."fidPOMakloon" = po_mak_dtl."fidPOMakloon" and grg."fidPOMakloonDetail" = po_mak_dtl."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp1".*, "xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp1"
LEFT JOIN "xTmp2" ON "xTmp1"."idBenangRcvdDetail" = "xTmp2"."fidBenangRcvdDetail"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOMakloon" = "xTmp3"."fidPOMakloon" AND "xTmp2"."idPOMakloonDetail" = "xTmp3"."fidPOMakloonDetail"
LEFT JOIN "xTmp4" ON 
	"xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail";

DROP TABLE "xTmp1",
 "xTmp2",
 "xTmp3",
 "xTmp4";

END; $$;
 6   DROP FUNCTION public."f_LaporanSisa_Benang_backup"();
       public       postgres    false                       1255    16405    f_LaporanSisa_Greige()    FUNCTION     9  CREATE FUNCTION public."f_LaporanSisa_Greige"() RETURNS TABLE("idPOGreige" integer, "NoPOGreige" character varying, "TglPOGreige" date, "NamaBahan" character varying, "UkuranKain" character varying, "idGreigeJob" integer, "Deskripsi" character varying, "idPOGreigeDetail" integer, "TotalRolGreige" numeric, "TotalKgGreige" numeric, "idGreigeRcvd" integer, "idGreigeRcvdDetail" integer, "NoSJGreige" character varying, "TotalTerimaRolGreige" bigint, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" character varying, "idGreigeRcvdTrans" integer, "idGreigeRcvdDetailTrans" integer, "NoSJRcvdGreigeTrans" character varying, "TotalTerimaRolGreigeTrans" bigint, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOGreige" smallint, "fidPOGreigeDetail" integer, "KodeSupplierGreige" character varying, "NamaSupplierGreige" character varying, "TotalSisaRolGreige" numeric, "TotalSisaKgGreige" numeric, "fidGreigeRcvd" smallint, "fidGreigeRcvdDetail" smallint, "TglPO-C" date, "PO-C" character varying, "idCelupanJob" integer, "DeskripsiJobCelupan" character varying, "WarnaCelupan" character varying, "TotalOrderCelupan" numeric, "AsalSJ-G" character varying, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_grg."idPOGreige",
	po_grg."NoPO" AS "NoPOGreige",
	po_grg."Tanggal" as "TglPOGreige",
	bh."NamaBahan",
	po_grg_detail."UkuranKain",
	grg_job."idGreigeJob",
	grg_job."Deskripsi",
	po_grg_detail."idPOGreigeDetail",
	po_grg_detail."BanyakRol" AS "TotalRolGreige",
	(
		po_grg_detail."BanyakRol" * po_grg_detail."BeratPerRol"
	) AS "TotalKgGreige",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdSup" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-COALESCE(po_celupan."TotalOrderRolCelupan",0)-COALESCE(po_celupan_trans."TotalOrderRolCelupanTrans",0)) AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOGreige" po_grg
LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."fidPOGreige" = po_grg."idPOGreige"
LEFT JOIN "dataMaster"."msBahan" bh ON po_grg_detail."fidBahan" = bh."idBahan"
LEFT JOIN "dataMaster"."msGreigeJob" grg_job ON grg_job."idGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."idGreigeRcvd" = trans_g."fidGreigeRcvd"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
GROUP BY
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail"
) grg_celupan ON grg_celupan."fidPOGreige" = po_grg."idPOGreige"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg."fidCelupan",
		grg_dtl."fidPOGreigeDetail",
		SUM (po_cel."TotalOrder") AS "TotalOrderRolCelupan"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."idPOGreigeDetail" = grg_dtl."fidPOGreigeDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrder"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd"
	AND po_cel."fidGreigeRcvdDetail" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg."fidCelupan",
		grg_dtl."fidPOGreigeDetail"
) po_celupan ON po_celupan."fidPOGreige" = po_grg."idPOGreige"
AND po_celupan."fidCelupan" = grg_rcvd."fidCelupan"
AND po_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN (
	SELECT
		grg."idGreigeRcvd",
		grg_dtl."fidPOGreigeDetail",
		grg_dtl."idGreigeRcvdSup",
		SUM (
			po_cel_trans."TotalOrderTrans"
		) AS "TotalOrderRolCelupanTrans"
	FROM
		ho."trGreigeRcvdSup" grg_dtl
	LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
	LEFT JOIN ho."trGreigeRcvd" grg ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
	LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrderTrans"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrderTrans"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel_trans ON po_cel_trans."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."idGreigeRcvdTransfer" = po_cel_trans."fidGreigeRcvdDetail"
	GROUP BY
		grg."idGreigeRcvd",
		grg_dtl."fidPOGreigeDetail",
		grg_dtl."idGreigeRcvdSup"
) po_celupan_trans ON po_celupan_trans."idGreigeRcvd" = grg_rcvd."idGreigeRcvd"
AND po_celupan_trans."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
AND po_celupan_trans."idGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_grg_dtl."fidPOGreige",
	po_grg_dtl."idPOGreigeDetail" as "fidPOGreigeDetail",
	ms_sup."KodeSupplier" AS "KodeSupplierGreige",
	ms_sup."NamaSupplier" AS "NamaSupplierGreige",
	(
		po_grg_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolGreige",
	(
		(
			po_grg_dtl."BanyakRol" * po_grg_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgGreige"
FROM
	ho."trPOGreigeDetail" po_grg_dtl
LEFT JOIN ho."trPOGreige" po_grg ON po_grg."idPOGreige" = po_grg_dtl."fidPOGreige"
LEFT JOIN "dataMaster"."msSupGreige" ms_sup ON ms_sup."idSupGreige" = po_grg."fidSupGreige"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
		FROM
			ho."trGreigeRcvdSupRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail"
) grg ON grg."fidPOGreige" = po_grg_dtl."fidPOGreige" AND grg."fidPOGreigeDetail" = po_grg_dtl."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		WHERE
		(rcvd_det."fidReturKainRcvdDetail" is null OR rcvd_det."fidReturKainRcvdDetail" = 0)
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp2"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOGreige" = "xTmp3"."fidPOGreige" AND "xTmp2"."idPOGreigeDetail" = "xTmp3"."fidPOGreigeDetail"
LEFT JOIN "xTmp4" ON 
	("xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail") OR ("xTmp2"."idGreigeRcvdTrans" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetailTrans" = "xTmp4"."fidGreigeRcvdDetail");

DROP TABLE "xTmp2",
 "xTmp3",
 "xTmp4";
END; $$;
 /   DROP FUNCTION public."f_LaporanSisa_Greige"();
       public       postgres    false                       1255    16407    f_LaporanSisa_Greige_backup()    FUNCTION     -  CREATE FUNCTION public."f_LaporanSisa_Greige_backup"() RETURNS TABLE("idPOGreige" integer, "NoPOGreige" character varying, "TglPOGreige" date, "NamaBahan" character varying, "UkuranKain" character varying, "idGreigeJob" integer, "Deskripsi" character varying, "idPOGreigeDetail" integer, "TotalRolGreige" numeric, "TotalKgGreige" numeric, "idGreigeRcvd" integer, "idGreigeRcvdDetail" integer, "NoSJGreige" character varying, "TotalTerimaRolGreige" bigint, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" character varying, "idGreigeRcvdTrans" integer, "idGreigeRcvdDetailTrans" integer, "NoSJRcvdGreigeTrans" character varying, "TotalTerimaRolGreigeTrans" bigint, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOGreige" smallint, "fidPOGreigeDetail" integer, "KodeSupplierGreige" character varying, "NamaSupplierGreige" character varying, "TotalSisaRolGreige" numeric, "TotalSisaKgGreige" numeric, "fidGreigeRcvd" smallint, "fidGreigeRcvdDetail" smallint, "TglPO-C" date, "PO-C" character varying, "idCelupanJob" integer, "DeskripsiJobCelupan" character varying, "WarnaCelupan" character varying, "TotalOrderCelupan" numeric, "AsalSJ-G" character varying, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_grg."idPOGreige",
	po_grg."NoPO" AS "NoPOGreige",
	po_grg."Tanggal" as "TglPOGreige",
	bh."NamaBahan",
	po_grg_detail."UkuranKain",
	grg_job."idGreigeJob",
	grg_job."Deskripsi",
	po_grg_detail."idPOGreigeDetail",
	po_grg_detail."BanyakRol" AS "TotalRolGreige",
	(
		po_grg_detail."BanyakRol" * po_grg_detail."BeratPerRol"
	) AS "TotalKgGreige",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdSup" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-grg_celupan."TotalOrderRolCelupan") AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOGreige" po_grg
LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."fidPOGreige" = po_grg."idPOGreige"
LEFT JOIN "dataMaster"."msBahan" bh ON po_grg_detail."fidBahan" = bh."idBahan"
LEFT JOIN "dataMaster"."msGreigeJob" grg_job ON grg_job."idGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."idGreigeRcvd" = trans_g."fidGreigeRcvd"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan",
	sum(po_cel."TotalOrder") as "TotalOrderRolCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN (
	SELECT
		po_cel_rcvd."fidGreigeRcvd",
		po_cel_rcvd."fidGreigeRcvdDetail",
		po_cel_det."TotalOrder"
	FROM
		ho."trPOCelupanRcvdDetail" po_cel_rcvd
	LEFT JOIN (
		SELECT
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
		FROM
			ho."trPOCelupanDetail" po_det
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		GROUP BY
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd" AND po_cel."fidGreigeRcvdDetail" = grg_dtl."idGreigeRcvdSup"
GROUP BY
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail"
) grg_celupan ON grg_celupan."fidPOGreige" = po_grg."idPOGreige"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_grg_dtl."fidPOGreige",
	po_grg_dtl."idPOGreigeDetail" as "fidPOGreigeDetail",
	ms_sup."KodeSupplier" AS "KodeSupplierGreige",
	ms_sup."NamaSupplier" AS "NamaSupplierGreige",
	(
		po_grg_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolGreige",
	(
		(
			po_grg_dtl."BanyakRol" * po_grg_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgGreige"
FROM
	ho."trPOGreigeDetail" po_grg_dtl
LEFT JOIN ho."trPOGreige" po_grg ON po_grg."idPOGreige" = po_grg_dtl."fidPOGreige"
LEFT JOIN "dataMaster"."msSupGreige" ms_sup ON ms_sup."idSupGreige" = po_grg."fidSupGreige"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
		FROM
			ho."trGreigeRcvdSupRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail"
) grg ON grg."fidPOGreige" = po_grg_dtl."fidPOGreige" AND grg."fidPOGreigeDetail" = po_grg_dtl."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp2"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOGreige" = "xTmp3"."fidPOGreige" AND "xTmp2"."idPOGreigeDetail" = "xTmp3"."fidPOGreigeDetail"
LEFT JOIN "xTmp4" ON 
	"xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail";

DROP TABLE "xTmp2",
 "xTmp3",
 "xTmp4";
END; $$;
 6   DROP FUNCTION public."f_LaporanSisa_Greige_backup"();
       public       postgres    false                       1255    16409    isdigit(text)    FUNCTION     �   CREATE FUNCTION public.isdigit(text) RETURNS boolean
    LANGUAGE sql
    AS $_$
  select $1 ~ '^(-)?[0-9]+$' as result
  $_$;
 $   DROP FUNCTION public.isdigit(text);
       public       postgres    false                       1255    16410    trig_FillStockBenang_Keluar()    FUNCTION     �  CREATE FUNCTION public."trig_FillStockBenang_Keluar"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	  Declare
					v_idpomakloon INTEGER;
					v_oldstatus integer;
					v_newstatus integer;

		
		Begin
				 IF TG_OP = 'UPDATE' THEN
						 v_idpomakloon := OLD."idPOMakloon";
						 v_oldstatus := OLD."Status";
						 v_newstatus := NEW."Status";

						 if (v_oldstatus < 10 and v_newstatus = 10) Then 
								 Create temporary table tmp1 ("fidBenang"			integer
																		, "fidMakloon"		INTEGER
																		, "Ukuran"				varchar(3)
																		, "NoLot"					varchar(15)
																		, "MerkBenang"		varchar(40)
																		, "Qty_Ball"			decimal(12,3)
																		, "Qty_Kg"				decimal(12,2)
																		, "idPOMaklonBenang"	integer
																		, "idKartuStockBenang" integer 
																		, "Tanggal"        date
																		, "NoPO"					 varchar(50));

								 insert into tmp1
								 select t1."fidBenang"
												, t2."fidMakloon"
												, t1."Ukuran"
												, t1."NoLot"
												, t3."MerkBenang"
												, t1."Qty_Ball"
												, t1."Qty_Kg"
												, t1."idPOMakloonBenang"	
												, t4."idKartuStockBenang"
												, t2."Tanggal"
												, t2."NoPO"
									from ho."trPOMakloonBenang" t1 inner join ho."trPOMakloon" t2 on "idPOMakloon" = "fidPOMakloon"
																								inner JOIN "dataMaster"."msBenang" t3 on "idBenang" = t1."fidBenang"
																								left outer join ho."luKartuStockBenang" t4 
																										on (t1."fidBenang" = t4."fidBenang"
																												and t1."Ukuran" = t4."Ukuran"
																												and t1."NoLot" = t4."NoLot"
																												and t2."fidMakloon" = t4."fidMakloon")
									where "fidPOMakloon" = v_idpomakloon;

									select * from ho."luStockBenang"
									 order by "fidBenang", "Ukuran", "NoLot";
											select * from ho."luKartuStockBenang"
											 order by "TanggalJam", "Tipe";
									
											Update ho."luStockBenang"
												 set "Qty_Ball" = "luStockBenang"."Qty_Ball" - tmp1."Qty_Ball"
													 , "Qty_Kg"   = "luStockBenang"."Qty_Kg" - tmp1."Qty_Kg"
												From tmp1
											 where tmp1."fidBenang" = "luStockBenang"."fidBenang"
															and tmp1."Ukuran" = "luStockBenang"."Ukuran"
															and tmp1."NoLot" = "luStockBenang"."NoLot"
															and tmp1."fidMakloon" = "luStockBenang"."fidMakloon";

											insert into ho."luKartuStockBenang" ("Tipe"
																														, "TanggalJam"
																														, "Deskripsi"
																														, "Qty_Ball"
																														, "Qty_Kg"
																														, "fidBenang"
																														, "Ukuran"
																														, "fidTrx"
																														, "NoLot"
																														, "fidMakloon")
										Select 101
													, "Tanggal"
													, 'USED ' || "NoPO"
													, "Qty_Ball"
													, "Qty_Kg"
													, "fidBenang"
													, "Ukuran"
													, "idPOMaklonBenang"
													, "NoLot"
													, "fidMakloon"
										from tmp1;

									select * from ho."luStockBenang"
									 order by "fidBenang", "Ukuran", "NoLot";
									select * from ho."luKartuStockBenang"
									 order by "TanggalJam", "Tipe";

									drop table tmp1;
							End If;
					End If;
					Return NEW;
		End;
	$$;
 6   DROP FUNCTION public."trig_FillStockBenang_Keluar"();
       public       postgres    false                       1255    16411 .   udf_KartuStockBenang(integer, character, date)    FUNCTION     �  CREATE FUNCTION public."udf_KartuStockBenang"(v_idbenang integer, v_ukuran character, v_pertanggal date) RETURNS TABLE("TanggalJam" character, "Deskripsi" character varying, "QtyMasuk_Ball" numeric, "QtyMasuk_Kg" numeric, "QtyKeluar_Ball" numeric, "QtyKeluar_Kg" numeric, "QtyBalance_Ball" numeric, "QtyBalance_Kg" numeric, "Tipe" smallint, "fidMakloon" smallint)
    LANGUAGE plpgsql
    AS $$	  
	  Declare 
				v_qtyst_ball decimal(12,3);
			  v_qtyst_kg   decimal(12,2);
			  v_qtysum_ball decimal(12,3);
			  v_qtysum_kg   decimal(12,3);
        v_start      timestamp;
				
		Begin
				select z."TanggalJam", z."Qty_Ball", z."Qty_Kg"
					into v_start, v_qtyst_ball, v_qtyst_kg
				  from (Select x."TanggalJam", x."Qty_Ball", x."Qty_Kg"
												, row_number() over (order by x."TanggalJam" Desc) as recno
									from ho."luKartuStockBenang" x
								 where x."TanggalJam" < "v_pertanggal"
											and x."fidBenang" = "v_idbenang"
											and x."Ukuran" = "v_ukuran"
												and x."Tipe" = 0
								) z
				 where z.recno = 1;
				
			  if "v_start" is null Then v_start := '1900-1-1'::date;
				End If;

				If v_qtyst_ball is null then v_qtyst_ball := 0;
				End If;

				If v_qtyst_kg is null then v_qtyst_kg := 0;
				End If;

				Select sum("SumQtyMasuk_Ball"-"SumQtyKeluar_Ball") as "SumQty_Ball"
							, Sum("SumQtyMasuk_Kg"-"SumQtyKeluar_Kg") as "SumQty_Kg"
				into v_qtysum_ball, v_qtysum_kg
				From ( 	Select Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
																Else 0
														 End as "SumQtyMasuk_Ball"
											,	Case When x."Tipe" > 100 Then x."Qty_Ball"
																	Else 0
														 End as "SumQtyKeluar_Ball"
											, Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyMasuk_Kg"
											,	Case When x."Tipe" > 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyKeluar_Kg"
											from ho."luKartuStockBenang" x
								 where x."TanggalJam" >=  v_start
											and x."TanggalJam" < v_pertanggal
											and x."fidBenang" = v_idbenang
											and x."Ukuran" = v_ukuran 
											and x."Tipe" > 0
							) y;

				If v_qtysum_ball is null Then v_qtysum_ball := 0;
				End if;

				If v_qtysum_kg is null then v_qtysum_kg := 0;
				End If;

				Create Temporary table temp_table ("TanggalJam" char(10)
																		, "Deskripsi" Varchar(100)
																		, "QtyMasuk_Ball" decimal(12,3)
																		, "QtyMasuk_Kg" decimal(12,2)
																		, "QtyKeluar_Ball" decimal(12,3)
																		, "QtyKeluar_Kg" decimal(12,2)
																		, "QtyBalance_Ball" decimal(12,3)
																		, "QtyBalance_Kg" decimal(12,2)
																		, "Tipe" int2
																		, "fidMakloon" int2);

		  Insert into temp_table
			Values ('','Stock sebelum tanggal ' || to_char(v_pertanggal,'DD-MM-YYYY')
								, 0
								, 0
								, 0
								, 0
								, v_qtyst_ball + v_qtysum_ball 
								, v_qtyst_kg   + v_qtysum_kg 
								, 0
								, 0);


			  Return Query
			  Select * from temp_table
				Union
        Select Cast(to_char(x."TanggalJam", 'DD-MM-YYYY') as Char(10)) as "TanggalJam"
								, x."Deskripsi"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyMasuk_Ball"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyMasuk_Kg"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyKeluar_Ball"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyKeluar_Kg"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyBalance_Ball"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyBalance_Kg"
								, x."Tipe"
							  , x."fidMakloon"
						from ho."luKartuStockBenang" x 
					where x."TanggalJam" > v_pertanggal
									and x."fidBenang" = "v_idbenang"
									and x."Ukuran" = "v_ukuran"
						order by "TanggalJam", "Tipe" ;

					Drop table temp_table;

		End;
		$$;
 h   DROP FUNCTION public."udf_KartuStockBenang"(v_idbenang integer, v_ukuran character, v_pertanggal date);
       public       postgres    false                        1255    16412 >   udf_KartuStockBenangNoLot(integer, character, character, date)    FUNCTION     F  CREATE FUNCTION public."udf_KartuStockBenangNoLot"(v_idbenang integer, v_ukuran character, v_nolot character, v_pertanggal date) RETURNS TABLE("TanggalJam" character, "Deskripsi" character varying, "NoLot" character varying, "QtyMasuk_Ball" numeric, "QtyMasuk_Kg" numeric, "QtyKeluar_Ball" numeric, "QtyKeluar_Kg" numeric, "QtyBalance_Ball" numeric, "QtyBalance_Kg" numeric, "Tipe" smallint, "fidMakloon" smallint)
    LANGUAGE plpgsql
    AS $$
	  Declare 
				v_qtyst_ball decimal(12,3);
			  v_qtyst_kg   decimal(12,2);
			  v_qtysum_ball decimal(12,3);
			  v_qtysum_kg   decimal(12,3);
        v_start      timestamp;
				
		Begin
				select z."TanggalJam", z."Qty_Ball", z."Qty_Kg"
					into v_start, v_qtyst_ball, v_qtyst_kg
				  from (Select x."TanggalJam", x."Qty_Ball", x."Qty_Kg"
												, row_number() over (order by x."TanggalJam" Desc) as recno
									from ho."luKartuStockBenang" x
								 where x."TanggalJam" < "v_pertanggal"
											and x."fidBenang" = "v_idbenang"
											and x."Ukuran" = "v_ukuran"
												and x."Tipe" = 0
								) z
				 where z.recno = 1;
				
			  if "v_start" is null Then v_start := '1900-1-1'::date;
				End If;

				If v_qtyst_ball is null then v_qtyst_ball := 0;
				End If;

				If v_qtyst_kg is null then v_qtyst_kg := 0;
				End If;

				Select sum("SumQtyMasuk_Ball"-"SumQtyKeluar_Ball") as "SumQty_Ball"
							, Sum("SumQtyMasuk_Kg"-"SumQtyKeluar_Kg") as "SumQty_Kg"
				into v_qtysum_ball, v_qtysum_kg
				From ( 	Select Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
																Else 0
														 End as "SumQtyMasuk_Ball"
											,	Case When x."Tipe" > 100 Then x."Qty_Ball"
																	Else 0
														 End as "SumQtyKeluar_Ball"
											, Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyMasuk_Kg"
											,	Case When x."Tipe" > 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyKeluar_Kg"
											from ho."luKartuStockBenang" x
								 where x."TanggalJam" >=  v_start
											and x."TanggalJam" < v_pertanggal
											and x."fidBenang" = v_idbenang
											and x."Ukuran" = v_ukuran 
											and x."Tipe" > 0
							) y;

				If v_qtysum_ball is null Then v_qtysum_ball := 0;
				End if;

				If v_qtysum_kg is null then v_qtysum_kg := 0;
				End If;

				Create Temporary table temp_table ("TanggalJam" char(10)
																		, "Deskripsi" Varchar(100)
																		, "NoLot" varchar(15)
																		, "QtyMasuk_Ball" decimal(12,3)
																		, "QtyMasuk_Kg" decimal(12,2)
																		, "QtyKeluar_Ball" decimal(12,3)
																		, "QtyKeluar_Kg" decimal(12,2)
																		, "QtyBalance_Ball" decimal(12,3)
																		, "QtyBalance_Kg" decimal(12,2)
																		, "Tipe" int2
																		, "fidMakloon" int2);

		  Insert into temp_table
			Values ('','Stock sebelum tanggal ' || to_char(v_pertanggal,'DD-MM-YYYY')
								, ''
								, 0
								, 0
								, 0
								, 0
								, v_qtyst_ball + v_qtysum_ball 
								, v_qtyst_kg   + v_qtysum_kg 
								, 0
								, 0);


			  Return Query
			  Select * from temp_table
				Union
        Select Cast(to_char(x."TanggalJam", 'DD-MM-YYYY') as Char(10)) as "TanggalJam"
								, x."Deskripsi"
								, x."NoLot"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyMasuk_Ball"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyMasuk_Kg"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyKeluar_Ball"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyKeluar_Kg"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyBalance_Ball"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyBalance_Kg"
								, x."Tipe"
							  , x."fidMakloon"
						from ho."luKartuStockBenang" x 
					where x."TanggalJam" > v_pertanggal
									and x."fidBenang" = "v_idbenang"
									and x."Ukuran" = "v_ukuran"
						order by "TanggalJam", "Tipe" ;

					Drop table temp_table;

		End;
		$$;
 �   DROP FUNCTION public."udf_KartuStockBenangNoLot"(v_idbenang integer, v_ukuran character, v_nolot character, v_pertanggal date);
       public       postgres    false            !           1255    16413 6   udf_SetNoHargaCelupan(integer, date, integer, integer)    FUNCTION     a  CREATE FUNCTION public."udf_SetNoHargaCelupan"(v_idcelupan integer, v_tanggal date, v_fromno integer, v_tono integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_idstart integer := 0;

BEGIN
  select "idCelupanHarga"
    into v_idstart
   from "dataMaster"."msCelupanHarga"
   where "fidCelupan" = "v_idcelupan"
				and "PerTanggal" = "v_tanggal"
				and "Nomor" = "v_fromno";

	if v_fromno > v_tono THEN
		BEGIN
			Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "msCelupanHarga"."Nomor" + 1
			 where "fidCelupan" = "v_idcelupan"
						and "PerTanggal" = "v_tanggal"
						and "Nomor" >= "v_tono"
						and "Nomor" < "v_fromno";

	  End;
  ELSEIF v_tono > v_fromno THEN
		BEGIN
			Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "msCelupanHarga"."Nomor" - 1
			 where "fidCelupan" = "v_idcelupan"
						and "PerTanggal" = "v_tanggal"
						and "Nomor" > "v_fromno"
						and "Nomor" <= "v_tono";
 
		End;
  End if ;

	Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "v_tono"
    where "idCelupanHarga" = "v_idstart";

END;
$$;
 u   DROP FUNCTION public."udf_SetNoHargaCelupan"(v_idcelupan integer, v_tanggal date, v_fromno integer, v_tono integer);
       public       postgres    false            "           1255    16414    udf_StockBenangMasuk(integer)    FUNCTION     �  CREATE FUNCTION public."udf_StockBenangMasuk"(v_idbenangrcvd integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
		  DECLARE
					v_idmakloon integer;
			Begin
				  select "fidMakloon" into v_idmakloon
						from ho."trBenangRcvd" 
				   where "idBenangRcvd" = v_idbenangrcvd ;

				  insert into ho."luStockBenang" ("fidBenang"
																				, "fidMakloon"
																				, "Ukuran"
																				, "NoLot"
																				, "MerkBenang"
																				, "Qty_Ball"
																				, "Qty_Kg")
				  select rcv."fidBenang"
									, v_idmakloon
									, rcv."Ukuran"
									, rcv."NoLot"
									, mst."MerkBenang"
									, rcv."QtyRcvd_Ball"
									, rcv."QtyRcvd_Kg"
						from ho."trBenangRcvdDetail" rcv inner join "dataMaster"."msBenang" mst
													on mst."idBenang" = rcv."fidBenang"
						where rcv."fidBenangRcvd" = v_idBenangRcvd ;

			End;

$$;
 E   DROP FUNCTION public."udf_StockBenangMasuk"(v_idbenangrcvd integer);
       public       postgres    false            #           1255    16415    udf_ValidasiDelGudang(integer)    FUNCTION     �   CREATE FUNCTION public."udf_ValidasiDelGudang"(v_idgudang integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    

BEGIN
  Return 0;


END;
$$;
 B   DROP FUNCTION public."udf_ValidasiDelGudang"(v_idgudang integer);
       public       postgres    false            $           1255    16416    udf_ValidasiDelToko(integer)    FUNCTION     �   CREATE FUNCTION public."udf_ValidasiDelToko"(v_idtoko integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    

BEGIN
  Return 0;


END;
$$;
 >   DROP FUNCTION public."udf_ValidasiDelToko"(v_idtoko integer);
       public       postgres    false            %           1255    16417    uuid_generate_v1()    FUNCTION     }   CREATE FUNCTION public.uuid_generate_v1() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v1';
 )   DROP FUNCTION public.uuid_generate_v1();
       public       postgres    false            &           1255    16418    uuid_generate_v1mc()    FUNCTION     �   CREATE FUNCTION public.uuid_generate_v1mc() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v1mc';
 +   DROP FUNCTION public.uuid_generate_v1mc();
       public       postgres    false            '           1255    16419    uuid_generate_v3(uuid, text)    FUNCTION     �   CREATE FUNCTION public.uuid_generate_v3(namespace uuid, name text) RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v3';
 B   DROP FUNCTION public.uuid_generate_v3(namespace uuid, name text);
       public       postgres    false            (           1255    16420    uuid_generate_v4()    FUNCTION     }   CREATE FUNCTION public.uuid_generate_v4() RETURNS uuid
    LANGUAGE c STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v4';
 )   DROP FUNCTION public.uuid_generate_v4();
       public       postgres    false            )           1255    16421    uuid_generate_v5(uuid, text)    FUNCTION     �   CREATE FUNCTION public.uuid_generate_v5(namespace uuid, name text) RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_generate_v5';
 B   DROP FUNCTION public.uuid_generate_v5(namespace uuid, name text);
       public       postgres    false            *           1255    16422 
   uuid_nil()    FUNCTION     w   CREATE FUNCTION public.uuid_nil() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_nil';
 !   DROP FUNCTION public.uuid_nil();
       public       postgres    false            +           1255    16423    uuid_ns_dns()    FUNCTION     }   CREATE FUNCTION public.uuid_ns_dns() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_dns';
 $   DROP FUNCTION public.uuid_ns_dns();
       public       postgres    false            ,           1255    16424    uuid_ns_oid()    FUNCTION     }   CREATE FUNCTION public.uuid_ns_oid() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_oid';
 $   DROP FUNCTION public.uuid_ns_oid();
       public       postgres    false            -           1255    16425    uuid_ns_url()    FUNCTION     }   CREATE FUNCTION public.uuid_ns_url() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_url';
 $   DROP FUNCTION public.uuid_ns_url();
       public       postgres    false            .           1255    16426    uuid_ns_x500()    FUNCTION        CREATE FUNCTION public.uuid_ns_x500() RETURNS uuid
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/uuid-ossp', 'uuid_ns_x500';
 %   DROP FUNCTION public.uuid_ns_x500();
       public       postgres    false            �            1259    16427    msCustomer_idMsCustomer_seq    SEQUENCE     �   CREATE SEQUENCE "customerRelationship"."msCustomer_idMsCustomer_seq"
    START WITH 104
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE "customerRelationship"."msCustomer_idMsCustomer_seq";
       customerRelationship       postgres    false    6            �            1259    16429    msBarang    TABLE     �  CREATE TABLE "dataMaster"."msBarang" (
    "KodeBarang" character varying(100) NOT NULL,
    "fidKelompokBarang" integer,
    "NamaBarang" character varying(200),
    "fidSatuanKecil" integer,
    "fidSatuanBesar" smallint,
    "HargaBeli" numeric(15,0),
    "fidSupplier" integer,
    "PhotoBarang" text,
    "Deskripsi" text,
    "TglInput" date,
    "TglUpdate" date,
    userinput character varying(100),
    userupdate character varying(100),
    "Stok" integer,
    "Merk" character varying(100)
);
 $   DROP TABLE "dataMaster"."msBarang";
    
   dataMaster         postgres    false    9            �            1259    16435    msConfig    TABLE     �   CREATE TABLE "dataMaster"."msConfig" (
    "idConfig" smallint NOT NULL,
    "Caption" character varying(255),
    "keyText" character varying(255),
    "keyValue" smallint
);
 $   DROP TABLE "dataMaster"."msConfig";
    
   dataMaster         postgres    false    9            �            1259    16441 
   msCustomer    TABLE     �  CREATE TABLE "dataMaster"."msCustomer" (
    "idCustomer" integer NOT NULL,
    "NamaLengkap" character varying(200),
    "NamaPanggilan" character varying(100),
    "NoHP" character varying(20),
    "Alamat" text,
    "Email" character varying(100),
    "Institusi" character varying(100),
    "Negara" character varying(200),
    "Provinsi" character varying(200),
    "KotaKabupaten" character varying(200),
    "Photo" text
);
 &   DROP TABLE "dataMaster"."msCustomer";
    
   dataMaster         postgres    false    9            �           0    0    COLUMN "msCustomer"."Institusi"    COMMENT     Y   COMMENT ON COLUMN "dataMaster"."msCustomer"."Institusi" IS 'perorangan atau perusahaan';
         
   dataMaster       postgres    false    204            �            1259    16447    msCustomer_idCustomer_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msCustomer_idCustomer_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE "dataMaster"."msCustomer_idCustomer_seq";
    
   dataMaster       postgres    false    9    204            �           0    0    msCustomer_idCustomer_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE "dataMaster"."msCustomer_idCustomer_seq" OWNED BY "dataMaster"."msCustomer"."idCustomer";
         
   dataMaster       postgres    false    205            �            1259    16449    msGudang    TABLE     �   CREATE TABLE "dataMaster"."msGudang" (
    "idGudang" integer NOT NULL,
    "KodeGudang" character varying(6),
    "NamaGudang" character varying(30),
    "AlamatGudang" character varying(200),
    "KotaGudang" character varying(30)
);
 $   DROP TABLE "dataMaster"."msGudang";
    
   dataMaster         postgres    false    9            �            1259    16452 
   msJenisKas    TABLE     �   CREATE TABLE "dataMaster"."msJenisKas" (
    "idJenisKas" smallint NOT NULL,
    "Nama" character varying(255),
    "Keterangan" character varying(255)
);
 &   DROP TABLE "dataMaster"."msJenisKas";
    
   dataMaster         postgres    false    9            �            1259    16458    msJenisKas_idJenisKas_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msJenisKas_idJenisKas_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE "dataMaster"."msJenisKas_idJenisKas_seq";
    
   dataMaster       postgres    false    207    9            �           0    0    msJenisKas_idJenisKas_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE "dataMaster"."msJenisKas_idJenisKas_seq" OWNED BY "dataMaster"."msJenisKas"."idJenisKas";
         
   dataMaster       postgres    false    208            �            1259    16460    msJenisKendaraan    TABLE     �   CREATE TABLE "dataMaster"."msJenisKendaraan" (
    "idJenisKendaraan" smallint NOT NULL,
    "NamaJenisKendaraan" character varying(255),
    "KodeJenisKendaraan" character varying(50)
);
 ,   DROP TABLE "dataMaster"."msJenisKendaraan";
    
   dataMaster         postgres    false    9            �            1259    16463 %   msJenisKendaraan_idJenisKendaraan_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq";
    
   dataMaster       postgres    false    9    209            �           0    0 %   msJenisKendaraan_idJenisKendaraan_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq" OWNED BY "dataMaster"."msJenisKendaraan"."idJenisKendaraan";
         
   dataMaster       postgres    false    210            �            1259    16465    msKelompokBarang    TABLE     �   CREATE TABLE "dataMaster"."msKelompokBarang" (
    "idKelompokBarang" smallint NOT NULL,
    "KodeKelompokBarang" character varying(10),
    "NamaKelompokBarang" character varying(50)
);
 ,   DROP TABLE "dataMaster"."msKelompokBarang";
    
   dataMaster         postgres    false    9            �            1259    16468 %   msKelompokBarang_idKelompokBarang_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msKelompokBarang_idKelompokBarang_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE "dataMaster"."msKelompokBarang_idKelompokBarang_seq";
    
   dataMaster       postgres    false    9    211            �           0    0 %   msKelompokBarang_idKelompokBarang_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE "dataMaster"."msKelompokBarang_idKelompokBarang_seq" OWNED BY "dataMaster"."msKelompokBarang"."idKelompokBarang";
         
   dataMaster       postgres    false    212            �            1259    16470    msKendaraan    TABLE     W  CREATE TABLE "dataMaster"."msKendaraan" (
    "idKendaraan" integer NOT NULL,
    "PlatNomor" character varying(20),
    "Merk" character varying(50),
    "Type" character varying(50),
    "fidJenisKendaraan" smallint,
    "Model" character varying(50),
    "TahunPembuatan" character varying(4),
    "Silinder" character varying(50),
    "NoRangka" character varying(50),
    "NoMesin" character varying(50),
    "Warna" character varying(50),
    "BahanBakar" character varying(50),
    "WarnaTNKB" character varying(50),
    "TahunRegistrasi" character varying(4),
    "NoBPKB" character varying(50),
    "KodeLokasi" character varying(50),
    "NoUrutDaftar" character varying(50),
    "PhotoKendaraan1" text,
    "PhotoKendaraan2" text,
    "PhotoKendaraan3" text,
    "PanjangKaroseri" integer,
    "LebarKaroseri" integer,
    "TinggiKaroseri" integer,
    "Dimensi" integer,
    "BeratKosong" integer,
    "BeratMax" integer,
    "PanjangMobil" integer,
    "LebarMobil" integer,
    "TinggiMobil" integer,
    "KecepatanMax" integer,
    "TenagaMax_" character varying(100),
    "UkuranBan_" character varying(100),
    "UkuranRoda_" character varying(100),
    "KapasitasMuatan_" character varying(100),
    "NamaKendaraan" character varying(200),
    "TenagaMax" integer,
    "UkuranBan" integer,
    "UkuranRoda" integer,
    "KapasitasMuatan" integer
);
 '   DROP TABLE "dataMaster"."msKendaraan";
    
   dataMaster         postgres    false    9            �           0    0    COLUMN "msKendaraan"."Silinder"    COMMENT     K   COMMENT ON COLUMN "dataMaster"."msKendaraan"."Silinder" IS 'isi silinder';
         
   dataMaster       postgres    false    213            �           0    0    COLUMN "msKendaraan"."NoRangka"    COMMENT     K   COMMENT ON COLUMN "dataMaster"."msKendaraan"."NoRangka" IS 'nomor rangka';
         
   dataMaster       postgres    false    213            �           0    0    COLUMN "msKendaraan"."NoMesin"    COMMENT     I   COMMENT ON COLUMN "dataMaster"."msKendaraan"."NoMesin" IS 'nomor mesin';
         
   dataMaster       postgres    false    213            �           0    0 !   COLUMN "msKendaraan"."KodeLokasi"    COMMENT     L   COMMENT ON COLUMN "dataMaster"."msKendaraan"."KodeLokasi" IS 'kode lokasi';
         
   dataMaster       postgres    false    213            �           0    0 &   COLUMN "msKendaraan"."PanjangKaroseri"    COMMENT     H   COMMENT ON COLUMN "dataMaster"."msKendaraan"."PanjangKaroseri" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0 $   COLUMN "msKendaraan"."LebarKaroseri"    COMMENT     F   COMMENT ON COLUMN "dataMaster"."msKendaraan"."LebarKaroseri" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0 %   COLUMN "msKendaraan"."TinggiKaroseri"    COMMENT     G   COMMENT ON COLUMN "dataMaster"."msKendaraan"."TinggiKaroseri" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0    COLUMN "msKendaraan"."Dimensi"    COMMENT     A   COMMENT ON COLUMN "dataMaster"."msKendaraan"."Dimensi" IS 'cbm';
         
   dataMaster       postgres    false    213            �           0    0 "   COLUMN "msKendaraan"."BeratKosong"    COMMENT     D   COMMENT ON COLUMN "dataMaster"."msKendaraan"."BeratKosong" IS 'kg';
         
   dataMaster       postgres    false    213            �           0    0    COLUMN "msKendaraan"."BeratMax"    COMMENT     A   COMMENT ON COLUMN "dataMaster"."msKendaraan"."BeratMax" IS 'kg';
         
   dataMaster       postgres    false    213            �           0    0 #   COLUMN "msKendaraan"."PanjangMobil"    COMMENT     E   COMMENT ON COLUMN "dataMaster"."msKendaraan"."PanjangMobil" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0 !   COLUMN "msKendaraan"."LebarMobil"    COMMENT     C   COMMENT ON COLUMN "dataMaster"."msKendaraan"."LebarMobil" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0 "   COLUMN "msKendaraan"."TinggiMobil"    COMMENT     D   COMMENT ON COLUMN "dataMaster"."msKendaraan"."TinggiMobil" IS 'cm';
         
   dataMaster       postgres    false    213            �           0    0 #   COLUMN "msKendaraan"."KecepatanMax"    COMMENT     I   COMMENT ON COLUMN "dataMaster"."msKendaraan"."KecepatanMax" IS 'km/jam';
         
   dataMaster       postgres    false    213            �           0    0 !   COLUMN "msKendaraan"."TenagaMax_"    COMMENT     G   COMMENT ON COLUMN "dataMaster"."msKendaraan"."TenagaMax_" IS 'ps/rpm';
         
   dataMaster       postgres    false    213            �           0    0 '   COLUMN "msKendaraan"."KapasitasMuatan_"    COMMENT     M   COMMENT ON COLUMN "dataMaster"."msKendaraan"."KapasitasMuatan_" IS 'kg/cbm';
         
   dataMaster       postgres    false    213            �           0    0     COLUMN "msKendaraan"."TenagaMax"    COMMENT     F   COMMENT ON COLUMN "dataMaster"."msKendaraan"."TenagaMax" IS 'ps/rpm';
         
   dataMaster       postgres    false    213            �           0    0 &   COLUMN "msKendaraan"."KapasitasMuatan"    COMMENT     L   COMMENT ON COLUMN "dataMaster"."msKendaraan"."KapasitasMuatan" IS 'kg/cbm';
         
   dataMaster       postgres    false    213            �            1259    16476    msKendaraan_idKendaraan_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msKendaraan_idKendaraan_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE "dataMaster"."msKendaraan_idKendaraan_seq";
    
   dataMaster       postgres    false    9    213            �           0    0    msKendaraan_idKendaraan_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE "dataMaster"."msKendaraan_idKendaraan_seq" OWNED BY "dataMaster"."msKendaraan"."idKendaraan";
         
   dataMaster       postgres    false    214            �            1259    16478    msPengemudi    TABLE     V  CREATE TABLE "dataMaster"."msPengemudi" (
    "idPengemudi" integer NOT NULL,
    "NamaLengkap" character varying(50),
    "NamaPanggilan" character varying(50),
    "NoHP" character varying(20),
    "Alamat" text,
    "Email" character varying(100),
    "NIK" character varying(100),
    "TglLahir" date,
    "GolDarah" character varying(2),
    "PhotoProfile" text,
    "PhotoKTP" text,
    "PhotoSIM_A" text,
    "PhotoSIM_B1" text,
    "PhotoSIM_B2" text,
    "PunyaSIM_A" smallint,
    "PunyaSIM_B1" smallint,
    "PunyaSIM_B2" smallint,
    "Status" smallint,
    "fidMsReligion" smallint
);
 '   DROP TABLE "dataMaster"."msPengemudi";
    
   dataMaster         postgres    false    9            �           0    0 !   COLUMN "msPengemudi"."PunyaSIM_A"    COMMENT     S   COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_A" IS '0 = tidak , 1 = ya';
         
   dataMaster       postgres    false    215            �           0    0 "   COLUMN "msPengemudi"."PunyaSIM_B1"    COMMENT     T   COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_B1" IS '0 = tidak , 1 = ya';
         
   dataMaster       postgres    false    215            �           0    0 "   COLUMN "msPengemudi"."PunyaSIM_B2"    COMMENT     T   COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_B2" IS '0 = tidak , 1 = ya';
         
   dataMaster       postgres    false    215            �           0    0    COLUMN "msPengemudi"."Status"    COMMENT     n   COMMENT ON COLUMN "dataMaster"."msPengemudi"."Status" IS '10 = available 20 = not available 30 = on the way';
         
   dataMaster       postgres    false    215            �            1259    16484    msPengemudi_idPengemudi_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msPengemudi_idPengemudi_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE "dataMaster"."msPengemudi_idPengemudi_seq";
    
   dataMaster       postgres    false    9    215            �           0    0    msPengemudi_idPengemudi_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE "dataMaster"."msPengemudi_idPengemudi_seq" OWNED BY "dataMaster"."msPengemudi"."idPengemudi";
         
   dataMaster       postgres    false    216            �            1259    16486    msSatuan    TABLE     t   CREATE TABLE "dataMaster"."msSatuan" (
    "idSatuan" smallint NOT NULL,
    "NamaSatuan" character varying(255)
);
 $   DROP TABLE "dataMaster"."msSatuan";
    
   dataMaster         postgres    false    9            �            1259    16489    msSatuan_idSatuan_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msSatuan_idSatuan_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE "dataMaster"."msSatuan_idSatuan_seq";
    
   dataMaster       postgres    false    9    217            �           0    0    msSatuan_idSatuan_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE "dataMaster"."msSatuan_idSatuan_seq" OWNED BY "dataMaster"."msSatuan"."idSatuan";
         
   dataMaster       postgres    false    218            �            1259    16491    msStatusOrder    TABLE     �   CREATE TABLE "dataMaster"."msStatusOrder" (
    "idStatusOrder" integer NOT NULL,
    "NamaStatusOrder" character varying(255)
);
 )   DROP TABLE "dataMaster"."msStatusOrder";
    
   dataMaster         postgres    false    9            
           1259    16777 
   msStatusPO    TABLE     y   CREATE TABLE "dataMaster"."msStatusPO" (
    "idStatusPO" integer NOT NULL,
    "NamaStatusPO" character varying(255)
);
 &   DROP TABLE "dataMaster"."msStatusPO";
    
   dataMaster         postgres    false    9            �            1259    16494 
   msSupplier    TABLE     -  CREATE TABLE "dataMaster"."msSupplier" (
    "idSupplier" smallint NOT NULL,
    "KodeSupplier" character varying(10),
    "NamaSupplier" character varying(40),
    "AlamatSupplier" character varying(200),
    "KotaSupplier" character varying(40),
    "Fax" character varying(20),
    "Alamat2Supplier" character varying(255),
    "Alamat3Supplier" character varying(255),
    "Telepon" character varying(20),
    "Telepon2" character varying(20),
    "Telepon3" character varying(20),
    "Fax2" character varying(20),
    "Email" character varying(40)
);
 &   DROP TABLE "dataMaster"."msSupplier";
    
   dataMaster         postgres    false    9            �            1259    16500    msSupplier_idSupplier_seq    SEQUENCE     �   CREATE SEQUENCE "dataMaster"."msSupplier_idSupplier_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE "dataMaster"."msSupplier_idSupplier_seq";
    
   dataMaster       postgres    false    9    220            �           0    0    msSupplier_idSupplier_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE "dataMaster"."msSupplier_idSupplier_seq" OWNED BY "dataMaster"."msSupplier"."idSupplier";
         
   dataMaster       postgres    false    221            �            1259    16502    trOrder    TABLE     �  CREATE TABLE ho."trOrder" (
    "NoOrder" character varying(100) NOT NULL,
    "fidKendaraan" integer,
    "JenisTrip" character varying(100),
    "DeskripsiBarang" text,
    "Berat" smallint,
    "Panjang" smallint,
    "Lebar" smallint,
    "Tinggi" smallint,
    "Total" smallint,
    "TglKirim" date,
    "WaktuPengambilan" time(6) without time zone,
    "LokasiAsal" character varying(200),
    "LokasiTujuan" character varying(200),
    "AreaAsal" character varying(200),
    "AreaTujuan" character varying(200),
    "DetailLokasiPengambilan" text,
    "DetailLokasiTujuan" text,
    "TipeKawasanAsal" character varying(100),
    "TipeKawasanTujuan" character varying(100),
    "InstruksiKhusus" text,
    "Totalharga" real,
    "PhotoBarang" text,
    "AddPackaging" smallint,
    "AddTenagaAngkut" smallint,
    "TglInput" date,
    "UserInput" character varying(50),
    "TglUpdate" date,
    "fidStatusOrder" smallint,
    "fidCustomer" integer,
    "fidPengemudi" integer,
    "UserUpdate" character varying(50),
    "NamaPajak" character varying(50),
    "PersenPajak" numeric(5,2),
    "TotalHargaBayar" numeric(15,2),
    "TglOrder" date
);
    DROP TABLE ho."trOrder";
       ho         postgres    false    12            �           0    0    COLUMN "trOrder"."JenisTrip"    COMMENT     H   COMMENT ON COLUMN ho."trOrder"."JenisTrip" IS 'single atau multi trip';
            ho       postgres    false    222            �           0    0    COLUMN "trOrder"."Berat"    COMMENT     0   COMMENT ON COLUMN ho."trOrder"."Berat" IS 'kg';
            ho       postgres    false    222            �           0    0    COLUMN "trOrder"."Panjang"    COMMENT     2   COMMENT ON COLUMN ho."trOrder"."Panjang" IS 'cm';
            ho       postgres    false    222            �           0    0    COLUMN "trOrder"."Lebar"    COMMENT     0   COMMENT ON COLUMN ho."trOrder"."Lebar" IS 'cm';
            ho       postgres    false    222            �           0    0    COLUMN "trOrder"."Tinggi"    COMMENT     1   COMMENT ON COLUMN ho."trOrder"."Tinggi" IS 'cm';
            ho       postgres    false    222            �           0    0    COLUMN "trOrder"."Total"    COMMENT     I   COMMENT ON COLUMN ho."trOrder"."Total" IS 'jika diubah jadi cbm / feet';
            ho       postgres    false    222            �           0    0 *   COLUMN "trOrder"."DetailLokasiPengambilan"    COMMENT     H   COMMENT ON COLUMN ho."trOrder"."DetailLokasiPengambilan" IS 'optional';
            ho       postgres    false    222            �           0    0 %   COLUMN "trOrder"."DetailLokasiTujuan"    COMMENT     C   COMMENT ON COLUMN ho."trOrder"."DetailLokasiTujuan" IS 'optional';
            ho       postgres    false    222            �           0    0 !   COLUMN "trOrder"."fidStatusOrder"    COMMENT     ^   COMMENT ON COLUMN ho."trOrder"."fidStatusOrder" IS '1 = order , 2 = acc , 3 = order selesai';
            ho       postgres    false    222            �            1259    16508    trOrderDetail    TABLE     �   CREATE TABLE ho."trOrderDetail" (
    "idOrderDetail" integer NOT NULL,
    "NoOrder" character varying(100),
    "NamaBiaya" character varying(200),
    "Nominal" real,
    "fidJenisKas" smallint
);
    DROP TABLE ho."trOrderDetail";
       ho         postgres    false    12            �            1259    16511    trOrderDet_idOrderDet_seq    SEQUENCE     �   CREATE SEQUENCE ho."trOrderDet_idOrderDet_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE ho."trOrderDet_idOrderDet_seq";
       ho       postgres    false    223    12            �           0    0    trOrderDet_idOrderDet_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE ho."trOrderDet_idOrderDet_seq" OWNED BY ho."trOrderDetail"."idOrderDetail";
            ho       postgres    false    224                       1259    16763    trPO    TABLE     5  CREATE TABLE ho."trPO" (
    "idPO" integer NOT NULL,
    "NoPembelian" character varying(50),
    "TglPembelian" date,
    "fidKendaraan" smallint,
    "fidSupplier" smallint,
    "NoFaktur" character varying(50),
    "TotalBiaya" numeric(12,2),
    "TotalDiskon" numeric(12,2),
    "UserInput" character varying(50),
    "TglInput" date,
    "UserApproval" character varying(50),
    "TglApproval" date,
    "TotalBiayaBayar" numeric(12,2),
    "fidStatusPO" smallint,
    "UserUpdate" character varying(50),
    "TglUpdate" date,
    "TotalPPN" numeric(12,2)
);
    DROP TABLE ho."trPO";
       ho         postgres    false    12            	           1259    16771 
   trPODetail    TABLE     +  CREATE TABLE ho."trPODetail" (
    "idPODetail" integer NOT NULL,
    "fidPO" integer,
    "Qty" integer,
    "HargaBeli" numeric(12,2),
    "Diskon" numeric(5,2),
    "PPN" numeric(12,2),
    "SubTotal" numeric(12,2),
    "KodeBarang" character varying(50),
    "fidJenisKas" smallint DEFAULT 2
);
    DROP TABLE ho."trPODetail";
       ho         postgres    false    12                       1259    16769    trPODetail_idPODetail_seq    SEQUENCE     �   CREATE SEQUENCE ho."trPODetail_idPODetail_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE ho."trPODetail_idPODetail_seq";
       ho       postgres    false    265    12            �           0    0    trPODetail_idPODetail_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE ho."trPODetail_idPODetail_seq" OWNED BY ho."trPODetail"."idPODetail";
            ho       postgres    false    264                       1259    16761    trPO_idPO_seq    SEQUENCE     �   CREATE SEQUENCE ho."trPO_idPO_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE ho."trPO_idPO_seq";
       ho       postgres    false    12    263            �           0    0    trPO_idPO_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE ho."trPO_idPO_seq" OWNED BY ho."trPO"."idPO";
            ho       postgres    false    262            �            1259    16513    msEmployee_idMsEmployee_seq    SEQUENCE     �   CREATE SEQUENCE "humanCapital"."msEmployee_idMsEmployee_seq"
    START WITH 97
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE "humanCapital"."msEmployee_idMsEmployee_seq";
       humanCapital       postgres    false    11            �            1259    16515 
   msEmployee    TABLE     p  CREATE TABLE "humanCapital"."msEmployee" (
    "idMsEmployee" integer DEFAULT nextval('"humanCapital"."msEmployee_idMsEmployee_seq"'::regclass) NOT NULL,
    "EmpCode" character varying(15),
    "Name" character varying(50),
    "DateOfBirth" date,
    "CityOfBirth" character varying(40),
    "IDCardNumber" character varying(30),
    "Address" text,
    "City" character varying(30),
    "PhoneNumber1" character varying(20),
    "PhoneNumber2" character varying(20),
    "Email" character varying(50),
    "ApprovalStatus" integer,
    "NickName" character varying(50),
    "fidGender" integer,
    "fidMsPosition" smallint,
    "fidMsReligion" smallint,
    "KodeAhass" character varying(6),
    "KodeDealer" character varying(6),
    "KodeSalesAHM" character varying(20),
    "Jabatan" character varying(50),
    "fidPendidikan" character varying(6),
    "fidHobi" character varying(6),
    "Provinsi" character varying(30),
    "KodePos" character varying(5),
    "Address_KTP" text,
    "Provinsi_KTP" character varying(30),
    "City_KTP" character varying(30),
    "KodePos_KTP" character varying(5),
    "isSales" smallint
);
 (   DROP TABLE "humanCapital"."msEmployee";
       humanCapital         postgres    false    225    11            �           0    0    COLUMN "msEmployee"."EmpCode"    COMMENT     �   COMMENT ON COLUMN "humanCapital"."msEmployee"."EmpCode" IS '- diisini dengan Nomor Induk Karyawan ( NIK )
- format sesuai kebijakan';
            humanCapital       postgres    false    226            �           0    0    COLUMN "msEmployee"."isSales"    COMMENT     R   COMMENT ON COLUMN "humanCapital"."msEmployee"."isSales" IS '0:NonSales, 1:Sales';
            humanCapital       postgres    false    226            �            1259    16522    msHobi    TABLE     �   CREATE TABLE "humanCapital"."msHobi" (
    "Code" character varying(3) NOT NULL,
    "Description" character varying(50),
    "OrderBy" smallint DEFAULT 0
);
 $   DROP TABLE "humanCapital"."msHobi";
       humanCapital         postgres    false    11            �            1259    16526 5   msOrganizationStructure_idMsOrganizationStructure_seq    SEQUENCE     �   CREATE SEQUENCE "humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq"
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 V   DROP SEQUENCE "humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq";
       humanCapital       postgres    false    11            �            1259    16528    msOrganizationStructure    TABLE     #  CREATE TABLE "humanCapital"."msOrganizationStructure" (
    "idMsOrganizationStructure" integer DEFAULT nextval('"humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq"'::regclass) NOT NULL,
    "fidMsOrg" integer,
    "Description" character(100),
    "Code" character(5)
);
 5   DROP TABLE "humanCapital"."msOrganizationStructure";
       humanCapital         postgres    false    228    11            �            1259    16532    msPendidikan    TABLE     �   CREATE TABLE "humanCapital"."msPendidikan" (
    "Code" character varying(2) NOT NULL,
    "Description" character varying(30),
    "OrderBy" smallint DEFAULT 0
);
 *   DROP TABLE "humanCapital"."msPendidikan";
       humanCapital         postgres    false    11            �            1259    16536    msPosition_idMsPosition_seq    SEQUENCE     �   CREATE SEQUENCE "humanCapital"."msPosition_idMsPosition_seq"
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE "humanCapital"."msPosition_idMsPosition_seq";
       humanCapital       postgres    false    11            �            1259    16538 
   msPosition    TABLE     �   CREATE TABLE "humanCapital"."msPosition" (
    "idMsPosition" integer DEFAULT nextval('"humanCapital"."msPosition_idMsPosition_seq"'::regclass) NOT NULL,
    "PositionCode" character varying(5),
    "Description" character varying(50)
);
 (   DROP TABLE "humanCapital"."msPosition";
       humanCapital         postgres    false    231    11            �            1259    16542    msReligion_idReligion_seq    SEQUENCE     �   CREATE SEQUENCE "humanCapital"."msReligion_idReligion_seq"
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE "humanCapital"."msReligion_idReligion_seq";
       humanCapital       postgres    false    11            �            1259    16544 
   msReligion    TABLE     �   CREATE TABLE "humanCapital"."msReligion" (
    "idMsReligion" integer DEFAULT nextval('"humanCapital"."msReligion_idReligion_seq"'::regclass) NOT NULL,
    "Description" character varying(20)
);
 (   DROP TABLE "humanCapital"."msReligion";
       humanCapital         postgres    false    233    11            �            1259    16548    AppDashboard    TABLE       CREATE TABLE public."AppDashboard" (
    "idAppDashboard" integer NOT NULL,
    "Title" character varying(40),
    "OnClick" character varying(100),
    "Icon" character varying(20),
    "fidAppMenu" smallint DEFAULT 0,
    "fidAppShorcut" smallint DEFAULT 0,
    "OrderBy" smallint,
    "GroupName" character varying(30),
    "Color" character varying(30),
    "InnerInfoURL" character varying(100),
    "ColumnWidth" numeric(2,0) DEFAULT 3,
    "Description" character varying(100),
    "IdResult" character varying(100)
);
 "   DROP TABLE public."AppDashboard";
       public         postgres    false            �           0    0    COLUMN "AppDashboard"."OnClick"    COMMENT     X   COMMENT ON COLUMN public."AppDashboard"."OnClick" IS 'javascript function event click';
            public       postgres    false    235            �           0    0 !   COLUMN "AppDashboard"."GroupName"    COMMENT     b   COMMENT ON COLUMN public."AppDashboard"."GroupName" IS 'Untuk grouping dalam short key tersebut';
            public       postgres    false    235            �           0    0    COLUMN "AppDashboard"."Color"    COMMENT     �  COMMENT ON COLUMN public."AppDashboard"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';
            public       postgres    false    235            �           0    0 $   COLUMN "AppDashboard"."InnerInfoURL"    COMMENT     q   COMMENT ON COLUMN public."AppDashboard"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';
            public       postgres    false    235            �           0    0 #   COLUMN "AppDashboard"."ColumnWidth"    COMMENT     r   COMMENT ON COLUMN public."AppDashboard"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';
            public       postgres    false    235            �            1259    16557    AppDashboard_idAppDashboard_seq    SEQUENCE     �   CREATE SEQUENCE public."AppDashboard_idAppDashboard_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public."AppDashboard_idAppDashboard_seq";
       public       postgres    false    235            �           0    0    AppDashboard_idAppDashboard_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public."AppDashboard_idAppDashboard_seq" OWNED BY public."AppDashboard"."idAppDashboard";
            public       postgres    false    236            �            1259    16559    AppMenus    TABLE     �  CREATE TABLE public."AppMenus" (
    "idAppMenu" integer NOT NULL,
    "Title" character varying(50),
    "URL" character varying(100),
    "IconImg" character varying(30),
    "ColorCode" character varying(20),
    "fidAppMenu" smallint DEFAULT 0,
    "OrderBy" smallint DEFAULT 0,
    "Description" character varying(100),
    "GroupMenu" smallint,
    "Project" smallint DEFAULT 0
);
    DROP TABLE public."AppMenus";
       public         postgres    false            �           0    0    COLUMN "AppMenus"."GroupMenu"    COMMENT     X   COMMENT ON COLUMN public."AppMenus"."GroupMenu" IS '1 = H1
2 = H2
3 = H3
4 = HC3
';
            public       postgres    false    237            �           0    0    COLUMN "AppMenus"."Project"    COMMENT     E   COMMENT ON COLUMN public."AppMenus"."Project" IS '0 = DMS
1 = SPK';
            public       postgres    false    237            �            1259    16565    AppMenus_id_app_menu_seq    SEQUENCE     �   CREATE SEQUENCE public."AppMenus_id_app_menu_seq"
    START WITH 117
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."AppMenus_id_app_menu_seq";
       public       postgres    false    237            �           0    0    AppMenus_id_app_menu_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."AppMenus_id_app_menu_seq" OWNED BY public."AppMenus"."idAppMenu";
            public       postgres    false    238            �            1259    16567    AppShortCut    TABLE     �  CREATE TABLE public."AppShortCut" (
    "idAppShortCut" integer NOT NULL,
    "Title" character varying(40),
    "OnClick" character varying(100),
    "Icon" character varying(20),
    "fidAppMenu" smallint,
    "OrderBy" smallint,
    "GroupName" character varying(30),
    "Color" character varying(30),
    "InnerInfoURL" character varying(100),
    "ColumnWidth" numeric(2,0) DEFAULT 3,
    "Description" character varying(100)
);
 !   DROP TABLE public."AppShortCut";
       public         postgres    false            �           0    0    COLUMN "AppShortCut"."OnClick"    COMMENT     W   COMMENT ON COLUMN public."AppShortCut"."OnClick" IS 'javascript function event click';
            public       postgres    false    239            �           0    0     COLUMN "AppShortCut"."GroupName"    COMMENT     a   COMMENT ON COLUMN public."AppShortCut"."GroupName" IS 'Untuk grouping dalam short key tersebut';
            public       postgres    false    239            �           0    0    COLUMN "AppShortCut"."Color"    COMMENT     �  COMMENT ON COLUMN public."AppShortCut"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';
            public       postgres    false    239            �           0    0 #   COLUMN "AppShortCut"."InnerInfoURL"    COMMENT     p   COMMENT ON COLUMN public."AppShortCut"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';
            public       postgres    false    239            �           0    0 "   COLUMN "AppShortCut"."ColumnWidth"    COMMENT     q   COMMENT ON COLUMN public."AppShortCut"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';
            public       postgres    false    239            �            1259    16571    AppShortKey_idAppShortCut_seq    SEQUENCE     �   CREATE SEQUENCE public."AppShortKey_idAppShortCut_seq"
    START WITH 34
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public."AppShortKey_idAppShortCut_seq";
       public       postgres    false    239            �           0    0    AppShortKey_idAppShortCut_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public."AppShortKey_idAppShortCut_seq" OWNED BY public."AppShortCut"."idAppShortCut";
            public       postgres    false    240            �            1259    16573 
   fileUpload    TABLE     �   CREATE TABLE public."fileUpload" (
    "idFileUpload" integer NOT NULL,
    "Category" character varying(50),
    "fidData" integer,
    "FileExtention" character varying(5),
    "FileName" character varying(100),
    "FileSize" integer
);
     DROP TABLE public."fileUpload";
       public         postgres    false            �            1259    16576    fileUpload_idFileUpload_seq    SEQUENCE     �   CREATE SEQUENCE public."fileUpload_idFileUpload_seq"
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."fileUpload_idFileUpload_seq";
       public       postgres    false    241            �           0    0    fileUpload_idFileUpload_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."fileUpload_idFileUpload_seq" OWNED BY public."fileUpload"."idFileUpload";
            public       postgres    false    242            �            1259    16578    karunia_sessions    TABLE     �   CREATE TABLE public.karunia_sessions (
    id character varying(128) NOT NULL,
    ip_address character varying(45) NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);
 $   DROP TABLE public.karunia_sessions;
       public         postgres    false            �            1259    16586    msOperatorModul    TABLE     �   CREATE TABLE public."msOperatorModul" (
    "idOperatorModul" integer NOT NULL,
    "Caption" character varying(50),
    "SpecialVar" character varying(50),
    "Deskripsi" text
);
 %   DROP TABLE public."msOperatorModul";
       public         postgres    false            �            1259    16592 3   msMasterOperatorSpecial_idMasterOperatorSpecial_seq    SEQUENCE     �   CREATE SEQUENCE public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 L   DROP SEQUENCE public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq";
       public       postgres    false    244            �           0    0 3   msMasterOperatorSpecial_idMasterOperatorSpecial_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq" OWNED BY public."msOperatorModul"."idOperatorModul";
            public       postgres    false    245            �            1259    16594 
   msOperator    TABLE     o  CREATE TABLE public."msOperator" (
    "idMsOperator" integer NOT NULL,
    "LoginName" character varying(32),
    "LoginPass" character varying(32),
    "FullName" character varying(40),
    "KodeAHASS" character varying(6),
    "LastUpdate" timestamp(6) without time zone,
    "ExpiryDate" date,
    "fidMsOperatorGroup" integer DEFAULT 0,
    "fidMsEmployee" integer,
    "KodeDealer" character varying(6),
    "KodeSalesPerson" character varying(10),
    "LoginPassFincoy" character varying(32),
    "fidMenuDefaultOpen" smallint,
    "isAccessGudang" character varying(100),
    "isDefaultGudang" integer DEFAULT 0
);
     DROP TABLE public."msOperator";
       public         postgres    false            �            1259    16599    msOperatorAppShortCut    TABLE     �   CREATE TABLE public."msOperatorAppShortCut" (
    "idMsOperatorAppShortKey" integer NOT NULL,
    "fidMsOperator" integer,
    "fidAppShortCut" smallint,
    "Status" smallint
);
 +   DROP TABLE public."msOperatorAppShortCut";
       public         postgres    false            �            1259    16602 1   msOperatorAppShortKey_idMsOperatorAppShortKey_seq    SEQUENCE     �   CREATE SEQUENCE public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq"
    START WITH 2635
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 J   DROP SEQUENCE public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq";
       public       postgres    false    247            �           0    0 1   msOperatorAppShortKey_idMsOperatorAppShortKey_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq" OWNED BY public."msOperatorAppShortCut"."idMsOperatorAppShortKey";
            public       postgres    false    248            �            1259    16604    msOperatorDashboardPrivilege    TABLE     �   CREATE TABLE public."msOperatorDashboardPrivilege" (
    "idDashboardPrivilege" integer NOT NULL,
    "fidMsOperator" integer DEFAULT 0,
    "fidAppDashboard" smallint DEFAULT 0,
    "Status" smallint DEFAULT 0
);
 2   DROP TABLE public."msOperatorDashboardPrivilege";
       public         postgres    false            �            1259    16610 5   msOperatorDashboardPrivilege_idDashboardPrivilege_seq    SEQUENCE     �   CREATE SEQUENCE public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 N   DROP SEQUENCE public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq";
       public       postgres    false    249            �           0    0 5   msOperatorDashboardPrivilege_idDashboardPrivilege_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq" OWNED BY public."msOperatorDashboardPrivilege"."idDashboardPrivilege";
            public       postgres    false    250            �            1259    16612    msOperatorGroup    TABLE       CREATE TABLE public."msOperatorGroup" (
    "idMsOperatorGroup" integer NOT NULL,
    "ShortName" character varying(12),
    "Name" character varying(20),
    "Description" character varying(100),
    "Icon" character varying(20),
    "DefaultMenu" text,
    "DefaultShortcut" text
);
 %   DROP TABLE public."msOperatorGroup";
       public         postgres    false            �            1259    16618 %   msOperatorGroup_idMsOperatorGroup_seq    SEQUENCE     �   CREATE SEQUENCE public."msOperatorGroup_idMsOperatorGroup_seq"
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public."msOperatorGroup_idMsOperatorGroup_seq";
       public       postgres    false    251            �           0    0 %   msOperatorGroup_idMsOperatorGroup_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public."msOperatorGroup_idMsOperatorGroup_seq" OWNED BY public."msOperatorGroup"."idMsOperatorGroup";
            public       postgres    false    252            �            1259    16620    msOperatorPrivilege    TABLE     �   CREATE TABLE public."msOperatorPrivilege" (
    "idPrivilege" integer NOT NULL,
    "fidMsOperator" integer DEFAULT 0,
    "fidAppMenu" integer,
    "Status" smallint DEFAULT 0
);
 )   DROP TABLE public."msOperatorPrivilege";
       public         postgres    false            �           0    0 %   COLUMN "msOperatorPrivilege"."Status"    COMMENT     Q   COMMENT ON COLUMN public."msOperatorPrivilege"."Status" IS '0 = off
1 = active';
            public       postgres    false    253            �            1259    16625 #   msOperatorPrivilege_idPrivilege_seq    SEQUENCE     �   CREATE SEQUENCE public."msOperatorPrivilege_idPrivilege_seq"
    START WITH 3573
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public."msOperatorPrivilege_idPrivilege_seq";
       public       postgres    false    253            �           0    0 #   msOperatorPrivilege_idPrivilege_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public."msOperatorPrivilege_idPrivilege_seq" OWNED BY public."msOperatorPrivilege"."idPrivilege";
            public       postgres    false    254            �            1259    16627    msOperatorSpecial    TABLE     �   CREATE TABLE public."msOperatorSpecial" (
    "idOperatorSpecial" smallint NOT NULL,
    "fidMsOperator" smallint,
    "SpecialValue" character varying(40),
    "fidOperatorModul" smallint,
    "Status" smallint
);
 '   DROP TABLE public."msOperatorSpecial";
       public         postgres    false                        1259    16630    msOperator_idMsOperator_seq    SEQUENCE     �   CREATE SEQUENCE public."msOperator_idMsOperator_seq"
    START WITH 67
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."msOperator_idMsOperator_seq";
       public       postgres    false    246            �           0    0    msOperator_idMsOperator_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."msOperator_idMsOperator_seq" OWNED BY public."msOperator"."idMsOperator";
            public       postgres    false    256                       1259    16632    trDataLog_idTrDataLog_seq    SEQUENCE     �   CREATE SEQUENCE terminal."trDataLog_idTrDataLog_seq"
    START WITH 90
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE terminal."trDataLog_idTrDataLog_seq";
       terminal       postgres    false    8                       1259    16634 	   trDataLog    TABLE     a  CREATE TABLE terminal."trDataLog" (
    "idTrDataLog" integer DEFAULT nextval('terminal."trDataLog_idTrDataLog_seq"'::regclass) NOT NULL,
    "SchemaName" character(30),
    "TableName" character(50),
    "fidData" integer,
    "fidMsOperator" integer,
    "ActionTime" timestamp(6) without time zone,
    "LogData" text,
    "LogType" character(10)
);
 !   DROP TABLE terminal."trDataLog";
       terminal         postgres    false    257    8            �           0    0    TABLE "trDataLog"    COMMENT     k   COMMENT ON TABLE terminal."trDataLog" IS 'Menyimpan semua perubahan data yang terjadi pada masing2 table';
            terminal       postgres    false    258                       1259    16641 -   trFileUploadManager_idTrFileUploadManager_seq    SEQUENCE     �   CREATE SEQUENCE terminal."trFileUploadManager_idTrFileUploadManager_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 H   DROP SEQUENCE terminal."trFileUploadManager_idTrFileUploadManager_seq";
       terminal       postgres    false    8                       1259    16643    tr_log_id_log_seq    SEQUENCE        CREATE SEQUENCE terminal.tr_log_id_log_seq
    START WITH 5779
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE terminal.tr_log_id_log_seq;
       terminal       postgres    false    8                       1259    16645    tr_log    TABLE     Y  CREATE TABLE terminal.tr_log (
    id_log integer DEFAULT nextval('terminal.tr_log_id_log_seq'::regclass) NOT NULL,
    value_before text,
    action_time timestamp(6) without time zone DEFAULT now(),
    ip_comp character(20),
    fid_operator integer,
    log_type character(30),
    table_name character varying(100),
    fid_data integer
);
    DROP TABLE terminal.tr_log;
       terminal         postgres    false    260    8            q           2604    16653    msCustomer idCustomer    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msCustomer" ALTER COLUMN "idCustomer" SET DEFAULT nextval('"dataMaster"."msCustomer_idCustomer_seq"'::regclass);
 N   ALTER TABLE "dataMaster"."msCustomer" ALTER COLUMN "idCustomer" DROP DEFAULT;
    
   dataMaster       postgres    false    205    204            r           2604    16654    msJenisKas idJenisKas    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msJenisKas" ALTER COLUMN "idJenisKas" SET DEFAULT nextval('"dataMaster"."msJenisKas_idJenisKas_seq"'::regclass);
 N   ALTER TABLE "dataMaster"."msJenisKas" ALTER COLUMN "idJenisKas" DROP DEFAULT;
    
   dataMaster       postgres    false    208    207            s           2604    16655 !   msJenisKendaraan idJenisKendaraan    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msJenisKendaraan" ALTER COLUMN "idJenisKendaraan" SET DEFAULT nextval('"dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"'::regclass);
 Z   ALTER TABLE "dataMaster"."msJenisKendaraan" ALTER COLUMN "idJenisKendaraan" DROP DEFAULT;
    
   dataMaster       postgres    false    210    209            t           2604    16656 !   msKelompokBarang idKelompokBarang    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msKelompokBarang" ALTER COLUMN "idKelompokBarang" SET DEFAULT nextval('"dataMaster"."msKelompokBarang_idKelompokBarang_seq"'::regclass);
 Z   ALTER TABLE "dataMaster"."msKelompokBarang" ALTER COLUMN "idKelompokBarang" DROP DEFAULT;
    
   dataMaster       postgres    false    212    211            u           2604    16657    msKendaraan idKendaraan    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msKendaraan" ALTER COLUMN "idKendaraan" SET DEFAULT nextval('"dataMaster"."msKendaraan_idKendaraan_seq"'::regclass);
 P   ALTER TABLE "dataMaster"."msKendaraan" ALTER COLUMN "idKendaraan" DROP DEFAULT;
    
   dataMaster       postgres    false    214    213            v           2604    16658    msPengemudi idPengemudi    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msPengemudi" ALTER COLUMN "idPengemudi" SET DEFAULT nextval('"dataMaster"."msPengemudi_idPengemudi_seq"'::regclass);
 P   ALTER TABLE "dataMaster"."msPengemudi" ALTER COLUMN "idPengemudi" DROP DEFAULT;
    
   dataMaster       postgres    false    216    215            w           2604    16659    msSatuan idSatuan    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msSatuan" ALTER COLUMN "idSatuan" SET DEFAULT nextval('"dataMaster"."msSatuan_idSatuan_seq"'::regclass);
 J   ALTER TABLE "dataMaster"."msSatuan" ALTER COLUMN "idSatuan" DROP DEFAULT;
    
   dataMaster       postgres    false    218    217            x           2604    16660    msSupplier idSupplier    DEFAULT     �   ALTER TABLE ONLY "dataMaster"."msSupplier" ALTER COLUMN "idSupplier" SET DEFAULT nextval('"dataMaster"."msSupplier_idSupplier_seq"'::regclass);
 N   ALTER TABLE "dataMaster"."msSupplier" ALTER COLUMN "idSupplier" DROP DEFAULT;
    
   dataMaster       postgres    false    221    220            y           2604    16661    trOrderDetail idOrderDetail    DEFAULT     �   ALTER TABLE ONLY ho."trOrderDetail" ALTER COLUMN "idOrderDetail" SET DEFAULT nextval('ho."trOrderDet_idOrderDet_seq"'::regclass);
 J   ALTER TABLE ho."trOrderDetail" ALTER COLUMN "idOrderDetail" DROP DEFAULT;
       ho       postgres    false    224    223            �           2604    16766 	   trPO idPO    DEFAULT     d   ALTER TABLE ONLY ho."trPO" ALTER COLUMN "idPO" SET DEFAULT nextval('ho."trPO_idPO_seq"'::regclass);
 8   ALTER TABLE ho."trPO" ALTER COLUMN "idPO" DROP DEFAULT;
       ho       postgres    false    263    262    263            �           2604    16774    trPODetail idPODetail    DEFAULT     |   ALTER TABLE ONLY ho."trPODetail" ALTER COLUMN "idPODetail" SET DEFAULT nextval('ho."trPODetail_idPODetail_seq"'::regclass);
 D   ALTER TABLE ho."trPODetail" ALTER COLUMN "idPODetail" DROP DEFAULT;
       ho       postgres    false    265    264    265            �           2604    16662    AppDashboard idAppDashboard    DEFAULT     �   ALTER TABLE ONLY public."AppDashboard" ALTER COLUMN "idAppDashboard" SET DEFAULT nextval('public."AppDashboard_idAppDashboard_seq"'::regclass);
 N   ALTER TABLE public."AppDashboard" ALTER COLUMN "idAppDashboard" DROP DEFAULT;
       public       postgres    false    236    235            �           2604    16663    AppShortCut idAppShortCut    DEFAULT     �   ALTER TABLE ONLY public."AppShortCut" ALTER COLUMN "idAppShortCut" SET DEFAULT nextval('public."AppShortKey_idAppShortCut_seq"'::regclass);
 L   ALTER TABLE public."AppShortCut" ALTER COLUMN "idAppShortCut" DROP DEFAULT;
       public       postgres    false    240    239            �           2604    16664    fileUpload idFileUpload    DEFAULT     �   ALTER TABLE ONLY public."fileUpload" ALTER COLUMN "idFileUpload" SET DEFAULT nextval('public."fileUpload_idFileUpload_seq"'::regclass);
 J   ALTER TABLE public."fileUpload" ALTER COLUMN "idFileUpload" DROP DEFAULT;
       public       postgres    false    242    241            �           2604    16665 -   msOperatorAppShortCut idMsOperatorAppShortKey    DEFAULT     �   ALTER TABLE ONLY public."msOperatorAppShortCut" ALTER COLUMN "idMsOperatorAppShortKey" SET DEFAULT nextval('public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq"'::regclass);
 `   ALTER TABLE public."msOperatorAppShortCut" ALTER COLUMN "idMsOperatorAppShortKey" DROP DEFAULT;
       public       postgres    false    248    247            �           2604    16666 1   msOperatorDashboardPrivilege idDashboardPrivilege    DEFAULT     �   ALTER TABLE ONLY public."msOperatorDashboardPrivilege" ALTER COLUMN "idDashboardPrivilege" SET DEFAULT nextval('public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq"'::regclass);
 d   ALTER TABLE public."msOperatorDashboardPrivilege" ALTER COLUMN "idDashboardPrivilege" DROP DEFAULT;
       public       postgres    false    250    249            �           2604    16667 !   msOperatorGroup idMsOperatorGroup    DEFAULT     �   ALTER TABLE ONLY public."msOperatorGroup" ALTER COLUMN "idMsOperatorGroup" SET DEFAULT nextval('public."msOperatorGroup_idMsOperatorGroup_seq"'::regclass);
 T   ALTER TABLE public."msOperatorGroup" ALTER COLUMN "idMsOperatorGroup" DROP DEFAULT;
       public       postgres    false    252    251            �           2604    16668    msOperatorModul idOperatorModul    DEFAULT     �   ALTER TABLE ONLY public."msOperatorModul" ALTER COLUMN "idOperatorModul" SET DEFAULT nextval('public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq"'::regclass);
 R   ALTER TABLE public."msOperatorModul" ALTER COLUMN "idOperatorModul" DROP DEFAULT;
       public       postgres    false    245    244            �           2604    16669    msOperatorPrivilege idPrivilege    DEFAULT     �   ALTER TABLE ONLY public."msOperatorPrivilege" ALTER COLUMN "idPrivilege" SET DEFAULT nextval('public."msOperatorPrivilege_idPrivilege_seq"'::regclass);
 R   ALTER TABLE public."msOperatorPrivilege" ALTER COLUMN "idPrivilege" DROP DEFAULT;
       public       postgres    false    254    253            Y          0    16429    msBarang 
   TABLE DATA               �   COPY "dataMaster"."msBarang" ("KodeBarang", "fidKelompokBarang", "NamaBarang", "fidSatuanKecil", "fidSatuanBesar", "HargaBeli", "fidSupplier", "PhotoBarang", "Deskripsi", "TglInput", "TglUpdate", userinput, userupdate, "Stok", "Merk") FROM stdin;
 
   dataMaster       postgres    false    202   )�      Z          0    16435    msConfig 
   TABLE DATA               X   COPY "dataMaster"."msConfig" ("idConfig", "Caption", "keyText", "keyValue") FROM stdin;
 
   dataMaster       postgres    false    203   �      [          0    16441 
   msCustomer 
   TABLE DATA               �   COPY "dataMaster"."msCustomer" ("idCustomer", "NamaLengkap", "NamaPanggilan", "NoHP", "Alamat", "Email", "Institusi", "Negara", "Provinsi", "KotaKabupaten", "Photo") FROM stdin;
 
   dataMaster       postgres    false    204   S�      ]          0    16449    msGudang 
   TABLE DATA               p   COPY "dataMaster"."msGudang" ("idGudang", "KodeGudang", "NamaGudang", "AlamatGudang", "KotaGudang") FROM stdin;
 
   dataMaster       postgres    false    206   ��      ^          0    16452 
   msJenisKas 
   TABLE DATA               P   COPY "dataMaster"."msJenisKas" ("idJenisKas", "Nama", "Keterangan") FROM stdin;
 
   dataMaster       postgres    false    207   8�      `          0    16460    msJenisKendaraan 
   TABLE DATA               r   COPY "dataMaster"."msJenisKendaraan" ("idJenisKendaraan", "NamaJenisKendaraan", "KodeJenisKendaraan") FROM stdin;
 
   dataMaster       postgres    false    209   y�      b          0    16465    msKelompokBarang 
   TABLE DATA               r   COPY "dataMaster"."msKelompokBarang" ("idKelompokBarang", "KodeKelompokBarang", "NamaKelompokBarang") FROM stdin;
 
   dataMaster       postgres    false    211   ��      d          0    16470    msKendaraan 
   TABLE DATA               h  COPY "dataMaster"."msKendaraan" ("idKendaraan", "PlatNomor", "Merk", "Type", "fidJenisKendaraan", "Model", "TahunPembuatan", "Silinder", "NoRangka", "NoMesin", "Warna", "BahanBakar", "WarnaTNKB", "TahunRegistrasi", "NoBPKB", "KodeLokasi", "NoUrutDaftar", "PhotoKendaraan1", "PhotoKendaraan2", "PhotoKendaraan3", "PanjangKaroseri", "LebarKaroseri", "TinggiKaroseri", "Dimensi", "BeratKosong", "BeratMax", "PanjangMobil", "LebarMobil", "TinggiMobil", "KecepatanMax", "TenagaMax_", "UkuranBan_", "UkuranRoda_", "KapasitasMuatan_", "NamaKendaraan", "TenagaMax", "UkuranBan", "UkuranRoda", "KapasitasMuatan") FROM stdin;
 
   dataMaster       postgres    false    213   Р      f          0    16478    msPengemudi 
   TABLE DATA               %  COPY "dataMaster"."msPengemudi" ("idPengemudi", "NamaLengkap", "NamaPanggilan", "NoHP", "Alamat", "Email", "NIK", "TglLahir", "GolDarah", "PhotoProfile", "PhotoKTP", "PhotoSIM_A", "PhotoSIM_B1", "PhotoSIM_B2", "PunyaSIM_A", "PunyaSIM_B1", "PunyaSIM_B2", "Status", "fidMsReligion") FROM stdin;
 
   dataMaster       postgres    false    215   �      h          0    16486    msSatuan 
   TABLE DATA               D   COPY "dataMaster"."msSatuan" ("idSatuan", "NamaSatuan") FROM stdin;
 
   dataMaster       postgres    false    217   K�      j          0    16491    msStatusOrder 
   TABLE DATA               S   COPY "dataMaster"."msStatusOrder" ("idStatusOrder", "NamaStatusOrder") FROM stdin;
 
   dataMaster       postgres    false    219   x�      �          0    16777 
   msStatusPO 
   TABLE DATA               J   COPY "dataMaster"."msStatusPO" ("idStatusPO", "NamaStatusPO") FROM stdin;
 
   dataMaster       postgres    false    266   ǡ      k          0    16494 
   msSupplier 
   TABLE DATA               �   COPY "dataMaster"."msSupplier" ("idSupplier", "KodeSupplier", "NamaSupplier", "AlamatSupplier", "KotaSupplier", "Fax", "Alamat2Supplier", "Alamat3Supplier", "Telepon", "Telepon2", "Telepon3", "Fax2", "Email") FROM stdin;
 
   dataMaster       postgres    false    220   ��      m          0    16502    trOrder 
   TABLE DATA               .  COPY ho."trOrder" ("NoOrder", "fidKendaraan", "JenisTrip", "DeskripsiBarang", "Berat", "Panjang", "Lebar", "Tinggi", "Total", "TglKirim", "WaktuPengambilan", "LokasiAsal", "LokasiTujuan", "AreaAsal", "AreaTujuan", "DetailLokasiPengambilan", "DetailLokasiTujuan", "TipeKawasanAsal", "TipeKawasanTujuan", "InstruksiKhusus", "Totalharga", "PhotoBarang", "AddPackaging", "AddTenagaAngkut", "TglInput", "UserInput", "TglUpdate", "fidStatusOrder", "fidCustomer", "fidPengemudi", "UserUpdate", "NamaPajak", "PersenPajak", "TotalHargaBayar", "TglOrder") FROM stdin;
    ho       postgres    false    222   ,�      n          0    16508    trOrderDetail 
   TABLE DATA               h   COPY ho."trOrderDetail" ("idOrderDetail", "NoOrder", "NamaBiaya", "Nominal", "fidJenisKas") FROM stdin;
    ho       postgres    false    223   ��      �          0    16763    trPO 
   TABLE DATA                 COPY ho."trPO" ("idPO", "NoPembelian", "TglPembelian", "fidKendaraan", "fidSupplier", "NoFaktur", "TotalBiaya", "TotalDiskon", "UserInput", "TglInput", "UserApproval", "TglApproval", "TotalBiayaBayar", "fidStatusPO", "UserUpdate", "TglUpdate", "TotalPPN") FROM stdin;
    ho       postgres    false    263   �      �          0    16771 
   trPODetail 
   TABLE DATA               �   COPY ho."trPODetail" ("idPODetail", "fidPO", "Qty", "HargaBeli", "Diskon", "PPN", "SubTotal", "KodeBarang", "fidJenisKas") FROM stdin;
    ho       postgres    false    265   ��      q          0    16515 
   msEmployee 
   TABLE DATA               �  COPY "humanCapital"."msEmployee" ("idMsEmployee", "EmpCode", "Name", "DateOfBirth", "CityOfBirth", "IDCardNumber", "Address", "City", "PhoneNumber1", "PhoneNumber2", "Email", "ApprovalStatus", "NickName", "fidGender", "fidMsPosition", "fidMsReligion", "KodeAhass", "KodeDealer", "KodeSalesAHM", "Jabatan", "fidPendidikan", "fidHobi", "Provinsi", "KodePos", "Address_KTP", "Provinsi_KTP", "City_KTP", "KodePos_KTP", "isSales") FROM stdin;
    humanCapital       postgres    false    226   �      r          0    16522    msHobi 
   TABLE DATA               L   COPY "humanCapital"."msHobi" ("Code", "Description", "OrderBy") FROM stdin;
    humanCapital       postgres    false    227   ˤ      t          0    16528    msOrganizationStructure 
   TABLE DATA               {   COPY "humanCapital"."msOrganizationStructure" ("idMsOrganizationStructure", "fidMsOrg", "Description", "Code") FROM stdin;
    humanCapital       postgres    false    229   g�      u          0    16532    msPendidikan 
   TABLE DATA               R   COPY "humanCapital"."msPendidikan" ("Code", "Description", "OrderBy") FROM stdin;
    humanCapital       postgres    false    230   ��      w          0    16538 
   msPosition 
   TABLE DATA               ]   COPY "humanCapital"."msPosition" ("idMsPosition", "PositionCode", "Description") FROM stdin;
    humanCapital       postgres    false    232   ��      y          0    16544 
   msReligion 
   TABLE DATA               M   COPY "humanCapital"."msReligion" ("idMsReligion", "Description") FROM stdin;
    humanCapital       postgres    false    234   �      z          0    16548    AppDashboard 
   TABLE DATA               �   COPY public."AppDashboard" ("idAppDashboard", "Title", "OnClick", "Icon", "fidAppMenu", "fidAppShorcut", "OrderBy", "GroupName", "Color", "InnerInfoURL", "ColumnWidth", "Description", "IdResult") FROM stdin;
    public       postgres    false    235   u�      |          0    16559    AppMenus 
   TABLE DATA               �   COPY public."AppMenus" ("idAppMenu", "Title", "URL", "IconImg", "ColorCode", "fidAppMenu", "OrderBy", "Description", "GroupMenu", "Project") FROM stdin;
    public       postgres    false    237   �      ~          0    16567    AppShortCut 
   TABLE DATA               �   COPY public."AppShortCut" ("idAppShortCut", "Title", "OnClick", "Icon", "fidAppMenu", "OrderBy", "GroupName", "Color", "InnerInfoURL", "ColumnWidth", "Description") FROM stdin;
    public       postgres    false    239   ��      �          0    16573 
   fileUpload 
   TABLE DATA               v   COPY public."fileUpload" ("idFileUpload", "Category", "fidData", "FileExtention", "FileName", "FileSize") FROM stdin;
    public       postgres    false    241   ��      �          0    16578    karunia_sessions 
   TABLE DATA               M   COPY public.karunia_sessions (id, ip_address, "timestamp", data) FROM stdin;
    public       postgres    false    243   ��      �          0    16594 
   msOperator 
   TABLE DATA                 COPY public."msOperator" ("idMsOperator", "LoginName", "LoginPass", "FullName", "KodeAHASS", "LastUpdate", "ExpiryDate", "fidMsOperatorGroup", "fidMsEmployee", "KodeDealer", "KodeSalesPerson", "LoginPassFincoy", "fidMenuDefaultOpen", "isAccessGudang", "isDefaultGudang") FROM stdin;
    public       postgres    false    246   ��      �          0    16599    msOperatorAppShortCut 
   TABLE DATA               y   COPY public."msOperatorAppShortCut" ("idMsOperatorAppShortKey", "fidMsOperator", "fidAppShortCut", "Status") FROM stdin;
    public       postgres    false    247   �      �          0    16604    msOperatorDashboardPrivilege 
   TABLE DATA               ~   COPY public."msOperatorDashboardPrivilege" ("idDashboardPrivilege", "fidMsOperator", "fidAppDashboard", "Status") FROM stdin;
    public       postgres    false    249   (�      �          0    16612    msOperatorGroup 
   TABLE DATA               �   COPY public."msOperatorGroup" ("idMsOperatorGroup", "ShortName", "Name", "Description", "Icon", "DefaultMenu", "DefaultShortcut") FROM stdin;
    public       postgres    false    251   O�      �          0    16586    msOperatorModul 
   TABLE DATA               d   COPY public."msOperatorModul" ("idOperatorModul", "Caption", "SpecialVar", "Deskripsi") FROM stdin;
    public       postgres    false    244   ��      �          0    16620    msOperatorPrivilege 
   TABLE DATA               g   COPY public."msOperatorPrivilege" ("idPrivilege", "fidMsOperator", "fidAppMenu", "Status") FROM stdin;
    public       postgres    false    253   �      �          0    16627    msOperatorSpecial 
   TABLE DATA               �   COPY public."msOperatorSpecial" ("idOperatorSpecial", "fidMsOperator", "SpecialValue", "fidOperatorModul", "Status") FROM stdin;
    public       postgres    false    255   ��      �          0    16634 	   trDataLog 
   TABLE DATA               �   COPY terminal."trDataLog" ("idTrDataLog", "SchemaName", "TableName", "fidData", "fidMsOperator", "ActionTime", "LogData", "LogType") FROM stdin;
    terminal       postgres    false    258   ��      �          0    16645    tr_log 
   TABLE DATA               |   COPY terminal.tr_log (id_log, value_before, action_time, ip_comp, fid_operator, log_type, table_name, fid_data) FROM stdin;
    terminal       postgres    false    261   ��      �           0    0    msCustomer_idMsCustomer_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('"customerRelationship"."msCustomer_idMsCustomer_seq"', 104, true);
            customerRelationship       postgres    false    201            �           0    0    msCustomer_idCustomer_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('"dataMaster"."msCustomer_idCustomer_seq"', 10, true);
         
   dataMaster       postgres    false    205            �           0    0    msJenisKas_idJenisKas_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('"dataMaster"."msJenisKas_idJenisKas_seq"', 6, true);
         
   dataMaster       postgres    false    208            �           0    0 %   msJenisKendaraan_idJenisKendaraan_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('"dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"', 4, true);
         
   dataMaster       postgres    false    210            �           0    0 %   msKelompokBarang_idKelompokBarang_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('"dataMaster"."msKelompokBarang_idKelompokBarang_seq"', 7, true);
         
   dataMaster       postgres    false    212            �           0    0    msKendaraan_idKendaraan_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('"dataMaster"."msKendaraan_idKendaraan_seq"', 6, true);
         
   dataMaster       postgres    false    214            �           0    0    msPengemudi_idPengemudi_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('"dataMaster"."msPengemudi_idPengemudi_seq"', 7, true);
         
   dataMaster       postgres    false    216            �           0    0    msSatuan_idSatuan_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('"dataMaster"."msSatuan_idSatuan_seq"', 6, true);
         
   dataMaster       postgres    false    218            �           0    0    msSupplier_idSupplier_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('"dataMaster"."msSupplier_idSupplier_seq"', 14, true);
         
   dataMaster       postgres    false    221            �           0    0    trOrderDet_idOrderDet_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('ho."trOrderDet_idOrderDet_seq"', 25, true);
            ho       postgres    false    224            �           0    0    trPODetail_idPODetail_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('ho."trPODetail_idPODetail_seq"', 30, true);
            ho       postgres    false    264            �           0    0    trPO_idPO_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('ho."trPO_idPO_seq"', 9, true);
            ho       postgres    false    262            �           0    0    msEmployee_idMsEmployee_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('"humanCapital"."msEmployee_idMsEmployee_seq"', 97, true);
            humanCapital       postgres    false    225            �           0    0 5   msOrganizationStructure_idMsOrganizationStructure_seq    SEQUENCE SET     n   SELECT pg_catalog.setval('"humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq"', 2, false);
            humanCapital       postgres    false    228            �           0    0    msPosition_idMsPosition_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('"humanCapital"."msPosition_idMsPosition_seq"', 7, false);
            humanCapital       postgres    false    231            �           0    0    msReligion_idReligion_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('"humanCapital"."msReligion_idReligion_seq"', 7, false);
            humanCapital       postgres    false    233            �           0    0    AppDashboard_idAppDashboard_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public."AppDashboard_idAppDashboard_seq"', 1, false);
            public       postgres    false    236            �           0    0    AppMenus_id_app_menu_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public."AppMenus_id_app_menu_seq"', 117, true);
            public       postgres    false    238            �           0    0    AppShortKey_idAppShortCut_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public."AppShortKey_idAppShortCut_seq"', 34, true);
            public       postgres    false    240            �           0    0    fileUpload_idFileUpload_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."fileUpload_idFileUpload_seq"', 42, true);
            public       postgres    false    242            �           0    0 3   msMasterOperatorSpecial_idMasterOperatorSpecial_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq"', 1, true);
            public       postgres    false    245            �           0    0 1   msOperatorAppShortKey_idMsOperatorAppShortKey_seq    SEQUENCE SET     d   SELECT pg_catalog.setval('public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq"', 2763, true);
            public       postgres    false    248            �           0    0 5   msOperatorDashboardPrivilege_idDashboardPrivilege_seq    SEQUENCE SET     f   SELECT pg_catalog.setval('public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq"', 1, false);
            public       postgres    false    250            �           0    0 %   msOperatorGroup_idMsOperatorGroup_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."msOperatorGroup_idMsOperatorGroup_seq"', 6, false);
            public       postgres    false    252            �           0    0 #   msOperatorPrivilege_idPrivilege_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."msOperatorPrivilege_idPrivilege_seq"', 5761, true);
            public       postgres    false    254            �           0    0    msOperator_idMsOperator_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."msOperator_idMsOperator_seq"', 67, true);
            public       postgres    false    256            �           0    0    trDataLog_idTrDataLog_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('terminal."trDataLog_idTrDataLog_seq"', 90, true);
            terminal       postgres    false    257                        0    0 -   trFileUploadManager_idTrFileUploadManager_seq    SEQUENCE SET     `   SELECT pg_catalog.setval('terminal."trFileUploadManager_idTrFileUploadManager_seq"', 1, false);
            terminal       postgres    false    259                       0    0    tr_log_id_log_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('terminal.tr_log_id_log_seq', 9386, true);
            terminal       postgres    false    260            �           2606    16671    msBarang msBarang_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY "dataMaster"."msBarang"
    ADD CONSTRAINT "msBarang_pkey" PRIMARY KEY ("KodeBarang");
 J   ALTER TABLE ONLY "dataMaster"."msBarang" DROP CONSTRAINT "msBarang_pkey";
    
   dataMaster         postgres    false    202            �           2606    16673    msConfig msConfig_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY "dataMaster"."msConfig"
    ADD CONSTRAINT "msConfig_pkey" PRIMARY KEY ("idConfig");
 J   ALTER TABLE ONLY "dataMaster"."msConfig" DROP CONSTRAINT "msConfig_pkey";
    
   dataMaster         postgres    false    203            �           2606    16675    msCustomer msCustomer_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "dataMaster"."msCustomer"
    ADD CONSTRAINT "msCustomer_pkey" PRIMARY KEY ("idCustomer");
 N   ALTER TABLE ONLY "dataMaster"."msCustomer" DROP CONSTRAINT "msCustomer_pkey";
    
   dataMaster         postgres    false    204            �           2606    16677    msGudang msGudang_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY "dataMaster"."msGudang"
    ADD CONSTRAINT "msGudang_pkey" PRIMARY KEY ("idGudang");
 J   ALTER TABLE ONLY "dataMaster"."msGudang" DROP CONSTRAINT "msGudang_pkey";
    
   dataMaster         postgres    false    206            �           2606    16679    msJenisKas msJenisKas_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "dataMaster"."msJenisKas"
    ADD CONSTRAINT "msJenisKas_pkey" PRIMARY KEY ("idJenisKas");
 N   ALTER TABLE ONLY "dataMaster"."msJenisKas" DROP CONSTRAINT "msJenisKas_pkey";
    
   dataMaster         postgres    false    207            �           2606    16681 &   msJenisKendaraan msJenisKendaraan_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY "dataMaster"."msJenisKendaraan"
    ADD CONSTRAINT "msJenisKendaraan_pkey" PRIMARY KEY ("idJenisKendaraan");
 Z   ALTER TABLE ONLY "dataMaster"."msJenisKendaraan" DROP CONSTRAINT "msJenisKendaraan_pkey";
    
   dataMaster         postgres    false    209            �           2606    16683 &   msKelompokBarang msKelompokBarang_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY "dataMaster"."msKelompokBarang"
    ADD CONSTRAINT "msKelompokBarang_pkey" PRIMARY KEY ("idKelompokBarang");
 Z   ALTER TABLE ONLY "dataMaster"."msKelompokBarang" DROP CONSTRAINT "msKelompokBarang_pkey";
    
   dataMaster         postgres    false    211            �           2606    16685    msKendaraan msKendaraan_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY "dataMaster"."msKendaraan"
    ADD CONSTRAINT "msKendaraan_pkey" PRIMARY KEY ("idKendaraan");
 P   ALTER TABLE ONLY "dataMaster"."msKendaraan" DROP CONSTRAINT "msKendaraan_pkey";
    
   dataMaster         postgres    false    213            �           2606    16687    msPengemudi msPengemudi_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY "dataMaster"."msPengemudi"
    ADD CONSTRAINT "msPengemudi_pkey" PRIMARY KEY ("idPengemudi");
 P   ALTER TABLE ONLY "dataMaster"."msPengemudi" DROP CONSTRAINT "msPengemudi_pkey";
    
   dataMaster         postgres    false    215            �           2606    16689    msSatuan msSatuan_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY "dataMaster"."msSatuan"
    ADD CONSTRAINT "msSatuan_pkey" PRIMARY KEY ("idSatuan");
 J   ALTER TABLE ONLY "dataMaster"."msSatuan" DROP CONSTRAINT "msSatuan_pkey";
    
   dataMaster         postgres    false    217            �           2606    16783 #   msStatusPO msStatusOrder_copy1_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY "dataMaster"."msStatusPO"
    ADD CONSTRAINT "msStatusOrder_copy1_pkey" PRIMARY KEY ("idStatusPO");
 W   ALTER TABLE ONLY "dataMaster"."msStatusPO" DROP CONSTRAINT "msStatusOrder_copy1_pkey";
    
   dataMaster         postgres    false    266            �           2606    16691     msStatusOrder msStatusOrder_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY "dataMaster"."msStatusOrder"
    ADD CONSTRAINT "msStatusOrder_pkey" PRIMARY KEY ("idStatusOrder");
 T   ALTER TABLE ONLY "dataMaster"."msStatusOrder" DROP CONSTRAINT "msStatusOrder_pkey";
    
   dataMaster         postgres    false    219            �           2606    16693    msSupplier msSupplier_pkey1 
   CONSTRAINT     m   ALTER TABLE ONLY "dataMaster"."msSupplier"
    ADD CONSTRAINT "msSupplier_pkey1" PRIMARY KEY ("idSupplier");
 O   ALTER TABLE ONLY "dataMaster"."msSupplier" DROP CONSTRAINT "msSupplier_pkey1";
    
   dataMaster         postgres    false    220            �           2606    16695    trOrderDetail trOrderDet_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY ho."trOrderDetail"
    ADD CONSTRAINT "trOrderDet_pkey" PRIMARY KEY ("idOrderDetail");
 G   ALTER TABLE ONLY ho."trOrderDetail" DROP CONSTRAINT "trOrderDet_pkey";
       ho         postgres    false    223            �           2606    16697    trOrder trOrder_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY ho."trOrder"
    ADD CONSTRAINT "trOrder_pkey" PRIMARY KEY ("NoOrder");
 >   ALTER TABLE ONLY ho."trOrder" DROP CONSTRAINT "trOrder_pkey";
       ho         postgres    false    222            �           2606    16776    trPODetail trPODetail_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY ho."trPODetail"
    ADD CONSTRAINT "trPODetail_pkey" PRIMARY KEY ("idPODetail");
 D   ALTER TABLE ONLY ho."trPODetail" DROP CONSTRAINT "trPODetail_pkey";
       ho         postgres    false    265            �           2606    16768    trPO trPO_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY ho."trPO"
    ADD CONSTRAINT "trPO_pkey" PRIMARY KEY ("idPO");
 8   ALTER TABLE ONLY ho."trPO" DROP CONSTRAINT "trPO_pkey";
       ho         postgres    false    263            �           2606    16699    msHobi msHobi_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY "humanCapital"."msHobi"
    ADD CONSTRAINT "msHobi_pkey" PRIMARY KEY ("Code");
 H   ALTER TABLE ONLY "humanCapital"."msHobi" DROP CONSTRAINT "msHobi_pkey";
       humanCapital         postgres    false    227            �           2606    16701    msPendidikan msPendidikan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "humanCapital"."msPendidikan"
    ADD CONSTRAINT "msPendidikan_pkey" PRIMARY KEY ("Code");
 T   ALTER TABLE ONLY "humanCapital"."msPendidikan" DROP CONSTRAINT "msPendidikan_pkey";
       humanCapital         postgres    false    230            �           2606    16703    AppDashboard AppDashboard_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public."AppDashboard"
    ADD CONSTRAINT "AppDashboard_pkey" PRIMARY KEY ("idAppDashboard");
 L   ALTER TABLE ONLY public."AppDashboard" DROP CONSTRAINT "AppDashboard_pkey";
       public         postgres    false    235            �           2606    16705    AppMenus AppMenus_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public."AppMenus"
    ADD CONSTRAINT "AppMenus_pkey" PRIMARY KEY ("idAppMenu");
 D   ALTER TABLE ONLY public."AppMenus" DROP CONSTRAINT "AppMenus_pkey";
       public         postgres    false    237            �           2606    16707    AppShortCut AppShortCut_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public."AppShortCut"
    ADD CONSTRAINT "AppShortCut_pkey" PRIMARY KEY ("idAppShortCut");
 J   ALTER TABLE ONLY public."AppShortCut" DROP CONSTRAINT "AppShortCut_pkey";
       public         postgres    false    239            �           2606    16709    fileUpload fileUpload_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public."fileUpload"
    ADD CONSTRAINT "fileUpload_pkey" PRIMARY KEY ("idFileUpload");
 H   ALTER TABLE ONLY public."fileUpload" DROP CONSTRAINT "fileUpload_pkey";
       public         postgres    false    241            �           2606    16711 ,   msOperatorModul msMasterOperatorSpecial_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public."msOperatorModul"
    ADD CONSTRAINT "msMasterOperatorSpecial_pkey" PRIMARY KEY ("idOperatorModul");
 Z   ALTER TABLE ONLY public."msOperatorModul" DROP CONSTRAINT "msMasterOperatorSpecial_pkey";
       public         postgres    false    244            �           2606    16713 0   msOperatorAppShortCut msOperatorAppShortCut_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."msOperatorAppShortCut"
    ADD CONSTRAINT "msOperatorAppShortCut_pkey" PRIMARY KEY ("idMsOperatorAppShortKey");
 ^   ALTER TABLE ONLY public."msOperatorAppShortCut" DROP CONSTRAINT "msOperatorAppShortCut_pkey";
       public         postgres    false    247            �           2606    16715 >   msOperatorDashboardPrivilege msOperatorDashboardPrivilege_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public."msOperatorDashboardPrivilege"
    ADD CONSTRAINT "msOperatorDashboardPrivilege_pkey" PRIMARY KEY ("idDashboardPrivilege");
 l   ALTER TABLE ONLY public."msOperatorDashboardPrivilege" DROP CONSTRAINT "msOperatorDashboardPrivilege_pkey";
       public         postgres    false    249            �           2606    16717 ,   msOperatorPrivilege msOperatorPrivilege_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public."msOperatorPrivilege"
    ADD CONSTRAINT "msOperatorPrivilege_pkey" PRIMARY KEY ("idPrivilege");
 Z   ALTER TABLE ONLY public."msOperatorPrivilege" DROP CONSTRAINT "msOperatorPrivilege_pkey";
       public         postgres    false    253            �           2606    16719 )   msOperatorSpecial msOperatorSepecial_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public."msOperatorSpecial"
    ADD CONSTRAINT "msOperatorSepecial_pkey" PRIMARY KEY ("idOperatorSpecial");
 W   ALTER TABLE ONLY public."msOperatorSpecial" DROP CONSTRAINT "msOperatorSepecial_pkey";
       public         postgres    false    255            �           2606    16721    msOperator msOperator_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public."msOperator"
    ADD CONSTRAINT "msOperator_pkey" PRIMARY KEY ("idMsOperator");
 H   ALTER TABLE ONLY public."msOperator" DROP CONSTRAINT "msOperator_pkey";
       public         postgres    false    246            �           2606    16723    trDataLog trDataLog_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY terminal."trDataLog"
    ADD CONSTRAINT "trDataLog_pkey" PRIMARY KEY ("idTrDataLog");
 H   ALTER TABLE ONLY terminal."trDataLog" DROP CONSTRAINT "trDataLog_pkey";
       terminal         postgres    false    258            �           1259    16724    karunia_sessions_timestamp    INDEX     ^   CREATE INDEX karunia_sessions_timestamp ON public.karunia_sessions USING btree ("timestamp");
 .   DROP INDEX public.karunia_sessions_timestamp;
       public         postgres    false    243            �           2606    16725    msKendaraan fk_jnskendaraan    FK CONSTRAINT     �   ALTER TABLE ONLY "dataMaster"."msKendaraan"
    ADD CONSTRAINT fk_jnskendaraan FOREIGN KEY ("fidJenisKendaraan") REFERENCES "dataMaster"."msJenisKendaraan"("idJenisKendaraan") ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY "dataMaster"."msKendaraan" DROP CONSTRAINT fk_jnskendaraan;
    
   dataMaster       postgres    false    213    209    2985            �           2606    16730    msBarang fk_satuanbsr    FK CONSTRAINT     �   ALTER TABLE ONLY "dataMaster"."msBarang"
    ADD CONSTRAINT fk_satuanbsr FOREIGN KEY ("fidSatuanBesar") REFERENCES "dataMaster"."msSatuan"("idSatuan") ON UPDATE CASCADE ON DELETE CASCADE;
 G   ALTER TABLE ONLY "dataMaster"."msBarang" DROP CONSTRAINT fk_satuanbsr;
    
   dataMaster       postgres    false    202    2993    217            �           2606    16735    msBarang fk_satuankcl    FK CONSTRAINT     �   ALTER TABLE ONLY "dataMaster"."msBarang"
    ADD CONSTRAINT fk_satuankcl FOREIGN KEY ("fidSatuanKecil") REFERENCES "dataMaster"."msSatuan"("idSatuan") ON UPDATE CASCADE ON DELETE CASCADE;
 G   ALTER TABLE ONLY "dataMaster"."msBarang" DROP CONSTRAINT fk_satuankcl;
    
   dataMaster       postgres    false    2993    217    202            �           2606    16740    msBarang fk_supplier    FK CONSTRAINT     �   ALTER TABLE ONLY "dataMaster"."msBarang"
    ADD CONSTRAINT fk_supplier FOREIGN KEY ("fidSupplier") REFERENCES "dataMaster"."msSupplier"("idSupplier") ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY "dataMaster"."msBarang" DROP CONSTRAINT fk_supplier;
    
   dataMaster       postgres    false    220    2997    202            Y   �   x���K�@�ϻ�E�ه⡋�� H��%P�,Y� ���CL�
�0;��o�� ��|0����+ێ�#�+@P�l[�p��]�4W��L-�?;<ab� D� 9�^����zS� ���~E��Ԙ@?[����ٴtx&����;Y��US�:����ŀ�6-,98|�܊?j©��L[��ʙ��<�0�oY:7|��n�a�J�>��A�u�      Z   7  x�mP�N�0<�~�? ���!��p���8T��"7���m��O�7�2�"����gֳS��txVc����ӁQ�3�����p���d&��zF�A���T�[7�X��ɧ,焴��?�q�Z���#��A91b{ֺ�5N�7�|�Ğ�Q���8����ZT��;��G���}~0�i��f
���B�[x:f��x>�V�Hf$�v��,��b��@��z�m�����R%�_�E%&�Ȑ�d�B~��x���q�2o���q�qXa�أ�FA��?~�8�_�)�j�:��_`�/"~��      [   *   x�34�,I-.A"��CjEbnAN�^r~.T��3Ə+F��� �?q      ]   �   x�}�1�0�99EN���?�.���3T����@j�z���l�B�U(��.����7qZ�'3tʁ}�c\`���21���B}�f8sm[�*���ȅ	�{�L��f��C��J��T���JI�ֹ�#{bȇz�ƫ�Z�L�B      ^   1   x�3��N,V�M,.��H�щy\F`Q�Ԝ��"�p^:������� �h>      `      x�3�,)*�\1z\\\ .��      b   "   x�3�,I-.\f`�Br��ic����� =��      d   (   x�3�N8(I-.R1~Pd��%��*����� ��9      f   3   x�3�,I-.��FƆ�H|0ad`h�k`�kd���㇂����b���� )��      h      x�3��NM���2�LJ-N,����� @�O      j   ?   x�34��/JI-RpJ�)�UH�,N-)�*��2�I����B%�9��\�0��Ԝ���L�=... p9      �   !   x�34�t)JL+�25�N�I-N������ VpL      k   $   x�34�,I-.A'��@�eh�a!Q9 �1z\\\ ���      m   }   x�}NA
�@<��e�I�j���C�'/�"Bk��c\��J�!$3��I	�&(�F�ǼL�i�&h�� azQo2^o�C?}vʆ��Ѳе�-γ�rA?���Ԉ9�:�+,̫������ι�/      n   :   x�32���7000���720��LJ,-�4
pr��Kg�p��䍸b���� ��      �   �   x��ϱ� ��x�����G�b;9u1��A��Q�46�Մ@�?��i� �_&/�Dy�0t<�@h��7&�f�$j���|����A��6�ju.Ҝy�y�|4^1�6ц/5���~,u�?������W*rk�D�9���8�\�����w�} 0�Zo      �   S   x�32�4�4�4450�30�4�`���9��o```��od`h�i�ed�b�M�1P�c�q [���zL���`h����� ��      q   �   x�5�1N1E뿧�֚�ĊݱTY"���4#l%�ٻ�ǀ(����c:�Y>��DĘ�c�K[�����w#��Ϛ㖯�;	��r�����fsپR5�T�����b���4]��Oy��R�U��vK�~������9�=��ʲ����̩�����01�����0߶�8�      r   �  x�U��n�0D�˯�1�&)���R�����@/��I�!Q	��*���<Crw�j����_���`����w���$�F.�[�$b��$ә�{�M�I*Hٳ?8ߐ�0���%�C�$���$��Sf�ɍ���*��s'8+��,����r�pk�[�=�Y9!*�5�b6*�F���5^"�E:��ol2��dr~q���ԇR��.2��P�~�;���g't�]V~����ZR U	Կ��ER U��O���HT%P���́���08��*�!"~tGR��T��������B/h��$p���)�ωW�G+�M�,5J?H�Fk��j�y.u�T�Dh-!�I�g���L X^����9#��&��|t�5�q�>$ 
��7��M�iCvu㺎#>�5��Tr�; x�9�v6��,���s��t�f#N�c�aX:�F���io��2�+(CO���;�k�%�ؓ}����4Pc��h϶�4�`��c4�b��EoǑ4�,�6tG�(����;/D��_�if%EY�c�M%*y�-�v$��7/bH����L!*u�v���@F�J_��i�rTD�!�������Ab��U'�1�0��敫�uƿ������G�] ���+U��؆3����/߯'��	!��4�      t      x������ � �      u   d   x�5Ȼ�0D�x�
W��?�o��6�d�_ltun��6W�T�yt��g��4�
/������)4>��'a���Sf�(�U�f�3F��>$o���      w      x������ � �      y   P   x�3��.�,.I�S(���%�y\F�މ%�9��\Ɯ�y)�\&�N�))�\����9��\f���y�
�
Υ\1z\\\ u=�      z   d   x�3�.�O�V�/�,������44��4�4�1�89�A�0i�����̼�".#N_ ;#�R�/$������Z��Z�_T7'�fP.D=X=... Y�'e      |   �  x�}��n�0���S�mIJO�ۮ֝��rReJ��D4��g!��*U;���1f�=Vg������	UZc����Y�B�� `A <���i��<��E����
"Ԅ����m�+,(��E�EF����:�{��F�<%��C�ba���
E�j?Cm$~�đ�������]��v��m���eL7���gҮ�Ɗ�`/rY(����ʆ��m���]��N�4�.����D����S�)�pAn�8w�q��r�UL]��yUɧ�@�][c��n�jL�ËO�$�u��Y�U֗�n���^���My��2�?6���+�Q�<%%ݖ'>a�C�����M������d�}�t�"����.Z�����ƖF���$�H�9V�qɕ!?�����A      ~      x������ � �      �   �  x�e����1���]8�'NʫN�fEM����_�����Ϟdf������ǯ�c�?�|�^?��6���sL;3��fڰ3抹R��c�n\�ȳbV�Ӹ=f(�yѵg�j����p�8q�\]ܹ����t}P�Me,��]���f�E���5T�V�7�l���>�e�m�Kɕ��l���u��F��H�,^b��*L�3ȕج���8�P�[�'�9��5�w5����w"L����8�/r�~^�p{����/�ʋ���?��`t&�[|�h�ݿ��}5n�֏�(=p4����u%�����{������]�E%ߍ��<�%h��=�y��{�8~��%x��,Yh������gyR��䆖'�i�WrC˗����g%7��8�vT�䁖�_%4�9	[0�(yD��h��?W��<l��������}�� }B����?�X      �      x���Ys�Ȗ ���3�����~�L�)�`�b^���bï�� Wu�h�ot5��P%�T*3�s�T*�Qj�TU�<0ͤLD�e�yfP�q�������k1��_^�����/:z��񎬛�ˣoO֚�������;4;�ٜ��fb��Ǝ�/3ef�nÂ��o����P�������r'4l?5��p,���yǚP1�v<Et�\����;bǻqܸ��2[��mM����Sk�o�<�yZ��y���������< �N�9�ߵ̂��T����Lb�f�}s�:��.��y��n��s���D�Ɔ/d�Ӱ�����)�M�x�y�g�G>��,���}4���|�@:Y�g��l���1����y���C�{/������xnѧ�c	���j��x���Wxv.�l4����x�i��˟][�'Y��8{��Q<�����#�<��>����~Go� N��6���8Z��z��Iׯ��=V����ߙ2Mv�:~��Ws"��T��:>�뙈h�z�����tlyl����w��1��n�^���zk�m�����s��c�'��B�F�c=�}�nl��]��/O�q4��ߝ���e�����9}��h����ۭ��=M���e]�c�&�n��`z�K�_�Vt����N_g˨��V�,�����r_�4�w/]{��o���%��t??��ĩ����0mfK�;���������c�����"s7�샟��<�c�����s+}�N��*��m�����������?���'�$��e�B���2�.�2�/�����X���inop�{���V�Q�d�3��~��j��1 zo��}#����z��޵�䵙>�wcD׷��e���:��ߚ�����~�����C�=��s9���e�Y��~�[9�&~k&�2��uֵ6��v�O۽�w|<w���|��7]��!���ˣ���c����D����vǹx��Ad�Xڲ��/�s�8W7�̍�^�Ɣ�X}�<E��N�Ԏ����umw�kC��y�e�k-#c#z}f�]_%���ȾHإ���͘��e?��1���ǻ�Cun��^��c�9~׊t����cti+_�-�����k��<yhz��}<������$nz��q��ƌUw&qdlj߼�>���q��6�.�\�O}�S��#w�����g���s�ޏa"ߏ/�c��m �c�������{�����I��s�G۰�������>�~��Ǳ��}\��էNuu>��<�_��P,��¦kӰr�I1�x����]"L���0���@��`�&�p{��fx��}}��z�%�!�����X��c�� =@�_�Af��l�1�:g�&������k~^���b �36_F�J�����@?�z���2蝜���R���j4g�c�ĵ_�2z�hC���w-�CF�CF�� ��Bς�4,�"YZ�XQ
�8J}�U�#���c��{�p���z����)�{��$y`�Uny^�D��N�(~PL�M�㌳���>Z�+�.��0���ۅ>���q���0M�w�H1�"3)�aL��S4�W}9{�z���z��'C�C#�c"a�Vs��	j�$�Ö��|�^ "4U=����G��ۃ��z���.��)ڒ��nh��&���r���{������'�� ��B��z���2�è^�-S���s��MRWvP�4�2z�������+��ȄS�r̊Télڴ^(�fZG�B��`��a�|�@�C=@���=z���z�{��ڹ߆nSz-�D���w�VQ.=��3�1�Gr`����2z�����3Dc�;E����v�e��ԥٰ�v��c�"��� I׾^�_ԝĸ��A��N6|�iz��j�� z����Ƌ۶4n6\%��[��X��Z�Tl�:_�WU��A����p�@=@{Я z���z%�)���35�6�����JEد�B�ݣ�B�|�8Ȏ�=�]a��D��䡫�n�Ow��G3}d���ڶ��0?Wֱ!#���O��������n�ϝ&ꂍ�YM΃O����S�[>D��x�]zL=��p���nG�Ȏ$�\O��S������+�;���Zv���w{Yw����8��x�r�F���ҹ�̚�{g�=H����>����W�_�����?5�σZ���u6���:���7|!���8��P׿	��d'��/�z.{l�ݾv��Pw����tcn��pK$(�G��%�;_l䙉��`�u���8>���2�s��DsJ�NtA}ݵW�ܷ���u�u���]������r�;������2����{����ܔ����S��?����ĥ����A��`$�}!�pV"Y$�ޞx�YB���>�gwlϫ����1Z��z��?�Kd��H��p��{��/��Ǡx��3y�5�_t�Z׷��~i����O���8~|~�o�=�����kt*����y�ǎ;����N��<3	�?�|��C�>�?�.�o7=�'ϝol^/O�w﷫����K Y�.�i��j��jEe������ǔe��%.�3�G��⮟~�� �p���~2��`��I�~�_?8x��I�~���K��cֱ�ˀ�7'�K�=,����z�͡���.�K9���pnw�Ƀ<�3��g���̓��ʠS��~�{
d��K������`�;�I�K9�o��ߡ��396�����u<P}?����ɧ}�|�1��OA�m3�h�k�u�p
(�g�}����NZ�p��OɆ����_����4��I+D�ư3#�l��r%O��:L���G���C��>L΃2|��,ï��׼�h8
�4��:&v����y�����p>d���C�>d���C��s3|7����6��9bQh��!��6ｯ�x��=|����3|��>d���_Y��~�E���^Y�5i����Ui�/���1<l-|=�"}4�����~�� =@]�j��uN[�\k܀��S��RFV~Y"�
ʇ.�;��
��ozX"���+����4�k��X�s��y�;qT(&�i���={�q4�5�˩��^c�� �09���+���J�%�M��2QB�q�XU�!���'8�CW΋����=@��Â: =@�_��g��mĵҶ�%n�g^�,+��f٬7�0��MƓ=�/{���=�!��z��ʠWB���c�m-jiF��Z�f�Ҡ����\�F_�y{�t�� =@��� �3O)�T:_�2(}��Ilen������)B�з���@e�d<���z����г,��J���jqcd�"j˦��D�]椷R��d��껕�<y�z���z������gqZZM�S�L�6+b	?Ҵ6���#E���v�W=� =@���d<����6E�S�0�ٸ"'��9�
��~rY0GE\�2^h��K&��7=,��� ��A��"�ÜZ�"�1���e!#��]��X8�~�����7=\��z��ʠ���03���X	EY��صT�ŧ�xL���٫>�8@�� =@��?}��D�hbU���4�6�QY{-�+�Q�^F�<�Fc���� =@��?�HN���Q�~Ъ�'��Xo��{��%BU���Vvt�� =@�� =@���GZ����b^��_��즡y�h�6���d<�� �s�a2@��W�k�fM�P�U��9G��ɪJsE�qE��d<A.�3��2�s ��2z������**/-7�$��C�#D�\T�,꿦VÖ������=�����+�>k*�P�u�l=ވ2�hHαo���@�!L�@�=90�z���z����ЇJTD&�aA�Z�"�qc�j8qB5T�^j#4<����1<^�� �p����+��0P\�\eE��G*�Ñ���+�v��s�X��o�[��2���� =@�� �φ�u-ߋ0�ͭs^��!Վ2J����xaa:z>���u ��ۃ��z���.�y�e�$^[�F�yԦIf9��,S��ee<E�+�6L�{�    ���=<^�� ��A�f��ǅmP[ضoVU�G�� ���r�!�^�����7=\��z��ʠ��o�EFڰ2�jjy\���HM��mB�w�+h��ݥ{��w =@��å{���zN��&ܰJ��0����a�I�k�[��߆.�{З̺�oz��z���2�:7����6%�p#��A�O���QB�_�G�eÐ�z��j�� z�����hi�pܪ��(Q�Jie�,�&E��K�HZ0�x�^��
=@{�å{������,�j��MڜR����0�W�z�a<h�{	}�g��@������ =@e�;�av��5�Y��4�pR��&���kje���kj�p=@���=z����֫Ұ�ԼNT�n��VvD�H�(*�d�+|�{��@�� �p����+����*����lkd�e���2ZFz�Р��;��`	\���z�����7U�FnX�8Ri�	ÊV	��Kj��K��3z6_BF��*�p����+�>�R!ե�+v�}5tܪ�S�t��\^jÅ��ז�]F��h� z��z����� =@e��L�m`E>b�d�g����2�/M���:6�����tx���I�!��z��ʠ7?Q�(����[7�r�mS���2�{�B�_]0g/3�@�_/��x@��W�S2�4*"���]�E��YF�T~���x
eye<=���{���z�����G�e�f\ٶgG�O��i��)Kl�*�pz�*߽��k��ujx���Ö ��=`���=`�7a/�S���)i�:�Xc�i��a.M2��^c��+�g�l,�=`���=`��M�g1�*/�L�Z��m�����U�s��_�L�˗����z���z���Z8�w7�BZ`�A�����6dJ�v��/��/���|9����{�����G���WIe*RZ��#����S�QO-���=����� ���=`�����79/
_bn�)��'efP��Q�%�1R��:���<n�� =@��ے�G�l66Ɣ&u\SZ��V\#ņ��	z��3!,��}�<;D��o�m-1�z���.�����4Ō��ֈ����*&��&g�5���~>�dG�q���=�!��z��ʠ�y%h ܖ3�	C�U��MM�>:C�S42,�E}	/��o�@�� �uAߴ�V��
̦2���<f�IqHLA1�\�W(!C^[�]���?��kk_���<4j3J�[򈅰j��JI�(!�a����������@�Da@��ώ0���!󝦈ê��\	%(E
D�K�A	WŰ{��l�� ��A���z��ʠ�hN#��6h�*�K��<ϓ�����X�x �r �� z���z������Bm꼈���,�r%OT��Ì�m��t*T�����C��z���2��0-4�x`(�L�4��&���ͫL�ܣ�H���{�r`�@���3�����^I�0��b�qZ�ˬ>)JZ;>��Xz���æ���W})O0@���p����+�>�教��)�2H����j\&�i
z�~>ڢ�!�:ݿVS-Npb{8*?���4/2�/S3U/������F�G.?~� "�0�6K	a@�ƕEX�b�<)��p��EjydK�K7�Zʵa� �ј�Kf� =@�� �φ�OƨR��YW�V�%�S�ġj�!/��A�sK������ =@�_�ķ��ִ�vy�� ��-k��ir�P����M�/���/@���� ���z��KIP9I4�anځ������>�hʠ��W\"����Ǔ z��Z���� =@�_�i�T�P���r��+�1éS�T��!+�M�Cb�J=��Nf��R@���=z�������j�1OiJ���������Vz��)S�=�?;��B� =@{�å{���zʰf!���Y�y8��2sl{������Y� ��B=@��W}���Q-S�İ��َf�7�c����%�����L�w���7=d� =@�_�r.n}�!�ܷ͋���ʸc��C�|m�h�:_��y ��A?� z���z/-M�9�.�$�U��t���&M�*� =@�� =@�_1�y�&>6J�i�̮-G�¨�h�Z3�z���6l������&�ukﺎC�^��q)����N�FT
��;&S2�A8��pX{"�ۋ0�c�0 "��0n���q#H��H�"5C�e�GB�B��`����z�����v�T�Uf�ʌ��-'���c��E�5��a���G����R@���ڻ =@�_�~�T�J\dF`W4�[5*�,����T��0��������� =@e�ǌe�iYcg(شCMKd��-	����!δ_�n��k_��/�Nb|�%���yõm(w���{  ��d|�c��ݠ���S��u�q�k�]'�w���7��8u����Џ�d���Ȁ#��h��.�h6Y�-Y��hAw���|��'9�as�6�n����?�����X.�;V�l�����|������r�p�֯��������|���q<�u�dT�^�.�����Gga�Nܫ�'*����(��_@z����e86^	�ov���=,>����G��z��Av��E�������Z�c���(r�s.�z.�n��[y�����(����]��b� �L��[���ΗA������� �͟ѫ�7]�?w����[�l���s�M{������c]��O /V{�ݹ��x�D�1ߺc>�}%�E���@�|>Zu��_�A��/�7�we|�u��/�)�-���ߍ967�g@:�$�og�2�Yo�K�"���~w	�;�����y�n� �oo��ep)�e�1���ϫ�3������t�=��O���F���a���c"���T揠��A��x�i�S{_��vW��#��2�Ve`�pg�u&��l��O����刂o>�H�/�X�eL���Խ1�L���|t���K����k���Q��H>���ߟw��\�Ŷ��c����?���~�9�G�e�]H�%l3tiw��������g��x2׭I%μ7����Ew䟹A�Z��q?OD`<_�)��{�����ڗ��R�8gߍ/����)_�w���{��8Y��L~�`�/�1��Uf�}\���gn�S]}JҰ�B����CW�3�uuL�e�wI������#O��	��F,�02�:%�0궰q!�����a+�����CF=d���CF�O���-��~#��u�صC촶����P(S� ��˭���@����\@��W�p01�r|%!75VYU�E�ff�g�5E*�h��K��ozxm@��W=���q%���fQ�۔��Np5�e�Dc�V��~>Z�J= �B=@��W=��4�b%�8)N���5<�J�����c��藫7X0��z�����g�F�<�1�p��2Έ�(��0���=zEp���z���	��+�@���RYi�U�Q-�Tc	�A�(�2��8�~A z���z����л��R�FM�5�T�J��<ʒ�J���=�8�������/��s��=<Gj�=<G�z�~���X��HM5q��!Q�T��m��d��܃� �� =@�� =@MЧJ4��P!T�i��m�:���g����:�(t������̺�K�7x���K�p�.�_٥��wsBC�!8F\1��2��6�jG�����L�>G���z���z��'C_�^�mBQd�&L���)�����=�"�f�[";<d� =@�� =@���Gy�'�˝����V �/C#��j��/�:��%;��G�� =@��?z�l̉�ہ��&jI�pf7M[V�����5����)����z���6��������ʧw    W      �   [   x��1�  ��M[Aa���4ԁAM��G���r�W�!j=I�Xb�,�Qcդ~SƴVȶ�wtO���'�i!0�AV�pev�}hOR      �      x�3�4A�=... ��      �      x�3�4A.# 	�\1z\\\ }      �   ^   x��A
�  ��+�XAO���l��b��������,8��ϐ����
�*u�\"%eE�<���a�2)��k�W�3I�*s���� �ߍ1�5�      �   O   x�3�N-Q.I,)-V�/JI-�,N-�/��SR2rS�J�"\FȺ���䣨/H�MJ��L������ x�%�      �   j   x�%��	�0C�s2L���Nw��sKǇ�@aĈ�F�͢�����0R�F	����+��%��5T��X�\@�T\@�T\@��� U�.��9��''�      �      x�3�4�4 bC.#0Hr��qqq &��      �      x������ � �      �   o  x���I�1�u�)t�1*����`0��9T4���2�I.r��?��0��O�A�ʩ2B\�8���o���3~��Q���u�#x�!� O�T��DJ-y�d�L���z��&�HNF �e�C恑��%jˍ����B����u %BJM9�UZ2�`ɕ٦���e��\S&L�Y���̇l��(�t�h��s�Ռ�a�R��7yg��慈�Z���'T檲$�����!ǫ��Sb-��?e�Ս��t�k]75�ޘ�k���N�7��a^[22�Y�v:C�.��L�,[��f�fK���·l�ip��$c�^f���{�S����Z@Z)Vc�^���?�ה+���6f�6��?l�pi     