/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : customerRelationship

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 24/06/2019 10:06:48
*/


-- ----------------------------
-- Sequence structure for msCustomer_idMsCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "customerRelationship"."msCustomer_idMsCustomer_seq";
CREATE SEQUENCE "customerRelationship"."msCustomer_idMsCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 104
CACHE 1;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"customerRelationship"."msCustomer_idMsCustomer_seq"', 105, true);
