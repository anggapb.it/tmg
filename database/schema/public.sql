/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 24/06/2019 10:07:26
*/


-- ----------------------------
-- Sequence structure for AppDashboard_idAppDashboard_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."AppDashboard_idAppDashboard_seq";
CREATE SEQUENCE "public"."AppDashboard_idAppDashboard_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for AppMenus_id_app_menu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."AppMenus_id_app_menu_seq";
CREATE SEQUENCE "public"."AppMenus_id_app_menu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 117
CACHE 1;

-- ----------------------------
-- Sequence structure for AppShortKey_idAppShortCut_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."AppShortKey_idAppShortCut_seq";
CREATE SEQUENCE "public"."AppShortKey_idAppShortCut_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 34
CACHE 1;

-- ----------------------------
-- Sequence structure for fileUpload_idFileUpload_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fileUpload_idFileUpload_seq";
CREATE SEQUENCE "public"."fileUpload_idFileUpload_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 5
CACHE 1;

-- ----------------------------
-- Sequence structure for msMasterOperatorSpecial_idMasterOperatorSpecial_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msMasterOperatorSpecial_idMasterOperatorSpecial_seq";
CREATE SEQUENCE "public"."msMasterOperatorSpecial_idMasterOperatorSpecial_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msOperatorAppShortKey_idMsOperatorAppShortKey_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msOperatorAppShortKey_idMsOperatorAppShortKey_seq";
CREATE SEQUENCE "public"."msOperatorAppShortKey_idMsOperatorAppShortKey_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 2635
CACHE 1;

-- ----------------------------
-- Sequence structure for msOperatorDashboardPrivilege_idDashboardPrivilege_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msOperatorDashboardPrivilege_idDashboardPrivilege_seq";
CREATE SEQUENCE "public"."msOperatorDashboardPrivilege_idDashboardPrivilege_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msOperatorGroup_idMsOperatorGroup_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msOperatorGroup_idMsOperatorGroup_seq";
CREATE SEQUENCE "public"."msOperatorGroup_idMsOperatorGroup_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 6
CACHE 1;

-- ----------------------------
-- Sequence structure for msOperatorPrivilege_idPrivilege_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msOperatorPrivilege_idPrivilege_seq";
CREATE SEQUENCE "public"."msOperatorPrivilege_idPrivilege_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 3573
CACHE 1;

-- ----------------------------
-- Sequence structure for msOperator_idMsOperator_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msOperator_idMsOperator_seq";
CREATE SEQUENCE "public"."msOperator_idMsOperator_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 67
CACHE 1;

-- ----------------------------
-- Table structure for AppDashboard
-- ----------------------------
DROP TABLE IF EXISTS "public"."AppDashboard";
CREATE TABLE "public"."AppDashboard" (
  "idAppDashboard" int4 NOT NULL DEFAULT nextval('"AppDashboard_idAppDashboard_seq"'::regclass),
  "Title" varchar(40) COLLATE "pg_catalog"."default",
  "OnClick" varchar(100) COLLATE "pg_catalog"."default",
  "Icon" varchar(20) COLLATE "pg_catalog"."default",
  "fidAppMenu" int2 DEFAULT 0,
  "fidAppShorcut" int2 DEFAULT 0,
  "OrderBy" int2,
  "GroupName" varchar(30) COLLATE "pg_catalog"."default",
  "Color" varchar(30) COLLATE "pg_catalog"."default",
  "InnerInfoURL" varchar(100) COLLATE "pg_catalog"."default",
  "ColumnWidth" numeric(2) DEFAULT 3,
  "Description" varchar(100) COLLATE "pg_catalog"."default",
  "IdResult" varchar(100) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."AppDashboard"."OnClick" IS 'javascript function event click';
COMMENT ON COLUMN "public"."AppDashboard"."GroupName" IS 'Untuk grouping dalam short key tersebut';
COMMENT ON COLUMN "public"."AppDashboard"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';
COMMENT ON COLUMN "public"."AppDashboard"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';
COMMENT ON COLUMN "public"."AppDashboard"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';

-- ----------------------------
-- Records of AppDashboard
-- ----------------------------
INSERT INTO "public"."AppDashboard" VALUES (1, 'Stock Position', '', '', 149, 0, 1, '', '', '', 12, '', 'stockPositionContainer');
INSERT INTO "public"."AppDashboard" VALUES (2, 'Monthly National Sales Report', '', '', 149, 0, NULL, '', '', '', 12, '', 'monthlySalesContainer');

-- ----------------------------
-- Table structure for AppMenus
-- ----------------------------
DROP TABLE IF EXISTS "public"."AppMenus";
CREATE TABLE "public"."AppMenus" (
  "idAppMenu" int4 NOT NULL,
  "Title" varchar(50) COLLATE "pg_catalog"."default",
  "URL" varchar(100) COLLATE "pg_catalog"."default",
  "IconImg" varchar(30) COLLATE "pg_catalog"."default",
  "ColorCode" varchar(20) COLLATE "pg_catalog"."default",
  "fidAppMenu" int2 DEFAULT 0,
  "OrderBy" int2 DEFAULT 0,
  "Description" varchar(100) COLLATE "pg_catalog"."default",
  "GroupMenu" int2,
  "Project" int2 DEFAULT 0
)
;
COMMENT ON COLUMN "public"."AppMenus"."GroupMenu" IS '1 = H1
2 = H2
3 = H3
4 = HC3
';
COMMENT ON COLUMN "public"."AppMenus"."Project" IS '0 = DMS
1 = SPK';

-- ----------------------------
-- Records of AppMenus
-- ----------------------------
INSERT INTO "public"."AppMenus" VALUES (101, 'Karyawan', 'human-capital/employee/employee/manage', '', '', 100, 20, 'Data Karyawan', 1, 0);
INSERT INTO "public"."AppMenus" VALUES (100, 'Master', '', 'fa-database', '', 0, 7, '', 1, 0);
INSERT INTO "public"."AppMenus" VALUES (117, 'Barang', 'master/barang/manage', NULL, NULL, 100, 1, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (111, 'Supplier', 'master/supplier/manage', NULL, NULL, 100, 4, 'Data Supplier', 1, 0);
INSERT INTO "public"."AppMenus" VALUES (112, 'Jenis kas', 'master/jenis_kas/manage', NULL, NULL, 100, 5, '', 1, 0);
INSERT INTO "public"."AppMenus" VALUES (113, 'Satuan', 'master/satuan/manage', NULL, NULL, 100, 6, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (114, 'Jenis Kendaraan', 'master/jenis_kendaraan/manage', NULL, NULL, 100, 7, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (115, 'Kelompok Barang', 'master/kelompok_barang/manage', NULL, NULL, 100, 8, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (116, 'Customer', 'master/customer/manage', NULL, NULL, 100, 9, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (118, 'Pengemudi', 'master/pengemudi/manage', NULL, NULL, 100, 2, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (119, 'Kendaraan', 'master/kendaraan/manage', NULL, NULL, 100, 3, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (121, 'Order', 'order/manage', NULL, NULL, 120, 1, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (120, 'Transaksi', '', 'fa-tag', NULL, 0, 1, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (122, 'Laporan', '', 'fa-bookmark', NULL, 0, 2, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (123, 'Kas Masuk', 'laporan.kas/manage/manage/1', NULL, NULL, 122, 1, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (125, 'Pembelian', 'po/manage', NULL, NULL, 120, 2, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (124, 'Kas Keluar', 'laporan.kas/manage/manage/2', NULL, NULL, 122, 2, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (126, 'Pembelian', 'laporan.pembelian/manage', NULL, NULL, 122, 3, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (127, 'Perbaikan Kendaraan', 'laporan.perbaikan/manage', NULL, NULL, 122, 4, NULL, 1, 0);
INSERT INTO "public"."AppMenus" VALUES (128, 'Laba Rugi', 'laporan.laba_rugi/manage', NULL, NULL, 122, 5, NULL, 1, 0);

-- ----------------------------
-- Table structure for AppShortCut
-- ----------------------------
DROP TABLE IF EXISTS "public"."AppShortCut";
CREATE TABLE "public"."AppShortCut" (
  "idAppShortCut" int4 NOT NULL DEFAULT nextval('"AppShortKey_idAppShortCut_seq"'::regclass),
  "Title" varchar(40) COLLATE "pg_catalog"."default",
  "OnClick" varchar(100) COLLATE "pg_catalog"."default",
  "Icon" varchar(20) COLLATE "pg_catalog"."default",
  "fidAppMenu" int2,
  "OrderBy" int2,
  "GroupName" varchar(30) COLLATE "pg_catalog"."default",
  "Color" varchar(30) COLLATE "pg_catalog"."default",
  "InnerInfoURL" varchar(100) COLLATE "pg_catalog"."default",
  "ColumnWidth" numeric(2) DEFAULT 3,
  "Description" varchar(100) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."AppShortCut"."OnClick" IS 'javascript function event click';
COMMENT ON COLUMN "public"."AppShortCut"."GroupName" IS 'Untuk grouping dalam short key tersebut';
COMMENT ON COLUMN "public"."AppShortCut"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';
COMMENT ON COLUMN "public"."AppShortCut"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';
COMMENT ON COLUMN "public"."AppShortCut"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';

-- ----------------------------
-- Table structure for fileUpload
-- ----------------------------
DROP TABLE IF EXISTS "public"."fileUpload";
CREATE TABLE "public"."fileUpload" (
  "idFileUpload" int4 NOT NULL DEFAULT nextval('"fileUpload_idFileUpload_seq"'::regclass),
  "Category" varchar(50) COLLATE "pg_catalog"."default",
  "fidData" int4,
  "FileExtention" varchar(5) COLLATE "pg_catalog"."default",
  "FileName" varchar(100) COLLATE "pg_catalog"."default",
  "FileSize" int4
)
;

-- ----------------------------
-- Records of fileUpload
-- ----------------------------
INSERT INTO "public"."fileUpload" VALUES (7, 'Profile', 806, 'jpg', 'KAR.1810-0005', 519548);
INSERT INTO "public"."fileUpload" VALUES (9, 'Profile', 810, 'jpg', 'KAR.1810-0009', 564567);
INSERT INTO "public"."fileUpload" VALUES (10, 'Profile', 808, 'jpg', 'KAR.1810-0007', 479623);
INSERT INTO "public"."fileUpload" VALUES (11, 'Profile', 809, 'jpg', 'KAR.1810-0008', 540456);
INSERT INTO "public"."fileUpload" VALUES (12, 'Profile', 811, 'jpg', 'KAR.1810-0010', 469933);
INSERT INTO "public"."fileUpload" VALUES (13, 'Profile', 812, 'jpg', 'KAR.1810-0011', 497383);
INSERT INTO "public"."fileUpload" VALUES (14, 'Profile', 803, 'jpg', 'APR.1809-0002', 456210);
INSERT INTO "public"."fileUpload" VALUES (8, 'Profile', 807, 'jpg', 'KAR.1810-0006', 511112);
INSERT INTO "public"."fileUpload" VALUES (15, 'Profile', 813, 'jpg', 'KAR.1810-0012', 540400);
INSERT INTO "public"."fileUpload" VALUES (16, 'Profile', 805, 'jpg', 'APR.1809-0004', 508195);
INSERT INTO "public"."fileUpload" VALUES (17, 'Profile', 814, 'jpg', 'KAR.1810-0013', 514605);
INSERT INTO "public"."fileUpload" VALUES (18, 'Profile', 815, 'jpg', 'KAR.1810-0014', 522712);
INSERT INTO "public"."fileUpload" VALUES (19, 'Profile', 804, 'jpg', 'APR.1809-0003', 477005);
INSERT INTO "public"."fileUpload" VALUES (20, 'Profile', 816, 'jpg', 'KAR.1810-0015', 528545);
INSERT INTO "public"."fileUpload" VALUES (21, 'Profile', 817, 'jpg', 'KAR.1810-0016', 490730);
INSERT INTO "public"."fileUpload" VALUES (22, 'Profile', 818, 'jpg', 'KAR.1810-0017', 512332);
INSERT INTO "public"."fileUpload" VALUES (23, 'Profile', 819, 'jpg', 'KAR.1811-0018', 499441);
INSERT INTO "public"."fileUpload" VALUES (24, 'Profile', 820, 'jpg', 'KAR.1811-0019', 547667);
INSERT INTO "public"."fileUpload" VALUES (25, 'Profile', 821, 'jpg', 'KAR.1811-0020', 462656);
INSERT INTO "public"."fileUpload" VALUES (27, 'Profile', 825, 'jpg', 'KAR.1812-0024', 450575);
INSERT INTO "public"."fileUpload" VALUES (28, 'Profile', 824, 'jpg', 'KAR.1812-0023', 477743);
INSERT INTO "public"."fileUpload" VALUES (29, 'Profile', 826, 'jpg', 'KAR.1812-0025', 637381);
INSERT INTO "public"."fileUpload" VALUES (26, 'Profile', 823, 'jpg', 'KAR.1812-0022', 448369);
INSERT INTO "public"."fileUpload" VALUES (30, 'Profile', 827, 'jpg', 'KAR.1812-0026', 446455);
INSERT INTO "public"."fileUpload" VALUES (31, 'Profile', 829, 'jpg', 'KAR.1812-0028', 494713);
INSERT INTO "public"."fileUpload" VALUES (32, 'Profile', 828, 'jpg', 'KAR.1812-0027', 427976);
INSERT INTO "public"."fileUpload" VALUES (33, 'Profile', 830, 'jpg', 'KAR.1812-0029', 409841);
INSERT INTO "public"."fileUpload" VALUES (34, 'Profile', 831, 'jpg', 'KAR.1812-0030', 429562);
INSERT INTO "public"."fileUpload" VALUES (35, 'Profile', 832, 'jpg', 'KAR.1812-0031', 451437);
INSERT INTO "public"."fileUpload" VALUES (36, 'Profile', 833, 'jpg', 'KAR.1812-0032', 459708);
INSERT INTO "public"."fileUpload" VALUES (37, 'Profile', 834, 'jpg', 'KAR.1812-0033', 424551);
INSERT INTO "public"."fileUpload" VALUES (38, 'Profile', 835, 'jpg', 'KAR.1812-0034', 409523);
INSERT INTO "public"."fileUpload" VALUES (39, 'Profile', 836, 'jpg', 'KAR.1812-0035', 417190);
INSERT INTO "public"."fileUpload" VALUES (40, 'Profile', 837, 'jpg', 'KAR.1812-0036', 490614);
INSERT INTO "public"."fileUpload" VALUES (41, 'Profile', 838, 'jpg', 'KAR.1812-0037', 316311);
INSERT INTO "public"."fileUpload" VALUES (42, 'Profile', 841, 'jpg', 'KAR.1904-0040', 497448);
INSERT INTO "public"."fileUpload" VALUES (6, 'Profile', 1, 'JPG', 'APR.1712-0001', 90494);

-- ----------------------------
-- Table structure for karunia_sessions
-- ----------------------------
DROP TABLE IF EXISTS "public"."karunia_sessions";
CREATE TABLE "public"."karunia_sessions" (
  "id" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "ip_address" varchar(45) COLLATE "pg_catalog"."default" NOT NULL,
  "timestamp" int8 NOT NULL DEFAULT 0,
  "data" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::text
)
;

-- ----------------------------
-- Records of karunia_sessions
-- ----------------------------
INSERT INTO "public"."karunia_sessions" VALUES ('ph0nb5n76hpibbmrm9koo7fkgbiqdlp7', '::1', 1558504318, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTA0MzE4O2xhbmd1YWdlfHM6MjoiZW4iOw==');
INSERT INTO "public"."karunia_sessions" VALUES ('07j630vtcq0l1gopejadhna6iaeceh5g', '::1', 1558897145, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk3MTQ1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('tla6c4oq4s1bfd9bacpben10k5vm5n74', '::1', 1558512077, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTExMDE1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI0ZWUxZGFjYzQyMzM1ZTNhNGY3NjY2NzliNTQ2NTJlNyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ioa7jc6dgeautp40d2ueo0n4i7eh8hnj', '::1', 1558538999, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTM4OTk5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('iep4n0e8a2ql1a5h7auk54teda22ltht', '::1', 1558510689, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTEwNjg5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI0ZWUxZGFjYzQyMzM1ZTNhNGY3NjY2NzliNTQ2NTJlNyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('4iobacbs2onsr166qfti50nh057850gj', '::1', 1558511015, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTExMDE1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI0ZWUxZGFjYzQyMzM1ZTNhNGY3NjY2NzliNTQ2NTJlNyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('4g78umpiddspcggvekbchveoe6hiqq48', '::1', 1558895454, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk1NDU0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('itjm1lltoru9bbbhffk6aqqob331jl3b', '::1', 1558890530, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODkwNTMwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('5jak5beai9m478t56vl20vumlpjv4t9t', '::1', 1559029876, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDI5ODc2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('64pqo2q63v2pc1fjdi6vlujpadsspg5v', '::1', 1558580118, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTgwMTE4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('jktj65q7v47dmfpp26jumtsdirl3kgeu', '::1', 1559029876, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDI5ODc2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ao9eqtsb3k7aesd3uvgliq0o5bntkl62', '::1', 1558944398, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQ0Mzk4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('1pg6f7dphvjfurgv0o9tkh25tk5fav66', '::1', 1559018421, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDE4NDIxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('iga9ud91pequ74j4pdqgof7utrbu4stf', '::1', 1559101402, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTAxNDAyO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('uglvvrau5bu5727fhcvaecve0var6das', '::1', 1558777401, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4Nzc3NDAxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjUiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('f0mbb96uupo7kguvotiktdls701htfq6', '::1', 1558943981, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQzOTgxO2xhbmd1YWdlfHM6MTE6ImxhcG9yYW4ua2FzIjtMb2dnZWRJbnxiOjE7T3BlcmF0b3J8YToxNjp7czoxMjoiaWRNc09wZXJhdG9yIjtzOjE6IjEiO3M6OToiTG9naW5OYW1lIjtzOjU6ImFkbWluIjtzOjk6IkxvZ2luUGFzcyI7czozMjoiOGRjZTFkYTEyYTg4NDJhMjQwZDhjZDlkNDdkMjA5NmMiO3M6ODoiRnVsbE5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6OToiS29kZUFIQVNTIjtzOjU6IjAyNTUyIjtzOjEwOiJMYXN0VXBkYXRlIjtOO3M6MTA6IkV4cGlyeURhdGUiO3M6MTA6IjIwNTAtMTItMzEiO3M6MTg6ImZpZE1zT3BlcmF0b3JHcm91cCI7czoxOiIxIjtzOjEzOiJmaWRNc0VtcGxveWVlIjtzOjE6IjEiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjE1OiJLb2RlU2FsZXNQZXJzb24iO047czoxNToiTG9naW5QYXNzRmluY295IjtOO3M6MTg6ImZpZE1lbnVEZWZhdWx0T3BlbiI7czoxOiIwIjtzOjE0OiJpc0FjY2Vzc0d1ZGFuZyI7czoxOiIwIjtzOjE1OiJpc0RlZmF1bHRHdWRhbmciO3M6MToiMCI7czo3OiJDdXJEYXRlIjtzOjEwOiIyMDE5LTA1LTI2Ijt9RW1wbG95ZWV8YToyOTp7czoxMjoiaWRNc0VtcGxveWVlIjtzOjE6IjEiO3M6NzoiRW1wQ29kZSI7czoxMzoiQVBSLjE3MTItMDAwMSI7czo0OiJOYW1lIjtzOjEzOiJBZG1pbmlzdHJhdG9yIjtzOjExOiJEYXRlT2ZCaXJ0aCI7czoxMDoiMTk4NS0wOC0wMiI7czoxMToiQ2l0eU9mQmlydGgiO3M6NzoiQmFuZHVuZyI7czoxMjoiSURDYXJkTnVtYmVyIjtzOjE2OiI5ODE3MzI5ODczMjk4NzE5IjtzOjc6IkFkZHJlc3MiO3M6MzQ6IkpsLiBQYW5nZXJhbiBTdXBlciBIZXJvIElJSSBuby4gMjkiO3M6NDoiQ2l0eSI7czoxNzoiQmFuZHVuZyBzYXR1IHNhamEiO3M6MTI6IlBob25lTnVtYmVyMSI7czozOiIwODEiO3M6MTI6IlBob25lTnVtYmVyMiI7czozOiIwODIiO3M6NToiRW1haWwiO3M6MTY6ImhlbmRyYUBzdWhlci5jb20iO3M6MTQ6IkFwcHJvdmFsU3RhdHVzIjtzOjE6IjkiO3M6ODoiTmlja05hbWUiO3M6NDoiYXNkZCI7czo5OiJmaWRHZW5kZXIiO3M6MToiMiI7czoxMzoiZmlkTXNQb3NpdGlvbiI7czoxOiI1IjtzOjEzOiJmaWRNc1JlbGlnaW9uIjtzOjE6IjAiO3M6OToiS29kZUFoYXNzIjtzOjU6IjAyNTUyIjtzOjEwOiJLb2RlRGVhbGVyIjtzOjM6IkFQMSI7czoxMjoiS29kZVNhbGVzQUhNIjtzOjc6IkFITTEyMzQiO3M6NzoiSmFiYXRhbiI7czoxNzoiU2FsZXMgS29vcmRpbmF0b3IiO3M6MTM6ImZpZFBlbmRpZGlrYW4iO3M6MToiNSI7czo3OiJmaWRIb2JpIjtzOjM6IkExMCI7czo4OiJQcm92aW5zaSI7czowOiIiO3M6NzoiS29kZVBvcyI7czoxOiIwIjtzOjExOiJBZGRyZXNzX0tUUCI7czowOiIiO3M6MTI6IlByb3ZpbnNpX0tUUCI7czowOiIiO3M6ODoiQ2l0eV9LVFAiO3M6MDoiIjtzOjExOiJLb2RlUG9zX0tUUCI7czoxOiIwIjtzOjc6ImlzU2FsZXMiO3M6MToiMSI7fQ==');
INSERT INTO "public"."karunia_sessions" VALUES ('srqhtnoherens038uadoaocddqsp6pml', '::1', 1558894701, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk0NzAxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('smqh8gt0po9u51iq2l3nkpttl2dq7ns6', '::1', 1558944399, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQ0Mzk4O2xhbmd1YWdlfHM6MTE6ImxhcG9yYW4ua2FzIjtMb2dnZWRJbnxiOjE7T3BlcmF0b3J8YToxNjp7czoxMjoiaWRNc09wZXJhdG9yIjtzOjE6IjEiO3M6OToiTG9naW5OYW1lIjtzOjU6ImFkbWluIjtzOjk6IkxvZ2luUGFzcyI7czozMjoiOGRjZTFkYTEyYTg4NDJhMjQwZDhjZDlkNDdkMjA5NmMiO3M6ODoiRnVsbE5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6OToiS29kZUFIQVNTIjtzOjU6IjAyNTUyIjtzOjEwOiJMYXN0VXBkYXRlIjtOO3M6MTA6IkV4cGlyeURhdGUiO3M6MTA6IjIwNTAtMTItMzEiO3M6MTg6ImZpZE1zT3BlcmF0b3JHcm91cCI7czoxOiIxIjtzOjEzOiJmaWRNc0VtcGxveWVlIjtzOjE6IjEiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjE1OiJLb2RlU2FsZXNQZXJzb24iO047czoxNToiTG9naW5QYXNzRmluY295IjtOO3M6MTg6ImZpZE1lbnVEZWZhdWx0T3BlbiI7czoxOiIwIjtzOjE0OiJpc0FjY2Vzc0d1ZGFuZyI7czoxOiIwIjtzOjE1OiJpc0RlZmF1bHRHdWRhbmciO3M6MToiMCI7czo3OiJDdXJEYXRlIjtzOjEwOiIyMDE5LTA1LTI2Ijt9RW1wbG95ZWV8YToyOTp7czoxMjoiaWRNc0VtcGxveWVlIjtzOjE6IjEiO3M6NzoiRW1wQ29kZSI7czoxMzoiQVBSLjE3MTItMDAwMSI7czo0OiJOYW1lIjtzOjEzOiJBZG1pbmlzdHJhdG9yIjtzOjExOiJEYXRlT2ZCaXJ0aCI7czoxMDoiMTk4NS0wOC0wMiI7czoxMToiQ2l0eU9mQmlydGgiO3M6NzoiQmFuZHVuZyI7czoxMjoiSURDYXJkTnVtYmVyIjtzOjE2OiI5ODE3MzI5ODczMjk4NzE5IjtzOjc6IkFkZHJlc3MiO3M6MzQ6IkpsLiBQYW5nZXJhbiBTdXBlciBIZXJvIElJSSBuby4gMjkiO3M6NDoiQ2l0eSI7czoxNzoiQmFuZHVuZyBzYXR1IHNhamEiO3M6MTI6IlBob25lTnVtYmVyMSI7czozOiIwODEiO3M6MTI6IlBob25lTnVtYmVyMiI7czozOiIwODIiO3M6NToiRW1haWwiO3M6MTY6ImhlbmRyYUBzdWhlci5jb20iO3M6MTQ6IkFwcHJvdmFsU3RhdHVzIjtzOjE6IjkiO3M6ODoiTmlja05hbWUiO3M6NDoiYXNkZCI7czo5OiJmaWRHZW5kZXIiO3M6MToiMiI7czoxMzoiZmlkTXNQb3NpdGlvbiI7czoxOiI1IjtzOjEzOiJmaWRNc1JlbGlnaW9uIjtzOjE6IjAiO3M6OToiS29kZUFoYXNzIjtzOjU6IjAyNTUyIjtzOjEwOiJLb2RlRGVhbGVyIjtzOjM6IkFQMSI7czoxMjoiS29kZVNhbGVzQUhNIjtzOjc6IkFITTEyMzQiO3M6NzoiSmFiYXRhbiI7czoxNzoiU2FsZXMgS29vcmRpbmF0b3IiO3M6MTM6ImZpZFBlbmRpZGlrYW4iO3M6MToiNSI7czo3OiJmaWRIb2JpIjtzOjM6IkExMCI7czo4OiJQcm92aW5zaSI7czowOiIiO3M6NzoiS29kZVBvcyI7czoxOiIwIjtzOjExOiJBZGRyZXNzX0tUUCI7czowOiIiO3M6MTI6IlByb3ZpbnNpX0tUUCI7czowOiIiO3M6ODoiQ2l0eV9LVFAiO3M6MDoiIjtzOjExOiJLb2RlUG9zX0tUUCI7czoxOiIwIjtzOjc6ImlzU2FsZXMiO3M6MToiMSI7fQ==');
INSERT INTO "public"."karunia_sessions" VALUES ('fl94sdhjda3cgrp04kjchda9ucq3d574', '::1', 1558777402, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4Nzc3NDAxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjUiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('mahkgkbdvlgrtnt2vjsapqhv692u02hl', '::1', 1558664410, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NjY0NDEwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('i7nultp3v0i0p8ufi52hh9reqnd6rkcp', '::1', 1558539358, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTM5MzU4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('rt7noltdsg4j1p3prplhelkq6b0l3p75', '::1', 1558928504, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTI4NTA0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('t16kh96ihb40mrm6j2evlkl2904stbj1', '::1', 1559095351, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDk1MzUxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('7gbjqdalt6vv4v2frpogv4cci3o8cd46', '::1', 1558924942, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTI0OTQyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('6j5pd5djlcfvc3c8av5dt81bl4nirgen', '::1', 1558882504, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODgyNTA0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('og6rsnjlcr5rirh9opmlcoftvpmbkrph', '::1', 1558930078, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTMwMDc4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('4ojohs7ssmsv1uao869tcd3temk8drp2', '::1', 1558899126, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk5MTI2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('pk7lolnrcu3ntp137bntvoqlsms088vp', '::1', 1559110685, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTEwNjgzO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('vn07et0jk9sbuf9p29d1p23c0i4ggjhm', '::1', 1558670572, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NjcwNTcyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('1pqjtjp4i8u4khubi24jia9rik0610ad', '::1', 1558891162, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODkxMTYyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('t92dmbqbo2vpaaecm30onnn1fdm0stqh', '::1', 1558930479, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTMwNDc5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('mam8s3mcspaul3ojnvi3krtgv0pshu6j', '::1', 1558895016, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk1MDE2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('k7j91n17eiuasphiv7eg90emcq5u6fut', '::1', 1558942977, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQyOTc3O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('08vm9lfol5qhtht9ea8duu3pie89dajv', '::1', 1558670575, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NjcwNTcyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('fdnbt3jj6h737vp50h6glkanp685ls2m', '::1', 1558891938, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODkxOTM4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('md51sqsgnqufkgnbtce20e99r4f0sp4k', '::1', 1558539794, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTM5MzU4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ousc23avfacrvg5u9roo3j341p1hd4if', '::1', 1558580120, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTgwMTE4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('j6kqkb51jq3et9r9np1uad7nielm380s', '::1', 1558889819, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODg5ODE5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('qa0lr2p74qqjjbg2scg1kekfdc5sddfo', '::1', 1558931914, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTMxOTE0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ffchgk118e8ftp5tfuhk07dko32po2dm', '::1', 1559018013, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDE4MDEzO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('5igvri6mgvqlavgg3d3moceme4cb6l3h', '::1', 1558683216, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NjgwMTQ1O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('fohek2m1lqda3d9ddhbssrhkmem0g31q', '::1', 1558900244, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4ODk5MTI2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('t1harqo2vjsb97b8plr2tus2t27nfuj6', '::1', 1559015606, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDE1NjA2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('52gk3m5acr84q9qbed5blev29crmi1t3', '::1', 1558943589, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQzNTg5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('etpamk0hogfui3269fk9lr5u0hh2i0ps', '::1', 1559103229, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTAzMjI5O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('sav3q651v7pae6kmoq6rco1bqgun0kj6', '::1', 1559015090, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDE1MDkwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('isoc9ib7rj0lfmvp3395m58j89bue5s8', '::1', 1559098118, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDk1MzUxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('e6abadiuqugkuubsa8d6u3aene41uidn', '::1', 1558942660, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTQyNjYwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('vgsnjug7ptm7btv2gfsdk3fk7hi1i671', '::1', 1559025650, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDI1NjUwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('c100avgj6bc1sirvt0cbrlnit7740jca', '::1', 1559024004, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MDI0MDA0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('usqfkfjsr1k73p145j24168s98hhr3dv', '::1', 1558925650, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTI1NjUwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('pj56s07f3lbl6dt1h7jefssdndbf3heo', '::1', 1558598562, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4NTkzODQ0O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('5o63vickh4ila2ojgbpug4ad2f3p1rbf', '::1', 1558931490, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU4OTMxNDkwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjYiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('b6hm77kkdnic9dft2fikp8fvnbtkbrnl', '::1', 1559102223, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTAyMjIzO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('er45j73kqk37fbdqfqlmdoo3mn69hrg9', '::1', 1559103634, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTAzNjM0O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('0qcavblsddgdkuh2iievhnurr1d9sjk1', '::1', 1559107366, 'bGFuZ3VhZ2V8czo1OiJvcmRlciI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTA3MzY2O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('m8gsj9ibd32vutk84ubrnilt5e4f3mo5', '::1', 1559108425, 'bGFuZ3VhZ2V8czo1OiJvcmRlciI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTA4NDI1O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ol3ssgpob94va4vvopntb0kq0qsoh58g', '::1', 1559103948, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTAzOTQ4O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('chf6fkoj3q14lpil87a6u5vj46tvidmp', '::1', 1559107915, 'bGFuZ3VhZ2V8czo1OiJvcmRlciI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTA3OTE1O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('517psmsbi706v8rtk9pfjhp32o3g7qan', '::1', 1559104315, 'bGFuZ3VhZ2V8czo1OiJvcmRlciI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTA0MzE1O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('0kj6h8il5b7n4g3fuq11mroa3hfp0m8m', '::1', 1559110684, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5MTEwNjgzO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDUtMjgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('u01knlcid1133mtlt33rf3t6fan1ae12', '::1', 1560745499, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNzQ1Mzk2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('69lrgc5tnn1440o13vaktihlqsl28dvm', '::1', 1560872985, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODcyOTM5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('1ps93ui01vrg4attujjhv6ntfnb4mah0', '::1', 1560494682, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDkzNTUwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTQiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('uvj8t3mcibusb6jf8g4b7g2n1j2b9313', '::1', 1560763224, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNzYzMjI0O2xhbmd1YWdlfHM6MjoiZW4iOw==');
INSERT INTO "public"."karunia_sessions" VALUES ('pjatbkmjt1cs0g2c017cfd732lj4km25', '::1', 1560478606, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDc4NjA2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTQiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('to4224oheuqljstdtp6ii6ir9qj903d3', '::1', 1560325795, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzI1Nzk1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('g3p3uajslmta3nf0spdr55jpppmi37b0', '::1', 1560311831, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzExODMxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('h97utpqlesqscfseu6pne0qrae4anfuj', '::1', 1560745397, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNzQ1Mzk2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTciO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('tjnq8045ia65gib2gf3pd6unsglfpso6', '::1', 1560410818, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDEwODE4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('6n7jlle5v1guev85cfsmqr3teh2o7l7i', '::1', 1560310512, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzEwNTEyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('oie73psbpj2urbrimanshqp2pn7d93i3', '::1', 1560864397, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODY0Mzk3O2xhbmd1YWdlfHM6MjoiZW4iOw==');
INSERT INTO "public"."karunia_sessions" VALUES ('at4kq0nijhk5j7s90331pcp369lvjbr0', '::1', 1561044857, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYxMDQxMDA1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMjAiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('t7n8lm1mdg1krehln7dh6npqobhrnbn7', '::1', 1560408144, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDA4MTQ0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('17gl1l2pmrpuae2dlqvj8pkdbhrrfp38', '::1', 1560417581, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDE3NTgxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('hne4436qn6btsp8tjr5pngr1j7brj5ra', '::1', 1560493550, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDkzNTUwO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTQiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('2hc46p24uv9tf5v4vi0qghldv3jtgv3n', '::1', 1560310969, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzEwOTY5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('21ml84n2isempiu4327015bdihif55n9', '::1', 1559968685, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTU5OTY4NDU3O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMDgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('nomsh96cjt8pbdhsb44aetnh2sv6o0ck', '::1', 1560400931, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDAwOTMxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ott37bid6k4g6um3229di8thcibkljd6', '::1', 1560313478, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzEzNDc4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('3418c04vq6g8chekg1jcorl984edg1g3', '::1', 1560417582, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDE3NTgxO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('im9r438o6qmadit4de8d1m1ureb9s2vt', '::1', 1560330108, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzMwMTA4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('4351kif1vhau2kd5qphdkjd1u0o5eart', '::1', 1560040955, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMDQwOTU1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMDkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('gnrb3ge5dq7mk1sle9nbfevg5b3musl5', '::1', 1560040955, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMDQwOTU1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMDkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('pm7mh1areeucodtce8i7qat6gavrt8b2', '::1', 1560177880, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMTc3ODc4O2xhbmd1YWdlfHM6MjoiZW4iOw==');
INSERT INTO "public"."karunia_sessions" VALUES ('fee3ebh9gr4hl05g060tdvmdnhu9s63g', '::1', 1560221328, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMjE5NzI0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('21v0app55ilm491772klf9qmrqnbj9go', '::1', 1560245096, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMjQwMjk5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('idi6sa1p74cp80oamdhceliemvtlfq58', '::1', 1560481827, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwNDgxODI3O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTQiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('hjnq39d6lqoaids3nh64lct0r6opimv1', '::1', 1560311367, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzExMzY3O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('l45ark88oudo0o91bdj88m818oqv2i8v', '::1', 1560780548, 'bGFuZ3VhZ2V8czoyOiJlbiI7TG9nZ2VkSW58YjoxO09wZXJhdG9yfGE6MTY6e3M6MTI6ImlkTXNPcGVyYXRvciI7czoxOiIxIjtzOjk6IkxvZ2luTmFtZSI7czo1OiJhZG1pbiI7czo5OiJMb2dpblBhc3MiO3M6MzI6IjhkY2UxZGExMmE4ODQyYTI0MGQ4Y2Q5ZDQ3ZDIwOTZjIjtzOjg6IkZ1bGxOYW1lIjtzOjEzOiJBZG1pbmlzdHJhdG9yIjtzOjk6IktvZGVBSEFTUyI7czo1OiIwMjU1MiI7czoxMDoiTGFzdFVwZGF0ZSI7TjtzOjEwOiJFeHBpcnlEYXRlIjtzOjEwOiIyMDUwLTEyLTMxIjtzOjE4OiJmaWRNc09wZXJhdG9yR3JvdXAiO3M6MToiMSI7czoxMzoiZmlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjEwOiJLb2RlRGVhbGVyIjtzOjM6IkFQMSI7czoxNToiS29kZVNhbGVzUGVyc29uIjtOO3M6MTU6IkxvZ2luUGFzc0ZpbmNveSI7TjtzOjE4OiJmaWRNZW51RGVmYXVsdE9wZW4iO3M6MToiMCI7czoxNDoiaXNBY2Nlc3NHdWRhbmciO3M6MToiMCI7czoxNToiaXNEZWZhdWx0R3VkYW5nIjtzOjE6IjAiO3M6NzoiQ3VyRGF0ZSI7czoxMDoiMjAxOS0wNi0xNyI7fUVtcGxveWVlfGE6Mjk6e3M6MTI6ImlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjc6IkVtcENvZGUiO3M6MTM6IkFQUi4xNzEyLTAwMDEiO3M6NDoiTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czoxMToiRGF0ZU9mQmlydGgiO3M6MTA6IjE5ODUtMDgtMDIiO3M6MTE6IkNpdHlPZkJpcnRoIjtzOjc6IkJhbmR1bmciO3M6MTI6IklEQ2FyZE51bWJlciI7czoxNjoiOTgxNzMyOTg3MzI5ODcxOSI7czo3OiJBZGRyZXNzIjtzOjM0OiJKbC4gUGFuZ2VyYW4gU3VwZXIgSGVybyBJSUkgbm8uIDI5IjtzOjQ6IkNpdHkiO3M6MTc6IkJhbmR1bmcgc2F0dSBzYWphIjtzOjEyOiJQaG9uZU51bWJlcjEiO3M6MzoiMDgxIjtzOjEyOiJQaG9uZU51bWJlcjIiO3M6MzoiMDgyIjtzOjU6IkVtYWlsIjtzOjE2OiJoZW5kcmFAc3VoZXIuY29tIjtzOjE0OiJBcHByb3ZhbFN0YXR1cyI7czoxOiI5IjtzOjg6Ik5pY2tOYW1lIjtzOjQ6ImFzZGQiO3M6OToiZmlkR2VuZGVyIjtzOjE6IjIiO3M6MTM6ImZpZE1zUG9zaXRpb24iO3M6MToiNSI7czoxMzoiZmlkTXNSZWxpZ2lvbiI7czoxOiIwIjtzOjk6IktvZGVBaGFzcyI7czo1OiIwMjU1MiI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTI6IktvZGVTYWxlc0FITSI7czo3OiJBSE0xMjM0IjtzOjc6IkphYmF0YW4iO3M6MTc6IlNhbGVzIEtvb3JkaW5hdG9yIjtzOjEzOiJmaWRQZW5kaWRpa2FuIjtzOjE6IjUiO3M6NzoiZmlkSG9iaSI7czozOiJBMTAiO3M6ODoiUHJvdmluc2kiO3M6MDoiIjtzOjc6IktvZGVQb3MiO3M6MToiMCI7czoxMToiQWRkcmVzc19LVFAiO3M6MDoiIjtzOjEyOiJQcm92aW5zaV9LVFAiO3M6MDoiIjtzOjg6IkNpdHlfS1RQIjtzOjA6IiI7czoxMToiS29kZVBvc19LVFAiO3M6MToiMCI7czo3OiJpc1NhbGVzIjtzOjE6IjEiO31fX2NpX2xhc3RfcmVnZW5lcmF0ZXxpOjE1NjA3ODA1NDg7');
INSERT INTO "public"."karunia_sessions" VALUES ('ejoj0lal97jkbktn2a9atvqd1uiqj5gm', '::1', 1560331102, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwMzMwMTA4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTIiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ltkvr36khhu9i2njtnmdte3a1evd8gp9', '::1', 1560963463, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwOTYzNDYzO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('9e12b6gceh1kmj5b84scsmjdqmfobiha', '::1', 1560868974, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODY4OTc0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('3lhsufgcer6kar8c33pd332stm1rkuo0', '::1', 1560962845, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwOTYyODQ1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('4srnokl6l9kdien1n4cpvt495lle334k', '::1', 1560951724, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwOTUxNzI0O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('mod4nq5lai3b0fsmurg9kia1la9nvbbk', '::1', 1561104512, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYxMTAwNzQ5O0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMjEiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ok83opkl1281mevo0oo542950lkrjpgg', '::1', 1560964122, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwOTYzNDYzO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('s56f904124pji6scnr578hj0t9dn84m3', '::1', 1560960346, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwOTYwMzQ2O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTkiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('ft9cn0fkus58br66s8hkvr8g34mismuo', '::1', 1560781851, 'bGFuZ3VhZ2V8czoyOiJlbiI7TG9nZ2VkSW58YjoxO09wZXJhdG9yfGE6MTY6e3M6MTI6ImlkTXNPcGVyYXRvciI7czoxOiIxIjtzOjk6IkxvZ2luTmFtZSI7czo1OiJhZG1pbiI7czo5OiJMb2dpblBhc3MiO3M6MzI6IjhkY2UxZGExMmE4ODQyYTI0MGQ4Y2Q5ZDQ3ZDIwOTZjIjtzOjg6IkZ1bGxOYW1lIjtzOjEzOiJBZG1pbmlzdHJhdG9yIjtzOjk6IktvZGVBSEFTUyI7czo1OiIwMjU1MiI7czoxMDoiTGFzdFVwZGF0ZSI7TjtzOjEwOiJFeHBpcnlEYXRlIjtzOjEwOiIyMDUwLTEyLTMxIjtzOjE4OiJmaWRNc09wZXJhdG9yR3JvdXAiO3M6MToiMSI7czoxMzoiZmlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjEwOiJLb2RlRGVhbGVyIjtzOjM6IkFQMSI7czoxNToiS29kZVNhbGVzUGVyc29uIjtOO3M6MTU6IkxvZ2luUGFzc0ZpbmNveSI7TjtzOjE4OiJmaWRNZW51RGVmYXVsdE9wZW4iO3M6MToiMCI7czoxNDoiaXNBY2Nlc3NHdWRhbmciO3M6MToiMCI7czoxNToiaXNEZWZhdWx0R3VkYW5nIjtzOjE6IjAiO3M6NzoiQ3VyRGF0ZSI7czoxMDoiMjAxOS0wNi0xNyI7fUVtcGxveWVlfGE6Mjk6e3M6MTI6ImlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjc6IkVtcENvZGUiO3M6MTM6IkFQUi4xNzEyLTAwMDEiO3M6NDoiTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czoxMToiRGF0ZU9mQmlydGgiO3M6MTA6IjE5ODUtMDgtMDIiO3M6MTE6IkNpdHlPZkJpcnRoIjtzOjc6IkJhbmR1bmciO3M6MTI6IklEQ2FyZE51bWJlciI7czoxNjoiOTgxNzMyOTg3MzI5ODcxOSI7czo3OiJBZGRyZXNzIjtzOjM0OiJKbC4gUGFuZ2VyYW4gU3VwZXIgSGVybyBJSUkgbm8uIDI5IjtzOjQ6IkNpdHkiO3M6MTc6IkJhbmR1bmcgc2F0dSBzYWphIjtzOjEyOiJQaG9uZU51bWJlcjEiO3M6MzoiMDgxIjtzOjEyOiJQaG9uZU51bWJlcjIiO3M6MzoiMDgyIjtzOjU6IkVtYWlsIjtzOjE2OiJoZW5kcmFAc3VoZXIuY29tIjtzOjE0OiJBcHByb3ZhbFN0YXR1cyI7czoxOiI5IjtzOjg6Ik5pY2tOYW1lIjtzOjQ6ImFzZGQiO3M6OToiZmlkR2VuZGVyIjtzOjE6IjIiO3M6MTM6ImZpZE1zUG9zaXRpb24iO3M6MToiNSI7czoxMzoiZmlkTXNSZWxpZ2lvbiI7czoxOiIwIjtzOjk6IktvZGVBaGFzcyI7czo1OiIwMjU1MiI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTI6IktvZGVTYWxlc0FITSI7czo3OiJBSE0xMjM0IjtzOjc6IkphYmF0YW4iO3M6MTc6IlNhbGVzIEtvb3JkaW5hdG9yIjtzOjEzOiJmaWRQZW5kaWRpa2FuIjtzOjE6IjUiO3M6NzoiZmlkSG9iaSI7czozOiJBMTAiO3M6ODoiUHJvdmluc2kiO3M6MDoiIjtzOjc6IktvZGVQb3MiO3M6MToiMCI7czoxMToiQWRkcmVzc19LVFAiO3M6MDoiIjtzOjEyOiJQcm92aW5zaV9LVFAiO3M6MDoiIjtzOjg6IkNpdHlfS1RQIjtzOjA6IiI7czoxMToiS29kZVBvc19LVFAiO3M6MToiMCI7czo3OiJpc1NhbGVzIjtzOjE6IjEiO31fX2NpX2xhc3RfcmVnZW5lcmF0ZXxpOjE1NjA3ODE4NTE7');
INSERT INTO "public"."karunia_sessions" VALUES ('qnb7mf8uu2a29303eb7ctfuai2o9qli1', '::1', 1560782097, 'bGFuZ3VhZ2V8czoyOiJlbiI7TG9nZ2VkSW58YjoxO09wZXJhdG9yfGE6MTY6e3M6MTI6ImlkTXNPcGVyYXRvciI7czoxOiIxIjtzOjk6IkxvZ2luTmFtZSI7czo1OiJhZG1pbiI7czo5OiJMb2dpblBhc3MiO3M6MzI6IjhkY2UxZGExMmE4ODQyYTI0MGQ4Y2Q5ZDQ3ZDIwOTZjIjtzOjg6IkZ1bGxOYW1lIjtzOjEzOiJBZG1pbmlzdHJhdG9yIjtzOjk6IktvZGVBSEFTUyI7czo1OiIwMjU1MiI7czoxMDoiTGFzdFVwZGF0ZSI7TjtzOjEwOiJFeHBpcnlEYXRlIjtzOjEwOiIyMDUwLTEyLTMxIjtzOjE4OiJmaWRNc09wZXJhdG9yR3JvdXAiO3M6MToiMSI7czoxMzoiZmlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjEwOiJLb2RlRGVhbGVyIjtzOjM6IkFQMSI7czoxNToiS29kZVNhbGVzUGVyc29uIjtOO3M6MTU6IkxvZ2luUGFzc0ZpbmNveSI7TjtzOjE4OiJmaWRNZW51RGVmYXVsdE9wZW4iO3M6MToiMCI7czoxNDoiaXNBY2Nlc3NHdWRhbmciO3M6MToiMCI7czoxNToiaXNEZWZhdWx0R3VkYW5nIjtzOjE6IjAiO3M6NzoiQ3VyRGF0ZSI7czoxMDoiMjAxOS0wNi0xNyI7fUVtcGxveWVlfGE6Mjk6e3M6MTI6ImlkTXNFbXBsb3llZSI7czoxOiIxIjtzOjc6IkVtcENvZGUiO3M6MTM6IkFQUi4xNzEyLTAwMDEiO3M6NDoiTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czoxMToiRGF0ZU9mQmlydGgiO3M6MTA6IjE5ODUtMDgtMDIiO3M6MTE6IkNpdHlPZkJpcnRoIjtzOjc6IkJhbmR1bmciO3M6MTI6IklEQ2FyZE51bWJlciI7czoxNjoiOTgxNzMyOTg3MzI5ODcxOSI7czo3OiJBZGRyZXNzIjtzOjM0OiJKbC4gUGFuZ2VyYW4gU3VwZXIgSGVybyBJSUkgbm8uIDI5IjtzOjQ6IkNpdHkiO3M6MTc6IkJhbmR1bmcgc2F0dSBzYWphIjtzOjEyOiJQaG9uZU51bWJlcjEiO3M6MzoiMDgxIjtzOjEyOiJQaG9uZU51bWJlcjIiO3M6MzoiMDgyIjtzOjU6IkVtYWlsIjtzOjE2OiJoZW5kcmFAc3VoZXIuY29tIjtzOjE0OiJBcHByb3ZhbFN0YXR1cyI7czoxOiI5IjtzOjg6Ik5pY2tOYW1lIjtzOjQ6ImFzZGQiO3M6OToiZmlkR2VuZGVyIjtzOjE6IjIiO3M6MTM6ImZpZE1zUG9zaXRpb24iO3M6MToiNSI7czoxMzoiZmlkTXNSZWxpZ2lvbiI7czoxOiIwIjtzOjk6IktvZGVBaGFzcyI7czo1OiIwMjU1MiI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTI6IktvZGVTYWxlc0FITSI7czo3OiJBSE0xMjM0IjtzOjc6IkphYmF0YW4iO3M6MTc6IlNhbGVzIEtvb3JkaW5hdG9yIjtzOjEzOiJmaWRQZW5kaWRpa2FuIjtzOjE6IjUiO3M6NzoiZmlkSG9iaSI7czozOiJBMTAiO3M6ODoiUHJvdmluc2kiO3M6MDoiIjtzOjc6IktvZGVQb3MiO3M6MToiMCI7czoxMToiQWRkcmVzc19LVFAiO3M6MDoiIjtzOjEyOiJQcm92aW5zaV9LVFAiO3M6MDoiIjtzOjg6IkNpdHlfS1RQIjtzOjA6IiI7czoxMToiS29kZVBvc19LVFAiO3M6MToiMCI7czo3OiJpc1NhbGVzIjtzOjE6IjEiO31fX2NpX2xhc3RfcmVnZW5lcmF0ZXxpOjE1NjA3ODE4NTE7');
INSERT INTO "public"."karunia_sessions" VALUES ('n6oiuhhe3997dvj3pvpv6033bpasgcdq', '::1', 1560872635, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODcyNjM1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('qhfp23jcu21l056bimsb0dnuq7dksbih', '::1', 1560864782, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODY0NzgyO2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('uh8ol56o4s3qrcng9gusa3ifljlf9vo4', '::1', 1561301767, 'bGFuZ3VhZ2V8czoyOiJlbiI7X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYxMjk3MDcwO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMjMiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('tfgpkjvm3a9qot89b6skct06j87vf7jj', '::1', 1560866295, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODY2Mjk1O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('0pmpm4gperg110br8q0fhrjap9v74tf5', '::1', 1560872939, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODcyOTM5O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');
INSERT INTO "public"."karunia_sessions" VALUES ('24d152ahdib8fb0v2u41odug3nv4shnb', '::1', 1560866828, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNTYwODY2ODI4O2xhbmd1YWdlfHM6MjoiZW4iO0xvZ2dlZElufGI6MTtPcGVyYXRvcnxhOjE2OntzOjEyOiJpZE1zT3BlcmF0b3IiO3M6MToiMSI7czo5OiJMb2dpbk5hbWUiO3M6NToiYWRtaW4iO3M6OToiTG9naW5QYXNzIjtzOjMyOiI4ZGNlMWRhMTJhODg0MmEyNDBkOGNkOWQ0N2QyMDk2YyI7czo4OiJGdWxsTmFtZSI7czoxMzoiQWRtaW5pc3RyYXRvciI7czo5OiJLb2RlQUhBU1MiO3M6NToiMDI1NTIiO3M6MTA6Ikxhc3RVcGRhdGUiO047czoxMDoiRXhwaXJ5RGF0ZSI7czoxMDoiMjA1MC0xMi0zMSI7czoxODoiZmlkTXNPcGVyYXRvckdyb3VwIjtzOjE6IjEiO3M6MTM6ImZpZE1zRW1wbG95ZWUiO3M6MToiMSI7czoxMDoiS29kZURlYWxlciI7czozOiJBUDEiO3M6MTU6IktvZGVTYWxlc1BlcnNvbiI7TjtzOjE1OiJMb2dpblBhc3NGaW5jb3kiO047czoxODoiZmlkTWVudURlZmF1bHRPcGVuIjtzOjE6IjAiO3M6MTQ6ImlzQWNjZXNzR3VkYW5nIjtzOjE6IjAiO3M6MTU6ImlzRGVmYXVsdEd1ZGFuZyI7czoxOiIwIjtzOjc6IkN1ckRhdGUiO3M6MTA6IjIwMTktMDYtMTgiO31FbXBsb3llZXxhOjI5OntzOjEyOiJpZE1zRW1wbG95ZWUiO3M6MToiMSI7czo3OiJFbXBDb2RlIjtzOjEzOiJBUFIuMTcxMi0wMDAxIjtzOjQ6Ik5hbWUiO3M6MTM6IkFkbWluaXN0cmF0b3IiO3M6MTE6IkRhdGVPZkJpcnRoIjtzOjEwOiIxOTg1LTA4LTAyIjtzOjExOiJDaXR5T2ZCaXJ0aCI7czo3OiJCYW5kdW5nIjtzOjEyOiJJRENhcmROdW1iZXIiO3M6MTY6Ijk4MTczMjk4NzMyOTg3MTkiO3M6NzoiQWRkcmVzcyI7czozNDoiSmwuIFBhbmdlcmFuIFN1cGVyIEhlcm8gSUlJIG5vLiAyOSI7czo0OiJDaXR5IjtzOjE3OiJCYW5kdW5nIHNhdHUgc2FqYSI7czoxMjoiUGhvbmVOdW1iZXIxIjtzOjM6IjA4MSI7czoxMjoiUGhvbmVOdW1iZXIyIjtzOjM6IjA4MiI7czo1OiJFbWFpbCI7czoxNjoiaGVuZHJhQHN1aGVyLmNvbSI7czoxNDoiQXBwcm92YWxTdGF0dXMiO3M6MToiOSI7czo4OiJOaWNrTmFtZSI7czo0OiJhc2RkIjtzOjk6ImZpZEdlbmRlciI7czoxOiIyIjtzOjEzOiJmaWRNc1Bvc2l0aW9uIjtzOjE6IjUiO3M6MTM6ImZpZE1zUmVsaWdpb24iO3M6MToiMCI7czo5OiJLb2RlQWhhc3MiO3M6NToiMDI1NTIiO3M6MTA6IktvZGVEZWFsZXIiO3M6MzoiQVAxIjtzOjEyOiJLb2RlU2FsZXNBSE0iO3M6NzoiQUhNMTIzNCI7czo3OiJKYWJhdGFuIjtzOjE3OiJTYWxlcyBLb29yZGluYXRvciI7czoxMzoiZmlkUGVuZGlkaWthbiI7czoxOiI1IjtzOjc6ImZpZEhvYmkiO3M6MzoiQTEwIjtzOjg6IlByb3ZpbnNpIjtzOjA6IiI7czo3OiJLb2RlUG9zIjtzOjE6IjAiO3M6MTE6IkFkZHJlc3NfS1RQIjtzOjA6IiI7czoxMjoiUHJvdmluc2lfS1RQIjtzOjA6IiI7czo4OiJDaXR5X0tUUCI7czowOiIiO3M6MTE6IktvZGVQb3NfS1RQIjtzOjE6IjAiO3M6NzoiaXNTYWxlcyI7czoxOiIxIjt9');

-- ----------------------------
-- Table structure for msOperator
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperator";
CREATE TABLE "public"."msOperator" (
  "idMsOperator" int4 NOT NULL,
  "LoginName" varchar(32) COLLATE "pg_catalog"."default",
  "LoginPass" varchar(32) COLLATE "pg_catalog"."default",
  "FullName" varchar(40) COLLATE "pg_catalog"."default",
  "KodeAHASS" varchar(6) COLLATE "pg_catalog"."default",
  "LastUpdate" timestamp(6),
  "ExpiryDate" date,
  "fidMsOperatorGroup" int4 DEFAULT 0,
  "fidMsEmployee" int4,
  "KodeDealer" varchar(6) COLLATE "pg_catalog"."default",
  "KodeSalesPerson" varchar(10) COLLATE "pg_catalog"."default",
  "LoginPassFincoy" varchar(32) COLLATE "pg_catalog"."default",
  "fidMenuDefaultOpen" int2,
  "isAccessGudang" varchar(100) COLLATE "pg_catalog"."default",
  "isDefaultGudang" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of msOperator
-- ----------------------------
INSERT INTO "public"."msOperator" VALUES (1, 'admin', '8dce1da12a8842a240d8cd9d47d2096c', 'Administrator', '02552', NULL, '2050-12-31', 1, 1, 'AP1', NULL, NULL, 0, '0', 0);

-- ----------------------------
-- Table structure for msOperatorAppShortCut
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorAppShortCut";
CREATE TABLE "public"."msOperatorAppShortCut" (
  "idMsOperatorAppShortKey" int4 NOT NULL DEFAULT nextval('"msOperatorAppShortKey_idMsOperatorAppShortKey_seq"'::regclass),
  "fidMsOperator" int4,
  "fidAppShortCut" int2,
  "Status" int2
)
;

-- ----------------------------
-- Records of msOperatorAppShortCut
-- ----------------------------
INSERT INTO "public"."msOperatorAppShortCut" VALUES (1, 1, 1, 1);

-- ----------------------------
-- Table structure for msOperatorDashboardPrivilege
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorDashboardPrivilege";
CREATE TABLE "public"."msOperatorDashboardPrivilege" (
  "idDashboardPrivilege" int4 NOT NULL DEFAULT nextval('"msOperatorDashboardPrivilege_idDashboardPrivilege_seq"'::regclass),
  "fidMsOperator" int4 DEFAULT 0,
  "fidAppDashboard" int2 DEFAULT 0,
  "Status" int2 DEFAULT 0
)
;

-- ----------------------------
-- Records of msOperatorDashboardPrivilege
-- ----------------------------
INSERT INTO "public"."msOperatorDashboardPrivilege" VALUES (1, 1, 1, 1);
INSERT INTO "public"."msOperatorDashboardPrivilege" VALUES (2, 1, 2, 1);

-- ----------------------------
-- Table structure for msOperatorGroup
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorGroup";
CREATE TABLE "public"."msOperatorGroup" (
  "idMsOperatorGroup" int4 NOT NULL DEFAULT nextval('"msOperatorGroup_idMsOperatorGroup_seq"'::regclass),
  "ShortName" varchar(12) COLLATE "pg_catalog"."default",
  "Name" varchar(20) COLLATE "pg_catalog"."default",
  "Description" varchar(100) COLLATE "pg_catalog"."default",
  "Icon" varchar(20) COLLATE "pg_catalog"."default",
  "DefaultMenu" text COLLATE "pg_catalog"."default",
  "DefaultShortcut" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msOperatorGroup
-- ----------------------------
INSERT INTO "public"."msOperatorGroup" VALUES (1, 'MD', 'Master Admin (MD)', 'Dapat mengakses semua menu, dan memberi privilages ke siapapun', NULL, '', '');

-- ----------------------------
-- Table structure for msOperatorModul
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorModul";
CREATE TABLE "public"."msOperatorModul" (
  "idOperatorModul" int4 NOT NULL DEFAULT nextval('"msMasterOperatorSpecial_idMasterOperatorSpecial_seq"'::regclass),
  "Caption" varchar(50) COLLATE "pg_catalog"."default",
  "SpecialVar" varchar(50) COLLATE "pg_catalog"."default",
  "Deskripsi" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msOperatorModul
-- ----------------------------
INSERT INTO "public"."msOperatorModul" VALUES (1, 'Set Status Order', 'set_status_order', 'Ada di menu order');
INSERT INTO "public"."msOperatorModul" VALUES (2, 'Set Status PO', 'set_status_po', 'Ada di menu pembelian');

-- ----------------------------
-- Table structure for msOperatorPrivilege
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorPrivilege";
CREATE TABLE "public"."msOperatorPrivilege" (
  "idPrivilege" int4 NOT NULL DEFAULT nextval('"msOperatorPrivilege_idPrivilege_seq"'::regclass),
  "fidMsOperator" int4 DEFAULT 0,
  "fidAppMenu" int4,
  "Status" int2 DEFAULT 0
)
;
COMMENT ON COLUMN "public"."msOperatorPrivilege"."Status" IS '0 = off
1 = active';

-- ----------------------------
-- Records of msOperatorPrivilege
-- ----------------------------
INSERT INTO "public"."msOperatorPrivilege" VALUES (1272, 1, 100, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (3575, 1, 101, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (3688, 1, 111, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5745, 1, 112, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5746, 1, 113, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5747, 1, 114, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5748, 1, 115, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5749, 1, 116, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5750, 1, 117, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5751, 1, 118, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5752, 1, 119, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5753, 1, 120, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5754, 1, 121, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5755, 1, 122, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5756, 1, 123, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5757, 1, 124, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5758, 1, 125, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5759, 1, 126, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5760, 1, 127, 1);
INSERT INTO "public"."msOperatorPrivilege" VALUES (5761, 1, 128, 1);

-- ----------------------------
-- Table structure for msOperatorSpecial
-- ----------------------------
DROP TABLE IF EXISTS "public"."msOperatorSpecial";
CREATE TABLE "public"."msOperatorSpecial" (
  "idOperatorSpecial" int2 NOT NULL,
  "fidMsOperator" int2,
  "SpecialValue" varchar(40) COLLATE "pg_catalog"."default",
  "fidOperatorModul" int2,
  "Status" int2
)
;

-- ----------------------------
-- Records of msOperatorSpecial
-- ----------------------------
INSERT INTO "public"."msOperatorSpecial" VALUES (1, 1, '0', 1, 1);
INSERT INTO "public"."msOperatorSpecial" VALUES (2, 1, '0', 2, 1);

-- ----------------------------
-- Function structure for f_LaporanSisa_Benang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."f_LaporanSisa_Benang"();
CREATE OR REPLACE FUNCTION "public"."f_LaporanSisa_Benang"()
  RETURNS TABLE("NoPOBenang" varchar, "TglPOBenang" date, "NamaBenang" varchar, "Ukuran" varchar, "total_order_ball_benang" numeric, "total_order_kg_benang" numeric, "NoSJRcvdBenang" varchar, "TglSJRcvdBenang" date, "KodeMakloon" varchar, "QtyRcvd_Ball" numeric, "QtyRcvd_Kg" numeric, "KodeMakloonTransfer" text, "QtyBall_Transfer" numeric, "QtyKg_Transfer" numeric, "idBenangRcvdDetail" int4, "fidBenangRcvdDetail" int2, "idPOMakloon" int4, "NoPOMakloon" varchar, "idMakloonJob" int4, "Deskripsi" varchar, "idPOMakloonDetail" int4, "TotalRolMakloon" numeric, "TotalKgMakloon" numeric, "idGreigeRcvd" int4, "idGreigeRcvdDetail" int4, "NoSJGreige" varchar, "TotalTerimaRolGreige" int8, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" varchar, "idGreigeRcvdTrans" int4, "idGreigeRcvdDetailTrans" int4, "NoSJRcvdGreigeTrans" varchar, "TotalTerimaRolGreigeTrans" int8, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOMakloon" int2, "fidPOMakloonDetail" int4, "KodeMakloonGreige" varchar, "TotalSisaRolMakloon" numeric, "TotalSisaKgMakloon" numeric, "fidGreigeRcvd" int2, "fidGreigeRcvdDetail" int2, "TglPO-C" date, "PO-C" varchar, "idCelupanJob" int4, "DeskripsiJobCelupan" varchar, "WarnaCelupan" varchar, "TotalOrderCelupan" numeric, "AsalSJ-G" varchar, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric) AS $BODY$
BEGIN
CREATE TEMPORARY TABLE "xTmp1" AS SELECT
	"po_benang"."NoPO" as "NoPOBenang",
	po_benang."Tanggal" as "TglPOBenang",
	"bn"."NamaBenang",
	rcvd_benang_det."Ukuran",
	po_benang_dtl.total_order_ball as "total_order_ball_benang",
	po_benang_dtl.total_order_kg AS "total_order_kg_benang",
	rcvd_benang."NoSJ" AS "NoSJRcvdBenang",
	rcvd_benang."TanggalSJ" AS "TglSJRcvdBenang",
	ms_mak_benang."KodeMakloon",
	rcvd_benang_det."QtyRcvd_Ball",
	rcvd_benang_det."QtyRcvd_Kg",
	ms_mak_asal_bn."KodeMakloon" || '->' || ms_mak_tuju_bn."KodeMakloon" AS "KodeMakloonTransfer",
	tr_benang_det."Qty_Ball" AS "QtyBall_Transfer",
	tr_benang_det."Qty_Kg" AS "QtyKg_Transfer",
	rcvd_benang_det."idBenangRcvdDetail"
FROM
	"ho"."trBenangOrder" "po_benang"
LEFT JOIN (
	SELECT
		MAX (
			a."fidBenangOrderDetailDelivery"
		) AS "fidBenangOrderDetailDelivery",
		a."fidBenangRcvd",
		a."fidBenangOrder"
	FROM
		"ho"."trBenangRcvdOrder" a
	GROUP BY
		a."fidBenangRcvd",
		a."fidBenangOrder"
) "rcvd_benang_order" ON rcvd_benang_order."fidBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN ho."trBenangRcvd" rcvd_benang ON rcvd_benang_order."fidBenangRcvd" = rcvd_benang."idBenangRcvd" -- LEFT JOIN "ho"."trBenangOrder" "po_benang" ON "rcvd_benang_order"."fidBenangOrder" = "po_benang"."idBenangOrder"
LEFT JOIN (
	SELECT
		"fidTrBenangOrder",
		SUM ("QtyOrder_Ball") AS total_order_ball,
		SUM ("QtyOrder_Kg") AS total_order_kg,
		SUM ("Jumlah") AS total_order_harga
	FROM
		ho."trBenangOrderDetail"
	GROUP BY
		"fidTrBenangOrder"
) po_benang_dtl ON po_benang_dtl."fidTrBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN (
	SELECT
		MAX (a."idBenangRcvdDetail") AS "idBenangRcvdDetail",
		COALESCE (SUM(a."QtyRcvd_Ball"), 0) AS "QtyRcvd_Ball",
		COALESCE (SUM(a."QtyRcvd_Kg"), 0) AS "QtyRcvd_Kg",
		a."fidBenangRcvd",
		a."fidBenang",
		a."Ukuran"
	FROM
		ho."trBenangRcvdDetail" a
	GROUP BY
		a."Ukuran",
		a."fidBenangRcvd",
		a."fidBenang"
) rcvd_benang_det ON rcvd_benang_det."fidBenangRcvd" = rcvd_benang_order."fidBenangRcvd"
LEFT JOIN ho."trBenangOrderDetailDelivery" po_benang_dtl_delivery ON po_benang_dtl_delivery."idBenangOrderDetailDelivery" = rcvd_benang_order."fidBenangOrderDetailDelivery"
LEFT JOIN ho."trBenangOrderDelivery" po_benang_delivery ON po_benang_delivery."idBenangOrderDelivery" = po_benang_dtl_delivery."fidBenangOrderDelivery"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_benang ON ms_mak_benang."idMakloon" = po_benang_delivery."fidMakloon"
LEFT JOIN "dataMaster"."msBenang" bn ON bn."idBenang" = rcvd_benang_det."fidBenang"
LEFT JOIN ho."trBenangTransfer" tr_benang ON tr_benang."fidBenangRcvd" = rcvd_benang."idBenangRcvd"
LEFT JOIN ho."trBenangTransferDetail" tr_benang_det ON tr_benang."idBenangTransfer" = tr_benang_det."fidBenangTransfer"
AND tr_benang_det."fidBenang" = rcvd_benang_det."fidBenang"
AND tr_benang_det."Ukuran" = rcvd_benang_det."Ukuran"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_asal_bn ON ms_mak_asal_bn."idMakloon" = tr_benang."fidMakloonAsal"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_tuju_bn ON ms_mak_tuju_bn."idMakloon" = tr_benang."fidMakloonTujuan";

CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_mak_benang."fidBenangRcvdDetail",
	po_mak."idPOMakloon",
	po_mak."NoPO" AS "NoPOMakloon",
	mak_job."idMakloonJob",
	mak_job."Deskripsi",
	po_mak_detail."idPOMakloonDetail",
	po_mak_detail."BanyakRol" AS "TotalRolMakloon",
	(
		po_mak_detail."BanyakRol" * po_mak_detail."BeratPerRol"
	) AS "TotalKgMakloon",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdMakloon" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(
		grg_celupan."TotalTerimaRolGreige" - COALESCE (
			po_celupan."TotalOrderRolCelupan",
			0
		) - COALESCE (
			po_celupan_trans."TotalOrderRolCelupanTrans",
			0
		)
	) AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOMakloonBenang" po_mak_benang
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_benang."fidPOMakloon"
LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."fidPOMakloon" = po_mak."idPOMakloon"
LEFT JOIN "dataMaster"."msMakloonJob" mak_job ON mak_job."idMakloonJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
GROUP BY
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail"
) grg_celupan ON grg_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg."fidCelupan",
		grg_dtl."fidPOMakloonDetail",
		SUM (po_cel."TotalOrder") AS "TotalOrderRolCelupan"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."idPOMakloonDetail" = grg_dtl."fidPOMakloonDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrder"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd"
	AND grg_dtl."idGreigeRcvdMakloon" = po_cel."fidGreigeRcvdDetail"
	GROUP BY
		grg."fidPOMakloon",
		grg."fidCelupan",
		grg_dtl."fidPOMakloonDetail"
) po_celupan ON po_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND po_celupan."fidCelupan" = grg_rcvd."fidCelupan"
AND po_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN (
	SELECT
		grg."idGreigeRcvd",
		grg_dtl."fidPOMakloonDetail",
		grg_dtl."idGreigeRcvdMakloon",
		SUM (
			po_cel_trans."TotalOrderTrans"
		) AS "TotalOrderRolCelupanTrans"
	FROM
		ho."trGreigeRcvdMakloon" grg_dtl
	LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
	LEFT JOIN ho."trGreigeRcvd" grg ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
	LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrderTrans"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrderTrans"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel_trans ON po_cel_trans."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."idGreigeRcvdTransfer" = po_cel_trans."fidGreigeRcvdDetail"
	GROUP BY
		grg."idGreigeRcvd",
		grg_dtl."fidPOMakloonDetail",
		grg_dtl."idGreigeRcvdMakloon"
) po_celupan_trans ON po_celupan_trans."idGreigeRcvd" = grg_rcvd."idGreigeRcvd"
AND po_celupan_trans."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
AND po_celupan_trans."idGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon";


CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_mak_dtl."fidPOMakloon",
	po_mak_dtl."idPOMakloonDetail" as "fidPOMakloonDetail",
	mak."KodeMakloon" AS "KodeMakloonGreige",
	(
		po_mak_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolMakloon",
	(
		(
			po_mak_dtl."BanyakRol" * po_mak_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgMakloon"
FROM
	ho."trPOMakloonDetail" po_mak_dtl
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_dtl."fidPOMakloon"
LEFT JOIN "dataMaster"."msMakloon" mak ON mak."idMakloon" = po_mak."fidMakloon"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
		FROM
			ho."trGreigeRcvdMakloonRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = grg_rcvd_rol."fidGreigeRcvdMakloon"
	GROUP BY
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail"
) grg ON grg."fidPOMakloon" = po_mak_dtl."fidPOMakloon" and grg."fidPOMakloonDetail" = po_mak_dtl."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		WHERE
		(rcvd_det."fidReturKainRcvdDetail" is null OR rcvd_det."fidReturKainRcvdDetail" = 0)
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp1".*, "xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp1"
LEFT JOIN "xTmp2" ON "xTmp1"."idBenangRcvdDetail" = "xTmp2"."fidBenangRcvdDetail"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOMakloon" = "xTmp3"."fidPOMakloon" AND "xTmp2"."idPOMakloonDetail" = "xTmp3"."fidPOMakloonDetail"
LEFT JOIN "xTmp4" ON 
	("xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail") OR ("xTmp2"."idGreigeRcvdTrans" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetailTrans" = "xTmp4"."fidGreigeRcvdDetail");

DROP TABLE "xTmp1",
 "xTmp2",
 "xTmp3",
 "xTmp4";

END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for f_LaporanSisa_Benang_backup
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."f_LaporanSisa_Benang_backup"();
CREATE OR REPLACE FUNCTION "public"."f_LaporanSisa_Benang_backup"()
  RETURNS TABLE("NoPOBenang" varchar, "TglPOBenang" date, "NamaBenang" varchar, "Ukuran" varchar, "total_order_ball_benang" numeric, "total_order_kg_benang" numeric, "NoSJRcvdBenang" varchar, "TglSJRcvdBenang" date, "KodeMakloon" varchar, "QtyRcvd_Ball" numeric, "QtyRcvd_Kg" numeric, "KodeMakloonTransfer" text, "QtyBall_Transfer" numeric, "QtyKg_Transfer" numeric, "idBenangRcvdDetail" int4, "fidBenangRcvdDetail" int2, "idPOMakloon" int4, "NoPOMakloon" varchar, "idMakloonJob" int4, "Deskripsi" varchar, "idPOMakloonDetail" int4, "TotalRolMakloon" numeric, "TotalKgMakloon" numeric, "idGreigeRcvd" int4, "idGreigeRcvdDetail" int4, "NoSJGreige" varchar, "TotalTerimaRolGreige" int8, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" varchar, "idGreigeRcvdTrans" int4, "idGreigeRcvdDetailTrans" int4, "NoSJRcvdGreigeTrans" varchar, "TotalTerimaRolGreigeTrans" int8, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOMakloon" int2, "fidPOMakloonDetail" int4, "KodeMakloonGreige" varchar, "TotalSisaRolMakloon" numeric, "TotalSisaKgMakloon" numeric, "fidGreigeRcvd" int2, "fidGreigeRcvdDetail" int2, "TglPO-C" date, "PO-C" varchar, "idCelupanJob" int4, "DeskripsiJobCelupan" varchar, "WarnaCelupan" varchar, "TotalOrderCelupan" numeric, "AsalSJ-G" varchar, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric) AS $BODY$
BEGIN
CREATE TEMPORARY TABLE "xTmp1" AS SELECT
	"po_benang"."NoPO" as "NoPOBenang",
	po_benang."Tanggal" as "TglPOBenang",
	"bn"."NamaBenang",
	rcvd_benang_det."Ukuran",
	po_benang_dtl.total_order_ball as "total_order_ball_benang",
	po_benang_dtl.total_order_kg AS "total_order_kg_benang",
	rcvd_benang."NoSJ" AS "NoSJRcvdBenang",
	rcvd_benang."TanggalSJ" AS "TglSJRcvdBenang",
	ms_mak_benang."KodeMakloon",
	rcvd_benang_det."QtyRcvd_Ball",
	rcvd_benang_det."QtyRcvd_Kg",
	ms_mak_asal_bn."KodeMakloon" || '->' || ms_mak_tuju_bn."KodeMakloon" AS "KodeMakloonTransfer",
	tr_benang_det."Qty_Ball" AS "QtyBall_Transfer",
	tr_benang_det."Qty_Kg" AS "QtyKg_Transfer",
	rcvd_benang_det."idBenangRcvdDetail"
FROM
	"ho"."trBenangOrder" "po_benang"
LEFT JOIN (
	SELECT
		MAX (
			a."fidBenangOrderDetailDelivery"
		) AS "fidBenangOrderDetailDelivery",
		a."fidBenangRcvd",
		a."fidBenangOrder"
	FROM
		"ho"."trBenangRcvdOrder" a
	GROUP BY
		a."fidBenangRcvd",
		a."fidBenangOrder"
) "rcvd_benang_order" ON rcvd_benang_order."fidBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN ho."trBenangRcvd" rcvd_benang ON rcvd_benang_order."fidBenangRcvd" = rcvd_benang."idBenangRcvd" -- LEFT JOIN "ho"."trBenangOrder" "po_benang" ON "rcvd_benang_order"."fidBenangOrder" = "po_benang"."idBenangOrder"
LEFT JOIN (
	SELECT
		"fidTrBenangOrder",
		SUM ("QtyOrder_Ball") AS total_order_ball,
		SUM ("QtyOrder_Kg") AS total_order_kg,
		SUM ("Jumlah") AS total_order_harga
	FROM
		ho."trBenangOrderDetail"
	GROUP BY
		"fidTrBenangOrder"
) po_benang_dtl ON po_benang_dtl."fidTrBenangOrder" = po_benang."idBenangOrder"
LEFT JOIN (
	SELECT
		MAX (a."idBenangRcvdDetail") AS "idBenangRcvdDetail",
		COALESCE (SUM(a."QtyRcvd_Ball"), 0) AS "QtyRcvd_Ball",
		COALESCE (SUM(a."QtyRcvd_Kg"), 0) AS "QtyRcvd_Kg",
		a."fidBenangRcvd",
		a."fidBenang",
		a."Ukuran"
	FROM
		ho."trBenangRcvdDetail" a
	GROUP BY
		a."Ukuran",
		a."fidBenangRcvd",
		a."fidBenang"
) rcvd_benang_det ON rcvd_benang_det."fidBenangRcvd" = rcvd_benang_order."fidBenangRcvd"
LEFT JOIN ho."trBenangOrderDetailDelivery" po_benang_dtl_delivery ON po_benang_dtl_delivery."idBenangOrderDetailDelivery" = rcvd_benang_order."fidBenangOrderDetailDelivery"
LEFT JOIN ho."trBenangOrderDelivery" po_benang_delivery ON po_benang_delivery."idBenangOrderDelivery" = po_benang_dtl_delivery."fidBenangOrderDelivery"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_benang ON ms_mak_benang."idMakloon" = po_benang_delivery."fidMakloon"
LEFT JOIN "dataMaster"."msBenang" bn ON bn."idBenang" = rcvd_benang_det."fidBenang"
LEFT JOIN ho."trBenangTransfer" tr_benang ON tr_benang."fidBenangRcvd" = rcvd_benang."idBenangRcvd"
LEFT JOIN ho."trBenangTransferDetail" tr_benang_det ON tr_benang."idBenangTransfer" = tr_benang_det."fidBenangTransfer"
AND tr_benang_det."fidBenang" = rcvd_benang_det."fidBenang"
AND tr_benang_det."Ukuran" = rcvd_benang_det."Ukuran"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_asal_bn ON ms_mak_asal_bn."idMakloon" = tr_benang."fidMakloonAsal"
LEFT JOIN "dataMaster"."msMakloon" ms_mak_tuju_bn ON ms_mak_tuju_bn."idMakloon" = tr_benang."fidMakloonTujuan";

CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_mak_benang."fidBenangRcvdDetail",
	po_mak."idPOMakloon",
	po_mak."NoPO" AS "NoPOMakloon",
	mak_job."idMakloonJob",
	mak_job."Deskripsi",
	po_mak_detail."idPOMakloonDetail",
	po_mak_detail."BanyakRol" AS "TotalRolMakloon",
	(
		po_mak_detail."BanyakRol" * po_mak_detail."BeratPerRol"
	) AS "TotalKgMakloon",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdMakloon" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-grg_celupan."TotalOrderRolCelupan") AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOMakloonBenang" po_mak_benang
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_benang."fidPOMakloon"
LEFT JOIN ho."trPOMakloonDetail" po_mak_detail ON po_mak_detail."fidPOMakloon" = po_mak."idPOMakloon"
LEFT JOIN "dataMaster"."msMakloonJob" mak_job ON mak_job."idMakloonJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_mak_detail."fidMakloonJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan",
	sum(po_cel."TotalOrder") as "TotalOrderRolCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
	FROM
		ho."trGreigeRcvdMakloonRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdMakloon"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdMakloon" = grg_dtl."idGreigeRcvdMakloon"
LEFT JOIN (
	SELECT
		po_cel_rcvd."fidGreigeRcvd",
		po_cel_rcvd."fidGreigeRcvdDetail",
		po_cel_det."TotalOrder"
	FROM
		ho."trPOCelupanRcvdDetail" po_cel_rcvd
	LEFT JOIN (
		SELECT
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
		FROM
			ho."trPOCelupanDetail" po_det
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		GROUP BY
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = po_cel."fidGreigeRcvdDetail"
GROUP BY
	grg."fidPOMakloon",
	"fidCelupan",
	grg_dtl."fidPOMakloonDetail"
) grg_celupan ON grg_celupan."fidPOMakloon" = po_mak."idPOMakloon"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOMakloonDetail" = po_mak_detail."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_mak_dtl."fidPOMakloon",
	po_mak_dtl."idPOMakloonDetail" as "fidPOMakloonDetail",
	mak."KodeMakloon" AS "KodeMakloonGreige",
	(
		po_mak_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolMakloon",
	(
		(
			po_mak_dtl."BanyakRol" * po_mak_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgMakloon"
FROM
	ho."trPOMakloonDetail" po_mak_dtl
LEFT JOIN ho."trPOMakloon" po_mak ON po_mak."idPOMakloon" = po_mak_dtl."fidPOMakloon"
LEFT JOIN "dataMaster"."msMakloon" mak ON mak."idMakloon" = po_mak."fidMakloon"
LEFT JOIN (
	SELECT
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdMakloon" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdMakloonRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
		FROM
			ho."trGreigeRcvdMakloonRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdMakloon"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_dtl."idGreigeRcvdMakloon" = grg_rcvd_rol."fidGreigeRcvdMakloon"
	GROUP BY
		grg."fidPOMakloon",
		grg_dtl."fidPOMakloonDetail"
) grg ON grg."fidPOMakloon" = po_mak_dtl."fidPOMakloon" and grg."fidPOMakloonDetail" = po_mak_dtl."idPOMakloonDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp1".*, "xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp1"
LEFT JOIN "xTmp2" ON "xTmp1"."idBenangRcvdDetail" = "xTmp2"."fidBenangRcvdDetail"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOMakloon" = "xTmp3"."fidPOMakloon" AND "xTmp2"."idPOMakloonDetail" = "xTmp3"."fidPOMakloonDetail"
LEFT JOIN "xTmp4" ON 
	"xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail";

DROP TABLE "xTmp1",
 "xTmp2",
 "xTmp3",
 "xTmp4";

END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for f_LaporanSisa_Greige
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."f_LaporanSisa_Greige"();
CREATE OR REPLACE FUNCTION "public"."f_LaporanSisa_Greige"()
  RETURNS TABLE("idPOGreige" int4, "NoPOGreige" varchar, "TglPOGreige" date, "NamaBahan" varchar, "UkuranKain" varchar, "idGreigeJob" int4, "Deskripsi" varchar, "idPOGreigeDetail" int4, "TotalRolGreige" numeric, "TotalKgGreige" numeric, "idGreigeRcvd" int4, "idGreigeRcvdDetail" int4, "NoSJGreige" varchar, "TotalTerimaRolGreige" int8, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" varchar, "idGreigeRcvdTrans" int4, "idGreigeRcvdDetailTrans" int4, "NoSJRcvdGreigeTrans" varchar, "TotalTerimaRolGreigeTrans" int8, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOGreige" int2, "fidPOGreigeDetail" int4, "KodeSupplierGreige" varchar, "NamaSupplierGreige" varchar, "TotalSisaRolGreige" numeric, "TotalSisaKgGreige" numeric, "fidGreigeRcvd" int2, "fidGreigeRcvdDetail" int2, "TglPO-C" date, "PO-C" varchar, "idCelupanJob" int4, "DeskripsiJobCelupan" varchar, "WarnaCelupan" varchar, "TotalOrderCelupan" numeric, "AsalSJ-G" varchar, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric) AS $BODY$
BEGIN
CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_grg."idPOGreige",
	po_grg."NoPO" AS "NoPOGreige",
	po_grg."Tanggal" as "TglPOGreige",
	bh."NamaBahan",
	po_grg_detail."UkuranKain",
	grg_job."idGreigeJob",
	grg_job."Deskripsi",
	po_grg_detail."idPOGreigeDetail",
	po_grg_detail."BanyakRol" AS "TotalRolGreige",
	(
		po_grg_detail."BanyakRol" * po_grg_detail."BeratPerRol"
	) AS "TotalKgGreige",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdSup" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-COALESCE(po_celupan."TotalOrderRolCelupan",0)-COALESCE(po_celupan_trans."TotalOrderRolCelupanTrans",0)) AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOGreige" po_grg
LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."fidPOGreige" = po_grg."idPOGreige"
LEFT JOIN "dataMaster"."msBahan" bh ON po_grg_detail."fidBahan" = bh."idBahan"
LEFT JOIN "dataMaster"."msGreigeJob" grg_job ON grg_job."idGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."idGreigeRcvd" = trans_g."fidGreigeRcvd"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
GROUP BY
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail"
) grg_celupan ON grg_celupan."fidPOGreige" = po_grg."idPOGreige"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg."fidCelupan",
		grg_dtl."fidPOGreigeDetail",
		SUM (po_cel."TotalOrder") AS "TotalOrderRolCelupan"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."idPOGreigeDetail" = grg_dtl."fidPOGreigeDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrder"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd"
	AND po_cel."fidGreigeRcvdDetail" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg."fidCelupan",
		grg_dtl."fidPOGreigeDetail"
) po_celupan ON po_celupan."fidPOGreige" = po_grg."idPOGreige"
AND po_celupan."fidCelupan" = grg_rcvd."fidCelupan"
AND po_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN (
	SELECT
		grg."idGreigeRcvd",
		grg_dtl."fidPOGreigeDetail",
		grg_dtl."idGreigeRcvdSup",
		SUM (
			po_cel_trans."TotalOrderTrans"
		) AS "TotalOrderRolCelupanTrans"
	FROM
		ho."trGreigeRcvdSup" grg_dtl
	LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
	LEFT JOIN ho."trGreigeRcvd" grg ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
	LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
	LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."fidGreigeTransfer" = trans_g."idGreigeTransfer"
	LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
	LEFT JOIN (
		SELECT
			po_cel_rcvd."fidGreigeRcvd",
			po_cel_rcvd."fidGreigeRcvdDetail",
			po_cel_det."TotalOrderTrans"
		FROM
			ho."trPOCelupanRcvdDetail" po_cel_rcvd
		LEFT JOIN (
			SELECT
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan",
				COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrderTrans"
			FROM
				ho."trPOCelupanDetail" po_det
			LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
			GROUP BY
				"fidPOCelupanRcvdDetail",
				wrn_det."KodeCelupan",
				wrn_det."WarnaCelupan"
		) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
	) po_cel_trans ON po_cel_trans."fidGreigeRcvd" = grg_trans."idGreigeRcvd"
	AND grg_trans_dtl."idGreigeRcvdTransfer" = po_cel_trans."fidGreigeRcvdDetail"
	GROUP BY
		grg."idGreigeRcvd",
		grg_dtl."fidPOGreigeDetail",
		grg_dtl."idGreigeRcvdSup"
) po_celupan_trans ON po_celupan_trans."idGreigeRcvd" = grg_rcvd."idGreigeRcvd"
AND po_celupan_trans."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
AND po_celupan_trans."idGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_grg_dtl."fidPOGreige",
	po_grg_dtl."idPOGreigeDetail" as "fidPOGreigeDetail",
	ms_sup."KodeSupplier" AS "KodeSupplierGreige",
	ms_sup."NamaSupplier" AS "NamaSupplierGreige",
	(
		po_grg_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolGreige",
	(
		(
			po_grg_dtl."BanyakRol" * po_grg_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgGreige"
FROM
	ho."trPOGreigeDetail" po_grg_dtl
LEFT JOIN ho."trPOGreige" po_grg ON po_grg."idPOGreige" = po_grg_dtl."fidPOGreige"
LEFT JOIN "dataMaster"."msSupGreige" ms_sup ON ms_sup."idSupGreige" = po_grg."fidSupGreige"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
		FROM
			ho."trGreigeRcvdSupRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail"
) grg ON grg."fidPOGreige" = po_grg_dtl."fidPOGreige" AND grg."fidPOGreigeDetail" = po_grg_dtl."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		WHERE
		(rcvd_det."fidReturKainRcvdDetail" is null OR rcvd_det."fidReturKainRcvdDetail" = 0)
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp2"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOGreige" = "xTmp3"."fidPOGreige" AND "xTmp2"."idPOGreigeDetail" = "xTmp3"."fidPOGreigeDetail"
LEFT JOIN "xTmp4" ON 
	("xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail") OR ("xTmp2"."idGreigeRcvdTrans" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetailTrans" = "xTmp4"."fidGreigeRcvdDetail");

DROP TABLE "xTmp2",
 "xTmp3",
 "xTmp4";
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for f_LaporanSisa_Greige_backup
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."f_LaporanSisa_Greige_backup"();
CREATE OR REPLACE FUNCTION "public"."f_LaporanSisa_Greige_backup"()
  RETURNS TABLE("idPOGreige" int4, "NoPOGreige" varchar, "TglPOGreige" date, "NamaBahan" varchar, "UkuranKain" varchar, "idGreigeJob" int4, "Deskripsi" varchar, "idPOGreigeDetail" int4, "TotalRolGreige" numeric, "TotalKgGreige" numeric, "idGreigeRcvd" int4, "idGreigeRcvdDetail" int4, "NoSJGreige" varchar, "TotalTerimaRolGreige" int8, "TotalTerimaKgGreige" numeric, "diTerimaDiCelupan" varchar, "idGreigeRcvdTrans" int4, "idGreigeRcvdDetailTrans" int4, "NoSJRcvdGreigeTrans" varchar, "TotalTerimaRolGreigeTrans" int8, "TotalTerimaKgGreigeTrans" numeric, "diTerimaDiCelupanTrans" text, "TotalAllTerimaRolGreige" numeric, "TotalAllTerimaKgGreige" numeric, "sisaGreigeRolDiCelupan" numeric, "fidPOGreige" int2, "fidPOGreigeDetail" int4, "KodeSupplierGreige" varchar, "NamaSupplierGreige" varchar, "TotalSisaRolGreige" numeric, "TotalSisaKgGreige" numeric, "fidGreigeRcvd" int2, "fidGreigeRcvdDetail" int2, "TglPO-C" date, "PO-C" varchar, "idCelupanJob" int4, "DeskripsiJobCelupan" varchar, "WarnaCelupan" varchar, "TotalOrderCelupan" numeric, "AsalSJ-G" varchar, "TotalTerimaKain" numeric, "SisaBelumTerima" numeric) AS $BODY$
BEGIN
CREATE TEMPORARY TABLE "xTmp2" AS SELECT
	po_grg."idPOGreige",
	po_grg."NoPO" AS "NoPOGreige",
	po_grg."Tanggal" as "TglPOGreige",
	bh."NamaBahan",
	po_grg_detail."UkuranKain",
	grg_job."idGreigeJob",
	grg_job."Deskripsi",
	po_grg_detail."idPOGreigeDetail",
	po_grg_detail."BanyakRol" AS "TotalRolGreige",
	(
		po_grg_detail."BanyakRol" * po_grg_detail."BeratPerRol"
	) AS "TotalKgGreige",
	grg_rcvd."idGreigeRcvd",
	grg_dtl."idGreigeRcvdSup" as "idGreigeRcvdDetail",
	grg_rcvd."NoSJ" as "NoSJGreige",
	grg_rcvd_rol."TotalTerimaRolGreige",
	grg_rcvd_rol."TotalTerimaKgGreige",
	ms_cel."KodeCelupan" AS "diTerimaDiCelupan",
	grg_trans."idGreigeRcvd" AS "idGreigeRcvdTrans",
	grg_trans_dtl."idGreigeRcvdTransfer" as "idGreigeRcvdDetailTrans",
	grg_trans."NoSJ" AS "NoSJRcvdGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaRolGreige" AS "TotalTerimaRolGreigeTrans",
	grg_rcvd_trans_rol."TotalTerimaKgGreige" AS "TotalTerimaKgGreigeTrans",
	ms_cel_asal."KodeCelupan" || ' -> ' || ms_cel_tujuan."KodeCelupan" AS "diTerimaDiCelupanTrans",
	grg_celupan."TotalTerimaRolGreige" as "TotalAllTerimaRolGreige",
	grg_celupan."TotalTerimaKgGreigeCelupan" as "TotalAllTerimaKgGreige",
	(grg_celupan."TotalTerimaRolGreige"-grg_celupan."TotalOrderRolCelupan") AS "sisaGreigeRolDiCelupan"
FROM
	ho."trPOGreige" po_grg
LEFT JOIN ho."trPOGreigeDetail" po_grg_detail ON po_grg_detail."fidPOGreige" = po_grg."idPOGreige"
LEFT JOIN "dataMaster"."msBahan" bh ON po_grg_detail."fidBahan" = bh."idBahan"
LEFT JOIN "dataMaster"."msGreigeJob" grg_job ON grg_job."idGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail"
LEFT JOIN ho."trGreigeRcvd" grg_rcvd ON grg_rcvd."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN "dataMaster"."msCelupan" ms_cel ON ms_cel."idCelupan" = grg_rcvd."fidCelupan"
LEFT JOIN ho."trGreigeTransfer" trans_g ON trans_g."fidGreigeRcvd" = grg_rcvd."idGreigeRcvd"
LEFT JOIN ho."trGreigeTransferDetail" trans_g_dtl ON trans_g_dtl."fidGreigeTransfer" = trans_g."idGreigeTransfer" AND trans_g_dtl."fidGreigeJob" = po_grg_detail."fidGreigeJob"
LEFT JOIN ho."trGreigeRcvd" grg_trans ON grg_trans."idGreigeRcvd" = trans_g."fidGreigeRcvd"
LEFT JOIN ho."trGreigeRcvdTransfer" grg_trans_dtl ON grg_trans_dtl."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_trans_dtl."fidGreigeTransferDetail" = trans_g_dtl."idGreigeTransferDetail"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdTransferRol") AS "TotalTerimaRolGreige",
		SUM ("Berat") AS "TotalTerimaKgGreige",
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
	FROM
		ho."trGreigeRcvdTransferRol" a
	GROUP BY
		a."fidGreigeRcvd",
		a."fidGreigeRcvdTransfer"
) grg_rcvd_trans_rol ON grg_rcvd_trans_rol."fidGreigeRcvd" = grg_trans."idGreigeRcvd" AND grg_rcvd_trans_rol."fidGreigeRcvdTransfer" = grg_trans_dtl."idGreigeRcvdTransfer"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_asal ON ms_cel_asal."idCelupan" = trans_g."fidCelupanAsal"
LEFT JOIN "dataMaster"."msCelupan" ms_cel_tujuan ON ms_cel_tujuan."idCelupan" = trans_g."fidCelupanTujuan"
LEFT JOIN (
	SELECT
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail",
	SUM (
		grg_rcvd_rol."TerimaRolGreige"
	) AS "TotalTerimaRolGreige",
	SUM (
		grg_rcvd_rol."TerimaKgGreige"
	) AS "TotalTerimaKgGreigeCelupan",
	sum(po_cel."TotalOrder") as "TotalOrderRolCelupan"
FROM
	ho."trGreigeRcvd" grg
LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg."idGreigeRcvd" = grg_dtl."fidGreigeRcvd"
LEFT JOIN (
	SELECT
		COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
		SUM ("Berat") AS "TerimaKgGreige",
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
	FROM
		ho."trGreigeRcvdSupRol" A
	GROUP BY
		A ."fidGreigeRcvd",
		a."fidGreigeRcvdSup"
) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
LEFT JOIN (
	SELECT
		po_cel_rcvd."fidGreigeRcvd",
		po_cel_rcvd."fidGreigeRcvdDetail",
		po_cel_det."TotalOrder"
	FROM
		ho."trPOCelupanRcvdDetail" po_cel_rcvd
	LEFT JOIN (
		SELECT
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
		FROM
			ho."trPOCelupanDetail" po_det
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		GROUP BY
			"fidPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
) po_cel ON po_cel."fidGreigeRcvd" = grg."idGreigeRcvd" AND po_cel."fidGreigeRcvdDetail" = grg_dtl."idGreigeRcvdSup"
GROUP BY
	grg."fidPOGreige",
	"fidCelupan",
	grg_dtl."fidPOGreigeDetail"
) grg_celupan ON grg_celupan."fidPOGreige" = po_grg."idPOGreige"
AND grg_celupan."fidCelupan" = grg_rcvd."fidCelupan" AND grg_celupan."fidPOGreigeDetail" = po_grg_detail."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp3" AS SELECT
	po_grg_dtl."fidPOGreige",
	po_grg_dtl."idPOGreigeDetail" as "fidPOGreigeDetail",
	ms_sup."KodeSupplier" AS "KodeSupplierGreige",
	ms_sup."NamaSupplier" AS "NamaSupplierGreige",
	(
		po_grg_dtl."BanyakRol" - grg."TotalTerimaRolGreige"
	) AS "TotalSisaRolGreige",
	(
		(
			po_grg_dtl."BanyakRol" * po_grg_dtl."BeratPerRol"
		) - grg."TotalTerimaKgGreige"
	) AS "TotalSisaKgGreige"
FROM
	ho."trPOGreigeDetail" po_grg_dtl
LEFT JOIN ho."trPOGreige" po_grg ON po_grg."idPOGreige" = po_grg_dtl."fidPOGreige"
LEFT JOIN "dataMaster"."msSupGreige" ms_sup ON ms_sup."idSupGreige" = po_grg."fidSupGreige"
LEFT JOIN (
	SELECT
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail",
		SUM (
			grg_rcvd_rol."TerimaRolGreige"
		) AS "TotalTerimaRolGreige",
		SUM (
			grg_rcvd_rol."TerimaKgGreige"
		) AS "TotalTerimaKgGreige"
	FROM
		ho."trGreigeRcvd" grg
	LEFT JOIN ho."trGreigeRcvdSup" grg_dtl ON grg_dtl."fidGreigeRcvd" = grg."idGreigeRcvd"
	LEFT JOIN (
		SELECT
			COUNT ("idGreigeRcvdSupRol") AS "TerimaRolGreige",
			SUM ("Berat") AS "TerimaKgGreige",
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
		FROM
			ho."trGreigeRcvdSupRol" a
		GROUP BY
			a."fidGreigeRcvd",
			a."fidGreigeRcvdSup"
	) grg_rcvd_rol ON grg_rcvd_rol."fidGreigeRcvd" = grg."idGreigeRcvd" AND grg_rcvd_rol."fidGreigeRcvdSup" = grg_dtl."idGreigeRcvdSup"
	GROUP BY
		grg."fidPOGreige",
		grg_dtl."fidPOGreigeDetail"
) grg ON grg."fidPOGreige" = po_grg_dtl."fidPOGreige" AND grg."fidPOGreigeDetail" = po_grg_dtl."idPOGreigeDetail";

CREATE TEMPORARY TABLE "xTmp4" AS SELECT
	po_cel_rcvd."fidGreigeRcvd",
	po_cel_rcvd."fidGreigeRcvdDetail",
	po_cel."Tanggal" AS "TglPO-C",
	po_cel."NoPO" AS "PO-C",
	job."idCelupanJob",
	job."Deskripsi" AS "DeskripsiJobCelupan",
	po_cel_det."WarnaCelupan",
	COALESCE (po_cel_det."TotalOrder", 0) AS "TotalOrderCelupan",
	grg_po_cel."NoSJ" AS "AsalSJ-G",
	COALESCE (rcvd_rol."TotalTerima", 0) AS "TotalTerima",
	(
		COALESCE (po_cel_det."TotalOrder", 0) - COALESCE (rcvd_rol."TotalTerima", 0)
	) AS "SisaBelumTerima"
FROM
	ho."trPOCelupanRcvdDetail" po_cel_rcvd
LEFT JOIN ho."trGreigeRcvd" grg_po_cel ON grg_po_cel."idGreigeRcvd" = po_cel_rcvd."fidGreigeRcvd"
LEFT JOIN ho."trPOCelupan" po_cel ON po_cel."idPOCelupan" = po_cel_rcvd."fidPOCelupan"
LEFT JOIN "dataMaster"."msCelupanJob" "job" ON "job"."idCelupanJob" = "po_cel_rcvd"."fidJob"
LEFT JOIN (
	SELECT
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan",
		COALESCE ("sum"("BanyakRol"), 0) AS "TotalOrder"
	FROM
		ho."trPOCelupanDetail" po_det
	LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
	GROUP BY
		"fidPOCelupanRcvdDetail",
		wrn_det."KodeCelupan",
		wrn_det."WarnaCelupan"
) po_cel_det ON po_cel_det."fidPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
LEFT JOIN (
	SELECT
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan",
		COALESCE (SUM(po_det.total_roll), 0) AS "TotalTerima"
	FROM
		ho."trPOCelupanRcvdDetail" po_rcvd
	LEFT JOIN (
		SELECT
			rcvd_rol."fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan",
			COUNT (
				rcvd_rol."idKainRcvdDetailRol"
			) AS total_roll
		FROM
			gudang."trKainRcvdDetailRol" rcvd_rol
		LEFT JOIN gudang."trKainRcvdDetail" rcvd_det ON rcvd_det."idKainRcvdDetail" = rcvd_rol."fidKainRcvdDetail"
		LEFT JOIN ho."trPOCelupanDetail" po_det ON po_det."idPOCelupanDetail" = rcvd_det."fidPOCelupanDetail"
		LEFT JOIN "dataMaster"."msKodeWarnaCelupan" wrn_det ON po_det."fidKodeCelupan" = wrn_det."idKodeWarnaCelupan"
		LEFT JOIN ho."trPOCelupanRcvdDetail" po_rcvd ON po_rcvd."idPOCelupanRcvdDetail" = po_det."fidPOCelupanRcvdDetail"
		GROUP BY
			"fidKainRcvdDetail",
			rcvd_det."fidPOCelupanDetail",
			po_rcvd."idPOCelupanRcvdDetail",
			wrn_det."KodeCelupan",
			wrn_det."WarnaCelupan"
	) po_det ON po_det."idPOCelupanRcvdDetail" = po_rcvd."idPOCelupanRcvdDetail"
	GROUP BY
		po_rcvd."idPOCelupanRcvdDetail",
		po_det."KodeCelupan",
		po_det."WarnaCelupan"
) rcvd_rol ON rcvd_rol."idPOCelupanRcvdDetail" = po_cel_rcvd."idPOCelupanRcvdDetail"
AND po_cel_det."KodeCelupan" = rcvd_rol."KodeCelupan";

RETURN QUERY SELECT
	"xTmp2".*, "xTmp3".*, "xTmp4".*
FROM
	"xTmp2"
LEFT JOIN "xTmp3" ON "xTmp2"."idPOGreige" = "xTmp3"."fidPOGreige" AND "xTmp2"."idPOGreigeDetail" = "xTmp3"."fidPOGreigeDetail"
LEFT JOIN "xTmp4" ON 
	"xTmp2"."idGreigeRcvd" = "xTmp4"."fidGreigeRcvd" AND "xTmp2"."idGreigeRcvdDetail" = "xTmp4"."fidGreigeRcvdDetail";

DROP TABLE "xTmp2",
 "xTmp3",
 "xTmp4";
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for isdigit
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."isdigit"(text);
CREATE OR REPLACE FUNCTION "public"."isdigit"(text)
  RETURNS "pg_catalog"."bool" AS $BODY$
  select $1 ~ '^(-)?[0-9]+$' as result
  $BODY$
  LANGUAGE sql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for trig_FillStockBenang_Keluar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."trig_FillStockBenang_Keluar"();
CREATE OR REPLACE FUNCTION "public"."trig_FillStockBenang_Keluar"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
	  Declare
					v_idpomakloon INTEGER;
					v_oldstatus integer;
					v_newstatus integer;

		
		Begin
				 IF TG_OP = 'UPDATE' THEN
						 v_idpomakloon := OLD."idPOMakloon";
						 v_oldstatus := OLD."Status";
						 v_newstatus := NEW."Status";

						 if (v_oldstatus < 10 and v_newstatus = 10) Then 
								 Create temporary table tmp1 ("fidBenang"			integer
																		, "fidMakloon"		INTEGER
																		, "Ukuran"				varchar(3)
																		, "NoLot"					varchar(15)
																		, "MerkBenang"		varchar(40)
																		, "Qty_Ball"			decimal(12,3)
																		, "Qty_Kg"				decimal(12,2)
																		, "idPOMaklonBenang"	integer
																		, "idKartuStockBenang" integer 
																		, "Tanggal"        date
																		, "NoPO"					 varchar(50));

								 insert into tmp1
								 select t1."fidBenang"
												, t2."fidMakloon"
												, t1."Ukuran"
												, t1."NoLot"
												, t3."MerkBenang"
												, t1."Qty_Ball"
												, t1."Qty_Kg"
												, t1."idPOMakloonBenang"	
												, t4."idKartuStockBenang"
												, t2."Tanggal"
												, t2."NoPO"
									from ho."trPOMakloonBenang" t1 inner join ho."trPOMakloon" t2 on "idPOMakloon" = "fidPOMakloon"
																								inner JOIN "dataMaster"."msBenang" t3 on "idBenang" = t1."fidBenang"
																								left outer join ho."luKartuStockBenang" t4 
																										on (t1."fidBenang" = t4."fidBenang"
																												and t1."Ukuran" = t4."Ukuran"
																												and t1."NoLot" = t4."NoLot"
																												and t2."fidMakloon" = t4."fidMakloon")
									where "fidPOMakloon" = v_idpomakloon;

									select * from ho."luStockBenang"
									 order by "fidBenang", "Ukuran", "NoLot";
											select * from ho."luKartuStockBenang"
											 order by "TanggalJam", "Tipe";
									
											Update ho."luStockBenang"
												 set "Qty_Ball" = "luStockBenang"."Qty_Ball" - tmp1."Qty_Ball"
													 , "Qty_Kg"   = "luStockBenang"."Qty_Kg" - tmp1."Qty_Kg"
												From tmp1
											 where tmp1."fidBenang" = "luStockBenang"."fidBenang"
															and tmp1."Ukuran" = "luStockBenang"."Ukuran"
															and tmp1."NoLot" = "luStockBenang"."NoLot"
															and tmp1."fidMakloon" = "luStockBenang"."fidMakloon";

											insert into ho."luKartuStockBenang" ("Tipe"
																														, "TanggalJam"
																														, "Deskripsi"
																														, "Qty_Ball"
																														, "Qty_Kg"
																														, "fidBenang"
																														, "Ukuran"
																														, "fidTrx"
																														, "NoLot"
																														, "fidMakloon")
										Select 101
													, "Tanggal"
													, 'USED ' || "NoPO"
													, "Qty_Ball"
													, "Qty_Kg"
													, "fidBenang"
													, "Ukuran"
													, "idPOMaklonBenang"
													, "NoLot"
													, "fidMakloon"
										from tmp1;

									select * from ho."luStockBenang"
									 order by "fidBenang", "Ukuran", "NoLot";
									select * from ho."luKartuStockBenang"
									 order by "TanggalJam", "Tipe";

									drop table tmp1;
							End If;
					End If;
					Return NEW;
		End;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for udf_KartuStockBenang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_KartuStockBenang"("v_idbenang" int4, "v_ukuran" bpchar, "v_pertanggal" date);
CREATE OR REPLACE FUNCTION "public"."udf_KartuStockBenang"("v_idbenang" int4, "v_ukuran" bpchar, "v_pertanggal" date)
  RETURNS TABLE("TanggalJam" bpchar, "Deskripsi" varchar, "QtyMasuk_Ball" numeric, "QtyMasuk_Kg" numeric, "QtyKeluar_Ball" numeric, "QtyKeluar_Kg" numeric, "QtyBalance_Ball" numeric, "QtyBalance_Kg" numeric, "Tipe" int2, "fidMakloon" int2) AS $BODY$	  
	  Declare 
				v_qtyst_ball decimal(12,3);
			  v_qtyst_kg   decimal(12,2);
			  v_qtysum_ball decimal(12,3);
			  v_qtysum_kg   decimal(12,3);
        v_start      timestamp;
				
		Begin
				select z."TanggalJam", z."Qty_Ball", z."Qty_Kg"
					into v_start, v_qtyst_ball, v_qtyst_kg
				  from (Select x."TanggalJam", x."Qty_Ball", x."Qty_Kg"
												, row_number() over (order by x."TanggalJam" Desc) as recno
									from ho."luKartuStockBenang" x
								 where x."TanggalJam" < "v_pertanggal"
											and x."fidBenang" = "v_idbenang"
											and x."Ukuran" = "v_ukuran"
												and x."Tipe" = 0
								) z
				 where z.recno = 1;
				
			  if "v_start" is null Then v_start := '1900-1-1'::date;
				End If;

				If v_qtyst_ball is null then v_qtyst_ball := 0;
				End If;

				If v_qtyst_kg is null then v_qtyst_kg := 0;
				End If;

				Select sum("SumQtyMasuk_Ball"-"SumQtyKeluar_Ball") as "SumQty_Ball"
							, Sum("SumQtyMasuk_Kg"-"SumQtyKeluar_Kg") as "SumQty_Kg"
				into v_qtysum_ball, v_qtysum_kg
				From ( 	Select Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
																Else 0
														 End as "SumQtyMasuk_Ball"
											,	Case When x."Tipe" > 100 Then x."Qty_Ball"
																	Else 0
														 End as "SumQtyKeluar_Ball"
											, Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyMasuk_Kg"
											,	Case When x."Tipe" > 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyKeluar_Kg"
											from ho."luKartuStockBenang" x
								 where x."TanggalJam" >=  v_start
											and x."TanggalJam" < v_pertanggal
											and x."fidBenang" = v_idbenang
											and x."Ukuran" = v_ukuran 
											and x."Tipe" > 0
							) y;

				If v_qtysum_ball is null Then v_qtysum_ball := 0;
				End if;

				If v_qtysum_kg is null then v_qtysum_kg := 0;
				End If;

				Create Temporary table temp_table ("TanggalJam" char(10)
																		, "Deskripsi" Varchar(100)
																		, "QtyMasuk_Ball" decimal(12,3)
																		, "QtyMasuk_Kg" decimal(12,2)
																		, "QtyKeluar_Ball" decimal(12,3)
																		, "QtyKeluar_Kg" decimal(12,2)
																		, "QtyBalance_Ball" decimal(12,3)
																		, "QtyBalance_Kg" decimal(12,2)
																		, "Tipe" int2
																		, "fidMakloon" int2);

		  Insert into temp_table
			Values ('','Stock sebelum tanggal ' || to_char(v_pertanggal,'DD-MM-YYYY')
								, 0
								, 0
								, 0
								, 0
								, v_qtyst_ball + v_qtysum_ball 
								, v_qtyst_kg   + v_qtysum_kg 
								, 0
								, 0);


			  Return Query
			  Select * from temp_table
				Union
        Select Cast(to_char(x."TanggalJam", 'DD-MM-YYYY') as Char(10)) as "TanggalJam"
								, x."Deskripsi"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyMasuk_Ball"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyMasuk_Kg"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyKeluar_Ball"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyKeluar_Kg"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyBalance_Ball"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyBalance_Kg"
								, x."Tipe"
							  , x."fidMakloon"
						from ho."luKartuStockBenang" x 
					where x."TanggalJam" > v_pertanggal
									and x."fidBenang" = "v_idbenang"
									and x."Ukuran" = "v_ukuran"
						order by "TanggalJam", "Tipe" ;

					Drop table temp_table;

		End;
		$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for udf_KartuStockBenangNoLot
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_KartuStockBenangNoLot"("v_idbenang" int4, "v_ukuran" bpchar, "v_nolot" bpchar, "v_pertanggal" date);
CREATE OR REPLACE FUNCTION "public"."udf_KartuStockBenangNoLot"("v_idbenang" int4, "v_ukuran" bpchar, "v_nolot" bpchar, "v_pertanggal" date)
  RETURNS TABLE("TanggalJam" bpchar, "Deskripsi" varchar, "NoLot" varchar, "QtyMasuk_Ball" numeric, "QtyMasuk_Kg" numeric, "QtyKeluar_Ball" numeric, "QtyKeluar_Kg" numeric, "QtyBalance_Ball" numeric, "QtyBalance_Kg" numeric, "Tipe" int2, "fidMakloon" int2) AS $BODY$
	  Declare 
				v_qtyst_ball decimal(12,3);
			  v_qtyst_kg   decimal(12,2);
			  v_qtysum_ball decimal(12,3);
			  v_qtysum_kg   decimal(12,3);
        v_start      timestamp;
				
		Begin
				select z."TanggalJam", z."Qty_Ball", z."Qty_Kg"
					into v_start, v_qtyst_ball, v_qtyst_kg
				  from (Select x."TanggalJam", x."Qty_Ball", x."Qty_Kg"
												, row_number() over (order by x."TanggalJam" Desc) as recno
									from ho."luKartuStockBenang" x
								 where x."TanggalJam" < "v_pertanggal"
											and x."fidBenang" = "v_idbenang"
											and x."Ukuran" = "v_ukuran"
												and x."Tipe" = 0
								) z
				 where z.recno = 1;
				
			  if "v_start" is null Then v_start := '1900-1-1'::date;
				End If;

				If v_qtyst_ball is null then v_qtyst_ball := 0;
				End If;

				If v_qtyst_kg is null then v_qtyst_kg := 0;
				End If;

				Select sum("SumQtyMasuk_Ball"-"SumQtyKeluar_Ball") as "SumQty_Ball"
							, Sum("SumQtyMasuk_Kg"-"SumQtyKeluar_Kg") as "SumQty_Kg"
				into v_qtysum_ball, v_qtysum_kg
				From ( 	Select Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
																Else 0
														 End as "SumQtyMasuk_Ball"
											,	Case When x."Tipe" > 100 Then x."Qty_Ball"
																	Else 0
														 End as "SumQtyKeluar_Ball"
											, Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyMasuk_Kg"
											,	Case When x."Tipe" > 100 Then x."Qty_Kg"
																	Else 0
														 End as "SumQtyKeluar_Kg"
											from ho."luKartuStockBenang" x
								 where x."TanggalJam" >=  v_start
											and x."TanggalJam" < v_pertanggal
											and x."fidBenang" = v_idbenang
											and x."Ukuran" = v_ukuran 
											and x."Tipe" > 0
							) y;

				If v_qtysum_ball is null Then v_qtysum_ball := 0;
				End if;

				If v_qtysum_kg is null then v_qtysum_kg := 0;
				End If;

				Create Temporary table temp_table ("TanggalJam" char(10)
																		, "Deskripsi" Varchar(100)
																		, "NoLot" varchar(15)
																		, "QtyMasuk_Ball" decimal(12,3)
																		, "QtyMasuk_Kg" decimal(12,2)
																		, "QtyKeluar_Ball" decimal(12,3)
																		, "QtyKeluar_Kg" decimal(12,2)
																		, "QtyBalance_Ball" decimal(12,3)
																		, "QtyBalance_Kg" decimal(12,2)
																		, "Tipe" int2
																		, "fidMakloon" int2);

		  Insert into temp_table
			Values ('','Stock sebelum tanggal ' || to_char(v_pertanggal,'DD-MM-YYYY')
								, ''
								, 0
								, 0
								, 0
								, 0
								, v_qtyst_ball + v_qtysum_ball 
								, v_qtyst_kg   + v_qtysum_kg 
								, 0
								, 0);


			  Return Query
			  Select * from temp_table
				Union
        Select Cast(to_char(x."TanggalJam", 'DD-MM-YYYY') as Char(10)) as "TanggalJam"
								, x."Deskripsi"
								, x."NoLot"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyMasuk_Ball"
								, Cast( Case When x."Tipe" > 0 and x."Tipe" < 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyMasuk_Kg"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyKeluar_Ball"
								, Cast( Case When x."Tipe" > 100 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyKeluar_Kg"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Ball"
															 Else 0
													End 
											 as decimal(12,3)) as "QtyBalance_Ball"
								, Cast( Case When x."Tipe" = 0 Then x."Qty_Kg"
															 Else 0
													End 
											 as decimal(12,2)) as "QtyBalance_Kg"
								, x."Tipe"
							  , x."fidMakloon"
						from ho."luKartuStockBenang" x 
					where x."TanggalJam" > v_pertanggal
									and x."fidBenang" = "v_idbenang"
									and x."Ukuran" = "v_ukuran"
						order by "TanggalJam", "Tipe" ;

					Drop table temp_table;

		End;
		$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for udf_SetNoHargaCelupan
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_SetNoHargaCelupan"("v_idcelupan" int4, "v_tanggal" date, "v_fromno" int4, "v_tono" int4);
CREATE OR REPLACE FUNCTION "public"."udf_SetNoHargaCelupan"("v_idcelupan" int4, "v_tanggal" date, "v_fromno" int4, "v_tono" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
DECLARE
    v_idstart integer := 0;

BEGIN
  select "idCelupanHarga"
    into v_idstart
   from "dataMaster"."msCelupanHarga"
   where "fidCelupan" = "v_idcelupan"
				and "PerTanggal" = "v_tanggal"
				and "Nomor" = "v_fromno";

	if v_fromno > v_tono THEN
		BEGIN
			Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "msCelupanHarga"."Nomor" + 1
			 where "fidCelupan" = "v_idcelupan"
						and "PerTanggal" = "v_tanggal"
						and "Nomor" >= "v_tono"
						and "Nomor" < "v_fromno";

	  End;
  ELSEIF v_tono > v_fromno THEN
		BEGIN
			Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "msCelupanHarga"."Nomor" - 1
			 where "fidCelupan" = "v_idcelupan"
						and "PerTanggal" = "v_tanggal"
						and "Nomor" > "v_fromno"
						and "Nomor" <= "v_tono";
 
		End;
  End if ;

	Update "dataMaster"."msCelupanHarga" 
         set "Nomor" = "v_tono"
    where "idCelupanHarga" = "v_idstart";

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for udf_StockBenangMasuk
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_StockBenangMasuk"("v_idbenangrcvd" int4);
CREATE OR REPLACE FUNCTION "public"."udf_StockBenangMasuk"("v_idbenangrcvd" int4)
  RETURNS "pg_catalog"."void" AS $BODY$
		  DECLARE
					v_idmakloon integer;
			Begin
				  select "fidMakloon" into v_idmakloon
						from ho."trBenangRcvd" 
				   where "idBenangRcvd" = v_idbenangrcvd ;

				  insert into ho."luStockBenang" ("fidBenang"
																				, "fidMakloon"
																				, "Ukuran"
																				, "NoLot"
																				, "MerkBenang"
																				, "Qty_Ball"
																				, "Qty_Kg")
				  select rcv."fidBenang"
									, v_idmakloon
									, rcv."Ukuran"
									, rcv."NoLot"
									, mst."MerkBenang"
									, rcv."QtyRcvd_Ball"
									, rcv."QtyRcvd_Kg"
						from ho."trBenangRcvdDetail" rcv inner join "dataMaster"."msBenang" mst
													on mst."idBenang" = rcv."fidBenang"
						where rcv."fidBenangRcvd" = v_idBenangRcvd ;

			End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for udf_ValidasiDelGudang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_ValidasiDelGudang"("v_idgudang" int4);
CREATE OR REPLACE FUNCTION "public"."udf_ValidasiDelGudang"("v_idgudang" int4)
  RETURNS "pg_catalog"."int4" AS $BODY$
DECLARE
    

BEGIN
  Return 0;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for udf_ValidasiDelToko
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."udf_ValidasiDelToko"("v_idtoko" int4);
CREATE OR REPLACE FUNCTION "public"."udf_ValidasiDelToko"("v_idtoko" int4)
  RETURNS "pg_catalog"."int4" AS $BODY$
DECLARE
    

BEGIN
  Return 0;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for uuid_generate_v1
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_generate_v1"();
CREATE OR REPLACE FUNCTION "public"."uuid_generate_v1"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v1'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_generate_v1mc
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_generate_v1mc"();
CREATE OR REPLACE FUNCTION "public"."uuid_generate_v1mc"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v1mc'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_generate_v3
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_generate_v3"("namespace" uuid, "name" text);
CREATE OR REPLACE FUNCTION "public"."uuid_generate_v3"("namespace" uuid, "name" text)
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v3'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_generate_v4
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_generate_v4"();
CREATE OR REPLACE FUNCTION "public"."uuid_generate_v4"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v4'
  LANGUAGE c VOLATILE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_generate_v5
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_generate_v5"("namespace" uuid, "name" text);
CREATE OR REPLACE FUNCTION "public"."uuid_generate_v5"("namespace" uuid, "name" text)
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v5'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_nil
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_nil"();
CREATE OR REPLACE FUNCTION "public"."uuid_nil"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_nil'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_ns_dns
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_ns_dns"();
CREATE OR REPLACE FUNCTION "public"."uuid_ns_dns"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_ns_dns'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_ns_oid
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_ns_oid"();
CREATE OR REPLACE FUNCTION "public"."uuid_ns_oid"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_ns_oid'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_ns_url
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_ns_url"();
CREATE OR REPLACE FUNCTION "public"."uuid_ns_url"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_ns_url'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Function structure for uuid_ns_x500
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."uuid_ns_x500"();
CREATE OR REPLACE FUNCTION "public"."uuid_ns_x500"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_ns_x500'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."AppDashboard_idAppDashboard_seq"
OWNED BY "public"."AppDashboard"."idAppDashboard";
SELECT setval('"public"."AppDashboard_idAppDashboard_seq"', 2, false);
ALTER SEQUENCE "public"."AppMenus_id_app_menu_seq"
OWNED BY "public"."AppMenus"."idAppMenu";
SELECT setval('"public"."AppMenus_id_app_menu_seq"', 118, true);
ALTER SEQUENCE "public"."AppShortKey_idAppShortCut_seq"
OWNED BY "public"."AppShortCut"."idAppShortCut";
SELECT setval('"public"."AppShortKey_idAppShortCut_seq"', 35, true);
ALTER SEQUENCE "public"."fileUpload_idFileUpload_seq"
OWNED BY "public"."fileUpload"."idFileUpload";
SELECT setval('"public"."fileUpload_idFileUpload_seq"', 43, true);
ALTER SEQUENCE "public"."msMasterOperatorSpecial_idMasterOperatorSpecial_seq"
OWNED BY "public"."msOperatorModul"."idOperatorModul";
SELECT setval('"public"."msMasterOperatorSpecial_idMasterOperatorSpecial_seq"', 2, true);
ALTER SEQUENCE "public"."msOperatorAppShortKey_idMsOperatorAppShortKey_seq"
OWNED BY "public"."msOperatorAppShortCut"."idMsOperatorAppShortKey";
SELECT setval('"public"."msOperatorAppShortKey_idMsOperatorAppShortKey_seq"', 2764, true);
ALTER SEQUENCE "public"."msOperatorDashboardPrivilege_idDashboardPrivilege_seq"
OWNED BY "public"."msOperatorDashboardPrivilege"."idDashboardPrivilege";
SELECT setval('"public"."msOperatorDashboardPrivilege_idDashboardPrivilege_seq"', 2, false);
ALTER SEQUENCE "public"."msOperatorGroup_idMsOperatorGroup_seq"
OWNED BY "public"."msOperatorGroup"."idMsOperatorGroup";
SELECT setval('"public"."msOperatorGroup_idMsOperatorGroup_seq"', 7, false);
ALTER SEQUENCE "public"."msOperatorPrivilege_idPrivilege_seq"
OWNED BY "public"."msOperatorPrivilege"."idPrivilege";
SELECT setval('"public"."msOperatorPrivilege_idPrivilege_seq"', 5762, true);
ALTER SEQUENCE "public"."msOperator_idMsOperator_seq"
OWNED BY "public"."msOperator"."idMsOperator";
SELECT setval('"public"."msOperator_idMsOperator_seq"', 68, true);

-- ----------------------------
-- Primary Key structure for table AppDashboard
-- ----------------------------
ALTER TABLE "public"."AppDashboard" ADD CONSTRAINT "AppDashboard_pkey" PRIMARY KEY ("idAppDashboard");

-- ----------------------------
-- Primary Key structure for table AppMenus
-- ----------------------------
ALTER TABLE "public"."AppMenus" ADD CONSTRAINT "AppMenus_pkey" PRIMARY KEY ("idAppMenu");

-- ----------------------------
-- Primary Key structure for table AppShortCut
-- ----------------------------
ALTER TABLE "public"."AppShortCut" ADD CONSTRAINT "AppShortCut_pkey" PRIMARY KEY ("idAppShortCut");

-- ----------------------------
-- Primary Key structure for table fileUpload
-- ----------------------------
ALTER TABLE "public"."fileUpload" ADD CONSTRAINT "fileUpload_pkey" PRIMARY KEY ("idFileUpload");

-- ----------------------------
-- Indexes structure for table karunia_sessions
-- ----------------------------
CREATE INDEX "karunia_sessions_timestamp" ON "public"."karunia_sessions" USING btree (
  "timestamp" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table msOperator
-- ----------------------------
ALTER TABLE "public"."msOperator" ADD CONSTRAINT "msOperator_pkey" PRIMARY KEY ("idMsOperator");

-- ----------------------------
-- Primary Key structure for table msOperatorAppShortCut
-- ----------------------------
ALTER TABLE "public"."msOperatorAppShortCut" ADD CONSTRAINT "msOperatorAppShortCut_pkey" PRIMARY KEY ("idMsOperatorAppShortKey");

-- ----------------------------
-- Primary Key structure for table msOperatorDashboardPrivilege
-- ----------------------------
ALTER TABLE "public"."msOperatorDashboardPrivilege" ADD CONSTRAINT "msOperatorDashboardPrivilege_pkey" PRIMARY KEY ("idDashboardPrivilege");

-- ----------------------------
-- Primary Key structure for table msOperatorModul
-- ----------------------------
ALTER TABLE "public"."msOperatorModul" ADD CONSTRAINT "msMasterOperatorSpecial_pkey" PRIMARY KEY ("idOperatorModul");

-- ----------------------------
-- Primary Key structure for table msOperatorPrivilege
-- ----------------------------
ALTER TABLE "public"."msOperatorPrivilege" ADD CONSTRAINT "msOperatorPrivilege_pkey" PRIMARY KEY ("idPrivilege");

-- ----------------------------
-- Primary Key structure for table msOperatorSpecial
-- ----------------------------
ALTER TABLE "public"."msOperatorSpecial" ADD CONSTRAINT "msOperatorSepecial_pkey" PRIMARY KEY ("idOperatorSpecial");
