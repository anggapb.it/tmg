/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : terminal

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 24/06/2019 10:07:38
*/


-- ----------------------------
-- Sequence structure for trDataLog_idTrDataLog_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "terminal"."trDataLog_idTrDataLog_seq";
CREATE SEQUENCE "terminal"."trDataLog_idTrDataLog_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 90
CACHE 1;

-- ----------------------------
-- Sequence structure for trFileUploadManager_idTrFileUploadManager_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "terminal"."trFileUploadManager_idTrFileUploadManager_seq";
CREATE SEQUENCE "terminal"."trFileUploadManager_idTrFileUploadManager_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tr_log_id_log_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "terminal"."tr_log_id_log_seq";
CREATE SEQUENCE "terminal"."tr_log_id_log_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 5779
CACHE 1;

-- ----------------------------
-- Table structure for trDataLog
-- ----------------------------
DROP TABLE IF EXISTS "terminal"."trDataLog";
CREATE TABLE "terminal"."trDataLog" (
  "idTrDataLog" int4 NOT NULL DEFAULT nextval('"terminal"."trDataLog_idTrDataLog_seq"'::regclass),
  "SchemaName" char(30) COLLATE "pg_catalog"."default",
  "TableName" char(50) COLLATE "pg_catalog"."default",
  "fidData" int4,
  "fidMsOperator" int4,
  "ActionTime" timestamp(6),
  "LogData" text COLLATE "pg_catalog"."default",
  "LogType" char(10) COLLATE "pg_catalog"."default"
)
;
COMMENT ON TABLE "terminal"."trDataLog" IS 'Menyimpan semua perubahan data yang terjadi pada masing2 table';

-- ----------------------------
-- Table structure for tr_log
-- ----------------------------
DROP TABLE IF EXISTS "terminal"."tr_log";
CREATE TABLE "terminal"."tr_log" (
  "id_log" int4 NOT NULL DEFAULT nextval('"terminal".tr_log_id_log_seq'::regclass),
  "value_before" text COLLATE "pg_catalog"."default",
  "action_time" timestamp(6) DEFAULT now(),
  "ip_comp" char(20) COLLATE "pg_catalog"."default",
  "fid_operator" int4,
  "log_type" char(30) COLLATE "pg_catalog"."default",
  "table_name" varchar(100) COLLATE "pg_catalog"."default",
  "fid_data" int4
)
;

-- ----------------------------
-- Records of tr_log
-- ----------------------------
INSERT INTO "terminal"."tr_log" VALUES (9364, '', '2019-05-21 22:53:54.516109', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9365, '', '2019-05-22 08:29:58.382252', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9366, '', '2019-05-22 18:41:52.510069', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9367, '', '2019-05-22 23:44:21.310282', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9368, '', '2019-05-23 19:01:31.3215', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9369, '', '2019-05-23 23:46:04.427931', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9370, '', '2019-05-25 02:10:42.597354', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9371, '', '2019-05-26 07:36:07.556891', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9372, '', '2019-05-26 19:21:24.822898', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9373, '', '2019-05-27 20:42:25.345923', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9374, '', '2019-05-28 19:02:20.907525', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9375, '', '2019-05-28 20:43:21.611156', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9376, '', '2019-06-08 11:34:30.005615', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9377, '', '2019-06-09 07:34:48.496644', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9378, '', '2019-06-11 09:22:55.06079', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9379, '', '2019-06-11 15:05:09.40223', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9380, '', '2019-06-12 09:10:40.978019', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9381, '', '2019-06-13 08:59:05.955028', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9382, '', '2019-06-14 08:36:32.776481', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9383, '', '2019-06-17 09:13:02.273278', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9384, '', '2019-06-17 20:43:23.57575', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9385, '', '2019-06-18 20:26:47.95879', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9386, '', '2019-06-19 20:36:23.715558', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9387, '', '2019-06-20 21:30:16.996385', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9388, '', '2019-06-21 14:05:58.86762', '::1                 ', 1, 'admin login                   ', '', 0);
INSERT INTO "terminal"."tr_log" VALUES (9389, '', '2019-06-23 20:40:06.665692', '::1                 ', 1, 'admin login                   ', '', 0);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"terminal"."trDataLog_idTrDataLog_seq"', 91, true);
SELECT setval('"terminal"."trFileUploadManager_idTrFileUploadManager_seq"', 2, false);
SELECT setval('"terminal"."tr_log_id_log_seq"', 9390, true);

-- ----------------------------
-- Primary Key structure for table trDataLog
-- ----------------------------
ALTER TABLE "terminal"."trDataLog" ADD CONSTRAINT "trDataLog_pkey" PRIMARY KEY ("idTrDataLog");
