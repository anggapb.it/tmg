/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : dataMaster

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 04/07/2019 22:55:32
*/


-- ----------------------------
-- Sequence structure for msCustomer_idCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msCustomer_idCustomer_seq";
CREATE SEQUENCE "dataMaster"."msCustomer_idCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msJenisKas_idJenisKas_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msJenisKas_idJenisKas_seq";
CREATE SEQUENCE "dataMaster"."msJenisKas_idJenisKas_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msJenisKendaraan_idJenisKendaraan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq";
CREATE SEQUENCE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msKelompokBarang_idKelompokBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msKelompokBarang_idKelompokBarang_seq";
CREATE SEQUENCE "dataMaster"."msKelompokBarang_idKelompokBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msKendaraan_idKendaraan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msKendaraan_idKendaraan_seq";
CREATE SEQUENCE "dataMaster"."msKendaraan_idKendaraan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msPengemudi_idPengemudi_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msPengemudi_idPengemudi_seq";
CREATE SEQUENCE "dataMaster"."msPengemudi_idPengemudi_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msSatuan_idSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msSatuan_idSatuan_seq";
CREATE SEQUENCE "dataMaster"."msSatuan_idSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for msSupplier_idSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dataMaster"."msSupplier_idSupplier_seq";
CREATE SEQUENCE "dataMaster"."msSupplier_idSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for msBarang
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msBarang";
CREATE TABLE "dataMaster"."msBarang" (
  "KodeBarang" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "fidKelompokBarang" int4,
  "NamaBarang" varchar(200) COLLATE "pg_catalog"."default",
  "fidSatuanKecil" int4,
  "fidSatuanBesar" int2,
  "HargaBeli" numeric(15),
  "fidSupplier" int4,
  "PhotoBarang" text COLLATE "pg_catalog"."default",
  "Deskripsi" text COLLATE "pg_catalog"."default",
  "TglInput" date,
  "TglUpdate" date,
  "userinput" varchar(100) COLLATE "pg_catalog"."default",
  "userupdate" varchar(100) COLLATE "pg_catalog"."default",
  "Stok" int4,
  "Merk" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msBarang
-- ----------------------------
INSERT INTO "dataMaster"."msBarang" VALUES ('B/0002/VI/2019', 6, 'test2', 5, 6, 20000, 14, 'UWk4d01EQXlMMVpKTHpJd01Uaz0=20190614105333-90x90.jpg', 'test2', '2019-06-14', NULL, 'admin', NULL, NULL, 'test2');
INSERT INTO "dataMaster"."msBarang" VALUES ('B/0003/VI/2019', 7, 'test5', 5, 6, 22500, 13, 'UWk4d01EQXpMMVpKTHpJd01Uaz0=20190614112701-90x90.png', 'test5', '2019-06-14', NULL, 'admin', NULL, NULL, 'test5');
INSERT INTO "dataMaster"."msBarang" VALUES ('B/0004/VI/2019', 5, 'test4', 5, 6, 13000, 13, NULL, 'tests4', '2019-06-14', NULL, 'admin', NULL, NULL, 'test4');
INSERT INTO "dataMaster"."msBarang" VALUES ('B/0001/VI/2019', 7, 'skrup', 5, 6, 25000, 14, 'UWk4d01EQXhMMVpKTHpJd01Uaz0=20190614110713-90x90.jpeg', 'test3', '2019-06-14', '2019-06-14', 'admin', 'admin', NULL, 'test3');

-- ----------------------------
-- Table structure for msConfig
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msConfig";
CREATE TABLE "dataMaster"."msConfig" (
  "idConfig" int2 NOT NULL,
  "Caption" varchar(255) COLLATE "pg_catalog"."default",
  "keyText" varchar(255) COLLATE "pg_catalog"."default",
  "keyValue" int2
)
;

-- ----------------------------
-- Records of msConfig
-- ----------------------------
INSERT INTO "dataMaster"."msConfig" VALUES (4, '<b>Version</b> 1.0.0', 'version', 4);
INSERT INTO "dataMaster"."msConfig" VALUES (8, 'skin-blue', 'app_skin', 8);
INSERT INTO "dataMaster"."msConfig" VALUES (15, '50', 'set_discount_max_persen', 1001);
INSERT INTO "dataMaster"."msConfig" VALUES (16, '2000', 'set_discount_max_rupiah', 1000);
INSERT INTO "dataMaster"."msConfig" VALUES (17, 'Kalau 1 = download, kalau 2= print html', 'print_mode', 15);
INSERT INTO "dataMaster"."msConfig" VALUES (2, 'Copyright © 2019', 'copyright', 2);
INSERT INTO "dataMaster"."msConfig" VALUES (3, '', 'perusahaan', 3);
INSERT INTO "dataMaster"."msConfig" VALUES (9, 'PT. TMG', 'pt_nama', 9);
INSERT INTO "dataMaster"."msConfig" VALUES (10, '', 'pt_addr1', 10);
INSERT INTO "dataMaster"."msConfig" VALUES (11, '', 'pt_addr2', 11);
INSERT INTO "dataMaster"."msConfig" VALUES (12, '', 'pt_addr3', 12);
INSERT INTO "dataMaster"."msConfig" VALUES (13, '', 'ppn_nama', 13);
INSERT INTO "dataMaster"."msConfig" VALUES (14, '', 'nonppn_nama', 14);
INSERT INTO "dataMaster"."msConfig" VALUES (1, 'TMG', 'app_name', 1);
INSERT INTO "dataMaster"."msConfig" VALUES (6, 'assets/images/wallpaper/tmg.png', 'app_cover', 6);
INSERT INTO "dataMaster"."msConfig" VALUES (5, 'TMG', 'app_title', 5);
INSERT INTO "dataMaster"."msConfig" VALUES (7, 'assets/images/icon/website_logo.png', 'app_icon', 7);

-- ----------------------------
-- Table structure for msCustomer
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msCustomer";
CREATE TABLE "dataMaster"."msCustomer" (
  "idCustomer" int4 NOT NULL DEFAULT nextval('"dataMaster"."msCustomer_idCustomer_seq"'::regclass),
  "NamaLengkap" varchar(200) COLLATE "pg_catalog"."default",
  "NamaPanggilan" varchar(100) COLLATE "pg_catalog"."default",
  "NoHP" varchar(20) COLLATE "pg_catalog"."default",
  "Alamat" text COLLATE "pg_catalog"."default",
  "Email" varchar(100) COLLATE "pg_catalog"."default",
  "Institusi" varchar(100) COLLATE "pg_catalog"."default",
  "Negara" varchar(200) COLLATE "pg_catalog"."default",
  "Provinsi" varchar(200) COLLATE "pg_catalog"."default",
  "KotaKabupaten" varchar(200) COLLATE "pg_catalog"."default",
  "Photo" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "dataMaster"."msCustomer"."Institusi" IS 'perorangan atau perusahaan';

-- ----------------------------
-- Records of msCustomer
-- ----------------------------
INSERT INTO "dataMaster"."msCustomer" VALUES (10, 'test', 'test', 'test', '', 'test@example.com', 'test', '', '', '', NULL);

-- ----------------------------
-- Table structure for msGudang
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msGudang";
CREATE TABLE "dataMaster"."msGudang" (
  "idGudang" int4 NOT NULL,
  "KodeGudang" varchar(6) COLLATE "pg_catalog"."default",
  "NamaGudang" varchar(30) COLLATE "pg_catalog"."default",
  "AlamatGudang" varchar(200) COLLATE "pg_catalog"."default",
  "KotaGudang" varchar(30) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msGudang
-- ----------------------------
INSERT INTO "dataMaster"."msGudang" VALUES (1, 'G.BTN', 'GUDANG BATUNUNGGAL', 'JL. BATUNUNGGAL LESTARI NO 42', 'BANDUNG');
INSERT INTO "dataMaster"."msGudang" VALUES (2, 'G.CLP', 'GUDANG CILAMPENI', 'JL. CILAMPENI NO 33', 'KAB BANDUNG');
INSERT INTO "dataMaster"."msGudang" VALUES (100001, 'T.BTN', 'KARUNIA BATUNUNGGAL', 'JL. BATUNUNGGAL INDAH RAYA NO 165', 'BANDUNG');
INSERT INTO "dataMaster"."msGudang" VALUES (100000, 'T.OTS', 'KARUNIA OTISTA', 'JL. OTTO ISKANDARDINATA NO 143A', 'BANDUNG');

-- ----------------------------
-- Table structure for msJenisKas
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msJenisKas";
CREATE TABLE "dataMaster"."msJenisKas" (
  "idJenisKas" int2 NOT NULL DEFAULT nextval('"dataMaster"."msJenisKas_idJenisKas_seq"'::regclass),
  "Nama" varchar(255) COLLATE "pg_catalog"."default",
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msJenisKas
-- ----------------------------
INSERT INTO "dataMaster"."msJenisKas" VALUES (1, 'Kas Masuk', 'Pemasukan');
INSERT INTO "dataMaster"."msJenisKas" VALUES (2, 'Kas Keluar', 'Pengeluaran');

-- ----------------------------
-- Table structure for msJenisKendaraan
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msJenisKendaraan";
CREATE TABLE "dataMaster"."msJenisKendaraan" (
  "idJenisKendaraan" int2 NOT NULL DEFAULT nextval('"dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"'::regclass),
  "NamaJenisKendaraan" varchar(255) COLLATE "pg_catalog"."default",
  "KodeJenisKendaraan" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msJenisKendaraan
-- ----------------------------
INSERT INTO "dataMaster"."msJenisKendaraan" VALUES (4, 'truk', 'truk');
INSERT INTO "dataMaster"."msJenisKendaraan" VALUES (5, 'mobil', 'mobil');

-- ----------------------------
-- Table structure for msKelompokBarang
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msKelompokBarang";
CREATE TABLE "dataMaster"."msKelompokBarang" (
  "idKelompokBarang" int2 NOT NULL DEFAULT nextval('"dataMaster"."msKelompokBarang_idKelompokBarang_seq"'::regclass),
  "KodeKelompokBarang" varchar(10) COLLATE "pg_catalog"."default",
  "NamaKelompokBarang" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msKelompokBarang
-- ----------------------------
INSERT INTO "dataMaster"."msKelompokBarang" VALUES (5, 'test', 'test');
INSERT INTO "dataMaster"."msKelompokBarang" VALUES (6, 'test2', 'test2');
INSERT INTO "dataMaster"."msKelompokBarang" VALUES (7, 'test3', 'test3');

-- ----------------------------
-- Table structure for msKendaraan
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msKendaraan";
CREATE TABLE "dataMaster"."msKendaraan" (
  "idKendaraan" int4 NOT NULL DEFAULT nextval('"dataMaster"."msKendaraan_idKendaraan_seq"'::regclass),
  "PlatNomor" varchar(20) COLLATE "pg_catalog"."default",
  "Merk" varchar(50) COLLATE "pg_catalog"."default",
  "Type" varchar(50) COLLATE "pg_catalog"."default",
  "fidJenisKendaraan" int2,
  "Model" varchar(50) COLLATE "pg_catalog"."default",
  "TahunPembuatan" varchar(4) COLLATE "pg_catalog"."default",
  "Silinder" varchar(50) COLLATE "pg_catalog"."default",
  "NoRangka" varchar(50) COLLATE "pg_catalog"."default",
  "NoMesin" varchar(50) COLLATE "pg_catalog"."default",
  "Warna" varchar(50) COLLATE "pg_catalog"."default",
  "BahanBakar" varchar(50) COLLATE "pg_catalog"."default",
  "WarnaTNKB" varchar(50) COLLATE "pg_catalog"."default",
  "TahunRegistrasi" varchar(4) COLLATE "pg_catalog"."default",
  "NoBPKB" varchar(50) COLLATE "pg_catalog"."default",
  "KodeLokasi" varchar(50) COLLATE "pg_catalog"."default",
  "NoUrutDaftar" varchar(50) COLLATE "pg_catalog"."default",
  "PhotoKendaraan1" text COLLATE "pg_catalog"."default",
  "PhotoKendaraan2" text COLLATE "pg_catalog"."default",
  "PhotoKendaraan3" text COLLATE "pg_catalog"."default",
  "PanjangKaroseri" int4,
  "LebarKaroseri" int4,
  "TinggiKaroseri" int4,
  "Dimensi" int4,
  "BeratKosong" int4,
  "BeratMax" int4,
  "PanjangMobil" int4,
  "LebarMobil" int4,
  "TinggiMobil" int4,
  "KecepatanMax" int4,
  "TenagaMax_" varchar(100) COLLATE "pg_catalog"."default",
  "UkuranBan_" varchar(100) COLLATE "pg_catalog"."default",
  "UkuranRoda_" varchar(100) COLLATE "pg_catalog"."default",
  "KapasitasMuatan_" varchar(100) COLLATE "pg_catalog"."default",
  "NamaKendaraan" varchar(200) COLLATE "pg_catalog"."default",
  "TenagaMax" int4,
  "UkuranBan" int4,
  "UkuranRoda" int4,
  "KapasitasMuatan" int4
)
;
COMMENT ON COLUMN "dataMaster"."msKendaraan"."Silinder" IS 'isi silinder';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."NoRangka" IS 'nomor rangka';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."NoMesin" IS 'nomor mesin';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."KodeLokasi" IS 'kode lokasi';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."PanjangKaroseri" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."LebarKaroseri" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."TinggiKaroseri" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."Dimensi" IS 'cbm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."BeratKosong" IS 'kg';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."BeratMax" IS 'kg';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."PanjangMobil" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."LebarMobil" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."TinggiMobil" IS 'cm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."KecepatanMax" IS 'km/jam';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."TenagaMax_" IS 'ps/rpm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."KapasitasMuatan_" IS 'kg/cbm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."TenagaMax" IS 'ps/rpm';
COMMENT ON COLUMN "dataMaster"."msKendaraan"."KapasitasMuatan" IS 'kg/cbm';

-- ----------------------------
-- Records of msKendaraan
-- ----------------------------
INSERT INTO "dataMaster"."msKendaraan" VALUES (6, 'Ba001', '', '', 4, '', '', '', '', '', '', '', '', '', 'test', '', '', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'test', 0, 0, 0, 0);
INSERT INTO "dataMaster"."msKendaraan" VALUES (7, 'Ba002', '', '', 5, '', '', '', '', '', '', '', '', '', 'test', '', '', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'test', 0, 0, 0, 0);

-- ----------------------------
-- Table structure for msPengemudi
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msPengemudi";
CREATE TABLE "dataMaster"."msPengemudi" (
  "idPengemudi" int4 NOT NULL DEFAULT nextval('"dataMaster"."msPengemudi_idPengemudi_seq"'::regclass),
  "NamaLengkap" varchar(50) COLLATE "pg_catalog"."default",
  "NamaPanggilan" varchar(50) COLLATE "pg_catalog"."default",
  "NoHP" varchar(20) COLLATE "pg_catalog"."default",
  "Alamat" text COLLATE "pg_catalog"."default",
  "Email" varchar(100) COLLATE "pg_catalog"."default",
  "NIK" varchar(100) COLLATE "pg_catalog"."default",
  "TglLahir" date,
  "GolDarah" varchar(2) COLLATE "pg_catalog"."default",
  "PhotoProfile" text COLLATE "pg_catalog"."default",
  "PhotoKTP" text COLLATE "pg_catalog"."default",
  "PhotoSIM_A" text COLLATE "pg_catalog"."default",
  "PhotoSIM_B1" text COLLATE "pg_catalog"."default",
  "PhotoSIM_B2" text COLLATE "pg_catalog"."default",
  "PunyaSIM_A" int2,
  "PunyaSIM_B1" int2,
  "PunyaSIM_B2" int2,
  "Status" int2,
  "fidMsReligion" int2
)
;
COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_A" IS '0 = tidak , 1 = ya';
COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_B1" IS '0 = tidak , 1 = ya';
COMMENT ON COLUMN "dataMaster"."msPengemudi"."PunyaSIM_B2" IS '0 = tidak , 1 = ya';
COMMENT ON COLUMN "dataMaster"."msPengemudi"."Status" IS '10 = available 20 = not available 30 = on the way';

-- ----------------------------
-- Records of msPengemudi
-- ----------------------------
INSERT INTO "dataMaster"."msPengemudi" VALUES (7, 'test', 'test', '12313', 'test', 'test', 'test', '2019-05-27', 'O', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 10, 1);

-- ----------------------------
-- Table structure for msSatuan
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msSatuan";
CREATE TABLE "dataMaster"."msSatuan" (
  "idSatuan" int2 NOT NULL DEFAULT nextval('"dataMaster"."msSatuan_idSatuan_seq"'::regclass),
  "NamaSatuan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msSatuan
-- ----------------------------
INSERT INTO "dataMaster"."msSatuan" VALUES (5, 'kecil');
INSERT INTO "dataMaster"."msSatuan" VALUES (6, 'besar');

-- ----------------------------
-- Table structure for msStatusOrder
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msStatusOrder";
CREATE TABLE "dataMaster"."msStatusOrder" (
  "idStatusOrder" int4 NOT NULL,
  "NamaStatusOrder" varchar(255) COLLATE "pg_catalog"."default",
  "Colour" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msStatusOrder
-- ----------------------------
INSERT INTO "dataMaster"."msStatusOrder" VALUES (10, 'Data Baru', 'black');
INSERT INTO "dataMaster"."msStatusOrder" VALUES (20, 'Approved', 'blue');
INSERT INTO "dataMaster"."msStatusOrder" VALUES (30, 'Reject', 'red');
INSERT INTO "dataMaster"."msStatusOrder" VALUES (50, 'Done', 'green');

-- ----------------------------
-- Table structure for msStatusPO
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msStatusPO";
CREATE TABLE "dataMaster"."msStatusPO" (
  "idStatusPO" int4 NOT NULL,
  "NamaStatusPO" varchar(255) COLLATE "pg_catalog"."default",
  "Colour" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msStatusPO
-- ----------------------------
INSERT INTO "dataMaster"."msStatusPO" VALUES (10, 'Data Baru', 'black');
INSERT INTO "dataMaster"."msStatusPO" VALUES (50, 'Done', 'green');

-- ----------------------------
-- Table structure for msSupplier
-- ----------------------------
DROP TABLE IF EXISTS "dataMaster"."msSupplier";
CREATE TABLE "dataMaster"."msSupplier" (
  "idSupplier" int2 NOT NULL DEFAULT nextval('"dataMaster"."msSupplier_idSupplier_seq"'::regclass),
  "KodeSupplier" varchar(10) COLLATE "pg_catalog"."default",
  "NamaSupplier" varchar(40) COLLATE "pg_catalog"."default",
  "AlamatSupplier" varchar(200) COLLATE "pg_catalog"."default",
  "KotaSupplier" varchar(40) COLLATE "pg_catalog"."default",
  "Fax" varchar(20) COLLATE "pg_catalog"."default",
  "Alamat2Supplier" varchar(255) COLLATE "pg_catalog"."default",
  "Alamat3Supplier" varchar(255) COLLATE "pg_catalog"."default",
  "Telepon" varchar(20) COLLATE "pg_catalog"."default",
  "Telepon2" varchar(20) COLLATE "pg_catalog"."default",
  "Telepon3" varchar(20) COLLATE "pg_catalog"."default",
  "Fax2" varchar(20) COLLATE "pg_catalog"."default",
  "Email" varchar(40) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msSupplier
-- ----------------------------
INSERT INTO "dataMaster"."msSupplier" VALUES (13, 'test', 'test', 'test', 'test', '', 'test', 'test', '', '', '', '', '');
INSERT INTO "dataMaster"."msSupplier" VALUES (14, 'test2', 'test2', 'test2', 'test2', '', 'test2', 'test2', '', '', '', '', '');

-- ----------------------------
-- Function structure for trig_FillNoUrutQC
-- ----------------------------
DROP FUNCTION IF EXISTS "dataMaster"."trig_FillNoUrutQC"();
CREATE OR REPLACE FUNCTION "dataMaster"."trig_FillNoUrutQC"()
  RETURNS "pg_catalog"."trigger" AS $BODY$

Declare 
    v_idkain integer;
    v_start  integer;
    v_end    integer;
	  v_counter integer;
    v_idbahan integer;

BEGIN
    v_idkain  := NEW."idKain";
		v_idbahan := NEW."fidBahan";
		v_start   := NEW."StartNoUrut";
    v_end     := NEW."EndNoUrut";
		
		if v_end is null or v_start is NULL Then
		     Select "StartNoUrut", "EndNoUrut"
           into v_start, v_end
				   from "dataMaster"."msBahan"
					where "idBahan" = v_idbahan;

					NEW."StartNoUrut" := v_start;
					NEW."EndNoUrut" 	:= v_end;
		End If;

		Delete from "dataMaster"."msNoUrutQC"
		 Where "fidKain" = v_idkain
						and "NoUrut" < 1000
					  and ("NoUrut" < v_start or "NoUrut" > v_end);

		create temporary table tmp1 ("NoUrut" integer);

		v_counter := v_start;

		Loop
		Exit When v_counter > v_end ;
			 Insert into tmp1 Values (v_counter);
			 v_counter := v_counter + 1;
		End Loop;

		Insert into "dataMaster"."msNoUrutQC"
								("fidKain", "NoUrut", "fidQCRol")
		Select v_idkain
						, tmp1."NoUrut"
						, null
		 from tmp1 left outer join (Select "NoUrut" from "dataMaster"."msNoUrutQC" where "fidKain" = v_idkain) x
								on tmp1."NoUrut" = x."NoUrut"
		where x."NoUrut" is null;

		Drop table tmp1;

	  Return NEW;

End;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for trig_UpdateMSKain
-- ----------------------------
DROP FUNCTION IF EXISTS "dataMaster"."trig_UpdateMSKain"();
CREATE OR REPLACE FUNCTION "dataMaster"."trig_UpdateMSKain"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
    If TG_OP ='UPDATE' Then
				Update "dataMaster"."msKain"
					 set "StartNoUrut" = NEW."StartNoUrut"
							,"EndNoUrut" = NEW."EndNoUrut"
				 where "fidBahan" = NEW."idBahan";
		END IF ;
		Return NEW;

End;


$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "dataMaster"."msCustomer_idCustomer_seq"
OWNED BY "dataMaster"."msCustomer"."idCustomer";
SELECT setval('"dataMaster"."msCustomer_idCustomer_seq"', 11, true);
ALTER SEQUENCE "dataMaster"."msJenisKas_idJenisKas_seq"
OWNED BY "dataMaster"."msJenisKas"."idJenisKas";
SELECT setval('"dataMaster"."msJenisKas_idJenisKas_seq"', 7, true);
ALTER SEQUENCE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"
OWNED BY "dataMaster"."msJenisKendaraan"."idJenisKendaraan";
SELECT setval('"dataMaster"."msJenisKendaraan_idJenisKendaraan_seq"', 5, true);
ALTER SEQUENCE "dataMaster"."msKelompokBarang_idKelompokBarang_seq"
OWNED BY "dataMaster"."msKelompokBarang"."idKelompokBarang";
SELECT setval('"dataMaster"."msKelompokBarang_idKelompokBarang_seq"', 8, true);
ALTER SEQUENCE "dataMaster"."msKendaraan_idKendaraan_seq"
OWNED BY "dataMaster"."msKendaraan"."idKendaraan";
SELECT setval('"dataMaster"."msKendaraan_idKendaraan_seq"', 7, true);
ALTER SEQUENCE "dataMaster"."msPengemudi_idPengemudi_seq"
OWNED BY "dataMaster"."msPengemudi"."idPengemudi";
SELECT setval('"dataMaster"."msPengemudi_idPengemudi_seq"', 8, true);
ALTER SEQUENCE "dataMaster"."msSatuan_idSatuan_seq"
OWNED BY "dataMaster"."msSatuan"."idSatuan";
SELECT setval('"dataMaster"."msSatuan_idSatuan_seq"', 7, true);
ALTER SEQUENCE "dataMaster"."msSupplier_idSupplier_seq"
OWNED BY "dataMaster"."msSupplier"."idSupplier";
SELECT setval('"dataMaster"."msSupplier_idSupplier_seq"', 15, true);

-- ----------------------------
-- Primary Key structure for table msBarang
-- ----------------------------
ALTER TABLE "dataMaster"."msBarang" ADD CONSTRAINT "msBarang_pkey" PRIMARY KEY ("KodeBarang");

-- ----------------------------
-- Primary Key structure for table msConfig
-- ----------------------------
ALTER TABLE "dataMaster"."msConfig" ADD CONSTRAINT "msConfig_pkey" PRIMARY KEY ("idConfig");

-- ----------------------------
-- Primary Key structure for table msCustomer
-- ----------------------------
ALTER TABLE "dataMaster"."msCustomer" ADD CONSTRAINT "msCustomer_pkey" PRIMARY KEY ("idCustomer");

-- ----------------------------
-- Primary Key structure for table msGudang
-- ----------------------------
ALTER TABLE "dataMaster"."msGudang" ADD CONSTRAINT "msGudang_pkey" PRIMARY KEY ("idGudang");

-- ----------------------------
-- Primary Key structure for table msJenisKas
-- ----------------------------
ALTER TABLE "dataMaster"."msJenisKas" ADD CONSTRAINT "msJenisKas_pkey" PRIMARY KEY ("idJenisKas");

-- ----------------------------
-- Primary Key structure for table msJenisKendaraan
-- ----------------------------
ALTER TABLE "dataMaster"."msJenisKendaraan" ADD CONSTRAINT "msJenisKendaraan_pkey" PRIMARY KEY ("idJenisKendaraan");

-- ----------------------------
-- Primary Key structure for table msKelompokBarang
-- ----------------------------
ALTER TABLE "dataMaster"."msKelompokBarang" ADD CONSTRAINT "msKelompokBarang_pkey" PRIMARY KEY ("idKelompokBarang");

-- ----------------------------
-- Primary Key structure for table msKendaraan
-- ----------------------------
ALTER TABLE "dataMaster"."msKendaraan" ADD CONSTRAINT "msKendaraan_pkey" PRIMARY KEY ("idKendaraan");

-- ----------------------------
-- Primary Key structure for table msPengemudi
-- ----------------------------
ALTER TABLE "dataMaster"."msPengemudi" ADD CONSTRAINT "msPengemudi_pkey" PRIMARY KEY ("idPengemudi");

-- ----------------------------
-- Primary Key structure for table msSatuan
-- ----------------------------
ALTER TABLE "dataMaster"."msSatuan" ADD CONSTRAINT "msSatuan_pkey" PRIMARY KEY ("idSatuan");

-- ----------------------------
-- Primary Key structure for table msStatusOrder
-- ----------------------------
ALTER TABLE "dataMaster"."msStatusOrder" ADD CONSTRAINT "msStatusOrder_pkey" PRIMARY KEY ("idStatusOrder");

-- ----------------------------
-- Primary Key structure for table msStatusPO
-- ----------------------------
ALTER TABLE "dataMaster"."msStatusPO" ADD CONSTRAINT "msStatusOrder_copy1_pkey" PRIMARY KEY ("idStatusPO");

-- ----------------------------
-- Primary Key structure for table msSupplier
-- ----------------------------
ALTER TABLE "dataMaster"."msSupplier" ADD CONSTRAINT "msSupplier_pkey1" PRIMARY KEY ("idSupplier");

-- ----------------------------
-- Foreign Keys structure for table msBarang
-- ----------------------------
ALTER TABLE "dataMaster"."msBarang" ADD CONSTRAINT "fk_satuanbsr" FOREIGN KEY ("fidSatuanBesar") REFERENCES "dataMaster"."msSatuan" ("idSatuan") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dataMaster"."msBarang" ADD CONSTRAINT "fk_satuankcl" FOREIGN KEY ("fidSatuanKecil") REFERENCES "dataMaster"."msSatuan" ("idSatuan") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dataMaster"."msBarang" ADD CONSTRAINT "fk_supplier" FOREIGN KEY ("fidSupplier") REFERENCES "dataMaster"."msSupplier" ("idSupplier") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table msKendaraan
-- ----------------------------
ALTER TABLE "dataMaster"."msKendaraan" ADD CONSTRAINT "fk_jnskendaraan" FOREIGN KEY ("fidJenisKendaraan") REFERENCES "dataMaster"."msJenisKendaraan" ("idJenisKendaraan") ON DELETE CASCADE ON UPDATE CASCADE;
