/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : ho

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 04/07/2019 22:55:51
*/


-- ----------------------------
-- Sequence structure for trOrderDet_idOrderDet_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ho"."trOrderDet_idOrderDet_seq";
CREATE SEQUENCE "ho"."trOrderDet_idOrderDet_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for trPODetail_idPODetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ho"."trPODetail_idPODetail_seq";
CREATE SEQUENCE "ho"."trPODetail_idPODetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for trPO_idPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ho"."trPO_idPO_seq";
CREATE SEQUENCE "ho"."trPO_idPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for trOrder
-- ----------------------------
DROP TABLE IF EXISTS "ho"."trOrder";
CREATE TABLE "ho"."trOrder" (
  "NoOrder" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "fidKendaraan" int4,
  "JenisTrip" varchar(100) COLLATE "pg_catalog"."default",
  "DeskripsiBarang" text COLLATE "pg_catalog"."default",
  "Berat" int2,
  "Panjang" int2,
  "Lebar" int2,
  "Tinggi" int2,
  "Total" int2,
  "TglKirim" date,
  "WaktuPengambilan" time(6),
  "LokasiAsal" varchar(200) COLLATE "pg_catalog"."default",
  "LokasiTujuan" varchar(200) COLLATE "pg_catalog"."default",
  "AreaAsal" varchar(200) COLLATE "pg_catalog"."default",
  "AreaTujuan" varchar(200) COLLATE "pg_catalog"."default",
  "DetailLokasiPengambilan" text COLLATE "pg_catalog"."default",
  "DetailLokasiTujuan" text COLLATE "pg_catalog"."default",
  "TipeKawasanAsal" varchar(100) COLLATE "pg_catalog"."default",
  "TipeKawasanTujuan" varchar(100) COLLATE "pg_catalog"."default",
  "InstruksiKhusus" text COLLATE "pg_catalog"."default",
  "Totalharga" float4,
  "PhotoBarang" text COLLATE "pg_catalog"."default",
  "AddPackaging" int2,
  "AddTenagaAngkut" int2,
  "TglInput" date,
  "UserInput" varchar(50) COLLATE "pg_catalog"."default",
  "TglUpdate" date,
  "fidStatusOrder" int2,
  "fidCustomer" int4,
  "fidPengemudi" int4,
  "UserUpdate" varchar(50) COLLATE "pg_catalog"."default",
  "PPH" numeric(5,2),
  "TotalHargaBayar" numeric(15,2),
  "TglOrder" date,
  "PPN" numeric(5,2),
  "NoInvoice" varchar(100) COLLATE "pg_catalog"."default",
  "TglInvoice" date
)
;
COMMENT ON COLUMN "ho"."trOrder"."JenisTrip" IS 'single atau multi trip';
COMMENT ON COLUMN "ho"."trOrder"."Berat" IS 'kg';
COMMENT ON COLUMN "ho"."trOrder"."Panjang" IS 'cm';
COMMENT ON COLUMN "ho"."trOrder"."Lebar" IS 'cm';
COMMENT ON COLUMN "ho"."trOrder"."Tinggi" IS 'cm';
COMMENT ON COLUMN "ho"."trOrder"."Total" IS 'jika diubah jadi cbm / feet';
COMMENT ON COLUMN "ho"."trOrder"."DetailLokasiPengambilan" IS 'optional';
COMMENT ON COLUMN "ho"."trOrder"."DetailLokasiTujuan" IS 'optional';
COMMENT ON COLUMN "ho"."trOrder"."fidStatusOrder" IS '1 = order , 2 = acc , 3 = order selesai';

-- ----------------------------
-- Records of trOrder
-- ----------------------------
INSERT INTO "ho"."trOrder" VALUES ('O/0001/VII/2019', 6, '', '', 0, 0, 0, 0, 0, '2019-07-04', '08:00:00', 'dago', 'jakarta', '', '', '', '', '', '', '', 175000, NULL, 0, 0, '2019-07-04', 'admin', '2019-07-04', 50, 10, 7, 'admin', 2.50, 200375.00, '2019-07-04', 12.00, 'O/0001/VII/2019', '2019-07-04');

-- ----------------------------
-- Table structure for trOrderDetail
-- ----------------------------
DROP TABLE IF EXISTS "ho"."trOrderDetail";
CREATE TABLE "ho"."trOrderDetail" (
  "idOrderDetail" int4 NOT NULL DEFAULT nextval('"ho"."trOrderDet_idOrderDet_seq"'::regclass),
  "NoOrder" varchar(100) COLLATE "pg_catalog"."default",
  "NamaBiaya" varchar(200) COLLATE "pg_catalog"."default",
  "Nominal" float4,
  "fidJenisKas" int2
)
;

-- ----------------------------
-- Records of trOrderDetail
-- ----------------------------
INSERT INTO "ho"."trOrderDetail" VALUES (34, 'O/0001/VII/2019', 'sparepart', 100000, 1);
INSERT INTO "ho"."trOrderDetail" VALUES (35, 'O/0001/VII/2019', 'truk', 50000, 1);
INSERT INTO "ho"."trOrderDetail" VALUES (36, 'O/0001/VII/2019', 'baut', 25000, 2);

-- ----------------------------
-- Table structure for trPO
-- ----------------------------
DROP TABLE IF EXISTS "ho"."trPO";
CREATE TABLE "ho"."trPO" (
  "idPO" int4 NOT NULL DEFAULT nextval('"ho"."trPO_idPO_seq"'::regclass),
  "NoPembelian" varchar(50) COLLATE "pg_catalog"."default",
  "TglPembelian" date,
  "fidKendaraan" int2,
  "fidSupplier" int2,
  "NoFaktur" varchar(50) COLLATE "pg_catalog"."default",
  "TotalBiaya" numeric(12,2),
  "TotalDiskon" numeric(12,2),
  "UserInput" varchar(50) COLLATE "pg_catalog"."default",
  "TglInput" date,
  "UserApproval" varchar(50) COLLATE "pg_catalog"."default",
  "TglApproval" date,
  "TotalBiayaBayar" numeric(12,2),
  "fidStatusPO" int2,
  "UserUpdate" varchar(50) COLLATE "pg_catalog"."default",
  "TglUpdate" date,
  "TotalPPN" numeric(12,2)
)
;

-- ----------------------------
-- Records of trPO
-- ----------------------------
INSERT INTO "ho"."trPO" VALUES (4, 'O/0001/VI/2019', '2019-06-12', 6, 14, 'test123', 10500.00, 525.00, 'admin', '2019-06-12', 'admin', '2019-06-17', 11025.00, 50, 'admin', '2019-06-14', 1050.00);
INSERT INTO "ho"."trPO" VALUES (8, 'P/0003/VI/2019', '2019-06-19', 6, 14, 'test5', 0.00, 0.00, 'admin', '2019-06-19', NULL, NULL, 0.00, 10, NULL, NULL, 0.00);
INSERT INTO "ho"."trPO" VALUES (9, 'P/0004/VI/2019', '2019-06-19', 6, 14, 'test5', 0.00, 0.00, 'admin', '2019-06-19', NULL, NULL, 0.00, 10, NULL, NULL, 0.00);
INSERT INTO "ho"."trPO" VALUES (7, 'P/0002/VI/2019', '2019-06-17', 7, 13, 'test1', 50000.00, 2500.00, 'admin', '2019-06-17', 'admin', '2019-06-17', 52500.00, 50, 'admin', '2019-06-17', 5000.00);
INSERT INTO "ho"."trPO" VALUES (10, 'P/0005/VI/2019', '2019-06-25', 6, 13, 'test', 25.00, 1.25, 'admin', '2019-06-25', NULL, NULL, 25.00, 10, NULL, NULL, 1.25);

-- ----------------------------
-- Table structure for trPODetail
-- ----------------------------
DROP TABLE IF EXISTS "ho"."trPODetail";
CREATE TABLE "ho"."trPODetail" (
  "idPODetail" int4 NOT NULL DEFAULT nextval('"ho"."trPODetail_idPODetail_seq"'::regclass),
  "fidPO" int4,
  "Qty" int4,
  "HargaBeli" numeric(12,2),
  "Diskon" numeric(5,2),
  "PPN" numeric(12,2),
  "SubTotal" numeric(12,2),
  "KodeBarang" varchar(50) COLLATE "pg_catalog"."default",
  "fidJenisKas" int2 DEFAULT 2
)
;

-- ----------------------------
-- Records of trPODetail
-- ----------------------------
INSERT INTO "ho"."trPODetail" VALUES (27, 4, 5, 1500.00, 5.00, 10.00, 7875.00, 'B/0001/VI/2019', 2);
INSERT INTO "ho"."trPODetail" VALUES (28, 4, 2, 1500.00, 5.00, 10.00, 3150.00, 'B/0001/VI/2019', 2);
INSERT INTO "ho"."trPODetail" VALUES (30, 7, 5, 10000.00, 5.00, 10.00, 52500.00, 'B/0001/VI/2019', 2);
INSERT INTO "ho"."trPODetail" VALUES (31, 10, 5, 5.00, 5.00, 5.00, 25.00, 'B/0002/VI/2019', 2);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ho"."trOrderDet_idOrderDet_seq"
OWNED BY "ho"."trOrderDetail"."idOrderDetail";
SELECT setval('"ho"."trOrderDet_idOrderDet_seq"', 37, true);
ALTER SEQUENCE "ho"."trPODetail_idPODetail_seq"
OWNED BY "ho"."trPODetail"."idPODetail";
SELECT setval('"ho"."trPODetail_idPODetail_seq"', 32, true);
ALTER SEQUENCE "ho"."trPO_idPO_seq"
OWNED BY "ho"."trPO"."idPO";
SELECT setval('"ho"."trPO_idPO_seq"', 11, true);

-- ----------------------------
-- Primary Key structure for table trOrder
-- ----------------------------
ALTER TABLE "ho"."trOrder" ADD CONSTRAINT "trOrder_pkey" PRIMARY KEY ("NoOrder");

-- ----------------------------
-- Primary Key structure for table trOrderDetail
-- ----------------------------
ALTER TABLE "ho"."trOrderDetail" ADD CONSTRAINT "trOrderDet_pkey" PRIMARY KEY ("idOrderDetail");

-- ----------------------------
-- Primary Key structure for table trPO
-- ----------------------------
ALTER TABLE "ho"."trPO" ADD CONSTRAINT "trPO_pkey" PRIMARY KEY ("idPO");

-- ----------------------------
-- Primary Key structure for table trPODetail
-- ----------------------------
ALTER TABLE "ho"."trPODetail" ADD CONSTRAINT "trPODetail_pkey" PRIMARY KEY ("idPODetail");
