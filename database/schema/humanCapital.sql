/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 110003
 Source Host           : localhost:5432
 Source Catalog        : tmg
 Source Schema         : humanCapital

 Target Server Type    : PostgreSQL
 Target Server Version : 110003
 File Encoding         : 65001

 Date: 24/06/2019 10:07:12
*/


-- ----------------------------
-- Sequence structure for msEmployee_idMsEmployee_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "humanCapital"."msEmployee_idMsEmployee_seq";
CREATE SEQUENCE "humanCapital"."msEmployee_idMsEmployee_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 97
CACHE 1;

-- ----------------------------
-- Sequence structure for msOrganizationStructure_idMsOrganizationStructure_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq";
CREATE SEQUENCE "humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 2
CACHE 1;

-- ----------------------------
-- Sequence structure for msPosition_idMsPosition_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "humanCapital"."msPosition_idMsPosition_seq";
CREATE SEQUENCE "humanCapital"."msPosition_idMsPosition_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 7
CACHE 1;

-- ----------------------------
-- Sequence structure for msReligion_idReligion_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "humanCapital"."msReligion_idReligion_seq";
CREATE SEQUENCE "humanCapital"."msReligion_idReligion_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 7
CACHE 1;

-- ----------------------------
-- Table structure for msEmployee
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msEmployee";
CREATE TABLE "humanCapital"."msEmployee" (
  "idMsEmployee" int4 NOT NULL DEFAULT nextval('"humanCapital"."msEmployee_idMsEmployee_seq"'::regclass),
  "EmpCode" varchar(15) COLLATE "pg_catalog"."default",
  "Name" varchar(50) COLLATE "pg_catalog"."default",
  "DateOfBirth" date,
  "CityOfBirth" varchar(40) COLLATE "pg_catalog"."default",
  "IDCardNumber" varchar(30) COLLATE "pg_catalog"."default",
  "Address" text COLLATE "pg_catalog"."default",
  "City" varchar(30) COLLATE "pg_catalog"."default",
  "PhoneNumber1" varchar(20) COLLATE "pg_catalog"."default",
  "PhoneNumber2" varchar(20) COLLATE "pg_catalog"."default",
  "Email" varchar(50) COLLATE "pg_catalog"."default",
  "ApprovalStatus" int4,
  "NickName" varchar(50) COLLATE "pg_catalog"."default",
  "fidGender" int4,
  "fidMsPosition" int2,
  "fidMsReligion" int2,
  "KodeAhass" varchar(6) COLLATE "pg_catalog"."default",
  "KodeDealer" varchar(6) COLLATE "pg_catalog"."default",
  "KodeSalesAHM" varchar(20) COLLATE "pg_catalog"."default",
  "Jabatan" varchar(50) COLLATE "pg_catalog"."default",
  "fidPendidikan" varchar(6) COLLATE "pg_catalog"."default",
  "fidHobi" varchar(6) COLLATE "pg_catalog"."default",
  "Provinsi" varchar(30) COLLATE "pg_catalog"."default",
  "KodePos" varchar(5) COLLATE "pg_catalog"."default",
  "Address_KTP" text COLLATE "pg_catalog"."default",
  "Provinsi_KTP" varchar(30) COLLATE "pg_catalog"."default",
  "City_KTP" varchar(30) COLLATE "pg_catalog"."default",
  "KodePos_KTP" varchar(5) COLLATE "pg_catalog"."default",
  "isSales" int2
)
;
COMMENT ON COLUMN "humanCapital"."msEmployee"."EmpCode" IS '- diisini dengan Nomor Induk Karyawan ( NIK )
- format sesuai kebijakan';
COMMENT ON COLUMN "humanCapital"."msEmployee"."isSales" IS '0:NonSales, 1:Sales';

-- ----------------------------
-- Records of msEmployee
-- ----------------------------
INSERT INTO "humanCapital"."msEmployee" VALUES (1, 'APR.1712-0001', 'Administrator', '1985-08-02', 'Bandung', '9817329873298719', 'Jl. Pangeran Super Hero III no. 29', 'Bandung satu saja', '081', '082', 'hendra@suher.com', 9, 'asdd', 2, 5, 0, '02552', 'AP1', 'AHM1234', 'Sales Koordinator', '5', 'A10', '', '0', '', '', '', '0', 1);

-- ----------------------------
-- Table structure for msHobi
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msHobi";
CREATE TABLE "humanCapital"."msHobi" (
  "Code" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "Description" varchar(50) COLLATE "pg_catalog"."default",
  "OrderBy" int2 DEFAULT 0
)
;

-- ----------------------------
-- Records of msHobi
-- ----------------------------
INSERT INTO "humanCapital"."msHobi" VALUES ('A1', 'Adventure (Petualangan)', 1);
INSERT INTO "humanCapital"."msHobi" VALUES ('A10', 'Makan', 10);
INSERT INTO "humanCapital"."msHobi" VALUES ('A11', 'Massage', 11);
INSERT INTO "humanCapital"."msHobi" VALUES ('A12', 'Melukis', 12);
INSERT INTO "humanCapital"."msHobi" VALUES ('A13', 'Memancing', 13);
INSERT INTO "humanCapital"."msHobi" VALUES ('A14', 'Memasak', 14);
INSERT INTO "humanCapital"."msHobi" VALUES ('A15', 'Membaca', 15);
INSERT INTO "humanCapital"."msHobi" VALUES ('A16', 'Membaca Puisi', 16);
INSERT INTO "humanCapital"."msHobi" VALUES ('A17', 'Memelihara Binatang Peliharaan', 17);
INSERT INTO "humanCapital"."msHobi" VALUES ('A18', 'Menanam Bunga', 18);
INSERT INTO "humanCapital"."msHobi" VALUES ('A19', 'Menari', 19);
INSERT INTO "humanCapital"."msHobi" VALUES ('A2', 'Aeromodeling', 2);
INSERT INTO "humanCapital"."msHobi" VALUES ('A20', 'Mendongeng', 20);
INSERT INTO "humanCapital"."msHobi" VALUES ('A21', 'Mengaji', 21);
INSERT INTO "humanCapital"."msHobi" VALUES ('A22', 'Mengarang Cerita', 22);
INSERT INTO "humanCapital"."msHobi" VALUES ('A23', 'Menggambar', 23);
INSERT INTO "humanCapital"."msHobi" VALUES ('A24', 'Mengoleksi Barang Antik', 24);
INSERT INTO "humanCapital"."msHobi" VALUES ('A25', 'Menjahit', 25);
INSERT INTO "humanCapital"."msHobi" VALUES ('A26', 'Menulis Buku', 26);
INSERT INTO "humanCapital"."msHobi" VALUES ('A27', 'Menyanyi', 27);
INSERT INTO "humanCapital"."msHobi" VALUES ('A28', 'Origami', 28);
INSERT INTO "humanCapital"."msHobi" VALUES ('A29', 'Otomotif', 29);
INSERT INTO "humanCapital"."msHobi" VALUES ('A3', 'Bercocok Tanam', 3);
INSERT INTO "humanCapital"."msHobi" VALUES ('A30', 'Pantomim', 30);
INSERT INTO "humanCapital"."msHobi" VALUES ('A31', 'Shopping', 31);
INSERT INTO "humanCapital"."msHobi" VALUES ('A32', 'Surat Menyurat', 32);
INSERT INTO "humanCapital"."msHobi" VALUES ('A33', 'Travelling', 33);
INSERT INTO "humanCapital"."msHobi" VALUES ('A4', 'Berkaraoke', 4);
INSERT INTO "humanCapital"."msHobi" VALUES ('A5', 'Bermain Drama', 5);
INSERT INTO "humanCapital"."msHobi" VALUES ('A6', 'Bermain Sulap ', 6);
INSERT INTO "humanCapital"."msHobi" VALUES ('A7', 'Fotografi', 7);
INSERT INTO "humanCapital"."msHobi" VALUES ('A8', 'Kaligrafi', 8);
INSERT INTO "humanCapital"."msHobi" VALUES ('A9', 'Koleksi Perangko (Fillateli)', 9);
INSERT INTO "humanCapital"."msHobi" VALUES ('B1', 'Badminton', 34);
INSERT INTO "humanCapital"."msHobi" VALUES ('B10', 'Senam', 43);
INSERT INTO "humanCapital"."msHobi" VALUES ('B11', 'Sepakbola', 44);
INSERT INTO "humanCapital"."msHobi" VALUES ('B12', 'Sepatu Roda', 45);
INSERT INTO "humanCapital"."msHobi" VALUES ('B13', 'Surfing', 46);
INSERT INTO "humanCapital"."msHobi" VALUES ('B14', 'Tennis', 47);
INSERT INTO "humanCapital"."msHobi" VALUES ('B15', 'Volley', 48);
INSERT INTO "humanCapital"."msHobi" VALUES ('B16', 'Yoga', 49);
INSERT INTO "humanCapital"."msHobi" VALUES ('B2', 'Basket', 35);
INSERT INTO "humanCapital"."msHobi" VALUES ('B3', 'Bersepeda ', 36);
INSERT INTO "humanCapital"."msHobi" VALUES ('B4', 'Bowling', 37);
INSERT INTO "humanCapital"."msHobi" VALUES ('B5', 'Fitness', 38);
INSERT INTO "humanCapital"."msHobi" VALUES ('B6', 'Golf', 39);
INSERT INTO "humanCapital"."msHobi" VALUES ('B7', 'Hiking', 40);
INSERT INTO "humanCapital"."msHobi" VALUES ('B8', 'Jogging', 41);
INSERT INTO "humanCapital"."msHobi" VALUES ('B9', 'Renang', 42);
INSERT INTO "humanCapital"."msHobi" VALUES ('C1', 'Bermain Games', 50);
INSERT INTO "humanCapital"."msHobi" VALUES ('C10', 'Menonton TV', 59);
INSERT INTO "humanCapital"."msHobi" VALUES ('C2', 'Bermain Komputer', 51);
INSERT INTO "humanCapital"."msHobi" VALUES ('C3', 'Bermain Musik', 52);
INSERT INTO "humanCapital"."msHobi" VALUES ('C4', 'Browsing Internet', 53);
INSERT INTO "humanCapital"."msHobi" VALUES ('C5', 'Chatting', 54);
INSERT INTO "humanCapital"."msHobi" VALUES ('C6', 'Mendengarkan Musik', 55);
INSERT INTO "humanCapital"."msHobi" VALUES ('C7', 'Mendengarkan Radio', 56);
INSERT INTO "humanCapital"."msHobi" VALUES ('C8', 'Menonton Bioskop', 57);
INSERT INTO "humanCapital"."msHobi" VALUES ('C9', 'Menonton Film', 58);

-- ----------------------------
-- Table structure for msOrganizationStructure
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msOrganizationStructure";
CREATE TABLE "humanCapital"."msOrganizationStructure" (
  "idMsOrganizationStructure" int4 NOT NULL DEFAULT nextval('"humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq"'::regclass),
  "fidMsOrg" int4,
  "Description" char(100) COLLATE "pg_catalog"."default",
  "Code" char(5) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for msPendidikan
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msPendidikan";
CREATE TABLE "humanCapital"."msPendidikan" (
  "Code" varchar(2) COLLATE "pg_catalog"."default" NOT NULL,
  "Description" varchar(30) COLLATE "pg_catalog"."default",
  "OrderBy" int2 DEFAULT 0
)
;

-- ----------------------------
-- Records of msPendidikan
-- ----------------------------
INSERT INTO "humanCapital"."msPendidikan" VALUES ('1', 'TIDAK TAMAT SD', 2);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('2', 'SD', 3);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('3', 'SLTP/SMP', 4);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('4', 'SLTA/SMU', 5);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('5', 'AKADEMI/DIPLOMA', 6);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('6', 'SARJANA', 7);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('7', 'PASCA SARJANA', 8);
INSERT INTO "humanCapital"."msPendidikan" VALUES ('N', 'NULL', 1);

-- ----------------------------
-- Table structure for msPosition
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msPosition";
CREATE TABLE "humanCapital"."msPosition" (
  "idMsPosition" int4 NOT NULL DEFAULT nextval('"humanCapital"."msPosition_idMsPosition_seq"'::regclass),
  "PositionCode" varchar(5) COLLATE "pg_catalog"."default",
  "Description" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for msReligion
-- ----------------------------
DROP TABLE IF EXISTS "humanCapital"."msReligion";
CREATE TABLE "humanCapital"."msReligion" (
  "idMsReligion" int4 NOT NULL DEFAULT nextval('"humanCapital"."msReligion_idReligion_seq"'::regclass),
  "Description" varchar(20) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of msReligion
-- ----------------------------
INSERT INTO "humanCapital"."msReligion" VALUES (1, 'Kristen Prostestan');
INSERT INTO "humanCapital"."msReligion" VALUES (2, 'Katolik');
INSERT INTO "humanCapital"."msReligion" VALUES (3, 'Hindu');
INSERT INTO "humanCapital"."msReligion" VALUES (4, 'Buddha');
INSERT INTO "humanCapital"."msReligion" VALUES (5, 'Islam');
INSERT INTO "humanCapital"."msReligion" VALUES (6, 'Kong Hu Cu');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"humanCapital"."msEmployee_idMsEmployee_seq"', 98, true);
SELECT setval('"humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq"', 3, false);
SELECT setval('"humanCapital"."msPosition_idMsPosition_seq"', 8, false);
SELECT setval('"humanCapital"."msReligion_idReligion_seq"', 8, false);

-- ----------------------------
-- Primary Key structure for table msHobi
-- ----------------------------
ALTER TABLE "humanCapital"."msHobi" ADD CONSTRAINT "msHobi_pkey" PRIMARY KEY ("Code");

-- ----------------------------
-- Primary Key structure for table msPendidikan
-- ----------------------------
ALTER TABLE "humanCapital"."msPendidikan" ADD CONSTRAINT "msPendidikan_pkey" PRIMARY KEY ("Code");
