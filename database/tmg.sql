--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: customerRelationship; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "customerRelationship";


ALTER SCHEMA "customerRelationship" OWNER TO postgres;

--
-- Name: dataMaster; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "dataMaster";


ALTER SCHEMA "dataMaster" OWNER TO postgres;

--
-- Name: ho; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA ho;


ALTER SCHEMA ho OWNER TO postgres;

--
-- Name: humanCapital; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "humanCapital";


ALTER SCHEMA "humanCapital" OWNER TO postgres;

--
-- Name: terminal; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA terminal;


ALTER SCHEMA terminal OWNER TO postgres;

SET search_path = "customerRelationship", pg_catalog;

--
-- Name: msCustomer_idMsCustomer_seq; Type: SEQUENCE; Schema: customerRelationship; Owner: postgres
--

CREATE SEQUENCE "msCustomer_idMsCustomer_seq"
    START WITH 104
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "customerRelationship"."msCustomer_idMsCustomer_seq" OWNER TO postgres;

--
-- Name: msCustomer_idMsCustomer_seq; Type: SEQUENCE SET; Schema: customerRelationship; Owner: postgres
--

SELECT pg_catalog.setval('"msCustomer_idMsCustomer_seq"', 104, true);


SET search_path = "dataMaster", pg_catalog;

--
-- Name: msAccount_idAccount_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msAccount_idAccount_seq"
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 32767
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msAccount_idAccount_seq" OWNER TO postgres;

--
-- Name: msAccount_idAccount_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msAccount_idAccount_seq"', 5, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: msAccount; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msAccount" (
    "idAccount" smallint DEFAULT nextval('"msAccount_idAccount_seq"'::regclass) NOT NULL,
    "NamaTTD" character varying(100),
    "Bank" character varying(100),
    "NoRek" character varying(50),
    "Status" smallint,
    "InisialBank" character varying(50),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    created_by character varying(50),
    updated_by character varying(50),
    "NamaRekening" character varying(255)
);


ALTER TABLE "dataMaster"."msAccount" OWNER TO postgres;

--
-- Name: msBarang; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msBarang" (
    "KodeBarang" character varying(100) NOT NULL,
    "fidKelompokBarang" integer,
    "NamaBarang" character varying(200),
    "fidSatuanKecil" integer,
    "fidSatuanBesar" smallint,
    "HargaBeli" numeric(15,0),
    "fidSupplier" integer,
    "PhotoBarang" text,
    "Deskripsi" text,
    "TglInput" date,
    "TglUpdate" date,
    userinput character varying(100),
    userupdate character varying(100),
    "Stok" integer,
    "Merk" character varying(100),
    "Seri" character varying(50)
);


ALTER TABLE "dataMaster"."msBarang" OWNER TO postgres;

--
-- Name: msConfig; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msConfig" (
    "idConfig" smallint NOT NULL,
    "Caption" character varying(255),
    "keyText" character varying(255),
    "keyValue" smallint
);


ALTER TABLE "dataMaster"."msConfig" OWNER TO postgres;

--
-- Name: msCustomer_idCustomer_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msCustomer_idCustomer_seq"
    START WITH 8
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msCustomer_idCustomer_seq" OWNER TO postgres;

--
-- Name: msCustomer_idCustomer_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msCustomer_idCustomer_seq"', 11, true);


--
-- Name: msCustomer; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msCustomer" (
    "idCustomer" integer DEFAULT nextval('"msCustomer_idCustomer_seq"'::regclass) NOT NULL,
    "NamaLengkap" character varying(200),
    "NamaPanggilan" character varying(100),
    "NoHP" character varying(20),
    "Alamat" text,
    "Email" character varying(100),
    "Institusi" character varying(100),
    "Negara" character varying(200),
    "Provinsi" character varying(200),
    "KotaKabupaten" character varying(200),
    "Photo" text
);


ALTER TABLE "dataMaster"."msCustomer" OWNER TO postgres;

--
-- Name: COLUMN "msCustomer"."Institusi"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msCustomer"."Institusi" IS 'perorangan atau perusahaan';


--
-- Name: msGudang; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msGudang" (
    "idGudang" integer NOT NULL,
    "KodeGudang" character varying(6),
    "NamaGudang" character varying(30),
    "AlamatGudang" character varying(200),
    "KotaGudang" character varying(30)
);


ALTER TABLE "dataMaster"."msGudang" OWNER TO postgres;

--
-- Name: msJenisKas_idJenisKas_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msJenisKas_idJenisKas_seq"
    START WITH 6
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msJenisKas_idJenisKas_seq" OWNER TO postgres;

--
-- Name: msJenisKas_idJenisKas_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msJenisKas_idJenisKas_seq"', 6, true);


--
-- Name: msJenisKas; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msJenisKas" (
    "idJenisKas" smallint DEFAULT nextval('"msJenisKas_idJenisKas_seq"'::regclass) NOT NULL,
    "Nama" character varying(255),
    "Keterangan" character varying(255)
);


ALTER TABLE "dataMaster"."msJenisKas" OWNER TO postgres;

--
-- Name: msJenisKendaraan_idJenisKendaraan_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msJenisKendaraan_idJenisKendaraan_seq"
    START WITH 3
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msJenisKendaraan_idJenisKendaraan_seq" OWNER TO postgres;

--
-- Name: msJenisKendaraan_idJenisKendaraan_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msJenisKendaraan_idJenisKendaraan_seq"', 7, true);


--
-- Name: msJenisKendaraan; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msJenisKendaraan" (
    "idJenisKendaraan" smallint DEFAULT nextval('"msJenisKendaraan_idJenisKendaraan_seq"'::regclass) NOT NULL,
    "NamaJenisKendaraan" character varying(255),
    "KodeJenisKendaraan" character varying(50)
);


ALTER TABLE "dataMaster"."msJenisKendaraan" OWNER TO postgres;

--
-- Name: msKelompokBarang_idKelompokBarang_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msKelompokBarang_idKelompokBarang_seq"
    START WITH 7
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msKelompokBarang_idKelompokBarang_seq" OWNER TO postgres;

--
-- Name: msKelompokBarang_idKelompokBarang_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msKelompokBarang_idKelompokBarang_seq"', 11, true);


--
-- Name: msKelompokBarang; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msKelompokBarang" (
    "idKelompokBarang" smallint DEFAULT nextval('"msKelompokBarang_idKelompokBarang_seq"'::regclass) NOT NULL,
    "KodeKelompokBarang" character varying(10),
    "NamaKelompokBarang" character varying(50)
);


ALTER TABLE "dataMaster"."msKelompokBarang" OWNER TO postgres;

--
-- Name: msKendaraan_idKendaraan_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msKendaraan_idKendaraan_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msKendaraan_idKendaraan_seq" OWNER TO postgres;

--
-- Name: msKendaraan_idKendaraan_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msKendaraan_idKendaraan_seq"', 19, true);


--
-- Name: msKendaraan; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msKendaraan" (
    "idKendaraan" integer DEFAULT nextval('"msKendaraan_idKendaraan_seq"'::regclass) NOT NULL,
    "PlatNomor" character varying(20),
    "Merk" character varying(50),
    "Type" character varying(50),
    "fidJenisKendaraan" smallint,
    "Model" character varying(50),
    "TahunPembuatan" character varying(4),
    "Silinder" character varying(50),
    "NoRangka" character varying(50),
    "NoMesin" character varying(50),
    "Warna" character varying(50),
    "BahanBakar" character varying(50),
    "WarnaTNKB" character varying(50),
    "TahunRegistrasi" character varying(4),
    "NoBPKB" character varying(50),
    "KodeLokasi" character varying(50),
    "NoUrutDaftar" character varying(50),
    "PhotoKendaraan1" text,
    "PhotoKendaraan2" text,
    "PhotoKendaraan3" text,
    "PanjangKaroseri" integer,
    "LebarKaroseri" integer,
    "TinggiKaroseri" integer,
    "Dimensi" integer,
    "BeratKosong" integer,
    "BeratMax" integer,
    "PanjangMobil" integer,
    "LebarMobil" integer,
    "TinggiMobil" integer,
    "KecepatanMax" integer,
    "TenagaMax_" character varying(100),
    "UkuranBan_" character varying(100),
    "UkuranRoda_" character varying(100),
    "KapasitasMuatan_" character varying(100),
    "NamaKendaraan" character varying(200),
    "TenagaMax" integer,
    "UkuranBan" integer,
    "UkuranRoda" integer,
    "KapasitasMuatan" integer,
    "Kondisi" character varying(50)
);


ALTER TABLE "dataMaster"."msKendaraan" OWNER TO postgres;

--
-- Name: COLUMN "msKendaraan"."Silinder"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."Silinder" IS 'isi silinder';


--
-- Name: COLUMN "msKendaraan"."NoRangka"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."NoRangka" IS 'nomor rangka';


--
-- Name: COLUMN "msKendaraan"."NoMesin"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."NoMesin" IS 'nomor mesin';


--
-- Name: COLUMN "msKendaraan"."KodeLokasi"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."KodeLokasi" IS 'kode lokasi';


--
-- Name: COLUMN "msKendaraan"."PanjangKaroseri"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."PanjangKaroseri" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."LebarKaroseri"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."LebarKaroseri" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."TinggiKaroseri"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."TinggiKaroseri" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."Dimensi"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."Dimensi" IS 'cbm';


--
-- Name: COLUMN "msKendaraan"."BeratKosong"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."BeratKosong" IS 'kg';


--
-- Name: COLUMN "msKendaraan"."BeratMax"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."BeratMax" IS 'kg';


--
-- Name: COLUMN "msKendaraan"."PanjangMobil"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."PanjangMobil" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."LebarMobil"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."LebarMobil" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."TinggiMobil"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."TinggiMobil" IS 'cm';


--
-- Name: COLUMN "msKendaraan"."KecepatanMax"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."KecepatanMax" IS 'km/jam';


--
-- Name: COLUMN "msKendaraan"."TenagaMax_"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."TenagaMax_" IS 'ps/rpm';


--
-- Name: COLUMN "msKendaraan"."KapasitasMuatan_"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."KapasitasMuatan_" IS 'kg/cbm';


--
-- Name: COLUMN "msKendaraan"."TenagaMax"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."TenagaMax" IS 'ps/rpm';


--
-- Name: COLUMN "msKendaraan"."KapasitasMuatan"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msKendaraan"."KapasitasMuatan" IS 'kg/cbm';


--
-- Name: msPengemudi_idPengemudi_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msPengemudi_idPengemudi_seq"
    START WITH 6
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msPengemudi_idPengemudi_seq" OWNER TO postgres;

--
-- Name: msPengemudi_idPengemudi_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msPengemudi_idPengemudi_seq"', 10, true);


--
-- Name: msPengemudi; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msPengemudi" (
    "idPengemudi" integer DEFAULT nextval('"msPengemudi_idPengemudi_seq"'::regclass) NOT NULL,
    "NamaLengkap" character varying(50),
    "NamaPanggilan" character varying(50),
    "NoHP" character varying(20),
    "Alamat" text,
    "Email" character varying(100),
    "NIK" character varying(100),
    "TglLahir" date,
    "GolDarah" character varying(2),
    "PhotoProfile" text,
    "PhotoKTP" text,
    "PhotoSIM_A" text,
    "PhotoSIM_B1" text,
    "PhotoSIM_B2" text,
    "PunyaSIM_A" smallint,
    "PunyaSIM_B1" smallint,
    "PunyaSIM_B2" smallint,
    "Status" smallint,
    "fidMsReligion" smallint,
    "Bank" character varying(100),
    "NoRekening" character varying(50)
);


ALTER TABLE "dataMaster"."msPengemudi" OWNER TO postgres;

--
-- Name: COLUMN "msPengemudi"."PunyaSIM_A"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msPengemudi"."PunyaSIM_A" IS '0 = tidak , 1 = ya';


--
-- Name: COLUMN "msPengemudi"."PunyaSIM_B1"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msPengemudi"."PunyaSIM_B1" IS '0 = tidak , 1 = ya';


--
-- Name: COLUMN "msPengemudi"."PunyaSIM_B2"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msPengemudi"."PunyaSIM_B2" IS '0 = tidak , 1 = ya';


--
-- Name: COLUMN "msPengemudi"."Status"; Type: COMMENT; Schema: dataMaster; Owner: postgres
--

COMMENT ON COLUMN "msPengemudi"."Status" IS '10 = available 20 = not available 30 = on the way';


--
-- Name: msSatuan_idSatuan_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msSatuan_idSatuan_seq"
    START WITH 4
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msSatuan_idSatuan_seq" OWNER TO postgres;

--
-- Name: msSatuan_idSatuan_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msSatuan_idSatuan_seq"', 7, true);


--
-- Name: msSatuan; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msSatuan" (
    "idSatuan" smallint DEFAULT nextval('"msSatuan_idSatuan_seq"'::regclass) NOT NULL,
    "NamaSatuan" character varying(255)
);


ALTER TABLE "dataMaster"."msSatuan" OWNER TO postgres;

--
-- Name: msStatusOrder; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msStatusOrder" (
    "idStatusOrder" integer NOT NULL,
    "NamaStatusOrder" character varying(255),
    "Colour" character varying(50)
);


ALTER TABLE "dataMaster"."msStatusOrder" OWNER TO postgres;

--
-- Name: msStatusPO; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msStatusPO" (
    "idStatusPO" integer NOT NULL,
    "NamaStatusPO" character varying(255),
    "Colour" character varying(50)
);


ALTER TABLE "dataMaster"."msStatusPO" OWNER TO postgres;

--
-- Name: msSupplier_idSupplier_seq; Type: SEQUENCE; Schema: dataMaster; Owner: postgres
--

CREATE SEQUENCE "msSupplier_idSupplier_seq"
    START WITH 14
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "dataMaster"."msSupplier_idSupplier_seq" OWNER TO postgres;

--
-- Name: msSupplier_idSupplier_seq; Type: SEQUENCE SET; Schema: dataMaster; Owner: postgres
--

SELECT pg_catalog.setval('"msSupplier_idSupplier_seq"', 16, true);


--
-- Name: msSupplier; Type: TABLE; Schema: dataMaster; Owner: postgres; Tablespace: 
--

CREATE TABLE "msSupplier" (
    "idSupplier" smallint DEFAULT nextval('"msSupplier_idSupplier_seq"'::regclass) NOT NULL,
    "KodeSupplier" character varying(10),
    "NamaSupplier" character varying(40),
    "AlamatSupplier" character varying(200),
    "KotaSupplier" character varying(40),
    "Fax" character varying(20),
    "Alamat2Supplier" character varying(255),
    "Alamat3Supplier" character varying(255),
    "Telepon" character varying(20),
    "Telepon2" character varying(20),
    "Telepon3" character varying(20),
    "Fax2" character varying(20),
    "Email" character varying(40)
);


ALTER TABLE "dataMaster"."msSupplier" OWNER TO postgres;

SET search_path = ho, pg_catalog;

--
-- Name: trOrder; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trOrder" (
    "NoOrder" character varying(100) NOT NULL,
    "fidKendaraan" integer,
    "JenisTrip" character varying(100),
    "DeskripsiBarang" text,
    "Berat" smallint,
    "Panjang" smallint,
    "Lebar" smallint,
    "Tinggi" smallint,
    "Total" smallint,
    "TglKirim" date,
    "WaktuPengambilan" time(6) without time zone,
    "LokasiAsal" character varying(200),
    "LokasiTujuan" character varying(200),
    "AreaAsal" character varying(200),
    "AreaTujuan" character varying(200),
    "DetailLokasiPengambilan" text,
    "DetailLokasiTujuan" text,
    "TipeKawasanAsal" character varying(100),
    "TipeKawasanTujuan" character varying(100),
    "InstruksiKhusus" text,
    "Totalharga" real,
    "PhotoBarang" text,
    "AddPackaging" smallint,
    "AddTenagaAngkut" smallint,
    "TglInput" date,
    "UserInput" character varying(50),
    "TglUpdate" date,
    "fidStatusOrder" smallint,
    "fidCustomer" integer,
    "fidPengemudi" integer,
    "UserUpdate" character varying(50),
    "TotalPPH" numeric(12,2),
    "TotalHargaBayar" numeric(15,2),
    "TglOrder" date,
    "TotalPPN" numeric(12,2),
    "NoInvoice" character varying(100),
    "TglInvoice" date
);


ALTER TABLE ho."trOrder" OWNER TO postgres;

--
-- Name: COLUMN "trOrder"."JenisTrip"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."JenisTrip" IS 'single atau multi trip';


--
-- Name: COLUMN "trOrder"."Berat"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."Berat" IS 'kg';


--
-- Name: COLUMN "trOrder"."Panjang"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."Panjang" IS 'cm';


--
-- Name: COLUMN "trOrder"."Lebar"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."Lebar" IS 'cm';


--
-- Name: COLUMN "trOrder"."Tinggi"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."Tinggi" IS 'cm';


--
-- Name: COLUMN "trOrder"."Total"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."Total" IS 'jika diubah jadi cbm / feet';


--
-- Name: COLUMN "trOrder"."DetailLokasiPengambilan"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."DetailLokasiPengambilan" IS 'optional';


--
-- Name: COLUMN "trOrder"."DetailLokasiTujuan"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."DetailLokasiTujuan" IS 'optional';


--
-- Name: COLUMN "trOrder"."fidStatusOrder"; Type: COMMENT; Schema: ho; Owner: postgres
--

COMMENT ON COLUMN "trOrder"."fidStatusOrder" IS '1 = order , 2 = acc , 3 = order selesai';


--
-- Name: trOrderDet_idOrderDet_seq; Type: SEQUENCE; Schema: ho; Owner: postgres
--

CREATE SEQUENCE "trOrderDet_idOrderDet_seq"
    START WITH 11
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE ho."trOrderDet_idOrderDet_seq" OWNER TO postgres;

--
-- Name: trOrderDet_idOrderDet_seq; Type: SEQUENCE SET; Schema: ho; Owner: postgres
--

SELECT pg_catalog.setval('"trOrderDet_idOrderDet_seq"', 31, true);


--
-- Name: trOrderDetail; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trOrderDetail" (
    "idOrderDetail" integer DEFAULT nextval('"trOrderDet_idOrderDet_seq"'::regclass) NOT NULL,
    "NoOrder" character varying(100),
    "NamaBiaya" character varying(200),
    "Nominal" real,
    "fidJenisKas" smallint,
    "PPN" numeric(5,2),
    "PPH" numeric(5,2),
    "SubTotal" numeric(12,2)
);


ALTER TABLE ho."trOrderDetail" OWNER TO postgres;

--
-- Name: trPO_idPO_seq; Type: SEQUENCE; Schema: ho; Owner: postgres
--

CREATE SEQUENCE "trPO_idPO_seq"
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE ho."trPO_idPO_seq" OWNER TO postgres;

--
-- Name: trPO_idPO_seq; Type: SEQUENCE SET; Schema: ho; Owner: postgres
--

SELECT pg_catalog.setval('"trPO_idPO_seq"', 15, true);


--
-- Name: trPO; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trPO" (
    "idPO" integer DEFAULT nextval('"trPO_idPO_seq"'::regclass) NOT NULL,
    "NoPembelian" character varying(50),
    "TglPembelian" date,
    "fidKendaraan" smallint,
    "fidSupplier" smallint,
    "NoFaktur" character varying(50),
    "TotalBiaya" numeric(12,2),
    "TotalDiskon" numeric(12,2),
    "UserInput" character varying(50),
    "TglInput" date,
    "UserApproval" character varying(50),
    "TglApproval" date,
    "TotalBiayaBayar" numeric(12,2),
    "fidStatusPO" smallint,
    "UserUpdate" character varying(50),
    "TglUpdate" date,
    "TotalPPN" numeric(12,2)
);


ALTER TABLE ho."trPO" OWNER TO postgres;

--
-- Name: trPODetail_idPODetail_seq; Type: SEQUENCE; Schema: ho; Owner: postgres
--

CREATE SEQUENCE "trPODetail_idPODetail_seq"
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE ho."trPODetail_idPODetail_seq" OWNER TO postgres;

--
-- Name: trPODetail_idPODetail_seq; Type: SEQUENCE SET; Schema: ho; Owner: postgres
--

SELECT pg_catalog.setval('"trPODetail_idPODetail_seq"', 43, true);


--
-- Name: trPODetail; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trPODetail" (
    "idPODetail" integer DEFAULT nextval('"trPODetail_idPODetail_seq"'::regclass) NOT NULL,
    "fidPO" integer,
    "Qty" integer,
    "HargaBeli" numeric(12,2),
    "Diskon" numeric(5,2),
    "PPN" numeric(12,2),
    "SubTotal" numeric(12,2),
    "KodeBarang" character varying(50),
    "fidJenisKas" smallint DEFAULT 2
);


ALTER TABLE ho."trPODetail" OWNER TO postgres;

--
-- Name: trPOMisc_idPOMisc_seq; Type: SEQUENCE; Schema: ho; Owner: postgres
--

CREATE SEQUENCE "trPOMisc_idPOMisc_seq"
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE ho."trPOMisc_idPOMisc_seq" OWNER TO postgres;

--
-- Name: trPOMisc_idPOMisc_seq; Type: SEQUENCE SET; Schema: ho; Owner: postgres
--

SELECT pg_catalog.setval('"trPOMisc_idPOMisc_seq"', 9, true);


--
-- Name: trPOMisc; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trPOMisc" (
    "idPOMisc" integer DEFAULT nextval('"trPOMisc_idPOMisc_seq"'::regclass) NOT NULL,
    "NoPembelian" character varying(50),
    "TglPembelian" date,
    "TotalBiaya" numeric(12,2),
    "TotalDiskon" numeric(12,2),
    "UserInput" character varying(50),
    "TglInput" date,
    "UserApproval" character varying(50),
    "TglApproval" date,
    "TotalBiayaBayar" numeric(12,2),
    "fidStatusPO" smallint,
    "UserUpdate" character varying(50),
    "TglUpdate" date,
    "TotalPPN" numeric(12,2),
    "TotalPPH" numeric(12,2),
    "NamaTransaksi" character varying(75)
);


ALTER TABLE ho."trPOMisc" OWNER TO postgres;

--
-- Name: trPOMiscDetail_idPOMiscDetail_seq; Type: SEQUENCE; Schema: ho; Owner: postgres
--

CREATE SEQUENCE "trPOMiscDetail_idPOMiscDetail_seq"
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE ho."trPOMiscDetail_idPOMiscDetail_seq" OWNER TO postgres;

--
-- Name: trPOMiscDetail_idPOMiscDetail_seq; Type: SEQUENCE SET; Schema: ho; Owner: postgres
--

SELECT pg_catalog.setval('"trPOMiscDetail_idPOMiscDetail_seq"', 12, true);


--
-- Name: trPOMiscDetail; Type: TABLE; Schema: ho; Owner: postgres; Tablespace: 
--

CREATE TABLE "trPOMiscDetail" (
    "idPOMiscDetail" integer DEFAULT nextval('"trPOMiscDetail_idPOMiscDetail_seq"'::regclass) NOT NULL,
    "fidPOMisc" integer,
    "Qty" integer,
    "HargaBeli" numeric(12,2),
    "Diskon" numeric(5,2),
    "PPN" numeric(12,2),
    "SubTotal" numeric(12,2),
    "NamaBarang" character varying(50),
    "fidJenisKas" smallint DEFAULT 2,
    "PPH" numeric(5,2)
);


ALTER TABLE ho."trPOMiscDetail" OWNER TO postgres;

SET search_path = "humanCapital", pg_catalog;

--
-- Name: msEmployee_idMsEmployee_seq; Type: SEQUENCE; Schema: humanCapital; Owner: postgres
--

CREATE SEQUENCE "msEmployee_idMsEmployee_seq"
    START WITH 97
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "humanCapital"."msEmployee_idMsEmployee_seq" OWNER TO postgres;

--
-- Name: msEmployee_idMsEmployee_seq; Type: SEQUENCE SET; Schema: humanCapital; Owner: postgres
--

SELECT pg_catalog.setval('"msEmployee_idMsEmployee_seq"', 97, true);


--
-- Name: msEmployee; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msEmployee" (
    "idMsEmployee" integer DEFAULT nextval('"msEmployee_idMsEmployee_seq"'::regclass) NOT NULL,
    "EmpCode" character varying(15),
    "Name" character varying(50),
    "DateOfBirth" date,
    "CityOfBirth" character varying(40),
    "IDCardNumber" character varying(30),
    "Address" text,
    "City" character varying(30),
    "PhoneNumber1" character varying(20),
    "PhoneNumber2" character varying(20),
    "Email" character varying(50),
    "ApprovalStatus" integer,
    "NickName" character varying(50),
    "fidGender" integer,
    "fidMsPosition" smallint,
    "fidMsReligion" smallint,
    "KodeAhass" character varying(6),
    "KodeDealer" character varying(6),
    "KodeSalesAHM" character varying(20),
    "Jabatan" character varying(50),
    "fidPendidikan" character varying(6),
    "fidHobi" character varying(6),
    "Provinsi" character varying(30),
    "KodePos" character varying(5),
    "Address_KTP" text,
    "Provinsi_KTP" character varying(30),
    "City_KTP" character varying(30),
    "KodePos_KTP" character varying(5),
    "isSales" smallint
);


ALTER TABLE "humanCapital"."msEmployee" OWNER TO postgres;

--
-- Name: COLUMN "msEmployee"."EmpCode"; Type: COMMENT; Schema: humanCapital; Owner: postgres
--

COMMENT ON COLUMN "msEmployee"."EmpCode" IS '- diisini dengan Nomor Induk Karyawan ( NIK )
- format sesuai kebijakan';


--
-- Name: COLUMN "msEmployee"."isSales"; Type: COMMENT; Schema: humanCapital; Owner: postgres
--

COMMENT ON COLUMN "msEmployee"."isSales" IS '0:NonSales, 1:Sales';


--
-- Name: msHobi; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msHobi" (
    "Code" character varying(3) NOT NULL,
    "Description" character varying(50),
    "OrderBy" smallint DEFAULT 0
);


ALTER TABLE "humanCapital"."msHobi" OWNER TO postgres;

--
-- Name: msOrganizationStructure_idMsOrganizationStructure_seq; Type: SEQUENCE; Schema: humanCapital; Owner: postgres
--

CREATE SEQUENCE "msOrganizationStructure_idMsOrganizationStructure_seq"
    START WITH 2
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "humanCapital"."msOrganizationStructure_idMsOrganizationStructure_seq" OWNER TO postgres;

--
-- Name: msOrganizationStructure_idMsOrganizationStructure_seq; Type: SEQUENCE SET; Schema: humanCapital; Owner: postgres
--

SELECT pg_catalog.setval('"msOrganizationStructure_idMsOrganizationStructure_seq"', 2, false);


--
-- Name: msOrganizationStructure; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOrganizationStructure" (
    "idMsOrganizationStructure" integer DEFAULT nextval('"msOrganizationStructure_idMsOrganizationStructure_seq"'::regclass) NOT NULL,
    "fidMsOrg" integer,
    "Description" character(100),
    "Code" character(5)
);


ALTER TABLE "humanCapital"."msOrganizationStructure" OWNER TO postgres;

--
-- Name: msPendidikan; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msPendidikan" (
    "Code" character varying(2) NOT NULL,
    "Description" character varying(30),
    "OrderBy" smallint DEFAULT 0
);


ALTER TABLE "humanCapital"."msPendidikan" OWNER TO postgres;

--
-- Name: msPosition_idMsPosition_seq; Type: SEQUENCE; Schema: humanCapital; Owner: postgres
--

CREATE SEQUENCE "msPosition_idMsPosition_seq"
    START WITH 7
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "humanCapital"."msPosition_idMsPosition_seq" OWNER TO postgres;

--
-- Name: msPosition_idMsPosition_seq; Type: SEQUENCE SET; Schema: humanCapital; Owner: postgres
--

SELECT pg_catalog.setval('"msPosition_idMsPosition_seq"', 7, false);


--
-- Name: msPosition; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msPosition" (
    "idMsPosition" integer DEFAULT nextval('"msPosition_idMsPosition_seq"'::regclass) NOT NULL,
    "PositionCode" character varying(5),
    "Description" character varying(50)
);


ALTER TABLE "humanCapital"."msPosition" OWNER TO postgres;

--
-- Name: msReligion_idReligion_seq; Type: SEQUENCE; Schema: humanCapital; Owner: postgres
--

CREATE SEQUENCE "msReligion_idReligion_seq"
    START WITH 7
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE "humanCapital"."msReligion_idReligion_seq" OWNER TO postgres;

--
-- Name: msReligion_idReligion_seq; Type: SEQUENCE SET; Schema: humanCapital; Owner: postgres
--

SELECT pg_catalog.setval('"msReligion_idReligion_seq"', 7, false);


--
-- Name: msReligion; Type: TABLE; Schema: humanCapital; Owner: postgres; Tablespace: 
--

CREATE TABLE "msReligion" (
    "idMsReligion" integer DEFAULT nextval('"msReligion_idReligion_seq"'::regclass) NOT NULL,
    "Description" character varying(20)
);


ALTER TABLE "humanCapital"."msReligion" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: AppDashboard_idAppDashboard_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "AppDashboard_idAppDashboard_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."AppDashboard_idAppDashboard_seq" OWNER TO postgres;

--
-- Name: AppDashboard_idAppDashboard_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"AppDashboard_idAppDashboard_seq"', 1, false);


--
-- Name: AppDashboard; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AppDashboard" (
    "idAppDashboard" integer DEFAULT nextval('"AppDashboard_idAppDashboard_seq"'::regclass) NOT NULL,
    "Title" character varying(40),
    "OnClick" character varying(100),
    "Icon" character varying(20),
    "fidAppMenu" smallint DEFAULT 0,
    "fidAppShorcut" smallint DEFAULT 0,
    "OrderBy" smallint,
    "GroupName" character varying(30),
    "Color" character varying(30),
    "InnerInfoURL" character varying(100),
    "ColumnWidth" numeric(2,0) DEFAULT 3,
    "Description" character varying(100),
    "IdResult" character varying(100)
);


ALTER TABLE public."AppDashboard" OWNER TO postgres;

--
-- Name: COLUMN "AppDashboard"."OnClick"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppDashboard"."OnClick" IS 'javascript function event click';


--
-- Name: COLUMN "AppDashboard"."GroupName"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppDashboard"."GroupName" IS 'Untuk grouping dalam short key tersebut';


--
-- Name: COLUMN "AppDashboard"."Color"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppDashboard"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';


--
-- Name: COLUMN "AppDashboard"."InnerInfoURL"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppDashboard"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';


--
-- Name: COLUMN "AppDashboard"."ColumnWidth"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppDashboard"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';


--
-- Name: AppMenus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AppMenus" (
    "idAppMenu" integer NOT NULL,
    "Title" character varying(50),
    "URL" character varying(100),
    "IconImg" character varying(30),
    "ColorCode" character varying(20),
    "fidAppMenu" smallint DEFAULT 0,
    "OrderBy" smallint DEFAULT 0,
    "Description" character varying(100),
    "GroupMenu" smallint,
    "Project" smallint DEFAULT 0
);


ALTER TABLE public."AppMenus" OWNER TO postgres;

--
-- Name: COLUMN "AppMenus"."GroupMenu"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppMenus"."GroupMenu" IS '1 = H1
2 = H2
3 = H3
4 = HC3
';


--
-- Name: COLUMN "AppMenus"."Project"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppMenus"."Project" IS '0 = DMS
1 = SPK';


--
-- Name: AppMenus_id_app_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "AppMenus_id_app_menu_seq"
    START WITH 117
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."AppMenus_id_app_menu_seq" OWNER TO postgres;

--
-- Name: AppMenus_id_app_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"AppMenus_id_app_menu_seq"', 117, true);


--
-- Name: AppShortKey_idAppShortCut_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "AppShortKey_idAppShortCut_seq"
    START WITH 34
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."AppShortKey_idAppShortCut_seq" OWNER TO postgres;

--
-- Name: AppShortKey_idAppShortCut_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"AppShortKey_idAppShortCut_seq"', 34, true);


--
-- Name: AppShortCut; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "AppShortCut" (
    "idAppShortCut" integer DEFAULT nextval('"AppShortKey_idAppShortCut_seq"'::regclass) NOT NULL,
    "Title" character varying(40),
    "OnClick" character varying(100),
    "Icon" character varying(20),
    "fidAppMenu" smallint,
    "OrderBy" smallint,
    "GroupName" character varying(30),
    "Color" character varying(30),
    "InnerInfoURL" character varying(100),
    "ColumnWidth" numeric(2,0) DEFAULT 3,
    "Description" character varying(100)
);


ALTER TABLE public."AppShortCut" OWNER TO postgres;

--
-- Name: COLUMN "AppShortCut"."OnClick"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppShortCut"."OnClick" IS 'javascript function event click';


--
-- Name: COLUMN "AppShortCut"."GroupName"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppShortCut"."GroupName" IS 'Untuk grouping dalam short key tersebut';


--
-- Name: COLUMN "AppShortCut"."Color"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppShortCut"."Color" IS '- bg-red
- bg-yellow
- bg-aqua
- bg-blue
- bg-light-blue
- bg-green
- bg-navy
- bg-teal
- bg-olive
- bg-lime
- bg-orange
- bg-fuchsia
- bg-purple
- bg-maroon
- bg-black
- bg-red-active
- bg-yellow-active
- bg-aqua-active
- bg-blue-active
- bg-light-blue-active
- bg-green-active
- bg-navy-active
- bg-teal-active
- bg-olive-active
- bg-lime-active
- bg-orange-active
- bg-fuchsia-active
- bg-purple-active
- bg-maroon-active
- bg-black-active';


--
-- Name: COLUMN "AppShortCut"."InnerInfoURL"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppShortCut"."InnerInfoURL" IS 'url untuk load data yang dimunculkan pada Short Cut';


--
-- Name: COLUMN "AppShortCut"."ColumnWidth"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "AppShortCut"."ColumnWidth" IS 'lebar shortcut menu :
- nilai yg teresia : 1,2,3 - 12';


--
-- Name: fileUpload_idFileUpload_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "fileUpload_idFileUpload_seq"
    START WITH 42
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."fileUpload_idFileUpload_seq" OWNER TO postgres;

--
-- Name: fileUpload_idFileUpload_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"fileUpload_idFileUpload_seq"', 43, true);


--
-- Name: fileUpload; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "fileUpload" (
    "idFileUpload" integer DEFAULT nextval('"fileUpload_idFileUpload_seq"'::regclass) NOT NULL,
    "Category" character varying(50),
    "fidData" integer,
    "FileExtention" character varying(5),
    "FileName" character varying(100),
    "FileSize" integer
);


ALTER TABLE public."fileUpload" OWNER TO postgres;

--
-- Name: karunia_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE karunia_sessions (
    id character varying(128) NOT NULL,
    ip_address character varying(45) NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.karunia_sessions OWNER TO postgres;

--
-- Name: msMasterOperatorSpecial_idMasterOperatorSpecial_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msMasterOperatorSpecial_idMasterOperatorSpecial_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msMasterOperatorSpecial_idMasterOperatorSpecial_seq" OWNER TO postgres;

--
-- Name: msMasterOperatorSpecial_idMasterOperatorSpecial_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msMasterOperatorSpecial_idMasterOperatorSpecial_seq"', 1, false);


--
-- Name: msOperator; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperator" (
    "idMsOperator" integer NOT NULL,
    "LoginName" character varying(32),
    "LoginPass" character varying(32),
    "FullName" character varying(40),
    "KodeAHASS" character varying(6),
    "LastUpdate" timestamp(6) without time zone,
    "ExpiryDate" date,
    "fidMsOperatorGroup" integer DEFAULT 0,
    "fidMsEmployee" integer,
    "KodeDealer" character varying(6),
    "KodeSalesPerson" character varying(10),
    "LoginPassFincoy" character varying(32),
    "fidMenuDefaultOpen" smallint,
    "isAccessGudang" character varying(100),
    "isDefaultGudang" integer DEFAULT 0
);


ALTER TABLE public."msOperator" OWNER TO postgres;

--
-- Name: msOperatorAppShortKey_idMsOperatorAppShortKey_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msOperatorAppShortKey_idMsOperatorAppShortKey_seq"
    START WITH 2763
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msOperatorAppShortKey_idMsOperatorAppShortKey_seq" OWNER TO postgres;

--
-- Name: msOperatorAppShortKey_idMsOperatorAppShortKey_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msOperatorAppShortKey_idMsOperatorAppShortKey_seq"', 2763, true);


--
-- Name: msOperatorAppShortCut; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorAppShortCut" (
    "idMsOperatorAppShortKey" integer DEFAULT nextval('"msOperatorAppShortKey_idMsOperatorAppShortKey_seq"'::regclass) NOT NULL,
    "fidMsOperator" integer,
    "fidAppShortCut" smallint,
    "Status" smallint
);


ALTER TABLE public."msOperatorAppShortCut" OWNER TO postgres;

--
-- Name: msOperatorDashboardPrivilege_idDashboardPrivilege_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msOperatorDashboardPrivilege_idDashboardPrivilege_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msOperatorDashboardPrivilege_idDashboardPrivilege_seq" OWNER TO postgres;

--
-- Name: msOperatorDashboardPrivilege_idDashboardPrivilege_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msOperatorDashboardPrivilege_idDashboardPrivilege_seq"', 1, false);


--
-- Name: msOperatorDashboardPrivilege; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorDashboardPrivilege" (
    "idDashboardPrivilege" integer DEFAULT nextval('"msOperatorDashboardPrivilege_idDashboardPrivilege_seq"'::regclass) NOT NULL,
    "fidMsOperator" integer DEFAULT 0,
    "fidAppDashboard" smallint DEFAULT 0,
    "Status" smallint DEFAULT 0
);


ALTER TABLE public."msOperatorDashboardPrivilege" OWNER TO postgres;

--
-- Name: msOperatorGroup_idMsOperatorGroup_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msOperatorGroup_idMsOperatorGroup_seq"
    START WITH 6
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msOperatorGroup_idMsOperatorGroup_seq" OWNER TO postgres;

--
-- Name: msOperatorGroup_idMsOperatorGroup_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msOperatorGroup_idMsOperatorGroup_seq"', 6, false);


--
-- Name: msOperatorGroup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorGroup" (
    "idMsOperatorGroup" integer DEFAULT nextval('"msOperatorGroup_idMsOperatorGroup_seq"'::regclass) NOT NULL,
    "ShortName" character varying(12),
    "Name" character varying(20),
    "Description" character varying(100),
    "Icon" character varying(20),
    "DefaultMenu" text,
    "DefaultShortcut" text
);


ALTER TABLE public."msOperatorGroup" OWNER TO postgres;

--
-- Name: msOperatorModul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorModul" (
    "idOperatorModul" integer DEFAULT nextval('"msMasterOperatorSpecial_idMasterOperatorSpecial_seq"'::regclass) NOT NULL,
    "Caption" character varying(50),
    "SpecialVar" character varying(50),
    "Deskripsi" text
);


ALTER TABLE public."msOperatorModul" OWNER TO postgres;

--
-- Name: msOperatorPrivilege_idPrivilege_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msOperatorPrivilege_idPrivilege_seq"
    START WITH 5749
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msOperatorPrivilege_idPrivilege_seq" OWNER TO postgres;

--
-- Name: msOperatorPrivilege_idPrivilege_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msOperatorPrivilege_idPrivilege_seq"', 5752, true);


--
-- Name: msOperatorPrivilege; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorPrivilege" (
    "idPrivilege" integer DEFAULT nextval('"msOperatorPrivilege_idPrivilege_seq"'::regclass) NOT NULL,
    "fidMsOperator" integer DEFAULT 0,
    "fidAppMenu" integer,
    "Status" smallint DEFAULT 0
);


ALTER TABLE public."msOperatorPrivilege" OWNER TO postgres;

--
-- Name: COLUMN "msOperatorPrivilege"."Status"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN "msOperatorPrivilege"."Status" IS '0 = off
1 = active';


--
-- Name: msOperatorSpecial; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "msOperatorSpecial" (
    "idOperatorSpecial" smallint NOT NULL,
    "fidMsOperator" smallint,
    "SpecialValue" character varying(40),
    "fidOperatorModul" smallint,
    "Status" smallint
);


ALTER TABLE public."msOperatorSpecial" OWNER TO postgres;

--
-- Name: msOperator_idMsOperator_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "msOperator_idMsOperator_seq"
    START WITH 67
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public."msOperator_idMsOperator_seq" OWNER TO postgres;

--
-- Name: msOperator_idMsOperator_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"msOperator_idMsOperator_seq"', 67, true);


SET search_path = terminal, pg_catalog;

--
-- Name: trDataLog_idTrDataLog_seq; Type: SEQUENCE; Schema: terminal; Owner: postgres
--

CREATE SEQUENCE "trDataLog_idTrDataLog_seq"
    START WITH 90
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE terminal."trDataLog_idTrDataLog_seq" OWNER TO postgres;

--
-- Name: trDataLog_idTrDataLog_seq; Type: SEQUENCE SET; Schema: terminal; Owner: postgres
--

SELECT pg_catalog.setval('"trDataLog_idTrDataLog_seq"', 90, true);


--
-- Name: trDataLog; Type: TABLE; Schema: terminal; Owner: postgres; Tablespace: 
--

CREATE TABLE "trDataLog" (
    "idTrDataLog" integer DEFAULT nextval('"trDataLog_idTrDataLog_seq"'::regclass) NOT NULL,
    "SchemaName" character(30),
    "TableName" character(50),
    "fidData" integer,
    "fidMsOperator" integer,
    "ActionTime" timestamp(6) without time zone,
    "LogData" text,
    "LogType" character(10)
);


ALTER TABLE terminal."trDataLog" OWNER TO postgres;

--
-- Name: TABLE "trDataLog"; Type: COMMENT; Schema: terminal; Owner: postgres
--

COMMENT ON TABLE "trDataLog" IS 'Menyimpan semua perubahan data yang terjadi pada masing2 table';


--
-- Name: trFileUploadManager_idTrFileUploadManager_seq; Type: SEQUENCE; Schema: terminal; Owner: postgres
--

CREATE SEQUENCE "trFileUploadManager_idTrFileUploadManager_seq"
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE terminal."trFileUploadManager_idTrFileUploadManager_seq" OWNER TO postgres;

--
-- Name: trFileUploadManager_idTrFileUploadManager_seq; Type: SEQUENCE SET; Schema: terminal; Owner: postgres
--

SELECT pg_catalog.setval('"trFileUploadManager_idTrFileUploadManager_seq"', 1, false);


--
-- Name: tr_log_id_log_seq; Type: SEQUENCE; Schema: terminal; Owner: postgres
--

CREATE SEQUENCE tr_log_id_log_seq
    START WITH 9363
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE terminal.tr_log_id_log_seq OWNER TO postgres;

--
-- Name: tr_log_id_log_seq; Type: SEQUENCE SET; Schema: terminal; Owner: postgres
--

SELECT pg_catalog.setval('tr_log_id_log_seq', 9430, true);


--
-- Name: tr_log; Type: TABLE; Schema: terminal; Owner: postgres; Tablespace: 
--

CREATE TABLE tr_log (
    id_log integer DEFAULT nextval('tr_log_id_log_seq'::regclass) NOT NULL,
    value_before text,
    action_time timestamp(6) without time zone DEFAULT now(),
    ip_comp character(20),
    fid_operator integer,
    log_type character(30),
    table_name character varying(100),
    fid_data integer
);


ALTER TABLE terminal.tr_log OWNER TO postgres;

SET search_path = "dataMaster", pg_catalog;

--
-- Data for Name: msAccount; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msAccount" VALUES (5, 'SAEFUL YAMIN', 'BANK CENTRAL ASIA', '10221300', 1, 'BCA', '2019-08-21 16:51:36', NULL, 'admin', NULL, 'PT. TRISULA MULTISARANA GLOBAL');
INSERT INTO "msAccount" VALUES (2, 'JURAGAN JENGKOL', 'Bank Rakyat Indonesia', '24234234', 0, 'BRI', NULL, '2019-08-21 16:52:08', NULL, 'admin', 'SAEFUL YAMIN');


--
-- Data for Name: msBarang; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msBarang" VALUES ('B/0001/VII/2019', 11, 'OLI REM', 7, 6, 250000, 16, NULL, 'OLI REM PELUMAS REM PALING YAHUT, UNTUK JENIS KENDARAAN SUPER OKE', '2019-07-15', NULL, 'admin', NULL, NULL, 'MESRAN', '');
INSERT INTO "msBarang" VALUES ('B/0002/VII/2019', 9, 'TROMBOL', 7, 6, 275000, 16, NULL, 'TROMBOL REM', '2019-07-15', NULL, 'admin', NULL, NULL, 'HIMOTO', '');


--
-- Data for Name: msConfig; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msConfig" VALUES (4, '<b>Version</b> 1.0.0', 'version', 4);
INSERT INTO "msConfig" VALUES (8, 'skin-blue', 'app_skin', 8);
INSERT INTO "msConfig" VALUES (15, '50', 'set_discount_max_persen', 1001);
INSERT INTO "msConfig" VALUES (16, '2000', 'set_discount_max_rupiah', 1000);
INSERT INTO "msConfig" VALUES (17, 'Kalau 1 = download, kalau 2= print html', 'print_mode', 15);
INSERT INTO "msConfig" VALUES (2, 'Copyright © 2019', 'copyright', 2);
INSERT INTO "msConfig" VALUES (9, 'PT. TMG', 'pt_nama', 9);
INSERT INTO "msConfig" VALUES (13, '', 'ppn_nama', 13);
INSERT INTO "msConfig" VALUES (14, '', 'nonppn_nama', 14);
INSERT INTO "msConfig" VALUES (1, 'TMG', 'app_name', 1);
INSERT INTO "msConfig" VALUES (6, 'assets/images/wallpaper/tmg.png', 'app_cover', 6);
INSERT INTO "msConfig" VALUES (5, 'TMG', 'app_title', 5);
INSERT INTO "msConfig" VALUES (3, 'PT. TRISULA MULTISARANA GLOBAL', 'perusahaan', 3);
INSERT INTO "msConfig" VALUES (10, 'GLOBAL TRUCKING SOLUTION', 'pt_addr1', 10);
INSERT INTO "msConfig" VALUES (11, 'Jl. Raya Laswi No. 155 Kel. Manggahang Kec. Baleendah Kab. Bandung', 'pt_addr2', 11);
INSERT INTO "msConfig" VALUES (12, 'Telp. 0813-22999168 E-mail : opa.tmglobal@gmail.com', 'pt_addr3', 12);
INSERT INTO "msConfig" VALUES (7, 'assets/images/icon/logo-tmg.png', 'app_icon', 7);


--
-- Data for Name: msCustomer; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msCustomer" VALUES (10, 'PT. ING INTERNATIONAL', 'SAEPUL YAMIN', '022-5959577', 'JL.RAYA RANCAEKEK-MAJALAYA NO.389 SOLOKAN JERUK, KAB.BANDUNG', 'exim3@ing-international.com', 'GARMENT', 'INDONESIA', 'JAWA BARAT', 'KAB. BANDUNG', NULL);
INSERT INTO "msCustomer" VALUES (11, 'PT. GREENTEX INDONESIA', 'ADE', '-', 'JL.RAYA BANJARAN BANDUNG', '-', 'GARMENT', 'INDONESIA', 'JAWA BARAT', 'KAB.BANDUNG', NULL);


--
-- Data for Name: msGudang; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msGudang" VALUES (1, 'G.BTN', 'GUDANG BATUNUNGGAL', 'JL. BATUNUNGGAL LESTARI NO 42', 'BANDUNG');
INSERT INTO "msGudang" VALUES (2, 'G.CLP', 'GUDANG CILAMPENI', 'JL. CILAMPENI NO 33', 'KAB BANDUNG');
INSERT INTO "msGudang" VALUES (100001, 'T.BTN', 'KARUNIA BATUNUNGGAL', 'JL. BATUNUNGGAL INDAH RAYA NO 165', 'BANDUNG');
INSERT INTO "msGudang" VALUES (100000, 'T.OTS', 'KARUNIA OTISTA', 'JL. OTTO ISKANDARDINATA NO 143A', 'BANDUNG');


--
-- Data for Name: msJenisKas; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msJenisKas" VALUES (2, 'KAS KELUAR', 'PENGELUARAN OPERASIONAL');
INSERT INTO "msJenisKas" VALUES (1, 'KAS MASUK', 'PEMASUKAN');


--
-- Data for Name: msJenisKendaraan; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msJenisKendaraan" VALUES (7, 'BOX', 'BX');
INSERT INTO "msJenisKendaraan" VALUES (6, 'WINGS BOX', 'WB');


--
-- Data for Name: msKelompokBarang; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msKelompokBarang" VALUES (9, 'SP', 'SPAREPART');
INSERT INTO "msKelompokBarang" VALUES (10, 'RD', 'RODA');
INSERT INTO "msKelompokBarang" VALUES (11, 'PL', 'PELUMAS');


--
-- Data for Name: msKendaraan; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msKendaraan" VALUES (13, 'B 9193 CCD', 'TOYOTA', 'CDD JUMBO', 7, 'DYNA 110ET', '2013', '6', '-', '-', 'MERAH', 'SOLAR', 'HITAM', '2018', '-', NULL, '', NULL, NULL, NULL, 600, 210, 210, 27, 4000, 8000, 750, 220, 220, 80, NULL, NULL, NULL, NULL, 'B9193CCD', 90, 750, 18, 6000, 'Sangat Baik');
INSERT INTO "msKendaraan" VALUES (14, 'D 8604 DZ', 'MITSUBISHI', 'CDD JUMBO', 7, 'FUSO 120PS', '1999', '6', '-', '-', 'ORANGE', 'SOLAR', 'HITAM', '2015', '-', NULL, '', NULL, NULL, NULL, 600, 210, 220, 28, 4000, 4000, 750, 220, 230, 80, NULL, NULL, NULL, NULL, 'D8604DZ', 90, 750, 18, 4000, 'Baik');
INSERT INTO "msKendaraan" VALUES (15, 'D 8051 EK', 'TOYOTA', 'CDD STANDARD', 7, 'DYNA 110ET', '2014', '6', '-', '-', 'MERAH/SILVER', 'SOLAR', 'HITAM', '2019', '-', NULL, '', NULL, NULL, NULL, 400, 210, 220, 18, 3000, 3000, 550, 220, 230, 80, NULL, NULL, NULL, NULL, 'D8051EK', 80, 750, 18, 3000, 'Sangat Baik');
INSERT INTO "msKendaraan" VALUES (16, 'D 8125 YH', 'ISUZU', 'CDE ENGKEL', 7, 'ELF', '2007', '6', '-', '-', 'PUTIH/SILVER', 'SOLAR', 'HITAM', '2017', '-', NULL, '', NULL, NULL, NULL, 210, 180, 180, 7, 2000, 2000, 360, 190, 190, 80, NULL, NULL, NULL, NULL, 'D8125YH', 80, 750, 18, 2000, 'Sangat Baik');
INSERT INTO "msKendaraan" VALUES (17, 'D 8902 EU', 'DAIHATSU', 'GRANDMAX', 7, 'GRANDMAX', '2013', '4', '-', '-', 'SILVER', 'BENSIN', 'HITAM', '2019', '-', NULL, '', NULL, NULL, NULL, 200, 150, 140, 4, 1000, 1000, 350, 160, 150, 80, NULL, NULL, NULL, NULL, 'D8902EU', 90, 170, 14, 1000, 'Sangat Baik');
INSERT INTO "msKendaraan" VALUES (18, 'E 9208 B', 'MITSUBISHI', 'ENGKEL', 7, 'FIGHTER 190PS', '1990', '8', '-', '-', 'COKLAT', 'SOLAR', 'KUNING', '2015', '-', NULL, '', NULL, NULL, NULL, 800, 240, 240, 46, 5000, 10000, 950, 250, 250, 80, NULL, NULL, NULL, NULL, 'E9208B', 190, 900, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (19, 'D 8516 AO', 'MITSUBISHI', 'ENGKEL', 7, 'FIGHTER 190PS', '1991', '8', '-', '-', 'COKLAT', 'SOLAR', 'HITAM', '2016', '-', NULL, '', NULL, NULL, NULL, 800, 240, 240, 46, 5000, 10000, 950, 250, 250, 80, NULL, NULL, NULL, NULL, 'D8516AO', 120, 900, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (8, 'B 9606 AP', 'MITSUBISHI', 'BUILD UP', 7, 'FUSO BUILD UP', '1995', '8', '-', '-', 'BIRU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 950, 240, 240, 54, 7000, 12000, 11000, 250, 250, 80, NULL, NULL, NULL, NULL, 'B9606AP', 120, 1000, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (7, 'D 9021 AL', 'HINO', 'TRONTON', 7, 'RANGER', '1993', '8', '-', '-', 'HIJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 800, 240, 230, 44, 6000, 12000, 950, 250, 240, 80, NULL, NULL, NULL, NULL, 'D9021AL', 120, 900, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (1, 'D 9086 MH', 'HINO', 'TRONTON', 7, 'RANGER', '1993', '8', '-', '-', 'HIJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', 'TVE9PQ==Kendaraan120190715111545-90x90.jpg', NULL, NULL, 800, 240, 240, 46, 8000, 60000, 9500, 250, 250, 80, NULL, NULL, NULL, NULL, 'D9086MH', 120, 900, 32, 10000, 'Kurang Baik');
INSERT INTO "msKendaraan" VALUES (2, 'D 9186 BC', 'HINO', 'ENGKEL', 7, 'RANGER', '1993', '8', '-', '-', 'HIJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 800, 240, 230, 44, 6000, 12000, 950, 250, 240, 80, NULL, NULL, NULL, NULL, 'D9186BC', 120, 1000, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (6, 'D 9188 AC', 'HINO', 'TRONTON', 7, 'RANGER', '1993', '8', '-', '-', 'HIJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 950, 240, 240, 54, 7000, 12000, 11000, 250, 250, 80, NULL, NULL, NULL, NULL, 'D9188AC', 120, 1000, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (4, 'D 9189 DC', 'HINO', 'TRONTON', 7, 'RANGER', '1993', '8', '-', '-', 'HIJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 750, 240, 240, 43, 6000, 12000, 900, 250, 250, 80, NULL, NULL, NULL, NULL, 'D9189DC', 120, 900, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (5, 'D 9358 AP', 'MITSUBISHI', 'TRONTON', 7, 'FIGHTER', '1990', '8', '-', '-', 'COKLAT', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 750, 240, 240, 43, 6000, 12000, 900, 250, 250, 80, NULL, NULL, NULL, NULL, 'D9358AP', 120, 900, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (3, 'D 9888 CP', 'HINO', 'TRONTON', 7, 'RANGER', '1993', '8', '-', '-', 'HAJAU', 'SOLAR', 'KUNING', '2016', '-', NULL, '', NULL, NULL, NULL, 850, 240, 250, 51, 7000, 12000, 1000, 250, 260, 80, NULL, NULL, NULL, NULL, 'D9888CP', 120, 900, 32, 12000, 'Baik');
INSERT INTO "msKendaraan" VALUES (9, 'H 1814 AY', 'MITSUBISHI', 'ENGKEL', 7, 'FIGHTER D16', '1990', '8', '-', '-', 'ORANGE', 'SOLAR', 'KUNING', '2017', '-', NULL, '', NULL, NULL, NULL, 800, 250, 250, 50, 6000, 12000, 950, 260, 260, 80, NULL, NULL, NULL, NULL, 'H1814AY', 120, 1000, 32, 10000, 'Baik');
INSERT INTO "msKendaraan" VALUES (10, 'D 8735 BY', 'MITSUBISHI', 'CDD STANDARD', 7, 'COLT DIESEL 100PS', '2000', '6', '-', '-', 'KUNING', 'SOLAR', 'HITAM', '2016', '-', NULL, '', NULL, NULL, NULL, 400, 210, 220, 18, 2000, 3000, 550, 220, 230, 80, NULL, NULL, NULL, NULL, 'D8735BY', 80, 750, 18, 3000, 'Baik');
INSERT INTO "msKendaraan" VALUES (11, 'D 9341 YB', 'ISUZU', 'CDD JUMBO', 7, 'GIGA NMR71', '2019', '6', '-', '-', 'PUTIH/ORANGE', 'SOLAR', 'KUNING', '2019', '-', NULL, '', NULL, NULL, NULL, 650, 220, 230, 33, 4000, 5000, 800, 230, 240, 80, NULL, NULL, NULL, NULL, 'D9341YB', 90, 750, 18, 5000, 'Sangat Baik');
INSERT INTO "msKendaraan" VALUES (12, 'B 9637 NI', 'ISUZU', 'CDD STANDARD', 7, 'ELF 70', '2005', '6', '-', '-', 'PUTIH/SILVER', 'SOLAR', 'HITAM', '2016', '-', NULL, '', NULL, NULL, NULL, 400, 190, 210, 16, 3000, 4000, 550, 200, 220, 80, NULL, NULL, NULL, NULL, 'B9637NI', 70, 750, 18, 3000, 'Baik');


--
-- Data for Name: msPengemudi; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msPengemudi" VALUES (9, 'FAJRUL HAYAT', 'FAJRUL', '081322456451', 'JL. SUKASENANG RT/RW. 07/05 DAYEUH MANGGUNG', 'FAJRUL.H@GMAIL.COM', '12345679754545', '1968-03-14', 'B', 'T1E9PQ==Profile20190715112349-90x90.jpg', 'T1E9PQ==KTP20190715112349-90x90.jpeg', NULL, NULL, 'T1E9PQ==SIM_B220190715112349-90x90.jpg', 0, 0, 1, 10, 5, 'BNI', '6543217890');
INSERT INTO "msPengemudi" VALUES (10, 'MAMAN SURAHMAN', 'JAIM', '08122121212', 'JL. KERSAMENAK NO.127 KAB. BANDUNG', 'MAMAN@FACBOOK.COM', '12345678978', '1977-01-01', 'A', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 10, 5, 'BNI', '456123455');


--
-- Data for Name: msSatuan; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msSatuan" VALUES (6, 'CARTON');
INSERT INTO "msSatuan" VALUES (7, 'PIECE');


--
-- Data for Name: msStatusOrder; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msStatusOrder" VALUES (10, 'Data Baru', 'black');
INSERT INTO "msStatusOrder" VALUES (20, 'Approved', 'blue');
INSERT INTO "msStatusOrder" VALUES (30, 'Reject', 'red');
INSERT INTO "msStatusOrder" VALUES (50, 'Done', 'green');


--
-- Data for Name: msStatusPO; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msStatusPO" VALUES (10, 'Data Baru', 'black');
INSERT INTO "msStatusPO" VALUES (50, 'Approve', 'green');


--
-- Data for Name: msSupplier; Type: TABLE DATA; Schema: dataMaster; Owner: postgres
--

INSERT INTO "msSupplier" VALUES (16, 'SUP-0001', 'AUTO SERVICE', 'JL. SOEKARNO HATTA NO. 385', 'BANDUNG', '', '', '', '0226549875', '', '', '', 'AUTOSERICE@YAHOO.COM');


SET search_path = ho, pg_catalog;

--
-- Data for Name: trOrder; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trOrder" VALUES ('0001/JOB/VIII/2019', 14, 'Single', '', 0, 750, 220, 230, 0, '2019-08-02', '08:00:00', 'GUDANG PABRIK', 'GUDANG APL', 'BANDUNG', 'JAKARTA', '', '', '', '', 'HARUS TEPAT WAKTU', 4250000, NULL, 0, 0, '2019-08-21', 'admin', NULL, 50, 10, 9, NULL, 70000.00, 4180000.00, '2019-08-01', 0.00, '0001/TMG/VIII/2019', '2019-08-02');
INSERT INTO "trOrder" VALUES ('0002/JOB/VIII/2019', 17, 'Single', '', 0, 350, 160, 150, 0, '2019-08-21', '08:00:00', 'GUDANG PABRIK', 'PELABUHAN', 'BANDUNG', 'JAKARTA', '', '', '', '', 'JANGAN TELAT', 4000000, NULL, 0, 0, '2019-08-21', 'admin', NULL, 50, 10, 9, NULL, 70000.00, 3930000.00, '2019-08-03', 0.00, '0002/TMG/VIII/2019', '2019-08-21');
INSERT INTO "trOrder" VALUES ('0003/JOB/VIII/2019', 14, 'Single', '', 0, 750, 220, 230, 0, '2019-08-08', '08:00:00', 'PELABUHAN', 'GUDANG PABRIK', 'JAKARTA', 'BANDUNG', '', '', '', '', '', 6000000, NULL, 0, 0, '2019-08-21', 'admin', NULL, 50, 11, 10, NULL, 0.00, 6000000.00, '2019-08-07', 0.00, '0003/TMG/VIII/2019', '2019-08-19');
INSERT INTO "trOrder" VALUES ('0004/JOB/VIII/2019', 16, 'Single', '', 0, 360, 190, 190, 0, '2019-08-07', '08:00:00', 'GUDANG PABRIK', 'PELABUHAN', 'BANDUNG', 'JAKARTA', 'OPSIONAL', 'OPSIONAL', 'GUDANG', 'PELABUHAN', 'LAIN LAIN SAJAH', 37000000, NULL, 0, 0, '2019-08-22', 'admin', NULL, 20, 11, 10, NULL, 700000.00, 36300000.00, '2019-08-06', 0.00, '0004/TMG/VIII/2019', '2019-08-09');


--
-- Data for Name: trOrderDetail; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trOrderDetail" VALUES (23, '0001/JOB/VIII/2019', 'TRUCKING', 3500000, 1, 0.00, 2.00, 3430000.00);
INSERT INTO "trOrderDetail" VALUES (24, '0001/JOB/VIII/2019', 'INAP 1 MALAM', 750000, 1, 0.00, 0.00, 750000.00);
INSERT INTO "trOrderDetail" VALUES (25, '0002/JOB/VIII/2019', 'TRUCKING', 3500000, 1, 0.00, 2.00, 3430000.00);
INSERT INTO "trOrderDetail" VALUES (26, '0002/JOB/VIII/2019', 'KAS JALAN', 500000, 2, 0.00, 0.00, 500000.00);
INSERT INTO "trOrderDetail" VALUES (27, '0003/JOB/VIII/2019', 'TRUCKING', 4500000, 1, 0.00, 0.00, 4500000.00);
INSERT INTO "trOrderDetail" VALUES (28, '0003/JOB/VIII/2019', 'KAS JALAN', 1500000, 2, 0.00, 0.00, 1500000.00);
INSERT INTO "trOrderDetail" VALUES (29, '0004/JOB/VIII/2019', 'TRUCKING', 35000000, 1, 0.00, 2.00, 34300000.00);
INSERT INTO "trOrderDetail" VALUES (30, '0004/JOB/VIII/2019', 'INAP GUDANG', 1500000, 1, 0.00, 0.00, 1500000.00);
INSERT INTO "trOrderDetail" VALUES (31, '0004/JOB/VIII/2019', 'HANDLING', 500000, 1, 0.00, 0.00, 500000.00);


--
-- Data for Name: trPO; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trPO" VALUES (15, '0001/PO/VIII/2019', '2019-08-05', 17, 16, 'FACTUR-0001', 650000.00, 0.00, 'admin', '2019-08-21', 'admin', '2019-08-21', 650000.00, 50, NULL, NULL, 0.00);


--
-- Data for Name: trPODetail; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trPODetail" VALUES (42, 15, 4, 75000.00, 0.00, 0.00, 300000.00, 'B/0002/VII/2019', 2);
INSERT INTO "trPODetail" VALUES (43, 15, 1, 350000.00, 0.00, 0.00, 350000.00, 'B/0001/VII/2019', 2);


--
-- Data for Name: trPOMisc; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trPOMisc" VALUES (7, '0001/TX/VIII/2019', '2019-08-01', 6500000.00, 0.00, 'admin', '2019-08-21', 'admin', '2019-08-21', 6500000.00, 50, NULL, NULL, 0.00, 0.00, 'GAJI PEGAWAI');
INSERT INTO "trPOMisc" VALUES (8, '0002/TX/VIII/2019', '2019-08-19', 1250000.00, 0.00, 'admin', '2019-08-21', 'admin', '2019-08-21', 1250000.00, 50, NULL, NULL, 0.00, 0.00, 'BAYAR PLN');
INSERT INTO "trPOMisc" VALUES (9, '0003/TX/VIII/2019', '2019-08-01', 35000000.00, 0.00, 'admin', '2019-08-22', 'admin', '2019-08-22', 35000000.00, 50, NULL, NULL, 0.00, 0.00, 'BIAYA BANGUNAN');


--
-- Data for Name: trPOMiscDetail; Type: TABLE DATA; Schema: ho; Owner: postgres
--

INSERT INTO "trPOMiscDetail" VALUES (9, 7, 1, 3500000.00, 0.00, 0.00, 3500000.00, 'SUSANTO', 2, 0.00);
INSERT INTO "trPOMiscDetail" VALUES (10, 7, 1, 3000000.00, 0.00, 0.00, 3000000.00, 'INDAH', 2, 0.00);
INSERT INTO "trPOMiscDetail" VALUES (11, 8, 1, 1250000.00, 0.00, 0.00, 1250000.00, '041220100', 2, 0.00);
INSERT INTO "trPOMiscDetail" VALUES (12, 9, 1, 35000000.00, 0.00, 0.00, 35000000.00, 'SEWA RUKO', 2, 0.00);


SET search_path = "humanCapital", pg_catalog;

--
-- Data for Name: msEmployee; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--

INSERT INTO "msEmployee" VALUES (1, 'APR.1712-0001', 'Administrator', '1985-08-02', 'Bandung', '9817329873298719', 'Jl. Pangeran Super Hero III no. 29', 'Bandung satu saja', '081', '082', 'hendra@suher.com', 9, 'asdd', 2, 5, 0, '02552', 'AP1', 'AHM1234', 'Sales Koordinator', '5', 'A10', '', '0', '', '', '', '0', 1);
INSERT INTO "msEmployee" VALUES (2, 'TMG.1905-0002', 'NUNUNG FITRIA', '1980-02-13', 'LONDON', '1597532584563210', 'JL. CIMENCRANG NO. 1 BANDUNG', 'BANDUNG', '', '081326547833', 'NUNUNG.ANGEL@GMAIL.COM', 9, 'ANGEL', 2, NULL, 5, NULL, NULL, NULL, NULL, '6', 'A5', 'JAWA BARAT', '62001', 'JL. CIMENCRANG NO. 1 BANDUNG', 'JAWA BARAT', 'BANDUNG', '62001', NULL);


--
-- Data for Name: msHobi; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--

INSERT INTO "msHobi" VALUES ('A1', 'Adventure (Petualangan)', 1);
INSERT INTO "msHobi" VALUES ('A10', 'Makan', 10);
INSERT INTO "msHobi" VALUES ('A11', 'Massage', 11);
INSERT INTO "msHobi" VALUES ('A12', 'Melukis', 12);
INSERT INTO "msHobi" VALUES ('A13', 'Memancing', 13);
INSERT INTO "msHobi" VALUES ('A14', 'Memasak', 14);
INSERT INTO "msHobi" VALUES ('A15', 'Membaca', 15);
INSERT INTO "msHobi" VALUES ('A16', 'Membaca Puisi', 16);
INSERT INTO "msHobi" VALUES ('A17', 'Memelihara Binatang Peliharaan', 17);
INSERT INTO "msHobi" VALUES ('A18', 'Menanam Bunga', 18);
INSERT INTO "msHobi" VALUES ('A19', 'Menari', 19);
INSERT INTO "msHobi" VALUES ('A2', 'Aeromodeling', 2);
INSERT INTO "msHobi" VALUES ('A20', 'Mendongeng', 20);
INSERT INTO "msHobi" VALUES ('A21', 'Mengaji', 21);
INSERT INTO "msHobi" VALUES ('A22', 'Mengarang Cerita', 22);
INSERT INTO "msHobi" VALUES ('A23', 'Menggambar', 23);
INSERT INTO "msHobi" VALUES ('A24', 'Mengoleksi Barang Antik', 24);
INSERT INTO "msHobi" VALUES ('A25', 'Menjahit', 25);
INSERT INTO "msHobi" VALUES ('A26', 'Menulis Buku', 26);
INSERT INTO "msHobi" VALUES ('A27', 'Menyanyi', 27);
INSERT INTO "msHobi" VALUES ('A28', 'Origami', 28);
INSERT INTO "msHobi" VALUES ('A29', 'Otomotif', 29);
INSERT INTO "msHobi" VALUES ('A3', 'Bercocok Tanam', 3);
INSERT INTO "msHobi" VALUES ('A30', 'Pantomim', 30);
INSERT INTO "msHobi" VALUES ('A31', 'Shopping', 31);
INSERT INTO "msHobi" VALUES ('A32', 'Surat Menyurat', 32);
INSERT INTO "msHobi" VALUES ('A33', 'Travelling', 33);
INSERT INTO "msHobi" VALUES ('A4', 'Berkaraoke', 4);
INSERT INTO "msHobi" VALUES ('A5', 'Bermain Drama', 5);
INSERT INTO "msHobi" VALUES ('A6', 'Bermain Sulap ', 6);
INSERT INTO "msHobi" VALUES ('A7', 'Fotografi', 7);
INSERT INTO "msHobi" VALUES ('A8', 'Kaligrafi', 8);
INSERT INTO "msHobi" VALUES ('A9', 'Koleksi Perangko (Fillateli)', 9);
INSERT INTO "msHobi" VALUES ('B1', 'Badminton', 34);
INSERT INTO "msHobi" VALUES ('B10', 'Senam', 43);
INSERT INTO "msHobi" VALUES ('B11', 'Sepakbola', 44);
INSERT INTO "msHobi" VALUES ('B12', 'Sepatu Roda', 45);
INSERT INTO "msHobi" VALUES ('B13', 'Surfing', 46);
INSERT INTO "msHobi" VALUES ('B14', 'Tennis', 47);
INSERT INTO "msHobi" VALUES ('B15', 'Volley', 48);
INSERT INTO "msHobi" VALUES ('B16', 'Yoga', 49);
INSERT INTO "msHobi" VALUES ('B2', 'Basket', 35);
INSERT INTO "msHobi" VALUES ('B3', 'Bersepeda ', 36);
INSERT INTO "msHobi" VALUES ('B4', 'Bowling', 37);
INSERT INTO "msHobi" VALUES ('B5', 'Fitness', 38);
INSERT INTO "msHobi" VALUES ('B6', 'Golf', 39);
INSERT INTO "msHobi" VALUES ('B7', 'Hiking', 40);
INSERT INTO "msHobi" VALUES ('B8', 'Jogging', 41);
INSERT INTO "msHobi" VALUES ('B9', 'Renang', 42);
INSERT INTO "msHobi" VALUES ('C1', 'Bermain Games', 50);
INSERT INTO "msHobi" VALUES ('C10', 'Menonton TV', 59);
INSERT INTO "msHobi" VALUES ('C2', 'Bermain Komputer', 51);
INSERT INTO "msHobi" VALUES ('C3', 'Bermain Musik', 52);
INSERT INTO "msHobi" VALUES ('C4', 'Browsing Internet', 53);
INSERT INTO "msHobi" VALUES ('C5', 'Chatting', 54);
INSERT INTO "msHobi" VALUES ('C6', 'Mendengarkan Musik', 55);
INSERT INTO "msHobi" VALUES ('C7', 'Mendengarkan Radio', 56);
INSERT INTO "msHobi" VALUES ('C8', 'Menonton Bioskop', 57);
INSERT INTO "msHobi" VALUES ('C9', 'Menonton Film', 58);


--
-- Data for Name: msOrganizationStructure; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--



--
-- Data for Name: msPendidikan; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--

INSERT INTO "msPendidikan" VALUES ('1', 'TIDAK TAMAT SD', 2);
INSERT INTO "msPendidikan" VALUES ('2', 'SD', 3);
INSERT INTO "msPendidikan" VALUES ('3', 'SLTP/SMP', 4);
INSERT INTO "msPendidikan" VALUES ('4', 'SLTA/SMU', 5);
INSERT INTO "msPendidikan" VALUES ('5', 'AKADEMI/DIPLOMA', 6);
INSERT INTO "msPendidikan" VALUES ('6', 'SARJANA', 7);
INSERT INTO "msPendidikan" VALUES ('7', 'PASCA SARJANA', 8);
INSERT INTO "msPendidikan" VALUES ('N', 'NULL', 1);


--
-- Data for Name: msPosition; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--



--
-- Data for Name: msReligion; Type: TABLE DATA; Schema: humanCapital; Owner: postgres
--

INSERT INTO "msReligion" VALUES (1, 'Kristen Prostestan');
INSERT INTO "msReligion" VALUES (2, 'Katolik');
INSERT INTO "msReligion" VALUES (3, 'Hindu');
INSERT INTO "msReligion" VALUES (4, 'Buddha');
INSERT INTO "msReligion" VALUES (5, 'Islam');
INSERT INTO "msReligion" VALUES (6, 'Kong Hu Cu');


SET search_path = public, pg_catalog;

--
-- Data for Name: AppDashboard; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "AppDashboard" VALUES (1, 'Stock Position', '', '', 149, 0, 1, '', '', '', 12, '', 'stockPositionContainer');
INSERT INTO "AppDashboard" VALUES (2, 'Monthly National Sales Report', '', '', 149, 0, NULL, '', '', '', 12, '', 'monthlySalesContainer');


--
-- Data for Name: AppMenus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "AppMenus" VALUES (101, 'Karyawan', 'human-capital/employee/employee/manage', '', '', 100, 20, 'Data Karyawan', 1, 0);
INSERT INTO "AppMenus" VALUES (100, 'Master', '', 'fa-database', '', 0, 7, '', 1, 0);
INSERT INTO "AppMenus" VALUES (117, 'Barang', 'master/barang/manage', NULL, NULL, 100, 1, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (111, 'Supplier', 'master/supplier/manage', NULL, NULL, 100, 4, 'Data Supplier', 1, 0);
INSERT INTO "AppMenus" VALUES (112, 'Jenis kas', 'master/jenis_kas/manage', NULL, NULL, 100, 5, '', 1, 0);
INSERT INTO "AppMenus" VALUES (113, 'Satuan', 'master/satuan/manage', NULL, NULL, 100, 6, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (114, 'Jenis Kendaraan', 'master/jenis_kendaraan/manage', NULL, NULL, 100, 7, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (115, 'Kelompok Barang', 'master/kelompok_barang/manage', NULL, NULL, 100, 8, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (116, 'Customer', 'master/customer/manage', NULL, NULL, 100, 9, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (118, 'Pengemudi', 'master/pengemudi/manage', NULL, NULL, 100, 2, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (119, 'Kendaraan', 'master/kendaraan/manage', NULL, NULL, 100, 3, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (121, 'Order', 'order/manage', NULL, NULL, 120, 1, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (120, 'Transaksi', '', 'fa-tag', NULL, 0, 1, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (122, 'Laporan', '', 'fa-bookmark', NULL, 0, 2, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (123, 'Kas Masuk', 'laporan.kas/manage/manage/1', NULL, NULL, 122, 1, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (125, 'Pembelian', 'po/manage', NULL, NULL, 120, 2, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (124, 'Kas Keluar', 'laporan.kas/manage/manage/2', NULL, NULL, 122, 2, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (127, 'Perbaikan Kendaraan', 'laporan.perbaikan/manage', NULL, NULL, 122, 4, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (128, 'Laba Rugi', 'laporan.laba_rugi/manage', NULL, NULL, 122, 5, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (129, 'Account', 'master/account/manage', NULL, NULL, 100, 10, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (130, 'Invoice', 'laporan.order/manage', NULL, NULL, 122, 6, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (126, 'Pembelian Sparepart', 'laporan.pembelian/manage', NULL, NULL, 122, 3, NULL, 1, 0);
INSERT INTO "AppMenus" VALUES (131, 'Lain-lain', 'po.misc/manage', NULL, NULL, 120, 3, NULL, 1, 0);


--
-- Data for Name: AppShortCut; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: fileUpload; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "fileUpload" VALUES (7, 'Profile', 806, 'jpg', 'KAR.1810-0005', 519548);
INSERT INTO "fileUpload" VALUES (9, 'Profile', 810, 'jpg', 'KAR.1810-0009', 564567);
INSERT INTO "fileUpload" VALUES (10, 'Profile', 808, 'jpg', 'KAR.1810-0007', 479623);
INSERT INTO "fileUpload" VALUES (11, 'Profile', 809, 'jpg', 'KAR.1810-0008', 540456);
INSERT INTO "fileUpload" VALUES (12, 'Profile', 811, 'jpg', 'KAR.1810-0010', 469933);
INSERT INTO "fileUpload" VALUES (13, 'Profile', 812, 'jpg', 'KAR.1810-0011', 497383);
INSERT INTO "fileUpload" VALUES (14, 'Profile', 803, 'jpg', 'APR.1809-0002', 456210);
INSERT INTO "fileUpload" VALUES (8, 'Profile', 807, 'jpg', 'KAR.1810-0006', 511112);
INSERT INTO "fileUpload" VALUES (15, 'Profile', 813, 'jpg', 'KAR.1810-0012', 540400);
INSERT INTO "fileUpload" VALUES (16, 'Profile', 805, 'jpg', 'APR.1809-0004', 508195);
INSERT INTO "fileUpload" VALUES (17, 'Profile', 814, 'jpg', 'KAR.1810-0013', 514605);
INSERT INTO "fileUpload" VALUES (18, 'Profile', 815, 'jpg', 'KAR.1810-0014', 522712);
INSERT INTO "fileUpload" VALUES (19, 'Profile', 804, 'jpg', 'APR.1809-0003', 477005);
INSERT INTO "fileUpload" VALUES (20, 'Profile', 816, 'jpg', 'KAR.1810-0015', 528545);
INSERT INTO "fileUpload" VALUES (21, 'Profile', 817, 'jpg', 'KAR.1810-0016', 490730);
INSERT INTO "fileUpload" VALUES (22, 'Profile', 818, 'jpg', 'KAR.1810-0017', 512332);
INSERT INTO "fileUpload" VALUES (23, 'Profile', 819, 'jpg', 'KAR.1811-0018', 499441);
INSERT INTO "fileUpload" VALUES (24, 'Profile', 820, 'jpg', 'KAR.1811-0019', 547667);
INSERT INTO "fileUpload" VALUES (25, 'Profile', 821, 'jpg', 'KAR.1811-0020', 462656);
INSERT INTO "fileUpload" VALUES (27, 'Profile', 825, 'jpg', 'KAR.1812-0024', 450575);
INSERT INTO "fileUpload" VALUES (28, 'Profile', 824, 'jpg', 'KAR.1812-0023', 477743);
INSERT INTO "fileUpload" VALUES (29, 'Profile', 826, 'jpg', 'KAR.1812-0025', 637381);
INSERT INTO "fileUpload" VALUES (26, 'Profile', 823, 'jpg', 'KAR.1812-0022', 448369);
INSERT INTO "fileUpload" VALUES (30, 'Profile', 827, 'jpg', 'KAR.1812-0026', 446455);
INSERT INTO "fileUpload" VALUES (31, 'Profile', 829, 'jpg', 'KAR.1812-0028', 494713);
INSERT INTO "fileUpload" VALUES (32, 'Profile', 828, 'jpg', 'KAR.1812-0027', 427976);
INSERT INTO "fileUpload" VALUES (33, 'Profile', 830, 'jpg', 'KAR.1812-0029', 409841);
INSERT INTO "fileUpload" VALUES (34, 'Profile', 831, 'jpg', 'KAR.1812-0030', 429562);
INSERT INTO "fileUpload" VALUES (35, 'Profile', 832, 'jpg', 'KAR.1812-0031', 451437);
INSERT INTO "fileUpload" VALUES (36, 'Profile', 833, 'jpg', 'KAR.1812-0032', 459708);
INSERT INTO "fileUpload" VALUES (37, 'Profile', 834, 'jpg', 'KAR.1812-0033', 424551);
INSERT INTO "fileUpload" VALUES (38, 'Profile', 835, 'jpg', 'KAR.1812-0034', 409523);
INSERT INTO "fileUpload" VALUES (39, 'Profile', 836, 'jpg', 'KAR.1812-0035', 417190);
INSERT INTO "fileUpload" VALUES (40, 'Profile', 837, 'jpg', 'KAR.1812-0036', 490614);
INSERT INTO "fileUpload" VALUES (41, 'Profile', 838, 'jpg', 'KAR.1812-0037', 316311);
INSERT INTO "fileUpload" VALUES (42, 'Profile', 841, 'jpg', 'KAR.1904-0040', 497448);
INSERT INTO "fileUpload" VALUES (6, 'Profile', 1, 'JPG', 'APR.1712-0001', 90494);


--
-- Data for Name: karunia_sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO karunia_sessions VALUES ('650056e3f15f76b22a268bd1383c5cfa97857589', '114.122.104.174', 1566451252, 'aXd1bmF3WmxVckVQaDVacnZkOUw1VHVPMDRxU3M2Z3RTSy1RRjdaQ01WaFBfTnlKa3dFZUZON2Z1MWFKLVZEbXJIT0pZOUpRMGJWZzJCd0dwVkRid1VBengzcy1qM3llUVpaZndKaFloQ2NvRXFyNnNGaXNHTlEyMzN0VGdUU25oaEhZS0NLT0YzN1hRVi1STmc5ajVmY0pja01IbGZNNkIyMGthRVAzQWRLejVKLUd0Zk95eVQ2dmUxSnhZVWZya1F0dEpkaWU2SzJTdE0xMWlJRTlCNzBaZkU3MExGVXJvaVQxMHNDUUZUQ2dBWGJkc0xtZTdxTXpzU2YzNmlSRktUZkxWWHlTOWs2TkFwU3Vadk50VkUzekFQQVh0a3RFemUwR0g3eVJ2ZTlsREdEdE0wTk1YRldBN3hhN2RFMm9LUURhRlJoTG8tZzNTN0ZuNWlmd0c2Z2lWR1JQdzdiNVNyNnZ3UFA2QVZpRmtYNjItOUtjWWJxbkdEV1FiSTdhYjBxaHdSM1JhQ2xpbjhsazE5X2RiNmdhMGhaX3VVenJkb2ZUTkZ4V1BpOV9nMjFDYTRzZ1czbVkxX05nZVlTb2hlVGd5OEppdVBBMWZ0dnlTQTFQQVZIaDJaUWVmZnpHZmtPWVZFR3IyQTdwMm52cWVoeWh6STBDRTJLcHppOEpYT245S1VhYU1iVG4yNW1Qc1pxMzdXamotYlFPbVNEN2dLdTZ5cjdVd2NkS2JyeWRHcUQ3Y3ZNdm4zTWlEM3EzQkJSOERQU1hOckVoNDlwRjZQWjkwMTY5N3lRdnRMcVVjN200TTZjdktXcUR6cnRsMHpIdW5ObDBBcnZZQy1kaHU1ZXZ2X0hPR1dFTnVXdXdQOTJuLUh6WGhPMVhIY0xsY1Z2SGt2TFBoNkRLSGJDZ0pyYzFLbHVRV0tKQ3I5eHdUUnp2MmFxXzNNZGdNMzRFQ0RNM04yU3pKcE51bl9teURYVzZrX2FMdUJYVWJHRlVkcnJBYk51eHN4V3otX0JCT3lTMjQySHpJS3BmeTZWNGkwdjhVM1l5X2R2RUVMQ0hBOV9ud3lNczZ2MmtLSlNleDF1dklVY01DVE5qNXNNYW5mbURiemRYR3N3MUY4WXlONkVjTDFCekZPZlRTY1NIVVhKWjh3MGI5TGNuTkVoalNHQ1ZUdUhkTlVSM2ZGemRMZFRMbjRaVEFiSXYtNzd3UWtjbll2ZEZhd3VmOW9yWDNScEg1VG1RbEhhTmVGZVBMMmRUNWVqd2tVT1FNU0ZiQm9jaTdVVVdWcnNwalhsRWl4cUxjeWFWZ3hTX00yTkpCT212el9GdV9BYkYtenZONFcteGtuOWlOYWxuNWdIcVc5TmVhQUoteGN0Z0NoUFkweFlzcUc2dmVXQ0Z3VmUwNks5UWdkMGpEcWJ2NWo2T1FvNnZlVkNZWVJiMW1JU1B2ajdhV1ljOHRjeWl0Unl3dE1rQ2lVY04wTS1ZZVgxTTNlMU5iVmVsQ0xhbG5BS2FTUmhTbWZEMzVVc3JVNTFCVHNkNEsybFN4VXJzYVloYlgtQ2VieXlKd2JYYkZjS3ZncE9QT3ZvZU5fTDRzZUt2Ri1MRkRkcDA3eHVlWV8xRHVhb1hfWWpkZnVKQTdQRW1FNlRIVzZ1OFg5cVY0cnBSVnZDYUJ5QTZpZXJqcksyRzFDN25sdUUwX3pZS0hINlBsWkh1dDBBWVdkdV9adkx2NnEwR3diM3Vld2pubWNWZF9YRl9PdVhidGdGZkZoc19DZDl1Sk1mc1BNWFA4S0s4N01faXVNUE5QMTFJYUROY3FYbzA0c1VZbnhQTXk4MWFOb1BrZ0xGbW83NmV6a1haRkRDV1RYZUJLYjdxYjBXZlBLU0dWZ2lhYkEzWnAxcmZkczdVUEdpa3Q3bmxlN2pna0M1RTZJaXJDS00zS1FBX1FrQnd4cWhaUGxPV3FZWUpOWGVOaWRjdTNXZmlHMXE3NUFib29uZ0RhdDVfVlVKdmUtYjlmbi1lYVZSYVZlVU9Rcm56NUJkTFVUOFRsNU11ZVJ6cFpXVlVveTBXOTE5bmpWT2pXTEwwUjlyMEJMQ3N0VHZCakYwaVJEanJHLTc1UkN4b244WTVRaUxiaVl3Y0NQWldmSlh3VnU2N041SnFTWi00MFI0WVVlVFJUU1diSEZLb24yRExUc2NBb2J6dThIek9kdmpjSmFXby1zMVdEZF9hZVBCOU5OSXVZWlMwb2N0MW1fTFNqSjNZUzFVRlNOc0N6a1dkS0R5bGV6QW1zZVZOSkFEMXh5MVJTZ3NKQW10S2lMT0IweUpySkpwRGxVeTY3bE9BOHJ2Y1c0X1NadzY5OFZUS3Y3MXdraDQ1WmhCaVhCMWw2NlVfV1lsdzBuVEFJb3ZkOFBzbDBfel9hQ3JIRUtCVlNSYzBGSVlXdHN3NGZYbzlYWmQwRU1JMERzWmV0MWROM2ZjR2hsSndRX2lvcmNOZkMxelpOdmdydjEwdFFDcnRmZzFZOW9TYi1JUVRjZV9ES0hNTEpWSVRuaHVfVElsODIxRWkydmhyOHk4RWJvekUzV0FUc0dhR29oa1RWVFdiX041TTNUclo0dkFnTk5tQTVxUkllM3RTYTVZajF5OVlnQ3V2T0pTalpCMzd5NkJwamlWS2M0NEdGUERvMmcuLg==');
INSERT INTO karunia_sessions VALUES ('10ed71e29b6bb8fdf5dd4e49349d18bee63960c6', '114.142.172.40', 1566453639, 'ajZvUzNieVJMTm9FSUZORUh4RkhaVS1YOG13TERFNXZzS2x3WF9SRlJtazdyTi1XbzZvakpBYmlwQ0tuZ2ZQb0pHaW9ubjN5OWpfbzMwRXU3RmdiVjM5YUtSbkYxNGJZdTg3dW9hdm05VnhTaWp4UkNxU1VUTXZLYnBaMmVLRE5QQXYyQXpVRURVQ3pmaEsxQWJVWUlnVmlNN1otNURQeC1IcjB5TUc5aTJ0S3JnT2NGbkExN3JfemhMSklSYllQRWphM25iUG1TZUpLclZka1plaDJMMm82amtnQldKX1FmTGxoWldlZmtURW56d0d3QUhpWDVsVnY4R0VxUWplcFdScmVhb1JSN3RVa0NmQkp4LTNZQWx4NUZiRUhiOXV3NmRTeEF0Z3podnpnai1KY1o0alhITjU4M2FXajBYVFlSUlNhbS01V1hnaTVYN1dxaWJoc1RubmFkSGxNRkh4TTNzS2lUMllMVTV1RWlzS2kzeGJwajJwdTcwYkUzeUktMlg5WWc5ZVVPUXlJVS1XZlN4Wms3bmZTZFJybE5nZkh2Vnc2ZDZNNlNFLVpDalhyWHp1a2hDTVZfVFVlT3hpZDRDNW1RSkV6LXgxVG5Ob2FzcGg1QUV3bjRsX1FFWjlSZnpTVG5FN2dtZm5DTGNLY0kyS3JOdkdOUnAzVlVTRFV3UFc3a2VJRWhKalgwUXotY0JnaTJFMWpJam1aSkhZWHoxWDM3MHJPNlNibldoaWtvVDJpa2pudlRlOExtbFl6VndsWFhRSmJ5S1A2bUs0S3huN1N1QnlJRVpLd2huQnZiRlRjdmFRbFNBMkE1LUhUNnF5SnAwV1M0NG5QQWNtTjhtRTZjUk1UWE93aUNnMVhpZ2ZLMUhqS3c5N3dBd0tQMmthT0ZjTEdsRVc4YlNvQ1gxRHZCOWVQV0Z5ZkhwNFBMSHhyTGNqSFUyVnpYRklPZ192ZkZzNWlFTWdEVG1iR1Q0bFplLW1jT2Y4WUlaQm5LMmhBOU1BQ3pTWUpKWjRZbjNrVHBGTmZuRl92MmRWbVRlYVMyaE5IUHhneHdSSXo2ZTZTdnc2WUF1MVRrZHI0UlpFbzBSUFNfbG80WC1Ca1oyVDZKcDg3OE1uN1JVdGEtNFZXM21JT0tnVzM0ckdVbDRUT0xlMEFPSzIzNDVLX0FJd0lFNGZUOTlZTzVHT1VHRWc3eVdxUjN5RFEwR3gyUm95YTBnWjNjV1VBQUlDMTBGS2xRX3dFR2VCQzlkdTBCd1o4cGtjMmhJaW5BZ3loZHY1QUZsbV9ZV1VyQ0w5WV90Vi1fZ0NWSUROdUZ5YkFlUHJBVXRMLW1WRmpNM29LNDlaMkFISDJBODVZWU15aVFocHQzWGwxaURHcHZ5a2xkWUxnRkJwVmNkQ1lJMURvdlFfUWNUc25WX0YydmQ2ZVpWRTl6akVCQ3VHQjEzcXBCcE93WHhHVHBEbG1ZV0FxWGlaWkU4ckZkdzUtSDJXcmJJUnFuYzFoRXdTTVpHcVZzVFpSRTE5WFhnWDZxRHYyeFpCbzZhQ0tWM2cwTXRMQ3lxeTNIOExxVVNxNkdOVEFVNXV4YTluaER1U014N0Y1QVRJdlNfdk5wYmtQaktSSVhaaUN0aWRzX1laVVZoZnN3VFdNVU92b0gtaGhReGNCZ2hVdVZmeFdIOXRTeGI4cDhqTUEwX1ZMVUlQaXpZajlsUElVVXJUYVZqbXU5RlBmTTVJQmVPRHRlMldNR1BNR0t3eHU4Qjc4OWo4ck1JNHJEem9pOEJDMzVsM1F5aVRXcm4zVzduLTBtQmJfVW81ZDV0c0VNcFVOQmFOcG5BXzlWenl5RENlSnFTUW5fT1FmUTRXTlVKX0xDaUs5X0I4VHBkcnoxQ0dsSnUxU3dQYU43d3BncFdqSWlLV2tkSXo2R0Y5eGctSno3cGtMS1FmSzhPZWRZdlVDVlRsclhLUWw0d29Id3N0TE45cHJFMlRDbDR3THdWaXJqTTEzMWJvNUZXbElNVmZDQW1hQUlGNEE2NW51S0twZk5BR0RySDY2ZnpUc2tkbDAyU3ZENF9vMzZ5WmotV2VMMS1xSTFLNzBjSENnaXNPM0dVZVVMbUVhV3l3aUNoTHhhd3dCajlzQUplS0NZdDl2anJleVJaWXFnd2hvN3JibW9fQUNLTEtSdnhDdVNwZFJpNzh0V3RxSDMwWjM5OVBfajJIb0ljMWJLNlJrN1VYa2xfOWlNMThuSXhXMmt2b29wMUdCLTB3al9SLXg0eURiU3lwaDlTa3VtZ09vdEFQNWF1WExiX1F4VnZlci1QZzZSQWZQUUM4ZndaUUg0MlY5dGpBOGpZd01CV05yM29yVkVyZTZ5azFLYWdmcmVNT3U5eWRpUVNSMFc0Qy15bk9sX2Z3anMzdVJNQ3BVektVVVRndGVKaGJNem9yT29ITW50WWhCUDJvTURDQXRISXVGV0FLdEtmeDBkZ09jZE1fb1BvNE9CTkUtMXF1Wl9JWFBnWUJWTkJrUDByQzU0bUZRLWpBYlhoOENoODZhZnlnSDJ2M2NRNXJzQkNNQnpmNFgtRzJpNXpFdXBKeXpBbG55VEVOUEtaajF5S0lIbnR4MkdUR2hNd21TcUROV0FQWFg3ZDdXZVZQbDV5UUpEcHFKd21ESmxDaHJKVEwtRUEuLg==');
INSERT INTO karunia_sessions VALUES ('107874ceb4a0d6706877d358c05c8f0479d75158', '202.154.40.62', 1566455394, 'a0R4bXV6UWpaRmE4YTU4QlBKd1I3bkNlUlFPS0ZHeDhoTWNoRVlvTEw3STMxT3dCUk1wdDROdUloSzZPMVJzci14cXNVV1RUbDRUcnZrUXBlNFYxWVFyeFZSM05YeEpLa1JteEZDWjdzWWcu');
INSERT INTO karunia_sessions VALUES ('a1273794c724da5358b80069f0c3d4260ac37d93', '114.122.104.174', 1566452954, 'OFdzbXlRS1RqSmR6dURfU1RkX0liRW1PRXRhS1lCSkhMTWxUM3BHNklRNHl0TjNzLTRyVGlkem12dXRMOTdjVXJfa3YtUXJtQk96a3c3a3RBYkl2bGJxdGUwTkxMck43RzhkM0VtdHVaQi1FVWJOSzRTLWxWaHNKb2ZLZ0VXOWhoWW9hN1RVUDh2LWNJUF9LZGhBZk9zdkYxdVlpdlV4dmthRDVndF9TcUZJY2tVSkkzem9pSThjR29lbnVJOWJQaEc4bnNhWVRycV90Q1k1WFJtcEZycEFCMElNR2xEQlBXT0RtcnlEd1EwaGpESWJENXFwN0MxMFlCQ0ZBNGJXYUhXNVBUcU9Fa0NDWWRkSGdHckdDcC1wNHZfSnFYY3pjeUVxM2dnWV9BMWRnQXAtZnBhT2RPTFJUTDg1TUFCTzRVenJudlZHcklZSjVxTkdjbmtzOXhjY1Fta0EtTkRnOU4tMEFoQ2dnZGJwcUM0ZHg0dElmUVRMbTBzY3BsWnVKY2M5MU9NWTg1blVtNk1oTmUxZ2Y2QkdDWHdrU1RhejQ3cXlWYTF1aWFlNmxEQlRhLTNHY1pQd1MtcmxFb2V6eXdxeFZ0UkRsd29GelBsWUhYWDJ6Z096el9CRFJqVWFVSWFVRXVhRjlMb2hkYnpDUXI2M1NJX0xtclhNQ1Vtb1BHWWNIbGJRd2V4WnNEUDV6bm4yekV5Qk1xdnl2N2NpZk1mYk1kV2s5QnRxT3pUOUlXSF9iYWlLT2VCOGRuS25uQlNlcmJHZjZ2NEpuZjM4SXBoMzA1NzVCRkttRDVNeHlacHI5V3FqQndEd1Jac1phd2JENjBpcm05Mk4wQWJzb21YYVh6UWYtYUZWa2F0NDdCMGhiR1dxOHNpbEg3LUxKQVhycU56eDlxeW1ybm1DX3daTlo5Sm40cGtoUlIxaWV5T1FuN1RzNl82Qnp0aWJFTE9TaGxOYVNfdmFSeVdFYnBtN0NJR1MwY002R0RxLVViSHdwcFVlYzBMSE9tV0hPVUVvdXk0VlpUX1FYWU8tbTY3cGlMMWJoNi1COFFjVXRYRGZPSFBIajBZSnpmNUJQSGRwSlFfNUh0YnFqUVc1U09pQzNqbm5mVVZ3dzBRWTE1UTQ0UVFhQWZWTEZYY0Fkb0dKckp0ZHROcjROZklhUDhna3JzeWl4bmE2MEJKcUxEOWdrMDlCQlNXekVRbGRDbGVLV1JsQnZTYVk1Q1ZjczlYLVdPdndYOUZSbUk4OVg5VVZ1REhISl82NF9VNU43cy1iNnZtS3RlYmptczFfMEpWclplN0xZX1hEbl9GeDRteVphcGZjQmhEbmV0cFE1aEZHd0thb1ZtMndxSm5tdXVjdlNYQ3Vhd3BtdVJOY1pxNVo2YjVCT1F6V3IyMnNXMXo0YVZvaTNpZkctSHo5MzZzZjNycFhJTVZBalVobmFVaGhoenhMbmpHdlR3dmtHWW5oa2tNSEhvZE5mUTdMVFJsbi1wM0o2ZDBsQjdqS3QtRE90MmVfX0drcllCWjFPOEplN1MySnhtd2lIVm5xZ0g1bHE5OS10cFd5TkVDMkVqWkdPaWNvZExxdHJSYXpkRklSRGlwNEFPc05udFlFOUdPeEdLakI2cjBIR011U3AwSG9tOUFDeXZOUE9wOVNHUUlLSzBZdXNxekpta2Q5cUhMakJPaHE0OTQwa2FoSFdxT3NKTmU3SXdfTkt4MDJuYl9TS1VkNlRJLUQ2RmVoWmZnQ0l6WmpBd3BLbmJnZXJJWFZvS0NiaFVINVhhUllydzVWR2NHQ25xdlhKbGdJVmJiUXhpN252akxqVFFyWmhCVTZyNlRRaXBvd0Y3Y0JRbm9NWXpEOTBySlVtc0cwSHNaeWQxY2JWcTlOMmRxYVdoWHRTNS15YnZZbWFIZGFNS3NUUTlDMFIzT2hyQ0lvMUl0YzlsVy1Ga2FqTDE2U0RQX2FDYVZzZnpiRm9ITkctNGFIeVlhYlQwS0I5MXVlajY2RHpxYzA3YV9GZTNvQ0FEMkJWY1Fmb1VMY1RoZG5qQlhQNGI3amt5bXhEVER0aWFWSVYyeGRSZmZUelRQTEliQmZYa0I3bm54SGtqZWdsUnhDUmhuYVlpNTVfcVNYaFFkcXlpR1paMlZZaEhCRFNla2R5ekZ1MHJIOTBzYkxaSmxTbUNnWkNNVG1qc095aGpoRkFCNkpyaFcwT1AzNTN0Q21CY3NIYXUzYVBVS0RzRWJ3R2RFOU9BNFVzUTJKSWZHTTlHVUh6TzFEa3BUWGFOeUJBWjhhUU9JZ1FsdVc2NHpxaWhqclBBSEJWc2UyVGhSR0d4dGJJZk9ZYzdzT1Yyc283dkFhSXlicEV2aVJGZDJreXk2bklBRDVJUTFkbzVzeHd3V3EyUk13WC1qNlJROEEyUUF0RlNSZHJtRXA0akVuZE1SY1B2QVhEYk1rbjZWbXhsQl9abk4yMHJPbndwNkRTS09Fa1JoU2xXTzJwUElJMzhnYTQwakVhak02WXJtaERMT0h2eElIMVd1M1p4QmpMUWVSNUI3MV80d05FNTNNc3JSQ0pxWDd1TFdidS05Q0pRZUF4X21XVlc1QU5tZ2xVRVU5RzROemhodXAzRi1LTUZzU0VtajkzMFkwbW9vYWhFaDJfU0lhaWd5ZWRRVHlyZ0EuLg==');
INSERT INTO karunia_sessions VALUES ('b9c6d9f456496135fc352a0cc8859b07a09ed89f', '114.142.172.40', 1566449243, 'SmVlMV9KOXFkbE9EQ0toekNNaXJ4U2kwZ3VYanVTbHB2dktzTTIwYzcxMEs1bDdBQXllRWNJbTlEbWlXaXAzVHBKV0FQdi15cDdLZGIxUEZXWWJGMk1HSE0xNWt1QzlyYXctbFpQUkF6bzI0TkZBZ1I0MVFBOXBWQVdUMmpqV2J5WnU5X0M5Q1g1NjV2M3RwV1ptNkdaLVRrSVVWTXNXVWZUOTI2VzNHRFRPekRpMGVQZHhEWEcwenZOdVJ0amw5NU9Cd0ViZ1FNdG1xeW1DQWhlUWdFa21TNW1iSmxJQ09RZ0p1eTJUcTZZYUF3UG83dGhfR0xDVGp0RVBRNlNCU05EX0R0QjZMWFRQTVJXOHVNN0V6a2xRbTNXWnlZdVMtejJ3ZmpIUHhraDhXbWFrMXFHUmxwTEdMc0VRUWd0THQ1cUg4M1dtR0ZyRnBORFhsblI3TE53MTZWNGU2TFRrOEFoOUpIbjdBYlItSVBveDhWcmZpT25fbEpNVktTUDNiVm44MXg5UV9reFB1UUtBbmFncVo1dURYYnhCWHY0dHEyWHZMdTI4cVJNZGx5U1FROGFmZUZLM0tnZnZCbmtfSmh5R3ZCaXNVNHN0dGlaeG4xUU1FbE1VWEtyRVNGNnpuYVRjTDlfdkdDRFpKVWFPY0w0TmVUOFFhSHk3STh5MjJEdy1EalNkUEticGJPcVAxcTJlclRKeFoySkdtRUFoMDVwZmdkREFpbW9DRG15Rzl5RnpxaDNDZDR1M3FJZU9CTVFqMlJON0ppeG1yQmNXcW1VWi1zczhsNUVYNnRlRHE2Ynp1SlFud0dsdWhjc0t6b0dhODI2Mkg3MU5iajliN1hOSGRLV00wMWpzYUh4cVRNbUlEZTBiYWlxX1FhQjk2ZXhYcnVuRHJ4ek5NVjRtamwxbC1wUmJzcUN0YnF4SVl2cHA4MXBhX0kyN0s2ZjRlMUVqeFpqNXo3Yk5tSVBHWFo2XzBDQzZiM1hKRGs0MnZaU2hwdVV6bVlhb2swMlVmamt2cGJyZXZ0aTA1OWxZSDR6QXFjcFRaZEphSGFoM09CWnV1Qlp4ZXlJdHJnR1l6R01Mb2htWlcyR0h3cXU5MDUzZzVuMGNtTzE4Tk5HM0tEYjh5M05lMDRBblNVc2VoZm9rc0NRSUE1WGVmWXNoVFp0Y0tVQVVDazEwMDR0N1IzTUxLVEp1R3FtcmFNNGRWVkRMbl94a2dXWXAxRXFYNE94R0s4UzVfX1dEMUpOZXpWTmVjLWR6TGF0RnVTakxWQWxIYjMydG1DVjdSWGc0YmsteGNkNVZhMXA1dnJUNTV6U3E2MWJuUTZwMnNvbDlMbWxNT2hHNFNlWEZvUU1xMGFYMFR6eE5zallOMUc5V2J4aHptUC00ZjR3OVAxblpQVUNYbmUwZlV6dFp1SXdnWFRCS0l5UmhIQVVkbGJPNVhPUDhwaDBIUGdUVlM2RDZEUi1YbnZlbmYxSkhSSGg0Q1lSaF9BVk5Xdmd2aHdGdTZTZjBsaHJLT0RjMUJ5WnlPNVVLMm5uYkJodF9wTkRKV3pWbTZyNllPUlNfNlNkcl9XY0gwbzkzTnZ4WEpORUdEejFyblpGZE11WFplMzQzR2wxUnNiajJKM2NpNHFmUmQ0UHlDeENMNS1LQ2VUYzFmcXhwTU1VYXpZek15ZE95TUVHT0h5VWI4X2hXRENjbEZSS1F2UDJON0JHMlRZMzNOSFpQVmNNbW9ybDhQRUttaklHMjduV3VXSzJXZDdrWjYwSU5ZZ0hXX3Mxd3RJWlZWMGlnZFhMNE5IM0VLS1BucTBGbUMzUWVLR3ppMmJuX0Uxbi1yZGloWTJPTFZhTEp1UWE3S255TWx4STJDSldRTDIxcTNCYUk3RjgxdjNnbHlsb0o1dVdZcHlla3o1OUVJbHQ0bU1XTjlvREJySHllbUtjeXk0bl9ycUtmYmw1X0lPX250OWZjQ3QyX3hKanlLT3IxbGV5T245QklFekFQSUg3Y3FHdGs2aW1XTWNLMklYOTRTZnZjeHBZOWZXaVRrYlY1cHpsMmttaWl3UEZRbi1OMkhYR3RXQ3JxUWJzRGRrLTRmRW1wQ1BCTjQwWXY3aGp5cXZjemJKbVN4RHFRY3h2VWc3M1l0MjRkQ2NoRkl2Ujc4SVVJWmZuN0pCRS0xVDROd0ZQRnEyeWpPSXVLN3RaS19mT3U5bkdpMGdwNVpMZ3AxQWIyblJmVVhqb3o3VmV2VXJDUU0zT013Z2pEU2FjNWg1ajdnSkZwWFI0TDA0NjFNZXhTNXlBUDRtbnVGOXNDNDdCUFpkajVETzFTSjl2dVpUcHBLYldqNklfSXByOTZxTjV4UXJKaEJ1Zl9xU2c4N3p0VDB0eGtFNEx5UlNDaFlEZGFCd2ZZa0d1amZVMFdMejVWNnlPUTJndXpGbExLblF4X010WnROa0tobGNDMEtITmdmMFUtY1MzdW9sZzlxX0pqS25hQTl5UHo2bnNsaXBtNm9BV2lkN2E2REN2X3BTNEhwSUpJSXpmdFU3ODhsTzZ1dXZzNXplSmVlMXZrM3dfUGFaR0xLZXN2SzVZeUFLWlRCVWFoOGFGYkEweTFVdGptNUNKaGMyR243Yi1JejYxM291SWMzeWlYaFNMemNYLWltUHZscEVETUJMa05sZ3cuLg==');
INSERT INTO karunia_sessions VALUES ('2284ce156cca881e0d8d51e28af53661e0211893', '114.142.172.40', 1566449786, 'cEMxN291cE1MSUwxTmpUR2tJME5lMDVueGdGRFVkNjVmejlJQWtIeDVlNnhwTGUySXdTMmkxQnN0NnBUYXhzN1EyV0hLcU9KZXFIeUUzMThEaEs4eTJhcVBvQUdVSkJrajVUaEhCMHREZXhFOHRWODdZbHliVzFwc3FyTFQyYmJQa3pIMm5JZi0yeldFekhnS1k0dGczVXZidHMwdDkxc2plY201THkwMW9Sd1dyUkt2VUVjc0d3aGFYZDRHR0NDSVRpd2FnWkZla3RYOU1aMldGaTJ6RW1VZXVnS0ZXOXFmQmZxajZma3ViWTkxbGpVS3hHeHM5cTJPbmd6RXVpNHFZVTBRc3V1MHpTc0FfWTNZdU9qeFhmZnF1eVNMUjlMR3JXUm9WUTJvN1ZQdHJKdFJ3V3ZyYWxvdVg0a2VqekMwM1VSSlgtcDhKMlBubF9BWmtiX3RackR0VUhSYnZNTVlnS3czc1B2cXZKSzI1dTQ0RVVFeVNqYUdidHE5X3dNVWktdUc1YmJrcFhfVXplY0dncFBOZ1BidWpoY0Jwa1pYUGpJX0VsNFMwczlRMWtxZW11SVlGVmVCX1oxLW12WDNVdEduSjNUMmpvUXNubzluOGN6dnAwcDRabWhtWHZta1JBWUVGWEE1RnRLY3J4UDkxd0hUOFlqTmxCVkdwZzRzMWpseXRqS1lHWnJoUHNVOXlBU3U1aWVtbENrTkJLSy1SVWV3Sk53SzRvb1dYTW9BQnEybkNLZkxPbUV5dDZXb09nS1dIbE8wWktKeTUwNUczTHZFQ2ZfVy1wVndtbER3Y3pnNGN4QUE0V2tiQTE1d1ZWLV9oNzZTTG9fLUtaQmZ6cENmejFxSmhIXzJPQzFoeUJ1a05CaXQ0Z1lYYzdDa1VXYS1NNmZxTUFRX0N3ZzhXejJTZzBrbmxobGF5b0hLeHhjNlBleUlZTjBpSFEzWVNKLTBKZlF4bmJjTlpFQzFkUkRvYzhlYVFmdk5iUGsxOWJFVzdLazljd1pIOG9NU2dpOGdUcGtGVmVtSE1VeWxQZDJVWEZpdFJzYXQ2Ri05YWQ3YzBFZjdKTTBtdUV3a2JzOHVJMVA2ZmtXSjc1ckhEYVZHdVFlR0FNYkVsYWNlWFN2S3h4Z1pIYkFiSEVKRGgwaUt4a2ZvZXZ5TEl3V0IyWV9BT3ltQk1KWjRVUWxtTGF6RWlLTllTbTRRdXY2OWQwNnBIM2dFekVZWjMyVkRMak9JOHh1X2JOVi1mRzdnYjdHUUdVT1B5WWMwWjhYbFI4eVZNTFpVY2Vvb1JjYnJGV3Y2ZW1IcG5WN3o0RUNvd0V3R1JyTDdlcVhDYnBKa0NjMkd0TWFhNnp2a2F4VktfLTZqc1lBSEw2cnZqYXFlTF9SelBUX3lxREFDaXMwOHRBS3VuU2NGY1lzM0JnbmE4TG04WGZfdzBwb3EyTGpJT05GME5DOWtETmZOQi1tLUI0VlFkcXRSMUd6UVpQZFFwTXgxUnVzYWtDMldMMHhsT3pLZXoxNjc0a0N0aWs1Q0ViMDFMWTRBVGstVHZ5aVVEWlBlT0pUaTVxR0JnSVRBdUhYUkp4UXlqMi1QcFhqOTFNYnkxV3VSV3JIaXdDMGhZcnJsZWFtY1dqNXNablYyZE85THBGZHZoZFFMVDZJUFNpN3FkX1F5QXVwU1RTX1FYMXlvdWxhd1UtLTcySFR6Vkt1MVFJeWctdXl2YzI5MEhxaFFiRWItUzUweEluYmwySEVQOTE2M2Zlc0phSDNOaEM2R3dIdmRKdUMxWUNhd2M5NTdZc1h6LWhaeVpjQXBYaXpkREQwSW9Gb3V5NEYyaENrZXRDOHl1WVNhWHhuaVNIajNhcjk2M1I0dXVybXV2a1pQVElVenhOR21WajZlMzNTSXlodmZ2VlFUSExlQkNXOUxCU25WbVlVWEpPN3FfT21yalVfUWVZYnNlTUh4VGhlTWhyeDBGLVJBWk9qM3dfb05ZcVVRdWdQVFV2OXhodk5mQnAzWFlYYWVuRUUtM0ozWXZ3RXdncFlHRzJ3cUFocnRIYllKaURHR1BRZjJVVGlRXzhaZERSTjdZanRZdXI2RTRPNk5ocHlRc0F2MWE5b1V5RFdQMzJUNkFic1lHSk1wUmpqTnRaa3FESWQ5bGVWemwtekVTd0hkYXVEaWo0N3R3Uml1Y21jbFRyS19TYWZfQmN2aklneXgybGk1RmxxcmNnNWo4aVJ1VlJQSW9Dd3BTYmpyeTV6QVN5cDNrLURoT1NJOFc1SWVHZUZjWXZmTTBmTFB4eVNqMF9CTTJqU3FPaGxJMm9DQzZyTFBEZ3hjaktxeWpxMVA2VkowSVp0eGkyRFBzNXZrSkM5UDdCVGNudlNnRWpsUFRnMl9qTjJnWmwwNXdWbE1HQ0lSQ2I4WE9lQldkdDRrUzhqd3FyVTVMMWkwVXFvR0xIc2U5OHluaTR2anktenE2cVBUc0NKNXZfYVMydVNteWVhdlN6WVdLT0Y5NDZPdmZzQ0ppbDdCYWZQS09zMzBtQVk1SUxlUFFpNmN4UVBDVzNDQno3ZFc5TklHTXBGTXdWdjhPZjhXcGhpdnFicTdHWURHS0FOazhNbHV3SFBCdVZJS1l1bHZ3WE5zMFBDVmUwX2dGeE1JY3JES1dlN0MwYnZfSGdycEEuLg==');
INSERT INTO karunia_sessions VALUES ('6d711e1606d4b8677553ec9f22639edecc0aca94', '114.122.104.174', 1566451985, 'cG4yUXg0b2JuNm9QeEliQmdLSFdTM3VVUW1oRHRiNWVyZlRnbTRRYzdnWnNIWUdvd0E4TnVISHNjTVhXSWxQQjljSlBCRE1kOWd2amY5a3FyOXc4N19GaDZKdWhXUlp5dGxfZnpKZ2ZmOTlrV0hIbEh2ZDczTDRQMlNuakhlNFVtQ2pUMkU0MXJvS2pQRDA5TkwxN256SEY4eW5rRWVPR1h4T3Y0Y1BCRVFpbk1CczhnRUstNnQ5YWRiQVRCQkZkVjdteU82WnBkVjMzODFObGRxWURfUUdTaHB6NFlIUlVRaUIwUnVLUjRVNHZid0FrOVdhcG1Nd3FrZ3paLTNRZnVJVzNMalMybHFvdjd5NTNpTlFVMlI1bkJFOWYyYlZLNTVPWEFIcUdjb3FoWTNMSDlwamdoR3Z3a3lNczZBMi02NTRSaWsxRHJ1U0RvQUxkX3drR1k0cWJ0dnRPN3FqRW54dFljVlJCMUdzeWlXOHVSVlFIeFlNWGl1ZnloclRPcW5DcUh0VUZLTWlGeTFpVldoNEo0S0d0dXhPVG1YOTRuUVpmUlB2Z3B0UG9wbDFhbHk3TEZ5N1diQWtGVHdwR1JLT1JoMXhESDBvb1FhamxDYl9TZHdkTGQ3SmF5Z1F3SXJjLVdxWmhHaHZwblhObDlCX3RhQnRXLU9id0dfUFNnNzJkWkw1YWhHRTRQY1l6ajJWNExBaWFxNVRjVEk3a013MjlvR3dlUUkyNVp4RmU1ZmwtZUUtWDB1WHlsTEZfV2hnNTB1cTczU25RUEtsVzlXbUZTeXBJcl94TTk5RFF3WXN4TmVmRG85cld3UXRrSHV1a1oxQVR1YnBoTzZBemVMSllselk4NG1aMUdnZ1g1VmVWVU9Vb3dCMVBJbF9MQ2lwaVVuekRscWJYNUVfRnJhMUVtM1RuUDhMOWVHeVBmc0QwLVFzbm9mNTlDbFdRN0xobVc5bzJPdWg2QmN6eDVpOWw1OVZrTGRLWHJudU1jTHpPbmJKdFFkZUFrajlONm1hWHdtNm1RVFZIelNxcWhIS1I0VkdONWNLRzFnN3RhYURQWk0ybHBEZ1VqNDREVGtXcXhXXzdPR2VlLW5VRVpXT0ZGTVpQcVlRLXA0OWNVZGFlbUdoZ28yMkNCYXlkay1IYkdQQUdvUW5tenBRU2d6VnNmRENtemZSWkszWEdGa0RrdVVCdDU0cDVGVmIyaFNTT2ZQem5CSjBhaEhEdGJ5MDNEXzFVdUtacFdKeEVwRVdUWEItUFRMeGRsdy04ZFBkQWRYQ1E0eXhONHFHY2Q1WjZzS1pycnF6dU9aU2Q0ODdLYXBmODd2dUdja1NPbG02VV9maDN1d0tvTm5RVVJleHk1UmZvQi1MZWJPNm9KQWZuVFFrQThCNUpRUzE0cUUwMVRhT0hIMDl5MVN3b09nUTV4elVpUkdKWFFfeTl6SWNLRllVV1hjZmNYSmF6VUJteUVPX1ZCTDd6TG5fNVdCazhLdUZvY1lqMnBhU2prTDlIbEx3ZHpFVWp4Nk5SN016Z3pOZUpDMGl0MWJhQ3p0U2VvNHl1aGI1dGRPQWxYczRDdEVNcFdmeGVRTVY3dUFJQ3VnaVpZd3NEM0hXYlhEY0FxUGlBdDRRNUpHNW9vQmloRkNmemNsN1kweDlRTWJkZS1SY0Z5cV9jU09DcEJYcTdIaVBhTXZxOVREUjgwMWZ0UGp5ZDY5c2oxZEpJSHpZVUVWbGJkOFV6c2F3MGZSSTZlaUpycGFSX0hVSU12d3ByZDZaNS1Vc3VicVZuU2FaLS1nSDVEaXZmal9YbEhoTFprZjlCYnV3TF80bG1TNXF6WEt0OGdjdDNEdnBlMFY5QWgwajRsbzcxQlVuYlFtZDZhUDFWbU5RVVhrcFNmVmgxanQ2dnFoNXdoZXpNTURoblRBSWU1dmJoaWVpeV95a0dEd1hDTHRnMW9tcS1GQlhHeUhuYXNpTUQ5UW82OTcwa1RSTkVISTZHR3dBNmw3OTNtcWhIOTVvWTBfdmI4Z2poOWs2czcySkc0NXJkdS1Yd29YQ3ZzVWJBbURkSV94ajJXR0h1cG9YZlVGb1JZNWpxcGxkZWt6eXJydmltclhMblAwbWlKbjhuRjhULTk3Z0h3dndOSVpLcXk3cXRiOV82UFE3eXFzNmpHUEk0NWpFRlUyaUcya0VpQV9VNDBWZVVHYzhQYW5uWXRfWXl1eVc0N3Z0Ymc4WXJHVWkxZjVpZ1h3N01NZGxCRm5HSDNtVE1WTnBhd0NVVXdSTU5ORkFFRDVWY2EtZFQ1ZEs4T2tqdV9uR1JiZFRyRXpjUlppX1JKS0huR1lqbVRDcWNlRVFuZXAzNGZNTHcxZllkcnhyWVlfdjdUeHdzNnVSTS1ydXdxa0RsbkVnV2phWTZfREszaUZhdnpTOHUtdUVFS0NQeEtESy15N0M3VWJCdGt4TEhZbmlYZU9qM1d4bzdwWlZjdlF2ejR6cW1JZTkwekRMUnlISF9Sd3BnRmh5VmtPNWVCdlNkNEdheEVmb0RvNzMwd0RPXzdwSWdrSVBMYjZKS0p4bVE1VnVoUGRaSVdMb3Y0dHRCWWxiTEhnV25HTTFzaHV2YjNJSTZSZFhjMHNtUWFoRXJ0RjdlNDN2Sk1Xc3N4dTBxTnlFa3l0MFA1b0NNVlEuLg==');
INSERT INTO karunia_sessions VALUES ('b98165cf901237cb5889c1e053ab9b4f7ce3752e', '114.122.104.174', 1566453444, 'N0s2OVlqVmR3dmFRVkp1R2QtNG9hY053N2IwbjhXVFRzaUhYQTJjN3BYMEViYll5U05YeVo2clYycjc1Yk53b3ljVUZ5SGVPbzljczBJMTd6SnpFLU5BeGJITnVHU09KYnJEU2ZPT0tCNVBQZmEwZnVHbTFWQzVaUEs0TVJPc3RoM3A5S0gxaVBZLTVweWx3bnc3T28tNTNWN3pPSVZSRXRRNnZyZVN1bHFHM3hJZTdWRURnWmxQb21IdklGOWxvYS1ST3ItZlkyRVZtMWpOc25NTklPU2tYOWZESV8zZnhWQl9vN2lDeUd4TWZtQnBYMnZSekItYjcySmZ6cFFja1ZDckNyWW9vWHNaNlhOTDVicXpQQVZBeEotYW9QNGJFZ3IwMm9hV25IdTBjcVFyRWVNUzhXaWZJX0xPTkNGbUNKWDV1R2Y0WjN3RlNOd01nMTItdm5ERFZ1VENUMTFZT2JwQjE5NHQxSXB2UDNQeXJJemZnT3lsNlljQzhKSk9lRElKajFzbVd4WEpZME9nS0QwLTg3MS1aM050MldOX1ZCUm8tc0FLdXliUHo3UzV4U25FSk54UDVpejFDcjkycVpUNFVYa2czU0ppbWlnM2p0YXFUNHo3VDlVemhDX2FCRWpmVjB1TWtWRTZ1QlpJaldXM0Z4U2dVSmtJWTAyODhUQUh0ckFoYTVHYWo5ak5xbmthUkd5TnpyY1hjWVBGVHhjbHQ3NXRRaFBVaTl3YWlDQTY3UXU1bzhIRFc5WEVRd1Y2WjZLdVZBdHc3UG1aczFTb0xRTVhoX3E5OExjLU9SM3ZuUXpkYzN0eV9vTTJXRmtjMzMtODdqWFZmSmlHOTlXZzlwaUxNN1dLcEVfSDgzQkVSOTd0emVPVXRKSWwxMm9nU1pXazIyaHViNDRlM2lzTkFKVEtvMEI2Q1AyMWg2LTdtQlk1S3ZwT0dDdzQ0eFZjRFB5Q3dBWEllUlhDYzBoX0ZaX3NTRk5hNE9lTVJXWjhyRzR3TkNrSUZSbnROYV9nenpIcUhjZmIxcktKY1loOG05UTU4OUJuLUtualFBV3hYTjAyWFM4RkxBM2hBSm9DLW54QTJwMXZxbFVzNkRvbzVMZHU1cmFFVWV1cnBBVXlrUDM0eEZtVHRBSDVmdWpteVpIc0tmdUdVcXRXcFMzV19WMGw5X1F4TU9rdFhxV2dBalVMU045Nll4alRwOExpc3NyRXBhTGZWUzM4aWlhSi1BZHp6eEJvNUtuVjhwMGJuU2ZLbG1hTE1GNWxRYmpXY20xNkpyX3U0QWd1d2o5c2pCcTR2YjVZWmFYUFhKNkVybGE3d09SZ1o2aXdHTUVEeDlsSEtPejA2cF9FdE0tZ0RxYUdueGkzTjM0NUJYTW5iNXlxRUpKWkJFMTllOEh6N05qY1drUFR3Y21aSEFuNnZJSGNkd2RaS3FzajVGTEQ4alFrVFg4eTBSM1UwdFpyYmFwLUZvOURWbkY2VFRLQkE4a2NMb1VUY0VUNGFHZml6Znk2NS16SUdRdi1NaGRIaGU0b190V3ZtUnlpellaRVZNUERhVmNiN3duSnhQOHlYWDRPYXZkbmVJcC15R3l1QjZoRUMyNm45cE41TXJkMFMwVTRlaTBpMHdkZlFpRUJPWXJldnVjTGtqX2Fpc0dpOHZUcVRab29DdUVxZklWZ3hWYnhEQzk0TC1xcjRXTTVkRHpsZXRVc0lRUi1zYnZQa0hrSUlESDVLR18ydEI1Y29uY3FNSHhnZ2hjb2ZpU3JmZDVoMk9sRXVxMDFHUUFPSHVGSkF2OHlXYVZqSmRXY2l5TWhaQmZIUnlQVXFUNmZkZ0Q5YUZrOEZaZmY3djlyeW5QLXdubC1KR2dpUWx5azhiYkxQMWN2bjdyTldqaEVYR214dlpEMXl5VUFsdjhjcWRka2Z1RUl6TlpUSGZmam0yR0dqNWl6bmNNV1ZkZUFPclRDSlBaMXFpVjJVTlhfeG40QS1vLS1aWG9HUExnNTRaLUFOdWJHTWh4aEtsWkE2SWxoT0Y2dXp5MnJCLXc4enhQa3ducG5fSEV2VDVxTTVjMk05OVJkRjN4elRJR2hFR1E2aDdWNTdRWkx3c29JNnBSVkMyWFByVGQyRGJCdmpCZm1QLUVSbnZHZmdCQjJUUmlGRU03dGZUck5ZYm5zTWVNZjgwbXhkOXVrRTZ1Mms3dlM2RGJ0UlU2b3d1OVlrS0hSV0Q2MzVaZHM4SjJiZEQyeFhVZkRVdGpVenk2ZkdQc2t0bVJFVV9pajRmbTZPMTJHZ1VIXzY5X1hCXy1yMWxFRnhYTHlROEQ5akNHSDhJeThmSmw4eTI2eFR2RWtCSzF6bkNLSm16cHIyZXlScE5VVUdEY1lUR0k4VWpPMjR2UDRVVWh2TG80bFdObGFIZTRyOXJ0cVY2NWlZSjd2SXAzZFJDRTBWeDhMSWN6LWh0Q3hOUWxKRXBrS0lBS3lCcEN6M1cwVGpBU2lQVHlGVGFwOWJ3UEJhQk1tZ3BnaXBNU3N2c1Z5UDZ1Ull4bDF5US1EVXVJRXoxUTJJNUtGc2VVMlY0RWNFb3FuWVlYWS1DZFNiZzhZeHB5VndZWng4SlF1MU5IZl9ObVN3d01nTEgtUElsUjBheWRvbGJ3Ui12RXdOSW15cElObTdRVmtkTncuLg==');
INSERT INTO karunia_sessions VALUES ('4a4611569ced191fde3d922f8ec2f2eb5990a1a4', '114.142.172.40', 1566455186, 'Mld0VUJub1pPdTVoSEJWR2x4am5XNnJJVWdHalFUZ19takY0X1M2TWJjQ2IzMUNfNUNqdkQzWldhWHNlREFOOUtHN1FycDk0VWhqRGNMNm9Va1YyMXExQjJBT1BMTWNtNnRLY1RHdEh3Vm9VYV9xOEpUc3k0TC0tY3VSbjQ2TW5uVG9pSkFjQUJwZDFuMnhOcHRBaHpVQXlNVWtBSmZSeWpUcVVGSnFXcGpjcnozd2RwYlFWZ3J4V2xsN21IQnNyRHFvNGlsNlFSNWRRTnZoUVNldXZqQlE1RTFzUHNIbkNFZ0MxM1dvQjd5UjhhOWRGZVJpTWxUbndQVzFQNm9fUHF2Sy1qVEZJLWt3eG4zd25takRQNDBUWUgtUm8tZHU4OUZsU19yUjNWMldCMHlhbnh3WGF3Z000bUNMdG9jRF92N3RrMlpsbWttOUhvOEVDaHJEeWFmMi1qcW9LY3pTVjM3ZE1Xc2pGd0Fvc2JtTFU5SU94RU1HZy1XbWxqQnJIeTNYVE1WNDNFWHNJakZMTDF1MTBja01ObktWUi0zajdIUFNRSVZabmV5SnFKdDUxM2E0OC1zYk5qNnlGN3FiYVUyOVpVenFCUTNsNzNmX0tzMThpSDBfMjBzUmVkU0dscnJJNlo0MmxaaFNXLTR2dXFiV2pTYURuelUzdEVZZE9YYTdRd2NzTEVUcm1kSHRTTzh6X0t2bzVxOTJycXBFUlZ2OU8xUGd4SzlUbmZScS03YWl3Vll3UlJrLXdRaEFGVnN5Y3R2bFE0ZXF2VWZfQlcxNmRfY2Z4UWJIeG9mbFZBa09KZU40UUZBT0dQSEpwcXh5cnowWDJwaTU1dE9VdmVRRUhwazkxLTdpUGR1b0h2VVVRR0xOb3d1TG5ZTXhvaUxJbnVMNkpTVEdQaFlHNlQtcDlwcFJiSFpRY2pkSWpQa0FIc3Zvb3AtenBIeFpibmo5alAxS1ZtM1EwV2FjRW5Xclgxc2RlYjhleld6SWtlLUZTVnRQR1JlNzMxQ1VCTDk0VUVuSVBzdjVCRm8yYTlWZGRuVFFIbk1JQlBQaUNIOGo2NVV3VzhOdXp5c0REYk5DbnliSl9aMWo0bVRhWjdQcm02X1hrdFR5c1lkWl9KVTk3Ri1UaWFLUmNSUHZ0UlFpWDZ5RW8zZlBsRTRsWmw4LXZzcG82aVc4TndKTk5KTWJValN6VGhRWVpPcXBIaGtZaTBaQy1LSFZZc3NmeUNzVUt4SjVKNnBnUW1yZzVOQmlncjk1SlJ3cjV4a0lyM2JjVUZIYWtCMGZacmFJcENYRndxem5vMklQWmJ4MHBCbmRZSlAyVlhiM0g0anhQTDdUOXdxa3RBOXNSUzFYemVBbDZac1M5VkRXZmE0eW5QVkNlRW1vbzhraUhxcmZjSGhEWFI1SnZXS25jUVBJODNYbV85cHhRQWQwVGZMeFdoUnZLd3VkN2xWWDN2Q0wweVBpcmMtbDJla1ZEUy1mUWtqcHcyS3hpR0xEemJXY0MxV3FJT0lKa29WaERWLVctenFqQnpKVWx3RlVCdkYya2FWb0NxZnF0VzQ0UXFhQzRNV3ZuM0VnU0NiM1VmYzJPaFVNVktVRmxiX3h5RU94TE5pdHFNWTdyMXlJd0VFQnFPQmVQeHB2dHNNUmp5a2d5elRRZ2MyTDd3cWlsc0dhbUJ6VjVYMmV1czFwUWNPQ295alFoemtRQzZ1UjQweE5NRGZWaERqMy01QWVSUkFhZ25IUGxqRzhwUDlIS3JSQjFnaS04RVYwbWVBWmJ6NUdzMG5rbU05dUVpWXNXN0pFdEUtRklVWW9FWmxhbUpWYWR1LTRES1BNcmZJSlgyMXpYdUxtcFl1aWNVcUxQZTJwR3g0MDJySlYtWWJaVlVoelNsamNXWk1fdnAyZjZqMml0MUxSREtRbDdkTjNRYlZ1eHdqVXNzbDhaTTdqQkpjS0NFcFZFY2JOd3RLOGlkZEE3cnBzeUhUZXd1MXBpTXduQUdxTXdpazZOSkFRR0JFUVQ3ZkN6V3hJR1p1WXJFTi1JRFVodVJidG9neDJjcFVRNXZzS3FBcXhqc3J5OVJJXzJ1dnI0TEU1eG9hVXF6N3F6b3c1cGo2YTdBOVNwRmxoU0tUUGFLbUR1U09YTl9vRGVuU08yemh6QTBxWldPSVltTVg2RUh3aXdqdGFkS3RVcTlTMGxGcWc3T3BOa3g4ZjlBSklnMURzcy1kd0hTVE91UDk5bm9rVVZla2ZpMTVPc2RsVURNNXhaNjZZSEwxYnhNWF9MTms1ZDRNYWtMaW95TEJzN3ZycmZiX3U0THFSakdVYldPVUo1UG8tOTB1OHB4LVhCWkxjQnlfUXo3QXhTTy1vbzlFdi1LT18yNlE0QmEzbzRLR1NqMnJTZGRfMFdNM2tSaXVyLTZRY0wxWWFkVFZLekpxSXBNYkl6a2NtS3AyTGNBaWFZLWFTQzVmV0hGUzBUdkoxS1BycnR1dmVGeGJnMkdsZjVPNmFtY3ExQjJRbHJRT2xnTjJiQUUzUGZWaHhxLUJwTGd2SE9tOXJqT1RfVXN3TF9TTHhiczJaS25oMEFBQWR6bUZkNlpyVlBHalJOY3Q0S25UOHRHZFJRaHFsTXN5NVUwQURWcnVlQ0xoR2tOOWFidy1ZYlVYUkowSG1YQmcuLg==');
INSERT INTO karunia_sessions VALUES ('fb1e3567dfe47dfce013f53c77c0f57001410e46', '202.154.40.62', 1566455394, 'SmpwdloyZE16a1RlRi1vSU9ZZmlybW9Nb2VLVDM2bDdWQmhYdldjc0l6UVg2TDVSd2U1cFU4V0FYazVaMFlaRW9zMVprei1LU29ST0pldFIyUVhRUkNwNXJ1TkZjR21kVk9vZ0Y4QVpvVmMu');
INSERT INTO karunia_sessions VALUES ('66cd315c5b142554c76ae6b4248b33f109aef26c', '114.142.172.40', 1566451107, 'ekhEaFduOHcwWVZvWUVTMl9pcElreElLVGg3aGFYM2hDd093VkZGRE4xNmMxWHc3ZjBBcTJjdjlXZUNxem90QTZsTFpXZVJ0MllhOXlWWmIxRmZOSmpneTVOSV84clJ1cTJnS3hhSnpwaGRpeFktRFN1cE5fMW5SRk9VaUlORkRiSllGR0MzRjFNVmJnd0ZwUG02MzhFZG5EYWozcURNcTM2bDdLek5KWEhvdkFlWkQ1UWRnNUdpRTduUnFuRmFVMS1TTGE5cEg5LVlBa1c1TlptR256ZktkNGhKZXFmYmRySk52U2NLNG54NmRocjNMLVdtU0w4bmF0Ym5fT3luSENydGZ0bURNRktrSWpYa2tEeUFyblZ3RGZhb1VHODFMN2pIZnpYVnM4azFRd2ZqRFpscUIyOUdYaG1YX3d5VVU0aHpCTVpta3AzVGlVYTljYnFJSE95WFdEazloQkM3bEpsZHNIVlhtV0xJWTRnVUlUaDdsLWdQRGg2LXlhSy0tOFNIaUVnOGRzYm9uUjFjNl9Pc1cwZUkwZlpvZU9mMmExTnRKcElSRUFaeTVjNU5KVXkxN00zT00zaXN6QWV2RHJ2alljbVBIQnZMd2lFdXg0QXFzcDVLM1dYTlJEdnFES3p6WG5RWVFuZHIyWGR5ZWk0MlFxV2dmSHpFNXVyXzNrVXlyWmxCSFhFTE84REUzRDZNR01FX01zUVpIel8tQ1ljQU90M2xFVEVzNVYwczV1c1JJQ1ZsazZLTGJuTHhLSjl5NTV0T3FPNkNCRWZQMGNtWDJiMUt2bGZraDVIa25IcEZHZGRYTThpNmxzOFh1VUI5NnVMczFCTEhic1IxNzdyTk4tOGZFZ1hjc0tfeUlLMjFpUlJGWi1kN3czLWdPX2NVaEY0eGxUUDY1Tm5RbWNOdkFPYzd2MDU1Z0ZYQm50emFZYnQxZ2liUENTMG5ibnp6cXVOVF9OdnlMRVdoWkc2NUFmOVpEbGd5RjA2R0R4bmdhaHNQY3NsY295cDBRRzFzSDBSdmwtUFN5dkdqcjlxZGFQR2JadVdMSFI3R1FWRERQZlY5Uzd0OFhEcTRpMGtZdVY4WERyenplc1lsb2pEQlh6WmV3RkJQbjl5MUttM0wwOExwY0tMc19IbFRxdE9lNXBSWk9Da1BmTTdMTHJORnE4VC1nakdNVkZtcDlQUWVMbHZXT3BoMXBFWEM4MmJ1cFI2Q1JJbVkzeGZ1ZTFjZWNkQWRJeWNWNzdRcGJRd1hvZHgxZ2MyY1liRlh1YkpRcHBZQXZDbGo3d0ZFN0VTS3FUdU9BbS01c1hqVFNFRDZxdGRTNXFwdXlrNDRDX3c3eHJZSXZOWUM1alFjZW9zb1ZlaFV5T0EwM3U0dGUxaC03Ylg5WmMzOFpvTzVrbjZ5Nno1ZlBsMVNYYlo1alJDRmU3YlNHUzFUTFctTENldFdGWDNoWG1KMmpTclFyeFFva2U2MkZfd0Q1aTNvc0hIRUpHQWJ6Nm1LeDVFQlFoWlVUZkhzbFN4TjlMY3NNdVpFVXk3ZzY4aUFDaV9hTzN6UG1pT2tqLVZWOUN2MjVRVUhuX1Zqd09ieUNOUUFRdW94Q2FpcDBMalB0SUZSN0d1YmluaGlsSDIzZ21jUEo5QU5VaFhWam5iTTdaaDgwM29Sd1gtR3A3ZHQzeVByWHlXNU5uMXRWdDZXLVFrYzFOY0lyb3V0cERFVW9qRnBXWG16ZU8zUXN4RVN0ckxwWmpyWWowZU02UW5UbXVTUVFsY3lmNFFVLUdYWmdOdXV3c1dHQjFGUTY1Njc4Wnp0Y0trNHFrVzRYZzhSRWtlZ2g5SXBvNXlDNFBDN01DTGJERTVyWVNJNEZUMVF4OFN1azZpeGFxXzlwbm1MR3ZjTFdIdUFuVzcyWGQyRVJZR3kzRGc1M2NKa1FhdXU1QURvR2ZoVkdJZ2RUM2haYlVmSGZkZmE2Mm5fT2VxS1FiamVBWnJRUmlOc0hWSFJ2cGNWeDNxcFJDcU05dTZVTTVxbXkyeDY2QkNKTzNDTXBJdzMtcVBTbGNZTE1TS1NSZE5hME1kbExaMHVVeGM4T1g1OWVlMkNGQl9VaDlZWmotZDNieFZXZW9LZ09DQUtCOUp3SDJLUVJyMGxNNXZEaTVhcmdRVGhlNVVxSllmVm14bzhkZ3hlb2R0dWt1ZThwNEkxc2ZBaGNLOTN2REVYa01zaUEzSnVEb3hZNlBVdUlqdkJEVWw5QnhsNTVJcFNjYXhiVmlqNTdIaWRHOWRjSDhtc09pSUQzb2FxMnBMQjNuUE00OGJrTmZiNDVsMm5HcmhDTmVrTkV5MlVURXN6YUdENHFhLUxmUmFndWhMN3ZlV0lpMkZadUcyMVZqeGJnZFIyZmNIdEF5ZmtSTkIwbkhFcWpJSEVwNllpNGllV0FFRUpza2FnbHZybjJ5eUZOWnhSQ1VrQlJzME9HNTdONGxlMHRKVkVzUkpXTGYzTFBoYTVFcGpVSWEybWhoaUdKdmtaNWFWalRDbGJnNG9QRGtKQmlwNE0xaUxXUWh5eWhNLTJ4OU5iWVZXdjRrcE5aQ19ZQ0pNQ2RHOHlZcXVwMElTd1A1eFA2UDNJNkRWQVdoU1JCdEc1cTNwMjQyMnViVWxjakZ5c0ZRRFpNVnJyR1Y1YjNJUVNwWlEuLg==');


--
-- Data for Name: msOperator; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperator" VALUES (2, 'dani', '30ed55b89373c25492993b3d004440ff', 'dani n', '', NULL, '2019-06-28', 1, 2, '', NULL, NULL, 0, NULL, 0);
INSERT INTO "msOperator" VALUES (1, 'admin', '8dce1da12a8842a240d8cd9d47d2096c', 'Administrator', '02552', NULL, '2050-12-31', 1, 1, 'AP1', NULL, NULL, 0, '0', 0);


--
-- Data for Name: msOperatorAppShortCut; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorAppShortCut" VALUES (1, 1, 1, 1);


--
-- Data for Name: msOperatorDashboardPrivilege; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorDashboardPrivilege" VALUES (1, 1, 1, 1);
INSERT INTO "msOperatorDashboardPrivilege" VALUES (2, 1, 2, 1);


--
-- Data for Name: msOperatorGroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorGroup" VALUES (1, 'MD', 'Master Admin (MD)', 'Dapat mengakses semua menu, dan memberi privilages ke siapapun', NULL, '', '');


--
-- Data for Name: msOperatorModul; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorModul" VALUES (1, 'Set Status Order', 'set_status_order', 'Ada di menu order');
INSERT INTO "msOperatorModul" VALUES (2, 'Set Status PO', 'set_status_po', 'Ada di menu pembelian');


--
-- Data for Name: msOperatorPrivilege; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorPrivilege" VALUES (5762, 2, 100, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5763, 2, 101, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5764, 2, 111, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5765, 2, 112, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5766, 2, 113, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5767, 2, 114, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5768, 2, 115, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5769, 2, 116, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5770, 2, 117, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5771, 2, 118, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5772, 2, 119, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5773, 2, 120, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5774, 2, 121, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5775, 2, 122, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5776, 2, 123, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5777, 2, 124, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5778, 2, 125, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5779, 2, 126, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5780, 2, 127, 0);
INSERT INTO "msOperatorPrivilege" VALUES (5781, 2, 128, 0);
INSERT INTO "msOperatorPrivilege" VALUES (1272, 1, 100, 1);
INSERT INTO "msOperatorPrivilege" VALUES (3575, 1, 101, 1);
INSERT INTO "msOperatorPrivilege" VALUES (3688, 1, 111, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5745, 1, 112, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5746, 1, 113, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5747, 1, 114, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5748, 1, 115, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5749, 1, 116, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5750, 1, 117, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5751, 1, 118, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5752, 1, 119, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5753, 1, 120, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5754, 1, 121, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5755, 1, 122, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5756, 1, 123, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5757, 1, 124, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5758, 1, 125, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5759, 1, 126, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5760, 1, 127, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5761, 1, 128, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5782, 1, 129, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5783, 1, 130, 1);
INSERT INTO "msOperatorPrivilege" VALUES (5784, 1, 131, 1);


--
-- Data for Name: msOperatorSpecial; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "msOperatorSpecial" VALUES (1, 1, '0', 1, 1);
INSERT INTO "msOperatorSpecial" VALUES (2, 1, '0', 2, 1);
INSERT INTO "msOperatorSpecial" VALUES (3, 2, '0', 1, 1);
INSERT INTO "msOperatorSpecial" VALUES (4, 2, '0', 2, 1);


SET search_path = terminal, pg_catalog;

--
-- Data for Name: trDataLog; Type: TABLE DATA; Schema: terminal; Owner: postgres
--



--
-- Data for Name: tr_log; Type: TABLE DATA; Schema: terminal; Owner: postgres
--

INSERT INTO tr_log VALUES (9364, '', '2019-05-22 11:49:30.758685', '202.154.40.66       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9365, '', '2019-05-22 11:52:21.228845', '202.154.40.66       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9366, '', '2019-05-22 11:52:33.779365', '202.154.40.66       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9367, '', '2019-05-22 12:21:35.238489', '125.160.159.14      ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9368, '', '2019-05-24 09:11:17.176504', '159.253.145.183     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9369, '', '2019-05-24 11:17:13.71185', '103.104.12.174      ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9370, '', '2019-05-24 14:47:46.383748', '159.253.145.183     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9371, '', '2019-05-24 15:29:13.314869', '198.7.58.98         ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9372, '', '2019-05-25 12:24:55.490243', '103.104.12.174      ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9373, '', '2019-05-27 10:26:50.226266', '36.85.231.115       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9374, '', '2019-05-27 11:04:36.221137', '202.154.40.66       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9375, '', '2019-05-27 11:12:13.420734', '36.85.231.115       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9376, '', '2019-05-27 12:10:02.561929', '114.122.74.97       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9377, '', '2019-05-27 14:13:27.407008', '202.154.40.66       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9378, '', '2019-05-28 11:39:41.00653', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9379, '', '2019-05-28 11:45:10.343941', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9380, '', '2019-05-29 11:12:22.633456', '36.85.230.36        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9381, '', '2019-06-11 09:49:01.043431', '36.69.198.179       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9382, '', '2019-06-11 15:54:09.708361', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9383, '', '2019-06-12 12:30:59.193878', '36.72.0.8           ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9384, '', '2019-07-11 14:58:34.172857', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9385, '', '2019-07-11 16:20:51.687998', '36.79.248.8         ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9386, '', '2019-07-15 09:51:48.881643', '36.65.247.75        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9387, '', '2019-07-22 12:51:40.818049', '118.96.226.16       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9388, '', '2019-07-22 15:54:30.931629', '61.94.89.159        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9389, '', '2019-07-23 09:45:46.256868', '36.79.248.156       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9390, '', '2019-07-23 09:45:50.141589', '36.79.248.156       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9391, '', '2019-07-23 10:43:22.4177', '180.245.141.168     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9392, '', '2019-07-23 20:25:13.655322', '114.122.100.132     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9393, '', '2019-07-24 09:56:30.822904', '36.79.251.122       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9394, '', '2019-07-24 10:10:23.876942', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9395, '', '2019-07-31 10:42:29.744932', '36.71.235.171       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9396, '', '2019-08-01 17:15:37.404473', '36.65.244.115       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9397, '', '2019-08-02 10:14:44.62996', '180.245.183.55      ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9398, '', '2019-08-02 10:14:46.3259', '180.245.183.55      ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9399, '', '2019-08-05 08:40:53.328785', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9400, '', '2019-08-06 10:50:10.844193', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9401, '', '2019-08-06 12:25:03.265736', '36.85.229.69        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9402, '', '2019-08-06 14:47:20.286133', '36.85.229.69        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9403, '', '2019-08-07 09:57:14.277447', '36.80.97.111        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9404, '', '2019-08-09 22:24:01.758737', '36.79.192.152       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9405, '', '2019-08-13 09:45:34.723953', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9406, '', '2019-08-13 13:59:30.210784', '36.71.240.19        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9407, '', '2019-08-15 09:18:56.678017', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9408, '', '2019-08-15 09:23:08.464083', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9409, '', '2019-08-15 10:01:37.370066', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9410, '', '2019-08-15 10:04:16.076394', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9411, '', '2019-08-15 10:35:04.112003', '36.79.250.148       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9412, '', '2019-08-15 16:09:40.798548', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9413, '', '2019-08-16 14:02:51.724526', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9414, '', '2019-08-16 14:20:52.418231', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9415, '', '2019-08-16 14:33:09.528693', '36.65.243.81        ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9416, '', '2019-08-19 09:39:28.302038', '125.163.2.133       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9417, '', '2019-08-19 10:29:34.848426', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9418, '', '2019-08-19 10:45:34.304799', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9419, '', '2019-08-19 13:24:00.241225', '125.163.2.133       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9420, '', '2019-08-19 16:22:50.779347', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9421, '', '2019-08-19 17:09:39.418886', '125.163.2.133       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9422, '', '2019-08-19 17:15:46.863636', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9423, '', '2019-08-20 09:50:01.652368', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9424, '', '2019-08-20 11:45:48.788151', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9425, '', '2019-08-20 12:10:39.932781', '36.72.8.117         ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9426, '', '2019-08-20 15:18:56.981185', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9427, '', '2019-08-21 09:29:03.183397', '202.154.40.62       ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9428, '', '2019-08-21 09:44:06.509589', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9429, '', '2019-08-21 14:14:44.659335', '202.129.187.226     ', 1, 'admin login                   ', '', 0);
INSERT INTO tr_log VALUES (9430, '', '2019-08-22 11:36:38.39273', '114.142.172.40      ', 1, 'admin login                   ', '', 0);


SET search_path = "dataMaster", pg_catalog;

--
-- Name: msBarang_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msBarang"
    ADD CONSTRAINT "msBarang_pkey" PRIMARY KEY ("KodeBarang");


--
-- Name: msConfig_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msConfig"
    ADD CONSTRAINT "msConfig_pkey" PRIMARY KEY ("idConfig");


--
-- Name: msCustomer_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msCustomer"
    ADD CONSTRAINT "msCustomer_pkey" PRIMARY KEY ("idCustomer");


--
-- Name: msGudang_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msGudang"
    ADD CONSTRAINT "msGudang_pkey" PRIMARY KEY ("idGudang");


--
-- Name: msJenisKas_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msJenisKas"
    ADD CONSTRAINT "msJenisKas_pkey" PRIMARY KEY ("idJenisKas");


--
-- Name: msJenisKendaraan_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msJenisKendaraan"
    ADD CONSTRAINT "msJenisKendaraan_pkey" PRIMARY KEY ("idJenisKendaraan");


--
-- Name: msKelompokBarang_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msKelompokBarang"
    ADD CONSTRAINT "msKelompokBarang_pkey" PRIMARY KEY ("idKelompokBarang");


--
-- Name: msKendaraan_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msKendaraan"
    ADD CONSTRAINT "msKendaraan_pkey" PRIMARY KEY ("idKendaraan");


--
-- Name: msPengemudi_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msPengemudi"
    ADD CONSTRAINT "msPengemudi_pkey" PRIMARY KEY ("idPengemudi");


--
-- Name: msSatuan_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msSatuan"
    ADD CONSTRAINT "msSatuan_pkey" PRIMARY KEY ("idSatuan");


--
-- Name: msStatusOrder_copy1_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msStatusPO"
    ADD CONSTRAINT "msStatusOrder_copy1_pkey" PRIMARY KEY ("idStatusPO");


--
-- Name: msStatusOrder_pkey; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msStatusOrder"
    ADD CONSTRAINT "msStatusOrder_pkey" PRIMARY KEY ("idStatusOrder");


--
-- Name: msSupplier_pkey1; Type: CONSTRAINT; Schema: dataMaster; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msSupplier"
    ADD CONSTRAINT "msSupplier_pkey1" PRIMARY KEY ("idSupplier");


SET search_path = ho, pg_catalog;

--
-- Name: trOrderDet_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trOrderDetail"
    ADD CONSTRAINT "trOrderDet_pkey" PRIMARY KEY ("idOrderDetail");


--
-- Name: trOrder_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trOrder"
    ADD CONSTRAINT "trOrder_pkey" PRIMARY KEY ("NoOrder");


--
-- Name: trPODetail_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trPODetail"
    ADD CONSTRAINT "trPODetail_pkey" PRIMARY KEY ("idPODetail");


--
-- Name: trPOMiscDetail_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trPOMiscDetail"
    ADD CONSTRAINT "trPOMiscDetail_pkey" PRIMARY KEY ("idPOMiscDetail");


--
-- Name: trPOMisc_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trPOMisc"
    ADD CONSTRAINT "trPOMisc_pkey" PRIMARY KEY ("idPOMisc");


--
-- Name: trPO_pkey; Type: CONSTRAINT; Schema: ho; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trPO"
    ADD CONSTRAINT "trPO_pkey" PRIMARY KEY ("idPO");


SET search_path = "humanCapital", pg_catalog;

--
-- Name: msHobi_pkey; Type: CONSTRAINT; Schema: humanCapital; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msHobi"
    ADD CONSTRAINT "msHobi_pkey" PRIMARY KEY ("Code");


--
-- Name: msPendidikan_pkey; Type: CONSTRAINT; Schema: humanCapital; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msPendidikan"
    ADD CONSTRAINT "msPendidikan_pkey" PRIMARY KEY ("Code");


SET search_path = public, pg_catalog;

--
-- Name: AppDashboard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AppDashboard"
    ADD CONSTRAINT "AppDashboard_pkey" PRIMARY KEY ("idAppDashboard");


--
-- Name: AppMenus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AppMenus"
    ADD CONSTRAINT "AppMenus_pkey" PRIMARY KEY ("idAppMenu");


--
-- Name: AppShortCut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AppShortCut"
    ADD CONSTRAINT "AppShortCut_pkey" PRIMARY KEY ("idAppShortCut");


--
-- Name: fileUpload_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "fileUpload"
    ADD CONSTRAINT "fileUpload_pkey" PRIMARY KEY ("idFileUpload");


--
-- Name: msMasterOperatorSpecial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperatorModul"
    ADD CONSTRAINT "msMasterOperatorSpecial_pkey" PRIMARY KEY ("idOperatorModul");


--
-- Name: msOperatorAppShortCut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperatorAppShortCut"
    ADD CONSTRAINT "msOperatorAppShortCut_pkey" PRIMARY KEY ("idMsOperatorAppShortKey");


--
-- Name: msOperatorDashboardPrivilege_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperatorDashboardPrivilege"
    ADD CONSTRAINT "msOperatorDashboardPrivilege_pkey" PRIMARY KEY ("idDashboardPrivilege");


--
-- Name: msOperatorPrivilege_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperatorPrivilege"
    ADD CONSTRAINT "msOperatorPrivilege_pkey" PRIMARY KEY ("idPrivilege");


--
-- Name: msOperatorSepecial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperatorSpecial"
    ADD CONSTRAINT "msOperatorSepecial_pkey" PRIMARY KEY ("idOperatorSpecial");


--
-- Name: msOperator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "msOperator"
    ADD CONSTRAINT "msOperator_pkey" PRIMARY KEY ("idMsOperator");


SET search_path = terminal, pg_catalog;

--
-- Name: trDataLog_pkey; Type: CONSTRAINT; Schema: terminal; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "trDataLog"
    ADD CONSTRAINT "trDataLog_pkey" PRIMARY KEY ("idTrDataLog");


SET search_path = public, pg_catalog;

--
-- Name: karunia_sessions_timestamp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX karunia_sessions_timestamp ON karunia_sessions USING btree ("timestamp");


SET search_path = "dataMaster", pg_catalog;

--
-- Name: fk_jnskendaraan; Type: FK CONSTRAINT; Schema: dataMaster; Owner: postgres
--

ALTER TABLE ONLY "msKendaraan"
    ADD CONSTRAINT fk_jnskendaraan FOREIGN KEY ("fidJenisKendaraan") REFERENCES "msJenisKendaraan"("idJenisKendaraan") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_satuanbsr; Type: FK CONSTRAINT; Schema: dataMaster; Owner: postgres
--

ALTER TABLE ONLY "msBarang"
    ADD CONSTRAINT fk_satuanbsr FOREIGN KEY ("fidSatuanBesar") REFERENCES "msSatuan"("idSatuan") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_satuankcl; Type: FK CONSTRAINT; Schema: dataMaster; Owner: postgres
--

ALTER TABLE ONLY "msBarang"
    ADD CONSTRAINT fk_satuankcl FOREIGN KEY ("fidSatuanKecil") REFERENCES "msSatuan"("idSatuan") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_supplier; Type: FK CONSTRAINT; Schema: dataMaster; Owner: postgres
--

ALTER TABLE ONLY "msBarang"
    ADD CONSTRAINT fk_supplier FOREIGN KEY ("fidSupplier") REFERENCES "msSupplier"("idSupplier") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: customerRelationship; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA "customerRelationship" FROM PUBLIC;
REVOKE ALL ON SCHEMA "customerRelationship" FROM postgres;
GRANT ALL ON SCHEMA "customerRelationship" TO postgres;
--GRANT ALL ON SCHEMA "customerRelationship" TO postgres_tmguser WITH GRANT OPTION;


--
-- Name: dataMaster; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA "dataMaster" FROM PUBLIC;
REVOKE ALL ON SCHEMA "dataMaster" FROM postgres;
GRANT ALL ON SCHEMA "dataMaster" TO postgres;
--GRANT ALL ON SCHEMA "dataMaster" TO postgres_tmguser WITH GRANT OPTION;


--
-- Name: humanCapital; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA "humanCapital" FROM PUBLIC;
REVOKE ALL ON SCHEMA "humanCapital" FROM postgres;
GRANT ALL ON SCHEMA "humanCapital" TO postgres;
--GRANT ALL ON SCHEMA "humanCapital" TO postgres_tmguser WITH GRANT OPTION;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: terminal; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA terminal FROM PUBLIC;
REVOKE ALL ON SCHEMA terminal FROM postgres;
GRANT ALL ON SCHEMA terminal TO postgres;
--GRANT ALL ON SCHEMA terminal TO postgres_tmguser WITH GRANT OPTION;


--
-- Name: msAccount; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msAccount" FROM PUBLIC;
REVOKE ALL ON TABLE "msAccount" FROM postgres;
GRANT ALL ON TABLE "msAccount" TO postgres;
--GRANT ALL ON TABLE "msAccount" TO postgres_tmg;


--
-- Name: msBarang; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msBarang" FROM PUBLIC;
REVOKE ALL ON TABLE "msBarang" FROM postgres;
GRANT ALL ON TABLE "msBarang" TO postgres;
--GRANT ALL ON TABLE "msBarang" TO postgres_tmg;


--
-- Name: msConfig; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msConfig" FROM PUBLIC;
REVOKE ALL ON TABLE "msConfig" FROM postgres;
GRANT ALL ON TABLE "msConfig" TO postgres;
--GRANT ALL ON TABLE "msConfig" TO postgres_tmg;


--
-- Name: msCustomer; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msCustomer" FROM PUBLIC;
REVOKE ALL ON TABLE "msCustomer" FROM postgres;
GRANT ALL ON TABLE "msCustomer" TO postgres;
--GRANT ALL ON TABLE "msCustomer" TO postgres_tmg;


--
-- Name: msGudang; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msGudang" FROM PUBLIC;
REVOKE ALL ON TABLE "msGudang" FROM postgres;
GRANT ALL ON TABLE "msGudang" TO postgres;
--GRANT ALL ON TABLE "msGudang" TO postgres_tmg;


--
-- Name: msJenisKas; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msJenisKas" FROM PUBLIC;
REVOKE ALL ON TABLE "msJenisKas" FROM postgres;
GRANT ALL ON TABLE "msJenisKas" TO postgres;
--GRANT ALL ON TABLE "msJenisKas" TO postgres_tmg;


--
-- Name: msJenisKendaraan; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msJenisKendaraan" FROM PUBLIC;
REVOKE ALL ON TABLE "msJenisKendaraan" FROM postgres;
GRANT ALL ON TABLE "msJenisKendaraan" TO postgres;
--GRANT ALL ON TABLE "msJenisKendaraan" TO postgres_tmg;


--
-- Name: msKelompokBarang; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msKelompokBarang" FROM PUBLIC;
REVOKE ALL ON TABLE "msKelompokBarang" FROM postgres;
GRANT ALL ON TABLE "msKelompokBarang" TO postgres;
--GRANT ALL ON TABLE "msKelompokBarang" TO postgres_tmg;


--
-- Name: msKendaraan; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msKendaraan" FROM PUBLIC;
REVOKE ALL ON TABLE "msKendaraan" FROM postgres;
GRANT ALL ON TABLE "msKendaraan" TO postgres;
--GRANT ALL ON TABLE "msKendaraan" TO postgres_tmg;


--
-- Name: msPengemudi; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msPengemudi" FROM PUBLIC;
REVOKE ALL ON TABLE "msPengemudi" FROM postgres;
GRANT ALL ON TABLE "msPengemudi" TO postgres;
--GRANT ALL ON TABLE "msPengemudi" TO postgres_tmg;


--
-- Name: msSatuan; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msSatuan" FROM PUBLIC;
REVOKE ALL ON TABLE "msSatuan" FROM postgres;
GRANT ALL ON TABLE "msSatuan" TO postgres;
--GRANT ALL ON TABLE "msSatuan" TO postgres_tmg;


--
-- Name: msStatusOrder; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msStatusOrder" FROM PUBLIC;
REVOKE ALL ON TABLE "msStatusOrder" FROM postgres;
GRANT ALL ON TABLE "msStatusOrder" TO postgres;
--GRANT ALL ON TABLE "msStatusOrder" TO postgres_tmg;


--
-- Name: msStatusPO; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msStatusPO" FROM PUBLIC;
REVOKE ALL ON TABLE "msStatusPO" FROM postgres;
GRANT ALL ON TABLE "msStatusPO" TO postgres;
--GRANT ALL ON TABLE "msStatusPO" TO postgres_tmg;


--
-- Name: msSupplier; Type: ACL; Schema: dataMaster; Owner: postgres
--

REVOKE ALL ON TABLE "msSupplier" FROM PUBLIC;
REVOKE ALL ON TABLE "msSupplier" FROM postgres;
GRANT ALL ON TABLE "msSupplier" TO postgres;
--GRANT ALL ON TABLE "msSupplier" TO postgres_tmg;


SET search_path = ho, pg_catalog;

--
-- Name: trOrder; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trOrder" FROM PUBLIC;
REVOKE ALL ON TABLE "trOrder" FROM postgres;
GRANT ALL ON TABLE "trOrder" TO postgres;
--GRANT ALL ON TABLE "trOrder" TO postgres_tmg;


--
-- Name: trOrderDetail; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trOrderDetail" FROM PUBLIC;
REVOKE ALL ON TABLE "trOrderDetail" FROM postgres;
GRANT ALL ON TABLE "trOrderDetail" TO postgres;
--GRANT ALL ON TABLE "trOrderDetail" TO postgres_tmg;


--
-- Name: trPO; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trPO" FROM PUBLIC;
REVOKE ALL ON TABLE "trPO" FROM postgres;
GRANT ALL ON TABLE "trPO" TO postgres;
--GRANT ALL ON TABLE "trPO" TO postgres_tmg;


--
-- Name: trPODetail; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trPODetail" FROM PUBLIC;
REVOKE ALL ON TABLE "trPODetail" FROM postgres;
GRANT ALL ON TABLE "trPODetail" TO postgres;
--GRANT ALL ON TABLE "trPODetail" TO postgres_tmg;


--
-- Name: trPOMisc; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trPOMisc" FROM PUBLIC;
REVOKE ALL ON TABLE "trPOMisc" FROM postgres;
GRANT ALL ON TABLE "trPOMisc" TO postgres;
--GRANT ALL ON TABLE "trPOMisc" TO postgres_tmg;


--
-- Name: trPOMiscDetail; Type: ACL; Schema: ho; Owner: postgres
--

REVOKE ALL ON TABLE "trPOMiscDetail" FROM PUBLIC;
REVOKE ALL ON TABLE "trPOMiscDetail" FROM postgres;
GRANT ALL ON TABLE "trPOMiscDetail" TO postgres;
--GRANT ALL ON TABLE "trPOMiscDetail" TO postgres_tmg;


SET search_path = "humanCapital", pg_catalog;

--
-- Name: msEmployee; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msEmployee" FROM PUBLIC;
REVOKE ALL ON TABLE "msEmployee" FROM postgres;
GRANT ALL ON TABLE "msEmployee" TO postgres;
--GRANT ALL ON TABLE "msEmployee" TO postgres_tmg;


--
-- Name: msHobi; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msHobi" FROM PUBLIC;
REVOKE ALL ON TABLE "msHobi" FROM postgres;
GRANT ALL ON TABLE "msHobi" TO postgres;
--GRANT ALL ON TABLE "msHobi" TO postgres_tmg;


--
-- Name: msOrganizationStructure; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msOrganizationStructure" FROM PUBLIC;
REVOKE ALL ON TABLE "msOrganizationStructure" FROM postgres;
GRANT ALL ON TABLE "msOrganizationStructure" TO postgres;
--GRANT ALL ON TABLE "msOrganizationStructure" TO postgres_tmg;


--
-- Name: msPendidikan; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msPendidikan" FROM PUBLIC;
REVOKE ALL ON TABLE "msPendidikan" FROM postgres;
GRANT ALL ON TABLE "msPendidikan" TO postgres;
--GRANT ALL ON TABLE "msPendidikan" TO postgres_tmg;


--
-- Name: msPosition; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msPosition" FROM PUBLIC;
REVOKE ALL ON TABLE "msPosition" FROM postgres;
GRANT ALL ON TABLE "msPosition" TO postgres;
--GRANT ALL ON TABLE "msPosition" TO postgres_tmg;


--
-- Name: msReligion; Type: ACL; Schema: humanCapital; Owner: postgres
--

REVOKE ALL ON TABLE "msReligion" FROM PUBLIC;
REVOKE ALL ON TABLE "msReligion" FROM postgres;
GRANT ALL ON TABLE "msReligion" TO postgres;
--GRANT ALL ON TABLE "msReligion" TO postgres_tmg;


SET search_path = public, pg_catalog;

--
-- Name: AppDashboard; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "AppDashboard" FROM PUBLIC;
REVOKE ALL ON TABLE "AppDashboard" FROM postgres;
GRANT ALL ON TABLE "AppDashboard" TO postgres;
--GRANT ALL ON TABLE "AppDashboard" TO postgres_tmg;


--
-- Name: AppMenus; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "AppMenus" FROM PUBLIC;
REVOKE ALL ON TABLE "AppMenus" FROM postgres;
GRANT ALL ON TABLE "AppMenus" TO postgres;
--GRANT ALL ON TABLE "AppMenus" TO postgres_tmg;


--
-- Name: AppShortCut; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "AppShortCut" FROM PUBLIC;
REVOKE ALL ON TABLE "AppShortCut" FROM postgres;
GRANT ALL ON TABLE "AppShortCut" TO postgres;
--GRANT ALL ON TABLE "AppShortCut" TO postgres_tmg;


--
-- Name: fileUpload; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "fileUpload" FROM PUBLIC;
REVOKE ALL ON TABLE "fileUpload" FROM postgres;
GRANT ALL ON TABLE "fileUpload" TO postgres;
--GRANT ALL ON TABLE "fileUpload" TO postgres_tmg;


--
-- Name: karunia_sessions; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE karunia_sessions FROM PUBLIC;
REVOKE ALL ON TABLE karunia_sessions FROM postgres;
GRANT ALL ON TABLE karunia_sessions TO postgres;
--GRANT ALL ON TABLE karunia_sessions TO postgres_tmg;


--
-- Name: msOperator; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperator" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperator" FROM postgres;
GRANT ALL ON TABLE "msOperator" TO postgres;
--GRANT ALL ON TABLE "msOperator" TO postgres_tmg;


--
-- Name: msOperatorAppShortCut; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorAppShortCut" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorAppShortCut" FROM postgres;
GRANT ALL ON TABLE "msOperatorAppShortCut" TO postgres;
--GRANT ALL ON TABLE "msOperatorAppShortCut" TO postgres_tmg;


--
-- Name: msOperatorDashboardPrivilege; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorDashboardPrivilege" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorDashboardPrivilege" FROM postgres;
GRANT ALL ON TABLE "msOperatorDashboardPrivilege" TO postgres;
--GRANT ALL ON TABLE "msOperatorDashboardPrivilege" TO postgres_tmg;


--
-- Name: msOperatorGroup; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorGroup" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorGroup" FROM postgres;
GRANT ALL ON TABLE "msOperatorGroup" TO postgres;
--GRANT ALL ON TABLE "msOperatorGroup" TO postgres_tmg;


--
-- Name: msOperatorModul; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorModul" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorModul" FROM postgres;
GRANT ALL ON TABLE "msOperatorModul" TO postgres;
--GRANT ALL ON TABLE "msOperatorModul" TO postgres_tmg;


--
-- Name: msOperatorPrivilege; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorPrivilege" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorPrivilege" FROM postgres;
GRANT ALL ON TABLE "msOperatorPrivilege" TO postgres;
--GRANT ALL ON TABLE "msOperatorPrivilege" TO postgres_tmg;


--
-- Name: msOperatorSpecial; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "msOperatorSpecial" FROM PUBLIC;
REVOKE ALL ON TABLE "msOperatorSpecial" FROM postgres;
GRANT ALL ON TABLE "msOperatorSpecial" TO postgres;
--GRANT ALL ON TABLE "msOperatorSpecial" TO postgres_tmg;


SET search_path = terminal, pg_catalog;

--
-- Name: trDataLog; Type: ACL; Schema: terminal; Owner: postgres
--

REVOKE ALL ON TABLE "trDataLog" FROM PUBLIC;
REVOKE ALL ON TABLE "trDataLog" FROM postgres;
GRANT ALL ON TABLE "trDataLog" TO postgres;
--GRANT ALL ON TABLE "trDataLog" TO postgres_tmg;


--
-- Name: tr_log; Type: ACL; Schema: terminal; Owner: postgres
--

REVOKE ALL ON TABLE tr_log FROM PUBLIC;
REVOKE ALL ON TABLE tr_log FROM postgres;
GRANT ALL ON TABLE tr_log TO postgres;
--GRANT ALL ON TABLE tr_log TO postgres_tmg;


--
-- PostgreSQL database dump complete
--

